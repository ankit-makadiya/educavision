<?php
$lang['msg_login_success'] = 'Welcome to Workout';
$lang['msg_logout_success'] = 'You\'ve been logged out successfully';
$lang['msg_payment_success'] = 'Your payment has been successfully done';

// Error messages
$lang['error_msg_name'] = 'Please enter your name';
$lang['error_msg_firstname'] = 'Please enter your firstname';
$lang['error_msg_lastname'] = 'Please enter your lastname';
$lang['error_msg_phone'] = 'Please enter your phone number';
$lang['error_msg_gender'] = 'Please enter your gender';
$lang['error_msg_valid_gender'] = 'Please enter your valid gender';
$lang['error_msg_tokengenerate'] = 'Please restart your application,Invalid token';
$lang['error_msg_email'] = 'Please enter your email';
$lang['error_msg_pass'] = 'Please enter your password';
$lang['error_msg_email_exits'] = 'An account with this email address already exists';
$lang['error_msg_password_sent_to_email'] = 'A new password has been sent to your e-mail address';
$lang['error_msg_email_not_found'] = 'We could not find your email address!';
$lang['error_msg_subject'] = 'Please enter your subject';
$lang['error_msg_message'] = 'Please enter your message';
$lang['error_msg_not_able_to_send_msg'] = 'We are not able to send the message at the moment';
$lang['error_msg_user_id'] = 'Please enter your user id';
$lang['error_msg_userid'] = 'Please enter user id';
$lang['error_msg_latitude'] = 'Please enter latitude';
$lang['error_msg_longitude'] = 'Please enter longitude';
$lang['error_msg_old_pass_message'] = 'Please enter old password';
$lang['error_msg_new_pass_message'] = 'Please enter new password';
$lang['error_msg_details'] = 'Please provide the details';
$lang['error_msg_correct_old_password'] = 'Please enter correct old password';
$lang['error_msg_login'] = 'Please enter valid login details';
$lang['error_msg_rating'] = 'Please enter your ratings';
$lang['error_account_blocked'] = 'Your account is blocked';
$lang['error_account_login'] = 'You are already logged in in other device , kindly logout and try again...';
$lang['error_something_wrong'] = 'Something went wrong';
$lang['error_email_failed'] = 'Email can not be sent';
$lang['error_msg_countrycode'] = 'Please enter country code';
$lang['error_msg_location'] = 'Please select location';
$lang['error_msg_change_avatar'] = 'Please choose photo';

$lang['error_device_type'] = 'Please define device type';
$lang['error_token_key'] = 'Please define token key';
$lang['error_device_id'] = 'Please define device id';
$lang['error_login_from'] = 'Please define login from';
$lang['error_fb_id'] = 'Please enter facebook id';
$lang['error_google_id'] = 'Please enter google id';
$lang['error_social_id'] = 'Please enter social id';

$lang['no_record_found'] = 'No record found';

// Success messages
$lang['success_msg_signup'] = 'Congratulations! your signup successfully completed';
$lang['success_change_password'] = 'Your password has been successfully changed';
$lang['success_msg_sent_message'] = 'Your message has been sent successfully';
$lang['success_msg_job_saved'] = 'Thank you for posting your job. Employee will contact you soon in few seconds and you will get notified by notifications';
$lang['success_msg_profile_saved'] = 'Your profile data has been successfully updated';
$lang['success_msg_photo_saved'] = 'Your profile avatar has been successfully updated';
$lang['success_no_profile_update'] = 'No data has been updated';

$lang['success_job_accepted'] = 'We have received your acceptance of the job';
$lang['success_job_rejected'] = 'You have declined a job offer';
$lang['success_job_canceled'] = 'You have canceled a job you\'ve accepted';
$lang['success_job_completed'] = 'Thank you for completing your job. You will receive a notification upon approval by job owner';
$lang['success_job_approved'] = 'You have approved a job completion';
$lang['success_job_disapproved'] = 'You have disapproved a job completion';
$lang['success_job_closed'] = 'Your job has been closed';
$lang['success_job_reopened'] = 'Your job has been reopened';
$lang['success_job_not_reopened'] = 'This job is now cancelled';
$lang['success_rating_submit'] = 'Your rating has been submitted';
$lang['success_preference_set'] = 'The preference has been set successfully';

// Push Notifications
$lang['push_job_offered'] = '%s sent request to work with you for %s to %s translation';
$lang['push_job_accepted'] = 'Congratulations! %s has accepted your job';
$lang['push_job_canceled'] = '%s has canceled your accepted job for %s to %s translation';
$lang['push_job_completed'] = 'Congratulations! %s has completed your job for %s to %s translation';
$lang['push_job_approved'] = 'Congratulations! %s has approved your job completion for %s to %s translation';
$lang['push_job_disapproved'] = 'Your job completion for %s to %s translation has been disapproved by %s';
$lang['push_job_rated'] = 'You are rated by %s for %s to %s translation';

// Job STATUS
$lang['label_job_posted'] = 'Posted';
$lang['label_job_accepted'] = 'Accepted';
$lang['label_job_canceled'] = 'Canceled';
$lang['label_job_completed'] = 'Completed';
$lang['label_job_approved'] = 'Approved';
$lang['label_job_disapproved'] = 'Disapprove';
