<?php
$lang['msg_login_success'] = 'Tervetuloa SomeLounge';
$lang['msg_logout_success'] = 'Olet kirjautunut ulos onnistuneesti';

// Error messages
$lang['error_msg_name'] = 'Syötä nimesi';
$lang['error_msg_phone'] = 'Syötä puhelinnumerosi';
$lang['error_msg_bio'] = 'Kirjoita lisätiedot profiiliisi';
$lang['error_msg_email'] = 'Syötä sähköpostiosoitteesi';
$lang['error_msg_pass'] = 'Syötä salasanasi';
$lang['error_msg_email_exits'] = 'tällä sähköpostiosoitteella on jo voimassaoleva tili';
$lang['error_msg_password_sent_to_email'] = 'Uusi salasana on lähetetty sähköpostiosoitteen';
$lang['error_msg_email_not_found'] = 'Sähköpostia ei löytynyt';
$lang['error_msg_subject'] = 'Syötä aihe';
$lang['error_msg_message'] = 'Syötä viesti';
$lang['error_msg_not_able_to_send_msg'] = 'Emme pysty lähettämään viestiä juuri nyt';
$lang['error_msg_user_id'] = 'Syötä käyttäjänimesi';
$lang['error_msg_job_id'] = 'Kirjoita toimeksiannon ID-numero';
$lang['error_msg_job_status'] = 'Kirjoita toimeksiannon tila';
$lang['error_msg_old_pass_message'] = 'Syötä vanha salasana';
$lang['error_msg_new_pass_message'] = 'Syötä uusi salasana';
$lang['error_msg_details'] = 'Kirjoita lisätiedot';
$lang['error_msg_correct_old_password'] = 'Syötä oikea vanha salasana';
$lang['error_msg_login'] = 'Syötä voimassaolevat tiedot';
$lang['error_msg_rating'] = 'Syötä arvostelusi';
$lang['error_account_blocked'] = 'Tilisi on lukittu';
$lang['error_something_wrong'] = 'jokin meni pieleen';
$lang['error_email_failed'] = 'sähköpostia ei voi lähettää';

$lang['error_job_accept'] = 'Pahoittelemme, toimeksianto on jo hyväksytty.';
$lang['error_job_cancel'] = 'Et voi peruttaa tätä toimeksiantoa';
$lang['error_job_complete'] = 'Et voi suorittaa tätä toimeksiantoa';
$lang['error_job_approve'] = 'Et voi hyväksyä tätä toimeksiantoa';
$lang['error_job_disapprove'] = 'Et voi hylätä tätä toimeksiantoa';
$lang['error_job_reopen'] = 'Et voi avata uudelleen tätä toimeksiantoa';

$lang['no_record_found'] = 'tietoa ei löytynyt';

// Success messages
$lang['success_msg_signup'] = 'Onneksi olkoon! rekisteröintisi on onnistunut';
$lang['success_change_password'] = 'Salasanasi on onnistunesti muutettu';
$lang['success_msg_sent_message'] = 'Viestisi on lähetetty onnistuneesti';
$lang['success_msg_job_saved'] = 'Kiitos toimeksiannon julkaisemisesta. Tulkki ottaa sinuun pian yhteyttä.';
$lang['success_msg_profile_saved'] = 'Profiilisi on päivitetty onnistuneesti';
$lang['success_msg_photo_saved'] = 'Profiilisi kuva on päivitetty onnistuneesti';
$lang['success_no_profile_update'] = 'mitään tietoja ei ole päivitetty';

$lang['success_job_accepted'] = 'Toimeksiannon hyväksymisesi on vastaanotettu.';
$lang['success_job_rejected'] = 'Olet hylännyt toimeksiannon.';
$lang['success_job_canceled'] = 'Olet perunut hyväksytyn toimeksiannon.';
$lang['success_job_completed'] = 'Kiitos tulkkaamisesta! Saat pian toimeksiantajalta hyväksynnän tulkkauksen suorittamisesta.';
$lang['success_job_approved'] = 'Olet hyväksynyt tulkkaussuorituksen.';
$lang['success_job_disapproved'] = 'Tulkkauksesi on hylätty.';
$lang['success_job_closed'] = 'Toimeksiantosi on poistettu.';
$lang['success_job_reopened'] = 'Toimeksianto on julkaistu uudelleen.';
$lang['success_job_not_reopened'] = 'Toimenanto on nyt peruttu';
$lang['success_rating_submit'] = 'Arvostelusi on lähetetty';
$lang['success_preference_set'] = 'Valintasi on vaihdettu onnistuneesti.';

// Push Notifications
$lang['push_job_offered'] = '%s lähetti tulkkausyhteistyöpyynnön kielistä %s - %s';
$lang['push_job_accepted'] = 'Onnekski olkoon! %s on hyväksynyt toimeksiantosi.';
$lang['push_job_canceled'] = '%s on hylännyt hyväksymäsi tulkkauksen kielistä %s - %s';
$lang['push_job_completed'] = 'Onneksi olkoon! %s on suorittanut tulkkauksen kielistä %s - %s';
$lang['push_job_approved'] = 'Onneksi olkoon! %s on hyväksynyt tulkkauksen kielistä %s - %s';
$lang['push_job_disapproved'] = '%s hylkäsi tulkkaussuorituksesi kielistä %s - %s';
$lang['push_job_rated'] = '%s arvosteli %s - %s tulkkauksen.';

// Job STATUS
$lang['label_job_posted'] = 'Julkaistu';
$lang['label_job_accepted'] = 'Hyväksytty';
$lang['label_job_canceled'] = 'Perutut';
$lang['label_job_completed'] = 'Suoritetut';
$lang['label_job_approved'] = 'Hyväksytty';
$lang['label_job_disapproved'] = 'Hylätty';
