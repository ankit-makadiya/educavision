<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('common');
    }

    public function getAppSetting() {
        $condition_array = array('status' => '1');
        $appSettingData = $this->common->select_data_by_condition('settings', $condition_array, '*', $sortby = 'setting_id', $orderby = 'ASC', $limit = '', $offset = '', $join_str = array(), $group_by = '');

        $appSetting = array();
        $appSetting['app_name'] = $appSettingData[0]['setting_val'];
        $appSetting['app_owner_name'] = $appSettingData[1]['setting_val'];
        $appSetting['app_email'] = $appSettingData[2]['setting_val'];
        $appSetting['app_sender_email'] = $appSettingData[3]['setting_val'];
        $appSetting['app_receiver_email'] = $appSettingData[4]['setting_val'];
        $appSetting['smtp_host_name'] = $appSettingData[6]['setting_val'];
        $appSetting['smtp_out_going_port'] = $appSettingData[7]['setting_val'];
        $appSetting['smtp_user_name'] = $appSettingData[8]['setting_val'];
        $appSetting['smtp_password'] = $appSettingData[9]['setting_val'];
        $appSetting['app_phone'] = $appSettingData[5]['setting_val'];

        return $appSetting;
    }

    public function sendEmail($subject = '', $template = '', $to_email = '') {
        $mail_body = '';
        $mail_body .= $this->email_template_header();
        $mail_body .= $template;
        $mail_body .= $this->email_template_footer();
        
        $this->load->library('email');

        $getAppSetting = $this->getAppSetting();

        $CI = &get_instance();
        $CI->load->library('email');

        $CI->email->initialize(array(
            'protocol' => 'smtp',
            'smtp_host' => $getAppSetting['smtp_host_name'],
            'smtp_user' => $getAppSetting['smtp_user_name'],
            'smtp_pass' => $getAppSetting['smtp_password'],
            'smtp_port' => $getAppSetting['smtp_out_going_port'],
            'smtp_crypto' => 'ssl',
            'crlf' => "\r\n",
            'newline' => "\r\n",
            'mailtype' => "html",
            'charset' => "utf-8",
        ));
        $CI->email->from($getAppSetting['app_sender_email'], $getAppSetting['app_name']);
        $CI->email->to(!empty($to_email) ? $to_email : $getAppSetting['app_receiver_email']);
        //$CI->email->bcc('educa@aol.com');
        //$CI->email->reply_to(!empty($to_email) ? $to_email : $getAppSetting['app_receiver_email'], $getAppSetting['app_name']);
        $CI->email->subject($subject);
        $CI->email->message($mail_body);

        if (!$CI->email->send()) {
//            echo $CI->email->print_debugger();
//            print_r($CI->email->ErrorInfo);
//
//            show_error($CI->email->print_debugger());
//            return 0;
        }

        return true;
    }

    public function email_template_header() {
        $return_html = '<!doctype html>
<html>
  <head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Email</title>
    <style>
    @media only screen and (max-width: 620px) {
      table[class=body] h1 {
        font-size: 28px !important;
        margin-bottom: 10px !important;
      }
      table[class=body] p,
            table[class=body] ul,
            table[class=body] ol,
            table[class=body] td,
            table[class=body] span,
            table[class=body] a {
        font-size: 16px !important;
      }
      table[class=body] .wrapper,
            table[class=body] .article {
        padding: 10px !important;
      }
      table[class=body] .content {
        padding: 0 !important;
      }
      table[class=body] .container {
        padding: 0 !important;
        width: 100% !important;
      }
      table[class=body] .main {
        border-left-width: 0 !important;
        border-radius: 0 !important;
        border-right-width: 0 !important;
      }
      table[class=body] .btn table {
        width: 100% !important;
      }
      table[class=body] .btn a {
        width: 100% !important;
      }
      table[class=body] .img-responsive {
        height: auto !important;
        max-width: 100% !important;
        width: auto !important;
      }
      table.table_border tr td{
        border:1px solid #ccc;
      }
    }
    @media all {
      .ExternalClass {
        width: 100%;
      }
      .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div {
        line-height: 100%;
      }
      .apple-link a {
        color: inherit !important;
        font-family: inherit !important;
        font-size: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
        text-decoration: none !important;
      }
      .btn-primary table td:hover {
        background-color: #34495e !important;
      }
      .btn-primary a:hover {
        background-color: #34495e !important;
        border-color: #34495e !important;
      }
    }
    </style>
  </head>
  <body class="" style="background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
    <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;">
      <tr>
        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
        <td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; Margin: 0 auto; max-width: 80%; padding: 10px; width: 80%;">
          <div class="content" style="box-sizing: border-box; display: block; Margin: 0 auto; padding: 10px;">
            <span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">This is preheader text. Some clients will show this text as a preview.</span>
            <table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ddd; border-radius: 3px;">
              <tr style="border-bottom:1px solid #000;">
                  <td style="background-color: #ddd; padding: 10px; width:32%">
                  	<div style="font-size: 20px; color: #f2741a; text-align: left;"><img src="' . base_url() . '/assets/frameworks/adminlte/img/logo.png" width="100" /></div>
                  </td>
                  <td style="background-color: #ddd; padding: 10px;width:32%">
                  	<div style="font-size: 20px; color: #f2741a; text-align: center;">' . $this->getAppSetting()['app_name'] . '</div>
                  </td>
                  <td style="background-color: #ddd; padding: 10px;width:32%">
                  	<div style="font-size: 20px; color: #f2741a; text-align: right;">
                  	<p style="font-size:11px;"><a href="mailto:' . $this->getAppSetting()['app_email'] . '">' . $this->getAppSetting()['app_email'] . '</a></p>
                  	<p style="font-size:11px;">' . $this->getAppSetting()['app_phone'] . '</p>
                  	</div>
                  </td>
              </tr>
              <tr>
                <td colspan="3" class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">
                  <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                    <tr>
                      <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">
                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">';
        return $return_html;
    }

    public function email_template_footer() {
        $return_html = '</p>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <div class="footer" style="clear: both; Margin-top: 10px; text-align: center; width: 100%;">
              <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                <tr>
                  <td class="content-block powered-by" style="font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 12px; color: #999999; text-align: center;">
                    Developed by <a href="javascript:void(0);" style="color: #0099ff; font-size: 12px; text-align: center; text-decoration: none;">Educavision</a>.
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </td>
        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
      </tr>
    </table>
  </body>
</html>';
        return $return_html;
    }

}

/**
 * Back end Controller
 *
 */
class Admin_Controller extends MY_Controller {

    public function __construct() {
        parent::__construct();

        /* Load */
        $this->load->config('admin/config');
        $this->load->library(array('form_validation', 'admin/template'));

        /* Data */
        $this->data['title'] = $this->config->item('title');
        $this->data['title_lg'] = $this->config->item('title_lg');
        $this->data['title_full'] = $this->config->item('title_full');
        $this->data['title_mini'] = $this->config->item('title_mini');
        $this->data['frameworks_dir'] = $this->config->item('frameworks_dir');
        $this->data['plugins_dir'] = $this->config->item('plugins_dir');
        $this->data['avatar_dir'] = $this->config->item('avatar_dir');

        $admin_session = $this->session->get_userdata('sl_admin');
        if (isset($admin_session['sl_admin'])) {
            $page_permission = $this->session->get_userdata('sl_admin')['sl_admin']['permissions'];
            $page_permission = explode(',', $page_permission);

            $this->data['admin_permission'] = $page_permission;
        }
        // date_default_timezone_set('Africa/Cairo');
    }

    public function last_query() {
        echo "<pre>";
        echo $this->db->last_query();
        echo "</pre>";
    }

    public function sendEmailBKP($app_name = '', $app_email = '', $to_email = '', $subject = '', $mail_body = '') {
        $this->config->load('email', TRUE);
        $this->cnfemail = $this->config->item('email');

        //Loading E-mail Class
        $this->load->library('email');
        $this->email->initialize($this->cnfemail);

        $this->email->from($app_email, $app_name);
        $this->email->to($to_email);
        $this->email->subject($subject);

        $this->email->message("<table border='0' cellpadding='0' cellspacing='0'><tr><td></td></tr><tr><td>" . $mail_body . "</td></tr></table>");
        $this->email->send();
        return;
    }

    function getCountryNameById($id = '') {
        if (!empty($id)) {
            $contition_array = array('id' => $id);
            $data = 'name';
            $country_data = $this->common_model->select_data_by_condition('country', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
            if (!empty($country_data)) {
                return $country_data[0]['name'];
            }
        }
    }

    function getStateNameById($id = '') {
        if (!empty($id)) {
            $contition_array = array('id' => $id);
            $data = 'name';
            $state_data = $this->common_model->select_data_by_condition('state', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
            if (!empty($state_data)) {
                return $state_data[0]['name'];
            }
        }
    }

    function getCityNameById($id = '') {
        if (!empty($id)) {
            $contition_array = array('id' => $id);
            $data = 'name';
            $city_data = $this->common_model->select_data_by_condition('city', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
            if (!empty($city_data)) {
                return $city_data[0]['name'];
            }
        }
    }

    function getProductDataBySku($sku = '') {
        if (!empty($sku)) {
            $contition_array = array('sku' => $sku);
            $data = '*';
            $product_data = $this->common_model->select_data_by_condition('product_variation', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
            if (!empty($product_data)) {
                return $product_data[0];
            }
        }
    }

}

/**
 * Default Front-end Controller
 *
 */
class Public_Controller extends MY_Controller {

    function __construct() {
        parent::__construct();
        // Load any front-end only dependencies
    }

    function getCartData() {
        if (!empty($this->session->userdata('cart_contents'))):
            $cartData = $this->session->userdata('cart_contents');
            $cart_shipping_country = '';
            if (isset($cartData['cart_shipping_country'])) {
                $cart_shipping_country = $cartData['cart_shipping_country'];
            }
            $contition_array = array('is_deleted' => '0', 'status' => 1, 'country_id' => $cart_shipping_country);
            $data = 'id, tax_label, tax_rate';
            $tax_data = $this->common->select_data_by_condition('tax', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

            $cart_shipping_charge = 0;
            foreach ($this->cart->contents() as $key => $items):
                $contition_array = array('product.is_deleted' => '0', 'product.status' => 1, 'pv.sku' => $items['id'], 'a.slug' => $items['book_type']);
                $data = 'product.id, product.name, pv.price, product.slug, pv.sku, product.author_name, product.image, pv.weight, pv.isbn_no, collection, a.slug as book_type';

                $join_str = array();
                $join_str[0]['table'] = 'product_variation as pv';
                $join_str[0]['join_table_id'] = 'pv.product_id';
                $join_str[0]['from_table_id'] = 'product.id';
                $join_str[0]['join_type'] = 'left';

                $join_str[1]['table'] = 'attribute as a';
                $join_str[1]['join_table_id'] = 'a.id';
                $join_str[1]['from_table_id'] = 'pv.attribute_id';
                $join_str[1]['join_type'] = 'left';

                $product_data = $this->common->select_data_by_condition('product', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '', $join_str, $custom_order_by = '', $group_by = '');

                $product_tax = 0;
                if (!empty($product_data)):
                    if (!empty($tax_data)):
                        $product_tax = ($product_data[0]['price'] * $items['qty'] * $tax_data[0]['tax_rate']) / 100;
                    endif;
                    if ($items['book_type'] != 'e-book'):
                        $cart_shipping_charge += $product_data[0]['price'] * $items['qty'];
                    endif;
                    $updateData = array();
                    $updateData['rowid'] = $key;
                    $updateData['name'] = strip_tags($product_data[0]['name']);
                    $updateData['price'] = $product_data[0]['price'];
                    $updateData['slug'] = $product_data[0]['slug'];
                    $updateData['id'] = $product_data[0]['product_id'];
                    $updateData['author_name'] = $product_data[0]['author_name'];
                    $updateData['image'] = $product_data[0]['image'];
                    $updateData['weight'] = $product_data[0]['weight'];
                    $updateData['isbn_no'] = $product_data[0]['isbn_no'];
                    $updateData['collection'] = $product_data[0]['collection'];
                    $updateData['book_type'] = $product_data[0]['book_type'];
                    $this->cart->update($updateData);
                else:
                    $this->cart->remove($key);
                endif;
            endforeach;
            // after update product cart data sync again cart data

            $cartData = $this->session->userdata('cart_contents');

            $cartData['cart_tax'] = '0';
            $cartData['cart_shipping'] = '0';
            $cartData['cart_discount'] = '0';
            $cartData['cart_grand_total'] = '0';
            $cartData['cart_shipping_country'] = $cart_shipping_country;

            $condition_array = array('is_deleted' => '0', 'status' => 1, 'country_id' => $cart_shipping_country);
            $search_condition = "'$cart_shipping_charge' BETWEEN price_from AND price_to";
            $data = 'id, price_from, price_to, amount';
            $shipping_data = $this->common->select_data_by_search('shipping', $search_condition, $condition_array, $data, $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = '', $custom_order_by = '', $group_by = '');
            if (!empty($cartData)) {
                //$cartData['cart_total'] = $this->cart->format_number($cartData['cart_total']);
                if (!empty($tax_data)) {
                    $cartData['cart_tax_data'] = $tax_data[0];
                    $cartData['cart_tax'] += $this->cart->format_number(($tax_data[0]['tax_rate'] * $cartData['cart_total']) / 100);
                }
                if (!empty($shipping_data)) {
                    $cartData['cart_shipping'] += $this->cart->format_number($shipping_data[0]['amount']);
                }
                //calculate discount
                $product_discount = 0;
                $shipping_discount = 0;
                $tax_discount = 0;
                if (isset($cartData['cart_coupon_data']) && !empty($cartData['cart_coupon_data'])) {
                    $pricing_data = $cartData['cart_coupon_data']['pricing_data'];
                    if (!empty($pricing_data[0]) && $pricing_data[0]['item_type'] == 'Discount') {
                        if ($pricing_data[0]['offer_type'] == '$ Off') {
                            $discount_different = $cartData['cart_total'] - $pricing_data[0]['offer_vale'];
                            if ($discount_different < 0) {
                                $product_discount = $cartData['cart_total'];
                            } else {
                                $product_discount = $pricing_data[0]['offer_vale'];
                            }
                        } else {
                            $discount_different = $cartData['cart_total'] - (($cartData['cart_total'] * $pricing_data[0]['offer_vale']) / 100);
                            if ($discount_different < 0) {
                                $product_discount = $cartData['cart_total'];
                            } else {
                                $product_discount = ($cartData['cart_total'] * $pricing_data[0]['offer_vale']) / 100;
                            }
                        }
                    }
                    if (!empty($pricing_data[1]) && $pricing_data[1]['item_type'] == 'Shipping') {
                        if ($pricing_data[1]['is_free_shipping'] == 1) {
                            $shipping_discount = $cartData['cart_shipping'];
                        } else {
                            if ($pricing_data[1]['offer_type'] == '$ Off') {
                                $discount_different = $cartData['cart_shipping'] - $pricing_data[1]['offer_vale'];
                                if ($discount_different < 0) {
                                    $shipping_discount = $cartData['cart_shipping'];
                                } else {
                                    $shipping_discount = $pricing_data[1]['offer_vale'];
                                }
                            } else {
                                $discount_different = $cartData['cart_shipping'] - (($cartData['cart_shipping'] * $pricing_data[1]['offer_vale']) / 100);
                                if ($discount_different < 0) {
                                    $shipping_discount = $cartData['cart_shipping'];
                                } else {
                                    $shipping_discount = ($cartData['cart_shipping'] * $pricing_data[1]['offer_vale']) / 100;
                                }
                            }
                        }
                    }
                    if (!empty($pricing_data[2]) && $pricing_data[2]['item_type'] == 'Tax') {
                        if ($pricing_data[2]['offer_type'] == '$ Off') {
                            $discount_different = $cartData['cart_tax'] - $pricing_data[2]['offer_vale'];
                            if ($discount_different < 0) {
                                $tax_discount = $cartData['cart_tax'];
                            } else {
                                $tax_discount = $pricing_data[2]['offer_vale'];
                            }
                        } else {
                            $discount_different = $cartData['cart_tax'] - (($cartData['cart_tax'] * $pricing_data[2]['offer_vale']) / 100);
                            if ($discount_different < 0) {
                                $tax_discount = $cartData['cart_tax'];
                            } else {
                                $tax_discount = ($cartData['cart_tax'] * $pricing_data[2]['offer_vale']) / 100;
                            }
                        }
                    }
                }
//				echo $product_discount.'_'.$shipping_discount.'_'.$tax_discount;
//				exit;

                $cartData['cart_discount'] = $this->cart->format_number($product_discount + $shipping_discount + $tax_discount);
                //$cartData['cart_grand_total'] = $this->cart->format_number(($cartData['cart_total'] + $cartData['cart_tax'] + $cartData['cart_shipping']) - $cartData['cart_discount']);
                $cart_grand_total = ($cartData['cart_total'] + $cartData['cart_tax'] + $cartData['cart_shipping']) - $cartData['cart_discount'];
                if ($cart_grand_total > 0) {
                    $cartData['cart_grand_total'] = $cart_grand_total;
                } else {
                    $cartData['cart_grand_total'] = 0;
                }
            }
            $this->session->set_userdata('cart_contents', $cartData);
        endif;
        return $cart_contents = $this->session->userdata('cart_contents');
    }

    function getCountryNameById($id = '') {
        if (!empty($id)) {
            $contition_array = array('id' => $id);
            $data = 'name';
            $country_data = $this->common->select_data_by_condition('country', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
            if (!empty($country_data)) {
                return $country_data[0]['name'];
            }
        }
    }

    function getStateNameById($id = '') {
        if (!empty($id)) {
            $contition_array = array('id' => $id);
            $data = 'name';
            $state_data = $this->common->select_data_by_condition('state', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
            if (!empty($state_data)) {
                return $state_data[0]['name'];
            }
        }
    }

    function getCityNameById($id = '') {
        if (!empty($id)) {
            $contition_array = array('id' => $id);
            $data = 'name';
            $city_data = $this->common->select_data_by_condition('city', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
            if (!empty($city_data)) {
                return $city_data[0]['name'];
            }
        }
    }

    function getProductDataBySku($sku = '') {
        if (!empty($sku)) {
            $contition_array = array('sku' => $sku);
            $data = '*';
            $product_data = $this->common->select_data_by_condition('product', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
            if (!empty($product_data)) {
                return $product_data[0];
            }
        }
    }

    public function sendEmailAddress($subject = '', $message = '', $to_email = '') {
        $getAppSetting = $this->getAppSetting();
        $to_email = !empty($to_email) ? $to_email : $getAppSetting['app_receiver_email'];
        $list = is_array($to_email) ? $to_email : (array) $to_email;
        $CI = &get_instance();
        $CI->load->library('email');

        $CI->email->initialize(array(
            'protocol' => 'smtp',
            'smtp_host' => $getAppSetting['smtp_host_name'],
            'smtp_user' => $getAppSetting['smtp_user_name'],
            'smtp_pass' => $getAppSetting['smtp_password'],
            'smtp_port' => $getAppSetting['smtp_out_going_port'],
            'smtp_crypto' => 'ssl',
            'crlf' => "\r\n",
            'newline' => "\r\n",
            'mailtype' => "html",
            'charset' => "utf-8",
        ));
        $CI->email->from($getAppSetting['app_sender_email'], $getAppSetting['app_name']);
        $CI->email->to($list);
        $CI->email->reply_to($getAppSetting['app_sender_email'], $getAppSetting['app_name']);
        $CI->email->subject($subject);
        $CI->email->message($message);

        if ($CI->email->send()) {
            return true;
        } else {
            echo $CI->email->print_debugger();
            print_r($CI->email->ErrorInfo);

            show_error($CI->email->print_debugger());
            return false;
        }
    }

    public function savecart() {
        $front_user = $this->session->get_userdata('educavision_front');
        if (isset($front_user['educavision_front']) && !empty($front_user['educavision_front']) && !empty($this->cart->contents())) {

            $session_user_id = $front_user['educavision_front']['id'];
            foreach ($this->cart->contents() as $key => $items):
                $cart_data = array();
                $cart_data['user_id'] = $session_user_id;
                // $cart_data['product_id'] = $items['id'];
                // $cart_data['product_sku'] = $items['sku'];
                $cart_data['product_id'] = $items['product_id'];
                $cart_data['product_sku'] = $items['id'];
                $cart_data['qty'] = $items['qty'];
                $cart_data['price'] = $items['price'];

                $condition_array = array('user_id' => $session_user_id, 'product_sku' => $items['id']);
                $getCartData = $this->common->select_data_by_condition('savecart', $condition_array, '*', $sortby = '', $orderby = 'ASC', $limit = '', $offset = '', $join_str = array(), $group_by = '');
                if (!empty($getCartData)) {
                    $updateData = $this->common->update_data($cart_data, 'savecart', 'id', $getCartData[0]['id']);
                } else {
                    $insertData = $this->common->insert_data_get_id($cart_data, 'savecart');
                }
            endforeach;
        }
    }

    public function removesavecart($productSku = '') {
        $front_user = $this->session->get_userdata('educavision_front');
        if (isset($front_user['educavision_front'])) {
            $session_user_id = $front_user['educavision_front']['id'];

            $delete_array = array();
            $delete_array['user_id'] = $session_user_id;
            if (!empty($productSku)) {
                $delete_array['product_sku'] = $productSku;
            }
            $this->db->delete('savecart', $delete_array);
        }
    }

}
