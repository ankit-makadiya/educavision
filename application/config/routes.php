<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = 'page404';
$route['translate_uri_dashes'] = FALSE;


$route['page/form-save'] = 'page/page_form_save';
$route['page/(:any)'] = 'page/index/$1';
$route['catalog'] = 'catalog/index';
$route['catalog/(:any)'] = 'catalog/index/$1';
$route['catalog/(:any)/(:any)'] = 'catalog/index/$1/$2';
$route['book/(:any)'] = 'book/index/$1';
$route['404'] = 'front/page404';
$route['registration'] = 'user/registration';
$route['user/save-registration'] = 'user/save_registration';
$route['login'] = 'user/login';
$route['logout'] = 'user/logout';
$route['search'] = 'catalog/search';
$route['news/(:any)'] = 'news/index/$1';
$route['news-add-comments'] = 'news/add_comments';
$route['user/login-authentication'] = 'user/login_authentication';
$route['user/forgot-password'] = 'user/forgotPassword';
$route['checkout'] = 'shoppingcart/checkout';
$route['calculate-shipping-tax'] = 'shoppingcart/calculate_shipping_tax';






$route['admin'] = 'admin/login';


$route['admin/country'] = 'admin/localization/index';
$route['admin/country/edit/(:any)'] = 'admin/localization/countryEdit/$1';

$route['admin/state'] = 'admin/localization/state';
$route['admin/state/edit/(:any)'] = 'admin/localization/stateEdit/$1';

$route['admin/city'] = 'admin/localization/city';
$route['admin/city/edit/(:any)'] = 'admin/localization/cityEdit/$1';
$route['admin/city/(:any)/(:any)'] = 'admin/localization/city/$1/$2';

$route['admin/getStateList'] = 'admin/localization/getStateList';

$route['admin/educavision/section/edit/(:any)'] = 'admin/educavision/section_edit/$1';
$route['admin/educavision/section/manage/(:any)'] = 'admin/educavision/section_manage/$1';

$route['admin/menu/getmenuitem'] = 'admin/menu/getMenuItem';
$route['admin/menu/getcategorymenuitemdata'] = 'admin/menu/getCategoryMenuItemData';
$route['admin/menu/getcategorymenu'] = 'admin/menu/getCategoryMenu';

$route['admin/news/comments/(:any)'] = 'admin/news/news_comments/$1';

$route['admin/role'] = 'admin/users/role';
$route['admin/role/edit/(:any)'] = 'admin/users/roleEdit/$1';
$route['admin/role/add'] = 'admin/users/roleAdd/';

$route['admin/users'] = 'admin/users/index';
$route['admin/users/edit/(:any)'] = 'admin/users/edit/$1';
$route['admin/customers'] = 'admin/users/customers';

$route['admin/get-attribute-html'] = 'admin/product/getAttributeHtml';

$route['admin/product/exportExcel'] = 'admin/product/exportExcel';
$route['admin/product/import_product'] = 'admin/product/import_product';
$route['admin/product/add'] = 'admin/product/add';
$route['admin/product/edit'] = 'admin/product/edit';
$route['admin/product/search'] = 'admin/product/search';
$route['admin/product/checkunique'] = 'admin/product/checkunique';
$route['admin/product/(:any)'] = 'admin/product/index/$1';


// Payment Gateway Details

$route['order'] = "Order/index";

$route['dashboard'] = "User/dashboard";
$route['change-password'] = "User/changePassword";
$route['change-password-save'] = "User/changePasswordSave";
$route['edit-profile'] = "User/editProfile";
$route['order-history'] = "User/orderHistory";
$route['order-details/(:any)'] = "User/orderDetails/$1";
$route['thankyou/(:any)'] = "Shoppingcart/thankyou/$1";

$route['update-shipping-tax'] = "Shoppingcart/updateShippingTax";



$route['update-product-variation'] = 'admin/login/product_variation';
