<?php 
// Server file
class PushNotifications extends CI_Model {

	// (Windows Phone 8) The name of our push channel.
    private static $channelName = "";
	
	// Sends Push notification for Android users
	public function android($data = array(), $deviceToken = array()) {
	
		$fields = [
			'registration_ids'	=> $deviceToken,
			'notification'  	=> $data,
			'data'          	=> $data
		];
		 
		$headers = array(
            'Authorization: key=' . $this->config->item('api_access_key'),
            'Content-Type: application/json'
        );
		
		// Open connection
        $ch = curl_init();
		
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		
		// Execute post
        $result = curl_exec($ch);

        if (curl_errno($ch)) {
            return curl_error($ch);
        }
		
        // Close connection
        curl_close($ch);

        return $result;
	}
	
	// Sends Push's toast notification for Windows Phone 8 users
	public function WP($data, $uri) {
	
		$delay = 2;
		$msg =  "<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
		        "<wp:Notification xmlns:wp=\"WPNotification\">" .
		            "<wp:Toast>" .
		                "<wp:Text1>".htmlspecialchars($data['mtitle'])."</wp:Text1>" .
		                "<wp:Text2>".htmlspecialchars($data['mdesc'])."</wp:Text2>" .
						"<wp:Text3>".htmlspecialchars($data['job_id'])."</wp:Text2>" .
		            "</wp:Toast>" .
		        "</wp:Notification>";
		
		$sendedheaders =  array(
		    'Content-Type: text/xml',
		    'Accept: application/*',
		    'X-WindowsPhone-Target: toast',
		    "X-NotificationClass: $delay"
		);
		
		$response = $this->useCurl($uri, $sendedheaders, $msg);
		
		$result = array();
		foreach(explode("\n", $response) as $line) {
		    $tab = explode(":", $line, 2);
		    if (count($tab) == 2)
		        $result[$tab[0]] = trim($tab[1]);
		}
		
		return $result;
	}
	
    // Sends Push notification for iOS users
	public function iOS($data = array(), $deviceToken = '') {

		$ctx = stream_context_create();
		// ck.pem is your certificate file
		stream_context_set_option($ctx, 'ssl', 'local_cert', $this->config->item('certificate'));
		stream_context_set_option($ctx, 'ssl', 'passphrase', $this->config->item('passphrase'));

		// Open a connection to the APNS server
		if ($this->config->item('sandbox') == TRUE) {
			$url = $this->config->item('push_gateway_sandbox');
		} else {
			$url = $this->config->item('push_gateway');
		}
		
		$fp = stream_socket_client($url, $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

		if (!$fp)
			exit("Failed to connect: $err $errstr" . PHP_EOL);

		// Create the payload body
		$body['aps'] = array(
			'badge' => +1,
			'alert' => array(
                'title'		=> $data['mtitle'],
                'body'		=> $data['mdesc'],
                'job_id'	=> $data['job_id'],
				'user_id'	=> $data['user_id'],
				'notify_id'	=> $data['notify_id'],
				'type'		=> $data['type']
             ),
			'sound' => 'ringtone.aiff',
			'content-available' => 1
		);

		// Encode the payload as JSON
		$payload = json_encode($body);

		// Build the binary notification
		foreach($deviceToken as $token){
			$msg = chr(0) . @pack('n', 32) . @pack('H*', $token) . @pack('n', strlen($payload)) . $payload;
			
			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));
		}
		
		// Close the connection to the server
		fclose($fp);

		if (!$result)
			return false;
		else
			return $result;
	}
	
	// Curl 
	private function useCurl(&$model, $url, $headers, $fields = null) {
		// Open connection
		$ch = curl_init();
		if ($url) {
			// Set the url, number of POST vars, POST data
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	 
			// Disabling SSL Certificate support temporarly
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			if ($fields) {
				curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
			}
	 
			// Execute post
			$result = curl_exec($ch);
			if ($result === FALSE) {
				die('Curl failed: ' . curl_error($ch));
			}
	 
			// Close connection
			curl_close($ch);

			return $result;
		}
    }

}
