<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Common class.
 *
 * @extends CI_Model
 */
class Common extends CI_Model {

    /**
     * __construct function.
     *
     * @access public
     * @return void
     */
    public function __construct() {

        parent::__construct();
        $this->load->database();
    }

    // insert database
    function insert_data($data, $tablename) {
        if ($this->db->insert($tablename, $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    // insert database
    function insert_data_get_id($data, $tablename) {
        if ($this->db->insert($tablename, $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    // update database
    function update_data($data, $tablename, $columnname, $columnid) {
        $this->db->where($columnname, $columnid);
        if ($this->db->update($tablename, $data)) {
            return true;
        } else {
            return false;
        }
    }

    // select data using colum id
    function select_data_by_id($tablename, $columnname, $columnid, $data = '*', $join_str = array()) {
        $this->db->select($data);
        if (!empty($join_str)) {
            foreach ($join_str as $join) {
                if ($join['join_type'] == '') {
                    $this->db->join($join['table'], $join['join_table_id'] . '=' . $join['from_table_id']);
                } else {
                    $this->db->join($join['table'], $join['join_table_id'] . '=' . $join['from_table_id'], $join['join_type']);
                }
            }
        }
        $this->db->where($columnname, $columnid);
        $query = $this->db->get($tablename);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    // select data using multiple conditions
    function select_data_by_condition($tablename, $contition_array = array(), $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $group_by = '') {
        $this->db->select($data);

        if (!empty($join_str)) {
            foreach ($join_str as $join) {
                if ($join['join_type'] == '') {
                    $this->db->join($join['table'], $join['join_table_id'] . '=' . $join['from_table_id']);
                } else {
                    $this->db->join($join['table'], $join['join_table_id'] . '=' . $join['from_table_id'], $join['join_type']);
                }
            }
        }

        $this->db->where($contition_array);


        //Setting Limit for Paging
        if ($limit != '' && $offset == 0) {
            $this->db->limit($limit);
        } else if ($limit != '' && $offset != 0) {
            $this->db->limit($limit, $offset);
        }
        //order by query
        if ($sortby != '' && $orderby != '') {
            $this->db->order_by($sortby, $orderby);
        }
        if ($group_by != '') {
            $this->db->group_by($group_by);
        }

        $query = $this->db->get($tablename);

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    // select data using multiple conditions and search keyword
    function select_data_by_search($tablename, $search_condition, $contition_array = array(), $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = '', $custom_order_by = '', $group_by = '') {
        $this->db->select($data);
        if (!empty($join_str)) {
            foreach ($join_str as $join) {
                if ($join['join_type'] == '') {
                    $this->db->join($join['table'], $join['join_table_id'] . '=' . $join['from_table_id']);
                } else {
                    $this->db->join($join['table'], $join['join_table_id'] . '=' . $join['from_table_id'], $join['join_type']);
                }
            }
        }
        $this->db->where($contition_array);
        $this->db->where($search_condition);

        //Setting Limit for Paging
        if ($limit != '' && $offset == 0) {
            $this->db->limit($limit);
        } else if ($limit != '' && $offset != 0) {
            $this->db->limit($limit, $offset);
        }
        //order by query
        if ($sortby != '' && $orderby != '') {
            $this->db->order_by($sortby, $orderby);
        }

        if ($custom_order_by != '') {
            //echo 'Jam';die;
            $this->db->order_by($custom_order_by);
        }
        if ($group_by != '') {
            $this->db->group_by($group_by);
        }

        $query = $this->db->get($tablename);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    // delete data
    function delete_data($tablename, $columnname, $columnid) {
        $this->db->where($columnname, $columnid);
        if ($this->db->delete($tablename)) {
            return true;
        } else {
            return false;
        }
    }

    // check unique avaliblity
    function check_unique_avalibility($tablename, $columname1, $columnid1_value, $columname2, $columnid2_value, $condition_array = array()) {
        // if edit than $columnid2_value use

        if ($columnid2_value != '') {
            $this->db->where($columname2 . " !=", $columnid2_value);
        }

        if (!empty($condition_array)) {
            $this->db->where($condition_array);
        }

        $this->db->where($columname1, $columnid1_value);
        $query = $this->db->get($tablename);
        if ($query->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }

    //get all record
    function get_all_record($tablename, $data = '*', $sortby = '', $orderby = '') {
        $this->db->select($data);
        $this->db->from($tablename);
        $this->db->where('status', 'Enable');
        if ($sortby != '' && $orderby != "") {
            $this->db->order_by($sortby, $orderby);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    //table records count
    function get_count_of_table($table, $where = array()) {
        if (count($where) > 0) {
            $this->db->where($where);
        }

        $query = $this->db->count_all_results($table);

        return $query;
    }

    //table Next Auto Increment ID
    function get_autoincrement_id($table) {
        $this->db->select_max('class_id');
        $Q = $this->db->get($table);
        $row = $Q->row_array();
        return $row['class_id'];
    }

    // check email id
    function chkemail($id, $email) {
        if ($id != 0) {
            $option = array('userid !=' => $id, 'useremail' => $email);
        } else {
            $option = array('useremail' => $email);
        }
        $query = $this->db->get_where('users', $option);
        if ($query->num_rows() > 0) {
            return 'old';
        } else {
            return 'new';
        }
    }

    //This function get all records from table by name
    function getallrecordbytablename($tablename, $data, $conditionarray = '', $limit = '', $offset = '', $sortby = '', $orderby = '') {

        //$this->db->order_by($sortby, $orderby);
        //Setting Limit for Paging
        if ($limit != '' && $offset == 0) {
            $this->db->limit($limit);
        } else if ($limit != '' && $offset != 0) {
            $this->db->limit($limit, $offset);
        }

        //Executing Query
        $this->db->select($data);
        $this->db->from($tablename);
        if ($conditionarray != '') {
            $this->db->where($conditionarray);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    //This function get all open record count
    function get_open_request_count($condition) {
        $this->db->select('COUNT(requestid) as count,maincategoryuniqid');
        $this->db->where($condition);
        $this->db->from('request');
        $this->db->group_by('maincategoryuniqid');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    // select data using colum id
    function select_database_id($tablename, $columnname, $columnid, $data = '*', $condition_array = array()) {
        $this->db->select($data);
        $this->db->where($columnname, $columnid);
        if (!empty($condition_array)) {
            $this->db->where($condition_array);
        }
        $query = $this->db->get($tablename);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    // change status
    function change_status($data, $tablename, $columnname, $columnid) {
        $this->db->where($columnname, $columnid);
        if ($this->db->update($tablename, $data)) {
            return true;
        } else {
            return false;
        }
    }

    function get_name_by_id($tablename, $columnname, $condition) {
        $this->db->select($columnname);
        $this->db->where($condition);
        $this->db->from($tablename);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return array();
        }
    }

    // Random password
    function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 6; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    function getSettings($key = '') {
        $this->db->select('*');

        if ($key != '') {
            $this->db->where("setting_key", $key);
        }

        $this->db->from("settings");

        $query = $this->db->get();
        //echo $this->db->last_query();die();

        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            return $result;
        } else {
            return array();
        }
    }

    function timeAgo($time_ago) {
        $time_ago = strtotime($time_ago);
        $cur_time = time();
        $time_elapsed = $cur_time - $time_ago;
        $seconds = $time_elapsed;
        $minutes = round($time_elapsed / 60);
        $hours = round($time_elapsed / 3600);
        $days = round($time_elapsed / 86400);
        $weeks = round($time_elapsed / 604800);
        $months = round($time_elapsed / 2600640);
        $years = round($time_elapsed / 31207680);
        // Seconds
        if ($seconds <= 60) {
            return "just now";
        }
        //Minutes
        else if ($minutes <= 60) {
            if ($minutes == 1) {
                return "1 min";
            } else {
                return "$minutes mins";
            }
        }
        //Hours
        else if ($hours <= 24) {
            if ($hours == 1) {
                return "an hour";
            } else {
                return "$hours hrs";
            }
        }
        //Days
        else if ($days <= 7) {
            if ($days == 1) {
                return "yesterday";
            } else {
                return "$days days";
            }
        }
        //Weeks
        else if ($weeks <= 4.3) {
            if ($weeks == 1) {
                return "a week";
            } else {
                return "$weeks weeks";
            }
        }
        //Months
        else if ($months <= 12) {
            if ($months == 1) {
                return "a month";
            } else {
                return "$months months";
            }
        }
        //Years
        else {
            if ($years == 1) {
                return "one year";
            } else {
                return "$years years";
            }
        }
    }

    function sendMail($to = '', $cc = '', $subject = '', $message = '') {
        $clean = $this->security->xss_clean($to);
        $this->email->to($clean);
        $this->email->from(APP_FROM_EMAIL, APP_NAME);
        $this->email->subject($subject);
        $this->email->message($message);

        if ($this->email->send()) {
            return true;
        } else {
            return false;
        }
    }

    function getRole($user_id) {
        $role_str = array(
            array(
                'table' => 'user_role',
                'join_table_id' => 'user_role.role_id',
                'from_table_id' => 'user.role_id',
                'join_type' => 'left'
            )
        );

        $user_info = $this->select_data_by_id('user', 'user_id', $user_id, $data = 'role_name', $role_str);
        return $user_info[0]['role_name'];
    }

    function getLangFromTranslation($trans_id) {
        $this->db->select('l1.lang_name as from_lang, l2.lang_name as to_lang');
        $this->db->from('translation');

        $this->db->join('language l1', 'l1.lang_id = translation.trans_from');
        $this->db->join('language l2', 'l2.lang_id = translation.trans_to');

        $this->db->where('trans_id', $trans_id);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    function pushNotification($from_id, $to_id, $job_id = '', $notify_id = '', $trans_id = '', $flag) {

        $push_info = array();

        $from_user = $this->select_data_by_id('user', 'user_id', $from_id, $data = 'name', $join_str = array());
        $to_user = $this->select_data_by_id('user', 'user_id', $to_id, $data = 'device_type, token_key', $join_str = array());
        $lang_info = $this->getLangFromTranslation($trans_id);

        $push_info['device_type'] = $to_user[0]['device_type'];
        $push_info['token_key'] = array($to_user[0]['token_key']);

        // Get To User Language
        $to_lang = $this->select_data_by_id('preference', 'user_id', $to_id, $data = 'lang_code', $join_str = array());

        if (!empty($to_lang[0]['lang_code'])) {
            if ($to_lang[0]['lang_code'] == 'en') {
                $this->lang->load('message', 'english');
            } else {
                $this->lang->load('message', 'finnish');
            }
        } else {
            $this->lang->load('message', 'english');
        }

        // Offer Job
        if ($flag == "offer") {
            $push_info['msg_payload'] = array(
                'mtitle' => 'SomeLounge',
                'mdesc' => sprintf($this->lang->line('push_job_offered'), $from_user[0]['name'], $lang_info[0]['from_lang'], $lang_info[0]['to_lang']),
                'job_id' => $job_id,
                'user_id' => $from_id,
                'notify_id' => $notify_id,
                'type' => 'job_offered'
            );
        }

        // Accept Job
        if ($flag == "accept") {
            $push_info['msg_payload'] = array(
                'mtitle' => 'SomeLounge',
                'mdesc' => sprintf($this->lang->line('push_job_accepted'), $from_user[0]['name']),
                'job_id' => $job_id,
                'user_id' => $from_id,
                'notify_id' => $notify_id,
                'type' => 'job_accepted'
            );
        }

        // Cancel Job
        if ($flag == "cancel") {
            $push_info['msg_payload'] = array(
                'mtitle' => 'SomeLounge',
                'mdesc' => sprintf($this->lang->line('push_job_canceled'), $from_user[0]['name'], $lang_info[0]['from_lang'], $lang_info[0]['to_lang']),
                'job_id' => $job_id,
                'user_id' => $from_id,
                'notify_id' => $notify_id,
                'type' => 'job_canceled'
            );
        }

        // Complete Job
        if ($flag == "complete") {
            $push_info['msg_payload'] = array(
                'mtitle' => 'SomeLounge',
                'mdesc' => sprintf($this->lang->line('push_job_completed'), $from_user[0]['name'], $lang_info[0]['from_lang'], $lang_info[0]['to_lang']),
                'job_id' => $job_id,
                'user_id' => $from_id,
                'notify_id' => $notify_id,
                'type' => 'job_completed'
            );
        }

        // Approve Job
        if ($flag == "approve") {
            $push_info['msg_payload'] = array(
                'mtitle' => 'SomeLounge',
                'mdesc' => sprintf($this->lang->line('push_job_approved'), $from_user[0]['name'], $lang_info[0]['from_lang'], $lang_info[0]['to_lang']),
                'job_id' => $job_id,
                'user_id' => $from_id,
                'notify_id' => $notify_id,
                'type' => 'job_approved'
            );
        }

        // Disapprove Job
        if ($flag == "disapprove") {
            $push_info['msg_payload'] = array(
                'mtitle' => 'SomeLounge',
                'mdesc' => sprintf($this->lang->line('push_job_disapproved'), $lang_info[0]['from_lang'], $lang_info[0]['to_lang'], $from_user[0]['name']),
                'job_id' => $job_id,
                'user_id' => $from_id,
                'notify_id' => $notify_id,
                'type' => 'job_disapproved'
            );
        }

        // Rate Job
        if ($flag == "rate") {
            $push_info['msg_payload'] = array(
                'mtitle' => 'SomeLounge',
                'mdesc' => sprintf($this->lang->line('push_job_rated'), $from_user[0]['name'], $lang_info[0]['from_lang'], $lang_info[0]['to_lang']),
                'job_id' => $job_id,
                'user_id' => $from_id,
                'notify_id' => $notify_id,
                'type' => 'job_rated'
            );
        }

        // Log Request
        $log_data = array(
            'log_url' => current_url(),
            'log_request' => json_encode($push_info['msg_payload'])
        );

        $log_id = $this->common->insert_data_get_id($log_data, 'api_log');

        return $push_info;
    }

    function getDistanceBetweenPointsNew($latitude1, $longitude1, $latitude2, $longitude2, $unit = 'Km') {
        $theta = $longitude1 - $longitude2;
        $distance = (sin(deg2rad($latitude1)) *
                sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1)) *
                cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
        $distance = acos($distance);
        $distance = rad2deg($distance);
        $distance = $distance * 60 * 1.1515;
        switch ($unit) {
            case 'Mi': break;
            case 'Km' : $distance = $distance * 1.609344;
        }
        return (round($distance, 2));
    }

    function getnearByUserOld($user_id, $latitude, $longitude, $distance, $offset = '', $limit = '') {

        $this->db->select("preferences");
        $this->db->from("user_set_preferences");
        $this->db->where(array('user_id' => $user_id));
        $query1 = $this->db->get();
        $result1 = $query1->row_array();
        $user_preferences = $result1['preferences'];
        //$user_preferences = '';
        if ($user_preferences == '') {
            $this->db->select("ul.*,u.name,u.photo");
            $this->db->from("user_location ul");
            $this->db->join("users u", "u.id=ul.user_id", "Left");
            $this->db->where("(((acos(sin((" . $latitude . "*pi()/180)) * 
sin((`latitude`*pi()/180))+cos((" . $latitude . "*pi()/180)) * 
cos((`latitude`*pi()/180)) * cos(((" . $longitude . "- `longitude`)
*pi()/180))))*180/pi())*60*1.1515*1.609344) <= " . $distance . " ");
            //$this->db->where(array('events.status !=' => 3, 'events.status' => 1));
            $this->db->where(array('ul.user_id !=' => $user_id));
            if ($limit != '' && $offset != '') {
                $this->db->limit($limit, $offset);
            }
            $query = $this->db->get();
            $result = $query->result_array();
            return $result;
        } else {
            $prefered_user = '';
            $user_preferences = explode(',', $user_preferences);
            foreach ($user_preferences as $key => $value) {
                $this->db->select('GROUP_CONCAT(user_id) as user_ids');
                $this->db->from('user_preferences');
                $this->db->where('find_in_set("' . $value . '", preferences) <> 0');
                $query2 = $this->db->get();
                $prefer_user = $query2->result_array();
                $prefered_user .= $prefer_user[0]['user_ids'] . ',';
            }
            $prefered_user = trim($prefered_user, ',');
            $prefered_user_array = explode(',', $prefered_user);
            $prefered_user_array = array_unique($prefered_user_array);
            $prefered_user = implode(',', $prefered_user_array);
            $prefered_user = str_replace(",", "','", $prefered_user);

            $this->db->select("ul.*,u.name,u.photo");
            $this->db->from("user_location ul");
            $this->db->join("users u", "u.id=ul.user_id", "Left");
            $this->db->join("user_set_preferences usp", "u.id=usp.user_id", "Left");
            $this->db->where("(((acos(sin((" . $latitude . "*pi()/180)) * 
sin((`latitude`*pi()/180))+cos((" . $latitude . "*pi()/180)) * 
cos((`latitude`*pi()/180)) * cos(((" . $longitude . "- `longitude`)
*pi()/180))))*180/pi())*60*1.1515*1.609344) <= " . $distance . " ");
            $this->db->where("ul.user_id IN('$prefered_user')");
            $this->db->where(array('ul.user_id !=' => $user_id));
            if ($limit != '' && $offset != '') {
                $this->db->limit($limit, $offset);
            }
            $query = $this->db->get();
            $result = $query->result_array();
            return $result;
        }
    }

    function getnearByUser($user_id, $latitude, $longitude, $distance, $offset = '', $limit = '') {
//        $this->db->select("setting_val");
//        $this->db->from("settings");
//        $this->db->where(array('setting_id' => 7));
//        $query = $this->db->get();
//        $result = $query->row_array();
//        $set_preferences_in_miles = $result['setting_val'];

        $this->db->select("preferences");
        $this->db->from("user_preferences");
        $this->db->where(array('user_id' => $user_id));
        $query1 = $this->db->get();
        $result1 = $query1->row_array();
        $user_preferences = $result1['preferences'];
        
        //To search by kilometers instead of miles, replace 3959 with 6371.
        //$user_preferences = '';
        if ($user_preferences == '') {
            $this->db->select("ul.*,u.name,u.photo,l.name as location_name");
            $this->db->select("(3959 * acos (cos ( radians('$latitude') )
                * cos( radians( latitude ) )
                * cos( radians( longitude ) - radians('$longitude') )
                + sin ( radians('$latitude') )
                * sin( radians( latitude ) )
              )
            ) AS distance");
            $this->db->from("user_location ul");
            $this->db->join("users u", "u.id=ul.user_id", "Left");
            $this->db->join("location l", "l.id=u.location", "Left");
            $this->db->where(array('u.status' => 1, 'u.is_deleted' => 0));
            $this->db->where(array('ul.user_id !=' => $user_id));
            $this->db->having('distance <= ' . $distance);
            $this->db->order_by('distance');
            if ($limit != '' && $offset != '') {
                $this->db->limit($limit, $offset);
            }
            $query = $this->db->get();
            $result = $query->result_array();
            return $result;
        } else {
            $prefered_user = '';
            $user_preferences = explode(',', $user_preferences);
            foreach ($user_preferences as $key => $value) {
                $this->db->select('GROUP_CONCAT(user_id) as user_ids');
                $this->db->from('user_preferences');
                $this->db->where('find_in_set("' . $value . '", preferences) <> 0');
                $query2 = $this->db->get();
                $prefer_user = $query2->result_array();
                $prefered_user .= $prefer_user[0]['user_ids'] . ',';
            }
            $prefered_user = trim($prefered_user, ',');
            $prefered_user_array = explode(',', $prefered_user);
            $prefered_user_array = array_unique($prefered_user_array);
            $prefered_user = implode(',', $prefered_user_array);
            $prefered_user = str_replace(",", "','", $prefered_user);

            $this->db->select("ul.*,u.name,u.photo,l.name as location_name");
            $this->db->select("(3959 * acos (cos ( radians('$latitude') )
                * cos( radians( latitude ) )
                * cos( radians( longitude ) - radians('$longitude') )
                + sin ( radians('$latitude') )
                * sin( radians( latitude ) )
              )
            ) AS distance");
            $this->db->from("user_location ul");
            $this->db->join("users u", "u.id=ul.user_id", "Left");
            $this->db->join("location l", "l.id=u.location", "Left");
            $this->db->join("user_preferences usp", "u.id=usp.user_id", "Left");
            $this->db->where("ul.user_id IN('$prefered_user')");
            $this->db->where(array('u.status' => 1, 'u.is_deleted' => 0));
            $this->db->where(array('ul.user_id !=' => $user_id));
            $this->db->having('distance <= ' . $distance);
            $this->db->order_by('distance');
            if ($limit != '' && $offset != '') {
                $this->db->limit($limit, $offset);
            }
            $query = $this->db->get();
            $result = $query->result_array();
            return $result;
        }
    }

    function getnearByUserNew($user_id, $latitude, $longitude, $distance, $offset = '', $limit = '') {
        $this->db->select("preferences");
        $this->db->from("user_preferences");
        $this->db->where(array('user_id' => $user_id));
        $query1 = $this->db->get();
        $result1 = $query1->row_array();
        $user_preferences = $result1['preferences'];

        //$user_preferences = '';
        if ($user_preferences == '') {
            $this->db->select("ul.*,u.name,u.photo,l.name as location_name");
            $this->db->from("user_location ul");
            $this->db->join("users u", "u.id=ul.user_id", "Left");
            $this->db->join("location l", "l.id=u.location", "Left");
            $this->db->where("(((acos(sin((" . $latitude . "*pi()/180)) * 
sin((`latitude`*pi()/180))+cos((" . $latitude . "*pi()/180)) * 
cos((`latitude`*pi()/180)) * cos(((" . $longitude . "- `longitude`)
*pi()/180))))*180/pi())*60*1.1515*1.609344) <= " . $distance . " ");
            $this->db->where(array('u.status' => 1, 'u.is_deleted' => 0));
            $this->db->where(array('ul.user_id !=' => $user_id));
            if ($limit != '' && $offset != '') {
                $this->db->limit($limit, $offset);
            }
            $query = $this->db->get();
            $result = $query->result_array();
            return $result;
        } else {
            $prefered_user = '';
            $user_preferences = explode(',', $user_preferences);
            foreach ($user_preferences as $key => $value) {
                $this->db->select('GROUP_CONCAT(user_id) as user_ids');
                $this->db->from('user_preferences');
                $this->db->where('find_in_set("' . $value . '", preferences) <> 0');
                $query2 = $this->db->get();
                $prefer_user = $query2->result_array();
                $prefered_user .= $prefer_user[0]['user_ids'] . ',';
            }
            $prefered_user = trim($prefered_user, ',');
            $prefered_user_array = explode(',', $prefered_user);
            $prefered_user_array = array_unique($prefered_user_array);
            $prefered_user = implode(',', $prefered_user_array);
            $prefered_user = str_replace(",", "','", $prefered_user);

            $this->db->select("ul.*,u.name,u.photo,l.name as location_name");
            $this->db->from("user_location ul");
            $this->db->join("users u", "u.id=ul.user_id", "Left");
            $this->db->join("location l", "l.id=u.location", "Left");
            $this->db->join("user_preferences usp", "u.id=usp.user_id", "Left");
            $this->db->where("(((acos(sin((" . $latitude . "*pi()/180)) * 
sin((`latitude`*pi()/180))+cos((" . $latitude . "*pi()/180)) * 
cos((`latitude`*pi()/180)) * cos(((" . $longitude . "- `longitude`)
*pi()/180))))*180/pi())*60*1.1515*1.609344) <= " . $distance . " ");
            $this->db->where("ul.user_id IN('$prefered_user')");
            $this->db->where(array('u.status' => 1, 'u.is_deleted' => 0));
            $this->db->where(array('ul.user_id !=' => $user_id));
            if ($limit != '' && $offset != '') {
                $this->db->limit($limit, $offset);
            }
            $query = $this->db->get();
            $result = $query->result_array();
            return $result;
        }
    }

    function getPostSendedUser($user_id, $latitude, $longitude, $distance, $offset = '', $limit = '') {
        $this->db->select("u.id");
        $this->db->from("user_location ul");
        $this->db->join("users u", "u.id=ul.user_id", "Left");
        $this->db->where("(((acos(sin((" . $latitude . "*pi()/180)) * 
sin((`latitude`*pi()/180))+cos((" . $latitude . "*pi()/180)) * 
cos((`latitude`*pi()/180)) * cos(((" . $longitude . "- `longitude`)
*pi()/180))))*180/pi())*60*1.1515*1.609344) <= " . $distance . " ");
        //$this->db->where(array('events.status !=' => 3, 'events.status' => 1));
        $this->db->where(array('u.status' => 1, 'u.is_deleted' => 0));
        $this->db->where(array('ul.user_id !=' => $user_id));
        if ($limit != '' && $offset != '') {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

}
