<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Common_model class.
 *
 * @extends CI_Model
 */
class Common_model extends CI_Model {

	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {

		parent::__construct();
		$this->load->database();

	}

    // insert database
    function insert_data($data, $tablename) {
        if ($this->db->insert($tablename, $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    // insert database
    function insert_data_get_id($data, $tablename) {
        if ($this->db->insert($tablename, $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    // update database
    function update_data($data, $tablename, $columnname, $columnid) {
        $this->db->where($columnname, $columnid);
        if ($this->db->update($tablename, $data)) {
            return true;
        } else {
            return false;
        }
    }

    // select data using colum id
    function select_data_by_id($tablename, $columnname, $columnid, $data = '*', $join_str = array()) {
        $this->db->select($data);
        if (!empty($join_str)) {
            foreach ($join_str as $join) {
                if ($join['join_type'] == '') {
                    $this->db->join($join['table'], $join['join_table_id'] . '=' . $join['from_table_id']);
                } else {
                    $this->db->join($join['table'], $join['join_table_id'] . '=' . $join['from_table_id'], $join['join_type']);
                }
            }
        }
        $this->db->where($columnname, $columnid);
        $query = $this->db->get($tablename);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    // select data using multiple conditions
    function select_data_by_condition($tablename, $contition_array = array(), $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $group_by='') {
        $this->db->select($data);

        if (!empty($join_str)) {
            foreach ($join_str as $join) {
                if ($join['join_type'] == '') {
                    $this->db->join($join['table'], $join['join_table_id'] . '=' . $join['from_table_id']);
                } else {
                    $this->db->join($join['table'], $join['join_table_id'] . '=' . $join['from_table_id'], $join['join_type']);
                }
            }
        }

        $this->db->where($contition_array);


        //Setting Limit for Paging
        if ($limit != '' && $offset == 0) {
            $this->db->limit($limit);
        } else if ($limit != '' && $offset != 0) {
            $this->db->limit($limit, $offset);
        }
        //order by query
        if ($sortby != '' && $orderby != '') {
            $this->db->order_by($sortby, $orderby);
        }
        if ($group_by != '') {
            $this->db->group_by($group_by);
        }

        $query = $this->db->get($tablename);

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    // select data using multiple conditions and search keyword
    function select_data_by_search($tablename, $search_condition, $contition_array = array(), $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = '', $custom_order_by = '', $group_by='') {
        $this->db->select($data);
        if (!empty($join_str)) {
            foreach ($join_str as $join) {
                $this->db->join($join['table'], $join['join_table_id'] . '=' . $join['from_table_id']);
            }
        }
        $this->db->where($contition_array);
        $this->db->where($search_condition);

        //Setting Limit for Paging
        if ($limit != '' && $offset == 0) {
            $this->db->limit($limit);
        } else if ($limit != '' && $offset != 0) {
            $this->db->limit($limit, $offset);
        }
        //order by query
        if ($sortby != '' && $orderby != '') {
            $this->db->order_by($sortby, $orderby);
        }

        if($custom_order_by != ''){
            //echo 'Jam';die;
            $this->db->order_by($custom_order_by);
        }
		if ($group_by != '') {
            $this->db->group_by($group_by);
        }

        $query = $this->db->get($tablename);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    // delete data
    function delete_data($tablename, $columnname, $columnid) {
        $this->db->where($columnname, $columnid);
        if ($this->db->delete($tablename)) {
            return true;
        } else {
            return false;
        }
    }

    // check unique avaliblity
    function check_unique_avalibility($tablename, $columname1, $columnid1_value, $columname2, $columnid2_value, $condition_array = array()) {
        // if edit than $columnid2_value use

        if ($columnid2_value != '') {
            $this->db->where($columname2 . " !=", $columnid2_value);
        }

        if (!empty($condition_array)) {
            $this->db->where($condition_array);
        }

        $this->db->where($columname1, $columnid1_value);
        $query = $this->db->get($tablename);
        if ($query->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }

    //get all record
    function get_all_record($tablename, $data = '*', $sortby = '', $orderby = '') {
        $this->db->select($data);
        $this->db->from($tablename);
        $this->db->where('status', 'Enable');
        if ($sortby != '' && $orderby != "") {
            $this->db->order_by($sortby, $orderby);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    //table records count
    function get_count_of_table($table, $where = array()) {
        if (count($where) > 0) {
            $this->db->where($where);
        }

        $query = $this->db->count_all_results($table);

        return $query;
    }

    //table Next Auto Increment ID
    function get_autoincrement_id($table) {
        $this->db->select_max('class_id');
        $Q = $this->db->get($table);
        $row = $Q->row_array();
        return $row['class_id'];
    }

	// check email id
    function chkemail($id, $email) {
        if ($id != 0) {
            $option = array('userid !=' => $id, 'useremail' => $email);
        } else {
            $option = array('useremail' => $email);
        }
        $query = $this->db->get_where('users', $option);
        if ($query->num_rows() > 0) {
            return 'old';
        } else {
            return 'new';
        }
    }

	//This function get all records from table by name
    function getallrecordbytablename($tablename, $data, $conditionarray = '', $limit = '', $offset = '', $sortby = '', $orderby = '') {

		//$this->db->order_by($sortby, $orderby);
		//Setting Limit for Paging
        if ($limit != '' && $offset == 0) {
            $this->db->limit($limit);
        } else if ($limit != '' && $offset != 0) {
            $this->db->limit($limit, $offset);
        }

		//Executing Query
        $this->db->select($data);
        $this->db->from($tablename);
        if ($conditionarray != '') {
            $this->db->where($conditionarray);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

	//This function get all open record count
    function get_open_request_count($condition) {
        $this->db->select('COUNT(requestid) as count,maincategoryuniqid');
        $this->db->where($condition);
        $this->db->from('request');
        $this->db->group_by('maincategoryuniqid');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

	// select data using colum id
    function select_database_id($tablename, $columnname, $columnid, $data = '*', $condition_array = array()) {
        $this->db->select($data);
        $this->db->where($columnname, $columnid);
        if (!empty($condition_array)) {
            $this->db->where($condition_array);
        }
        $query = $this->db->get($tablename);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    // change status
    function change_status($data, $tablename, $columnname, $columnid) {
        $this->db->where($columnname, $columnid);
        if ($this->db->update($tablename, $data)) {
            return true;
        } else {
            return false;
        }
    }

    function get_name_by_id($tablename, $columnname, $condition) {
        $this->db->select($columnname);
        $this->db->where($condition);
        $this->db->from($tablename);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return array();
        }
    }

	// Random password
    function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 6; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    function getSettings($key = '') {
        $this->db->select('*');

        if($key != '') {
            $this->db->where("setting_key", $key);
        }

        $this->db->from("settings");

        $query = $this->db->get();
        //echo $this->db->last_query();die();

        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            return $result;
        } else {
            return array();
        }
    }

    function timeAgo($time_ago) {
        $time_ago = strtotime($time_ago);
        $cur_time   = time();
        $time_elapsed   = $cur_time - $time_ago;
        $seconds    = $time_elapsed ;
        $minutes    = round($time_elapsed / 60 );
        $hours      = round($time_elapsed / 3600);
        $days       = round($time_elapsed / 86400 );
        $weeks      = round($time_elapsed / 604800);
        $months     = round($time_elapsed / 2600640 );
        $years      = round($time_elapsed / 31207680 );
        // Seconds
        if($seconds <= 60){
            return "just now";
        }
        //Minutes
        else if($minutes <=60){
            if($minutes==1){
                return "1 min";
            }
            else{
                return "$minutes mins";
            }
        }
        //Hours
        else if($hours <=24){
            if($hours==1){
                return "an hour";
            }else{
                return "$hours hrs";
            }
        }
        //Days
        else if($days <= 7){
            if($days==1){
                return "yesterday";
            }else{
                return "$days days";
            }
        }
        //Weeks
        else if($weeks <= 4.3){
            if($weeks==1){
                return "a week";
            }else{
                return "$weeks weeks";
            }
        }
        //Months
        else if($months <=12){
            if($months==1){
                return "a month";
            }else{
                return "$months months";
            }
        }
        //Years
        else{
            if($years==1){
                return "one year";
            }else{
                return "$years years";
            }
        }
    }

    function sendMail ($to = '', $cc = '', $subject = '', $mail_body = '') {
        $this->load->library('email');

        if($subject == '') {
            $subject = 'SomeLounge - New Notification';
        }

        $result = $this->email
                ->from('youremail@somedomain.com', 'SomeLounge')
                // ->reply_to('yoursecondemail@somedomain.com')    // Optional, an account where a human being reads.
                ->to($to)
                ->subject($subject)
                ->message($mail_body)
                ->send();

        if($result) {
            return true;
        } else {
            return false;
        }
    }

		function getRole ($user_id) {
			$role_str = array(
				array(
					'table' => 'user_role',
					'join_table_id' => 'user_role.role_id',
					'from_table_id' => 'user.role_id',
					'join_type' => 'left'
				)
			);

			$user_info = $this->select_data_by_id('user', 'user_id', $user_id, $data = 'role_name', $role_str);
			return $user_info[0]['role_name'];
		}

		function ajaxHistory ($id, $start = '', $end = '') {
				$result = array();

				$join_str = array(
					array(
						'table' => 'user u1',
						'join_table_id' => 'u1.user_id',
						'from_table_id' => 'job.client_id',
						'join_type' => 'left'
					),
					array(
						'table' => 'user u2',
						'join_table_id' => 'u2.user_id',
						'from_table_id' => 'job.emp_id',
						'join_type' => 'left'
					),
					array(
						'table' => 'translation',
						'join_table_id' => 'translation.trans_id',
						'from_table_id' => 'job.trans_id',
						'join_type' => 'left'
					),
					array(
						'table' => 'language l1',
						'join_table_id' => 'l1.lang_id',
						'from_table_id' => 'translation.trans_from',
						'join_type' => 'left'
					),
					array(
						'table' => 'language l2',
						'join_table_id' => 'l2.lang_id',
						'from_table_id' => 'translation.trans_to',
						'join_type' => 'left'
					)
				);

				if ($this->getRole($id) == "client") {
						$search_condition = 'job.client_id = '.$id.'';
				} else {
						$search_condition = 'job.emp_id = '.$id.'';
				}

				$search_condition .= ' AND job.deleted = 0 AND job.datetime BETWEEN "'.$start.'" AND "'.$end.'"';
				$work_history = $this->select_data_by_search('job', $search_condition, $contition_array = array(), $data = 'job.job_id, u1.name as client_name, u2.name as emp_name, l1.lang_name as frm_lang, l2.lang_name as to_lang, job_desc, job.datetime, job.job_status, hours, minutes', $sortby = 'job.job_id', $orderby = 'DESC', $limit = '', $offset = '', $join_str, $custom_order_by = '', $group_by='');

				foreach ($work_history as $row) {
						$i[0] = $row['job_id'];

						if ($this->getRole($id) == "client") {
								$i[1] = (empty($row['emp_name'])) ? "N/A" : $row['emp_name'];
						} else {
								$i[1] = (empty($row['client_name'])) ? "N/A" : $row['client_name'];
						}

						$i[2] = $row['frm_lang'] . ' to ' . $row['to_lang'];
						$i[3] = $row['job_desc'];

						switch ($row['job_status']) {
							case 1:
								$i[4] = '<a href="javascript:void(0)" id="edit_btn">Accepted</a>';
								break;
							case 2:
								$i[4] = '<a href="javascript:void(0)" id="edit_btn">Canceled</a>';
								break;
							case 3:
								$i[4] = '<a href="javascript:void(0)" id="edit_btn">Completed</a>';
								break;
							case 4:
								$i[4] = '<a href="javascript:void(0)" id="edit_btn">Approved</a>';
								break;
							case 5:
								$i[4] = '<a href="javascript:void(0)" id="edit_btn">Closed</a>';
								break;
							default:
								$i[4] = '<a href="javascript:void(0)" id="edit_btn">Posted</a>';
						}

						$hours = (empty($row['hours'])) ? "" : " ".$row['hours']." hours";
						$minutes = (empty($row['minutes'])) ? "" : " ".$row['minutes']." minutes";

						$i[5] = $hours.$minutes;
						$i[6] = $row['datetime'];
						$i[7] = '<a href="'.base_url('admin/job/view/' . $row['job_id']).'" id="edit_btn" title="Ratings & Feedback"><button type="button" class="btn btn-primary" style="margin-top: 3px;"><i class="icon-pencil"></i> <i class="fa fa-comments-o"></i></button></a>';

						array_push($result, $i);
				}

				return $result;
		}

		function ajaxTime ($id, $start = '', $end = '') {
				// get total time
				if ($this->getRole($id) == "client") {
						$search_condition = 'job.client_id = '.$id.'';
				} else {
						$search_condition = 'job.emp_id = '.$id.'';
				}
				
				$search_condition .= ' AND job.datetime BETWEEN "'.$start.'" AND "'.$end.'"';
				$work_hours = $this->select_data_by_search('job', $search_condition, $contition_array = array(), 'SUM(hours) as hours, SUM(minutes) as minutes');

				$total_time = '';
				$total_hours = 0;

				if (!empty($work_hours[0]['hours'])) {
						$total_hours = $work_hours[0]['hours'];
				}

				if (!empty($work_hours[0]['minutes'])) {
						$total_hours = $total_hours + floor($work_hours[0]['minutes'] / 60);
						$total_minutes = $work_hours[0]['minutes'] - ((floor($work_hours[0]['minutes'] / 60)) * 60);
				}

				$total_time .= (empty($total_hours)) ? "" : " ".$total_hours." hours";
				$total_time .= (empty($total_minutes)) ? "" : " ".$total_minutes." minutes";

				return $total_time;
		}

}
