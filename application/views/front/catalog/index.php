<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <title><?php echo (isset($categoryData['name'])) ? $categoryData['name'] : 'Catalog' . ' | ' . $settingData['site_title']; ?></title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo $settingData['site_logo'] ?>">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/front/style.css">
        <script src="<?php echo base_url() ?>assets/front/js/vendor/modernizr-2.8.3.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="<?php echo base_url(); ?>assets/front/css/jquery-ui.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/front/css/bootstrap_sweetalert.css">
    </head>
    <body>
        <div id="loading-wrap">
            <div class="loading-effect"></div>
        </div>
        <div class="wrapper">
            <?php echo $header; ?>
            <?php echo $breadcum; ?>
            <section class="courses-area section-padding white-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 pb-80">
                            <div class="courses-bar">
                                <div class="section-title pull-left">
                                    <h2 class="mb-5"><?php echo (isset($categoryData['name'])) ? $categoryData['name'] : 'Catalog'; ?></h2>
                                    <hr class="line">
                                </div>
                                <div class="sort-courses pull-right mlr-40">
                                    <div class="pull-left pt-10">
                                        <h4>Sort By: </h4>
                                    </div>
                                    <div class="pull-right">
                                        <select name="sortby" id="sortby" onchange="sortBy(this.value);">
                                            <option value="" <?php
                                            if ($sort == '') {
                                                echo 'selected="selected"';
                                            }
                                            ?>>Default
                                            </option>
                                            <option value="PriceLowtoHigh" <?php
                                            if ($sort == 'PriceLowtoHigh') {
                                                echo 'selected="selected"';
                                            }
                                            ?>>Price Low to High
                                            </option>
                                            <option value="PriceHightoLow" <?php
                                            if ($sort == 'PriceHightoLow') {
                                                echo 'selected="selected"';
                                            }
                                            ?>>Price High to Low
                                            </option>
                                            <option value="NewesttoOldest" <?php
                                            if ($sort == 'NewesttoOldest') {
                                                echo 'selected="selected"';
                                            }
                                            ?>>Newest to Oldest
                                            </option>
                                            <option value="OldesttoNewest" <?php
                                            if ($sort == 'OldesttoNewest') {
                                                echo 'selected="selected"';
                                            }
                                            ?>>Oldest to Newest
                                            </option>
                                            <option value="BestSeller" <?php
                                            if ($sort == 'BestSeller') {
                                                echo 'selected="selected"';
                                            }
                                            ?>>Best Seller
                                            </option>
                                            <option value="ItemOfTheMonth" <?php
                                            if ($sort == 'ItemOfTheMonth') {
                                                echo 'selected="selected"';
                                            }
                                            ?>>Item of the Month
                                            </option>
                                        </select>
                                        <span><i class="zmdi zmdi-chevron-down"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-2">
                            <div class="widget-title">
                                <div class="list-group">
                                    <h5>Price</h5>
                                    <div class="horizontal-line">
                                        <hr class="top">
                                        <hr class="bottom">
                                    </div>
                                    <input type="hidden" id="hidden_minimum_price"
                                           value="<?php echo $filterData['pricerange'][0] ?>"/>
                                    <input type="hidden" id="hidden_maximum_price"
                                           value="<?php echo $filterData['pricerange'][1] ?>"/>
                                    <p id="price_show">$ <?php echo $filterData['pricerange'][0] ?> -
                                        $ <?php echo $filterData['pricerange'][1] ?></p>
                                    <div id="price_range"></div>
                                </div>
                                <div class="list-group">
                                    <h5>Category</h5>
                                    <div class="horizontal-line">
                                        <hr class="top">
                                        <hr class="bottom">
                                    </div>
                                    <?php foreach ($filter_category as $category) { ?>
                                        <div class="list-group-item checkbox">
                                            <label>
                                                <input type="checkbox" class="common_selector category"
                                                       value="<?php echo $category['id']; ?>" <?php
                                                       if (in_array($category['id'], $filterData['category'])) {
                                                           echo 'checked="checked"';
                                                       }
                                                       ?>>
                                                       <?php echo $category['name']; ?>
                                            </label>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="list-group">
                                    <h5>Author</h5>
                                    <div class="horizontal-line">
                                        <hr class="top">
                                        <hr class="bottom">
                                    </div>
                                    <div class="mt-10">
                                        <input type="text" id="myInput" onkeyup="searchAuthor()" placeholder="Search for Author..">  
                                    </div>
                                    <div id="myUL">        
                                        <?php
                                        foreach ($filter_author as $author) {
                                            ?>
                                            <div class="list-group-item checkbox">
                                                <label><input type="checkbox" class="common_selector author" data-name="<?php echo $author['author_name']; ?>" 
                                                              value="<?php echo base64_encode($author['author_name']); ?>" <?php
                                                              if (in_array(base64_encode($author['author_name']), $filterData['author'])) {
                                                                  echo 'checked="checked"';
                                                              }
                                                              ?>>
                                                    <?php echo $author['author_name']; ?></label>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <button class="btn" id="clearall">CLear All</button>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-8 mobi-mb-50">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="tab-content">

                                        <div class="tab-pane fade in active" id="grid-view">
                                            <div class="row">
                                                <?php
                                                if (!empty($productData)) {
                                                    $i = 1;
                                                    foreach ($productData as $product) {
                                                        $bookUrl = base_url() . 'book/' . $product['slug'];
                                                        $imageUrl = '';
                                                        if (file_exists($_SERVER['DOCUMENT_ROOT'] . $product['image']) && is_file($_SERVER['DOCUMENT_ROOT'] . $product['image'])) {
                                                            $imageUrl = $product['image'];
                                                        } else {
                                                            $imageUrl = '/uploads/no-image.png';
                                                        }
                                                        ?>
                                                        <div class="col-xs-12 col-sm-6 col-md-4 mb-50">
                                                            <div class="course-box">
                                                                <div class="course-content plr-25 clearfix">
                                                                    <div class="thumb text-center">
                                                                        <a href="<?php echo $bookUrl; ?>"
                                                                           title="<?php echo strip_tags($product['name']); ?>"><img
                                                                                src="<?php echo base_url($imageUrl); ?>"
                                                                                alt="<?php echo strip_tags($product['name']); ?>"
                                                                                width="100%" height="170px;"/></a>
                                                                        <div class="price">
                                                                            <h3 class="no-margin">
                                                                                <span>$</span><?php echo $product['price'] ?>
                                                                            </h3>
                                                                        </div>
                                                                    </div>
                                                                    <a href="<?php echo $bookUrl; ?>" title="<?php echo strip_tags($product['name']); ?>">
                                                                        <h3 class="text-capitalize"><?php echo (strlen(strip_tags($product['name'])) > 15) ? substr(strip_tags($product['name']), 0, 15) . '...' : $product['name']; ?></h3>
                                                                    </a>
                                                                    <div class="date-comment clearfix">
                                                                        <div class="pull-left">
                                                                            <h6><span>SKU: </span><?php echo $product['sku'] ?>
                                                                            </h6>
                                                                            <h6>
                                                                                <span>ISBN No: </span><?php echo $product['isbn_no'] ?>
                                                                            </h6>
                                                                            <h6><?php echo (!empty($product['collection'])) ? $product['collection'] : '&nbsp;' ?></h6>
                                                                        </div>
                                                                    </div>
                                                                    <div class="add-to-cart">
                                                                        <button class="btn add-to-cart" type="button"
                                                                                onclick="addtocart('<?php echo $product['id'] ?>', '<?php echo $product['attribute_slug'] ?>')">
                                                                            Add to Cart
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                                <div class="footer clearfix plr-10">
                                                                    <div class="pull-left">
                                                                        <h5 class="no-margin">By
                                                                            : <?php echo $product['author_name'] ?></h5>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php if ($i % 3 == 0) { ?>
                                                            <div class="clearfix"></div>
                                                        <?php } ?>
                                                        <?php
                                                        $i++;
                                                    }
                                                } else {
                                                    ?>
                                                    <div class="col-xs-12 mb-30 text-center">
                                                        <h4>Oops No Product Found.</h4>
                                                        <p>We couldn't find what you were looking for.</p>
                                                        <p>Make sure you used the right keywords.</p>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php /* ?>
                                          <div class="tab-pane fade in active" id="list-view">
                                          <div class="row">
                                          <?php
                                          if(!empty($productData)){
                                          foreach($productData as $product){
                                          $bookUrl = base_url() .'book/'.$product['slug'];
                                          ?>
                                          <div class="col-xs-12 mb-30">
                                          <div class="course-box list-view clearfix">
                                          <div class="thumb text-center pull-left">
                                          <a href="<?php echo $bookUrl; ?>">
                                          <img src="<?php echo $product['image'] ?>" alt="<?php echo $product['name'] ?>" width="200px" />
                                          </a>
                                          <div class="price">
                                          <h3 class="no-margin"><span>$</span><?php echo $product['price'] ?></h3>
                                          </div>
                                          </div>
                                          <div class="course-content">
                                          <a href="<?php echo $bookUrl; ?>">
                                          <h3 class="text-capitalize"><?php echo $product['name'] ?></h3>
                                          </a>
                                          <div class="date-comment clearfix">
                                          <div class="pull-left">
                                          <h6><span>ISBN NO: </span><?php echo $product['isbn_no'] ?></h6>
                                          </div>
                                          <div class="pull-right">
                                          <h6><?php echo $product['collection'] ?></h6>
                                          </div>
                                          </div>
                                          <?php
                                          $product['description'] = substr(trim(preg_replace('/\s+/',' ', $product['description'])),0,100);
                                          ?>
                                          <p class="mb-20">
                                          <?php echo (!empty($product['description'])) ? $product['description'] .'...' : ''  ?>
                                          </p>
                                          <div class="footer clearfix plr-25">
                                          <div class="pull-left">
                                          <h5 class="no-margin">By : <?php echo $product['author_name'] ?></h5>
                                          </div>
                                          <div class="add-to-cart">
                                          <button class="btn" type="submit">Add to Cart</button>
                                          </div>
                                          </div>
                                          </div>
                                          </div>
                                          </div>
                                          <?php
                                          }
                                          }else{ ?>
                                          <div class="col-xs-12 mb-30 text-center">
                                          <h4>Oops No Product Found.</h4>
                                          <p>We couldn't find what you were looking for.</p>
                                          <p>Make sure you used the right keywords.</p>
                                          </div>
                                          <?php }
                                          ?>
                                          </div>
                                          </div>
                                          <?php */ ?>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="rt-pagination round pt-60 text-center" id="pagination_link">
                                        <?php
                                        include('pagination.php');
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-2">
                            <div class="sidebar white-bg">
                                <div class="widget mb-45">
                                    <div class="widget-title mb-30">
                                        <h5 class="mb-5">Categories</h5>
                                        <div class="horizontal-line">
                                            <hr class="top">
                                            <hr class="bottom">
                                        </div>
                                    </div>
                                    <div class="category">
                                        <ul>
                                            <li><a href="<?php echo base_url() . 'catalog/all' ?>">All Category</a></li>
                                            <?php echo $leftCategory; ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container -->
            </section>
            <?php echo $footer; ?>
        </div>
        <script src="<?php echo base_url() ?>assets/front/js/vendor/jquery-1.12.4.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/jquery.validate.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/slick.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/swiper.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/isotope.pkgd.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/plugins.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/main.js"></script>
        <script>
                                                                                                                                                            var max_price = <?php echo $productPriceData[0]['max_price'] ?>;
                                                                                                                                                            var filter_max_price = <?php echo $filterData['pricerange'][1] ?>;
                                                                                                                                                            var filter_min_price = <?php echo $filterData['pricerange'][0] ?>;
        </script>
        <script src="<?php echo base_url() ?>assets/front/js/front.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/bootstrap_sweetalert.js"></script>
    </body>
</html>
