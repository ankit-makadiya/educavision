<ul class="c-content-pagination c-square c-theme">
    <?php
	if(($paginationData['pSel'] <= $paginationData['pMids']) || ($paginationData['pTot'] <= $paginationData['pLinks'])){
        $sPage = 1;
        $ePage = ($paginationData['pTot'] <= $paginationData['pLinks']) ? $paginationData['pTot'] : $paginationData['pLinks'];
    }else{
        $etPage = $paginationData['pSel'] + ($paginationData['pMids'] - 1);
        $ePage = ($etPage <= $paginationData['pTot']) ? $etPage : $paginationData['pTot'];
        $sPage = $ePage - ($paginationData['pLinks'] - 1);
    }
    if($paginationData['pSel'] > $sPage){
        $url_first = $paginationData['url'] .'/page/1';
        $url_prev = $paginationData['url'] .'/page/' . ($paginationData['pSel'] - 1);
        $sL = '<a href="javascript:void(0);" id="1" data-id="1" class="pagination_box">First</a>';
        $sN = '<a href="javascript:void(0);" id="'.($paginationData['pSel'] - 1).'" data-id="'.($paginationData['pSel'] - 1).'" class="pagination_box">❮</a>';
    }else{
        $sL = '';
        $sN = '';
    }    
    if(empty($paginationData['offset'])){
        $offset = 1;
    }else{
        $offset = $paginationData['offset'] + 1;
    }
    $showing_from = $offset;
    $showing_to = $paginationData['pSel'] * $paginationData['limit'];

    if($paginationData['pSel'] >= $paginationData['pTot']){
        $showing_to = $paginationData['total_count'];        
    }
    ?>
    <!-- <li>Showing <?php echo $showing_from; ?> - <?php echo $showing_to; ?> of <?php echo $paginationData['total_count']; ?> Products&nbsp;&nbsp;&nbsp;</li> -->
    <li><?php echo $sN; ?></li>
    <?php
    if($paginationData['pSel'] <= $ePage){
        $next = $paginationData['pSel'] + 1;
        $url_link_last = $paginationData['url'] . '/page/' . $paginationData['pTot'];
        $url_link_next = $paginationData['url'] . '/page/' . $next;
        if($paginationData['pTot'] == $paginationData['pSel']){
            $eL = '';
            $eN = '';
        }else{
            $eL = '<a href="javascript:void(0);" data-id="'.$paginationData['pTot'].'" class="pagination_box">End</a>';
            $eN = '<a href="javascript:void(0);" data-id="'.$next.'" class="pagination_box">❯</a>';
        }
    }else{
        $eL = '';
        $eN = '';
    }
    $i = $sPage;

    for($i=$sPage; $i<=$ePage; $i++){
        $url_link = $paginationData['url'] . '/page/' . $i;
        if($i != $paginationData['pSel']){ ?>
            <li class="page-number"><a href="javascript:void(0);" id="<?php echo $i; ?>" data-id="<?php echo $i; ?>" class="pagination_box"><?php echo $i; ?></a></li>
        <?php }else{ ?>
            <li class="active page-number"><a href="javascript:void(0);" data-id="<?php echo $i; ?>" class="pagination_box"><?php echo $i; ?></a></li>
        <?php }
    }
    ?>
    <li class="renderPN"><?php echo $eN ?></li>
            <!-- <li class="renderFL"><?php /* echo $eL */ ?></li> -->
</ul>
