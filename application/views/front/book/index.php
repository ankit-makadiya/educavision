<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <title><?php echo $productData['name'] . ' | ' . $settingData['site_title']; ?></title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo $settingData['site_logo'] ?>">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/front/style.css">
        <script src="<?php echo base_url() ?>assets/front/js/vendor/modernizr-2.8.3.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/front/css/bootstrap_sweetalert.css">
    </head>
    <body>
        <!-- <div id="loading-wrap">
                <div class="loading-effect"></div>
        </div> -->
        <div class="wrapper">
            <?php echo $header; ?>
            <?php echo $breadcum; ?>
            <section class="course-details section-padding white-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-8 mobi-mb-50">
                            <div class="single-course mr-minus-30">
                                <div class="thumb">
                                    <div class="col-md-5">
                                        <img src="<?php echo base_url($productData['image']) ?>"
                                             alt="<?php echo strip_tags($productData['name']) ?>"/>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="title clearfix mb-15">
                                            <h2 class="pull-left text-capitalize no-margin"><?php echo strip_tags($productData['name']) ?></h2>
<!--                                            <h2 class="pull-right no-margin"><sup>$</sup><span id="product-price"><?php //echo $productData['price'] ?></span>
                                            </h2>-->
                                        </div>
                                        <!--<p class="mb-20"><?php //echo $productData['collection'] ?></p>-->
                                        <div class="organization-info mb-15">
                                            <ul>
                                                <?php if(!empty($productData['author_name'])){ ?><li><b>Author : </b><?php echo $productData['author_name'];  ?></li><?php } ?>
                                                <?php if(!empty($productData['isbn_no'])){ ?><li><b>ISBN : </b><span id="product-isbn"><?php echo $productData['isbn_no']  ?></span></li><?php } ?>
                                                <li><b>Cat. Number : </b><span id="product-sku"><?php echo $productData['sku'];  ?></span></li>
                                                <li><b>Price : </b>$<span id="product-price"><?php echo $productData['price'] ?></span></li>
                                                <?php if(!empty($productData['collection'])){ ?><li><b>Collection : </b><?php echo $productData['collection'] ?></li> <?php } ?>
                                            </ul>
                                        </div>
                                        <div class="this-info mb-15">
                                            <ul class="clearfix">
                                                <li>
                                                    <div class="teacher">
                                                        <div class="question-form">
                                                            <form class="custom-input">
                                                                <h5 class="no-margin">Quantity : <input type="text" name="quantity" id="quantity" value="1" size="4" min="1"></h5>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="this-info mb-10">
                                            <ul class="clearfix">
                                                <li>
                                                    <div class="teacher">
                                                        <div class="question-form">
                                                            <form class="custom-input">
                                                                <select id="attribute" name="attribute" class="form-control">
                                                                    <?php foreach ($attributeData as $attribute): ?>
                                                                        <option data-sku="<?php echo $attribute['sku'] ?>" data-isbn="<?php echo $attribute['isbn_no'] ?>" data-price="<?php echo $attribute['price'] ?>" value="<?php echo $attribute['slug'] ?>" <?php
                                                                        if ($attribute['is_default'] == 1) {
                                                                            echo 'selected="selected"';
                                                                        }
                                                                        ?> ><?php echo $attribute['name'] ?></option>
<?php endforeach; ?>
                                                                </select>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="this-info mb-15">
                                            <ul class="clearfix">
                                                <li style="border-left: none;"><a class="btn" href="javascript:void(0);"
                                                                                  onclick="addtocart(<?php echo $productData['id'] ?>)">Add
                                                        to Cart</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-content ptb-60">
                                    <div class="tab-pane fade in active" id="description">
                                        <div class="des-title mb-25">
                                            <h5 class="mb-5">Description</h5>
                                            <div class="horizontal-line">
                                                <hr class="top">
                                                <!--<hr class="bottom">-->
                                            </div>
                                        </div>
                                            <?php if (!empty($productData['language'])) { ?><p class="mb-25"><b>Language
                                                    : </b><?php
                                                if (!empty($productData['language'])) {
                                                    echo $productData['language'];
                                                } else {
                                                    echo '-';
                                                }
                                                ?></p><?php } ?>
                                        <p class="mb-25"><?php echo $productData['description'] ?></p>
                                        <p class="mb-25">
                                            <?php
                                            $product_tags = explode(',', $productData['tags']);
                                            ?>
                                        <div class="widget">
                                            <div class="tags">
                                                <?php
                                                foreach ($product_tags as $tag) {
                                                    ?>
                                                    <a href="javascript:void(0)"><?php echo $tag; ?></a>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        </p>
                                    </div>
                                </div>
                            </div>
<?php if (!empty($suggestedProductData)) { ?>
                                <div class="row">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="des-title mb-25">
                                                <h5 class="mb-5">Book suggestions</h5>
                                                <div class="horizontal-line">
                                                    <hr class="top">
                                                    <hr class="bottom">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <!-- Controls -->
                                            <div class="controls pull-right hidden-xs">
                                                <a class="left fa fa-chevron-left btn btn-success" href="#carousel-example"
                                                   data-slide="prev"></a><a class="right fa fa-chevron-right btn btn-success" href="#carousel-example"
                                                   data-slide="next"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="carousel-example" class="carousel slide hidden-xs" data-ride="carousel">
                                        <div class="carousel-inner">
                                            <?php
                                            $i = 1;
                                            foreach ($suggestedProductData as $product) {
                                                $bookUrl = base_url() . 'book/' . $product['slug'];
                                                $imageUrl = '';
                                                if (file_exists($_SERVER['DOCUMENT_ROOT'] . $product['image']) && is_file($_SERVER['DOCUMENT_ROOT'] . $product['image'])) {
                                                    $imageUrl = $product['image'];
                                                } else {
                                                    $imageUrl = '/uploads/no-image.png';
                                                }
                                                ?>
                                                <?php if ($i == 1 || $i % 4 == 0) { ?>
                                                    <div class="item <?php
                                        if ($i == 1) {
                                            echo 'active';
                                        }
                                        ?>">
                                                        <div class="row">
        <?php } ?>
                                                        <div class="col-xs-12 col-sm-6 col-md-4 mb-50">
                                                            <div class="course-box col-item">
                                                                <div class="course-content plr-25 clearfix">
                                                                    <div class="thumb text-center">
                                                                        <a href="<?php echo $bookUrl; ?>"
                                                                           title="<?php echo $product['name']; ?>"><img
                                                                                src="<?php echo base_url($imageUrl); ?>"
                                                                                alt="<?php echo $product['name']; ?>"
                                                                                width="100%" height="170px;"/></a>
                                                                        <div class="price">
                                                                            <h3 class="no-margin">
                                                                                <span>$</span><?php echo $product['price'] ?>
                                                                            </h3>
                                                                        </div>
                                                                    </div>
                                                                    <a href="<?php echo $bookUrl; ?>" title="<?php echo $product['name']; ?>">
                                                                        <h3 class="text-capitalize"><?php echo (strlen($product['name']) > 15) ? substr($product['name'], 0, 15) . '...' : $product['name']; ?></h3>
                                                                    </a>
                                                                    <div class="date-comment clearfix">
                                                                        <div class="pull-left">
                                                                            <h6><span>SKU: </span><?php echo $product['sku'] ?>
                                                                            </h6>
                                                                            <h6>
                                                                                <span>ISBN No: </span><?php echo $product['isbn_no'] ?>
                                                                            </h6>
                                                                            <h6><?php echo (!empty($product['collection'])) ? $product['collection'] : '&nbsp;' ?></h6>
                                                                        </div>
                                                                    </div>
                                                                    <div class="add-to-cart">
                                                                        <button class="btn add-to-cart" type="button"
                                                                                onclick="addtocart('<?php echo $product['id'] ?>', '<?php echo $product['attribute_slug'] ?>')">
                                                                            Add to Cart
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                                <div class="footer clearfix plr-10">
                                                                    <div class="pull-left">
                                                                        <h5 class="no-margin">By
                                                                            : <?php echo $product['author_name'] ?></h5>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                <?php if ($i % 3 == 0 || count($suggestedProductData) == $i) { ?>
                                                        </div>
                                                    </div>
                                                <?php } ?>
        <?php
        $i++;
    }
    ?>
                                        </div>
                                    </div>
                                </div>

<?php } ?>
                            <div class="clearfix"></div>
                        </div>

                        <div class="col-xs-12 col-sm-4">
                            <div class="sidebar pl-40 white-bg">
                                <div class="widget mb-45 pr-70">
                                    <div class="widget-title mb-30">
                                        <h5 class="mb-5">Categories</h5>
                                        <div class="horizontal-line">
                                            <hr class="top"/>
                                            <hr class="bottom"/>
                                        </div>
                                    </div>
                                    <div class="category style-2">
                                        <ul>
<?php echo $leftCategory; ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.Sidebar End -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container -->
            </section>

<?php echo $footer; ?>
        </div>
        <script src="<?php echo base_url() ?>assets/front/js/vendor/jquery-1.12.4.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/jquery.validate.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/slick.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/swiper.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/isotope.pkgd.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/plugins.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/main.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/front.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/bootstrap_sweetalert.js"></script>
    </body>
</html>
