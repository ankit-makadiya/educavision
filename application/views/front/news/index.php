<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <title><?php echo $newsData['name'] . ' | ' . $settingData['site_title']; ?></title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo $settingData['site_logo'] ?>">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/front/style.css">
        <script src="<?php echo base_url() ?>assets/front/js/vendor/modernizr-2.8.3.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/front/css/bootstrap_sweetalert.css">
    </head>
    <body>
        <div id="loading-wrap">
            <div class="loading-effect"></div>
        </div>
        <div class="wrapper">
            <?php echo $header; ?>
            <?php echo $breadcum; ?>
            <section class="blog-details section-padding white-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-8 mobi-mb-50">
                            <div class="blog-post column-1 mb-50 mr-minus-30">
                                <div class="thumb text-center">
                                    <a href="blog-details.html"><img src="<?php echo $newsData['image'] ?>" alt="<?php echo $newsData['name'] ?>" /></a>
                                </div>
                                <div class="blog-content">
                                    <div class="date-box clearfix mb-25">
                                        <h4 class="theme-color pull-left no-margin"><?php echo date('d', strtotime($newsData['created_date'])) ?> <span><?php echo date('F', strtotime($newsData['created_date'])) ?></span></h4>
                                        <div class="name-comment">
                                            <h2 class="text-capitalize"><?php echo $newsData['name'] ?></h2>
                                            <h5 class="pull-left no-margin"><span class="theme-color">By:</span> <?php echo $newsData['posted_by'] ?></h5>
                                            <h5 class="pl-45 no-margin"><span class="theme-color">Comment:</span> <?php echo count($newsCommentData); ?></h5>
                                        </div>
                                    </div>
                                    <p class="mb-30"><?php echo $newsData['description'] ?></p>
                                </div>
                            </div>
                            <div class="comment-area mr-minus-30">
                                <div class="comment-list mb-60">
                                    <div class="comment-title mb-40">
                                        <h5 class="mb-5">Comments</h5>
                                        <div class="horizontal-line">
                                            <hr class="top" />
                                            <hr class="bottom" />
                                        </div>
                                    </div>
                                    <ul>
                                        <?php
                                        if (!empty($newsCommentData)) {
                                            foreach ($newsCommentData as $comment) {
                                                ?>
                                                <li>
                                                    <div class="text pt-5">
                                                        <h5 class="text-capitalize"><?php echo $comment['name'] ?></h5>
                                                        <p><?php echo $comment['comment'] ?></p>
                                                        <h6><?php echo date('F d Y', strtotime($comment['created_date'])) ?></h6>
                                                    </div>
                                                </li>
                                            <?php }
                                        } else {
                                            ?>
                                            <li> No Comments Available</li>
                                        <?php }
                                        ?>
                                    </ul>
                                </div>
                                <div class="comment-box">
                                    <div class="comment-title mb-40">
                                        <h5 class="mb-5">leave a Comment</h5>
                                        <div class="horizontal-line">
                                            <hr class="top" />
                                            <hr class="bottom" />
                                        </div>
                                    </div>
                                    <form class="custom-input" id="news_comment_form" name="news_comment_form" method="post" action="">
                                        <div class="row">
                                            <input type="hidden" name="news_id" id="news_id" value="<?php echo $newsData['id'] ?>" />
                                            <div class="col-xs-12 col-sm-6">
                                                <input type="text" name="name" id="name" placeholder="Your Name" />
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <input type="email" name="email" id="email" placeholder="Email" />
                                            </div>
                                        </div>
                                        <textarea name="comment" id="comment" rows="3" placeholder="Comment"></textarea>
                                        <input class="btn" type="submit" name="submit" value="submit">
                                    </form>
                                </div>
                            </div>
                            <!-- /.Comments Area End -->
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <div class="sidebar pl-50 white-bg">
                                <div class="widget mb-45">
                                    <div class="widget-title mb-30">
                                        <h5 class="mb-5">Categories</h5>
                                        <div class="horizontal-line">
                                            <hr class="top" />
                                            <hr class="bottom" />
                                        </div>
                                    </div>
                                    <div class="category">
                                        <ul>
<?php echo $leftCategory; ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.Sidebar End -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container -->
            </section>

<?php echo $footer; ?>
        </div>
        <script src="<?php echo base_url() ?>assets/front/js/vendor/jquery-1.12.4.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/jquery.validate.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/slick.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/swiper.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/isotope.pkgd.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/plugins.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/main.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/front.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/bootstrap_sweetalert.js"></script>
    </body>
</html>