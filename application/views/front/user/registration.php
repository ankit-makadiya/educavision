<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <title>Registration | <?php echo $settingData['site_title']; ?></title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo $settingData['site_logo'] ?>">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/front/style.css">
        <script src="<?php echo base_url() ?>assets/front/js/vendor/modernizr-2.8.3.min.js"></script>
        <link href="<?php echo base_url() ?>assets/front/css/select2.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/front/css/bootstrap_sweetalert.css">
        <style type="text/css">
            .select2-container .select2-selection--single{ 
                height: 40px;
                border-color: #e7e7e7;
                border-radius: 4px;
                color: #959595;
                font-size: 13px;
                font-weight: 500;
                line-height: 40px; 
                margin-bottom:05px;
            }
            .select2-selection__placeholder{
                padding: 0px 7px;
                color: #959595;
                font-size: 13px;
                font-weight: 500;
                line-height: 40px; 
            }
            .select2-selection__arrow{
                height: 38px !important;
            }
        </style>
    </head>
    <body>
        <div id="loading-wrap">
            <div class="loading-effect"></div>
        </div>
        <div class="wrapper">
            <?php echo $header; ?>
            <?php echo $breadcum; ?>
            <section class="register-area section-padding white-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-text-center">
                            <div class="enter-box">
                                <h3 class="mb-35">Registration</h3>
                                <form name="registration_form" class="custom-input" action="#" method="post" id="registration_form">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-6">
                                            <input type="text" name="first_name" id="first_name" placeholder="First Name">
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-6">
                                            <input type="text" name="last_name" id="last_name" placeholder="Last Name">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-6">
                                            <input type="text" name="email" id="email" placeholder="Email">
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-6">
                                            <input type="text" name="phone" id="phone" placeholder="Phone Number">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-6">
                                            <input type="password" name="password" id="password" placeholder="Password">
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-6">
                                            <input type="password" name="confirm_password" id="confirm_password" placeholder="Confirm Password">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-6">
                                            <select name="country" id="country" class="country" onchange="changeCountry(this.value)">
                                                <option value="">Select Country</option>
                                                <?php foreach ($country_list as $country) { ?>
                                                    <option value="<?php echo $country['id'] ?>"><?php echo $country['name'] ?></option>
                                                <?php }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-6">
                                            <select name="state" id="state" class="state" onchange="changeState(this.value)">
                                                <option value="">Select State</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-6">
                                            <select name="city" id="city" class="city">
                                                <option value="">Select City</option>
                                            </select>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-6">
                                            <input type="text" name="zipcode" id="zipcode" placeholder="Enter Zipcode">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <input type="text" name="company_name" id="company_name" placeholder="Company Name">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-6">
                                            <input type="text" name="address1" id="address1" placeholder="Enter Street Address">
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-6">
                                            <input type="text" name="address2" id="address2" placeholder="Enter Apartment, Suite, Unit etc.">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 pull-left">
                                            <input type="submit" class="btn" name="submit" id="registration_submit" value="Create Account" />
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <?php echo $footer; ?>
        </div>
        <script src="<?php echo base_url() ?>assets/front/js/vendor/jquery-1.12.4.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/jquery.validate.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/slick.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/swiper.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/isotope.pkgd.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/plugins.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/main.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/front.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/bootstrap_sweetalert.js"></script>
    </body>
</html>
