<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <title>Order History | <?php echo $settingData['site_title']; ?></title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo $settingData['site_logo'] ?>">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/front/style.css">
        <script src="<?php echo base_url() ?>assets/front/js/vendor/modernizr-2.8.3.min.js"></script>
        <link href="<?php echo base_url() ?>assets/front/css/select2.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/front/css/bootstrap_sweetalert.css">
        <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/datatables/dataTables.bootstrap.css'; ?>">
        <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
        <style type="text/css">
            .btn{
                line-height: normal !important;
                padding: 5px 8px !important;
            }
        </style>
    </head>
    <body>
        <div id="loading-wrap">
            <div class="loading-effect"></div>
        </div>
        <div class="wrapper">
            <?php echo $header; ?>
            <?php echo $breadcum; ?>
            <section class="register-area section-padding white-bg">
                <div class="container">
                    <div class="c-layout-sidebar-menu c-theme ">
                        <div class="c-sidebar-menu-toggler">
                            <h3 class="c-title c-font-uppercase c-font-bold">My Profile</h3>
                            <a href="javascript:;" class="c-content-toggler" data-toggle="collapse" data-target="#sidebar-menu-1">
                                <span class="c-line"></span> <span class="c-line"></span> <span class="c-line"></span>
                            </a>
                        </div>
                        <?php include 'sidebar-menu.php'; ?>
                    </div>
                    <div class="c-layout-sidebar-content ">
                        <!-- BEGIN: PAGE CONTENT -->
                        <!-- BEGIN: CONTENT/SHOPS/SHOP-CUSTOMER-DASHBOARD-1 -->
                        <div class="c-content-title-1">
                            <h3 class="c-font-uppercase c-font-bold">Order History</h3>
                            <div class="c-line-left"></div>

                        </div>
                        <div class="row">
                            <div class="box-body">
                                <table id="history" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Order Number</th>
                                            <th>Quantity</th>
                                            <th>Amount</th>
                                            <th>View</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i=1; 
                                        foreach ($order_list as $order) { ?>
                                            <tr>
                                                <td><?php echo $i ?></td>
                                                <td><?php echo $order['order_number'] ?></td>
                                                <td><?php echo $order['total_qty'] ?></td>
                                                <td><?php echo $order['total_amount'] ?></td>
                                                <td>
                                                    <a href="<?php echo base_url('order-details/' . $order['id']) ?>" id="edit_tax_btn" title="View Order">
                                                        <button type="button" class="btn btn-primary"><i class="fa fa-eye"></i></button>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php
                                        $i++; 
                                            } 
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div><!-- END: CONTENT/SHOPS/SHOP-CUSTOMER-DASHBOARD-1 -->
                        <!-- END: PAGE CONTENT -->
                    </div>
                </div>
            </section>
            <?php echo $footer; ?>
        </div>
        <script src="<?php echo base_url() ?>assets/front/js/vendor/jquery-1.12.4.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/jquery.validate.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/slick.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/swiper.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/isotope.pkgd.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/plugins.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/main.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/front.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/bootstrap_sweetalert.js"></script>
        <script src="<?php echo base_url().'assets/plugins/datatables/jquery.dataTables.min.js'; ?>"></script>
        <script src="<?php echo base_url().'assets/plugins/datatables/dataTables.bootstrap.min.js'; ?>"></script>
        <script>
            $(function () {
                $('#history').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": false,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false
                });
            });
        </script>
    </body>
</html>