<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <title>Edit Profile | <?php echo $settingData['site_title']; ?></title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo $settingData['site_logo'] ?>">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/front/style.css">
        <script src="<?php echo base_url() ?>assets/front/js/vendor/modernizr-2.8.3.min.js"></script>
        <link href="<?php echo base_url() ?>assets/front/css/select2.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/front/css/bootstrap_sweetalert.css">
        <style type="text/css">
            .select2-container .select2-selection--single{ 
                height: 40px;
                border-color: #e7e7e7;
                border-radius: 4px;
                color: #959595;
                font-size: 13px;
                font-weight: 500;
                line-height: 40px; 
                margin-bottom:05px;
            }
            .select2-selection__placeholder{
                padding: 0px 7px;
                color: #959595;
                font-size: 13px;
                font-weight: 500;
                line-height: 40px; 
            }
            .select2-selection__arrow{
                height: 38px !important;
            }
        </style>
    </head>
    <body>
        <!-- <div id="loading-wrap">
            <div class="loading-effect"></div>
        </div> -->
        <div class="wrapper">
            <?php echo $header; ?>
            <?php echo $breadcum; ?>
            <section class="register-area section-padding white-bg">
                <div class="container">
                    <div class="c-layout-sidebar-menu c-theme ">
                        <div class="c-sidebar-menu-toggler">
                            <h3 class="c-title c-font-uppercase c-font-bold">My Profile</h3>
                            <a href="javascript:;" class="c-content-toggler" data-toggle="collapse" data-target="#sidebar-menu-1">
                                <span class="c-line"></span> <span class="c-line"></span> <span class="c-line"></span>
                            </a>
                        </div>
                        <?php include 'sidebar-menu.php'; ?>
                    </div>
                    <div class="c-layout-sidebar-content ">
                        <!-- BEGIN: PAGE CONTENT -->
                        <div class="c-content-title-1">
                            <h3 class="c-font-uppercase c-font-bold">Edit Profile</h3>
                            <div class="c-line-left"></div>
                        </div>
                        <?php
                        $attributes = array('id' => 'edit_user_profile_form', 'enctype' => 'multipart/form-data');
                        $hidden = array('id' => $userDetails['id']);
                        echo form_open_multipart('user/editProfile', $attributes, $hidden);
                        ?>
                        <!-- BEGIN: ADDRESS FORM -->
                        <div class="">
                            <!-- BEGIN: BILLING ADDRESS -->
                            <input type="hidden" class="form-control c-square c-theme" placeholder="User ID" value="<?php echo (!empty($userDetails['id'])) ? $userDetails['id'] : ''; ?>" name="user_id">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="control-label">First Name</label>
                                            <input type="text" value="<?php echo (!empty($userDetails['firstname'])) ? $userDetails['firstname'] : ''; ?>" class="form-control c-square c-theme" placeholder="First Name" name="firstname" id="firstname">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="control-label">Last Name</label>
                                            <input type="text" class="form-control c-square c-theme" value="<?php echo (!empty($userDetails['lastname'])) ? $userDetails['lastname'] : ''; ?>" placeholder="Last Name" name="lastname" id="lastname">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label class="control-label">Company Name</label>
                                    <input type="text" value="<?php echo (!empty($userDetails['company_name'])) ? $userDetails['company_name'] : ''; ?>" class="form-control c-square c-theme" placeholder="Company Name" name="company_name" id="company_name">
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label class="control-label">Address</label>
                                    <input type="text" value="<?php echo (!empty($userDetails['address1'])) ? $userDetails['address1'] : ''; ?>" class="form-control c-square c-theme" placeholder="Street Address" name="address1" id="address1">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <input type="text" class="form-control c-square c-theme" value="<?php echo (!empty($userDetails['address2'])) ? $userDetails['address2'] : ''; ?>" placeholder="Apartment, suite, unit etc. (optional)" name="address2" id="address2">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="control-label">Country</label>
                                            <select class="form-control c-square c-theme country" name="country" id="country" onchange="changeCountry(this.value)">
                                                <option value=" ">Please Select Country</option>
                                                <?php foreach ($country_list as $ckey => $cval) { ?>
                                                    <option value="<?php echo $cval['id']; ?>" <?php
                                                    if ($userDetails['country'] == $cval['id']) {
                                                        echo "selected";
                                                    }
                                                    ?>><?php echo $cval['name']; ?></option>
                                                        <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="control-label">State / County</label> 
                                            <select class="form-control c-square c-theme state" name="state" id="state" onchange="changeState(this.value)">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="control-label">Town / City</label>
                                            <select class="form-control c-square c-theme city" name="city" id="city" onchange="changeCity(this.value)">
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="control-label">Postcode / Zip</label>
                                            <input type="text" class="form-control c-square c-theme" placeholder="Postcode / Zip" value="<?php echo (!empty($userDetails['zipcode'])) ? $userDetails['zipcode'] : ''; ?>" name="zipcode" id="zipcode">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="control-label">Email Address</label>
                                            <input type="email" name="email" class="form-control c-square c-theme" value="<?php echo (!empty($userDetails['email'])) ? $userDetails['email'] : ''; ?>" placeholder="Email Address" disabled="disabled">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="control-label">Phone</label>
                                            <input type="tel" name="phone" id="phone" class="form-control c-square c-theme" value="<?php echo (!empty($userDetails['phone'])) ? $userDetails['phone'] : ''; ?>" placeholder="Phone">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END: BILLING ADDRESS -->
                            <div class="row c-margin-t-10">
                                <div class="form-group col-md-12" role="group">
                                    <button type="submit" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Submit</button>
                                </div>
                            </div>
                        </div>
                        <!-- END: ADDRESS FORM -->
                        <?php echo form_close(); ?>		<!-- END: PAGE CONTENT -->
                    </div>
                </div>
            </section>
            <?php echo $footer; ?>
        </div>
        <script src="<?php echo base_url() ?>assets/front/js/vendor/jquery-1.12.4.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/jquery.validate.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/slick.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/swiper.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/isotope.pkgd.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/plugins.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/main.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/front.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/bootstrap_sweetalert.js"></script>
        <script src="<?php echo base_url() ?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script type="text/javascript">
        var selected_country = '<?php echo ($userDetails['country']) ? $userDetails['country'] : '' ?>';
        var selected_state = '<?php echo ($userDetails['state']) ? $userDetails['state'] : '' ?>';
        var selected_city = '<?php echo ($userDetails['city']) ? $userDetails['city'] : '' ?>';
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                if (selected_country != '') {
                    changeCountry(selected_country, selected_state);
                }
                if (selected_state != '') {
                    changeState(selected_state, selected_city);
                }
                $(".country").select2({
                    placeholder: "Select Country",
                    allowClear: true,
                    width: '100%'
                });
                $(".state").select2({
                    placeholder: "Select State",
                    allowClear: true,
                    width: '100%'
                });
                $(".city").select2({
                    placeholder: "Select City",
                    allowClear: true,
                    width: '100%'
                });

                $("#edit_user_profile_form").validate({
                    rules: {
                        firstname: {
                            required: true,
                        },
                        lastname: {
                            required: true,
                        },
                        phone: {
                            required: true,
                        },
                        company_name: {
                            required: true,
                        },
                        address1: {
                            required: true,
                        },
                        country: {
                            required: true,
                        },
                        state: {
                            required: true,
                        },
                        city: {
                            required: true,
                        },
                        zipcode: {
                            required: true,
                        },
                    },
                    messages: {
                        firstname: {
                            required: "Please enter first name",
                        },
                        lastname: {
                            required: "Please enter last name",
                        },
                        phone: {
                            required: "Please enter phone number",
                        },
                        company_name: {
                            required: "Please enter company name",
                        },
                        address1: {
                            required: "Please enter address",
                        },
                        country: {
                            required: "Please select country",
                        },
                        state: {
                            required: "Please select state",
                        },
                        city: {
                            required: "Please select city",
                        },
                        zipcode: {
                            required: "Please enter zipcode",
                        }
                    },
                    highlight: function (element, errorClass, validClass) {
                        var elem = $(element);
                        if (elem.hasClass("select2-hidden-accessible")) {
                            $("#select2-" + elem.attr("id") + "-container").parent().addClass(errorClass);
                        } else {
                            elem.addClass(errorClass);
                        }
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        var elem = $(element);
                        if (elem.hasClass("select2-hidden-accessible")) {
                            $("#select2-" + elem.attr("id") + "-container").parent().removeClass(errorClass);
                        } else {
                            elem.removeClass(errorClass);
                        }
                    },
                    errorPlacement: function (error, element) {
                        var elem = $(element);
                        if (elem.hasClass("select2-hidden-accessible")) {
                            element = $("#select2-" + elem.attr("id") + "-container").parent();
                            error.insertAfter(element);
                        } else {
                            error.insertAfter(element);
                        }
                    },
                    submitHandler: submitEditProfileForm
                });
                function submitEditProfileForm()
                {
                    $.ajax({
                        url: base_url + 'user/edit_profile_save',
                        type: 'post',
                        data: $("#edit_user_profile_form").serialize(),
                        dataType: "json",
                        success: function (response) {
                            console.log(response)
                            if (response.success == '1') {
                                swal({
                                    title: "Success",
                                    text: response.message,
                                    type: "success",
                                    showCancelButton: false,
                                    confirmButtonClass: "btn-success",
                                    confirmButtonText: "Ok",
                                    closeOnConfirm: false
                                },
                                        function () {
                                            window.location.href = "";
                                        });
                            } else {
                                swal({
                                    title: "Error",
                                    text: response.message,
                                    type: "error",
                                    showCancelButton: false,
                                    confirmButtonClass: "btn-error",
                                    confirmButtonText: "Ok",
                                    closeOnConfirm: false
                                },
                                        function () {
                                            window.location.href = "";
                                        });
                            }
                        },
                    });
                }
            });
        </script>
    </body>
</html>