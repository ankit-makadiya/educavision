<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <title>Change Password | <?php echo $settingData['site_title']; ?></title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo $settingData['site_logo'] ?>">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/front/style.css">
        <script src="<?php echo base_url() ?>assets/front/js/vendor/modernizr-2.8.3.min.js"></script>
        <link href="<?php echo base_url() ?>assets/front/css/select2.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/front/css/bootstrap_sweetalert.css">
        <style type="text/css">
            .select2-container .select2-selection--single{ 
                height: 40px;
                border-color: #e7e7e7;
                border-radius: 4px;
                color: #959595;
                font-size: 13px;
                font-weight: 500;
                line-height: 40px; 
                margin-bottom:05px;
            }
            .select2-selection__placeholder{
                padding: 0px 7px;
                color: #959595;
                font-size: 13px;
                font-weight: 500;
                line-height: 40px; 
            }
            .select2-selection__arrow{
                height: 38px !important;
            }
        </style>
    </head>
    <body>
        <!--        <div id="loading-wrap">
                    <div class="loading-effect"></div>
                </div>-->
        <div class="wrapper">
            <?php echo $header; ?>
            <?php echo $breadcum; ?>
            <section class="register-area section-padding white-bg">
                <div class="container">
                    <div class="c-layout-sidebar-menu c-theme ">
                        <div class="c-sidebar-menu-toggler">
                            <h3 class="c-title c-font-uppercase c-font-bold">My Profile</h3>
                            <a href="javascript:;" class="c-content-toggler" data-toggle="collapse" data-target="#sidebar-menu-1">
                                <span class="c-line"></span> <span class="c-line"></span> <span class="c-line"></span>
                            </a>
                        </div>
                        <?php include 'sidebar-menu.php'; ?>
                    </div>
                    <div class="c-layout-sidebar-content ">
                        <div class="c-content-title-1">
                            <h3 class="c-font-uppercase c-font-bold">Change Password</h3>
                            <div class="c-line-left"></div>
                        </div>
                        <?php
                        $form_attr = array('id' => 'change_pass_form', 'class' => 'form-horizontal row-border');
                        echo form_open('user/changePassword', $form_attr);
                        ?>
                        <div class="">
                            <div class="row">
                                <div class="form-group col-md-8">
                                    <label class="control-label">Old Password</label>
                                    <input type="password" class="form-control c-square c-theme" placeholder="Old Password" name="old_password" id="old_password">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-8">
                                    <label class="control-label">New Password</label>
                                    <input type="password" class="form-control c-square c-theme" placeholder="New Password"  name="new_password" id="new_password">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-8">
                                    <label class="control-label">Confirm Password</label>
                                    <input type="password" class="form-control c-square c-theme" placeholder="Confirm Password"  name="confirm_password" id="confirm_password">
                                </div>
                            </div>
                            <div class="row c-margin-t-10">
                                <div class="form-group col-md-8" role="group">
                                    <button type="submit" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase">Submit</button>
                                </div>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <?php echo $footer; ?>
        </div>

        <script src="<?php echo base_url() ?>assets/front/js/vendor/jquery-1.12.4.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/jquery.validate.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/slick.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/swiper.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/isotope.pkgd.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/plugins.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/main.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/front.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/bootstrap_sweetalert.js"></script>

        <script>
            $(document).ready(function () {
                $('.callout-danger').delay(3000).hide('700');
                $('.callout-success').delay(3000).hide('700');
                $("#change_pass_form").validate({
                    rules: {
                        old_password: {
                            required: true,
                           remote: {
                               url: "<?php echo base_url() . 'user/check_old_pass' ?>",
                               type: "post",
                               data: {
                                   old_password: function () {
                                       return $("#old_password").val();
                                   },
                                   '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>',
                               },
                           },
                        },
                        new_password: {
                            required: true,
                            maxlength: 16,
                            minlength: 5
                        },
                        confirm_password: {
                            required: true,
                            equalTo: '#new_password',
                        },
                    },
                    messages:
                            {
                                old_password: {
                                    required: "Old password is required",
                                    remote: "Old Password Not Match",
                                },
                                new_password: {
                                    required: "New password is required",
                                    minlength: "Password must be between 5-16 characters long"
                                },
                                confirm_password: {
                                    required: "Confirm password is required",
                                    equalTo: "New Password and Confirm Password Must match"
                                }
                            },
                            submitHandler: submitChangePasswordForm
                });

                function submitChangePasswordForm(){
                    var new_password = $("#new_password").val();
    
                    var post_data = {
                        'new_password': new_password,
                    }
                    $.ajax({
                        type: 'POST',
                        url: base_url + 'change-password-save',
                        data: post_data,
                        dataType: "json",
                        beforeSend: function ()
                        {
                            $("#error").fadeOut();
                            $("#btn-login").html('Submit ...');
                        },
                        success: function (response)
                        {
                            console.log(response)
                            if (response.success == '1') {
                                swal({
                                    title: "Success",
                                    text: response.message,
                                    type: "success",
                                    showCancelButton: false,
                                    confirmButtonClass: "btn-success",
                                    confirmButtonText: "Ok",
                                    closeOnConfirm: false
                                },
                                        function () {
                                            window.location.href = "";
                                        });
                            } else {
                                swal({
                                    title: "Error",
                                    text: response.message,
                                    type: "error",
                                    showCancelButton: false,
                                    confirmButtonClass: "btn-error",
                                    confirmButtonText: "Ok",
                                    closeOnConfirm: false
                                },
                                        function () {
                                            window.location.href = "";
                                        });
                            }
                        }
                    });
                    return false;
                }
            });
        </script>

    </body>
</html>