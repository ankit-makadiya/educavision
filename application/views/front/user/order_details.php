<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <title>Order History | <?php echo $settingData['site_title']; ?></title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo $settingData['site_logo'] ?>">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/front/style.css">
        <script src="<?php echo base_url() ?>assets/front/js/vendor/modernizr-2.8.3.min.js"></script>
        <link href="<?php echo base_url() ?>assets/front/css/select2.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/front/css/bootstrap_sweetalert.css">
        <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.css'; ?>">
        <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
        <style type="text/css">
            .select2-container .select2-selection--single{ 
                height: 40px;
                border-color: #e7e7e7;
                border-radius: 4px;
                color: #959595;
                font-size: 13px;
                font-weight: 500;
                line-height: 40px; 
                margin-bottom:05px;
            }
            .select2-selection__placeholder{
                padding: 0px 7px;
                color: #959595;
                font-size: 13px;
                font-weight: 500;
                line-height: 40px; 
            }
            .select2-selection__arrow{
                height: 38px !important;
            }
        </style>
    </head>
    <body>
        <?php
        $CI = & get_instance();
        ?>  
        <!-- <div id="loading-wrap">
            <div class="loading-effect"></div>
        </div> -->
        <div class="wrapper">
            <?php echo $header; ?>
            <?php echo $breadcum; ?>
            <section class="register-area section-padding white-bg">
                <div class="container">
                    <div class="c-layout-sidebar-menu c-theme ">
                        <div class="c-sidebar-menu-toggler">
                            <h3 class="c-title c-font-uppercase c-font-bold">My Profile</h3>
                            <a href="javascript:;" class="c-content-toggler" data-toggle="collapse" data-target="#sidebar-menu-1">
                                <span class="c-line"></span> <span class="c-line"></span> <span class="c-line"></span>
                            </a>
                        </div>
                        <?php include 'sidebar-menu.php'; ?>
                    </div>
                    
                    <div class="c-layout-sidebar-content ">
                        <!-- BEGIN: PAGE CONTENT -->
                        <!-- BEGIN: CONTENT/SHOPS/SHOP-CUSTOMER-DASHBOARD-1 -->

                        <div class="row">
                            <div class="col-md-12">

                                <div class="box box-info">
                                    <h3 class="pull-left"><b>Order Number</b> : <?php echo $order_data[0]['order_number']; ?>
                                    </h3>
                                    <?php if(!empty($order_data[0]['tracking_detail'])){ ?><a href="<?php echo $order_data[0]['tracking_detail'] ?>" class="pull-right btn btn-default" title="Track Order" target="_blank">Track Order</a><?php } ?>
                                            <table class="table table-bordered" style="padding:20px;">
                                                <tr><td><h3>Shipping Details<h3></td></tr>
                                                <tr><td>
                                                    <?php echo $order_data[0]['shipping_first_name'] .' '.$order_data[0]['shipping_last_name'] ; ?><br><br>
                                                    <b><?php echo $order_data[0]['shipping_company_name']; ?></b><br>
                                                    <?php echo $order_data[0]['shipping_address'] .'<br>'.$order_data[0]['shipping_address_1'] ; ?><br>

                                                    <?php echo $CI->getCityNameById($order_data[0]['shipping_city']); ?>,
                                                    <?php echo $CI->getStateNameById($order_data[0]['shipping_state']); ?>,
                                                    <?php echo $CI->getCountryNameById($order_data[0]['shipping_country']); ?> - <?php echo $order_data[0]['shipping_zipcode']; ?><br>
                                                    <b>Email : </b> <?php echo $order_data[0]['shipping_email']; ?><br>
                                                    <b>Phone : </b> <?php echo $order_data[0]['shipping_phone']; ?>
                                                </td></tr>
                                                </table>
                                                <hr>
                                                <table class="table table-bordered" style="padding:20px;">
                                                    <tr><td><h3>Billing Details<h3></td></tr>
                                                <tr><td>
                                                    <?php echo $order_data[0]['billing_first_name'] .' '.$order_data[0]['billing_last_name'] ; ?><br><br>
                                                    <b><?php echo $order_data[0]['billing_company_name']; ?></b><br>
                                                    <?php echo $order_data[0]['billing_address'] .'<br>'.$order_data[0]['billing_address_1'] ; ?><br>

                                                    <?php echo $CI->getCityNameById($order_data[0]['billing_city']); ?>,
                                                    <?php echo $CI->getStateNameById($order_data[0]['billing_state']); ?>,
                                                    <?php echo $CI->getCountryNameById($order_data[0]['billing_country']); ?> - <?php echo $order_data[0]['billing_zipcode']; ?><br>
                                                    <b>Email : </b> <?php echo $order_data[0]['billing_email']; ?><br>
                                                    <b>Phone : </b> <?php echo $order_data[0]['billing_phone']; ?>
                                                </td></tr>
                                                </table>
                                                <hr>
                                                <table class="table table-bordered" style="padding:20px;">
                                                    <tr><td colspan="6"><h3>Order Details<h3></td></tr>
                                                    <tr>
                                                        <td><b>Id</b></td>
                                                        <td><b>Product Name</b></td>
                                                        <td><b>Product Sku</b></td>
                                                        <td><b>Product Qty</b></td>
                                                        <td><b>Product Price</b></td>
                                                        <td><b>Total Amount</b></td>
                                                    </tr>
                                                <?php
                                                $i = 0;
                                                $totalAmount = 0;
                                                foreach ($order_details as $orderDetais) {
                                                    $i++;
                                                    $totalAmount += $orderDetais['product_qty'] * $orderDetais['product_price'];

                                                    // $product_data = $CI->getProductDataBySku($orderDetais['product_sku']);
                                                    // $book_type = '';
                                                    // $download_book = '';
                                                    // if(!empty($product_data)){
                                                    //     $book_type = $product_data['book_type'];
                                                    //     $download_book = $product_data['download_book'];    
                                                    // }
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $i; ?></td>
                                                        <!--<td><?php /* echo $orderDetais['product_name']; ?><?php if($orderDetais['attribute_slug'] == 'e-book'): ?><a href="<?php echo base_url().$orderDetais['attribute_slug']; ?>" style="float:right; color:#000;" title="Download Book"><i class="fa fa-download"></i></a><?php endif; */?></td>-->
                                                        <td><?php echo $orderDetais['product_name']; ?></td>
                                                        <td><?php echo $orderDetais['product_sku']; ?></td>
                                                        <td><?php echo $orderDetais['product_qty']; ?></td>
                                                        <td><?php echo '$'.$orderDetais['product_price']; ?></td>
                                                        <td><?php echo '$'. ($orderDetais['product_qty'] * $orderDetais['product_price']); ?></td>
                                                    </tr>
                                                <?php } ?>
                                                    <tr>
                                                        <td colspan="5">Sub Total</td>
                                                        <td><?php echo '$'.$order_data[0]['sub_total']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5">Total Tax</td>
                                                        <td><?php echo '$'.$order_data[0]['total_tax']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5">Shipping Charge</td>
                                                        <td><?php echo '$'.$order_data[0]['shipping_charge']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5">Discount</td>
                                                        <td><?php echo '$'.$order_data[0]['total_discount']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5">Total Amount</td>
                                                        <td><?php echo '$'.$order_data[0]['total_amount']; ?></td>
                                                    </tr>
                                                </table>
                                                <hr>
                                                <table class="table table-bordered" style="padding:20px;">
                                                    <tr><td><h3>Payment Details<h3></td></tr>
                                                    <tr><td><b>Invoice Number</b></td><td><?php echo $order_payment['invoice_number']; ?></td></tr>
                                                    <tr><td><b>Transaction Id</b></td><td><?php echo $order_payment['transaction_id']; ?></td></tr>
                                                    <tr><td><b>Pay Amount</b></td><td><?php echo '$'.$order_payment['pay_amount']; ?></td></tr>
                                                    <tr><td><b>Payment Status</b></td>
                                                        <td>
                                                        <?php echo ($order_payment['payment_status'] == '1') ? 'Success' : (($order_payment['payment_status'] == '2') ? 'Fail' : 'Pending');  ?>
                                                        </td>
                                                    </tr>
                                                    <tr><td><b>Order Date</b></td><td><?php echo $order_payment['created_date']; ?></td></tr>
                                                </table>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <?php echo $footer; ?>
            </div>
            <script src="<?php echo base_url() ?>assets/front/js/vendor/jquery-1.12.4.min.js"></script>
            <script src="<?php echo base_url() ?>assets/front/js/bootstrap.min.js"></script>
            <script src="<?php echo base_url() ?>assets/front/js/jquery.validate.min.js"></script>
            <script src="<?php echo base_url() ?>assets/front/js/slick.min.js"></script>
            <script src="<?php echo base_url() ?>assets/front/js/swiper.min.js"></script>
            <script src="<?php echo base_url() ?>assets/front/js/isotope.pkgd.min.js"></script>
            <script src="<?php echo base_url() ?>assets/front/js/plugins.js"></script>
            <script src="<?php echo base_url() ?>assets/front/js/main.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
            <script src="<?php echo base_url() ?>assets/front/js/front.js"></script>
            <script src="<?php echo base_url() ?>assets/front/js/bootstrap_sweetalert.js"></script>
            <script src="<?php echo base_url() . 'assets/plugins/datatables/jquery.dataTables.min.js'; ?>"></script>
            <script src="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.min.js'; ?>"></script>
<script>
    $(function () {
        $('#history').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
</body>
</html>