<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <title>Dashboard | <?php echo $settingData['site_title']; ?></title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo $settingData['site_logo'] ?>">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/front/style.css">
        <script src="<?php echo base_url() ?>assets/front/js/vendor/modernizr-2.8.3.min.js"></script>
        <link href="<?php echo base_url() ?>assets/front/css/select2.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/front/css/bootstrap_sweetalert.css">
        <style type="text/css">
            .select2-container .select2-selection--single{ 
                height: 40px;
                border-color: #e7e7e7;
                border-radius: 4px;
                color: #959595;
                font-size: 13px;
                font-weight: 500;
                line-height: 40px; 
                margin-bottom:05px;
            }
            .select2-selection__placeholder{
                padding: 0px 7px;
                color: #959595;
                font-size: 13px;
                font-weight: 500;
                line-height: 40px; 
            }
            .select2-selection__arrow{
                height: 38px !important;
            }
        </style>
    </head>
    <body>
        <!-- <div id="loading-wrap">
            <div class="loading-effect"></div>
        </div> -->
        <div class="wrapper">
            <?php echo $header; ?>
            <?php echo $breadcum; ?>
            <section class="register-area section-padding white-bg">
                <div class="container">
                    <div class="c-layout-sidebar-menu c-theme ">
                        <div class="c-sidebar-menu-toggler">
                            <h3 class="c-title c-font-uppercase c-font-bold">My Profile</h3>
                            <a href="javascript:;" class="c-content-toggler" data-toggle="collapse" data-target="#sidebar-menu-1">
                                <span class="c-line"></span> <span class="c-line"></span> <span class="c-line"></span>
                            </a>
                        </div>
                        <?php include 'sidebar-menu.php'; ?>
                    </div>
                    <div class="c-layout-sidebar-content ">
                        <div class="c-content-title-1">
                            <h3 class="c-font-uppercase c-font-bold">My Dashboard</h3>
                            <?php
                            $userData = $this->session->userdata('educavision_front');
                            $fullName = '';
                            $fullName .= (!empty($userData['firstname'])) ? $userData['firstname'] : '';
                            $fullName .= ' ';
                            $fullName .= (!empty($userData['lastname'])) ? $userData['lastname'] : '';
                            ?>
                            <div class="c-line-left"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12 c-margin-b-20">
                                <h4 class="c-font-uppercase c-font-bold">Hello, <?php echo $fullName; ?></h4>
                                <ul class="list-unstyled">
        <?php
        $contition_array = array('user_id' => $userData['id']);
        $data = '*';
        $user_address = $this->common->select_data_by_condition('user_address', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

        $contition_array = array('id' => $user_address[0]['city']);
        $data = '*';
        $user_city = $this->common->select_data_by_condition('city', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

        $contition_array = array('id' => $user_address[0]['state']);
        $data = '*';
        $user_state = $this->common->select_data_by_condition('state', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

        $contition_array = array('id' => $user_address[0]['country']);
        $data = '*';
        $user_country = $this->common->select_data_by_condition('country', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

        ?>

                                    <li><?php echo ($user_address[0]['company_name']) ? $user_address[0]['company_name'] : '' ?><br>
                                        <?php echo ($user_address[0]['address1']) ? $user_address[0]['address1'] : '' ?><br>
                                        <?php echo ($user_address[0]['address2']) ? $user_address[0]['address2'] : '' ?><br>
                                        <?php echo ($user_city[0]['name']) ? $user_city[0]['name'] : '' ?>, <?php echo ($user_state[0]['name']) ? $user_state[0]['name'] : '' ?>, <?php echo ($user_country[0]['name']) ? $user_country[0]['name'] : '' ?>  
                                        <?php echo ($user_address[0]['zipcode']) ? '-'.$user_address[0]['zipcode'] : '' ?>
                                    </li>
                                    <li>Phone: <?php echo (!empty($userData['phone'])) ? $userData['phone'] : ''; ?></li>
                                    <li>Email: <a href="mailto:<?php echo (!empty($userData['email'])) ? $userData['email'] : ''; ?>" class="c-theme-link"><?php echo (!empty($userData['email'])) ? $userData['email'] : ''; ?></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <?php echo $footer; ?>
        </div>
        <script src="<?php echo base_url() ?>assets/front/js/vendor/jquery-1.12.4.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/jquery.validate.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/slick.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/swiper.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/isotope.pkgd.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/plugins.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/main.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/front.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/bootstrap_sweetalert.js"></script>
    </body>
</html>