<ul class="c-sidebar-menu collapse " id="sidebar-menu-1">
    <li class="c-dropdown c-open">
        <a href="javascript:;" class="c-toggler">My Profile<span class="c-arrow"></span></a>
        <ul class="c-dropdown-menu">
            <li class="<?php echo ($this->uri->segment(1) == 'dashboard') ? 'c-active' : '' ?>">
                <a href="<?php echo base_url('dashboard') ?>">My Dashboard</a>
            </li>
            <li class="<?php echo ($this->uri->segment(1) == 'edit-profile') ? 'c-active' : '' ?>">
                <a href="<?php echo base_url('edit-profile') ?>">Edit Profile</a>
            </li>
            <li class="<?php echo ($this->uri->segment(1) == 'order-history' || $this->uri->segment(1) == 'order-details') ? 'c-active' : '' ?>">
                <a href="<?php echo base_url('order-history') ?>">Order History</a>
            </li>
            <li class="<?php echo ($this->uri->segment(1) == 'change-password') ? 'c-active' : '' ?>">
                <a href="<?php echo base_url('change-password') ?>">Change Password</a>
            </li>
        </ul>
    </li>
</ul>