<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <title><?php echo $pageData[0]['name'] . ' | ' . $settingData['site_title']; ?></title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo $settingData['site_logo'] ?>">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/front/style.css">
        <script src="<?php echo base_url() ?>assets/front/js/vendor/modernizr-2.8.3.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/front/css/bootstrap_sweetalert.css">
    </head>
    <body>
        <div id="loading-wrap">
            <div class="loading-effect"></div>
        </div>
        <div class="wrapper">
            <?php echo $header; ?>
            <?php echo $breadcum; ?>
            <section class="features-area section-padding white-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="section-title mb-70">
                                <?php echo ($pageData[0]['is_advance'] == 1) ? $pageData[0]['advance_description'] : $pageData[0]['description'] ?>
                            </div>

                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="list-view">
                                    <div class="row">
                                        <div class="col-xs-12 mb-30">
                                            <?php if (!empty($news_data)) { ?>
                                                <h2>News</h2>
                                                <?php foreach ($news_data as $news) { ?>
                                                    <div class="course-box list-view clearfix">
                                                        <div class="thumb text-center pull-left">
                                                            <a href="<?php echo base_url() . 'news/' . $news['slug'] ?>"><img src="<?php echo $news['image'] ?>" alt="<?php echo $news['name'] ?>"></a>
                                                        </div>
                                                        <div class="course-content">
                                                            <a href="<?php echo base_url() . 'news/' . $news['slug'] ?>">
                                                                <h3 class="text-capitalize"><?php echo $news['name'] ?></h3>
                                                            </a>
                                                            <div class="date-comment clearfix">
                                                                <div class="pull-left">
                                                                    <h6><span>Date: </span><?php echo date('F d, Y', strtotime($news['created_date'])) ?></h6>
                                                                </div>
                                                                <div class="pull-right">
                                                                    <h6><i class="zmdi zmdi-comments"></i><?php echo $news['comment_count'] ?></h6>
                                                                </div>
                                                            </div>
                                                            <p class="mb-20"><?php echo $news['description'] ?></p>
                                                            <a class="btn btn-1" href="<?php echo base_url() . 'news/' . $news['slug'] ?>">Read More</a>
                                                            <div class="footer clearfix plr-25">
                                                                <div class="pull-left">
                                                                    <h5 class="no-margin">Posted By : <?php echo $news['posted_by'] ?></h5>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <!-- /.row -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <?php echo $footer; ?>
        </div>
        <script src="<?php echo base_url() ?>assets/front/js/vendor/jquery-1.12.4.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/jquery.validate.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/slick.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/swiper.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/isotope.pkgd.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/plugins.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/main.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/front.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/bootstrap_sweetalert.js"></script>
        <script type="text/javascript">
            $(function () {
                $('form').on('submit', function (e) {
                    e.preventDefault();
                    $.ajax({
                        type: 'post',
                        url: '<?php echo base_url() ?>page/form-save',
                        data: $('form').serialize(),
                        dataType: "json",
                        success: function (response) {
                            if (response.success == '1') {
                                swal({
                                    title: "Success",
                                    text: response.message,
                                    type: "success",
                                    showCancelButton: false,
                                    confirmButtonClass: "btn-success",
                                    confirmButtonText: "Ok",
                                    closeOnConfirm: false
                                },
                                        function () {
                                            window.location.href = "";
                                        });
                            } else {
                                swal({
                                    title: "Error",
                                    text: response.message,
                                    type: "error",
                                    showCancelButton: false,
                                    confirmButtonClass: "btn-error",
                                    confirmButtonText: "Ok",
                                    closeOnConfirm: false
                                },
                                        function () {
                                            window.location.href = "";
                                        });
                            }
                        }
                    });

                });
            });
        </script>
    </body>
</html>