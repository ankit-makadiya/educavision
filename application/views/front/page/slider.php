<div class="container">
    <section class="slider-area">
        <div id="mainslider-2">
            <?php
            foreach($sliderImageData as $slider){
            ?>
            <a href="<?php echo (isset($slider['link'])) ? $slider['link'] : 'javascript:void(0)' ?>" target="<?php echo (isset($slider['link_target'])) ? $slider['link_target'] : '' ?>">
                <div class="single-item overlay dark-4">
                    <img src="<?php echo base_url($slider['img_name']) ?>" alt="<?php echo (isset($slider['title'])) ? $slider['title'] : '' ?>" />
                    <?php
                    if(isset($slider['title']) && !empty($slider['title'])){
                    ?>
                    <div class="slider-caption">
                        <div class="container">
                            <div class="cd-intro-content mask-2">
                                <div class="intro-content">
                                    <div class="inner-div">
                                        <p class="mb-25 hidden-xs hidden-sm"><?php echo (isset($slider['title'])) ? $slider['title'] : '' ?></p>
                                        <!-- <a class="btn" href="javascript:viod(0)">Start a Course</a> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </a>
            <?php
            }
            ?>
        </div>
    </section>
</div>
