<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <title>Checkout | <?php echo $settingData['site_title']; ?></title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo $settingData['site_logo'] ?>">
        <link href="<?php echo base_url() ?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <script src="<?php echo base_url() ?>assets/front/js/vendor/modernizr-2.8.3.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/front/css/bootstrap_sweetalert.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/front/css/jquery.bootstrap-touchspin.css">
        <link href="<?php echo base_url() ?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/front/style.css">
        <style type="text/css">
            .panel-title {
                display: inline;
                font-weight: bold;
            }
            .display-table {
                display: table;
            }
            .display-tr {
                display: table-row;
            }
            .display-td {
                display: table-cell;
                vertical-align: middle;
                width: 61%;
            }
        </style>
    </head>
    <body>
        <!--        <div id="loading-wrap">
                    <div class="loading-effect"></div>
                </div>-->
        <div class="wrapper">
            <?php echo $header; ?>
            <?php echo $breadcum; ?>
            <section class="course-details section-padding white-bg">
                <div class="container">
                    <?php
                    if ($this->cart->total_items() > 0) {
                        ?>
                        <form id="checkout-form" name="checkout-form" role="form" action="<?php echo base_url('order') ?>" method="post" class="require-validation" data-cc-on-file="false" id="payment-form">
                            <div class="row">
                                <!-- BEGIN: ADDRESS FORM -->
                                <div class="col-md-7 c-padding-20">
                                    <!-- BEGIN: BILLING ADDRESS -->
                                    <h3 class="c-font-bold c-font-uppercase c-font-24">Shipping Address</h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="form-group required col-md-6">
                                                    <label class="control-label">First Name</label>
                                                    <input type="text" name="shipping_first_name" id="shipping_first_name" class="form-control c-square c-theme" placeholder="First Name" value="<?php echo (isset($user_data['firstname']) && !empty($user_data['firstname'])) ? $user_data['firstname'] : '' ?>">
                                                </div>
                                                <div class="form-group required col-md-6">
                                                    <label class="control-label">Last Name</label>
                                                    <input type="text" name="shipping_last_name" id="shipping_last_name" class="form-control c-square c-theme" placeholder="Last Name" value="<?php echo (isset($user_data['lastname']) && !empty($user_data['lastname'])) ? $user_data['lastname'] : '' ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <div class="row">
                                                <div class="form-group required col-md-6">
                                                    <label class="control-label">Email Address</label>
                                                    <input type="email" name="shipping_email" id="shipping_email" class="form-control c-square c-theme" placeholder="Email Address" value="<?php echo (isset($user_data['email']) && !empty($user_data['email'])) ? $user_data['email'] : '' ?>">
                                                </div>
                                                <div class="form-group required col-md-6">
                                                    <label class="control-label">Phone</label>
                                                    <input type="tel" name="shipping_phone" id="shipping_phone" class="form-control c-square c-theme" placeholder="Phone" value="<?php echo (isset($user_data['phone']) && !empty($user_data['phone'])) ? $user_data['phone'] : '' ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group required col-md-12">
                                            <label class="control-label">Company Name</label>
                                            <input type="text" name="shipping_company_name" id="shipping_company_name" class="form-control c-square c-theme" placeholder="Company Name" value="<?php echo (isset($shipping_details['company_name']) && !empty($shipping_details['company_name'])) ? $shipping_details['company_name'] : '' ?>">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group required col-md-12">
                                            <label class="control-label">Address</label>
                                            <input type="text" name="shipping_address" id="shipping_address" class="form-control c-square c-theme" placeholder="Street Address" value="<?php echo (isset($shipping_details['address1']) && !empty($shipping_details['address1'])) ? $shipping_details['address1'] : '' ?>">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group required col-md-12">
                                            <input type="text" name="shipping_address1" id="shipping_address1" class="form-control c-square c-theme" placeholder="Apartment, suite, unit etc. (optional)" value="<?php echo (isset($shipping_details['address2']) && !empty($shipping_details['address2'])) ? $shipping_details['address2'] : '' ?>">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group required col-md-12">
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label class="control-label">Country</label>
                                                    <select class="form-control required country select2" name="shipping_country" id="shipping_country" tabindex="-1" onchange="changeCountry(this.value);" autocomplete="off">
                                                        <option value="">Select Country</option>
                                                        <?php
                                                        if (isset($shipping_details['country']) && !empty($shipping_details['country'])) {
                                                            $selected_country = $shipping_details['country'];
                                                        } elseif (isset($this->session->userdata('cart_contents')['cart_shipping_country']) && !empty($this->session->userdata('cart_contents')['cart_shipping_country'])) {
                                                            $selected_country = $this->session->userdata('cart_contents')['cart_shipping_country'];
                                                        } else {
                                                            $selected_country = 21;
                                                        }
                                                        foreach ($country_list as $country):
                                                            $selected = '';
                                                            if ($country['id'] == $selected_country):
                                                                $selected = 'selected="selected"';
                                                            endif;
                                                            ?>
                                                            <option value="<?php echo $country['id'] ?>" <?php echo $selected; ?>><?php echo $country['name'] ?></option>
                                                        <?php endforeach; ?>
                                                    </select> 
                                                </div>
                                                <div class="form-group required col-md-6">
                                                    <label class="control-label">State / County</label>
                                                    <select class="form-control state select2" name="shipping_state" id="shipping_state" tabindex="-1" onchange="changeState(this.value);" autocomplete="off">
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <div class="row">
                                                <div class="form-group required col-md-6">
                                                    <label class="control-label">Town / City</label>
                                                    <select class="form-control city select2" name="shipping_city" id="shipping_city" tabindex="-1" autocomplete="off">
                                                    </select> 
                                                </div>
                                                <div class="form-group required col-md-6">
                                                    <label class="control-label">Postcode / Zip</label>
                                                    <input type="text" name="shipping_zip" id="shipping_zip" class="form-control c-square c-theme" placeholder="Postcode / Zip" value="<?php echo (isset($shipping_details['zipcode']) && !empty($shipping_details['zipcode'])) ? $shipping_details['zipcode'] : '' ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- SHIPPING ADDRESS -->
                                    <h3 class="c-font-bold c-font-uppercase c-font-24">Billing Address</h3>
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <div class="c-checkbox-inline">
                                                <div class="c-checkbox c-toggle-hide" data-object-selector="c-billing-address" data-animation-speed="600">
                                                    <input name="is_billing_checkbox" type="checkbox" id="is_billing_checkbox" class="c-check">
                                                    <label for="is_billing_checkbox">
                                                        <span class="inc"></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span>
                                                        Bill to different address?
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="c-billing-address" style="display: none;">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="form-group required col-md-6">
                                                        <label class="control-label">First Name</label>
                                                        <input type="text" name="billing_first_name" id="billing_first_name" class="form-control c-square c-theme" placeholder="First Name">
                                                    </div>
                                                    <div class="form-group required col-md-6">
                                                        <label class="control-label">Last Name</label>
                                                        <input type="text" name="billing_last_name" id="billing_last_name" class="form-control c-square c-theme" placeholder="Last Name">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-12">
                                                <div class="row">
                                                    <div class="form-group required col-md-6">
                                                        <label class="control-label">Email Address</label>
                                                        <input type="email" name="billing_email" id="billing_email" class="form-control c-square c-theme" placeholder="Email Address">
                                                    </div>
                                                    <div class="form-group required col-md-6">
                                                        <label class="form-group required control-label">Phone</label>
                                                        <input type="tel" name="billing_phone" id="billing_phone" class="form-control c-square c-theme" placeholder="Phone">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group required col-md-12">
                                                <label class="control-label">Company Name</label>
                                                <input type="text" name="billing_company_name" id="billing_company_name" class="form-control c-square c-theme" placeholder="Company Name">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group required col-md-12">
                                                <label class="control-label">Address</label>
                                                <input type="text" name="billing_address" id="billing_address" class="form-control c-square c-theme" placeholder="Street Address">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group required col-md-12">
                                                <input type="text" name="billing_address1" id="billing_address1" class="form-control c-square c-theme" placeholder="Apartment, suite, unit etc. (optional)">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group required col-md-12">
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label class="control-label">Country</label>
                                                        <select class="form-control country select2" name="billing_country" id="billing_country" tabindex="-1"  onchange="changeCountry(this.value);">
                                                            <?php
                                                            if (isset($billing_details['country']) && !empty($billing_details['country'])) {
                                                                $selected_country = $billing_details['country'];
                                                            } else {
                                                                $selected_country = 21;
                                                            }
                                                            foreach ($country_list as $country):
                                                                $selected = '';
                                                                if ($country['id'] == $selected_country):
                                                                    $selected = 'selected="selected"';
                                                                endif;
                                                                ?>
                                                                <option value="<?php echo $country['id'] ?>" <?php echo $selected; ?>><?php echo $country['name'] ?></option>
                                                            <?php endforeach; ?>
                                                        </select> 
                                                    </div>
                                                    <div class="form-group required col-md-6">
                                                        <label class="control-label">State / County</label>
                                                        <select class="form-control state select2" name="billing_state" id="billing_state" tabindex="-1" onchange="changeState(this.value);">
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-12">
                                                <div class="row">
                                                    <div class="form-group required col-md-6">
                                                        <label class="control-label">Town / City</label>
                                                        <select class="form-control city select2" name="billing_city" id="billing_city" tabindex="-1">
                                                        </select> 
                                                    </div>
                                                    <div class="form-group required col-md-6">
                                                        <label class="control-label">Postcode / Zip</label>
                                                        <input type="text" name="billing_zip" id="billing_zip" class="form-control c-square c-theme" placeholder="Postcode / Zip">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- SHIPPING ADDRESS -->
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label class="control-label">Order Notes</label>
                                            <textarea name="order_notes" id="order_notes" class="form-control c-square c-theme" rows="3" placeholder="Note about your order, e.g. special notes for delivery."></textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <h4>Apply Coupon Code</h4>
                                            <form class="form-inline" name="apply_coupon_form" id="apply_coupon_form"
                                                  action="javascript:void(0);">
                                                <div class="form-group" role="group">
                                                    <input class="form-control" name="coupon_code" id="coupon_code"
                                                           placeholder="Coupon Code" type="text" maxlength="50" style="width:250px;">
                                                    <a href="javascript:void(0);" class="btn c-btn applycoupon input-small c-btn-square calculate-btn"
                                                       title="Add">Add
                                                    </a>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- END: ADDRESS FORM -->
                                <!-- BEGIN: ORDER FORM -->
                                <?php
//                                echo '<pre>';
//                                print_r($cart_contents);
//                                exit;
                                ?>
                                <div class="col-md-5">
                                    <div class="c-content-bar-1 c-align-left c-bordered c-theme-border c-shadow">
                                        <h1 class="c-font-bold c-font-uppercase c-font-24">Your Order</h1>
                                        <ul class="c-order list-unstyled">
                                            <li class="row c-margin-b-15">
                                                <div class="col-md-6 c-font-16"><h2>Product</h2></div>
                                                <div class="col-md-6 c-font-16"><h2>Total</h2></div>
                                            </li>
                                            <li class="row c-border-bottom"></li>
                                            <?php foreach ($this->cart->contents() as $items): ?>
                                                <li class="row c-margin-b-15 c-margin-t-15">
                                                    <div class="col-md-6 c-font-20"><a href="<?php echo base_url('book/' . $items['slug']) ?>" class="c-theme-link"><?php echo $items['name'] ?></a></div>
                                                    <div class="col-md-6 c-font-20">
                                                        <p class="">$<?php echo $this->cart->format_number($items['price'] * $items['qty']); ?></p>
                                                    </div>
                                                </li>
                                            <?php endforeach; ?>
                                            <li class="row c-margin-b-15 c-margin-t-15">
                                                <div class="col-md-6 c-font-20">Subtotal</div>
                                                <div class="col-md-6 c-font-20">
                                                    <p class="">$<span class="c-subtotal"><?php echo $this->cart->format_number($cart_contents['cart_total']) ?></span></p>
                                                </div>
                                            </li>
                                            <li class="row c-margin-b-15 c-margin-t-15">
                                                <div class="col-md-6 c-font-20">Shipping</div>
                                                <div class="col-md-6 c-font-20">
                                                    <p class="">$<span class="c-shipping"><?php echo $this->cart->format_number($cart_contents['cart_shipping']) ?></span></p>
                                                </div>
                                            </li>
                                            <li class="row c-margin-b-15 c-margin-t-15">
                                                <div class="col-md-6 c-font-20">Tax</div>
                                                <div class="col-md-6 c-font-20">
                                                    <p class="">$<span class="c-tax"><?php echo $this->cart->format_number($cart_contents['cart_tax']) ?></span></p>
                                                </div>
                                            </li>
                                            <?php
                                            if (!empty($cart_contents['cart_coupon_data'])) {
                                                ?>
                                                <li class="row c-margin-b-15 c-margin-t-15">
                                                    <div class="col-md-6 c-font-20">Discount</div>
                                                    <div class="col-md-6 c-font-20">
                                                        <p class="">$<span class="c-discount"><?php echo $this->cart->format_number($cart_contents['cart_discount']) ?></span> <a href="javascript:void(0);" onclick="removeCoupon();" class="c-theme-link c-cart-remove-desktop">×</a></p>
                                                    </div>
                                                </li>
                                            <?php } ?>
                                            <li class="row c-border-top c-margin-b-15"></li>
                                            <li class="row c-margin-b-15 c-margin-t-15">
                                                <div class="col-md-6 c-font-20">
                                                    <p class="c-font-30">Total</p>
                                                </div>
                                                <div class="col-md-6 c-font-20">
                                                    <p class="c-font-bold c-font-30">$<span class="c-grand-total"><?php echo $this->cart->format_number($cart_contents['cart_grand_total']) ?></span></p>
                                                </div>
                                                <input type="hidden" name="cart_grand_total" id="cart_grand_total" value="<?php echo $cart_contents['cart_grand_total']; ?>">
                                            </li>
                                            <li class="row stripe_payment <?php if($cart_contents['cart_grand_total'] > 0){ echo 'show'; }else{ echo 'hide'; } ?>">
                                                <div class="col-md-12">
                                                    <div class="c-radio-list">
                                                        <div class="c-radio">
                                                            <input type="radio" id="radio3" class="c-radio" name="payment" checked="checked">
                                                            <label for="radio3" class="c-font-bold c-font-20">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>
                                                                Stripe
                                                            </label>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="panel panel-default credit-card-box">
                                                                        <div class="panel-heading display-table" >
                                                                            <div class="row display-tr">
                                                                                <div class="display-td">    <img class="img-responsive pull-right" src="<?php echo base_url('assets/front/img/accepted_c22e0.png') ?>">
                                                                                </div>
                                                                            </div>                    
                                                                        </div>
                                                                        <div class="panel-body">
                                                                            <?php if ($this->session->flashdata('success')) { ?>
                                                                                <div class="alert alert-success text-center">
                                                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                                                                    <p><?php echo $this->session->flashdata('success'); ?></p>
                                                                                </div>
                                                                            <?php } ?>
                                                                            <div class="form-row row">
                                                                                <div class="col-xs-12 form-group required">
                                                                                    <label class="control-label">Name on Card</label> 
                                                                                    <input name="card_name" id="card_name" class="form-control" size="4" type="text">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-row row">
                                                                                <div class="col-xs-12 form-group card required">
                                                                                    <label class="control-label">Card Number</label> 
                                                                                    <input name="card_number" id="card_number" autocomplete="off" class="form-control card-number" size="20" type="text" maxlength="16">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-row row">
                                                                                <div class="col-xs-12 col-md-4 form-group cvc required">
                                                                                    <label class="control-label">CVC</label> 
                                                                                    <input name="card_cvc" id="card_cvc" autocomplete="off" class="form-control card-cvc" placeholder="ex. 311" size="4" type="text">
                                                                                </div>
                                                                                <div class="col-xs-12 col-md-4 form-group expiration required">
                                                                                    <label class="control-label">Expiration Month</label> 
                                                                                    <input name="card_exp_month" id="card_exp_month" class="form-control card-expiry-month" placeholder="MM" size="2" type="text">
                                                                                </div>
                                                                                <div class="col-xs-12 col-md-4 form-group expiration required">
                                                                                    <label class="control-label">Expiration Year</label> 
                                                                                    <input name="card_exp_year" id="card_exp_year" class="form-control card-expiry-year" placeholder="YYYY" size="4" type="text">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-row row">
                                                                                <div class="col-md-12 error form-group hide">
                                                                                    <div class="alert-danger alert">Please correct the errors and try again.</div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>        
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="row c-margin-b-15 c-margin-t-15">
                                                <div class="form-group col-md-12">
                                                    <div class="c-checkbox">
                                                        <input type="checkbox" id="terms_condition" class="form-control c-check" name="terms_condition">
                                                        <label for="terms_condition">
                                                            <span class="inc"></span>
                                                            <span class="check"></span>
                                                            <span class="box"></span>
                                                            I’ve read and accept the Terms &amp; Conditions
                                                        </label>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="form-group col-md-12" role="group">
                                                    <button id="payBtn" class="btn btn-primary btn-lg btn-block" type="submit">Pay Now</button>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- END: ORDER FORM -->
                            </div>
                        </form>
                    <?php } else { ?>
                        <div class="c-shop-cart-page-1 text-center">
                            <h2 class="c-font-thin c-center">Your Shopping Cart is Empty</h2>
                            <a href="<?php echo base_url('catalog/all') ?>" class="btn c-btn btn-lg c-btn-dark c-btn-square c-font-white c-font-bold c-font-uppercase">Continue Shopping</a>
                        </div>  
                    <?php } ?>
                </div>
            </section>
            <?php echo $footer; ?>
        </div>
        <script src="<?php echo base_url() ?>assets/front/js/vendor/jquery-1.12.4.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/jquery.validate.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/slick.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/swiper.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/isotope.pkgd.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/plugins.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/main.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/bootstrap_sweetalert.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/jquery.bootstrap-touchspin.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/front.js"></script>
        <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
        <script src="<?php echo base_url() ?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script type="text/javascript">
                                                    //var selected_country = '<?php //echo ($shipping_details['country']) ? $shipping_details['country'] : ''       ?>//';
                                                    //var selected_state = '<?php //echo ($shipping_details['state']) ? $shipping_details['state'] : ''       ?>//';
                                                    //var selected_city = '<?php //echo ($shipping_details['city']) ? $shipping_details['city'] : ''       ?>//';

                                                    var selected_country = $('#shipping_country').val();
                                                    var selected_state = $('#shipping_state').val();
                                                    var selected_city = $('#shipping_city').val();
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#shipping_country').val(selected_country).change();
                var $form = $("#checkout-form");
                if (selected_country != '') {
                    changeCountry(selected_country, selected_state);
                }
                if (selected_state != '') {
                    changeState(selected_state, selected_city);
                }
                $(".country").select2({
                    placeholder: "Select Country",
                    allowClear: true,
                    width: '100%'
                });
                $(".state").select2({
                    placeholder: "Select State",
                    allowClear: true,
                    width: '100%'
                });
                $(".city").select2({
                    placeholder: "Select City",
                    allowClear: true,
                    width: '100%'
                });

                $("#checkout-form").validate({
                    rules: {
                        shipping_first_name: {
                            required: true,
                        },
                        shipping_last_name: {
                            required: true,
                        },
                        shipping_email: {
                            required: true,
                            email: true,
                        },
                        shipping_phone: {
                            required: true,
                        },
                        // shipping_company_name: {
                        //     required: true,
                        // },
                        shipping_address: {
                            required: true,
                        },
                        shipping_country: {
                            required: true,
                        },
                        shipping_state: {
                            required: true,
                        },
                        shipping_city: {
                            required: true,
                        },
                        shipping_zip: {
                            required: true,
                        },
                        billing_first_name: {
                            required: true,
                        },
                        billing_last_name: {
                            required: true,
                        },
                        billing_email: {
                            required: true,
                            email: true,
                        },
                        billing_phone: {
                            required: true,
                        },
                        // billing_company_name: {
                        //     required: true,
                        // },
                        billing_address: {
                            required: true,
                        },
                        billing_country: {
                            required: true,
                        },
                        billing_state: {
                            required: true,
                        },
                        billing_city: {
                            required: true,
                        },
                        billing_zip: {
                            required: true,
                        },
                        card_name: {
                            required: true,
                        },
                        card_number: {
                            required: true,
                        },
                        card_exp_month: {
                            required: true,
                        },
                        card_exp_year: {
                            required: true,
                        },
                        card_cvc: {
                            required: true,
                        },
                        terms_condition: {
                            required: true,
                        },
                    },
                    messages: {
                        shipping_first_name: {
                            required: "Please enter first name",
                        },
                        shipping_last_name: {
                            required: "Please enter last name",
                        },
                        shipping_email: {
                            required: "Please enter email",
                            email: "Please enter valid email",
                        },
                        shipping_phone: {
                            required: "Please enter phone number",
                        },
                        // shipping_company_name: {
                        //     required: "Please enter company name",
                        // },
                        shipping_address: {
                            required: "Please enter address",
                        },
                        shipping_country: {
                            required: "Please select country",
                        },
                        shipping_state: {
                            required: "Please select state",
                        },
                        shipping_city: {
                            required: "Please select city",
                        },
                        shipping_zip: {
                            required: "Please enter zipcode",
                        },
                        billing_first_name: {
                            required: "Please enter first name",
                        },
                        billing_last_name: {
                            required: "Please enter last name",
                        },
                        billing_email: {
                            required: "Please enter email",
                            email: "Please enter valid email",
                        },
                        billing_phone: {
                            required: "Please enter phone number",
                        },
                        // billing_company_name: {
                        //     required: "Please enter company name",
                        // },
                        billing_address: {
                            required: "Please enter address",
                        },
                        billing_country: {
                            required: "Please select country",
                        },
                        billing_state: {
                            required: "Please select state",
                        },
                        billing_city: {
                            required: "Please select city",
                        },
                        billing_zip: {
                            required: "Please enter zipcode",
                        },
                        card_name: {
                            required: "Please enter card name",
                        },
                        card_number: {
                            required: "Please enter card number",
                        },
                        card_exp_month: {
                            required: "Please enter card expiry month",
                        },
                        card_exp_year: {
                            required: "Please enter card expiry year",
                        },
                        card_cvc: {
                            required: "Please enter card cvc",
                        },
                        terms_condition: {
                            required: "Please accept the terms & conditions",
                        },
                    },
                    highlight: function (element, errorClass, validClass) {
                        var elem = $(element);
                        if (elem.hasClass("select2-hidden-accessible")) {
                            $("#select2-" + elem.attr("id") + "-container").parent().addClass(errorClass);
                        } else {
                            elem.addClass(errorClass);
                        }
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        var elem = $(element);
                        if (elem.hasClass("select2-hidden-accessible")) {
                            $("#select2-" + elem.attr("id") + "-container").parent().removeClass(errorClass);
                        } else {
                            elem.removeClass(errorClass);
                        }
                    },
                    errorPlacement: function (error, element) {
                        var elem = $(element);
                        if (elem.hasClass("select2-hidden-accessible")) {
                            element = $("#select2-" + elem.attr("id") + "-container").parent();
                            error.insertAfter(element);
                        } else {
                            error.insertAfter(element);
                        }
                    },
                    submitHandler: submitCheckoutForm
                });
                function submitCheckoutForm(e)
                {
                    if($('#cart_grand_total').val() > 0){
                        $('#payBtn').attr("disabled", "disabled");
                        Stripe.setPublishableKey('<?php echo $this->config->item('stripe_key') ?>');
                        Stripe.createToken({
                            number: $('#card_number').val(),
                            exp_month: $('#card_exp_month').val(),
                            exp_year: $('#card_exp_year').val(),
                            cvc: $('#card_cvc').val()
                        }, stripeResponseHandler);
                        return false;
                    }else{
                        $form.find('input[type=text]').empty();
                        $form.append("<input type='hidden' name='stripeToken' value='offline'/>");
                        $form.get(0).submit();
                    }
                }
                function stripeResponseHandler(status, response) {
                    if (response.error) {
                        $('.error')
                                .removeClass('hide')
                                .find('.alert')
                                .text(response.error.message);
                    } else {
                        var token = response['id'];
                        $form.find('input[type=text]').empty();
                        $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
                        $form.get(0).submit();
                    }
                    $('#payBtn').removeAttr("disabled");
                }
            });
            function removeCoupon() {
                $.ajax({
                    url: base_url + 'shoppingcart/removeCoupon',
                    type: 'post',
                    data: '',
                    dataType: "json",
                    success: function (result) {
                        if (result.success == 1) {
                            swal({
                                title: "Success",
                                text: result.message,
                                type: "success",
                                showCancelButton: false,
                                confirmButtonClass: "btn-success",
                                confirmButtonText: "Ok",
                                closeOnConfirm: false
                            },
                                    function () {
                                        window.location.href = "";
                                    });
                        }
                    },
                });
            }
        </script>
    </body>
</html>
