<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0" />
        <title>Thank you | <?php echo $settingData['site_title']; ?></title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo $settingData['site_logo'] ?>">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/front/style.css">
        <script src="<?php echo base_url() ?>assets/front/js/vendor/modernizr-2.8.3.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/front/css/bootstrap_sweetalert.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/front/css/jquery.bootstrap-touchspin.css">
        <link href="<?php echo base_url() ?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <?php
//        echo '<pre>';
//        print_r($order_data);
//        print_r($order_details);
//        print_r($order_payment);
//        exit;
//        
        ?>
        <!-- <div id="loading-wrap">
            <div class="loading-effect"></div>
        </div> -->
        <div class="wrapper">
            <?php echo $header; ?>
            <?php echo $breadcum; ?>
            <section class="course-details section-padding white-bg">
                <div class="container">
                    <div class="c-shop-order-complete-1 c-content-bar-1 c-align-left c-bordered c-theme-border c-shadow">
                        <?php if ($order_data['payment_status'] == 1): ?>
                        <div class="c-content-title-1">
                            <h3 class="c-center c-font-uppercase c-font-bold">Checkout Completed</h3>
                            <div class="c-line-center c-theme-bg"></div>
                        </div>
                        <div class="c-theme-bg">
                            <p class="c-message c-center c-font-white c-font-20 c-font-sbold">
                                <i class="fa fa-check"></i> Thank you. Your order has been received.
                            </p>
                        </div>
                        <!-- BEGIN: ORDER SUMMARY -->
                        <div class="row c-order-summary c-center">
                            <ul class="c-list-inline list-inline">
                                <li>
                                    <h3>Order Number</h3>
                                    <p>#<?php echo $order_data['order_number']; ?></p>
                                </li>
                                <li>
                                    <h3>Date Purchased</h3>
                                    <p><?php echo date('Y-m-d', strtotime($order_data['created_date'])); ?></p>
                                </li>
                                <li>
                                    <h3>Total Payable</h3>
                                    <p>$<?php echo $order_data['total_amount']; ?></p>
                                </li>
                            </ul>
                        </div>
                        <div class="c-bank-details c-margin-t-30 c-margin-b-30">
                            <p class="c-margin-b-20">Make your payment directly into our account. Please use your Order ID as the payment reference. Your order won't be shipped until the funds have cleared in our account.</p>
                        </div>
                        <div class="c-order-details">
                            <div class="c-border-bottom hidden-sm hidden-xs">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h3 class="c-font-uppercase c-font-16 c-font-grey-2 c-font-bold">Product</h3>
                                    </div>
                                    <div class="col-md-2">
                                        <h3 class="c-font-uppercase c-font-16 c-font-grey-2 c-font-bold">Quantity</h3>
                                    </div>
                                    <div class="col-md-2">
                                        <h3 class="c-font-uppercase c-font-16 c-font-grey-2 c-font-bold">Unit Price</h3>
                                    </div>
                                    <div class="col-md-2">
                                        <h3 class="c-font-uppercase c-font-16 c-font-grey-2 c-font-bold">Total</h3>
                                    </div>
                                </div>
                            </div>
                            <!-- BEGIN: PRODUCT ITEM ROW -->
                            <?php
                            foreach ($order_details as $details):
                                // echo '<pre>';
                                // print_r($details);
                                // exit;
                                ?>
                                <div class="c-border-bottom c-row-item">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-8">
                                            <ul class="c-list list-unstyled">
                                                <li class="c-margin-b-25"><a href="javascript:void(0);" class="c-font-bold c-font-22 c-theme-link"><?php echo $details['product_name'] ?></a></li>
                                                <li><p class="c-font-13">SKU : <?php echo $details['product_sku'] ?></p></li>
                                                <?php if($details['attribute_slug'] == 'e-book'){ ?><li><a href="<?php echo base_url().$details['download_book'] ?>" target="_blank" class="c-font-14 c-font-bold" style="color:#f00;">Click Here for Download your Book</a></li><?php } ?>
                                            </ul>
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                            <p class="visible-xs-block c-theme-font c-font-uppercase c-font-bold">Quantity</p>
                                            <p class="c-font-sbold c-font-uppercase c-font-18"><?php echo $details['product_qty'] ?></p>
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                            <p class="visible-xs-block c-theme-font c-font-uppercase c-font-bold">Unit Price</p>
                                            <p class="c-font-sbold c-font-uppercase c-font-18">$<?php echo $details['product_price'] ?></p>
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                            <p class="visible-xs-block c-theme-font c-font-uppercase c-font-bold">Total</p>
                                            <p class="c-font-sbold c-font-18">$<?php echo ($details['product_price'] * $details['product_qty']) ?></p>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            endforeach;
                            ?>
                            <div class="c-row-item c-row-total c-right">
                                <ul class="c-list list-unstyled">
                                    <li>
                                        <h3 class="c-font-regular c-font-22">Subtotal : &nbsp;
                                            <span class="c-font-dark c-font-bold c-font-22">$<?php echo $order_data['sub_total'] ?></span>
                                        </h3>
                                    </li>
                                    <li>
                                        <h3 class="c-font-regular c-font-22">Tax : &nbsp;
                                            <span class="c-font-dark c-font-bold c-font-22">$<?php echo $order_data['total_tax'] ?></span>
                                        </h3>
                                    </li>
                                    <li>
                                        <h3 class="c-font-regular c-font-22">Shipping Fee : &nbsp;
                                            <span class="c-font-dark c-font-bold c-font-22">$<?php echo $order_data['shipping_charge'] ?></span>
                                        </h3>
                                    </li>
									<li>
										<h3 class="c-font-regular c-font-22">Discount : &nbsp;
											<span class="c-font-dark c-font-bold c-font-22">$<?php echo $order_data['total_discount'] ?></span>
										</h3>
									</li>
                                    <li>
                                        <h3 class="c-font-regular c-font-22">Grand Total : &nbsp;
                                            <span class="c-font-dark c-font-bold c-font-22">$<?php echo $order_data['total_amount'] ?></span>
                                        </h3>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="c-customer-details row" data-auto-height="true">
                            <div class="col-md-6 col-sm-6 c-margin-t-20">
                                <div data-height="height" style="height: 164px;">
                                    <h3 class=" c-margin-b-20 c-font-uppercase c-font-22 c-font-bold">Customer Details</h3>
                                    <ul class="list-unstyled">
                                        <li>Name: <?php echo $order_data['billing_first_name'] . ' ' . $order_data['shipping_last_name']; ?></li>
                                        <li>Phone: <?php echo $order_data['shipping_phone']; ?></li>
                                        <li>Email: <a href="mailto:<?php echo $order_data['shipping_email']; ?>" class="c-theme-color"><?php echo $order_data['shipping_email']; ?></a></li>
                                        <li>Address: 
                                            <p><?php echo ($order_data['shipping_company_name']) ? $order_data['shipping_company_name'] : ''; ?><br>
                                                <?php echo ($order_data['shipping_address']) ? $order_data['shipping_address'] : ''; ?><br>
                                                <?php echo ($order_data['shipping_address_1']) ? $order_data['shipping_address_1'] : ''; ?><br>

                                                <?php
                                                $contition_array = array('id' => $order_data['shipping_city']);
                                                $data = 'name';
                                                $city_details = $this->common->select_data_by_condition('city', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

                                                $contition_array = array('id' => $order_data['shipping_state']);
                                                $data = 'name';
                                                $state_details = $this->common->select_data_by_condition('state', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

                                                $contition_array = array('id' => $order_data['shipping_country']);
                                                $data = 'name';
                                                $country_details = $this->common->select_data_by_condition('country', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
                                                ?>
                                                <?php echo ($city_details[0]['name']) ? $city_details[0]['name'] : ''; ?>, <?php echo ($state_details[0]['name']) ? $state_details[0]['name'] : ''; ?>, <?php echo ($country_details[0]['name']) ? $country_details[0]['name'] : ''; ?> - <?php echo ($order_data['shipping_zipcode']) ? $order_data['shipping_zipcode'] : ''; ?></p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 c-margin-t-20">
                                <div data-height="height" style="height: 164px;">
                                    <h3 class=" c-margin-b-20 c-font-uppercase c-font-22 c-font-bold">Billing Address</h3>
                                    <ul class="list-unstyled">
                                        <li>Name: <?php echo $order_data['billing_first_name'] . ' ' . $order_data['billing_last_name']; ?></li>
                                        <li>Phone: <?php echo $order_data['billing_phone']; ?></li>
                                        <li>Email: <a href="mailto:<?php echo $order_data['billing_email']; ?>" class="c-theme-color"><?php echo $order_data['billing_email']; ?></a></li>
                                        <li>Address: 
                                            <p><?php echo ($order_data['billing_company_name']) ? $order_data['billing_company_name'] : ''; ?><br>
                                                <?php echo ($order_data['billing_address']) ? $order_data['billing_address'] : ''; ?><br>
                                                <?php echo ($order_data['billing_address_1']) ? $order_data['billing_address_1'] : ''; ?><br>

                                                <?php
                                                $contition_array = array('id' => $order_data['billing_city']);
                                                $data = 'name';
                                                $city_details = $this->common->select_data_by_condition('city', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

                                                $contition_array = array('id' => $order_data['billing_state']);
                                                $data = 'name';
                                                $state_details = $this->common->select_data_by_condition('state', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

                                                $contition_array = array('id' => $order_data['billing_country']);
                                                $data = 'name';
                                                $country_details = $this->common->select_data_by_condition('country', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
                                                ?>
                                                <?php echo ($city_details[0]['name']) ? $city_details[0]['name'] : ''; ?>, <?php echo ($state_details[0]['name']) ? $state_details[0]['name'] : ''; ?>, <?php echo ($country_details[0]['name']) ? $country_details[0]['name'] : ''; ?> - <?php echo ($order_data['billing_zipcode']) ? $order_data['billing_zipcode'] : ''; ?></p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <?php else: ?>
                        <div class="c-content-title-1">
                            <h3 class="c-center c-font-uppercase c-font-bold">Checkout Failed</h3>
                            <div class="c-line-center c-theme-bg"></div>
                        </div>
                        <div class="c-theme-bg">
                            <p class="c-message c-center c-font-white c-font-20 c-font-sbold">
                                <i class="fa fa-close"></i> Oops. Your order has been declined. <br> <p><?php
                        if (is_string($order_payment['payment_response'])){
                            echo $order_payment['payment_response'];
                        }
                        ?></p>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </section>
            <?php echo $footer; ?>
        </div>
        <script src="<?php echo base_url() ?>assets/front/js/vendor/jquery-1.12.4.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/jquery.validate.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/slick.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/swiper.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/isotope.pkgd.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/plugins.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/main.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/bootstrap_sweetalert.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/jquery.bootstrap-touchspin.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/front.js"></script>
        <script src="<?php echo base_url() ?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $(".country").select2({
                    placeholder: "Select Country",
                    allowClear: true,
                    width: '100%'
                });
            });
        </script>
    </body>
</html>
