<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate"/>
	<meta http-equiv="Pragma" content="no-cache"/>
	<meta http-equiv="Expires" content="0"/>
	<title>Shopping Cart | <?php echo $settingData['site_title']; ?></title>
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo $settingData['site_logo'] ?>">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/front/style.css">
	<script src="<?php echo base_url() ?>assets/front/js/vendor/modernizr-2.8.3.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/front/css/bootstrap_sweetalert.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/front/css/jquery.bootstrap-touchspin.css">
	<link href="<?php echo base_url() ?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet"
		  type="text/css"/>
</head>
<body>
<!-- <div id="loading-wrap">
	<div class="loading-effect"></div>
</div> -->
<div class="wrapper">
	<?php echo $header; ?>
	<?php echo $breadcum; ?>
	<section class="course-details section-padding white-bg">
		<div class="container">
			<?php
			if ($this->cart->total_items() > 0) {
				?>
				<div class="c-shop-cart-page-1">
					<div class="row c-cart-table-title">
						<div class="col-md-2 c-cart-image">
							<h4 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Image</h4>
						</div>
						<div class="col-md-4 c-cart-desc">
							<h4 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Description</h4>
						</div>
						<div class="col-md-1 c-cart-ref">
							<h4 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">SKU</h4>
						</div>
						<div class="col-md-1 c-cart-qty">
							<h4 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Qty</h4>
						</div>
						<div class="col-md-2 c-cart-price">
							<h4 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Unit Price</h4>
						</div>
						<div class="col-md-1 c-cart-total">
							<h4 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Total</h4>
						</div>
						<div class="col-md-1 c-cart-remove"></div>
					</div>

					<?php foreach ($this->cart->contents() as $items): ?>
						<div class="row c-cart-table-row">
							<h2 class="c-font-uppercase c-font-bold c-theme-bg c-font-white c-cart-item-title c-cart-item-first">
								Item 1</h2>
							<div class="col-md-2 col-sm-3 col-xs-5 c-cart-image">
								<?php
								$imageUrl = '';
								if(file_exists($_SERVER['DOCUMENT_ROOT'].$items['image']) && is_file($_SERVER['DOCUMENT_ROOT'].$items['image'])){
									$imageUrl = $items['image'];
								}else{
									$imageUrl = '/uploads/no-image.png' ;
								}
								?>
								<img src="<?php echo $imageUrl ?>" alt="<?php echo $items['name'] ?>">
							</div>
							<div class="col-md-4 col-sm-9 col-xs-7 c-cart-desc">
								<h3><a href="<?php echo base_url('book/' . $items['slug']) ?>"
									   class="c-font-bold c-theme-link c-font-22 c-font-dark"><?php echo $items['name'] ?></a>
								</h3>
								<p>ISBN: <?php echo $items['isbn_no'] ?></p>
								<p>Author Name: <?php echo $items['author_name'] ?></p>
								<p><i style="font-size: 12px;"><?php echo ucfirst($items['book_type']) ?></i></p>
								<?php if (!empty($items['collection'])) { ?><p>
									Collection: <?php echo $items['collection'] ?></p><?php } ?>
							</div>
							<div class="col-md-1 col-sm-3 col-xs-6 c-cart-ref">
								<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">SKU</p>
								<p><?php echo $items['id'] ?></p>
							</div>
							<div class="col-md-1 col-sm-3 col-xs-6 c-cart-qty">
								<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">QTY</p>
								<div class="c-input-group c-spinner">
									<input type="text" class="form-control c-item-1 touchspin"
										   value="<?php echo $items['qty'] ?>"
										   data-rowid="<?php echo $items['rowid'] ?>">
								</div>
							</div>
							<div class="col-md-2 col-sm-3 col-xs-6 c-cart-price">
								<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Unit Price</p>
								<p class="c-cart-price c-font-bold">
									$<?php echo $this->cart->format_number($items['price']) ?></p>
							</div>
							<div class="col-md-1 col-sm-3 col-xs-6 c-cart-total">
								<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Total</p>
								<p class="c-cart-price c-font-bold">
									$<?php echo $this->cart->format_number($items['price'] * $items['qty']); ?></p>
							</div>
							<div class="col-md-1 col-sm-12 c-cart-remove">
								<a href="javascript:void(0);" onclick="removeCart('<?php echo $items['rowid'] ?>')"
								   class="c-theme-link c-cart-remove-desktop">×</a>
								<a href="javascript:void(0);" onclick="removeCart('<?php echo $items['rowid'] ?>')"
								   class="c-cart-remove-mobile btn c-btn c-btn-md c-btn-square c-btn-red c-btn-border-1x c-font-uppercase">Remove
									item from Cart</a>
							</div>
						</div>
					<?php endforeach; ?>
					<div class="row">
						<div class="col-md-8 col-xs-12 col-sm-12 mt-20">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="row C-Border-Box">
										<?php /* ?>
										<div class="col-xs-12 col-sm-6 col-md-6 form-group">
											<h4>Calculate Shipping & Tax</h4>
											<form method="post" name="shippingcalculationform"
												  id="shippingcalculationform" action="">
												<select name="shipping_country" id="shipping_country"
														class="form-control select2 country" tabindex="-1"
														aria-hidden="true">
													<option value="">Select Country</option>
													<?php
													foreach ($country_list as $country) {
														$selected = '';
														if (isset($cart_contents) && !empty($cart_contents)) {
															$cart_shipping_country = $cart_contents['cart_shipping_country'];
															if ($cart_shipping_country == $country['id']) {
																$selected = 'selected="selected"';
															}
														}
														?>
														<option
															value="<?php echo $country['id'] ?>" <?php echo $selected; ?>><?php echo $country['name'] ?></option>
													<?php } ?>
												</select>
												<button type="submit" id="calculate_shipping_charge"
														class="btn c-btn-square calculate-btn">Calculate
												</button>
											</form>
										</div>
										<?php */ ?>
										<!-- <div class="col-xs-12 col-sm-8 col-md-8 form-group">
											<h4>Apply Coupon Code</h4>
											<form class="form-inline" name="apply_coupon_form" id="apply_coupon_form"
												  action="javascript:void(0);">
												<div class="form-group" role="group">
													<input class="form-control" name="coupon_code" id="coupon_code"
														   placeholder="Coupon Code" type="text" maxlength="50">
													<button type="submit"
															class="btn c-btn applycoupon input-small c-btn-square calculate-btn"
															title="Add">Add
													</button>
												</div>
											</form>
										</div> -->
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-xs-12 col-sm-12">
							<div class="row">
								<div class="c-cart-subtotal-row mt-20">
									<div class="col-md-8 col-sm-6 col-xs-6 c-cart-subtotal-border">
										<h4 class="c-font-uppercase c-font-bold c-right c-font-14 c-font-grey-2">
											Subtotal</h4>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-6 c-cart-subtotal-border">
										<?php
										$cart_total = $this->cart->format_number($cart_contents['cart_total']);
										?>
										<h4 class="c-font-bold c-font-14">$<span
												id="cart_total"><?php echo $cart_total; ?></span></h4>
									</div>
								</div>
							</div>
							<!--							<div class="row">-->
							<!--								<div class="c-cart-subtotal-row mt-20">-->
							<!--									<div class="col-md-8 col-sm-6 col-xs-6 c-cart-subtotal-border">-->
							<!--										<h4 class="c-font-uppercase c-font-bold c-right c-font-14 c-font-grey-2">-->
							<!--											Shipping</h4>-->
							<!--									</div>-->
							<!--									<div class="col-md-4 col-sm-6 col-xs-6 c-cart-subtotal-border">-->
							<!--										--><?php
							//										$cart_shipping = $this->cart->format_number($cart_contents['cart_shipping']);
							//										?>
							<!--										<h4 class="c-font-bold c-font-14">$<span-->
							<!--												id="cart_shipping">-->
							<?php //echo $cart_shipping; ?><!--</span></h4>-->
							<!--									</div>-->
							<!--								</div>-->
							<!--							</div>-->
							<?php
							//							if (!empty($cart_contents['cart_tax_data'])) {
							//								$taxCartData = $cart_contents['cart_tax_data'];
							//								$tax_lable = $taxCartData['tax_label'];
							//								$tax_rate = $taxCartData['tax_rate'];
							//								$cart_tax = $this->cart->format_number(($cart_total * $tax_rate) / 100);
							//							} else {
							//								$cart_tax = '0.00';
							//							}
							$cart_tax = $this->cart->format_number($cart_contents['cart_tax']);
							?>
							<!--							<div class="row">-->
							<!--								<div class="c-cart-subtotal-row mt-20">-->
							<!--									<div class="col-md-8 col-sm-6 col-xs-6 c-cart-subtotal-border">-->
							<!--										<h4 class="c-font-uppercase c-font-bold c-right c-font-14 c-font-grey-2">-->
							<!--											Tax -->
							<?php //if (isset($tax_lable) && !empty($tax_lable)) { ?><!-- <span>-->
							<!--												(-->
							<?php //echo $tax_lable; ?><!--)</span>--><?php //} ?><!--</h4>-->
							<!--									</div>-->
							<!--									<div class="col-md-4 col-sm-6 col-xs-6 c-cart-subtotal-border">-->
							<!--										<h4 class="c-font-bold c-font-14">$<span-->
							<!--												id="cart_tax">-->
							<?php //echo $cart_tax; ?><!--</span></h4>-->
							<!--									</div>-->
							<!--								</div>-->
							<!--							</div>-->
							<?php
							$cart_discount = $this->cart->format_number($cart_contents['cart_discount']);
							?>
							<?php
							if (!empty($cart_contents['cart_coupon_data'])) { ?>
								<div class="row">
									<div class="c-cart-subtotal-row mt-20">
										<div class="col-md-8 col-sm-6 col-xs-6 c-cart-subtotal-border">
											<h4 class="c-font-uppercase c-font-bold c-right c-font-14 c-font-grey-2">
												Discount</h4>
										</div>
										<div class="col-md-4 col-sm-6 col-xs-6 c-cart-subtotal-border">
											<?php
											$cart_discount = $this->cart->format_number($cart_contents['cart_discount']);
											?>
											<h4 class="c-font-bold c-font-14">$<span
													id="cart_shipping"><?php echo $cart_discount; ?></span></h4>
										</div>
									</div>
								</div>
								<?php
							}
							?>
							<div class="row">
								<div class="c-cart-subtotal-row mt-20">
									<div class="col-md-8 col-sm-6 col-xs-6 c-cart-subtotal-border">
										<h4 class="c-font-uppercase c-font-bold c-right c-font-16 c-font-grey-2">Grand
											Total</h4>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-6 c-cart-subtotal-border">
										<?php
										$cart_grand_total = $this->cart->format_number(($cart_total + $cart_tax + $cart_shipping)-$cart_discount);
										?>
										<!-- <h4 class="c-font-bold c-font-16">$<span
												id="cart_grand_total"><?php echo $cart_grand_total; ?></span></h4> -->
										<h4 class="c-font-bold c-font-16">$<span
												id="cart_grand_total"><?php echo $this->cart->format_number($cart_contents['cart_grand_total'])?></span></h4>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="c-cart-buttons">
						<a href="<?php echo base_url('catalog/all') ?>"
						   class="btn c-btn btn-lg c-btn-red c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-l">Continue
							Shopping</a>
						<a href="<?php echo base_url('checkout') ?>"
						   class="btn c-btn btn-lg c-theme-btn c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-r">Checkout</a>
					</div>
				</div>
			<?php } else { ?>
				<div class="c-shop-cart-page-1 text-center">
					<h2 class="c-font-thin c-center">Your Shopping Cart is Empty</h2>
					<a href="<?php echo base_url('catalog/all') ?>"
					   class="btn c-btn btn-lg c-btn-dark c-btn-square c-font-white c-font-bold c-font-uppercase">Continue
						Shopping</a>
				</div>
			<?php } ?>
		</div>
	</section>
	<?php echo $footer; ?>
</div>
<script src="<?php echo base_url() ?>assets/front/js/vendor/jquery-1.12.4.min.js"></script>
<script src="<?php echo base_url() ?>assets/front/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/front/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url() ?>assets/front/js/slick.min.js"></script>
<script src="<?php echo base_url() ?>assets/front/js/swiper.min.js"></script>
<script src="<?php echo base_url() ?>assets/front/js/isotope.pkgd.min.js"></script>
<script src="<?php echo base_url() ?>assets/front/js/plugins.js"></script>
<script src="<?php echo base_url() ?>assets/front/js/main.js"></script>
<script src="<?php echo base_url() ?>assets/front/js/bootstrap_sweetalert.js"></script>
<script src="<?php echo base_url() ?>assets/front/js/jquery.bootstrap-touchspin.js"></script>
<script src="<?php echo base_url() ?>assets/front/js/front.js"></script>
<script src="<?php echo base_url() ?>assets/global/plugins/select2/js/select2.full.min.js"
		type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(".country").select2({
            placeholder: "Select Country",
            allowClear: true,
            width: '100%'
        });
    });
</script>
</body>
</html>
