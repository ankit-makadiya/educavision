<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <title>Welcome to Educavision</title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url() . $settingData['site_logo'] ?>">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/front/style.css">
        <script src="<?php echo base_url() ?>assets/front/js/vendor/modernizr-2.8.3.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/front/css/bootstrap_sweetalert.css">
    </head>
    <body>
        <div id="loading-wrap">
            <div class="loading-effect"></div>
        </div>
        <div class="wrapper">
            <?php echo $header; ?>
            <?php echo $sliderData; ?>
            <section class="features-area section-padding white-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <div class="section-title mb-70">
                                <?php echo ($pageData['is_advance'] == 1) ? $pageData['advance_description'] : $pageData['description'] ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <?php echo $footer; ?>
        </div>
        <script src="<?php echo base_url() ?>assets/front/js/vendor/jquery-1.12.4.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/jquery.validate.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/slick.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/swiper.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/isotope.pkgd.min.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/plugins.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/main.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/front.js"></script>
        <script src="<?php echo base_url() ?>assets/front/js/bootstrap_sweetalert.js"></script>
    </body>
</html>