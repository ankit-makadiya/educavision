<!--<div class="breadcrumbs-title bg-img-4 parallax overlay dark-5 blank-space">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="breadcrumbs-menu ptb-150">
                    <h1 class="l-height"><?php echo $pageTitle; ?></h1>
                    <ul class="clearfix">
                        <?php
                        foreach ($breadcumData as $breadcum) {
                            if (!empty($breadcum['link'])) {
                                ?>
                                <li><a href="<?php echo $breadcum['link'] ?>"><?php echo $breadcum['title'] ?></a> <i class="zmdi zmdi-chevron-right"></i></li>
                                <?php
                            } else {
                                ?>
                                <li><?php echo $breadcum['title'] ?></li>
                                <?php
                            }
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>-->

<div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
    <div class="container">
        <div class="c-page-title c-pull-left">
            <h3 class="c-font-uppercase c-font-sbold"><?php echo $pageTitle; ?> </h3>
            <h4 class=""></h4>
        </div>
        <ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
            <?php
            foreach ($breadcumData as $breadcum) {
                if (!empty($breadcum['link'])) {
                    ?>
                    <li><a href="<?php echo $breadcum['link'] ?>"><?php echo $breadcum['title'] ?></a></li><li>/</li>
                    <?php
                } else {
                    ?>
                    <li class="c-state_active"><?php echo $breadcum['title'] ?></li>
                        <?php
                    }
                }
                ?>
        </ul>
    </div>
</div>
