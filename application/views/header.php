<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<header class="main-header">
    <div class="top-bar theme-bg">
        <div class="container">
            <div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6">
					<ul class="clearfix top-left-ul">
						<?php
						if (isset($settingData['site_mobile']) && !empty($settingData['site_mobile'])) {
							?>
							<li>Phone: <a href="tel:<?php echo $settingData['site_mobile'] ?>"><?php echo $settingData['site_mobile'] ?></a></li>
							<?php
						}
						?>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 text-right">
					<ul class="clearfix d-inblock top-right-ul">
						<?php if ($this->session->has_userdata('educavision_front')) { ?>
							<li>
								<?php
								$userData = $this->session->userdata('educavision_front');
								$fullName = '';
								$fullName .= (!empty($userData['firstname'])) ? $userData['firstname'] : '';
								$fullName .= ' ';
								$fullName .= (!empty($userData['lastname'])) ? $userData['lastname'] : '';
								?>
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" title="User Profile">Welcome To <?php echo $fullName; ?> <i class="zmdi zmdi-chevron-down"></i> </a>
								<ul class="dropdown-menu">
									<li class="dropdown-submenu"><a class="hit" href="<?php echo base_url() ?>dashboard" title="My Dashboard">My Dashboard</a></li>
									<li class="dropdown-submenu"><a class="hit" href="<?php echo base_url() ?>edit-profile" title="Edit Profile">Edit Profile</a></li>
									<li class="dropdown-submenu"><a class="hit" href="<?php echo base_url() ?>order-history" title="Order History">Order History</a></li>
									<li class="dropdown-submenu"><a class="hit" href="<?php echo base_url() ?>change-password" title="Change Password">Change Password</a></li>
									<li class="dropdown-submenu"><a class="hit" href="<?php echo base_url() ?>logout" title="Logout?">Logout</a></li>
								</ul>
							</li>
						<?php } else { ?>
							<li><a href="<?php echo base_url() ?>login"><i class="fa fa-lock"></i> Login</a></li>
							<li><a href="<?php echo base_url() ?>registration"><i class="fa fa-lock"></i> Registration</a></li>
						<?php } ?>
						<li class="c-cart-toggler-wrapper"><a href="#" id="cart" class="c-btn-icon c-cart-toggler"><i class="fa fa-shopping-cart"></i> Cart <span class="badge"><?php echo $this->cart->total_items() ?></span></a></li>
					</ul>
					<div class="c-cart-menu">
						<?php if ($this->cart->total_items() > 0) { ?>
							<div class="c-cart-menu-title">
								<p class="c-cart-menu-float-l c-font-sbold"><?php echo $this->cart->total_items() ?> item(s)</p>
								<p class="c-cart-menu-float-r c-theme-font c-font-sbold">$ <?php echo $this->cart->total() ?></p>
							</div>
							<ul class="c-cart-menu-items">
								<?php 
                                foreach ($this->cart->contents() as $items): ?>
									<li>
										<div class="c-cart-menu-close">
											<a href="javascript:void(0);" onclick="removeCart('<?php echo $items['rowid']; ?>')" class="c-theme-link">×</a>
										</div>
                                        <?php
                                        $imageUrl = '';
                                        if(file_exists($_SERVER['DOCUMENT_ROOT'].$items['image']) && is_file($_SERVER['DOCUMENT_ROOT'].$items['image'])){
                                            $imageUrl = $items['image'];
                                        }else{
                                            $imageUrl = '/uploads/no-image.png' ;
                                        }
                                        ?>
										<img src="<?php echo $imageUrl; ?>" alt="<?php echo $items['name'] ?>">
										<div class="c-cart-menu-content">
											<p><?php echo $items['qty'] ?> x
												<span class="c-item-price c-theme-font">$ <?php echo $items['price'] ?></span>
											</p>
											<a href="<?php echo base_url() . 'book/' . $items['slug'] ?>" class="c-item-name c-font-sbold"><?php echo $items['name'] ?></a>
                                            <br>
                                            <span style="font-size: 12px;"><i><?php echo ucfirst($items['book_type']) ?></i></span>
										</div>
									</li>
								<?php endforeach; ?>
							</ul>
							<div class="c-cart-menu-footer">
								<div class="pull-left">
									<a href="<?php echo base_url() ?>shoppingcart" class="btn btn-md c-btn c-btn-square c-btn-grey-3 c-font-white c-font-bold c-center c-font-uppercase">View Cart</a>
								</div>
								<div class="pull-right">
									<a href="<?php echo base_url() ?>checkout" class="btn btn-md c-btn c-btn-square c-theme-btn c-font-white c-font-bold c-center c-font-uppercase">Checkout</a>
								</div>
							</div>
						<?php }else { ?>
							<div class="c-cart-menu-title">
								<p class="text-center"><i class="fa fa-cart-arrow-down"></i> Your cart is empty</p>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
			<?php /* ?>
			<div class="row">
                <div class="col-xs-12">
                    <div class="d-flex justify-end">
                        <div class="col-md-12 col-sm-2 col-xs-6">
                            <div class="left mr-auto">
                                <ul class="clearfix top-left-ul">
                                    <?php
                                    if (isset($settingData['site_mobile']) && !empty($settingData['site_mobile'])) {
                                        ?>
                                        <li>Phone: <a href="tel:<?php echo $settingData['site_mobile'] ?>"><?php echo $settingData['site_mobile'] ?></a></li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-2 col-xs-6">
                            <div class="middle visible-sm">
                                <div class="search-container">
                                    <form action="<?php echo base_url() ?>search" id="search_form">
                                        <input type="text" placeholder="Search.." name="q" id="q" required="">
                                        <button type="submit"><i class="fa fa-search"></i></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-2 col-xs-6">
                            <div class="right l-height">
                                <ul class="clearfix d-inblock top-right-ul">
                                    <?php if ($this->session->has_userdata('educavision_front')) { ?>
                                        <li>
                                            <?php
                                            $userData = $this->session->userdata('educavision_front');
                                            $fullName = '';
                                            $fullName .= (!empty($userData['firstname'])) ? $userData['firstname'] : '';
                                            $fullName .= ' ';
                                            $fullName .= (!empty($userData['lastname'])) ? $userData['lastname'] : '';
                                            ?>
                                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" title="User Profile">Welcome To <?php echo $fullName; ?> <i class="zmdi zmdi-chevron-down"></i> </a>
                                            <ul class="dropdown-menu">
                                                <li class="dropdown-submenu"><a class="hit" href="<?php echo base_url() ?>dashboard" title="My Dashboard">My Dashboard</a></li>
                                                <li class="dropdown-submenu"><a class="hit" href="<?php echo base_url() ?>edit-profile" title="Edit Profile">Edit Profile</a></li>
                                                <li class="dropdown-submenu"><a class="hit" href="<?php echo base_url() ?>order-history" title="Order History">Order History</a></li>
                                                <li class="dropdown-submenu"><a class="hit" href="<?php echo base_url() ?>change-password" title="Change Password">Change Password</a></li>
                                                <li class="dropdown-submenu"><a class="hit" href="<?php echo base_url() ?>logout" title="Logout?">Logout</a></li>
                                            </ul>
                                        </li>
                                    <?php } else { ?>
                                        <li><a href="<?php echo base_url() ?>login"><i class="fa fa-lock"></i> Login</a></li>
                                        <li><a href="<?php echo base_url() ?>registration"><i class="fa fa-lock"></i> Registration</a></li>
                                    <?php } ?>
                                    <li class="c-cart-toggler-wrapper"><a href="#" id="cart" class="c-btn-icon c-cart-toggler"><i class="fa fa-shopping-cart"></i> Cart <span class="badge"><?php echo $this->cart->total_items() ?></span></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="c-cart-menu">
                            <?php if ($this->cart->total_items() > 0) { ?>
                                <div class="c-cart-menu-title">
                                    <p class="c-cart-menu-float-l c-font-sbold"><?php echo $this->cart->total_items() ?> item(s)</p>
                                    <p class="c-cart-menu-float-r c-theme-font c-font-sbold">$ <?php echo $this->cart->total() ?></p>
                                </div>
                                <ul class="c-cart-menu-items">
                                    <?php foreach ($this->cart->contents() as $items): ?>
                                        <li>
                                            <div class="c-cart-menu-close">
                                                <a href="javascript:void(0);" onclick="removeCart('<?php echo $items['rowid']; ?>')" class="c-theme-link">×</a>
                                            </div>
                                            <img src="<?php echo $items['image'] ?>" alt="<?php echo $items['name'] ?>">
                                            <div class="c-cart-menu-content">
                                                <p><?php echo $items['qty'] ?> x
                                                    <span class="c-item-price c-theme-font">$ <?php echo $items['price'] ?></span>
                                                </p>
                                                <a href="<?php echo base_url() . 'book/' . $items['slug'] ?>" class="c-item-name c-font-sbold"><?php echo $items['name'] ?></a>
                                            </div>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                                <div class="c-cart-menu-footer">
                                    <div class="pull-left">
                                        <a href="<?php echo base_url() ?>shoppingcart" class="btn btn-md c-btn c-btn-square c-btn-grey-3 c-font-white c-font-bold c-center c-font-uppercase">View Cart</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="<?php echo base_url() ?>checkout" class="btn btn-md c-btn c-btn-square c-theme-btn c-font-white c-font-bold c-center c-font-uppercase">Checkout</a>
                                    </div>
                                </div>
                            <?php }else { ?>
                                <div class="c-cart-menu-title">
                                    <p class="text-center"><i class="fa fa-cart-arrow-down"></i> Your cart is empty</p>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
			<?php */ ?>
        </div>
    </div>
    <div id="active-sticky" class="navgation-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="d-flex justify-end">
                        <!-- <div class="row"> -->
                            <div class="logo">
                                    <?php
                                    if (isset($settingData['site_logo']) && !empty($settingData['site_logo'])) {
                                        ?>
                                        <a href="<?php echo base_url() ?>" title="<?php echo $settingData['site_title'] ?>"><img src="<?php echo base_url($settingData['site_logo']) ?>" alt="<?php echo $settingData['site_title'] ?>" width="200" /></a>
                                        <?php
                                    }
                                    ?>
                            </div>
                            <!--</div>-->
                            <!--<div class="col-md-8 col-sm-8 col-xs-6">-->
                                <nav class="mainmenu">
                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                    </div>
                                    <div id="navbar" class="navbar-collapse collapse no-padding">
                                        <ul class="navbar-nav dropdown">
                                            <?php
                                            if (isset($menuItem) && !empty($menuItem)) {
                                                foreach ($menuItem as $menu) {
                                                    $chevron_down = (!empty($menu['children'])) ? '<i class="zmdi zmdi-chevron-down"></i>' : '';
                                                    $dropdown_toggle = (!empty($menu['children'])) ? ' class="dropdown-toggle" data-toggle="dropdown" ' : ' ';
                                                    ?>
                                                    <li>
                                                        <?php if ($menu['table'] == 'custom_link') {
                                                            ?>

                                                            <a href="<?php
                                                            if ($menu['menu_url'] == 'http://' || $menu['menu_url'] == 'https://' || $menu['menu_url'] == '') {
                                                                echo 'javascript:void(0)';
                                                            } else {
                                                                echo $menu['menu_url'];
                                                            }
                                                            ?>" 
                                                               <?php
                                                               if (isset($menu['menu_target']) && !empty($menu['menu_target'])) {
                                                                   echo 'target="' . $menu['menu_target'] . '"';
                                                               }
                                                               ?><?php echo $dropdown_toggle; ?>title="<?php echo $menu['menu_name']; ?>"><?php echo $menu['menu_name'] . ' ' . $chevron_down; ?> </a>
                                                               <?php include 'recursive-menu.php'; ?>
                                                               <?php
                                                           } elseif ($menu['table'] == 'cms_page') {
                                                               if (in_array($menu['table_id'], $CommonAvailableData['PageData']['pageId'])) {
                                                                   ?>
                                                                <a href="<?php echo base_url('page/' . $CommonAvailableData['PageData']['pageSlug'][$menu['table_id']]) ?>" 
                                                                   <?php echo $dropdown_toggle; ?>title="<?php echo $menu['navigation_label']; ?>"><?php echo $menu['navigation_label'] . ' ' . $chevron_down; ?> </a>
                                                                   <?php include 'recursive-menu.php'; ?>
                                                                   <?php
                                                               }
                                                           } elseif ($menu['table'] == 'category') {
                                                               if (in_array($menu['table_id'], $CommonAvailableData['CategoryData']['categoryId'])) {
                                                                   ?>
                                                                <a href="<?php echo base_url('catalog/' . $CommonAvailableData['CategoryData']['categorySlug'][$menu['table_id']]) ?>" 
                                                                   <?php echo $dropdown_toggle; ?>title="<?php echo $menu['navigation_label']; ?>"><?php echo $menu['navigation_label'] . ' ' . $chevron_down; ?> </a>                                                           
                                                                   <?php include 'recursive-menu.php'; ?>
                                                                   <?php
                                                               }
                                                           }
                                                           ?>
                                                    </li>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </nav>
                            <!--</div>-->
                            <!--<div class="col-md-2 col-sm-2 col-xs-12 hidden-sm">-->
                                <div class="search-container hidden-sm">
                                    <form action="<?php echo base_url() ?>search" id="search_form">
                                        <input type="text" placeholder="Search.." name="q" id="q" required="">
                                        <button type="submit"><i class="fa fa-search"></i></button>
                                    </form>
                                </div>
                            <!--</div>-->
                        <!-- </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
