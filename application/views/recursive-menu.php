<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php
if (isset($menu['children'])) {
    $menuItem = $menu['children'];
    if (isset($menuItem) && !empty($menuItem)) {
        ?>
        <ul class="dropdown-menu">
            <?php
            foreach ($menuItem as $menu) {
                $chevron_down = (!empty($menu['children'])) ? '<i class="zmdi zmdi-chevron-right"></i>' : '';
                $dropdown_submenu = (!empty($menu['children'])) ? ' class="dropdown-submenu"' : '';
                $hit = (!empty($menu['children'])) ? ' class="hit" ' : ' ';
                if ($menu['table'] == 'custom_link') {
                    ?>
                    <li<?php echo $dropdown_submenu; ?>>
                        <a<?php echo $hit; ?>href="<?php
                            if ($menu['menu_url'] == 'http://' || $menu['menu_url'] == 'https://' || $menu['menu_url'] == '') {
                                echo 'javascript:void(0)';
                            } else {
                                echo $menu['menu_url'];
                            }
                            ?>" 
                                <?php
                                if (isset($menu['menu_target']) && !empty($menu['menu_target'])) {
                                    echo 'target="' . $menu['menu_target'] . '"';
                                }
                                ?> title="<?php echo $menu['menu_name']; ?>"><?php echo $menu['menu_name'] . ' ' . $chevron_down; ?> </a>
                            <?php include 'recursive-menu.php'; ?>
                    </li>
                    <?php
                } elseif ($menu['table'] == 'cms_page') {
                    if (in_array($menu['table_id'], $CommonAvailableData['PageData']['pageId'])) {
                        ?>
                        <li <?php echo $dropdown_submenu; ?>><a <?php echo $hit; ?> href="<?php echo base_url('page/' . $CommonAvailableData['PageData']['pageSlug'][$menu['table_id']]) ?>" 
                                                                                    title="<?php echo $menu['navigation_label']; ?>"><?php echo $menu['navigation_label'] . ' ' . $chevron_down; ?> </a>
                                                                                    <?php include 'recursive-menu.php'; ?>
                        </li>
                        <?php
                    }
                } elseif ($menu['table'] == 'category') {
                    if (in_array($menu['table_id'], $CommonAvailableData['CategoryData']['categoryId'])) {
                        ?>
                        <li <?php echo $dropdown_submenu; ?>><a <?php echo $hit; ?> href="<?php echo base_url('catalog/' . $CommonAvailableData['CategoryData']['categorySlug'][$menu['table_id']]) ?>" 
                                                                                    title="<?php echo $menu['navigation_label']; ?>"><?php echo $menu['navigation_label'] . ' ' . $chevron_down; ?> </a>                                                           <?php include 'recursive-menu.php'; ?>
                        </li>
                        <?php
                    }
                }
                ?>
            <?php }
            ?>
        </ul>
        <?php
    }
}
?>