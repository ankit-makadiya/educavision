<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="cta-one gradient-1">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="cta-content theme-bg clearfix">
                    <div class="pull-left">
                        <h2 class="white-color">Learn Online</h2>
                        <p>Access to E-learning Courses</p>
                    </div>
                    <div class="pull-right pt-10">
                        <a class="btn black" href="http://www.educavisiononline.com/" target="_blank">Get Started</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<footer class="section-pt dark-bg">
    <div class="footer-widget-area pb-40">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3 mobi-mb-50 tab-mb-50">
                    <div class="footer-widget address">
                        <div class="title mb-40">
                            <?php
                            if (isset($settingData['site_logo']) && !empty($settingData['site_logo'])) {
                                ?>
                                <a href="<?php echo base_url() ?>" title="<?php echo $settingData['site_title'] ?>"><img src="<?php echo $settingData['site_logo'] ?>" alt="<?php echo $settingData['site_title'] ?>" width="100" /></a>
                                <?php
                            }
                            ?>
                        </div>
                        <ul>
                            <?php
                            if (isset($settingData['site_mobile']) && !empty($settingData['site_mobile'])) {
                                ?>
                                <li><span>Phone:</span> <a href="tel:<?php echo $settingData['site_mobile'] ?>"><?php echo $settingData['site_mobile'] ?></a></li>
                                <?php
                            }
                            if (isset($settingData['site_email']) && !empty($settingData['site_email'])) {
                                ?>
                                <li><span>Email:</span> <a href="mailto:<?php echo $settingData['site_email'] ?>"><?php echo $settingData['site_email'] ?></a></li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                </div>
                <!-- /.Widget end -->
                <div class="col-xs-12 col-sm-6 col-md-3 mobi-mb-50 tab-mb-50">
                    <div class="footer-widget links">
                        <div class="title mb-40">
                            <h4 class="text-capitalize">User <span class="theme-color">Links</span></h4>
                            <hr class="line" />
                        </div>
                        <?php
                        if (isset($menuItem) && !empty($menuItem)) {
                            foreach ($menuItem as $mkey => $menu) {
                                if ($mkey == 0 || $mkey == 5) {
                                    ?>
                                    <ul class="pull-left">
                                        <?php
                                    }
                                    ?>
                                    <li>
                                        <?php if ($menu['table'] == 'custom_link') {
                                            ?>
                                            <a href="<?php
                                            if ($menu['menu_url'] == 'http://' || $menu['menu_url'] == 'https://' || $menu['menu_url'] == '') {
                                                echo 'javascript:void(0)';
                                            } else {
                                                echo $menu['menu_url'];
                                            }
                                            ?>" 
                                               <?php
                                               if (isset($menu['menu_target']) && !empty($menu['menu_target'])) {
                                                   echo 'target="' . $menu['menu_target'] . '"';
                                               }
                                               ?>title="<?php echo $menu['menu_name']; ?>"><?php echo $menu['menu_name']; ?> </a>
                                               <?php
                                           } elseif ($menu['table'] == 'cms_page') {
                                               if (in_array($menu['table_id'], $CommonAvailableData['PageData']['pageId'])) {
                                                   ?>
                                                <a href="<?php echo base_url('page/' . $CommonAvailableData['PageData']['pageSlug'][$menu['table_id']]) ?>" 
                                                   title="<?php echo $menu['navigation_label']; ?>"><?php echo $menu['navigation_label'] ?> </a>
                                                   <?php
                                               }
                                           } elseif ($menu['table'] == 'category') {
                                               if (in_array($menu['table_id'], $CommonAvailableData['CategoryData']['categoryId'])) {
                                                   ?>
                                                <a href="<?php echo base_url('catalog/' . $CommonAvailableData['CategoryData']['categorySlug'][$menu['table_id']]) ?>" 
                                                   title="<?php echo $menu['navigation_label']; ?>"><?php echo $menu['navigation_label']; ?> </a>              <?php
                                               }
                                           }
                                           ?>
                                    </li>
                                    <?php
                                    if ($mkey == 4) {
                                        ?>
                                    </ul>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </div>
                </div>
                <!-- /.Widget end -->
                <div class="col-xs-12 col-sm-6 col-md-3 mobi-mb-50">
                    <div class="footer-widget recent-course">
                        <div class="title mb-40">
                            <h4 class="text-capitalize">Recent <span class="theme-color">Books</span></h4>
                            <hr class="line" />
                        </div>
                        <ul>
                            <?php foreach ($recentBook as $book) { 
                                $imageUrl = '';
                                if(file_exists($_SERVER['DOCUMENT_ROOT'].$book['image']) && is_file($_SERVER['DOCUMENT_ROOT'].$book['image'])){
                                    $imageUrl = $book['image'];
                                }else{
                                    $imageUrl = '/uploads/no-image.png' ;
                                }
                                ?>
                                <li>
                                    <a class="pull-left" href="<?php echo base_url('book/' . $book['slug']) ?>" title="<?php echo $book['name']; ?>">
                                        <img src="<?php echo $imageUrl ?>" alt="<?php echo $book['name'] ?>" width="80px" />
                                    </a>
                                    <div class="headline pl-15">
                                        <a href="<?php echo base_url('book/' . $book['slug']) ?>">
                                            <h5 class="mb-5 text-capitalize"><?php echo (strlen($book['name']) > 15) ? substr($book['name'], 0,15).'...' : $book['name']; ?></h5>
                                        </a>
                                        <h6 class="no-margin"><?php echo $book['author_name'] ?></h6>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <!-- /.Widget end -->
                <div class="col-xs-12 col-sm-6 col-md-3 mobi-mb-50 tab-mb-50">
                    <div class="footer-widget links">
                        <div class="title mb-40">
                            <h4 class="text-capitalize">Our <span class="theme-color">Category</span></h4>
                            <hr class="line" />
                        </div>
                        <?php
                        foreach ($parentCategoryData as $catKey => $category) {
                            if ($catKey == 0 || $catKey == 5) {
                                ?>
                                <ul class="pull-left">
                                <?php } ?>
                                <li><a href="<?php echo base_url() . 'catalog/' . $category['slug'] ?>" title="<?php echo $category['name'] ?>"><?php echo $category['name'] ?></a></li>
                                <?php
							    if ($catKey == 4 || (count($parentCategoryData)-1) == $catKey) {
                                    ?>
                                </ul>
                                <?php
                            }
                        }
                        ?>
                    </div>
                </div>
                <!-- /.Widget end -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.Widget Area End -->
    <div class="divide">
        <div class="container">
            <hr class="line" />
        </div>
    </div>
    <!-- /.Divede Line -->
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="left">
                        <p>Copyright &copy; <?php echo date('Y'); ?> <a href="<?php echo base_url() ?>"><?php echo $settingData['site_title'] ?></a>.</p>
                    </div>
                    <!-- Change your Copyright -->
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="social-icon text-right">
                        <ul class="clearfix d-inblock">
                            <li><a href="javascript:void(0);" target="_blank"><i class="zmdi zmdi-facebook"></i></a></li>
                            <li><a href="javascript:void(0);" target="_blank"><i class="zmdi zmdi-twitter"></i></a></li>
                            <li><a href="javascript:void(0);" target="_blank"><i class="zmdi zmdi-pinterest"></i></a></li>
                            <li><a href="javascript:void(0);" target="_blank"><i class="zmdi zmdi-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<script>
    var base_url = '<?php echo base_url() ?>';
</script>

<!-- Login  -->
<div class="modal fade login-model login" id="login" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="modal-close" data-dismiss="modal">&times;</button>     	
            <div class="modal-body">
                <div class="clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-text-center">
                        <h3 class="mb-35">Login with your site account</h3>
                        <form class="custom-input" action="#" id="login_form" name="login_form">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="email" name="email" placeholder="User Email">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="password" name="password" placeholder="Password">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="submit" name="submit" value="Sign In">
                                </div>
                                <div class="col-md-6">
                                    <div class="clearfix">
                                        <a class="pull-right" href="javascript:void(0);" data-toggle="modal" data-target="#forgotPassword">Forget Password?</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- forgot password -->
<div class="modal fade login-model forgotPassword" id="forgotPassword" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="modal-close" data-dismiss="modal">&times;</button>     	
            <div class="modal-body">
                <div class="clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-text-center">
                        <h3 class="mb-35">Forgot Password</h3>
                        <p>We do not share your personal details with anyone.</p>
                        <form class="custom-input" action="#" id="forgot_form" name="forgot_form">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="email" name="email" placeholder="User Email" id="forgot_email">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="submit" name="submit" value="Recover Password">
                                </div>
                                <div class="col-md-6">
                                    <div class="clearfix">
                                        <a class="pull-right" href="javascript:void(0);" data-toggle="modal" data-target="#login">Forget Password?</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- forgot password -->
