<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo $module_name; ?>
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('admin/dashboard'); ?>">
                    <i class="fa fa-dashboard"></i>
                    Home
                </a>
            </li>
            <li class="active"><?php echo $module_name; ?></li>
        </ol>
    </section>
    <section class="content">
        <div class="row" >
            <div class="col-xs-12" >
                <?php if ($this->session->flashdata('success')) { ?>
                    <div class="callout callout-success">
                        <p><?php echo $this->session->flashdata('success'); ?></p>
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('error')) { ?>  
                    <div class="callout callout-danger">
                        <p><?php echo $this->session->flashdata('error'); ?></p>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Manage User Role</h3>
                        <?php if(in_array('role-create', $admin_permission)){ ?>
                        <div class="pull-right">
                            <a href="<?php echo base_url('admin/role/add'); ?>" class="btn btn-primary pull-right">Add Role</a>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No.</th>
									<th>Name</th>
									<th>Created Date</th>
									<th>Modify Date</th>
									<th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($role_list as $role) { ?>
                                    <tr>
                                        <td><?php echo $role['id'] ?></td>
                                        <td><?php echo $role['name'] ?></td>
										<td><?php echo $role['created_date'] ?></td>
                                        <td><?php echo $role['modify_date'] ?></td>
                                        <td>
                                            <?php if(in_array('role-edit', $admin_permission)){ ?>
                                            <a href="<?php echo base_url('admin/role/edit/' . $role['id']) ?>" id="edit_role_btn" title="Edit Role">
                                                <button type="button" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i></button>
                                            </a>
                                            <?php } ?>
<!--											<a href="--><?php //echo base_url('admin/role/delete/' . $role['id']); ?><!--" id="delete_btn" title="Delete Page" onclick="if (confirm('Are you sure delete this item?')) {-->
<!--                                                        return true;-->
<!--                                                    } else {-->
<!--                                                        event.stopPropagation();-->
<!--                                                        event.preventDefault();-->
<!--                                                    }-->
<!--                                                    ;">-->
<!--                                                <button type="button" class="btn btn-primary" style="margin-top: 3px;"><i class="fa fa-trash-o"></i></button>-->
<!--                                            </a>-->
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Country</h4>
            </div>
            <div class="modal-body">
                <?php echo form_open('country/edit', array('name' => 'country_frm_user', 'id' => 'country_frm', 'method' => 'POST')); ?>
                <input type="hidden" class="form-control" name="country_id" id="country_id" value="">
                <div class="row">
                    <div class="form-group col-sm-10">
                        <label for="inputEmail3" name="country" id="country">Country Name</label>
                        <input type="text" class="form-control" name="country_name" id="country_name" class="country_name" value="">
                    </div>
                    <div class="col-sm-3   ">
                        <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">send</button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
<script type="text/javascript">
    function getCountryname($id)
    {
        var country_id = $id;
        $.ajax({
            url: "<?php echo base_url('country/getCatName'); ?>",
            type: "POST",
            data: {'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>', 'country_id': country_id},
            success: function (data) {
                console.log(data['country_name']);
                $("#country_name").val(data['country_name']);
                $("#country_id").val(country_id);
            }
        });
    }
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#country_frm").validate({
            rules: {
                country_name: {
                    required: true,
                }
            },
            messages:
                    {
                        country_name: {
                            required: "Country name is required",
                        }
                    },
        });

    });

</script>
<script language="javascript" type="text/javascript">
    $(document).ready(function () {
        $('.callout-danger').delay(3000).hide('700');
        $('.callout-success').delay(3000).hide('700');
    });
</script>
