<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo $module_name; ?>
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('dashboard'); ?>">
                    <i class="fa fa-dashboard"></i>
                    Home
                </a>
            </li>
            <li class="active"><?php echo $module_name; ?></li>
        </ol>
    </section>
    <section class="content-header">
        <?php if ($this->session->flashdata('success')) { ?>
            <div class="callout callout-success">
                <p><?php echo $this->session->flashdata('success'); ?></p>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>  
            <div class="callout callout-danger" >
                <p><?php echo $this->session->flashdata('error'); ?></p>
            </div>
        <?php } ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo $section_title; ?></h3>
                    </div>
                    <?php
                    $form_attr = array('id' => 'edit_role_frm', 'enctype' => 'multipart/form-data');
                    echo form_open_multipart('admin/role/edit/'.$role_detail[0]['id'], $form_attr);
                    ?>
                    <input type="hidden" name="id" value="<?php echo $role_detail[0]['id'] ?>">
                    <div class="box-body">
                        <div class="form-group col-sm-10">
                            <label for="inputEmail3" name="name" id="name" class="col-sm-2 control-label">Role Title</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name" id="name" placeholder="Role Title" value="<?php echo $role_detail[0]['name'] ?>">
                            </div>
                        </div>
						<div class="form-group col-sm-10">
							<label for="lable_permissions"  class="col-sm-2 control-label">Assign Permission</label>
							<div class="col-sm-10">
								<a href="javascript:void(0);" class="role_select_all pull-left">Select All</a> <a href="javascript:void(0);" class="role_deselect_all pull-right">Unselect All</a>
								<?php $selected_permissions = explode(',', $role_detail[0]['permissions']); ?>
								<select class="form-control permissions select2" multiple="multiple" name="permissions[]" id="permissions" style="width: 100%;" tabindex="-1" required="" >
									<?php foreach ($permissions_data as $permissions) { ?>
										<option value="<?php echo $permissions['slug'] ?>" <?php
										if (in_array($permissions['slug'], $selected_permissions)) {
											echo 'selected="selected"';
										}
										?>>
											<?php echo $permissions['name'] ?>
										</option>
									<?php } ?>
								</select>
							</div>
						</div>
					</div>
                    <div class="box-footer">
                        <?php
                        $save_attr = array('id' => 'btn_save', 'name' => 'btn_save', 'value' => 'Save', 'class' => 'btn btn-primary');
                        echo form_submit($save_attr);
                        ?>    
                        <button type="button" onclick="window.history.back();" class="btn btn-default">Back</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    //validation for edit email formate form
    $(document).ready(function () {
        $('.bootstrap3-editor').wysihtml5();

		$(".permissions").select2({
			placeholder: "Select Permissions",
			allowClear: true,
			width: '100%'
		});

		$('.role_select_all').on('click',function(){
			$('.permissions').select2('destroy').find('option').prop('selected', 'selected').end().select2();
		});

		$('.role_deselect_all').on('click',function(){
			$('.permissions').select2('destroy').find('option').prop('selected', false).end().select2();
		});

        $("#edit_role_frm").validate({
            rules: {
                name: {
                    required: true,
                },
                parent_id: {
                    required: true,
                }
            },
            messages:
                    {
                        name: {
                            required: "Please enter role name.",
                        },
                        role_id: {
                            required: "Please select parent role.",
                        },
                    },
        });
    });

</script>
