<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo $module_name; ?>
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('dashboard'); ?>">
                    <i class="fa fa-dashboard"></i>
                    Home
                </a>
            </li>
            <li class="active"><?php echo $module_name; ?></li>
        </ol>
    </section>
    <section class="content-header">
        <?php if ($this->session->flashdata('success')) { ?>
            <div class="callout callout-success">
                <p><?php echo $this->session->flashdata('success'); ?></p>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>  
            <div class="callout callout-danger" >
                <p><?php echo $this->session->flashdata('error'); ?></p>
            </div>
        <?php } ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo $section_title; ?></h3>
                    </div>
                    <?php
                    $form_attr = array('id' => 'edit_menu_frm', 'enctype' => 'multipart/form-data');
                    echo form_open_multipart('admin/menu/edit', $form_attr);
                    ?>
                    <input type="hidden" name="menu_id" value="<?php echo $menu_detail[0]['id'] ?>">
                    <div class="box-body">
                        <div class="portlet-body">
                            <div class="">
                                <div class="col-md-4">
                                    <div class="box box-success box-solid blue">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Pages</h3>
                                            <div class="box-tools pull-right">
                                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="box-body" style="height: 200px; overflow: scroll;">
                                            <div id="page-data"></div>
                                            <form name="page-form" id="page-form">
                                                <div class="form-group">
                                                    <div class="checkbox-list">
                                                        <div id="page_form_html"></div>
                                                        <?php foreach ($page_data as $page) { ?>
                                                            <div class="checker"><label><span><input type="checkbox" name="pages" value="cms_page*<?php echo $page['id'] ?>*<?php echo $page['name'] ?>"></span><?php echo $page['name'] ?></label></div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <button id="page-submit" type="button" class="btn blue add-to-menu">Add to Menu</button>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="box box-success box-solid blue">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Categories</h3>
                                            <div class="box-tools pull-right">
                                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="box-body" style="height: 200px; overflow: scroll;">
                                            <form name="category-form" id="category-form">
                                                <div class="form-group">
                                                    <div class="checkbox-list">
                                                        <div id="category_form_html"></div>
                                                        <?php echo $category_html; ?>
                                                    </div>
                                                </div>
                                                <button type="button" id="category-submit" class="btn blue add-to-menu">Add to Menu</button>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="box box-success box-solid blue">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Custom Links</h3>
                                            <div class="box-tools pull-right">
                                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="box-body" style="height: 250px;">
                                            <form name="custom-form" id="custom-form">
                                                <div class="form-group">
                                                    <div class="input-icon">
                                                        <i class="fa fa-text-width"></i>
                                                        <input type="text" id="custom-menu" class="form-control" placeholder="Menu Name"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="input-icon">
                                                        <i class="fa fa-html5"></i>
                                                        <input type="url" id="custom-url" class="form-control" placeholder="Menu Url" value="http://"/>
                                                    </div>
                                                </div>  
                                                <div class="form-group">
                                                    <select id="custom-target" class="form-control">
                                                        <option value="_self">Same Window</option>
                                                        <option value="_blank">New Window</option>
                                                    </select>
                                                </div>
                                                <button type="button" id="custom-submit" class="btn blue add-to-menu">Add to Menu</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="General">
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="english">
                                                    <?php
                                                    $form_attr = array('id' => 'menu_add_edit_form', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data');
                                                    echo form_open_multipart('admin/menu/index', $form_attr);
                                                    ?>
                                                    <input type="hidden" name="menu_id" id="menu_id" value="1"/>
                                                    <input type="hidden" name="commasapareted_images_name" id="commasapareted_images_name" >
                                                    <div class="form-body mar_t45">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Name <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-8">
                                                                <input type="text" class="form-control" name="name" id="name" placeholder="Menu Name" data-error-container="#editor2_error" data-required="1" value="<?php echo $menu_detail[0]['name'] ?>" readonly="readonly">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Menu Item<span class="required"> * </span></label>
                                                            <div class="col-md-9">
                                                                <div class="portlet box blue">
                                                                    <div class="portlet-body">
                                                                        <div class="dd" id="nestable_list_1">
                                                                            <ol class="dd-list">
                                                                                <!--all listing menu-->
                                                                            </ol>
                                                                        </div>
                                                                        <input type="hidden" name="menuitem_validate" id="menuitem_validate" value="" />
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="menuitem" id="nestable_list_1_output" value="" />
                                                        </div>
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-2 col-md-8">
                                                                    <?php
                                                                    $save_attr = array('id' => 'btn_save', 'title' => 'Save Menu Detail', 'name' => 'btn_save', 'value' => 'Save', 'class' => 'btn btn-primary');
                                                                    echo form_submit($save_attr);
                                                                    ?> 
                                                                    <button type="button" onclick="window.history.back();" class="btn btn-default">Back</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php echo form_close() ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="modal fade" id="menu_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="width:60% !important; height: 60% !important;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Edit Menu</h4>
            </div>
            <form name="editMenu-Form" id="editMenu-Form">
                <div class="modal-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Navigation Label</label>
                            <input class="form-control" type="text" id="navigation_label" data-tabindex="1" value="" required="">
                        </div>
                        <div class="form-group">
                            <label>Original Name</label>
                            <input class="form-control" type="text" id="original_name" data-tabindex="2" value="" readonly="">
                        </div>
                    </div>
                    <input class="form-control" type="hidden" id="data_id" data-tabindex="3" value="">
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" name="submit" class="btn btn-primary" id="save-edit-menu" data-dismiss="modal">Save changes</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="custom_menu_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
    <div class="modal-dialog" style="width:60% !important; height: 30% !important;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Edit Menu</h4>
            </div>
            <form name="editCustomMenu-Form" id="editCustomMenu-Form">
                <div class="modal-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Menu Name</label>
                            <input class="form-control" type="text" id="menu_name" data-tabindex="1" value="" required="">
                        </div>
                        <div class="form-group">
                            <label>Menu Link</label>
                            <input class="form-control" type="text" id="menu_link" data-tabindex="2" value="http://" required="">
                        </div>
                        <div class="form-group">
                            <label>Menu Target</label>
                            <select id="menu_target" name="custom-target" class="form-control">
                                <option value="_self">Same Window</option>
                                <option value="_blank">New Window</option>
                            </select>
                        </div>
                    </div>
                    <input class="form-control" type="hidden" id="data_id" data-tabindex="3" value="">
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" name="submit" class="btn btn-primary" id="save-custom-edit-menu" data-dismiss="modal">Save changes</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script src="<?php echo base_url() ?>assets/global/plugins/jquery-nestable/jquery.nestable.js" type="text/javascript"></script>
<script type="text/javascript">
                                                                        //validation for edit email formate form
                                                                        $(document).ready(function () {
                                                                            $('.bootstrap3-editor').wysihtml5();

                                                                            $('.callout-danger').delay(3000).hide('700');
                                                                            $('.callout-success').delay(3000).hide('700');

                                                                            var site_url = site_url_menu = $(location).attr('href');
                                                                            var output = site_url.split(/[/]+/).pop();
                                                                        });
                                                                        /*Nestable List Start*/
                                                                        $(window).load(function () {
                                                                            var menu_id = $('#menu_id').val();
                                                                            $.ajax({
                                                                                url: '/admin/menu/getmenuitem',
                                                                                type: 'post',
                                                                                data: {
                                                                                    'menu_id': menu_id,
                                                                                },
                                                                                dataType: 'html',
                                                                                success: function (result) {
                                                                                    if (result) {
                                                                                        $('#nestable_list_1 .dd-list').html(result);
                                                                                        updateOutput($('#nestable_list_1').data('output', $('#nestable_list_1_output')));
                                                                                        $('#menuitem_validate').val(result);
                                                                                    }
                                                                                },
                                                                                error: function (jqXHR, textStatus, errorThrown) {
                                                                                    return false;
                                                                                }
                                                                            });
                                                                        });


                                                                        var updateOutput = function (e) {
                                                                            var list = e.length ? e : $(e.target),
                                                                                    output = list.data('output');
                                                                            if (window.JSON) {
                                                                                output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
                                                                            } else {
                                                                                output.val('JSON browser support required for this demo.');
                                                                            }
                                                                        };

                                                                        $('#nestable_list_1').nestable({group: 1}).on('change', updateOutput);
                                                                        updateOutput($('#nestable_list_1').data('output', $('#nestable_list_1_output')));
                                                                        $('#nestable_list_menu').on('click', function (e) {
                                                                            var target = $(e.target),
                                                                                    action = target.data('action');
                                                                            if (action === 'expand-all') {
                                                                                $('.dd').nestable('expandAll');
                                                                            }
                                                                            if (action === 'collapse-all') {
                                                                                $('.dd').nestable('collapseAll');
                                                                            }
                                                                        });

                                                                        $('#page-submit').on('click', function () {
                                                                            $('input[name="pages"]:checked').each(function () {
                                                                                var li_length = maxDataId() + 1;
                                                                                var selected_val = this.value;
                                                                                var result = selected_val.split('*');
                                                                                var nested_li = '<li class="dd-item" data-id="' + li_length + '" data-table="' + result[0] + '" data-table_id="' + result[1] + '" data-original_name="' + result[2] + '" data-navigation_label="' + result[2] + '" data-menu_image=""><div class="dd-handle"><span class="menu_name"><b>' + result[2] + '</b></span><span class="pull-right">Page<span class="li_edit"> | <a href="javascript:void(0);" onclick="editMenu(' + li_length + ')"><i class="fa fa-pencil"></i> </a><a href="javascript:void(0);" onclick="itemDelete(' + li_length + ');"><i class="fa fa-trash"></i></a></span></span></div></li>';
                                                                                $('#nestable_list_1 .dd-list:first').append(nested_li);
                                                                                $('#menuitem_validate').val(result);
                                                                            });
                                                                            updateOutput($('#nestable_list_1').data('output', $('#nestable_list_1_output')));
                                                                            $('input[name="pages"]:checkbox').prop('checked', false);
                                                                        });
                                                                        $('#category-submit').on('click', function () {
                                                                            var li_length = maxDataId() + 1;
                                                                            var selectedCat = [];
                                                                            $('input[name="category"]:checked').each(function () {
                                                                                var cat_id = $(this).attr('id');
                                                                                selectedCat.push(cat_id);
                                                                            });
                                                                            var all_selected_cat = selectedCat.join(',');

                                                                            $.ajax({
                                                                                url: '/admin/menu/getcategorymenu',
                                                                                type: 'post',
                                                                                data: {
                                                                                    'category_ids': all_selected_cat,
                                                                                    'max_data_id': li_length,
                                                                                },
                                                                                dataType: 'html',
                                                                                success: function (result) {
                                                                                    if (result) {
                                                                                        $('#nestable_list_1 .dd-list:first').append(result);
                                                                                        updateOutput($('#nestable_list_1').data('output', $('#nestable_list_1_output')));
                                                                                        $('#menuitem_validate').val(result);
                                                                                    }

                                                                                },
                                                                                error: function (jqXHR, textStatus, errorThrown) {
                                                                                    return false;
                                                                                }
                                                                            });


                                                                            $('input[name="category"]:checkbox').prop('checked', false);
                                                                        });
                                                                        $('#custom-submit').on('click', function () {
                                                                            var li_length = maxDataId() + 1;
                                                                            var custom_url = $('#custom-url').val();
                                                                            var custom_menu = $('#custom-menu').val();
                                                                            var custom_icon = "";
                                                                            if ($('#custom-icon1').val() != undefined) {
                                                                                custom_icon = $('#custom-icon1').val();
                                                                            }
                                                                            var custom_icon_position = $('#custom-icon-position').val();
                                                                            var custom_target = $("#custom-target").val();
                                                                            if (custom_menu != '') {
                                                                                var nested_li = '<li class="dd-item" data-id="' + li_length + '" data-table="custom_link" data-menu_name="' + custom_menu + '" data-menu_url="' + custom_url + '" data-menu_icon="' + custom_icon + '" data-menu_icon_position="' + custom_icon_position + '" data-menu_target="' + custom_target + '"><div class="dd-handle"><span class="menu_name"><b>' + custom_menu + '</b></span><span class="pull-right">Custom Link<span class="li_edit"> | <a href="javascript:void(0);" onclick="editCustomMenu(' + li_length + ')"><i class="fa fa-pencil"></i> </a><a href="javascript:void(0);" onclick="itemDelete(' + li_length + ');"><i class="fa fa-trash"></i></a></span></span></div></li>';
                                                                                $('#nestable_list_1 .dd-list:first').append(nested_li);
                                                                                updateOutput($('#nestable_list_1').data('output', $('#nestable_list_1_output')));
                                                                                $('#menuitem_validate').val(custom_menu);
                                                                            }
                                                                            $('#custom-url').val('http://');
                                                                            $('#custom-menu').val('');
                                                                        });

                                                                        function maxDataId() {
                                                                            var dataList = $("li.dd-item").map(function () {
                                                                                return parseInt($(this).attr("data-id"));
                                                                            }).get();
                                                                            var max_data_id = Math.max.apply(null, dataList);
                                                                            if (max_data_id == '-Infinity') {
                                                                                return parseInt(0);
                                                                            } else {
                                                                                return max_data_id;
                                                                            }
                                                                        }
                                                                        function editMenu(id) {
                                                                            var data_table = $("[data-id='" + id + "']").data('table');
                                                                            var data_table_id = $("[data-id='" + id + "']").data('table_id');
                                                                            var data_original_name = $("[data-id='" + id + "']").data('original_name');
                                                                            var data_navigation_label = $('[data-id="' + id + '"]').data("navigation_label");
                                                                            var data_menu_type = $('[data-id="' + id + '"]').data("menu_type");
                                                                            var data_menu_sub_type = $('[data-id="' + id + '"]').data("menu_sub_type");
                                                                            var data_menu_description = $('[data-id="' + id + '"]').data("menu_description");
                                                                            var data_menu_icon = "";
                                                                            if ($('[data-id="' + id + '"]').data("menu_icon") != undefined) {
                                                                                data_menu_icon = $('[data-id="' + id + '"]').data("menu_icon");
                                                                            }
                                                                            var data_menu_icon_position = $('[data-id="' + id + '"]').data("menu_icon_position");
                                                                            var data_menu_image = $('[data-id="' + id + '"]').data("menu_image");

                                                                            var data_special_main_title = $('[data-id="' + id + '"]').data("special_main_title");
                                                                            var data_special_sub_title = $('[data-id="' + id + '"]').data("special_sub_title");
                                                                            var data_special_description = $('[data-id="' + id + '"]').data("special_description");
                                                                            var data_special_image = $('[data-id="' + id + '"]').data("special_image");
                                                                            var data_special_position = $('[data-id="' + id + '"]').data("special_position");

                                                                            $('#navigation_label').val(data_navigation_label);
                                                                            $('#original_name').val(data_original_name);
                                                                            $("#menu_type1").val(data_menu_type);

                                                                            if (data_menu_type == 2) {
                                                                                $('#div_menu_sub_type1').show();
                                                                            } else {
                                                                                $('#div_menu_sub_type1').hide();
                                                                            }
                                                                            $("#menu_sub_type1").val(data_menu_sub_type);
                                                                            $("#menu_description1").val(data_menu_description);
                                                                            $("#menu_icon").val(data_menu_icon);
                                                                            $("#menu_icon_position").val(data_menu_icon_position);
                                                                            $('#menu_image').val(data_menu_image);

                                                                            $('#special_main_title').val(data_special_main_title);
                                                                            $('#special_sub_title').val(data_special_sub_title);
                                                                            $('#special_description').val(data_special_description);
                                                                            $('#special_image').val(data_special_image);
                                                                            $('#special_position').val(data_special_position);

                                                                            if (data_menu_image != '' && data_menu_image != undefined) {
                                                                                $('#menu_image_html').html('<tbody><tr><td class="col-md-1" style="background-color:#ccc"><img src="' + data_menu_image + '" id="c186" width="100"></td></tr></tbody>');
                                                                            } else {
                                                                                $('#menu_image_html').html('<tbody><tr></tr></tbody>');
                                                                            }
                                                                            if (data_special_image != '' && data_special_image != undefined) {
                                                                                $('#special_image_html').html('<tbody><tr><td class="col-md-1" style="background-color:#ccc"><img src="' + data_special_image + '" id="c187" width="100"></td></tr></tbody>');
                                                                            } else {
                                                                                $('#special_image_html').html('<tbody><tr></tr></tbody>');
                                                                            }
                                                                            $('#data_id').val(id);
                                                                            $('#menu_edit').modal('show');
                                                                        }
                                                                        function editCustomMenu(id) {
                                                                            $('#custom_menu_edit').modal('show');

                                                                            var data_table = $("[data-id='" + id + "']").data('table');
                                                                            var data_table_id = $("[data-id='" + id + "']").data('table_id');
                                                                            var data_menu_name = $("[data-id='" + id + "']").data('menu_name');
                                                                            var data_menu_link = $("[data-id='" + id + "']").data('menu_url');
                                                                            var data_menu_target = $("[data-id='" + id + "']").data('menu_target');
                                                                            var data_menu_type = $("[data-id='" + id + "']").data('menu_type');
                                                                            var data_menu_sub_type = $("[data-id='" + id + "']").data('menu_sub_type');
                                                                            var data_menu_description = $('[data-id="' + id + '"]').data("menu_description");
                                                                            var data_menu_icon = "";
                                                                            if ($("[data-id='" + id + "']").data('menu_icon') != undefined) {
                                                                                data_menu_icon = $("[data-id='" + id + "']").data('menu_icon');
                                                                            }
                                                                            var data_menu_icon_position = $("[data-id='" + id + "']").data('menu_icon_position');
                                                                            var data_menu_image = $("[data-id='" + id + "']").data('menu_image');
                                                                            var data_special_main_title = $('[data-id="' + id + '"]').data("special_main_title");
                                                                            var data_special_sub_title = $('[data-id="' + id + '"]').data("special_sub_title");
                                                                            var data_special_description = $('[data-id="' + id + '"]').data("special_description");
                                                                            var data_special_image = $('[data-id="' + id + '"]').data("special_image");
                                                                            var data_special_position = $('[data-id="' + id + '"]').data("special_position");

                                                                            $('#menu_name').val(data_menu_name);
                                                                            $('#menu_link').val(data_menu_link);
                                                                            $('#menu_target').val(data_menu_target);
                                                                            $("#menu_type2").val(data_menu_type);
                                                                            if (data_menu_type == 2) {
                                                                                $('#div_menu_sub_type2').show();
                                                                            } else {
                                                                                $('#div_menu_sub_type2').hide();
                                                                            }
                                                                            $("#menu_sub_type2").val(data_menu_sub_type);
                                                                            $("#menu_description2").val(data_menu_description);
                                                                            $("#menu_icon1").select2().val(data_menu_icon).trigger('change.select2');
                                                                            $('#menu_image1').val(data_menu_image);
                                                                            $('#menu_icon_position1').val(data_menu_icon_position);

                                                                            $('#special_main_title1').val(data_special_main_title);
                                                                            $('#special_sub_title1').val(data_special_sub_title);
                                                                            $('#special_description1').val(data_special_description);
                                                                            $('#special_image1').val(data_special_image);
                                                                            $('#special_position1').val(data_special_position);

                                                                            if (data_menu_image != '' && data_menu_image != undefined) {
                                                                                $('#menu_image_html1').html('<tbody><tr><td class="col-md-1" style="background-color:#ccc"><img src="' + data_menu_image + '" id="c188" width="100"></td></tr></tbody>');
                                                                            } else {
                                                                                $('#menu_image_html1').html('<tbody><tr></tr></tbody>');
                                                                            }
                                                                            if (data_special_image != '' && data_special_image != undefined) {
                                                                                $('#special_image_html1').html('<tbody><tr><td class="col-md-1" style="background-color:#ccc"><img src="' + data_special_image + '" id="c189" width="100"></td></tr></tbody>');
                                                                            } else {
                                                                                $('#special_image_html1').html('<tbody><tr></tr></tbody>');
                                                                            }
                                                                            $('#data_id').val(id);
                                                                        }
                                                                        $('#menu_edit').on('shown.bs.modal', function (e) {
                                                                            $("#remove_special_images").css("display", "none");
                                                                            $("#remove_special_images1").css("display", "none");
                                                                            $("#remove_slider_images").css("display", "none");
                                                                            $("#remove_slider_images1").css("display", "none");
                                                                            $("#menu_icon").select2({
                                                                                placeholder: "Select Icon",
                                                                                allowClear: true,
                                                                                width: '100%',
                                                                                dropdownParent: $("#menu_edit"),
                                                                                templateSelection: iformat,
                                                                                templateResult: iformat,
                                                                                escapeMarkup: function (text) {
                                                                                    return text;
                                                                                }
                                                                            });
                                                                            $("#menu_icon1").select2({
                                                                                placeholder: "Select Icon",
                                                                                allowClear: true,
                                                                                width: '100%',
                                                                                dropdownParent: $("#menu_edit"),
                                                                                templateSelection: iformat,
                                                                                templateResult: iformat,
                                                                                escapeMarkup: function (text) {
                                                                                    return text;
                                                                                }
                                                                            });
                                                                            var hdnimgSrc = $('#c187').attr('src');
                                                                            if (hdnimgSrc != null || hdnimgSrc != undefined) {
                                                                                $('#remove_special_images').show();
                                                                            }
                                                                            var hdnimgSrcmenu = $('#c186').attr('src');
                                                                            if (hdnimgSrcmenu != null || hdnimgSrcmenu != undefined) {
                                                                                $('#remove_slider_images').show();
                                                                            }
                                                                            function iformat(icon) {
                                                                                var originalOption = icon.element;
                                                                                return '<i class="fa ' + $(originalOption).data('icon') + '"></i> ' + icon.text;
                                                                            }
                                                                        });
                                                                        $('#custom_menu_edit').on('shown.bs.modal', function (e) {
                                                                            $("#remove_special_images").css("display", "none");
                                                                            $("#remove_special_images1").css("display", "none");
                                                                            $("#remove_slider_images").css("display", "none");
                                                                            $("#remove_slider_images1").css("display", "none");
                                                                            $("#menu_icon").select2({
                                                                                placeholder: "Select Icon",
                                                                                allowClear: true,
                                                                                width: '100%',
                                                                                templateSelection: iformat,
                                                                                templateResult: iformat,
                                                                                escapeMarkup: function (text) {
                                                                                    return text;
                                                                                }
                                                                            });
                                                                            $("#menu_icon1").select2({
                                                                                placeholder: "Select Icon",
                                                                                allowClear: true,
                                                                                width: '100%',
                                                                                dropdownParent: $("#custom_menu_edit"),
                                                                                templateSelection: iformat,
                                                                                templateResult: iformat,
                                                                                escapeMarkup: function (text) {
                                                                                    return text;
                                                                                }
                                                                            });
                                                                            var hdnimgSrc1 = $('#c189').attr('src');
                                                                            if (hdnimgSrc1 != null || hdnimgSrc1 != undefined) {
                                                                                $('#remove_special_images1').show();
                                                                            }
                                                                            var hdnimgSrcmenu1 = $('#c188').attr('src');
                                                                            if (hdnimgSrcmenu1 != null || hdnimgSrcmenu1 != undefined) {
                                                                                $('#remove_slider_images1').show();
                                                                            }
                                                                            function iformat(icon) {
                                                                                var originalOption = icon.element;
                                                                                return '<i class="fa ' + $(originalOption).data('icon') + '"></i> ' + icon.text;
                                                                            }
                                                                        });
                                                                        $('#save-edit-menu').on('click', function () {
                                                                            var navigation_label = $('#navigation_label').val();
                                                                            var original_name = $('#original_name').val();
                                                                            var menu_type = $('#menu_type1').val();
                                                                            var menu_sub_type = $('#menu_sub_type1').val();
                                                                            var menu_description = $('#menu_description1').val();
                                                                            var menu_icon = "";
                                                                            if ($('#menu_icon').val() != undefined) {
                                                                                menu_icon = $('#menu_icon').val();
                                                                            }
                                                                            var menu_icon_position = $('#menu_icon_position').val();
                                                                            var menu_image = $('#menu_image').val();

                                                                            var special_main_title = $('#special_main_title').val();
                                                                            var special_sub_title = $('#special_sub_title').val();
                                                                            var special_description = $('#special_description').val();
                                                                            var special_image = $('#special_image').val();
                                                                            var special_position = $('#special_position').val();

                                                                            var data_id = $('#data_id').val();
                                                                            if (navigation_label == '') {
                                                                                alert('Please enter navigation lable');
                                                                                return false;
                                                                            }
                                                                            $('[data-id="' + data_id + '"]').data("navigation_label", navigation_label);
                                                                            $('[data-id="' + data_id + '"]').data("menu_type", menu_type);
                                                                            $('[data-id="' + data_id + '"]').data("menu_sub_type", menu_sub_type);
                                                                            $('[data-id="' + data_id + '"]').data("menu_description", menu_description);
                                                                            $('[data-id="' + data_id + '"]').data("menu_image", menu_image);
                                                                            $('[data-id="' + data_id + '"]').data("menu_icon", menu_icon);
                                                                            $('[data-id="' + data_id + '"]').data("menu_icon_position", menu_icon_position);

                                                                            $('[data-id="' + data_id + '"]').data("special_main_title", special_main_title);
                                                                            $('[data-id="' + data_id + '"]').data("special_sub_title", special_sub_title);
                                                                            $('[data-id="' + data_id + '"]').data("special_description", special_description);
                                                                            $('[data-id="' + data_id + '"]').data("special_image", special_image);
                                                                            $('[data-id="' + data_id + '"]').data("special_position", special_position);

                                                                            $('[data-id="' + data_id + '"]').find("> .dd-handle span.menu_name").html('<b>' + navigation_label + '</b>');
                                                                            // update JSON
                                                                            updateOutput($('#nestable_list_1').data('output', $('#nestable_list_1_output')));
                                                                        });
                                                                        $('#save-custom-edit-menu').on('click', function () {
                                                                            var menu_name = $('#menu_name').val();
                                                                            var menu_link = $('#menu_link').val();
                                                                            var menu_target = $('#menu_target').val();
                                                                            var menu_type = $('#menu_type2').val();
                                                                            var menu_sub_type = $('#menu_sub_type2').val();
                                                                            var menu_description = $('#menu_description2').val();
                                                                            var menu_image = $('#menu_image1').val();
                                                                            var menu_icon = "";
                                                                            if ($('#menu_icon1').val() != undefined) {
                                                                                menu_icon = $('#menu_icon1').val();
                                                                            }
                                                                            var menu_icon_position = $('#menu_icon_position1').val();
                                                                            var special_main_title = $('#special_main_title1').val();
                                                                            var special_sub_title = $('#special_sub_title1').val();
                                                                            var special_description = $('#special_description1').val();
                                                                            var special_image = $('#special_image1').val();
                                                                            var special_position = $('#special_position1').val();

                                                                            var data_id = $('#data_id').val();
                                                                            if (menu_name == '') {
                                                                                alert('Please enter menu name');
                                                                                return false;
                                                                            }
                                                                            if (menu_link == '') {
                                                                                alert('Please enter menu link');
                                                                                return false;
                                                                            }
                                                                            $('[data-id="' + data_id + '"]').data("menu_name", menu_name);
                                                                            $('[data-id="' + data_id + '"]').data("menu_url", menu_link);
                                                                            $('[data-id="' + data_id + '"]').data("menu_target", menu_target);
                                                                            $('[data-id="' + data_id + '"]').data("menu_type", menu_type);
                                                                            $('[data-id="' + data_id + '"]').data("menu_sub_type", menu_sub_type);
                                                                            $('[data-id="' + data_id + '"]').data("menu_description", menu_description);
                                                                            $('[data-id="' + data_id + '"]').data("menu_image", menu_image);
                                                                            $('[data-id="' + data_id + '"]').data("menu_icon", menu_icon);
                                                                            $('[data-id="' + data_id + '"]').data("menu_icon_position", menu_icon_position);

                                                                            $('[data-id="' + data_id + '"]').data("special_main_title", special_main_title);
                                                                            $('[data-id="' + data_id + '"]').data("special_sub_title", special_sub_title);
                                                                            $('[data-id="' + data_id + '"]').data("special_description", special_description);
                                                                            $('[data-id="' + data_id + '"]').data("special_image", special_image);
                                                                            $('[data-id="' + data_id + '"]').data("special_position", special_position);

                                                                            $('[data-id="' + data_id + '"]').find("> .dd-handle span.menu_name").html('<b>' + menu_name + '</b>');

                                                                            // update JSON
                                                                            updateOutput($('#nestable_list_1').data('output', $('#nestable_list_1_output')));
                                                                        });
                                                                        function itemDelete(id) {
                                                                            swal({
                                                                                title: 'Are you sure want to remove this menu?',
                                                                                type: 'warning',
                                                                                showCancelButton: true,
                                                                                confirmButtonText: 'Yes',
                                                                                cancelButtonText: 'No, Cancel'
                                                                            }, function () {
                                                                                $("li[data-id='" + id + "']").remove();
                                                                                updateOutput($('#nestable_list_1').data('output', $('#nestable_list_1_output')));
                                                                                var nestableList = $("#nestable_list_1_output").val();
                                                                                if (nestableList == '[]') {
                                                                                    $('#menuitem_validate').val('');
                                                                                } else {
                                                                                    $('#menuitem_validate').val(nestableList);
                                                                                }
                                                                            });

                                                                        }

                                                                        function getCategoryHtml(id = '') {
                                                                            $.ajax({
                                                                                url: '/admin/menu/getcategorymenuitemdata',
                                                                                type: 'post',
                                                                                data: {
                                                                                    'category_id': id,
                                                                                },
                                                                                dataType: 'html',
                                                                                success: function (result) {
                                                                                    if (result) {
                                                                                        $('#category_form_html').html(result);
                                                                                    } else {
                                                                                        $('#category_form_html').html('<div class="text-center">No Data Available</div>');
                                                                                    }
                                                                                },
                                                                                error: function (jqXHR, textStatus, errorThrown) {
                                                                                    return false;
                                                                                }
                                                                            });
                                                                        }

                                                                        function getChildCategory(id) {
                                                                            if (!$(this).is(':checked')) {
                                                                                $('.child_of_' + id).prop('checked', true);
                                                                                getRecursiveChildCategory(id);
                                                                            } else {
                                                                                $('.child_of_' + id).prop('checked', false);
                                                                            }
                                                                        }

                                                                        function getRecursiveChildCategory(id) {
                                                                            $('.child_of_' + id).each(function () {
                                                                                if ($(this).is(':checked')) {
                                                                                    var sub_id = this.id.split(/[_]+/).pop();
                                                                                    $('.child_of_' + sub_id).prop('checked', true);
                                                                                    getRecursiveChildCategory(sub_id);
                                                                                }
                                                                            });
                                                                        }
                                                                        /*Nestable List End*/

                                                                        function openPopup() {
                                                                            var img_name_count = $("#commasapareted_images_name").val();
                                                                            var number_records = 0;
                                                                            if (img_name_count != '') {
                                                                                number_records = img_name_count.split("|").length;
                                                                            }
                                                                            var slider_div_id = $('#menu_image_html');
                                                                            var slider_inder_div = $('<tr/>');
                                                                            var newHtml = '';
                                                                            CKFinder.popup({
                                                                                chooseFiles: true,
                                                                                width: 800,
                                                                                height: 600,
                                                                                onInit: function (finder) {
                                                                                    finder.on('files:choose', function (evt) {
                                                                                        if (evt.data.files.length > 0) { // checking file selected or not
                                                                                            var always_show_checkbox_count = number_records - 1;
                                                                                            for (var i = 0; i < evt.data.files.length; i++) {
                                                                                                always_show_checkbox_count++;
                                                                                                var alt = evt.data.files.models[i].attributes.name;
                                                                                                var name_separater = "|";
                                                                                                var path = evt.data.files.models[i].attributes.url;
                                                                                                var cid = evt.data.files.models[i].attributes.cid;
                                                                                                var commasapareted = '';
                                                                                                var commasapareted_images_name = $("#commasapareted_images_name").val();
                                                                                                if (commasapareted_images_name != '' && commasapareted_images_name != undefined) {
                                                                                                    commasapareted = commasapareted_images_name + name_separater + alt;
                                                                                                } else {
                                                                                                    commasapareted = alt;
                                                                                                }
                                                                                                var html = '<td class="col-md-1" style="background-color:#ccc"><img src="' + path + '" width="100" id="' + cid + '"/></td>';
                                                                                                slider_div_id.html(slider_inder_div.clone().html(html));
                                                                                                var new_image_name = '';

                                                                                                $("#menu_image").val(path);
                                                                                                $("#commasapareted_images_name").val(commasapareted);
                                                                                                $('.cls_switch_always_show_ckbox').bootstrapSwitch({onColor: 'primary', offColor: 'default', onText: '<i class="fa fa-check"></i>', offText: '<i class="fa fa-times"></i>', labelText: '&nbsp', baseClass: 'bootstrap-switch', wrapperClass: 'wrapper'});
                                                                                            }
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });
                                                                        }

                                                                        function openKCFinder(div) {
                                                                            var slider_div_id = $('#menu_image_html');
                                                                            var slider_inder_div = $('<tr/>');
                                                                            var newHtml = '';

                                                                            window.KCFinder = {
                                                                                callBack: function (url) {
                                                                                    window.KCFinder = null;
                                                                                    //div.innerHTML = '<div style="margin:5px">Loading...</div>';
                                                                                    var img = new Image();
                                                                                    img.src = url;
                                                                                    img.onload = function () {
                                                                                        // div.innerHTML = '<img id="img" src="' + url + '" />';
                                                                                        var html = '<td class="col-md-1" style="background-color:#ccc"><img id="img" src="' + url + '" width="100"/></td>';
                                                                                        slider_div_id.html(slider_inder_div.clone().html(html));
                                                                                        $("#menu_image").val(url);
                                                                                        var img = document.getElementById('img');
                                                                                        img.style.visibility = "visible";
                                                                                    }
                                                                                }
                                                                            };
//    window.KCFinder = {
//        callBack: function (url) {
//            window.KCFinder = null;
//            div.innerHTML = '<div style="margin:5px">Loading...</div>';
//            var img = new Image();
//            img.src = url;
//            img.onload = function () {
//                div.innerHTML = '<img id="img" src="' + url + '" />';
//                var img = document.getElementById('img');
//                var o_w = img.offsetWidth;
//                var o_h = img.offsetHeight;
//                var f_w = div.offsetWidth;
//                var f_h = div.offsetHeight;
//                if ((o_w > f_w) || (o_h > f_h)) {
//                    if ((f_w / f_h) > (o_w / o_h))
//                        f_w = parseInt((o_w * f_h) / o_h);
//                    else if ((f_w / f_h) < (o_w / o_h))
//                        f_h = parseInt((o_h * f_w) / o_w);
//                    img.style.width = f_w + "px";
//                    img.style.height = f_h + "px";
//                } else {
//                    f_w = o_w;
//                    f_h = o_h;
//                }
//                img.style.marginLeft = parseInt((div.offsetWidth - f_w) / 2) + 'px';
//                img.style.marginTop = parseInt((div.offsetHeight - f_h) / 2) + 'px';
//                img.style.visibility = "visible";
//            }
//        }
//    };
                                                                            window.open('/assets/global/plugins/kcfinder/browse.php?type=images&dir=images/public',
                                                                                    'kcfinder_image', 'status=0, toolbar=0, location=0, menubar=0, ' +
                                                                                    'directories=0, resizable=1, scrollbars=0, width=800, height=600'
                                                                                    );
                                                                        }

                                                                        function openKCFinder1(div) {
                                                                            var slider_div_id = $('#menu_image_html1');
                                                                            var slider_inder_div = $('<tr/>');
                                                                            var newHtml = '';

                                                                            window.KCFinder = {
                                                                                callBack: function (url) {
                                                                                    window.KCFinder = null;
                                                                                    //div.innerHTML = '<div style="margin:5px">Loading...</div>';
                                                                                    var img = new Image();
                                                                                    img.src = url;
                                                                                    img.onload = function () {
                                                                                        // div.innerHTML = '<img id="img" src="' + url + '" />';
                                                                                        var html = '<td class="col-md-1" style="background-color:#ccc"><img id="img" src="' + url + '" width="100"/></td>';
                                                                                        slider_div_id.html(slider_inder_div.clone().html(html));
                                                                                        console.log($("#menu_image1").val(url));
                                                                                        var img = document.getElementById('img');
                                                                                        img.style.visibility = "visible";
                                                                                    }
                                                                                }
                                                                            };
                                                                            window.open('/assets/global/plugins/kcfinder/browse.php?type=images&dir=images/public',
                                                                                    'kcfinder_image', 'status=0, toolbar=0, location=0, menubar=0, ' +
                                                                                    'directories=0, resizable=1, scrollbars=0, width=800, height=600'
                                                                                    );
                                                                        }

                                                                        function openKCFinder2(div) {
                                                                            var slider_div_id = $('#special_image_html');
                                                                            var slider_inder_div = $('<tr/>');
                                                                            var newHtml = '';

                                                                            window.KCFinder = {
                                                                                callBack: function (url) {
                                                                                    window.KCFinder = null;
                                                                                    //div.innerHTML = '<div style="margin:5px">Loading...</div>';
                                                                                    var img = new Image();
                                                                                    img.src = url;
                                                                                    img.onload = function () {
                                                                                        // div.innerHTML = '<img id="img" src="' + url + '" />';
                                                                                        var html = '<td class="col-md-1" style="background-color:#ccc"><img id="img" src="' + url + '" width="100"/></td>';
                                                                                        slider_div_id.html(slider_inder_div.clone().html(html));
                                                                                        console.log($("#special_image").val(url));
                                                                                        var img = document.getElementById('img');
                                                                                        img.style.visibility = "visible";
                                                                                    }
                                                                                }
                                                                            };
                                                                            window.open('/assets/global/plugins/kcfinder/browse.php?type=images&dir=images/public',
                                                                                    'kcfinder_image', 'status=0, toolbar=0, location=0, menubar=0, ' +
                                                                                    'directories=0, resizable=1, scrollbars=0, width=800, height=600'
                                                                                    );
                                                                        }

                                                                        function openKCFinder3(div) {
                                                                            var slider_div_id = $('#special_image_html1');
                                                                            var slider_inder_div = $('<tr/>');
                                                                            var newHtml = '';

                                                                            window.KCFinder = {
                                                                                callBack: function (url) {
                                                                                    window.KCFinder = null;
                                                                                    //div.innerHTML = '<div style="margin:5px">Loading...</div>';
                                                                                    var img = new Image();
                                                                                    img.src = url;
                                                                                    img.onload = function () {
                                                                                        // div.innerHTML = '<img id="img" src="' + url + '" />';
                                                                                        var html = '<td class="col-md-1" style="background-color:#ccc"><img id="img" src="' + url + '" width="100"/></td>';
                                                                                        slider_div_id.html(slider_inder_div.clone().html(html));
                                                                                        console.log($("#special_image1").val(url));
                                                                                        var img = document.getElementById('img');
                                                                                        img.style.visibility = "visible";
                                                                                    }
                                                                                }
                                                                            };
                                                                            window.open('/assets/global/plugins/kcfinder/browse.php?type=images&dir=images/public',
                                                                                    'kcfinder_image', 'status=0, toolbar=0, location=0, menubar=0, ' +
                                                                                    'directories=0, resizable=1, scrollbars=0, width=800, height=600'
                                                                                    );
                                                                        }

                                                                        $('#menu_type1').on('change', function () {
                                                                            var selected_menu_type = this.value;
                                                                            if (selected_menu_type == 2) {
                                                                                $('#div_menu_sub_type1').show();
                                                                            } else {
                                                                                $('#div_menu_sub_type1').hide();
                                                                            }
                                                                        });

                                                                        $('#menu_type2').on('change', function () {
                                                                            var selected_menu_type = this.value;
                                                                            if (selected_menu_type == 2) {
                                                                                $('#div_menu_sub_type2').show();
                                                                            } else {
                                                                                $('#div_menu_sub_type2').hide();
                                                                            }
                                                                        });


                                                                        function getPrimaryMenuData(langId, langCode) {
                                                                            $.ajax({
                                                                                url: '/admin/contentcms/menu/getMenuItemWithLang',
                                                                                type: 'post',
                                                                                data: {
                                                                                    'langId': langId,
                                                                                    'langCode': langCode,
                                                                                },
                                                                                dataType: 'json',
                                                                                success: function (result) {
                                                                                    if (result) {
                                                                                        $('#form_' + langCode + 'MainMenu').html(result.primary_html);
                                                                                        $('#form_' + langCode + 'TopHeaderMenu').html(result.secondary_html);

                                                                                        var mainMenu = $('#form_' + langCode + 'MainMenu').val();
                                                                                        var topHeaderMenu = $('#form_' + langCode + 'TopHeaderMenu').val();

                                                                                        if (topHeaderMenu != '') {
                                                                                            var top_header_menu_enc_val = encMenuId(topHeaderMenu);
                                                                                            top_header_menu(top_header_menu_enc_val, langCode);
                                                                                        }
                                                                                        if (mainMenu != '') {
                                                                                            var main_menu_enc_val = encMenuId(mainMenu);
                                                                                            main_menu(main_menu_enc_val, langCode);
                                                                                        }
                                                                                    }
                                                                                },
                                                                                error: function (jqXHR, textStatus, errorThrown) {
                                                                                    return false;
                                                                                }
                                                                            });
                                                                        }

                                                                        $("#select_special_images").on('click', function () {
                                                                            var hidebutton = $('#special_image_html img').attr('src', '');
                                                                            if (hidebutton)
                                                                            {
                                                                                $("#remove_special_images").show();
                                                                                $('#special_image_html').show();
                                                                            }
                                                                            // else
                                                                            // {
                                                                            //      $("#remove_special_images").css("display" , "block");
                                                                            // }  
                                                                        });
                                                                        $("#select_special_images1").on('click', function () {
                                                                            var hidebutton1 = $('#special_image_html1 img').attr('src', '');
                                                                            if (hidebutton1)
                                                                            {
                                                                                $("#remove_special_images1").show();
                                                                                $('#special_image_html1').show();
                                                                            }
                                                                        });
                                                                        $("#slider_images").on('click', function () {
                                                                            var hidemenuImage = $('#menu_image_html img').attr('src', '');
                                                                            if (hidemenuImage)
                                                                            {
                                                                                $("#remove_slider_images").show();
                                                                                $('#menu_image_html').show();
                                                                            }
                                                                        });
                                                                        $("#slider_images1").on('click', function () {
                                                                            var hidemenuImage1 = $('#menu_image_html1 img').attr('src', '');
                                                                            if (hidemenuImage1)
                                                                            {
                                                                                $("#remove_slider_images1").show();
                                                                                $('#menu_image_html').show();
                                                                            }
                                                                        });

                                                                        function removeImage(id) {
                                                                            if (id != '' && id != undefined) {
                                                                                if (id == 'remove_special_images') {
                                                                                    swal({
                                                                                        title: 'Are you sure you want to remove this image?', type: 'warning', showCancelButton: true, confirmButtonText: 'Yes', cancelButtonText: 'No'
                                                                                    }, function () {
                                                                                        $('#special_image_html img').attr('src', '');
                                                                                        $('#special_image').val('');
                                                                                        $("#remove_special_images").hide();
                                                                                        $('#special_image_html').hide();
                                                                                    });
                                                                                }
                                                                                if (id == 'remove_special_images1') {
                                                                                    swal({
                                                                                        title: 'Are you sure you want to remove this image?', type: 'warning', showCancelButton: true, confirmButtonText: 'Yes', cancelButtonText: 'No'
                                                                                    }, function () {
                                                                                        $('#special_image_html1 img').attr('src', '');
                                                                                        $('#special_image1').val('');
                                                                                        $("#remove_special_images1").hide();
                                                                                        $('#special_image_html1').hide();
                                                                                    });

                                                                                }
                                                                                if (id == 'remove_slider_images') {
                                                                                    swal({
                                                                                        title: 'Are you sure you want to remove this image?', type: 'warning', showCancelButton: true, confirmButtonText: 'Yes', cancelButtonText: 'No'
                                                                                    }, function () {
                                                                                        $('#menu_image_html img').attr('src', '');
                                                                                        $('#menu_image').val('');
                                                                                        $("#remove_slider_images").hide();
                                                                                        $('#menu_image_html').hide();
                                                                                    });
                                                                                }
                                                                                if (id == 'remove_slider_images1') {
                                                                                    swal({
                                                                                        title: 'Are you sure you want to remove this image?', type: 'warning', showCancelButton: true, confirmButtonText: 'Yes', cancelButtonText: 'No'
                                                                                    }, function () {
                                                                                        $('#menu_image_html1 img').attr('src', '');
                                                                                        $('#menu_image1').val('');
                                                                                        $("#remove_slider_images1").hide();
                                                                                        $('#menu_image_html1').hide();
                                                                                    });
                                                                                }
                                                                            }
                                                                        }
                                                                        function iformat(icon) {
                                                                            var originalOption = icon.element;
                                                                            return '<i class="fa ' + $(originalOption).data('icon') + '"></i> ' + icon.text;
                                                                        }


</script>