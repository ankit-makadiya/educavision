<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo $module_name; ?>
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li>    
                <a href="<?php echo base_url('admin/dashboard'); ?>">
                    <i class="fa fa-dashboard"></i>
                    Home
                </a>
            </li>
            <li class="active"><?php echo $module_name; ?></li>
        </ol>
    </section>

    <section class="content-header">
        <?php if ($this->session->flashdata('success')) { ?>
            <div class="callout callout-success">
                <p><?php echo $this->session->flashdata('success'); ?></p>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>  
            <div class="callout callout-danger" >
                <p><?php echo $this->session->flashdata('error'); ?></p>
            </div>
        <?php } ?>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-md-12">

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo $section_title; ?></h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php
                    $form_attr = array('id' => 'add_shipping_frm', 'enctype' => 'multipart/form-data');
                    echo form_open_multipart('admin/shipping/add', $form_attr);
                    ?>
                    <div class="box-body">
                        <div class="form-group col-sm-10">
                            <label for="lable_category"  class="col-sm-2 control-label">Shipping Country*</label>
                            <div class="col-sm-10">
                                <select class="form-control country select2"  name="country_id" id="country_id" style="width: 100%;" tabindex="-1" required="" >
                                    <option value="">Select Country</option>
                                    <?php foreach ($country_list as $country) { ?>
                                        <option value="<?php echo $country['id'] ?>"><?php echo $country['name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group col-sm-10">
                            <label for="lable_slug" name="price_from" id="shipping_price_from" class="col-sm-2 control-label">Price From*</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="price_from" id="price_from" placeholder="Enter Price from">
                            </div>
                            <label for="lable_slug" name="price_to" id="shipping_price_to" class="col-sm-2 control-label">Price To*</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="price_to" id="price_to" placeholder="Enter Price to">
                            </div>
                        </div>

                        <div class="form-group col-sm-10">
                            <label for="amount" class="col-sm-2 control-label">Amount*</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="amount" id="amount" placeholder="Enter Amount">
                            </div>
                        </div>

                        <div class="form-group col-sm-10">
                            <label for="product_status" id="product_status" class="col-sm-2 control-label">Status*</label>
                            <div class="col-sm-10">
                                <?php $selected = 1; ?><br>
                                <?php echo form_label('Publish', '1') . ' ' . form_radio(array('name' => 'status', 'value' => '1', 'checked' => ('1' == $selected) ? TRUE : FALSE, 'id' => 'publish')); ?>
                                <?php echo form_label('Draft', '0') . ' ' . form_radio(array('name' => 'status', 'value' => '0', 'checked' => ('0' == $selected) ? TRUE : FALSE, 'id' => 'draft')); ?>
                            </div>
                        </div>
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <?php
                        $save_attr = array('id' => 'btn_save', 'name' => 'btn_save', 'value' => 'Save', 'class' => 'btn btn-primary');
                        echo form_submit($save_attr);
                        ?>    
                        <button type="button" onclick="window.history.back();" class="btn btn-default">Back</button>
                        <!--<button type="submit" class="btn btn-info pull-right">Sign in</button>-->
                    </div><!-- /.box-footer -->
                    </form>
                </div><!-- /.box -->


            </div><!--/.col (right) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">
    //validation for edit email formate form
    $(document).ready(function () {
        $('.bootstrap3-editor').wysihtml5();
        $.validator.addMethod("greaterThan",
                function (value, element, param) {
                    var $otherElement = $(param);
                    return parseInt(value, 10) > parseInt($otherElement.val(), 10);
                });

        $.validator.addMethod("lessThan",
                function (value, element, param) {
                    var $otherElement = $(param);
                    return parseInt(value, 10) < parseInt($otherElement.val(), 10);
                });

        $("#add_shipping_frm").validate({
            ignore: [],
            debug: false,
            rules: {
                country_id: {
                    required: true,
                },
                price_from: {
                    required: true,
                    number: true,
                    lessThan: "#price_to",
                },
                price_to: {
                    required: true,
                    number: true,
                    greaterThan: "#price_from",
                },
                amount: {
                    required: true,
                    number: true,
                },
            },
            messages: {
                country_id: {
                    required: "Please select country id.",
                },
                price_from: {
                    required: "Please enter price from.",
                    number: "Please enter number only.",
                    lessThan: "Value must be lessthan price to.",
                },
                price_to: {
                    required: "Please enter price to.",
                    number: "Please enter number only.",
                    greaterThan: "Value must be graterthan price from.",
                },
                amount: {
                    required: "Please enter amount.",
                    number: "Please enter number only.",
                },
            },
        });

        $(".country").select2({
            placeholder: "Select Country",
            allowClear: true,
            width: '100%'
        });
    });
</script>