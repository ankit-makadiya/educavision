<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <?php /* ?><img src="<?php echo base_url($frameworks_dir . '/adminlte/img/logo.jpg')?>" class="" alt="SomeLounge"> <?php */ ?>
            </div>
        </div>
        <ul class="sidebar-menu">
            <li <?php if ($this->uri->segment(2) == 'dashboard' || $this->uri->segment(1) == '') { ?> class="active treeview" <?php } else { ?> class="treeview"   <?php } ?> >
                <a href="<?php echo base_url('admin/dashboard'); ?>">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <?php if(in_array('order-access', $admin_permission)){ ?>
            <li <?php if ($this->uri->segment(2) == 'localization') { ?> class="active treeview" <?php } else { ?> class="treeview"   <?php } ?>>
                <a href="javascript:void(0);">
                    <i class="fa fa-globe"></i><span>Order</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('admin/order?status=pending'); ?>">Pending Order</a></li>
                    <li><a href="<?php echo base_url('admin/order?status=confirm'); ?>">Confirm Order</a></li>
                </ul>
            </li>
	    <?php } ?>
            <?php if(in_array('page-access', $admin_permission)){ ?>
            <li <?php if ($this->uri->segment(2) == 'page' || $this->uri->segment(1) == '') { ?> class="active treeview" <?php } else { ?> class="treeview"   <?php } ?> >
                <a href="<?php echo base_url('admin/page'); ?>">
                    <i class="fa fa-file-text"></i> <span>Pages</span>
                </a>
            </li>
            <?php } ?>
            <?php if(in_array('slider-access', $admin_permission)){ ?>
            <li <?php if ($this->uri->segment(2) == 'slider' || $this->uri->segment(1) == '') { ?> class="active treeview" <?php } else { ?> class="treeview"   <?php } ?> >
                <a href="<?php echo base_url('admin/slider'); ?>">
                    <i class="fa fa-image"></i> <span>Slider</span>
                </a>
            </li>
            <?php } ?>
            <?php if(in_array('category-access', $admin_permission)){ ?>
            <li <?php if ($this->uri->segment(2) == 'category') { ?> class="active treeview" <?php } else { ?> class="treeview"   <?php } ?>>
                <a href="<?php echo base_url('admin/category'); ?>">
                    <i class="fa fa-life-ring"></i><span>Category</span>
                </a>
            </li>
            <?php } ?>
            <?php if(in_array('product-access', $admin_permission)){ ?>
            <li <?php if ($this->uri->segment(2) == 'product') { ?> class="active treeview" <?php } else { ?> class="treeview"   <?php } ?>>
                <a href="<?php echo base_url('admin/product'); ?>">
                    <i class="fa fa-flag"></i><span>Product</span>
                </a>
            </li>
            <?php } ?>
            <?php if(in_array('news-access', $admin_permission)){ ?>
            <li <?php if ($this->uri->segment(2) == 'news') { ?> class="active treeview" <?php } else { ?> class="treeview"   <?php } ?>>
                <a href="<?php echo base_url('admin/news'); ?>">
                    <i class="fa fa-rocket"></i><span>News</span>
                </a>
            </li>
            <?php } ?>
            <?php if(in_array('localization-access', $admin_permission)){ ?>
            <li <?php if ($this->uri->segment(2) == 'localization') { ?> class="active treeview" <?php } else { ?> class="treeview"   <?php } ?>>
                <a href="javascript:void(0);">
                    <i class="fa fa-globe"></i><span>Localization</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <?php if(in_array('country-access', $admin_permission)){ ?>
                    <li><a href="<?php echo base_url('admin/country'); ?>">Country</a></li>
                    <?php } ?>
                    <?php if(in_array('state-access', $admin_permission)){ ?>
                    <li><a href="<?php echo base_url('admin/state'); ?>">State</a></li>
                    <?php } ?>
                    <?php if(in_array('city-access', $admin_permission)){ ?>
                    <li><a href="<?php echo base_url('admin/city'); ?>">City</a></li>
                    <?php } ?>
                </ul>
            </li>
            <?php } ?>
            <?php if(in_array('modules-access', $admin_permission)){ ?>
            <li <?php if ($this->uri->segment(2) == 'modules') { ?> class="active treeview" <?php } else { ?> class="treeview"   <?php } ?>>
                <a href="javascript:void(0);">
                    <i class="fa fa-globe"></i><span>Modules</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <?php if(in_array('shipping-access', $admin_permission)){ ?>
                    <li><a href="<?php echo base_url('admin/shipping'); ?>">Shipping</a></li>
                    <?php } ?>
                    <?php if(in_array('couponcode-access', $admin_permission)){ ?>
                    <li><a href="<?php echo base_url('admin/couponcode'); ?>">Coupon Code</a></li>
                    <?php } ?>
                    <?php if(in_array('formdata-access', $admin_permission)){ ?>
                    <li><a href="<?php echo base_url('admin/formdata'); ?>">Form Data</a></li>
                    <?php } ?>
                    <?php if(in_array('tax-access', $admin_permission)){ ?>
                    <li><a href="<?php echo base_url('admin/tax'); ?>">Tax</a></li>
                    <?php } ?>
                </ul>
            </li>
            <?php } ?>
            <?php if(in_array('menu-access', $admin_permission)){ ?>
            <li <?php if ($this->uri->segment(2) == 'menu') { ?> class="active treeview" <?php } else { ?> class="treeview"   <?php } ?>>
                <a href="<?php echo base_url('admin/menu'); ?>">
                    <i class="fa fa-list-ul"></i><span>Menu Management</span>
                </a>
            </li>
            <?php } ?>
            <?php if(in_array('user-management-access', $admin_permission)){ ?>
            <li <?php if ($this->uri->segment(2) == 'user-management') { ?> class="active treeview" <?php } else { ?> class="treeview"   <?php } ?>>
                <a href="javascript:void(0);">
                    <i class="fa fa-users"></i><span>User Management</span>
					<span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
				</a>
				<ul class="treeview-menu">
                    <?php if(in_array('role-access', $admin_permission)){ ?>
                    <li><a href="<?php echo base_url('admin/role'); ?>">User Role</a></li>
                    <?php } ?>
                    <?php if(in_array('users-access', $admin_permission)){ ?>
                    <li><a href="<?php echo base_url('admin/users'); ?>">Users</a></li>
                    <?php } ?>
                    <?php if(in_array('customers-access', $admin_permission)){ ?>
					<!--<li><a href="--><?php //echo base_url('admin/customers'); ?><!--">Customers</a></li>-->
                    <?php } ?>
                </ul>
            </li>
            <?php } ?>
            <?php if(in_array('emailtemplate-access', $admin_permission)){ ?>
            <li <?php if ($this->uri->segment(2) == 'emailtemplate') { ?> class="active treeview" <?php } else { ?> class="treeview"   <?php } ?>>
                <a href="<?php echo base_url('admin/emailtemplate'); ?>">
                    <i class="fa fa-envelope"></i><span>Email Template</span>
                </a>
            </li>
            <?php } ?>
            <?php if(in_array('settings-access', $admin_permission)){ ?>
            <li <?php if ($this->uri->segment(2) == 'settings') { ?> class="active treeview" <?php } else { ?> class="treeview"   <?php } ?> >
                <a href="<?php echo base_url('admin/settings'); ?>">
                    <i class="fa fa-cogs"></i> <span>Settings</span>
                </a>
            </li>
            <?php } ?>
            <li <?php if ($this->uri->segment(3) == 'change_password') { ?> class="active treeview" <?php } else { ?> class="treeview"   <?php } ?> >
                <a href="<?php echo base_url('admin/dashboard/change_password'); ?>">
                    <i class="fa fa-lock"></i> <span>Change Password</span>
                </a>
            </li>
        </ul>
    </section>
</aside>
