<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<header class="main-header">
    <a href="<?php echo base_url('admin/dashboard'); ?>" class="logo">
        <span class="logo-mini"><b><?php echo $title_mini; ?></b></span>
        <span class="logo-lg"><?php echo $title_full; ?></span>
    </a>

    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?php echo base_url() . $frameworks_dir . '/adminlte/img/logo_preview.png'; ?>" class="user-image" alt="User Image" style="width:auto; height: 30px;">
                        <span class="hidden-xs"><?php if(isset($this->session->get_userdata('sl_admin')['sl_admin']['name'])){ echo $this->session->get_userdata('sl_admin')['sl_admin']['name']; } ?></span>
					</a>
                    <ul class="dropdown-menu">
                        <li class="user-header">
                            <img src="<?php echo base_url() . $frameworks_dir . '/adminlte/img/logo_preview.png'; ?>" class="" alt="User Image" style="background-color: #6453ED; width: auto; height: auto;">
                            <p><?php if(isset($this->session->get_userdata('sl_admin')['sl_admin']['name'])){ echo $this->session->get_userdata('sl_admin')['sl_admin']['name']; } ?></p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left"></div>
                            <div class="pull-right">
                                <a href="<?php echo base_url('admin/dashboard/logout'); ?>" class="btn btn-default btn-flat">Sign Out</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
