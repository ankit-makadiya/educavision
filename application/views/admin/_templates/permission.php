<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo $module_name; ?>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('admin/dashboard'); ?>">
                    <i class="fa fa-dashboard"></i>
                    Home
                </a>
            </li>
            <li class="active"><?php echo $module_name; ?></li>
        </ol>
    </section>
    <section class="content">
        <div class="row" >
            <div class="col-xs-12" style="margin-top:150px;">
            	<?php echo PERMISSION_DENIED_MESSAGE; ?>
            </div>
        </div>
    </section>
</div>