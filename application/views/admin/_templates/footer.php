<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.1.0
    </div>
    <strong>Copyright &copy; <?php echo gmdate('Y'); ?> <a href="javascript:void(0)"><?php echo APP_NAME ?></a>.</strong> All rights reserved.
</footer>
<script src="<?php echo base_url() . $plugins_dir . '/knob/jquery.knob.js'; ?>"></script>
<script src="<?php echo base_url() . $frameworks_dir . '/cdn/js/moment.min.js'; ?>"></script>
<script src="<?php echo base_url() . $plugins_dir . '/daterangepicker/daterangepicker.js'; ?>"></script>
<script src="<?php echo base_url() . $plugins_dir . '/datepicker/bootstrap-datepicker.js'; ?>"></script>
<!-- Advance Editor -->
<script src="<?php echo base_url() ?>assets/keditor-master/CMS/plugins/bootstrap-3.3.6/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/keditor-master/CMS/plugins/jquery-ui-1.12.1.custom/jquery-ui.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/keditor-master/CMS/plugins/jquery-ui.touch-punch-0.2.3/jquery.ui.touch-punch.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/keditor-master/CMS/plugins/jquery.nicescroll-3.6.6/jquery.nicescroll.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/keditor-master/CMS/plugins/ckeditor-4.5.6/ckeditor.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/keditor-master/CMS/plugins/ckeditor-4.5.6/adapters/jquery.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/keditor-master/CMS/plugins/formBuilder-2.5.3/form-builder.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/keditor-master/CMS/plugins/formBuilder-2.5.3/form-render.min.js"></script>

<script src="<?php echo base_url() ?>assets/keditor-master/dist/js/keditor-1.1.5.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/keditor-master/dist/js/keditor-components-1.1.5.js" type="text/javascript"></script>
<!-- Advance Editor-->
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-sweetalert/sweetalert.js" type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url() . $plugins_dir . '/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'; ?>"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url() ?>assets/admin/js/select2totree.js" type="text/javascript"></script>
<script src="<?php echo base_url() . $plugins_dir . '/slimScroll/jquery.slimscroll.min.js'; ?>"></script>
<script src="<?php echo base_url() . $plugins_dir . '/fastclick/fastclick.min.js'; ?>"></script>
<script src="<?php echo base_url() . $frameworks_dir . '/adminlte/js/app.js'; ?>"></script>
<script src="<?php echo base_url() . $frameworks_dir . '/adminlte/js/demo.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url() . $plugins_dir . '/validation/jquery.validate.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url() . $plugins_dir . '/validation/jquery.validate.methods.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url() . $plugins_dir . '/validation/additional-methods.js'; ?>"></script>
<script src="<?php echo base_url() . $plugins_dir . '/datatables/jquery.dataTables.min.js'; ?>"></script>
<script src="<?php echo base_url() . $plugins_dir . '/datatables/dataTables.bootstrap.min.js'; ?>"></script>
<script src="<?php echo base_url() ?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js" type="text/javascript"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        // Set Autocomplete Off
        $("form").attr('autocomplete', 'off');

        $('#confirm-delete').on('show.bs.modal', function (e) {
            $(this).find('.danger').attr('href', $(e.relatedTarget).data('href'));
        });
        $('#search_btn').click(function () {
            $('#search_frm').submit();
        });
        $('#checkedall').click(function (event) {
            if (this.checked) {
                // Iterate each checkbox
                $('.deletes').each(function () {
                    this.checked = true;
                });
            } else {
                $('.deletes').each(function () {
                    this.checked = false;
                });
            }
        });
        $('.deletes').click(function (event) {
            var flag = 0;
            $('.deletes').each(function () {
                if (this.checked == false) {
                    flag++;
                }
            });
            if (flag) {
                $('.checkedall').prop('checked', false);
            } else {
                $('.checkedall').prop('checked', true);
            }
        });
    });
    if ($(".parent_id").length > 0) {
        $(".parent_id").select2({
            placeholder: "Select Parent Category",
            allowClear: true,
            width: '100%'
        });
    }
    if ($(".country_id").length > 0) {
        $(".country_id").select2({
            placeholder: "Select Country",
            allowClear: true,
            width: '100%'
        });
    }
    if ($(".state_id").length > 0) {
        $(".state_id").select2({
            placeholder: "Select State",
            allowClear: true,
            width: '100%'
        });
    }
    if($(".variation_is_default").length > 0){
        $(".variation_is_default").rules("add", { 
            required:function (element) {
                var boxes = $('.variation_is_default');
                if (boxes.filter(':checked').length != 1) {
                    return true;
                }
                return false;
            },  
            maxlength:1
        });
    }
</script>

<script type="text/javascript" src="<?php echo base_url() . $plugins_dir . '/multiselect/jquery.multiselect.js'; ?>"></script>
</body>
</html>
