<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $title; ?></title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="<?php echo base_url() . $frameworks_dir . '/bootstrap/css/bootstrap.min.css'; ?>">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url() . $frameworks_dir . '/adminlte/css/AdminLTE.css'; ?>">
        <link rel="stylesheet" href="<?php echo base_url() . $frameworks_dir . '/adminlte/css/skins/skin-educavision.min.css'; ?>">
        <link rel="stylesheet" href="<?php echo base_url() . $plugins_dir . '/iCheck/flat/blue.css'; ?>">
        <link rel="stylesheet" href="<?php echo base_url() . $plugins_dir . '/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css' ?>">
        <link rel="stylesheet" href="<?php echo base_url() . $plugins_dir . '/datatables/dataTables.bootstrap.css'; ?>">
        <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . $plugins_dir . '/multiselect/jquery.multiselect.css'; ?>">
        <link href="<?php echo base_url() ?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo base_url() ?>assets/global/plugins/jquery-nestable/jquery.nestable.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/admin/css/custom.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<style type="text/css">
            .sweet-alert h2{font-size: 27px;}
            p.lead{font-size: 17px;}
			.select2-container--default .select2-selection--multiple .select2-selection__choice{ color: #000;}
        </style>
        <script src="<?php echo base_url() . $plugins_dir . '/jQuery/jQuery-2.1.4.min.js'; ?>"></script>
    </head>
    <body class="hold-transition skin-yellow sidebar-mini">
        <div class="wrapper">
