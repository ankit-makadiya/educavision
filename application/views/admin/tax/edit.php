<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo $module_name; ?>
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li>    
                <a href="<?php echo base_url('admin/dashboard'); ?>">
                    <i class="fa fa-dashboard"></i>
                    Home
                </a>
            </li>
            <li class="active"><?php echo $module_name; ?></li>
        </ol>
    </section>

    <section class="content-header">
        <?php if ($this->session->flashdata('success')) { ?>
            <div class="callout callout-success">
                <p><?php echo $this->session->flashdata('success'); ?></p>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>  
            <div class="callout callout-danger" >
                <p><?php echo $this->session->flashdata('error'); ?></p>
            </div>
        <?php } ?>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-md-12">

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo $section_title; ?></h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php
                    $form_attr = array('id' => 'edit_tax_frm', 'enctype' => 'multipart/form-data');
                    echo form_open_multipart('admin/tax/edit', $form_attr);
                    ?>
                    <input type="hidden" name="tax_id" value="<?php echo $tax_detail[0]['id'] ?>">
                    <div class="box-body">
                        <div class="form-group col-sm-10">
                            <label for="lable_category"  class="col-sm-2 control-label">Tax Country*</label>
                            <div class="col-sm-10">
                                <select class="form-control country select2"  name="country_id" id="country_id" style="width: 100%;" tabindex="-1" required="" >
                                    <option value="">Select Country</option>
                                    <?php
                                    foreach ($country_list as $country) {
                                        $selected = "";
                                        if ($country['id'] == $tax_detail[0]['country_id']) {
                                            $selected = 'Selected="selected"';
                                        }
                                        ?>
                                        <option value="<?php echo $country['id'] ?>" <?php echo $selected; ?>><?php echo $country['name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-sm-10">
                            <label for="tax_label" id="shipping_price_from" class="col-sm-2 control-label">Tax label*</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="tax_label" id="tax_label" placeholder="Enter label" value="<?php echo $tax_detail[0]['tax_label']; ?>">
                            </div>
                        </div>

                        <div class="form-group col-sm-10">
                            <label for="tax_rate"   class="col-sm-2 control-label">Tax rate*</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="tax_rate" id="tax_rate" placeholder="Enter rate" value="<?php echo $tax_detail[0]['tax_rate']; ?>">
                            </div>
                        </div>

                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <?php
                        $save_attr = array('id' => 'btn_save', 'name' => 'btn_save', 'value' => 'Save', 'class' => 'btn btn-primary');
                        echo form_submit($save_attr);
                        ?>    
                        <button type="button" onclick="window.history.back();" class="btn btn-default">Back</button>
                        <!--<button type="submit" class="btn btn-info pull-right">Sign in</button>-->
                    </div><!-- /.box-footer -->
                    </form>
                </div><!-- /.box -->


            </div><!--/.col (right) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">
    //validation for edit email formate form
    $(document).ready(function () {
        $('.bootstrap3-editor').wysihtml5();
        $.validator.addMethod("greaterThan",
                function (value, element, param) {
                    var $otherElement = $(param);
                    return parseInt(value, 10) > parseInt($otherElement.val(), 10);
                });

        $.validator.addMethod("lessThan",
                function (value, element, param) {
                    var $otherElement = $(param);
                    return parseInt(value, 10) < parseInt($otherElement.val(), 10);
                });

        $("#edit_tax_frm").validate({
            ignore: [],
            debug: false,
            rules: {
                country_id: {
                    required: true,
                },
                tax_label: {
                    required: true,
                },
                tax_rate: {
                    required: true,
                    floatprice: true
                },
            },
            messages: {
                country_id: {
                    required: "Please select country id.",
                },
                tax_label: {
                    required: "Please enter tax label.",
                },
                tax_rate: {
                    required: "Please enter tax rate.",
                    floatprice: "Please enter proper rate."
                },
            },
        });

        $.validator.addMethod("floatprice", function (value, element) {
            // allow any non-whitespace characters as the host part
            return this.optional(element) || /^-?\d*(\.\d+)?$/.test(value);
        }, 'Please enter a valid price.');

        $(".country").select2({
            placeholder: "Select Country",
            allowClear: true,
            width: '100%'
        });
    });

</script>