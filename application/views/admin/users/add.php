<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo $module_name; ?>
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('admin/dashboard'); ?>">
                    <i class="fa fa-dashboard"></i>
                    Home
                </a>
            </li>
            <li class="active"><?php echo $module_name; ?></li>
        </ol>
    </section>
    <section class="content-header">
        <?php if ($this->session->flashdata('success')) { ?>
            <div class="callout callout-success">
                <p><?php echo $this->session->flashdata('success'); ?></p>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>  
            <div class="callout callout-danger" >
                <p><?php echo $this->session->flashdata('error'); ?></p>
            </div>
        <?php } ?>

    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-md-12">

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo $section_title; ?></h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php
                    $form_attr = array('id' => 'add_users_frm', 'enctype' => 'multipart/form-data');
                    echo form_open_multipart('admin/users/add', $form_attr);
                    ?>
                    <div class="box-body">
                        <div class="form-group col-sm-10">
                            <label for="username" id="label_username">User Name*</label>
                            <?php
                            $data = array(
                                'name' => 'username',
                                'id' => 'username',
                                'value' => '',
                                'class' => 'form-control'
                            );

                            echo form_input($data);
                            ?>
                        </div>
                        <div class="form-group col-sm-10">
                            <label for="name" id="label_name">Name*</label>
                            <?php
                            $data = array(
                                'name' => 'name',
                                'id' => 'name',
                                'value' => '',
                                'class' => 'form-control'
                            );

                            echo form_input($data);
                            ?>
                        </div>
                        <div class="form-group col-sm-10">
                            <label for="email" id="label_email">Email*</label>
                            <?php
                            $data = array(
                                'name' => 'email',
                                'id' => 'email',
                                'value' => '',
                                'class' => 'form-control'
                            );

                            echo form_input($data);
                            ?>
                        </div>
                        <div class="form-group col-sm-10">
                            <label for="role" id="label_role">Role*</label>
                            <select class="form-control role" name="role" id="role" style="width: 100%;" tabindex="-1" required="" >
                            <?php foreach ($role_data as $role) { ?>
                                <option value="<?php echo $role['id'] ?>"><?php echo $role['name'] ?></option>
                            <?php } ?>
                            </select>
                        </div>
                        <div class="form-group col-sm-10">
                            <label for="password" id="password">Password*</label>
                            <?php
                            $data = array(
                                'name' => 'password',
                                'type' => 'password',
                                'id' => 'password',
                                'value' => '',
                                'class' => 'form-control'
                            );

                            echo form_input($data);
                            ?>
                        </div>
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <?php
                        $save_attr = array('id' => 'btn_save', 'name' => 'btn_save', 'value' => 'Save', 'class' => 'btn btn-primary');
                        echo form_submit($save_attr);
                        ?>    
                        <button type="button" onclick="window.history.back();" class="btn btn-default">Back</button>
                        <!--<button type="submit" class="btn btn-info pull-right">Sign in</button>-->
                    </div><!-- /.box-footer -->
                    <?php echo form_close(); ?>
                </div><!-- /.box -->


            </div><!--/.col (right) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
    //validation for edit email formate form
    $(document).ready(function () {

        $("#add_users_frm").validate({
            rules: {
                firstname: {
                    required: true,
                    minlength: 3,
                    maxlength: 50
                },
                lastname: {
                    required: true,
                    minlength: 3,
                    maxlength: 50
                },
                email: {
                    required: true,
                    emailregex: true,
                },
                password: {
                    required: true,
                },
                phone: {
                    required: true,
                    digits: true,
                },
                location: {
                    required: true,
                },
                latitude: {
                    required: true,
                },
                longitude: {
                    required: true,
                }
            },
            messages:
                    {
                        firstname: {
                            required: "Please enter first name",
                        },
                        lastname: {
                            required: "Please enter last name",
                        },
                        email: {
                            required: "Please enter email address",
                        },
                        password: {
                            required: "Please enter password",
                        },
                        phone: {
                            required: "Please enter phone number",
                            digits: "Please specify a valid phone number",
                        },
                        location: {
                            required: "Please enter city/country",
                        },
                        latitude: {
                            required: "Please enter latitude",
                        },
                        longitude: {
                            required: "Please enter longitude",
                        }
                    },
        });

        $('.callout-danger').delay(3000).hide('700');
        $('.callout-success').delay(3000).hide('700');
    });
</script>
