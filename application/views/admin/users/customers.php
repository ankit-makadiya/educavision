<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo $module_name; ?>
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('admin/dashboard'); ?>">
                    <i class="fa fa-dashboard"></i>
                    Home
                </a>
            </li>
            <li class="active"><?php echo $module_name; ?></li>
        </ol>
    </section>
    <section class="content">
        <div class="row" >
            <div class="col-xs-12" >
                <?php if ($this->session->flashdata('success')) { ?>
                    <div class="callout callout-success">
                        <p><?php echo $this->session->flashdata('success'); ?></p>
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('error')) { ?>
                    <div class="callout callout-danger">
                        <p><?php echo $this->session->flashdata('error'); ?></p>
                    </div>
                <?php } ?>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Manage Customers</h3>
                        <?php if(in_array('customers-create', $admin_permission)){ ?>
                        <!-- <div class="pull-right">
                            <a href="<?php // echo base_url('admin/users/add'); ?>" class="btn btn-primary pull-right">Add User</a>
                        </div> -->
                        <?php } ?>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="data-table" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email Address</th>
                                    <th>Phone Number</th>
                                    <th>Status</th>
                                    <!-- <th>Action</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($users_list as $users) {
                                    if ($users['status'] == 1) {
                                        $users_status = 'Active';
                                    } elseif ($users['status'] == 0) {
                                        $users_status = 'Inactive';
                                    }
                                    ?>
                                    <tr>
                                        <td><?php echo $users['id']; ?></td>
                                        <td><?php echo $users['firstname']; ?></td>
                                        <td><?php echo $users['lastname']; ?></td>
                                        <td><a href="mailto:<?php echo $users['email']; ?>"><?php echo $users['email']; ?></a></td>
                                        <td><?php echo $users['phone']; ?></td>
                                        <td><?php echo $users_status ?></td>
                                        <?php /* ?>
                                        <td>
                                            <?php if(in_array('users-edit', $admin_permission)){ ?>
                                            <a href="<?php echo base_url('admin/users/edit/' . $users['id']); ?>" id="edit_btn" title="Edit Users">
                                                <button type="button" class="btn btn-primary" style="margin-top: 3px;"><i class="fa fa-pencil-square-o"></i></button>
                                            </a>
                                            <?php } ?>
                                            <?php if(in_array('users-delete', $admin_permission)){ ?>
                                            <a data-href="<?php echo base_url('admin/users/delete/' . $users['id']); ?>" id="delete_btn" data-toggle="modal" data-target="#confirm-delete" href="#" title="Delete Users">
                                                <button type="button" class="btn btn-primary" style="margin-top: 3px;"><i class="icon-trash"></i> <i class="fa fa-trash-o"></i></button>
                                            </a>
                                            <?php } ?>
                                        </td>
                                        <?php */ ?>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                </tbody>
                <tfoot>

                </tfoot>
                </table>
            </div><!-- /.box -->


        </div><!-- /.col -->
</div><!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="frm_title">Delete Conformation</h4>
            </div>
            <div class="modal-body">
                Are you sure want to delete this users?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a href="#" class="btn btn-danger danger">Delete</a>
            </div>
        </div>
    </div>
</div>

<!-- page script -->
<script type="text/javascript">
    $(function () {
        $("#data-table").DataTable({
            order: [[0, "desc"]],
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        $('#confirm-delete').on('show.bs.modal', function (e) {
            $(this).find('.danger').attr('href', $(e.relatedTarget).data('href'));
        });

        $('#search_frm').submit(function () {
            var value = $('#search_keyword').val();
            if (value == '')
                return false;
        });

        $('.callout-danger').delay(3000).hide('700');
        $('.callout-success').delay(3000).hide('700');
    });
</script>
