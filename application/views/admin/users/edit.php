<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo $module_name; ?>
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('admin/dashboard'); ?>">
                    <i class="fa fa-dashboard"></i>
                    Home
                </a>
            </li>
            <li class="active"><?php echo $module_name; ?></li>
        </ol>
    </section>
    <section class="content-header">
        <?php if ($this->session->flashdata('success')) { ?>
            <div class="callout callout-success">
                <p><?php echo $this->session->flashdata('success'); ?></p>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>  
            <div class="callout callout-danger" >
                <p><?php echo $this->session->flashdata('error'); ?></p>
            </div>
        <?php } ?>

    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-md-12">

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo $section_title; ?></h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php
                    $attributes = array('id' => 'edit_user_frm', 'enctype' => 'multipart/form-data');
                    $hidden = array('id' => $user_detail[0]['id']);
                    echo form_open_multipart('admin/users/edit', $attributes, $hidden);
                    ?>

                    <div class="box-body">
                        <div class="form-group col-sm-10">
                            <label for="username" id="label_username">User Name*</label>
                            <?php
                            $data = array(
                                'name' => 'username',
                                'id' => 'username',
                                'value' => $user_detail[0]['username'],
                                'class' => 'form-control'
                            );
                            echo form_input($data);
                            ?>
                        </div>
                        <div class="form-group col-sm-10">
                            <label for="name" id="label_name">Name*</label>
                            <?php
                            $data = array(
                                'name' => 'name',
                                'id' => 'name',
                                'value' => $user_detail[0]['name'],
                                'class' => 'form-control'
                            );
                            echo form_input($data);
                            ?>
                        </div>
                        <div class="form-group col-sm-10">
                            <label for="email" id="label_email">Email*</label>
                            <?php
                            $data = array(
                                'name' => 'email',
                                'id' => 'email',
                                'value' => $user_detail[0]['email'],
                                'readonly' => 'readonly',
                                'class' => 'form-control'
                            );

                            echo form_input($data);
                            ?>
                        </div>
                        <div class="form-group col-sm-10">
                            <label for="role" id="label_role">Role*</label>
                            <select class="form-control role" name="role" id="role" style="width: 100%;" tabindex="-1" required="" >
                            <?php foreach ($role_data as $role) { ?>
                                <option value="<?php echo $role['id'] ?>" <?php if($role['id'] == $user_detail[0]['role_id']){ echo 'selected="selected"'; } ?>><?php echo $role['name'] ?></option>
                            <?php } ?>
                            </select>
                        </div>
                        <div class="form-group col-sm-10">
                            <label for="password" id="page_title">Reset Password</label>
                            <?php
                            $data = array(
                                'name' => 'reset_password',
                                'id' => 'reset_password',
                                'value' => '',
                                'class' => 'form-control'
                            );

                            echo form_password($data);
                            ?>
                        </div>
                        <div class="form-group col-sm-10">
                            <label for="confirm_password" id="page_title">Confirm Password</label>
                            <?php
                            $data = array(
                                'name' => 'confirm_password',
                                'id' => 'confirm_password',
                                'value' => '',
                                'class' => 'form-control'
                            );

                            echo form_password($data);
                            ?>
                        </div>
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <?php
                        $save_attr = array('id' => 'btn_save', 'name' => 'btn_save', 'value' => 'Save', 'class' => 'btn btn-primary');
                        echo form_submit($save_attr);
                        ?>    
                        <button type="button" onclick="window.history.back();" class="btn btn-default">Back</button>
                        <!--<button type="submit" class="btn btn-info pull-right">Sign in</button>-->
                    </div><!-- /.box-footer -->
                    <?php echo form_close(); ?>
                </div><!-- /.box -->


            </div><!--/.col (right) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<div class="modal fade" id="confirm-remove" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="frm_title">Delete Conformation</h4>
            </div>
            <div class="modal-body">
                Are you sure want to remove this photo?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a href="#" class="btn btn-danger danger">Confirm</a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    //validation for edit email formate form
    $(document).ready(function () {

        $('#confirm-remove').on('show.bs.modal', function (e) {
            $(this).find('.danger').attr('href', $(e.relatedTarget).data('href'));
        });

        $("#edit_user_frm").validate({
            rules: {
                name: {
                    required: true,
                    minlength: 3,
                    maxlength: 50
                },
                reset_password: {
                    minlength: 5,
                    maxlength: 50
                },
                confirm_password: {
                    equalTo: "#reset_password"
                },
                email: {
                    required: true,
                    emailregex: true,
                },
                phone: {
                    required: true,
                    digits: true,
                },
                location: {
                    required: true,
                },
                latitude: {
                    required: true,
                },
                longitude: {
                    required: true,
                }
            },
            messages:
                    {
                        name: {
                            required: "Please enter user name",
                        },
                        confirm_password: {
                            equalTo: "Please enter the same password again",
                        },
                        email: {
                            required: "Please enter email address",
                        },
                        phone: {
                            required: "Please enter phone number",
                            digits: "Please specify a valid phone number",
                        },
                        location: {
                            required: "Please enter city/country",
                        },
                        latitude: {
                            required: "Please enter latitude",
                        },
                        longitude: {
                            required: "Please enter longitude",
                        }
                    },
        });

        $('.callout-danger').delay(3000).hide('700');
        $('.callout-success').delay(3000).hide('700');
    });
</script>
