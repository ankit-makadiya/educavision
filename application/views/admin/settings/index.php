<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo $module_name; ?>
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('admin/dashboard'); ?>">
                    <i class="fa fa-dashboard"></i>
                    Home
                </a>
            </li>
            <li class="active"><?php echo $module_name; ?></li>
        </ol>
    </section>
    <section class="content-header">
        <?php if ($this->session->flashdata('success')) { ?>
            <div class="callout callout-success">
                <p><?php echo $this->session->flashdata('success'); ?></p>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>  
            <div class="callout callout-danger" >
                <p><?php echo $this->session->flashdata('error'); ?></p>
            </div>
        <?php } ?>

    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-md-12">

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo $section_title; ?></h3>
                    </div>
                    <?php
                    $form_attr = array('id' => 'setting_form', 'name' => 'setting_form', 'class' => 'form-horizontal row-border');
                    echo form_open('admin/settings/edit', $form_attr);
                    ?>
                    <div class="box-body">
                        <?php
                        foreach ($setting_list as $list) {
                            ?>
                            <div class="form-group col-sm-10">
                                <div class="col-sm-2"><label for="name" id="page_title"><?php echo $list['setting_name'] ?>*</label></div>
                                <?php if ($list['setting_key'] == 'site_logo') { ?>
                                    <div class="col-md-8">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" id="siteLogo" style="width: 200px; height: 195px;"><img src="<?php echo $list['setting_val'] ?>" alt="Site Logo" title="Site Logo" /></div>
                                            <div>
                                                <button type="button" id="site_logos" name="site_logos" class="btn btn-primary btn-file fileinput-new"  onclick="openPopup(this.id)" tabindex="2">Select Image</button>
                                                <!--<button type="button" id="remove_news_images" name="remove_news_images" class="btn btn-default"  onclick="removeImage(this.id)" tabindex="2">Remove</button>-->
                                                <a class="popovers" data-container="body" data-trigger="hover" data-placement="top" data-html="true" <span aria-hidden="true" class="icon-info"></span></a>
                                            </div>
                                            <input type="hidden" id="imageName" name="site_logo" value="<?php echo $list['setting_val'] ?>" />
                                        </div>
                                    </div>
                                <?php } elseif ($list['setting_key'] == 'site_home_page') {
                                    ?>
                                    <div class="col-md-8">
                                        <select class="form-control site_home_page" name="site_home_page" id="site_home_page" style="width: 100%;" tabindex="-1" >
                                            <?php foreach ($pageData as $page) { ?>
                                                <option value="<?php echo $page['id'] ?>" <?php if ($page['id'] == $list['setting_val']) {
                                        echo 'selected="selected"';
                                    } ?>><?php echo $page['name'] ?></option>
                                    <?php } ?>
                                        </select>
                                    </div>
                                    <?php } else {
                                        ?>
                                    <div class="col-sm-8">
                                        <?php
                                        $data = array(
                                            'name' => $list['setting_key'],
                                            'id' => 'site_title',
                                            'value' => $list['setting_val'],
                                            'class' => 'form-control'
                                        );
                                        echo form_input($data);
                                        ?>
                                        <span style="font-size: 11px;">
                                            <i>
                                                <?php
                                                if ($list['setting_key'] == 'site_email') {
                                                    echo 'This email id show in website';
                                                } elseif ($list['setting_key'] == 'site_sender_email') {
                                                    echo 'All mail send from this email id';
                                                } elseif ($list['setting_key'] == 'site_receiver_email') {
                                                    echo 'All mail notification got on this email id';
                                                }
                                                ?>
                                            </i>
                                        </span>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="box-footer">
                        <?php
                        $save_attr = array('id' => 'btn_save', 'name' => 'btn_save', 'value' => 'Save', 'class' => 'btn btn-primary');
                        echo form_submit($save_attr);
                        ?>    
                        <button type="button" onclick="window.history.back();" class="btn btn-default">Back</button>
                    </div>
<?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    $(document).ready(function () {
        $('.callout-danger').delay(3000).hide('700');
        $('.callout-success').delay(3000).hide('700');
        $("#setting_form").validate({
            rules: {
                site_title: {
                    required: true,
                },
                site_url: {
                    required: true,
                },
                site_owner: {
                    required: true,
                },
//                site_email: {
//                    required: true,
//                },
                site_sender_email: {
                    required: true,
                },
                site_receiver_email: {
                    required: true,
                },
                preferences: {
                    required: true,
                },
                smtp_host_name: {
                    required: true,
                },
                smtp_out_going_port: {
                    required: true,
                },
                smtp_user_name: {
                    required: true,
                },
                smtp_password: {
                    required: true,
                },
            },
            messages:
                    {
                        site_title: {
                            required: "Please enter site title",
                        },
                        site_url: {
                            required: "Please enter site url",
                        },
                        site_owner: {
                            required: "Please enter site owner",
                        },
//                        site_email: {
//                            required: "Please enter site email",
//                        },
                        site_sender_email: {
                            required: "Please enter site sender email",
                        },
                        site_receiver_email: {
                            required: "Please enter site receiver email",
                        },
                        preferences: {
                            required: "Please enter preferences",
                        },
                        smtp_host_name: {
                            required: "Please enter smtp host name",
                        },
                        smtp_out_going_port: {
                            required: "Please enter smtp out going port",
                        },
                        smtp_user_name: {
                            required: "Please enter smtp user name",
                        },
                        smtp_password: {
                            required: "Please enter smtp password",
                        }
                    },
        });
    });

    function openPopup(id) {
        var baseUrl = '<?php echo base_url() ?>';
        //$('#or_imge').hide();
        //document.getElementById('form_mainImage').onchange = function () {
        window.KCFinder = {
            callBackMultiple: function (files) {
                window.KCFinder = null;
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    var alt = file.split('/').pop();
                    var fullimage_name = baseUrl + file;
                    var error_msg = 'true';
                    var width;
                    var imgLoader = new Image();
                    imgLoader.src = fullimage_name;
                    var name_separater = "|";
                    var path = file;
                    var cid = parseInt(i) + 100;

                    // return false;
                    if (id != '' && id != undefined) {
                        if (id == 'site_logos') {
                            $('#siteLogo').html('<img src="' + path + '">');
                            $('#setting_form').find('#imageName').val(path);
                        }
                    }
                    // Log image data:
                }

            },
        };

        window.open('/assets/global/plugins/kcfinder/browse.php?type=images&dir=images/public',
                'kcfinder_image', 'status=0, toolbar=0, location=0, menubar=0, ' +
                'directories=0, resizable=1, scrollbars=0, width=800, height=600'
                );
    }
</script>
