<link href="<?php echo base_url() ?>assets/admin/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			<?php echo $module_name; ?>
			<small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li>
				<a href="<?php echo base_url('admin/dashboard'); ?>">
					<i class="fa fa-dashboard"></i>
					Home
				</a>
			</li>
			<li class="active"><?php echo $module_name; ?></li>
		</ol>
	</section>

	<section class="content-header">
		<?php if ($this->session->flashdata('success')) { ?>
			<div class="callout callout-success">
				<p><?php echo $this->session->flashdata('success'); ?></p>
			</div>
		<?php } ?>
		<?php if ($this->session->flashdata('error')) { ?>
			<div class="callout callout-danger">
				<p><?php echo $this->session->flashdata('error'); ?></p>
			</div>
		<?php } ?>

	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">

			<div class="col-md-12">

				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo $section_title; ?></h3>
					</div><!-- /.box-header -->
					<!-- form start -->
					<div class="box-body">
						<div class="portlet-body">
							<form name="form" method="post" class="form-horizontal" id="coupon_form"
								  data-toggle="validator" enctype="multipart/form-data">
								<div class="tabbable-line">
									<ul class="nav nav-tabs ">
										<li class="active"><a href="#general" data-toggle="tab" title="General Setting"><i
													class="fa fa-list"></i> General Setting</a></li>
										<li class=""><a href="#price" data-toggle="tab" title="Price Setting"><i
													class="fa fa-money"></i> Price Setting</a></li>
										<!-- <li class=""><a href="#restriction" data-toggle="tab"
														title="Restriction Setting"><i class="fa fa-lock"></i>
												Restriction Setting</a></li> -->
									</ul>
									<div class="tab-content">
										<div class="tab-pane active" id="general">
											<div class="form-group">
												<label class="control-label col-md-3">Coupon Name<span class="required"> * </span></label>
												<div class="col-md-8">
													<input type="text" id="coupon_name" name="coupon_name"
														   required="required" class="form-control"
														   placeholder="Enter Coupon Name" maxlength="100"
														   tabindex="1"/>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Coupon Code<span class="required"> * </span></label>
												<div class="col-md-3">
													<input type="text" id="coupon_code" name="coupon_code"
														   required="required" class="form-control input-medium"
														   placeholder="Enter Coupon Code" maxlength="50" tabindex="2"/>
												</div>
												<div class="col-md-5">
													<button type="button" class="btn green autocode" name="autocode"
															id="autocode" tabindex="8">Generate Code
													</button>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Start Date<span class="required"> * </span></label>
												<div class="col-md-8">
													<input type="text" name="start_date" id="start_date"
														   class="form-control input-medium datepicker" tabindex="3"
														   maxlength="10" placeholder="Select Coupon Start Date"
														   value=""/>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">End Date<span
														class="required"> * </span></label>
												<div class="col-md-8">
													<input type="text" name="end_date" id="end_date"
														   class="form-control input-medium datepicker" tabindex="4"
														   maxlength="10" placeholder="Select Coupon End Date" value=""
													/>
												</div>
											</div>
										</div>
										<div class="tab-pane" id="price">
											<fieldset>
												<legend>Discount</legend>
											</fieldset>
											<div class="form-group">
												<label class="control-label col-md-3">Offer Type</label>
												<div class="col-md-8">
													<input type="checkbox" name="discount_offer_type"
														   id="discount_offer_type" tabindex="5" class="make-switch"
														   data-on-text="% OFF" data-off-text="$ OFF" value="1"
														   checked="checked"/>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Offer Value<span class="required"> * </span></label>
												<div class="col-md-8">
													<input type="text" name="discount_offer_value"
														   id="discount_offer_value" class="form-control offerValue"
														   tabindex="6" maxlength="6" placeholder="Enter Offer Value"
														   value=""
														   onkeypress="return goodchars(event,'0123456789.');"/>
												</div>
											</div>
											<fieldset>
												<legend>Shipping</legend>
											</fieldset>
											<div class="form-group">
												<label class="control-label col-md-3">Offer Type</label>
												<div class="col-md-8">
													<input type="checkbox" name="shipping_offer_type"
														   id="shipping_offer_type" tabindex="7" class="make-switch"
														   data-on-text="% OFF" data-off-text="$ OFF" value="1"
														   checked="checked"/>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Offer Value<span class="required"> * </span></label>
												<div class="col-md-8">
													<input type="text" name="shipping_offer_value"
														   id="shipping_offer_value" class="form-control offerValue"
														   tabindex="8" maxlength="6" placeholder="Enter Offer Value"
														   value=""
														   onkeypress="return goodchars(event,'0123456789.');"/>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Free Shipping</label>
												<div class="col-md-8">
													<input type="checkbox" name="free_shipping" id="free_shipping"
														   tabindex="9" class="make-switch" data-on-text="Yes"
														   data-off-text="No" value="1"/>
												</div>
											</div>
											<fieldset>
												<legend>Tax</legend>
											</fieldset>
											<div class="form-group">
												<label class="control-label col-md-3">Offer Type</label>
												<div class="col-md-8">
													<input type="checkbox" name="tax_offer_type" id="tax_offer_type"
														   tabindex="10" class="make-switch" data-on-text="% OFF"
														   data-off-text="$ OFF" value="1" checked="checked"/>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Offer Value<span class="required"> * </span></label>
												<div class="col-md-8">
													<input type="text" name="tax_offer_value" id="tax_offer_value"
														   class="form-control offerValue" tabindex="11" maxlength="6"
														   placeholder="Enter Offer Value" value=""
														   onkeypress="return goodchars(event,'0123456789.');"/>
												</div>
											</div>
										</div>
										<!-- <div class="tab-pane" id="restriction">
											<div class="form-group">
												<label class="control-label col-md-3">Allow Multiple Usage per
													Customer?</label>
												<div class="col-md-8">
													<input type="checkbox" name="allow_per_email" id="allow_per_email"
														   tabindex="12" class="make-switch" data-on-text="Yes"
														   data-off-text="No" value="1" checked="checked"/>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Unlimited Usage?</label>
												<div class="col-md-8">
													<input type="checkbox" name="unlimited_usage" id="unlimited_usage"
														   tabindex="13" class="make-switch" data-on-text="Yes"
														   data-off-text="No" value="1"/>
												</div>
											</div>
											<div class="form-group nousage">
												<label class="control-label col-md-3">No. of Usage<span
														class="required"> * </span></label>
												<div class="col-md-8">
													<input type="text" name="no_of_usage" id="no_of_usage"
														   class="form-control" tabindex="33" value="" maxlength="10"
														   placeholder="Enter No. of Coupon Usage"
														   onkeypress="return goodchars(event,'0123456789.');"/>
												</div>
											</div>
										</div> -->
										<div class="row">
											<div class="col-md-offset-3 col-md-8">
												<button type="submit" class="btn green newcoupon" name="frmSubmit"
														id="frmSubmit" tabindex="14">Submit
												</button>
												<a href="/admin/couponcode" type="button" class="btn default"
												   tabindex="15">Cancel</a>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div><!-- /.box -->


			</div><!--/.col (right) -->
		</div>   <!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript" src="<?php echo base_url() ?>/assets/admin/js/bootstrap-switch.min.js"></script>
<script type="text/javascript">
    //validation for edit email formate form
    $(document).ready(function () {
        $('.bootstrap3-editor').wysihtml5();
        $.validator.addMethod("greaterThan",
            function (value, element, param) {
                var $otherElement = $(param);
                return parseInt(value, 10) > parseInt($otherElement.val(), 10);
            });

        $.validator.addMethod("lessThan",
            function (value, element, param) {
                var $otherElement = $(param);
                return parseInt(value, 10) < parseInt($otherElement.val(), 10);
            });

        $("#coupon_form").validate({
            ignore: [],
            debug: false,
            rules: {
                coupon_name: {
                    required: true,
                },
                coupon_code: {
                    required: true,
                },
                start_date: {
                    required: true,
                    //lessThan: "#end_date",
                },
                end_date: {
                    required: true,
                    //greaterThan: "#start_date",
                },
                discount_offer_value: {
                    required: true,
                },
                shipping_offer_value: {
                    required: true,
                },
                tax_offer_value: {
                    required: true,
                },
                // no_of_usage: {
                //     required: true,
                // },
            },
            messages: {
                coupon_name: {
                    required: "Please enter coupon name.",
                },
                coupon_code: {
                    required: "Please enter coupon code.",
                },
                start_date: {
                    required: "Please enter start date.",
                    //lessThan: "Please enter valid date.",
                },
                end_date: {
                    required: "Please enter end date.",
                    //greaterThan: "Please enter valid date.",
                },
                discount_offer_value: {
                    required: "Please enter discount offer value.",
                },
                shipping_offer_value: {
                    required: "Please enter shipping offer value.",
                },
                tax_offer_value: {
                    required: "Please enter tax offer value.",
                },
                // no_of_usage: {
                //     required: "Please enter number of usage.",
                // },
            },
        });
    });
    if ($("#free_shipping").size() > 0) {
        $("#free_shipping").bootstrapSwitch({
            'onSwitchChange': function (event, state) {
                if (state == true) {
                    $("#shipping_offer_type").prop("checked", true);
                    $("[name='shipping_offer_type']").bootstrapSwitch('disabled', true);
                    $("#shipping_offer_value").val(100);
                    $("#shipping_offer_value").attr('readonly', true);
                } else {
                    $("[name='shipping_offer_type']").bootstrapSwitch('disabled', false);
                    $("#shipping_offer_value").val('');
                    $("#shipping_offer_value").attr('readonly', false);
                }
                return false;
            },
        });
    }

    if ($("#unlimited_usage").size() > 0) {
        $("#unlimited_usage").bootstrapSwitch({
            'onSwitchChange': function (event, state) {
                if (state == true) {
                    $(".nousage").addClass("hide");
                    $(".nousage").removeClass("show");
                    $("#no_of_usage").val("");
                } else {
                    $(".nousage").addClass("show");
                    $(".nousage").removeClass("hide");
                    $("#no_of_usage").val("");
                }
                return false;
            },
        });
    }
    if ($("#discount_offer_type").size() > 0) {
        $("#discount_offer_type").bootstrapSwitch();
    }
    if ($("#shipping_offer_type").size() > 0) {
        $("#shipping_offer_type").bootstrapSwitch();
    }
    if ($("#tax_offer_type").size() > 0) {
        $("#tax_offer_type").bootstrapSwitch();
    }
    $("#allow_per_email").bootstrapSwitch();

    function getkey(e) {
        if (window.event)
            return window.event.keyCode;
        else if (e)
            return e.which;
        else
            return null;
    }

    function goodchars(e, goods) {
        var key, keychar;
        key = getkey(e);
        if (key == null) return true;
        // get character
        keychar = String.fromCharCode(key);
        keychar = keychar.toLowerCase();
        goods = goods.toLowerCase();
        // check goodkeys
        if (goods.indexOf(keychar) != -1)
            return true;
        // control keys
        if (key == null || key == 0 || key == 8 || key == 9 || key == 13 || key == 27)
            return true;
        // else return false
        return false;
    }

    function slugify(string) {
        return string
            .toString()
            .trim()
            .toLowerCase()
            .replace(/\s+/g, "-")
            .replace(/[^\w\-]+/g, "")
            .replace(/\-\-+/g, "-")
            .replace(/^-+/, "")
            .replace(/-+$/, "");
    }

    $(".autocode").click(function () {
        var CouponCode = GenerateCode();
        $("#coupon_code").val(CouponCode);
    });

    function GenerateCode(m) {
        var m = m || 8;
        s = '', r = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789';
        for (var i = 0; i < m; i++) {
            s += r.charAt(Math.floor(Math.random() * r.length));
        }
        return s;
    };

    $( function() {
        $("#start_date").datepicker({
            numberOfMonths: 1,
            dateFormat: "yy-mm-dd",
            onSelect: function(selected) {
                $("#end_date").datepicker("option","minDate", selected)
            }
        });
        $("#end_date").datepicker({
            numberOfMonths: 1,
            dateFormat: "yy-mm-dd",
            onSelect: function(selected) {
                $("#start_date").datepicker("option","maxDate", selected)
            }
        });
    });

</script>
