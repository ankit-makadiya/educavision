<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo $module_name; ?>
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('admin/dashboard'); ?>">
                    <i class="fa fa-dashboard"></i>
                    Home
                </a>
            </li>
            <li class="active"><?php echo $module_name; ?></li>
        </ol>
    </section>
    <section class="content-header">
        <?php if ($this->session->flashdata('success')) { ?>
            <div class="callout callout-success">
                <p><?php echo $this->session->flashdata('success'); ?></p>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>  
            <div class="callout callout-danger" >
                <p><?php echo $this->session->flashdata('error'); ?></p>
            </div>
        <?php } ?>

    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-md-12">

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo $section_title; ?></h3>
                    </div><!-- /.box-header -->
                    <div class="container">
                        <div class="col-md-6">
                            <table class="table table-bordered" style="padding:20px;">
                                <tr><td><b>Coupon Name</b></td><td><?php  echo $coupon_data[0]['name']; ?></td></tr>
                                <tr><td><b>Coupon Code</b></td><td><?php  echo $coupon_data[0]['coupon_code']; ?></td></tr>
                                <tr><td><b>Start Date</b></td><td><?php  echo $coupon_data[0]['start_date']; ?></td></tr>
                                <tr><td><b>End Date</b></td><td><?php  echo $coupon_data[0]['end_date']; ?></td></tr>
                                <tr><td><b>On Discount</b></td><td><?php  echo $coupon_pricing_data[0]['offer_vale'] . ' ' . $coupon_pricing_data[0]['offer_type'] ; ?></td></tr>
                                <tr><td><b>On Shipping</b></td><td><?php  echo $coupon_pricing_data[1]['offer_vale'] . ' ' . $coupon_pricing_data[1]['offer_type'] ; ?></td></tr>
                                <tr><td><b>On Tax</b></td><td>
                                    <?php  echo $coupon_pricing_data[2]['offer_vale'] . ' ' . $coupon_pricing_data[2]['offer_type'] ; ?></td></tr>
                            </table>
                        </div>
                    </div>
                </div><!-- /.box -->


            </div><!--/.col (right) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
    //validation for edit email formate form
    $(document).ready(function () {
        $('.bootstrap3-editor').wysihtml5();
    
        $("#edit_payments_frm").validate({
            rules: {
                payment_title: {
                    required: true,
                },
                payment_cont: {
                    required: true,
                    minlength: 100
                }
            },
            messages:
            {
                payment_title: {
                    required: "Please enter payment title",
                },
                payment_cont: {
                    required: "Please enter payment content",
                }
            },
        });
        
        $('.callout-danger').delay(3000).hide('700');
        $('.callout-success').delay(3000).hide('700');
    });
</script>

