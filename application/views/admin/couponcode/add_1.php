<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo $module_name; ?>
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li>    
                <a href="<?php echo base_url('admin/dashboard'); ?>">
                    <i class="fa fa-dashboard"></i>
                    Home
                </a>
            </li>
            <li class="active"><?php echo $module_name; ?></li>
        </ol>
    </section>

    <section class="content-header">
        <?php if ($this->session->flashdata('success')) { ?>
            <div class="callout callout-success">
                <p><?php echo $this->session->flashdata('success'); ?></p>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>  
            <div class="callout callout-danger" >
                <p><?php echo $this->session->flashdata('error'); ?></p>
            </div>
        <?php } ?>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-md-12">

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo $section_title; ?></h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php
                    $form_attr = array('id' => 'add_coupon_frm', 'enctype' => 'multipart/form-data');
                    echo form_open_multipart('admin/couponcode/add', $form_attr);
                    ?>
                    <div class="box-body">
                        <div class="form-group col-sm-10">
                            <label for="coupon_name" class="col-sm-2 control-label">Coupon Name*</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="coupon_name" id="coupon_name" placeholder="Enter Coupon Name">
                            </div>
                        </div>
                        <div class="form-group col-sm-10">
                            <label for="description" class="col-sm-2 control-label">Description*</label>
                            <div class="col-sm-10">
                                <textarea class="form-control ckeditor" id="description" name="description" rows="10" cols="80"></textarea>
                            </div>
                        </div>
                        <div class="form-group col-sm-10">
                            <label class="control-label col-md-2">Image*</label>
                            <div class="col-md-8">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" id="couponImage" style="width: 200px; height: 195px;"> </div>
                                    <div>
                                        <button type="button" id="coupon_images" name="coupon_images" class="btn btn-primary btn-file fileinput-new"  onclick="openPopup(this.id)" tabindex="2">Select Image</button>

                                        <button type="button" id="remove_coup_images" name="remove_coup_images" class="btn btn-default"  onclick="removeImage(this.id)" tabindex="2">Remove</button>
                                        <a class="popovers" data-container="body" data-trigger="hover" data-placement="top" data-html="true" <span aria-hidden="true" class="icon-info"></span></a>
                                    </div>
                                    <input type="hidden" id="imageName" name="coupon_image" />
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-sm-10">
                            <label for="lable_slug" name="start_date" id="coupon_start_date" class="col-sm-2 control-label">Start Date*</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="start_date" id="start_date" placeholder="Enter Price from">
                            </div>
                            <label for="lable_slug" name="end_date" id="coupon_end_date" class="col-sm-2 control-label">End Date*</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="end_date" id="end_date" placeholder="Enter Price to">
                            </div>
                        </div>
                        <div class="form-group col-sm-10">
                            <label for="is_visible" id="coupon_is_visible" class="col-sm-2 control-label">Is Visible Front?</label>
                            <div class="col-sm-10">
                                <input type="checkbox" name="is_visible" id="is_visible" value="1">
                            </div>
                        </div>
                        <div class="form-group col-sm-10">
                            <label for="lable_category"  class="col-sm-2 control-label">Select Coupon Type*</label>
                            <div class="col-sm-10">
                                <select class="form-control coupon_type select2" name="coupon_type" id="coupon_type" style="width: 100%;" tabindex="-1" required="" >
                                    <option value="">Select CouponType</option>
                                    <option value=""></option>

                                </select>
                            </div>
                        </div>

                        <div class="form-group col-sm-10">
                            <label for="lable_category"  class="col-sm-2 control-label">Select Offer Type*</label>
                            <div class="col-sm-10">
                                <select class="form-control offer_type select2"  name="offer_type" id="offer_type" style="width: 100%;" tabindex="-1" required="" >
                                    <option value="">Select Offer Type</option>
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group col-sm-10">
                            <label for="amount" class="col-sm-2 control-label">Offer Value*</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="offer_value" id="offer_value" placeholder="Enter Offer Value">
                            </div>
                        </div>

                        <div class="form-group col-sm-10">
                            <label for="is_free_shipping" id="is_free_shipping" class="col-sm-2 control-label">Is Free Shipping?</label>
                            <div class="col-sm-10">
                                <input type="checkbox" name="free_shipping" id="free_shipping" value="1">
                            </div>
                        </div>

                        <div class="form-group col-sm-10">
                            <label for="installment" id="allow_installment" class="col-sm-2 control-label">Allow Installment?</label>
                            <div class="col-sm-10">
                                <input type="checkbox" name="installment" id="installment" value="1">
                            </div>
                        </div>      

                        <div class="form-group col-sm-10">
                            <label for="full_payment" id="allow_fullpaymnet" class="col-sm-2 control-label">Allow Fullpayment?</label>
                            <div class="col-sm-10">
                                <input type="checkbox" name="full_payment" id="full_payment" value="1">
                            </div>
                        </div>
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <?php
                        $save_attr = array('id' => 'btn_save', 'name' => 'btn_save', 'value' => 'Save', 'class' => 'btn btn-primary');
                        echo form_submit($save_attr);
                        ?>    
                        <button type="button" onclick="window.history.back();" class="btn btn-default">Back</button>
                    </div><!-- /.box-footer -->
                    </form>
                </div><!-- /.box -->


            </div><!--/.col (right) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">
    //validation for edit email formate form
    $(document).ready(function () {
        $('.bootstrap3-editor').wysihtml5();
        $.validator.addMethod("greaterThan",
                function (value, element, param) {
                    var $otherElement = $(param);
                    return parseInt(value, 10) > parseInt($otherElement.val(), 10);
                });

        $.validator.addMethod("lessThan",
                function (value, element, param) {
                    var $otherElement = $(param);
                    return parseInt(value, 10) < parseInt($otherElement.val(), 10);
                });

        $("#add_couspon_frm").validate({
            ignore: [],
            debug: false,
            rules: {
                coupon_name: {
                    required: true,
                },
                coupon_image: {
                    required: true,
                },
                description: {
                    required: true,
                },
                is_visible: {
                    required: true,
                },
                is_visible: {
                    required: true,
                },
                        start_date: {
                            required: true,
                            lessThan: "#end_date",
                        },
                end_date: {
                    required: true,
                    lessThan: "#start_date",
                },
                coupon_type: {
                    required: true,
                },
                offer_type: {
                    required: true,
                },
                offer_value: {
                    required: true,
                },
                free_shipping: {
                    required: true,
                },
                installment: {
                    required: true,
                },
                full_payment: {
                    required: true,
                },
            },
            messages: {
                country_id: {
                    required: "Please select country id.",
                },
                price_from: {
                    required: "Please enter price from.",
                    number: "Please enter number only.",
                    lessThan: "Value must be lessthan price to.",
                },
                price_to: {
                    required: "Please enter price to.",
                    number: "Please enter number only.",
                    greaterThan: "Value must be graterthan price from.",
                },
                amount: {
                    required: "Please enter amount.",
                    number: "Please enter number only.",
                },
            },
        });

        $(".offer_type").select2({
            placeholder: "Select Offer Type",
            allowClear: true,
            width: '100%'
        });
        $(".coupon_type").select2({
            placeholder: "Select Coupon Type",
            allowClear: true,
            width: '100%'
        });
    });
</script>