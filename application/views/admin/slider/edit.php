<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo $module_name; ?>
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('dashboard'); ?>">
                    <i class="fa fa-dashboard"></i>
                    Home
                </a>
            </li>
            <li class="active"><?php echo $module_name; ?></li>
        </ol>
    </section>
    <section class="content-header">
        <?php if ($this->session->flashdata('success')) { ?>
            <div class="callout callout-success">
                <p><?php echo $this->session->flashdata('success'); ?></p>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>  
            <div class="callout callout-danger" >
                <p><?php echo $this->session->flashdata('error'); ?></p>
            </div>
        <?php } ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo $section_title; ?></h3>
                    </div>
                    <?php
                    $form_attr = array('id' => 'edit_slider_frm', 'enctype' => 'multipart/form-data');
                    echo form_open_multipart('admin/slider/edit', $form_attr);
                    ?>
                    <input type="hidden" name="commasapareted_images_name" id="commasapareted_images_name" value="<?php echo $commasapareted_images_name ?>" >
                    <?php 
                    /*foreach($slider_img_data as $sliderImage){ ?>
                        <input type="hidden" id="sliderimg_<?php echo $sliderImage['id'] ?>" name="slider_image_id[]" value="<?php echo $sliderImage['id'] ?>">
                    <?php } */ ?>
                    <input type="hidden" name="row_count" id="row_count" value="<?php echo $hdn_row_count ?>">
                    <input type="hidden" name="slider_id" value="<?php echo $slider_detail[0]['id'] ?>">
                    <div class="box-body">
                        <div class="form-group col-sm-10">
                            <label for="lable_name" name="lable_name" id="lable_name" class="col-sm-2 control-label">Slider Name*</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name" id="name" placeholder="Enter Slider Name" value="<?php echo $slider_detail[0]['name'] ?>">
                            </div>
                        </div>
                        <div class="form-group col-sm-10">
                            <label class="control-label col-md-2 control-label">Slider*</label>
                            <div class="col-md-3">
                                <button type="button" id="slider_images" name="slider_images" class="form-control"  onclick="openPopup()" tabindex="3">Select Images</button>
                            </div>
                        </div>
                        <div class="form-group col-sm-10">
                            <div class="table-responsive">
                                <table id="slider_images_html" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer" aria-describedby="datatable_ajax" role="grid">
                                    <?php echo $slider_img_data_html ?>
                                </table>
                            </div>
                        </div>
                        <div class="form-group col-sm-10">
                            <label for="slider_status" id="slider_status" class="col-sm-2 control-label">Status*</label>
                            <div class="col-sm-10">
                                <?php $selected = $slider_detail[0]['status']; ?><br>
                                <?php echo form_label('Publish', '1') . ' ' . form_radio(array('name' => 'status', 'value' => '1', 'checked' => ('1' == $selected) ? TRUE : FALSE, 'id' => 'publish')); ?>
                                <?php echo form_label('Draft', '0') . ' ' . form_radio(array('name' => 'status', 'value' => '0', 'checked' => ('0' == $selected) ? TRUE : FALSE, 'id' => 'draft')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <?php
                        $save_attr = array('id' => 'btn_save', 'name' => 'btn_save', 'value' => 'Save', 'class' => 'btn btn-primary');
                        echo form_submit($save_attr);
                        ?>    
                        <button type="button" onclick="window.history.back();" class="btn btn-default">Back</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
    </section>
</div>
<script type="text/javascript">
    //validation for edit email formate form
    $(document).ready(function () {
        $('.bootstrap3-editor').wysihtml5();

        $("#edit_slider_frm").validate({
            ignore: [],
            debug: false,
            rules: {
                name: {
                    required: true,
                },
                slug: {
                    required: true,
                    remote: {
                        url: '<?php echo base_url() ?>' + 'admin/slider/checkunique',
                        type: "post",
                        data: {
                            slug: function () {
                                return $("#slug").val();
                            },
                            id: function () {
                                return '<?php echo $slider_detail[0]['id'] ?>';
                            }
                        }
                    },
                },
                description: {
                    required: function ()
                    {
                        CKEDITOR.instances.description.updateElement();
                    },
                },
                slider_image: {
                    required: true,
                },
            },
            messages: {
                name: {
                    required: "Please enter slider name.",
                },
                slug: {
                    required: "Please enter slider slug",
                    remote: "This slug is already in used",
                },
                description: {
                    required: "Please enter slider description.",
                },
                slider_image: {
                    required: "Please enter slider image.",
                }
            },
        });

    });

    function openPopup1(id) {
        var baseUrl = '<?php echo base_url() ?>';
        //$('#or_imge').hide();
        //document.getElementById('form_mainImage').onchange = function () {
        window.KCFinder = {
            callBackMultiple: function (files) {
                window.KCFinder = null;
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    var alt = file.split('/').pop();
                    var fullimage_name = baseUrl + file;
                    var error_msg = 'true';
                    var width;
                    var imgLoader = new Image();
                    imgLoader.src = fullimage_name;
                    var name_separater = "|";
                    var path = file;
                    var cid = parseInt(i) + 100;

                    // return false;
                    if (id != '' && id != undefined) {
                        if (id == 'slider_images') {
                            $('#sliderImage').html('<img src="' + path + '">');
                            $('#edit_slider_frm').find('#imageName').val(path);
                        }
                    }
                    // Log image data:
                }

            },
        };

        window.open('/assets/global/plugins/kcfinder/browse.php?type=images&dir=images/public',
                'kcfinder_image', 'status=0, toolbar=0, location=0, menubar=0, ' +
                'directories=0, resizable=1, scrollbars=0, width=800, height=600'
                );
    }
    
    function openPopup() {

        var img_name_count = $("#commasapareted_images_name").val();
        var number_records = 0;
        if (img_name_count != '') {
            number_records = img_name_count.split("|").length;
        }
        var slider_div_id = $('#slider_images_html');
        var slider_inder_div = $('<tr/>');
        var newHtml = '';
        var errArr = [];
        var baseUrl = '<?php echo base_url() ?>';
        window.KCFinder = {
            callBackMultiple: function (files) {
                window.KCFinder = null;
                var always_show_checkbox_count = number_records - 1;
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    always_show_checkbox_count++;
                    var alt = file.split('/').pop();
                    var fullimage_name = baseUrl + file;
                    var imgLoader = new Image();
                    imgLoader.src = fullimage_name;
                    $(imgLoader).on('load', function () {
                        var width = imgLoader.naturalWidth;
                        if (width < 1910) {
                            var error_msg = 'true';
                        } else {
                            var error_msg = 'false';
                        }
                        var name_separater = "|";
                        var path = file;
                        var cid = parseInt(i) + 100;
                        var commasapareted = '';
                        var commasapareted_images_name = $("#commasapareted_images_name").val();
                        if (commasapareted_images_name != '' && commasapareted_images_name != undefined) {
                            commasapareted = commasapareted_images_name + name_separater + path;
                        } else {
                            commasapareted = path;
                        }
                        var html = '<td class="col-md-1"><input class="imgSize" type="hidden" name="imgSize_' + i + '" id="imgSize_' + i + '" value="' + error_msg + '" ><img src="' + path + '" width="66" height="66" id="' + cid + '"/></td><td  align="center" class="col-md-1"><label class="control-label">Title</label><input class="form-control " type="text" name="slider_title[]" value="" style="width:400px;" ></td><td  align="center" class="col-md-1"><label class="control-label ">Sort</label><input class="form-control " type="text" name="slider_sort[]" style="width:50px;" ></td><td  align="center" class="col-md-1"><label class="control-label">Hyperlink</label><input class="form-control " type="text" name="slider_title_link[]" style="width:200px;" id="slider_title_link' + cid + '"><small>e.g:http://www.xyz.com</small></td><td  align="center" class="col-md-1"><label class="control-label">Target</label><select class="form-control " name="slider_target[]" style="width:130px;" id="slider_target' + cid + '"><option value="_blank">Opens the linked document in a new tab</option><option value="_self">Open the link in the current frame or window</option></select></td><td  class="col-md-1" align="center" valign="middle"><i class="fa fa-trash-o fafont  btn_remove " title="remove" style="cursor: pointer;color:#337ab7;line-height:70px !important;" data-img-name="' + path + '" ></td>';
                        slider_div_id.append(slider_inder_div.clone().html(html));
                        var new_image_name = '';
                        $("#commasapareted_images_name").val(commasapareted);
                        $('.imgWidthMsg').hide();
                    });
                }
            },
        };

        window.open('/assets/global/plugins/kcfinder/browse.php?type=images&dir=images/public',
                'kcfinder_image', 'status=0, toolbar=0, location=0, menubar=0, ' +
                'directories=0, resizable=1, scrollbars=0, width=800, height=600'
                );
    }

    function removeImage(id) {
        var idVal = id.split('_');
        if (id != '' && id != undefined) {
            if (id == 'remove_slider_images') {
                swal({
                    title: 'Are you sure you want to remove this image?', type: 'warning', showCancelButton: true, confirmButtonText: 'Yes', cancelButtonText: 'No'
                }, function () {
                    $('#sliderImage img').attr('src', '');
                    $('#imageName').val('');
                    $('#sliderImage img').attr('alt', '');
                    $('#sliderImage img').attr('title', '');
                });
            }
        }
    }
    $("#name").blur(function () {
        var name = $("#name").val();
        if (name != "")
        {
            var slug = slugify(name);
            if ($("#slug").val().trim() == "") {
                $("#slug").val(slug);
            }
        }
    });

    function slugify(string) {
        return string
                .toString()
                .trim()
                .toLowerCase()
                .replace(/\s+/g, "-")
                .replace(/[^\w\-]+/g, "")
                .replace(/\-\-+/g, "-")
                .replace(/^-+/, "")
                .replace(/-+$/, "");
    }
    $(document).on("click", ".btn_remove", function (e) { //user click on remove btn to remove that slider
        e.preventDefault();
        var img_name = $(this).attr('data-img-name');
        var row = $(this).parent().parent();
        swal({
            title: 'Are you sure you want to delete this record?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, Delete',
            cancelButtonText: 'No, Cancel'
        }, function () {
            var value = img_name;
            var arr = $("#commasapareted_images_name").val();
            arr = arr.split("|");
            arr = arr.filter(function (item) {
                return item !== value
            });
            row.remove();
            var arr = $("#commasapareted_images_name").val(arr.join('|')); // make it string with | sign
            var img_name_count = $("#commasapareted_images_name").val();
            var number_records = 0;
            if (img_name_count != '') {
                number_records = img_name_count.split("|").length;
            }
            var always_show_checkbox_count = number_records - 1;
            return false;
        });
    });
</script>