<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo $module_name; ?>
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('admin/dashboard'); ?>">
                    <i class="fa fa-dashboard"></i>
                    Home
                </a>
            </li>
            <li class="active"><?php echo $module_name; ?></li>
        </ol>
    </section>
    <section class="content-header">
        <?php if ($this->session->flashdata('success')) { ?>
            <div class="callout callout-success">
                <p><?php echo $this->session->flashdata('success'); ?></p>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>  
            <div class="callout callout-danger" >
                <p><?php echo $this->session->flashdata('error'); ?></p>
            </div>
        <?php } ?>

    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-md-12">

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo $section_title; ?></h3>
                    </div><!-- /.box-header -->
                    <div class="container">
                        <div class="col-md-6">
                            <h3>Form Data Detail</h3>
                            <table class="table table-bordered" style="padding:20px;">
                                <?php if (count($formDatadetails) > 0) { ?>
                                    <tr><td><b>Form Name</b></td><td><?php echo $form_detail[0]['form_name']; ?></td></tr>
                                    <?php foreach ($formDatadetails as $fkey => $fval) {
                                        ?>
                                        <tr><td><b><?php echo ucfirst(str_replace("_", " ", $fkey)); ?></b></td><td><?php echo $fval; ?></td></tr>
                                    <?php } ?>
                                    <tr><td><b>Created Date</b></td><td><?php echo $form_detail[0]['created_date']; ?></td></tr>
                                <?php } else {
                                    ?>
                                    <tr><td><b>No Record Found.</b></tr>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </div><!-- /.box -->


            </div><!--/.col (right) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->



