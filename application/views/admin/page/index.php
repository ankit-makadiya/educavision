<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo $module_name; ?>
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('admin/dashboard'); ?>">
                    <i class="fa fa-dashboard"></i>
                    Home
                </a>
            </li>
            <li class="active"><?php echo $module_name; ?></li>
        </ol>
    </section>
    <section class="content">
        <div class="row" >
            <div class="col-xs-12" >
                <?php if ($this->session->flashdata('success')) { ?>
                    <div class="callout callout-success">
                        <p><?php echo $this->session->flashdata('success'); ?></p>
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('error')) { ?>  
                    <div class="callout callout-danger">
                        <p><?php echo $this->session->flashdata('error'); ?></p>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Manage Pages</h3>
                        <div class="pull-right">
                            <?php if(in_array('page-create', $admin_permission)){ ?>
                            <a href="<?php echo base_url('admin/page/add'); ?>" class="btn btn-primary pull-right">Add Page</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="box-body">
                        <table id="tbl-pages" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Slug</th>
                                    <th>Status</th>
                                    <th>Created On</th>
                                    <th>Updated On</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($pages as $row) { ?>
                                    <tr>
                                        <td><?php echo $row['id']; ?></td>
                                        <td><?php echo $row['name']; ?></td>
                                        <td><?php echo $row['slug']; ?></td>
                                        <td><?php echo ($row['status']) == 1 ? 'Published' : 'Draft'; ?></td>
                                        <td><?php echo $row['created_date']; ?></td>
                                        <td><?php echo $row['modify_date']; ?></td>
                                        <td>
                                            <?php if(in_array('page-edit', $admin_permission)){ ?>
                                            <a class="btn btn-primary" href="<?php echo base_url('admin/page/edit/' . $row['id']); ?>" id="edit_btn" title="Edit Page">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </a>
                                            <?php } ?>
                                            <?php if(in_array('page-delete', $admin_permission)){ ?>
                                            <a class="btn btn-primary" href="<?php echo base_url('admin/page/delete/' . $row['id']); ?>" id="delete_btn" title="Delete Page" onclick="if (confirm('Are you sure delete this item?')){return true;}else{event.stopPropagation(); event.preventDefault();};">
                                                <i class="fa fa-trash-o"></i>
                                            </a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                </tbody>
                <tfoot>
                </tfoot>
                </table>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#tbl-pages").DataTable();
        $('#search_frm').submit(function () {
            var value = $('#search_keyword').val();
            if (value == '')
                return false;
        });
        $('.callout-danger').delay(3000).hide('700');
        $('.callout-success').delay(3000).hide('700');
    });
</script>
