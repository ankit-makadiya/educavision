<!--Advance Editor -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/keditor-master/CMS/plugins/bootstrap-3.3.6/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/keditor-master/CMS/plugins/font-awesome-4.5.0/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/keditor-master/dist/css/keditor-1.1.5.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/keditor-master/dist/css/keditor-components-1.1.5.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/keditor-master/CMS/css/examples.css" />
<!-- Advance Editor -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo $module_name; ?>
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('admin/dashboard'); ?>">
                    <i class="fa fa-dashboard"></i>
                    Home
                </a>
            </li>
            <li class="active"><?php echo $module_name; ?></li>
        </ol>
    </section>
    <section class="content-header">
        <?php if ($this->session->flashdata('success')) { ?>
            <div class="callout callout-success">
                <p><?php echo $this->session->flashdata('success'); ?></p>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>  
            <div class="callout callout-danger" >
                <p><?php echo $this->session->flashdata('error'); ?></p>
            </div>
        <?php } ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo $section_title; ?></h3>
                    </div>
                    <?php
                    $attributes = array('id' => 'add_pages_frm');
                    echo form_open('admin/page/add', $attributes);
                    ?>
                    <div class="box-body">
                        <div class="form-group col-sm-10">
                            <label for="page_title" id="page_title">Name*</label>
                            <?php
                            $data = array(
                                'name' => 'name',
                                'id' => 'name',
                                'value' => '',
                                'class' => 'form-control'
                            );
                            echo form_input($data);
                            ?>
                        </div>
                        <div class="form-group col-sm-10">
                            <label for="page_slug" id="page_slug">Slug*</label>
                            <?php
                            $data = array(
                                'name' => 'slug',
                                'id' => 'slug',
                                'value' => '',
                                'class' => 'form-control'
                            );
                            echo form_input($data);
                            ?>
                        </div>
                        <div class="form-group col-sm-10">
                            <label for="page_slider">Slider*</label>
                            <select class="form-control slider_id" name="slider_id" id="slider_id" style="width: 100%;" tabindex="-1" >
                                <option value="0">Select Slider</option>
                                <?php foreach ($sliderData as $slider) { ?>
                                    <option value="<?php echo $slider['id'] ?>"><?php echo $slider['name'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group col-sm-10">
                            <label for="page_template">Page Template</label>
                            <select class="form-control template" name="template" id="template" style="width: 100%;" tabindex="-1" >
                                <option value="">Select Page Template</option>
                                <option value="news">News</option>
                            </select>
                        </div>
                        <div class="form-group col-sm-10">
                            <label for="page_cont" id="page_title">Description*</label>
                            <div style="width:100%; float: left; margin-bottom: 10px;">
                                <div class="btn-group pull-left">
                                    <a href="javascript:void(0);" class="btn btn-default pull-left" id="default-editor">Default Editor</a>
                                    <a href="javascript:void(0);" 
                                       class="btn btn-success pull-right" id="advance-editor">Advance Editor</a>
                                </div>
                                <a href="javascript:void(0);" class="btn btn-primary pull-right <?php echo 'hide'; ?>" data-toggle="modal" data-target="#myModal" id="change-advance-editor">Change Advance Content</a>
                                <input type="hidden" name="layout_editor_type" id="layout_editor_type" value="<?php echo 'default_editor'; ?>" />
                            </div>
                            <div class="defaulteditor show">
                                <?php
                                $data = array(
                                    'name' => 'description',
                                    'id' => 'description',
                                    'value' => "",
                                    //'class' => 'form-control bootstrap3-editor',
                                    'class' => 'form-control ckeditor page_description',
                                );
                                echo form_textarea($data);
                                ?>
                            </div>
                            <div class="advanceeditor hide">
                                <?php
                                $data = array(
                                    'name' => 'advance_description',
                                    'id' => 'advance_description',
                                    'value' => "",
                                    'class' => 'form-control page_description',
                                );

                                echo form_textarea($data);
                                ?>
                            </div>
                        </div>
                        <div class="form-group col-sm-10">
                            <label for="page_status" id="page_status">Status*</label>
                            <?php $selected = 1; ?><br>
                            <?php echo form_label('Publish', '1') . ' ' . form_radio(array('name' => 'status', 'value' => '1', 'checked' => ('1' == $selected) ? TRUE : FALSE, 'id' => 'publish')); ?>
                            <?php echo form_label('Draft', '0') . ' ' . form_radio(array('name' => 'status', 'value' => '0', 'checked' => ('0' == $selected) ? TRUE : FALSE, 'id' => 'draft')); ?>
                        </div>
                    </div>
                    <div class="box-footer">
                        <?php
                        $save_attr = array('id' => 'btn_save', 'name' => 'btn_save', 'value' => 'Save', 'class' => 'btn btn-primary');
                        echo form_submit($save_attr);
                        ?>    
                        <button type="button" onclick="window.history.back();" class="btn btn-default">Back</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;  z-index: 11000;">
    <div class="modal-dialog" style="width:98% !important; height: 98% !important; margin-top: 5px !important;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Advance Layout Builder</h4>
            </div>
            <div class="modal-body" style="height: 670px !important;">
                <iframe id="custom-editor-iframe" data-src="<?php echo base_url() ?>assets/keditor-master/CMS/index.html" width="94%" height="98%" frameborder="0" allowtransparency="true"></iframe>  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="save-document" data-dismiss="modal">Save changes</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
                            //validation for edit email formate form
                            $(document).ready(function () {
                                $('.bootstrap3-editor').wysihtml5();
                                $("#add_pages_frm").validate({
                                    ignore: [],
                                    rules: {
                                        name: {
                                            required: true,
                                        },
                                        slug: {
                                            required: true,
                                            remote: {
                                                url: '<?php echo base_url() ?>' + 'admin/page/checkunique',
                                                type: "post",
                                                data: {
                                                    slug: function () {
                                                        return $("#slug").val();
                                                    }
                                                }
                                            }
                                        },
                                    },
                                    messages:
                                            {
                                                name: {
                                                    required: "Please enter page name",
                                                },
                                                slug: {
                                                    required: "Please enter page slug",
                                                    remote: "This slug is already in used",
                                                },
                                            },
                                });

                                $('.callout-danger').delay(3000).hide('700');
                                $('.callout-success').delay(3000).hide('700');
                            });
                            $("#change-advance-editor").click(function () {
                                var iframe = $("#custom-editor-iframe");
                                iframe.attr("src", iframe.data("src"));
                            });

                            $("#default-editor").click(function () {
                                swal({
                                    title: 'Are you sure want to change editor?',
                                    text: "Your content will be change after choose other editor!",
                                    type: 'warning',
                                    showCancelButton: true,
                                    confirmButtonText: 'Yes, Change',
                                    cancelButtonText: 'No, Cancel'
                                }, function () {
                                    $('#default-editor').removeClass('btn-primary');
                                    $('#default-editor').addClass('btn-default');
                                    $("#advance-editor").removeClass("btn-default");
                                    $("#advance-editor").addClass("btn-primary");
                                    $(".defaulteditor").addClass('show');
                                    $(".defaulteditor").removeClass('hide');
                                    $(".advanceeditor").addClass('hide');
                                    $(".advanceeditor").removeClass('show');
                                    $("#change-advance-editor").removeClass("show");
                                    $("#change-advance-editor").addClass("hide");
                                    $("#layout_editor_type").val('default_editor');
                                });//swal end

                            });
                            $("#advance-editor").click(function () {
                                swal({
                                    title: 'Are you sure want to change editor?',
                                    text: "Your content will be change after choose other editor!",
                                    type: 'warning',
                                    showCancelButton: true,
                                    confirmButtonText: 'Yes, Change',
                                    cancelButtonText: 'No, Cancel'
                                }, function () {
                                    $('#default-editor').removeClass('btn-default');
                                    $('#default-editor').addClass('btn-primary');
                                    $("#advance-editor").removeClass("btn-primary");
                                    $("#advance-editor").addClass("btn-default");
                                    $(".advanceeditor").addClass('show');
                                    $(".advanceeditor").removeClass('hide');
                                    $(".defaulteditor").addClass('hide');
                                    $(".defaulteditor").removeClass('show');
                                    $("#change-advance-editor").removeClass("hide");
                                    $("#change-advance-editor").addClass("show");
                                    $("#layout_editor_type").val('advance_editor');
                                });//swal end

                            });

                            $('#save-document').click(function (e) {
                                var iframe = document.getElementById('custom-editor-iframe');
                                var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
                                var input = innerDoc.getElementById('content').value;
                                //$('#form_pageDescription').html(input);
                                input = input.replace(/\n/g, "");
                                input = input.replace(' keditor-highlighted-dropzone', '');
                                $('#advance_description').html(input);
                                $('#description').html('');
                            });

                            $("#name").blur(function () {
                                var name = $("#name").val();
                                if (name != "")
                                {
                                    var slug = slugify(name);
                                    if ($("#slug").val().trim() == "") {
                                        $("#slug").val(slug);
                                    }
                                }
                            });

                            function slugify(string) {
                                return string
                                        .toString()
                                        .trim()
                                        .toLowerCase()
                                        .replace(/\s+/g, "-")
                                        .replace(/[^\w\-]+/g, "")
                                        .replace(/\-\-+/g, "-")
                                        .replace(/^-+/, "")
                                        .replace(/-+$/, "");
                            }
                            $('#btn_save').on('click', function () {
                                var layout_editor_type = $('#layout_editor_type').val();
                                if (layout_editor_type == 'default_editor') {
                                    var messageLength = CKEDITOR.instances['description'].getData().replace(/<[^>]*>/gi, '').length;
                                    if (!messageLength) {
                                        alert('Please enter a description');
                                        return false;
                                    }
                                } else {
                                    var advance_description = $('#advance_description').val();
                                    if (advance_description == '') {
                                        alert('Please enter a description');
                                        return false;
                                    }
                                }
                                return true;
                            });
</script>

