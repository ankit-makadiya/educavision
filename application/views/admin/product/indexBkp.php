<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo $module_name; ?>
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('admin/dashboard'); ?>">
                    <i class="fa fa-dashboard"></i>
                    Home
                </a>
            </li>
            <li class="active"><?php echo $module_name; ?></li>
        </ol>
    </section>
    <section class="content">
        <div class="row" >
            <div class="col-xs-12" >
                <?php if ($this->session->flashdata('success')) { ?>
                    <div class="callout callout-success">
                        <p><?php echo $this->session->flashdata('success'); ?></p>
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('error')) { ?>  
                    <div class="callout callout-danger">
                        <p><?php echo $this->session->flashdata('error'); ?></p>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Manage Product</h3>
                        <?php if(in_array('product-create', $admin_permission)){ ?>
                        <div class="pull-right">
                            <a href="<?php echo base_url('admin/product/add'); ?>" class="btn btn-primary pull-right">Add Product</a>
                        </div>
                        <?php } ?>

                        <button type="button" id="importpopup" data-toggle="modal" data-target="#myModalimport" class="btn btn-light pull-right">Import Products</button>
                        <button type="button" class="btn btn-light pull-right"><a href="<?php echo base_url('admin/product/exportExcel'); ?>" id="dformat">Export Products</a></button>

                    </div>
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th>Slug</th>
                                    <th>Category</th>
                                    <th>Price</th>
                                    <th>Author</th>
                                    <th>Created Date</th>
                                    <th>Modify Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($product_list as $product) { ?>
                                    <tr>
                                        <td><?php echo $product['id'] ?></td>
                                        <td><img src="<?php echo str_replace('/assets/uploads/images/','/assets/uploads/.thumbs/images/', $product['image']) ?>" alt="<?php echo $product['name'] ?>"></td>
                                        <td><?php echo $product['name'] ?></td>
                                        <td><?php echo $product['slug'] ?></td>
                                        <td><?php echo str_replace(',', '<br>', $product['categoryName']); ?></td>
                                        <td>$<?php echo $product['price'] ?></td>
                                        <td><?php echo $product['author_name'] ?></td>
                                        <td><?php echo $product['created_date'] ?></td>
                                        <td><?php echo $product['modify_date'] ?></td>
                                        <td><?php echo ($product['status'] == 1) ? 'Published' : 'Draft'; ?></td>
                                        <td>
                                            <?php if(in_array('product-edit', $admin_permission)){ ?>
                                            <a class="btn btn-primary" href="<?php echo base_url('admin/product/edit/' . $product['id']) ?>" id="edit_product_btn" title="Edit Product">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </a>
                                            <?php } ?>
                                            <?php if(in_array('product-delete', $admin_permission)){ ?>
                                            <a class="btn btn-primary" href="<?php echo base_url('admin/product/delete/' . $product['id']) ?>" id="delete_product_btn" title="Delete Product" onclick="if (confirm('Are you sure delete this item?')) {
                                                        return true;
                                                    } else {
                                                        event.stopPropagation();
                                                        event.preventDefault();
                                                    }
                                                    ;">
                                                <i class="fa fa-trash-o"></i>
                                            </a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Category</h4>
            </div>
            <div class="modal-body">
                <?php echo form_open('category/edit', array('name' => 'category_frm_user', 'id' => 'category_frm', 'method' => 'POST')); ?>
                <input type="hidden" class="form-control" name="category_id" id="category_id" value="">
                <div class="row">
                    <div class="form-group col-sm-10">
                        <label for="inputEmail3" name="category" id="category">Category Name</label>
                        <input type="text" class="form-control" name="category_name" id="category_name" class="category_name" value="">
                    </div>
                    <div class="col-sm-3   ">
                        <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">send</button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- The Product import Modal Starts-->
<div class="modal" id="myModalimport">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Import Products</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row center">
                    <?php
                    $form_attr = array('id' => 'import_product', 'enctype' => 'multipart/form-data');
                    echo form_open_multipart('admin/product/import_product', $form_attr);
                    ?>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-4 control-label">Select file</label>
                            <div class="col-md-8">
                                <div class="form-material">
                                    <input id="sheet" type="file" name="sheet" class="form-control"><br>
                                    <input id="image_alt" type="submit" value="Import Products" name="import_product"><span>(Allow only xlsx extension files.)</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-8">
                                <div class="form-material">
                                    <a href="<?php echo base_url(); ?>assets/admin/sheet/products.xlsx" class="btn btn-default">Download Sample File</a>
                                </div>
                            </div>
                        </div>
                    </div>
					<p style="padding: 10px;"><b>NOTES</b></b>:
						<ul>
                        <li><b><i>- If you want to update any product then you need export product first and then do your change and again import product with id column otherwise this product consider as a new product</i></b></li>    
						<li><i>- Red column data is required.</i></li>
						<li><i>- Category id can be multiple and you can add by comma seperate. </i></li>
						<!-- <li><i>- Three types of book : paperback, e-book, hardcover. </i></li> -->
						<li><i>- Best Seller : 1 for assign product as a best seller otherwise 0. </i></li>
						<li><i>- Item Of The Month : 1 for assign product as a item of the month otherwise 0. </i></li>
                        <li><i>- You can add suggested book in comma separate sku </i></li>
                        <li><b><i>- You need to add paperback, e-book, hardcover variation wise sku, isbn no, weight, price, download file fields. If you want to select only one variant then you can left the blank fields of those variation. </i></b></li>
                        <li><i>- At least one variation is must be required. </i></li>
                        <li><i>- Status : 1 for Publish and 0 for Draft. </i></li>
						</ul>
					</p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- The Product import Modal End-->

<!-- page script -->
<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
<script type="text/javascript">
    function getCategoryname($id)
    {
        var category_id = $id;
        $.ajax({
            url: "<?php echo base_url('category/getCatName'); ?>",
            type: "POST",
            data: {'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>', 'category_id': category_id},
            success: function (data) {
                console.log(data['category_name']);
                $("#category_name").val(data['category_name']);
                $("#category_id").val(category_id);
                //   $("#category_name").val("Dolly Duck");
            }
        });
    }
</script>
<script type="text/javascript">
    //validation for edit email formate form
    $(document).ready(function () {
        $("#category_frm").validate({
            rules: {
                category_name: {
                    required: true,
                }
            },
            messages:
                    {
                        category_name: {
                            required: "Category name is required",
                        }
                    },
        });

    });

</script>
<script language="javascript" type="text/javascript">
    $(document).ready(function () {
        $('.callout-danger').delay(3000).hide('700');
        $('.callout-success').delay(3000).hide('700');
    });
</script>
