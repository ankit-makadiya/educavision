<?php foreach($attribute_list as $attribute): ?>
<fieldset>
    <legend><?php echo $attribute['name'] ?></legend>
    <div class="form-group col-sm-10">
    <label for="lable_sku" name="lable_sku" id="lable_sku" class="col-sm-2 control-label">SKU*</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="variation[<?php echo $attribute['slug'] ?>][sku]" id="<?php echo $attribute['slug'] ?>_sku" placeholder="Enter Product SKU" required="required" value="<?php if(isset($attribute['sku'])): echo $attribute['sku']; endif; ?>">
    </div>
</div>
<div class="form-group col-sm-10">
    <label for="lable_isbn_no" name="lable_isbn_no" id="lable_isbn_no" class="col-sm-2 control-label">ISBN No*</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="variation[<?php echo $attribute['slug'] ?>][isbn_no]" id="<?php echo $attribute['slug'] ?>_isbn_no" placeholder="Enter Product ISBN No" required="required" value="<?php if(isset($attribute['isbn_no'])): echo $attribute['isbn_no']; endif; ?>">
    </div>
</div>
<div class="form-group col-sm-10">
    <label for="lable_price" name="lable_price" id="lable_price" class="col-sm-2 control-label">Price*</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="variation[<?php echo $attribute['slug'] ?>][price]" id="<?php echo $attribute['slug'] ?>_price" placeholder="Enter Product Price" required="required" value="<?php if(isset($attribute['price'])): echo $attribute['price']; endif; ?>">
    </div>
</div>
<div class="form-group col-sm-10">
    <label for="lable_weight" name="lable_weight" id="lable_weight" class="col-sm-2 control-label">Product Weight</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="variation[<?php echo $attribute['slug'] ?>][weight]" id="<?php echo $attribute['slug'] ?>_weight" placeholder="Enter Product weight" value="<?php if(isset($attribute['weight'])): echo $attribute['weight']; endif; ?>">
    </div>
</div>
<?php if($attribute['slug'] == 'e-book'): ?>
<div class="form-group col-sm-10" id="download_book_file">
    <label class="control-label col-md-2">Download Book File</label>
    <div class="col-md-8">
        <div class="fileinput fileinput-new" data-provides="fileinput">
            <input type="text" id="productDownload" name="productDownload" value=""><br>
            <div>
                <button type="button" id="product_download" name="product_download" class="btn btn-primary btn-file fileinput-new"  onclick="openPopup1(this.id)" tabindex="2">Select Book File</button>
                <button type="button" id="remove_prod_download" name="remove_prod_download" class="btn btn-default"  onclick="removeDownload(this.id)" tabindex="2">Remove</button>
                <a class="popovers" data-container="body" data-trigger="hover" data-placement="top" data-html="true" <span aria-hidden="true" class="icon-info"></span></a>
            </div>
            <input type="hidden" id="<?php echo $attribute['slug'] ?>_download_book" name="variation[<?php echo $attribute['slug'] ?>][download_book]" value="<?php if(isset($attribute['download_book'])): echo $attribute['download_book']; endif; ?>" />
        </div>
    </div>
</div>
<?php endif; ?>
<div class="form-group col-sm-10">
    <label for="product_default_variation" id="product_default_variation" class="col-sm-2 control-label">is Product Default Variation?</label>
    <div class="col-sm-10">
        <input type="checkbox" class="variation_is_default" name="variation[<?php echo $attribute['slug'] ?>][is_default]" id="<?php echo $attribute['slug'] ?>_is_default" value="1" <?php if(isset($attribute['is_default']) && $attribute['is_default'] == 1): echo 'checked="checked"'; endif; ?>>
    </div>
</div>
</fieldset>
<?php endforeach; ?>