<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo $module_name; ?>
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('admin/dashboard'); ?>">
                    <i class="fa fa-dashboard"></i>
                    Home
                </a>
            </li>
            <li class="active"><?php echo $module_name; ?></li>
        </ol>
    </section>
    <section class="content-header">
        <?php if ($this->session->flashdata('success')) { ?>
            <div class="callout callout-success">
                <p><?php echo $this->session->flashdata('success'); ?></p>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>  
            <div class="callout callout-danger" >
                <p><?php echo $this->session->flashdata('error'); ?></p>
            </div>
        <?php } ?>

    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo $section_title; ?></h3>
                    </div>
                    <?php
                    $form_attr = array('id' => 'add_product_frm', 'enctype' => 'multipart/form-data');
                    echo form_open_multipart('admin/product/add', $form_attr);
                    ?>
                    <div class="box-body">
                        <div class="form-group col-sm-10">
                            <label for="lable_language" name="lable_language" id="lable_language" class="col-sm-2 control-label">Product Language*</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="language" id="language" placeholder="Enter Product language" data-role="tagsinput" required="" width="100%" size="100">
                            </div>
                        </div>
                        <div class="form-group col-sm-10">
                            <label for="lable_name" name="lable_name" id="lable_name" class="col-sm-2 control-label">Product Name*</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name" id="name" placeholder="Enter Product Name">
                            </div>
                        </div>
                        <div class="form-group col-sm-10">
                            <label for="lable_slug" name="lable_slug" id="lable_slug" class="col-sm-2 control-label">Product Slug*</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="slug" id="slug" placeholder="Enter Product Slug">
                            </div>
                        </div>
                        <!-- <div class="form-group col-sm-10">
                            <label for="lable_sku" name="lable_sku" id="lable_sku" class="col-sm-2 control-label">SKU*</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="sku" id="sku" placeholder="Enter Product SKU">
                            </div>
                        </div> -->
                        <!-- <div class="form-group col-sm-10">
                            <label for="lable_isbn_no" name="lable_isbn_no" id="lable_isbn_no" class="col-sm-2 control-label">ISBN No*</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="isbn_no" id="isbn_no" placeholder="Enter Product ISBN No">
                            </div>
                        </div> -->
                        <div class="form-group col-sm-10">
                            <label for="lable_collection" name="lable_collection" id="lable_collection" class="col-sm-2 control-label">Collection</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="collection" id="collection" placeholder="Enter Product Collection">
                            </div>
                        </div>
                        <div class="form-group col-sm-10">
                            <label for="lable_category"  class="col-sm-2 control-label">Assign Category*</label>
                            <div class="col-sm-10">
                                <select class="form-control category select2" multiple="multiple" name="category_id[]" id="category_id" style="width: 100%;" tabindex="-1" required="" >
                                    <option value="0">Select Category</option>
                                    <?php foreach ($category_list as $category) { ?>
                                        <option value="<?php echo $category['id'] ?>"><?php echo $category['name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <!-- <div class="form-group col-sm-10">
                            <label for="lable_price" name="lable_price" id="lable_price" class="col-sm-2 control-label">Price*</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="price" id="price" placeholder="Enter Product Price">
                            </div>
                        </div> -->
                        <div class="form-group col-sm-10">
                            <label for="lable_author" name="lable_author" id="lable_author" class="col-sm-2 control-label">Author Name*</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="author_name" id="author_name" placeholder="Enter Author Name">
                            </div>
                        </div>
                        <div class="form-group col-sm-10">
                            <label for="lable_tags" name="lable_tags" id="lable_tags" class="col-sm-2 control-label">Product Tags*</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="tags" id="tags" placeholder="Enter Product tags" data-role="tagsinput" required="">
                            </div>
                        </div>
                        <!-- <div class="form-group col-sm-10">
                            <label for="lable_weight" name="lable_weight" id="lable_weight" class="col-sm-2 control-label">Product Weight*</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="weight" id="weight" placeholder="Enter Product weight" required="">
                            </div>
                        </div> -->
                        <!-- <div class="form-group col-sm-10">
                            <label for="lable_book_type"  class="col-sm-2 control-label">Book Type*</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="book_type" id="book_type" style="width: 100%;" tabindex="-1" required="" >
                                    <option value="paperback">Paperback</option>
                                    <option value="hardcover">Hardcover</option>
                                    <option value="e-book">E-book</option>
                                </select>
                            </div>
                        </div> -->
                        <div class="form-group col-sm-10">
                            <label for="inputEmail3" class="col-sm-2 control-label">Description*</label>
                            <div class="col-sm-10">
                                <textarea class="form-control ckeditor" id="description" name="description" rows="10" cols="80"></textarea>
                            </div>
                        </div>
                        <div class="form-group col-sm-10">
                            <label class="control-label col-md-2">Image*</label>
                            <div class="col-md-8">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" id="productImage" style="width: 200px; height: 195px;"> </div>
                                    <div>
                                        <button type="button" id="product_images" name="product_images" class="btn btn-primary btn-file fileinput-new"  onclick="openPopup(this.id)" tabindex="2">Select Image</button>

                                        <button type="button" id="remove_prod_images" name="remove_prod_images" class="btn btn-default"  onclick="removeImage(this.id)" tabindex="2">Remove</button>
                                        <a class="popovers" data-container="body" data-trigger="hover" data-placement="top" data-html="true" <span aria-hidden="true" class="icon-info"></span></a>
                                    </div>
                                    <input type="hidden" id="imageName" name="product_image" />
                                </div>
                            </div>
                        </div>
                        <!-- <div class="form-group col-sm-10" id="download_book_file">
                            <label class="control-label col-md-2">Download Book File</label>
                            <div class="col-md-8">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <input type="text" id="productDownload" name="productDownload" value=""><br>
                                    <div>
                                        <button type="button" id="product_download" name="product_download" class="btn btn-primary btn-file fileinput-new"  onclick="openPopup1(this.id)" tabindex="2">Select Book File</button>
                                        <button type="button" id="remove_prod_download" name="remove_prod_download" class="btn btn-default"  onclick="removeDownload(this.id)" tabindex="2">Remove</button>
                                        <a class="popovers" data-container="body" data-trigger="hover" data-placement="top" data-html="true" <span aria-hidden="true" class="icon-info"></span></a>
                                    </div>
                                    <input type="hidden" id="download_book" name="download_book" value="" />
                                </div>
                            </div>
                        </div> -->
                        <div class="form-group col-sm-10">
                            <label for="product_best_seller" id="product_best_seller" class="col-sm-2 control-label">Best Seller</label>
                            <div class="col-sm-10">
                                <input type="checkbox" name="best_seller" id="best_seller" value="1">
                            </div>
                        </div>
                        <div class="form-group col-sm-10">
                            <label for="product_item_of_the_month" id="product_item_of_the_month" class="col-sm-2 control-label">Item Of The Month</label>
                            <div class="col-sm-10">
                                <input type="checkbox" name="item_of_the_month" id="item_of_the_month" value="1">
                            </div>
                        </div>
						<!-- <div class="form-group col-sm-10">
							<label for="product_is_suggest" id="product_is_suggest" class="col-sm-2 control-label">Is Suggested Book?</label>
							<div class="col-sm-10">
								<input type="checkbox" name="suggest_book" id="is_suggest" value="1">
							</div>
						</div> -->
                        <div class="form-group col-sm-10">
                            <label for="product_suggest_book" id="product_suggest_book" class="col-sm-2 control-label">Suggested Book</label>
                            <div class="col-sm-10">
                                <select class="form-control suggest_book select2" multiple="multiple" name="suggest_book[]" id="suggest_book" style="width: 100%;" tabindex="-1">
                                    <?php foreach ($suggested_product_list as $product) { ?>
                                        <option value="<?php echo $product['sku'] ?>"><?php echo $product['name'] .' ('. $product['attribute_name'].')'; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-sm-10">
                            <label for="product_priority" id="product_priority" class="col-sm-2 control-label">Priority</label>
                            <div class="col-sm-2">
                                <input type="number" class="form-control" name="priority" id="priority" value="1" min="1" max="99">
                            </div>
                        </div>
                        <div class="form-group col-sm-10" style="min-height: 200px;">
                            <fieldset>
                                <legend>Select Attribute</legend>
                                <div class="row">
                                    <div class="col-md-12">
                                    <?php foreach($attribute_list as $attribute): ?>
                                    <div class="col-md-2">
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="attribute[]" class="product_attribute" value="<?php echo $attribute['id'] ?>" <?php if($attribute['slug'] == 'paperback'){ echo 'checked="checked"'; } ?>><?php echo $attribute['name'] ?></label>
                                        </div>
                                    </div>
                                     <?php endforeach; ?>
                                    </div>
                                    <!-- <div class="col-md-3">
                                        <a href="javascript:void(0);" id="attribute_apply" class="btn btn-primary">Apply</a>
                                    </div> -->
                                </div>
                                <div class="row">
                                    <div class="col-md-12" id="attribute_html">
                                    </div>
                                </div>
                            </fieldset>    
                        </div>
                        <div class="form-group col-sm-10">
                            <label for="product_status" id="product_status" class="col-sm-2 control-label">Status*</label>
                            <div class="col-sm-10">
                                <?php $selected = 1; ?><br>
                                <?php echo form_label('Publish', '1') . ' ' . form_radio(array('name' => 'status', 'value' => '1', 'checked' => ('1' == $selected) ? TRUE : FALSE, 'id' => 'publish')); ?>
                                <?php echo form_label('Draft', '0') . ' ' . form_radio(array('name' => 'status', 'value' => '0', 'checked' => ('0' == $selected) ? TRUE : FALSE, 'id' => 'draft')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <?php
                        $save_attr = array('id' => 'btn_save', 'name' => 'btn_save', 'value' => 'Save', 'class' => 'btn btn-primary');
                        echo form_submit($save_attr);
                        ?>    
                        <button type="button" onclick="window.history.back();" class="btn btn-default">Back</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    //validation for edit email formate form
    $(document).ready(function () {
        $('.bootstrap3-editor').wysihtml5();

        $("#add_product_frm").validate({
            ignore: [],
            debug: false,
            rules: {
                language: {
                    required: true,
                },
                name: {
                    required: true,
                },
                // isbn_no: {
                //     required: true,
                //     remote: {
                //         url: '<?php echo base_url() ?>' + 'admin/product/checkuniqueisbn',
                //         type: "post",
                //         data: {
                //             isbn_no: function () {
                //                 return $("#isbn_no").val();
                //             }
                //         }
                //     }
                // },
                slug: {
                    required: true,
                    remote: {
                        url: '<?php echo base_url() ?>' + 'admin/product/checkunique',
                        type: "post",
                        data: {
                            slug: function () {
                                return $("#slug").val();
                            }
                        }
                    }
                },
                category_id: {
                    required: true,
                },
                // price: {
                //     required: true,
                // },
                author_name: {
                    required: true,
                },
                tags: {
                    required: true,
                },
                // weight: {
                //     required: true,
                // },
                description: {
                    required: function ()
                    {
                        CKEDITOR.instances.description.updateElement();
                    },
                },
                product_image: {
                    required: true,
                },
                // productDownload: {
                //     required: true,
                // },
                "attribute[]": {
                    required:function (element) {
                        var boxes = $('.product_attribute');
                        if (boxes.filter(':checked').length == 0) {
                            return true;
                        }
                        return false;
                    },  
                    minlength:1
                }
            },
            messages: {
                language: {
                    required: "Please enter product language.",
                },
                name: {
                    required: "Please enter product name.",
                },
                // isbn_no: {
                //     required: "Please enter ISBN No",
                //     remote: "This isbn no. is already in used",
                // },
                slug: {
                    required: "Please enter product slug",
                    remote: "This slug is already in used",
                },
                category_id: {
                    required: "Please enter category.",
                },
                // price: {
                //     required: "Please enter product price.",
                // },
                author_name: {
                    required: "Please enter author name.",
                },
                tags: {
                    required: "Please enter tags.",
                },
                // weight: {
                //     required: "Please enter weight.",
                // },
                description: {
                    required: "Please enter product description.",
                },
                product_image: {
                    required: "Please enter product image.",
                },
                // productDownload: {
                //     required: "Please upload zip file.",
                // },
                "attribute[]":{
                    required: "Please select at least one variation",
                    minlength: "Please select at least one variation",
                }
            },
        });
        $(".category").select2({
            placeholder: "Select Category",
            allowClear: true,
            width: '100%'
        });
        $(".suggest_book").select2({
            placeholder: "Select Book",
            allowClear: true,
            width: '100%'
        });

        $('.product_attribute').on('click',function(){
            getAttributeHtml();            
        });
        getAttributeHtml();

        $('.variation_is_default').on('change', function() {
            $('.variation_is_default').not(this).prop('checked', false);
            $("form").validate().element(".variation_is_default");  
        });
        // $(".variation_is_default").rules("add", { 
        //     required:function (element) {
        //         var boxes = $('.variation_is_default');
        //         if (boxes.filter(':checked').length != 1) {
        //             return true;
        //         }
        //         return false;
        //     },  
        //     maxlength:1
        // });
    });

    function getAttributeHtml(){
        var baseUrl = '<?php echo base_url() ?>';
        var valuesArray = $('input[name="attribute[]"]:checked').map(function () {  
            return this.value;
        }).get().join(",");
        if(valuesArray != ''){
            $.ajax({
                url: baseUrl + 'admin/get-attribute-html',
                type: 'post',
                data: 'attribute=' + valuesArray,
                dataType: "html",
                success: function (result) {
                    $('#attribute_html').html(result);
                    $(".variation_is_default").rules("add", { 
                        required:function (element) {
                            var boxes = $('.variation_is_default');
                            if (boxes.filter(':checked').length != 1) {
                                return true;
                            }
                            return false;
                        },  
                        maxlength:1
                    });
                    $('.variation_is_default').on('change', function() {
                        $('.variation_is_default').not(this).prop('checked', false);  
                        $('#add_product_frm').validate();
                    });
                },
            });
        }
    }

    function openPopup(id) {
        var baseUrl = '<?php echo base_url() ?>';
        //$('#or_imge').hide();
        //document.getElementById('form_mainImage').onchange = function () {
        window.KCFinder = {
            callBackMultiple: function (files) {
                window.KCFinder = null;
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    var alt = file.split('/').pop();
                    var fullimage_name = baseUrl + file;
                    var error_msg = 'true';
                    var width;
                    var imgLoader = new Image();
                    imgLoader.src = fullimage_name;
                    var name_separater = "|";
                    var path = file;
                    var cid = parseInt(i) + 100;

                    // return false;
                    if (id != '' && id != undefined) {
                        if (id == 'product_images') {
                            $('#productImage').html('<img src="' + path + '">');
                            $('#add_product_frm').find('#imageName').val(path);
                        }
                    }
                    // Log image data:
                }

            },
        };

        window.open('<?php echo base_url('/assets/global/plugins/kcfinder/browse.php?type=images&dir=images/public'); ?>',
                'kcfinder_image', 'status=0, toolbar=0, location=0, menubar=0, ' +
                'directories=0, resizable=1, scrollbars=0, width=800, height=600'
                );
    }

    function openPopup1(id) {
        var baseUrl = '<?php echo base_url() ?>';
        window.KCFinder = {
            callBackMultiple: function (files) {
                window.KCFinder = null;
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    var alt = file.split('/').pop();
                    var fullimage_name = baseUrl + file;
                    var error_msg = 'true';
                    var width;
                    var imgLoader = new Image();
                    imgLoader.src = fullimage_name;
                    var name_separater = "|";
                    var path = file;
                    var cid = parseInt(i) + 100;

                    // return false;
                    if (id != '' && id != undefined) {
                        if (id == 'product_download') {
                            $('#productDownload').val(alt);
                            $('#edit_product_frm').find('#download_book').val(path);
                        }
                    }
                }

            },
        };

        window.open('<?php echo base_url('/assets/global/plugins/kcfinder/browse.php?type=files&dir=files/public'); ?>',
                'kcfinder_image', 'status=0, toolbar=0, location=0, menubar=0, ' +
                'directories=0, resizable=1, scrollbars=0, width=800, height=600'
                );
    }

    function removeImage(id) {
        var idVal = id.split('_');
        if (id != '' && id != undefined) {
            if (id == 'remove_prod_images') {
                swal({
                    title: 'Are you sure you want to remove this image?', type: 'warning', showCancelButton: true, confirmButtonText: 'Yes', cancelButtonText: 'No'
                }, function () {
                    $('#productImage img').attr('src', '');
                    $('#imageName').val('');
                    $('#productImage img').attr('alt', '');
                    $('#productImage img').attr('title', '');
                });
            }
        }
    }
    function removeDownload(id){
        var idVal = id.split('_');
        if (id != '' && id != undefined) {
            if (id == 'remove_prod_download') {
                swal({
                    title: 'Are you sure you want to remove this file?', type: 'warning', showCancelButton: true, confirmButtonText: 'Yes', cancelButtonText: 'No'
                }, function () {
                    $('#productDownload').val('');
                    $('#edit_product_frm').find('#download_book').val('');
                });
            }
        }
    }
    $("#name").blur(function () {
        var name = $("#name").val();
        if (name != "")
        {
            var slug = slugify(name);
            if ($("#slug").val().trim() == "") {
                $("#slug").val(slug);
            }
        }
    });

    function slugify(string) {
        return string
                .toString()
                .trim()
                .toLowerCase()
                .replace(/\s+/g, "-")
                .replace(/[^\w\-]+/g, "")
                .replace(/\-\-+/g, "-")
                .replace(/^-+/, "")
                .replace(/-+$/, "");
    }
    download_book_file();
    $('#book_type').on('change',function(){
        download_book_file();
    });
    function download_book_file(){
        var book_type = $('#book_type').val();
        if(book_type == 'e-book'){
            $('#download_book_file').show();
        }else{
            $('#download_book_file').hide();
        }    
    }
</script>
