<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo $module_name; ?>
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('admin/dashboard'); ?>">
                    <i class="fa fa-dashboard"></i>
                    Home
                </a>
            </li>
            <li class="active"><?php echo $module_name; ?></li>
        </ol>
    </section>
    <section class="content-header">
        <?php if ($this->session->flashdata('success')) { ?>
            <div class="callout callout-success">
                <p><?php echo $this->session->flashdata('success'); ?></p>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>  
            <div class="callout callout-danger" >
                <p><?php echo $this->session->flashdata('error'); ?></p>
            </div>
        <?php } ?>

    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
			<?php
			$CI = & get_instance();
			?>
            <div class="col-md-12">

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo $section_title; ?></h3>
                    </div><!-- /.box-header -->
                    <div class="container">
                        <div class="col-md-6">
                             <div class="row">
                            <div class="col-md-12">

                                <div class="box box-info">
                                    <h3><b>Order Number</b> : <?php echo $order_data[0]['order_number']; ?></h3>
                                            <table class="table table-bordered" style="padding:20px;">
                                                <tr><td><h3>Shipping Details<h3></td></tr>
                                                <tr><td>
                                                    <?php echo $order_data[0]['shipping_first_name'] .' '.$order_data[0]['shipping_last_name'] ; ?><br><br>
                                                    <b><?php echo $order_data[0]['shipping_company_name']; ?></b><br>
                                                    <?php echo $order_data[0]['shipping_address'] .'<br>'.$order_data[0]['shipping_address_1'] ; ?><br>

                                                    <?php echo $CI->getCityNameById($order_data[0]['shipping_city']); ?>,
                                                    <?php echo $CI->getStateNameById($order_data[0]['shipping_state']); ?>,
                                                    <?php echo $CI->getCountryNameById($order_data[0]['shipping_country']); ?> - <?php echo $order_data[0]['shipping_zipcode']; ?><br>
                                                    <b>Email : </b> <?php echo $order_data[0]['shipping_email']; ?><br>
                                                    <b>Phone : </b> <?php echo $order_data[0]['shipping_phone']; ?>
                                                </td></tr>
                                                </table>
                                                <hr>
                                                <table class="table table-bordered" style="padding:20px;">
                                                    <tr><td><h3>Billing Details<h3></td></tr>
                                                <tr><td>
                                                    <?php echo $order_data[0]['billing_first_name'] .' '.$order_data[0]['billing_last_name'] ; ?><br><br>
                                                    <b><?php echo $order_data[0]['billing_company_name']; ?></b><br>
                                                    <?php echo $order_data[0]['billing_address'] .'<br>'.$order_data[0]['billing_address_1'] ; ?><br>

                                                    <?php echo $CI->getCityNameById($order_data[0]['billing_city']); ?>,
                                                    <?php echo $CI->getStateNameById($order_data[0]['billing_state']); ?>,
                                                    <?php echo $CI->getCountryNameById($order_data[0]['billing_country']); ?> - <?php echo $order_data[0]['billing_zipcode']; ?><br>
                                                    <b>Email : </b> <?php echo $order_data[0]['billing_email']; ?><br>
                                                    <b>Phone : </b> <?php echo $order_data[0]['billing_phone']; ?>
                                                </td></tr>
                                                </table>
                                                <hr>
                                                <table class="table table-bordered" style="padding:20px;">
                                                    <tr><td><h3>Order Details<h3></td></tr>
                                                    <tr>
                                                        <td><b>Id</b></td>
                                                        <td><b>Product Name</b></td>
                                                        <td><b>Product Sku</b></td>
                                                        <td><b>Product Qty</b></td>
                                                        <td><b>Product Price</b></td>
                                                        <td><b>Total Amount</b></td>
                                                    </tr>
                                                <?php
                                                $i = 0;
                                                $totalAmount = 0;
                                                foreach ($order_details as $orderDetais) {
                                                    $i++;
                                                    $totalAmount += $orderDetais['product_qty'] * $orderDetais['product_price'];

                                                    $product_data = $CI->getProductDataBySku($orderDetais['product_sku']);
                                                    $book_type = '';
                                                    $download_book = '';
                                                    if(!empty($product_data)){
                                                        $book_type = $product_data['book_type'];
                                                        $download_book = $product_data['download_book'];    
                                                    }
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $i; ?></td>
                                                        <td><?php echo $orderDetais['product_name']; ?><?php if($book_type == 'e-book'): ?><a href="<?php echo $download_book; ?>" style="float:right; color:#000;" title="Download Book"><i class="fa fa-download"></i></a><?php endif; ?></td>
                                                        <td><?php echo $orderDetais['product_sku']; ?></td>
                                                        <td><?php echo $orderDetais['product_qty']; ?></td>
                                                        <td><?php echo '$'.$orderDetais['product_price']; ?></td>
                                                        <td><?php echo '$'. ($orderDetais['product_qty'] * $orderDetais['product_price']); ?></td>
                                                    </tr>
                                                <?php } ?>
                                                    <tr>
                                                        <td colspan="5">Sub Total</td>
                                                        <td><?php echo '$'.$order_data[0]['sub_total']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5">Total Tax</td>
                                                        <td><?php echo '$'.$order_data[0]['total_tax']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5">Shipping Charge</td>
                                                        <td><?php echo '$'.$order_data[0]['shipping_charge']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5">Discount</td>
                                                        <td><?php echo '$'.$order_data[0]['total_discount']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5">Total Amount</td>
                                                        <td><?php echo '$'.$order_data[0]['total_amount']; ?></td>
                                                    </tr>
                                                </table>
                                                <hr>
                                                <table class="table table-bordered" style="padding:20px;">
                                                    <tr><td><h3>Payment Details<h3></td></tr>
                                                    <tr><td><b>Invoice Number</b></td><td><?php echo $order_payment['invoice_number']; ?></td></tr>
                                                    <tr><td><b>Transaction Id</b></td><td><?php echo $order_payment['transaction_id']; ?></td></tr>
                                                    <tr><td><b>Pay Amount</b></td><td><?php echo '$'.$order_payment['pay_amount']; ?></td></tr>
                                                    <tr><td><b>Payment Status</b></td>
                                                        <td>
                                                        <?php echo ($order_payment['payment_status'] == '1') ? 'Success' : (($order_payment['payment_status'] == '2') ? 'Fail' : 'Pending');  ?>
                                                        </td>
                                                    </tr>
                                                    <tr><td><b>Order Date</b></td><td><?php echo $order_payment['created_date']; ?></td></tr>
                                                </table>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box -->


            </div><!--/.col (right) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
    //validation for edit email formate form
    $(document).ready(function () {
		$('.bootstrap3-editor').wysihtml5();
	
        $("#edit_payments_frm").validate({
            rules: {
				payment_title: {
					required: true,
				},
                payment_cont: {
                    required: true,
					minlength: 100
                }
            },
            messages:
            {
                payment_title: {
					required: "Please enter payment title",
				},
				payment_cont: {
                    required: "Please enter payment content",
                }
            },
        });
		
		$('.callout-danger').delay(3000).hide('700');
        $('.callout-success').delay(3000).hide('700');
    });
</script>

