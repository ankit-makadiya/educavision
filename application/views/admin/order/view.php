<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo $module_name; ?>
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('admin/dashboard'); ?>">
                    <i class="fa fa-dashboard"></i>
                    Home
                </a>
            </li>
            <li class="active"><?php echo $module_name; ?></li>
        </ol>
    </section>
    <section class="content-header">
        <?php if ($this->session->flashdata('success')) { ?>
            <div class="callout callout-success">
                <p><?php echo $this->session->flashdata('success'); ?></p>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>  
            <div class="callout callout-danger" >
                <p><?php echo $this->session->flashdata('error'); ?></p>
            </div>
        <?php } ?>

    </section>
    <!-- Main content -->
    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-globe"></i> Order Number : #<?php echo $order_data[0]['order_number']; ?>
                    <small class="pull-right">Date: <?php echo date('d-M-Y', strtotime($order_data[0]['created_date'])) ?></small>
                </h2>
            </div>
            <!-- /.col -->
        </div>
        <?php
        $CI = & get_instance();
        ?>
        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                Shipping Details
                <address>
                    <?php echo $order_data[0]['shipping_first_name'] . ' ' . $order_data[0]['shipping_last_name']; ?><br><br>
                    <b><?php echo $order_data[0]['shipping_company_name']; ?></b><br>
                    <?php echo $order_data[0]['shipping_address'] . '<br>' . $order_data[0]['shipping_address_1']; ?><br>

                    <?php echo $CI->getCityNameById($order_data[0]['shipping_city']); ?>,
                    <?php echo $CI->getStateNameById($order_data[0]['shipping_state']); ?>,
                    <?php echo $CI->getCountryNameById($order_data[0]['shipping_country']); ?> - <?php echo $order_data[0]['shipping_zipcode']; ?><br>
                    <b>Email : </b> <?php echo $order_data[0]['shipping_email']; ?><br>
                    <b>Phone : </b> <?php echo $order_data[0]['shipping_phone']; ?>
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                Billing Details
                <address>
                    <?php echo $order_data[0]['billing_first_name'] . ' ' . $order_data[0]['billing_last_name']; ?><br><br>
                    <b><?php echo $order_data[0]['billing_company_name']; ?></b><br>
                    <?php echo $order_data[0]['billing_address'] . '<br>' . $order_data[0]['billing_address_1']; ?><br>

                    <?php echo $CI->getCityNameById($order_data[0]['billing_city']); ?>,
                    <?php echo $CI->getStateNameById($order_data[0]['billing_state']); ?>,
                    <?php echo $CI->getCountryNameById($order_data[0]['billing_country']); ?> - <?php echo $order_data[0]['billing_zipcode']; ?><br>
                    <b>Email : </b> <?php echo $order_data[0]['billing_email']; ?><br>
                    <b>Phone : </b> <?php echo $order_data[0]['billing_phone']; ?>
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <b>Invoice #<?php echo $order_payment['invoice_number']; ?></b><br>
                <b>Transaction Id:</b> <?php echo $order_payment['transaction_id']; ?><br>
                <b>Payment Status:</b> <?php echo ($order_payment['payment_status'] == '1') ? 'Success' : (($order_payment['payment_status'] == '2') ? 'Fail' : 'Pending'); ?><br>
                <b>Due Date:</b> <?php echo $order_payment['created_date']; ?><br>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Product Name</th>
                            <th>Product Sku</th>
                            <th>Product Qty</th>
                            <th>Product Price</th>
                            <th>Total Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 0;
                        $totalAmount = 0;
                        foreach ($order_details as $orderDetais) {
                            $i++;
                            $totalAmount += $orderDetais['product_qty'] * $orderDetais['product_price'];

                            // $product_data = $CI->getProductDataBySku($orderDetais['product_sku']);
                            // $book_type = '';
                            // $download_book = '';
                            // if(!empty($product_data)){
                            //     $book_type = $product_data['book_type'];
                            //     $download_book = $product_data['download_book'];    
                            // }
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $orderDetais['product_name']; ?><?php if ($orderDetais['attribute_slug'] == 'e-book'): ?><a href="<?php echo base_url() . $orderDetais['download_book']; ?>" style="float:right; color:#000;" title="Download Book" target="_blank"><i class="fa fa-download"></i></a><?php endif; ?></td>
                                <td><?php echo $orderDetais['product_sku']; ?></td>
                                <td><?php echo $orderDetais['product_qty']; ?></td>
                                <td><?php echo '$' . $orderDetais['product_price']; ?></td>
                                <td><?php echo '$' . ($orderDetais['product_qty'] * $orderDetais['product_price']); ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <!-- accepted payments column -->
            <div class="col-xs-6">
                <?php
                if($order_data[0]['order_status_id'] == '3'){
                $form_attr = array('id' => 'add_tracking form', 'enctype' => 'multipart/form-data');
                echo form_open_multipart('admin/order/updatetracking', $form_attr);
                ?>
                <div class="box-body">
                    <div class="form-group">
                        <label for="lable_name" name="lable_name" id="lable_name" class="col-sm-12 control-label">Tracking Detail*</label>
                        <div class="col-sm-12">
                            <input type="url" class="form-control required" name="tracking" id="tracking" placeholder="Enter Tracking Url" required="required" value="<?php echo $order_data[0]['tracking_detail'] ?>">
                        </div>
                    </div>
                    <input type="hidden" name="order_id" value="<?php echo $order_data[0]['id']; ?>">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <?php
                            $save_attr = array('id' => 'btn_save', 'name' => 'btn_save', 'value' => 'Save', 'class' => 'btn btn-primary');
                            echo form_submit($save_attr);
                            ?>    
                        </div>
                    </div>
                </div>
                </form>
                <?php } ?>
            </div>
            <!-- /.col -->
            <div class="col-xs-6">
                <div class="table-responsive">
                    <table class="table">
                        <tbody><tr>
                                <th style="width:50%">Subtotal:</th>
                                <td><?php echo '$' . $order_data[0]['sub_total']; ?></td>
                            </tr>
                            <tr>
                                <th>Total Tax:</th>
                                <td><?php echo '$' . $order_data[0]['total_tax']; ?></td>
                            </tr>
                            <tr>
                                <th>Shipping Charge:</th>
                                <td><?php echo '$' . $order_data[0]['shipping_charge']; ?></td>
                            </tr>
                            <tr>
                                <th>Discount:</th>
                                <td><?php echo '$' . $order_data[0]['total_discount']; ?></td>
                            </tr>
                            <tr>
                                <th>Total Amount:</th>
                                <td><?php echo '$' . $order_data[0]['total_amount']; ?></td>
                            </tr>
                        </tbody></table>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- this row will not appear when printing -->
        <!-- <div class="row no-print">
          <div class="col-xs-12">
            <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
            <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
            </button>
            <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
              <i class="fa fa-download"></i> Generate PDF
            </button>
          </div>
        </div> -->
    </section>
</div><!-- /.content-wrapper -->

<script type="text/javascript">
    //validation for edit email formate form
    $(document).ready(function () {
        $('.bootstrap3-editor').wysihtml5();

        $("#edit_payments_frm").validate({
            rules: {
                payment_title: {
                    required: true,
                },
                payment_cont: {
                    required: true,
                    minlength: 100
                }
            },
            messages:
                    {
                        payment_title: {
                            required: "Please enter payment title",
                        },
                        payment_cont: {
                            required: "Please enter payment content",
                        }
                    },
        });

        $('.callout-danger').delay(3000).hide('700');
        $('.callout-success').delay(3000).hide('700');
    });
</script>

