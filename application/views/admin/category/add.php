<?php if(!isset($admin_permission) || !in_array('category-create',$admin_permission)){ ?><?php echo PERMISSION_DENIED_MESSAGE; exit; ?><?php } ?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo $module_name; ?>
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('admin/dashboard'); ?>">
                    <i class="fa fa-dashboard"></i>
                    Home
                </a>
            </li>
            <li class="active"><?php echo $module_name; ?></li>
        </ol>
    </section>

    <section class="content-header">
        <?php if ($this->session->flashdata('success')) { ?>
            <div class="callout callout-success">
                <p><?php echo $this->session->flashdata('success'); ?></p>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>  
            <div class="callout callout-danger" >
                <p><?php echo $this->session->flashdata('error'); ?></p>
            </div>
        <?php } ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo $section_title; ?></h3>
                    </div>
                    <?php
                    $form_attr = array('id' => 'add_category_frm', 'enctype' => 'multipart/form-data');
                    echo form_open_multipart('admin/category/add', $form_attr);
                    ?>
                    <div class="box-body">
                        <div class="form-group col-sm-10">
                            <label for="inputEmail3"  class="col-sm-2 control-label">Parent Category*</label>
                            <div class="col-sm-10">
                                <select class="form-control parent_id select2" name="parent_id" id="parent_id" style="width: 100%;" tabindex="-1" >
                                    <option value="0">Select Parent </option>
                                    <?php foreach ($category_list as $category) { ?>
                                        <option value="<?php echo $category['id'] ?>"><?php echo $category['name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-sm-10">
                            <label for="inputEmail3" name="label_name" id="label_name" class="col-sm-2 control-label">Category Name*</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name" id="name" placeholder="Category Name">
                            </div>
                        </div>
                        <div class="form-group col-sm-10">
                            <label for="inputEmail3" name="lable_slug" id="lable_slug" class="col-sm-2 control-label">Category Slug*</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="slug" id="slug" placeholder="Category Slug">
                            </div>
                        </div>
                        <!--                        <div class="form-group col-sm-10">
                                                    <label for="inputEmail3" class="col-sm-2 control-label">Description</label>
                                                    <div class="col-sm-10">
                                                        <textarea class="form-control ckeditor" id="description" name="description" rows="10" cols="80"></textarea>
                                                    </div>
                                                </div>-->
                        <div class="form-group col-sm-10">
                            <label for="page_status" id="page_status" class="col-sm-2 control-label">Status*</label>
                            <div class="col-sm-10">
                                <?php $selected = 1; ?><br>
                                <?php echo form_label('Publish', '1') . ' ' . form_radio(array('name' => 'status', 'value' => '1', 'checked' => ('1' == $selected) ? TRUE : FALSE, 'id' => 'publish')); ?>
                                <?php echo form_label('Draft', '0') . ' ' . form_radio(array('name' => 'status', 'value' => '0', 'checked' => ('0' == $selected) ? TRUE : FALSE, 'id' => 'draft')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <?php
                        $save_attr = array('id' => 'btn_save', 'name' => 'btn_save', 'value' => 'Save', 'class' => 'btn btn-primary');
                        echo form_submit($save_attr);
                        ?>    
                        <button type="button" onclick="window.history.back();" class="btn btn-default">Back</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    //validation for edit email formate form
    $(document).ready(function () {
        $('.bootstrap3-editor').wysihtml5();

        $("#add_category_frm").validate({
            rules: {
                parent_id: {
                    required: true,
                },
                name: {
                    required: true,
                },
                slug: {
                    required: true,
                    remote: {
                        url: '<?php echo base_url() ?>' + 'admin/category/checkunique',
                        type: "post",
                        data: {
                            slug: function () {
                                return $("#slug").val();
                            }
                        }
                    }
                },
            },
            messages: {
                parent_id: {
                    required: "Please select parent category.",
                },
                name: {
                    required: "Please enter category name.",
                },
                slug: {
                    required: "Please enter category slug",
                    remote: "This slug is already in used",
                },
            },
        });

    });

    $("#name").blur(function () {
        var name = $("#name").val();
        if (name != "")
        {
            var slug = slugify(name);
            if ($("#slug").val().trim() == "") {
                $("#slug").val(slug);
            }
        }
    });

    function slugify(string) {
        return string
                .toString()
                .trim()
                .toLowerCase()
                .replace(/\s+/g, "-")
                .replace(/[^\w\-]+/g, "")
                .replace(/\-\-+/g, "-")
                .replace(/^-+/, "")
                .replace(/-+$/, "");
    }
</script>