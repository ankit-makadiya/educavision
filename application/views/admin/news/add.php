<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo $module_name; ?>
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('admin/dashboard'); ?>">
                    <i class="fa fa-dashboard"></i>
                    Home
                </a>
            </li>
            <li class="active"><?php echo $module_name; ?></li>
        </ol>
    </section>

    <section class="content-header">
        <?php if ($this->session->flashdata('success')) { ?>
            <div class="callout callout-success">
                <p><?php echo $this->session->flashdata('success'); ?></p>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>  
            <div class="callout callout-danger" >
                <p><?php echo $this->session->flashdata('error'); ?></p>
            </div>
        <?php } ?>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-md-12">

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo $section_title; ?></h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php
                    $form_attr = array('id' => 'add_news_frm', 'enctype' => 'multipart/form-data');
                    echo form_open_multipart('admin/news/add', $form_attr);
                    ?>
                    <div class="box-body">
                        <div class="form-group col-sm-10">
                            <label for="lable_name" name="lable_name" id="lable_name" class="col-sm-2 control-label">News Name*</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name" id="name" placeholder="Enter News Name">
                            </div>
                        </div>

                        <div class="form-group col-sm-10">
                            <label for="lable_slug" name="lable_slug" id="lable_slug" class="col-sm-2 control-label">News Slug*</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="slug" id="slug" placeholder="Enter News Slug">
                            </div>
                        </div>

                        <div class="form-group col-sm-10">
                            <label for="lable_posted_by" name="lable_posted_by" id="lable_posted_by" class="col-sm-2 control-label">News Posted By*</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="posted_by" id="posted_by" placeholder="Enter News Posted By">
                            </div>
                        </div>

                        <div class="form-group col-sm-10">
                            <label for="inputEmail3" class="col-sm-2 control-label">Description*</label>
                            <div class="col-sm-10">
                                <textarea class="form-control ckeditor" id="description" name="description" rows="10" cols="80"></textarea>
                            </div>
                        </div>
                        <div class="form-group col-sm-10">
                            <label class="control-label col-md-2">Image*</label>
                            <div class="col-md-8">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" id="newsImage" style="width: 200px; height: 195px;"></div>
                                    <div>
                                        <button type="button" id="news_images" name="news_images" class="btn btn-primary btn-file fileinput-new"  onclick="openPopup(this.id)" tabindex="2">Select Image</button>

                                        <button type="button" id="remove_news_images" name="remove_news_images" class="btn btn-default"  onclick="removeImage(this.id)" tabindex="2">Remove</button>
                                        <a class="popovers" data-container="body" data-trigger="hover" data-placement="top" data-html="true" <span aria-hidden="true" class="icon-info"></span></a>
                                    </div>
                                    <input type="hidden" id="imageName" name="news_image" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-sm-10">
                            <label for="product_status" id="product_status" class="col-sm-2 control-label">Status*</label>
                            <div class="col-sm-10">
                                <?php $selected = 1; ?><br>
                                <?php echo form_label('Publish', '1') . ' ' . form_radio(array('name' => 'status', 'value' => '1', 'checked' => ('1' == $selected) ? TRUE : FALSE, 'id' => 'publish')); ?>
                                <?php echo form_label('Draft', '0') . ' ' . form_radio(array('name' => 'status', 'value' => '0', 'checked' => ('0' == $selected) ? TRUE : FALSE, 'id' => 'draft')); ?>
                            </div>
                        </div>
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <?php
                        $save_attr = array('id' => 'btn_save', 'name' => 'btn_save', 'value' => 'Save', 'class' => 'btn btn-primary');
                        echo form_submit($save_attr);
                        ?>    
                        <button type="button" onclick="window.history.back();" class="btn btn-default">Back</button>
                        <!--<button type="submit" class="btn btn-info pull-right">Sign in</button>-->
                    </div><!-- /.box-footer -->
                    </form>
                </div><!-- /.box -->


            </div><!--/.col (right) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">
    //validation for edit email formate form
    $(document).ready(function () {
        $('.bootstrap3-editor').wysihtml5();

        $("#add_news_frm").validate({
            ignore: [],
            debug: false,
            rules: {
                name: {
                    required: true,
                },
                posted_by: {
                    required: true,
                },
                slug: {
                    required: true,
                    remote: {
                        url: '<?php echo base_url() ?>' + 'admin/news/checkunique',
                        type: "post",
                        data: {
                            slug: function () {
                                return $("#slug").val();
                            }
                        }
                    }
                },
                description: {
                    required: function ()
                    {
                        CKEDITOR.instances.description.updateElement();
                    },
                },
                news_image: {
                    required: true,
                },
            },
            messages: {
                name: {
                    required: "Please enter news name",
                },
                posted_by: {
                    required: "Please enter news posted by",
                },
                slug: {
                    required: "Please enter news slug",
                    remote: "This slug is already in used",
                },
                description: {
                    required: "Please enter news description.",
                },
                news_image: {
                    required: "Please enter news image.",
                }
            },
        });
    });

    function openPopup(id) {
        var baseUrl = '<?php echo base_url() ?>';
        //$('#or_imge').hide();
        //document.getElementById('form_mainImage').onchange = function () {
        window.KCFinder = {
            callBackMultiple: function (files) {
                window.KCFinder = null;
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    var alt = file.split('/').pop();
                    var fullimage_name = baseUrl + file;
                    var error_msg = 'true';
                    var width;
                    var imgLoader = new Image();
                    imgLoader.src = fullimage_name;
                    var name_separater = "|";
                    var path = file;
                    var cid = parseInt(i) + 100;

                    // return false;
                    if (id != '' && id != undefined) {
                        if (id == 'news_images') {
                            $('#newsImage').html('<img src="' + path + '">');
                            $('#add_news_frm').find('#imageName').val(path);
                        }
                    }
                    // Log image data:
                }

            },
        };

        window.open('/assets/global/plugins/kcfinder/browse.php?type=images&dir=images/public',
                'kcfinder_image', 'status=0, toolbar=0, location=0, menubar=0, ' +
                'directories=0, resizable=1, scrollbars=0, width=800, height=600'
                );
    }

    function removeImage(id) {
        var idVal = id.split('_');
        if (id != '' && id != undefined) {
            if (id == 'remove_prod_images') {
                swal({
                    title: 'Are you sure you want to remove this image?', type: 'warning', showCancelButton: true, confirmButtonText: 'Yes', cancelButtonText: 'No'
                }, function () {
                    $('#productImage img').attr('src', '');
                    $('#imageName').val('');
                    $('#productImage img').attr('alt', '');
                    $('#productImage img').attr('title', '');
                });
            }
        }
    }
    $("#name").blur(function () {
        var name = $("#name").val();
        if (name != "")
        {
            var slug = slugify(name);
            if ($("#slug").val().trim() == "") {
                $("#slug").val(slug);
            }
        }
    });

    function slugify(string) {
        return string
                .toString()
                .trim()
                .toLowerCase()
                .replace(/\s+/g, "-")
                .replace(/[^\w\-]+/g, "")
                .replace(/\-\-+/g, "-")
                .replace(/^-+/, "")
                .replace(/-+$/, "");
    }
</script>