<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo $module_name; ?>
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('admin/dashboard'); ?>">
                    <i class="fa fa-dashboard"></i>
                    Home
                </a>
            </li>
            <li class="active"><?php echo $module_name; ?></li>
        </ol>
    </section>
    <section class="content-header">
        <?php if ($this->session->flashdata('success')) { ?>
            <div class="callout callout-success">
                <p><?php echo $this->session->flashdata('success'); ?></p>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>
            <div class="callout callout-danger" >
                <p><?php echo $this->session->flashdata('error'); ?></p>
            </div>
        <?php } ?>

    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-md-12">

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo $section_title; ?></h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php
                    $attributes = array('id' => 'edit_email_template_frm');
                    $hidden = array('id' => $email_template[0]['id']);
                    echo form_open('admin/emailtemplate/edit', $attributes, $hidden);
                    ?>

                    <div class="box-body">
                        <div class="form-group col-sm-8">
                            <label for="email_template_title" id="page_title">Email Template Title</label>
                            <?php
                            $data = array(
                                'name' => 'title',
                                'id' => 'title',
                                'value' => $email_template[0]['title'],
                                'class' => 'form-control'
                            );
                            echo form_input($data);
                            ?>
                        </div>
                        <div class="form-group col-sm-8">
                            <label for="email_template_subject" id="page_subject">Email Template Subject</label>
                            <?php
                            $data = array(
                                'name' => 'subject',
                                'id' => 'subject',
                                'value' => $email_template[0]['subject'],
                                'class' => 'form-control'
                            );
                            echo form_input($data);
                            ?>
                        </div>
                        <div class="form-group col-sm-8">
                            <label for="email_template_variables" id="page_title">Email Template Variables</label>
                            <?php
                            $data = array(
                                'name' => 'variables',
                                'id' => 'variables',
                                'value' => $email_template[0]['variables'],
                                'class' => 'form-control',
                                'rows' => '2',
                                'cols' => '50',
                                'readonly' => 'true'
                            );
                            echo form_textarea($data);
                            ?>
                        </div>

                        <div class="form-group col-sm-8">
                            <label for="email_template_variables" id="page_title">Email Template</label>
                            <?php
                            $data = array(
                                'name' => 'template',
                                'id' => 'template',
                                'value' => $email_template[0]['template'],
                                'class' => 'form-control textarea ckeditor',
                                'rows' => '10',
                                'cols' => '50'
                            );
                            echo form_textarea($data);
                            ?>
                        </div>

                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <?php
                        $save_attr = array('id' => 'btn_save', 'name' => 'btn_save', 'value' => 'Save', 'class' => 'btn btn-primary');
                        echo form_submit($save_attr);
                        ?>
                        <button type="button" onclick="window.history.back();" class="btn btn-default">Back</button>
                        <!--<button type="submit" class="btn btn-info pull-right">Sign in</button>-->
                    </div><!-- /.box-footer -->
                    <?php echo form_close(); ?>
                </div><!-- /.box -->

            </div><!--/.col (right) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
//validation for edit email formate form
    $(document).ready(function () {
        $("#edit_email_template_frm").validate({
            rules: {
                name: {
                    required: true,
                    maxlength: 25
                }
            },
            messages:
                    {
                        name: {
                            required: "Please enter valid email_template",
                        }
                    },
        });

        $('.callout-danger').delay(3000).hide('700');
        $('.callout-success').delay(3000).hide('700');
    });

    
    $('.textarea').wysihtml5()
</script>
