<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo $section_title; ?>
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
    <div class="row" >
        <div class="col-xs-12" >
            <?php if ($this->session->flashdata('success')) { ?>
                <div class="alert fade in alert-success myalert">
                    <i class="icon-remove close" data-dismiss="alert"></i>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('error')) { ?>  
                <div class="alert fade in alert-danger myalert" >
                    <i class="icon-remove close" data-dismiss="alert"></i>
                    <?php echo $this->session->flashdata('error'); ?>
                </div>
            <?php } ?>
        </div>
    </div>
    <section class="content">
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3><?php echo $pages_count; ?></h3>
                        <p>Pages</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-file-text"></i>
                    </div>
                    <a href="<?php echo base_url('admin/page') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3><?php echo $category_count; ?></h3>
                        <p>Category</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-life-ring"></i>
                    </div>
                    <a href="<?php echo base_url('admin/category') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3><?php echo $product_count; ?></h3>
                        <p>Product</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-flag"></i>
                    </div>
                    <a href="<?php echo base_url('admin/product') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3><?php echo $news_count; ?></h3>
                        <p>News</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-rocket"></i>
                    </div>
                    <a href="<?php echo base_url('admin/news') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>  
        </div>
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3><?php echo $slider_count; ?></h3>
                        <p>Slider</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-image"></i>
                    </div>
                    <a href="<?php echo base_url('admin/slider') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3><?php echo $order_count; ?></h3>
                        <p>Orders</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-flag"></i>
                    </div>
                    <a href="<?php echo base_url('admin/order') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            
<!--            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3><?php echo $product_count; ?></h3>
                        <p>Product</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-life-ring"></i>
                    </div>
                    <a href="<?php echo base_url('admin/product') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3><?php echo $category_count; ?></h3>
                        <p>Category</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-flag"></i>
                    </div>
                    <a href="<?php echo base_url('admin/category') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3><?php echo $pages_count; ?></h3>
                        <p>Pages</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-users"></i>
                    </div>
                    <a href="<?php echo base_url('admin/page') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>-->
        </div>
        <div class="row">
            <a href="<?php echo base_url('admin/menu') ?>">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-yellow"><i class="fa fa-envelope"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-number">Menu Management</span>
                        </div>
                    </div>
                </div>
            </a>
            <a href="<?php echo base_url('admin/emailtemplate') ?>">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-green"><i class="fa fa-envelope"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-number">Email Template</span>
                        </div>
                    </div>
                </div>
            </a>
            <a href="<?php echo base_url('admin/settings') ?>">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-aqua"><i class="fa fa-cogs"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-number">Settings</span>
                        </div>
                    </div>
                </div>
            </a>
            <div class="clearfix visible-sm-block"></div>
            <a href="<?php echo base_url('admin/dashboard/change_password') ?>">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-red"><i class="fa fa-lock"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-number">Change Password</span>
                        </div>
                    </div>
                </div>
            </a> 
            <!-- /.col -->
            <!-- <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>
    
                <div class="info-box-content">
                  <span class="info-box-text">New Members</span>
                  <span class="info-box-number">2,000</span>
                </div>
              </div>
            </div>
          </div> -->
            <!-- Main row -->
        </div>


    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<style type="text/css">
    .info-box-number{
        margin-top: 25px; 
    }
</style>