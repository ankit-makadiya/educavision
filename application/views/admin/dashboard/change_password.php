<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo $module_name; ?>
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('admin/dashboard'); ?>">
                    <i class="fa fa-dashboard"></i>
                    Home
                </a>
            </li>
            <li class="active"><?php echo $module_name; ?></li>
        </ol>
    </section>
    <section class="content-header">
        <?php if ($this->session->flashdata('success')) { ?>
            <div class="callout callout-success">
                <p><?php echo $this->session->flashdata('success'); ?></p>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>  
            <div class="callout callout-danger" >
                <p><?php echo $this->session->flashdata('error'); ?></p>
            </div>
        <?php } ?>

    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-md-12">

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo $section_title; ?></h3>
                    </div>
                    <?php
                    $form_attr = array('id' => 'change_pass_form', 'class' => 'form-horizontal row-border');
                    echo form_open('admin/dashboard/change_password', $form_attr);
                    ?>
                    <div class="box-body">
                        <div class="form-group col-sm-10">
                            <label for="name" id="page_title">Old Password*</label>
                            <?php
                            $data = array(
                                'name' => 'old_password',
                                'id' => 'old_password',
                                'value' => '',
                                'class' => 'form-control'
                            );
                            echo form_password($data);
                            ?>
                        </div>
                        <div class="form-group col-sm-10">
                            <label for="name" id="page_title">New Password*</label>
                            <?php
                            $data = array(
                                'name' => 'new_password',
                                'id' => 'new_password',
                                'value' => '',
                                'class' => 'form-control'
                            );
                            echo form_password($data);
                            ?>
                        </div>
                        <div class="form-group col-sm-10">
                            <label for="email" id="page_title">Repeat Password*</label>
                            <?php
                            $data = array(
                                'name' => 'confirm_password',
                                'id' => 'confirm_password',
                                'value' => '',
                                'class' => 'form-control'
                            );
                            echo form_password($data);
                            ?>
                        </div>
                    </div>
                    <div class="box-footer">
                        <?php
                        $save_attr = array('id' => 'btn_save', 'name' => 'btn_save', 'value' => 'Save', 'class' => 'btn btn-primary');
                        echo form_submit($save_attr);
                        ?>    
                        <button type="button" onclick="window.history.back();" class="btn btn-default">Back</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    $(document).ready(function () {
        $('.callout-danger').delay(3000).hide('700');
        $('.callout-success').delay(3000).hide('700');
        $("#change_pass_form").validate({

            rules: {
                old_password: {
                    required: true,
                },
                new_password: {
                    required: true,
                    maxlength: 16,
                    minlength: 5
                },
                confirm_password: {
                    required: true,
                    equalTo: '#new_password',
                },
            },
            messages:
                    {
                        old_password: {
                            required: "Old password is required",
                            // remote:"Old Password Not Match",
                        },
                        new_password: {
                            required: "New password is required",
                            minlength: "Password must be between 5-16 characters long"
                        },
                        confirm_password: {
                            required: "Confirm password is required",
                            equalTo: "New Password and Confirm Password Must match"
                        }
                    },
        });
    });
</script>
