<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo $module_name; ?>
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('admin/dashboard'); ?>">
                    <i class="fa fa-dashboard"></i>
                    Home
                </a>
            </li>
            <li class="active"><?php echo $module_name; ?></li>
        </ol>
    </section>
    <section class="content">
        <div class="row" >
            <div class="col-xs-12" >
                <?php if ($this->session->flashdata('success')) { ?>
                    <div class="callout callout-success">
                        <p><?php echo $this->session->flashdata('success'); ?></p>
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('error')) { ?>  
                    <div class="callout callout-danger">
                        <p><?php echo $this->session->flashdata('error'); ?></p>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Manage State</h3>
                        <div class="pull-right">
<!--                            <a href="<?php echo base_url('admin/state/add'); ?>" class="btn btn-primary pull-right">Add State</a>-->
                        </div>
                    </div>
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Country Name</th>
                                    <th>State Name</th>
                                    <th>Modify Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($state_list as $state) { ?>
                                    <tr>
                                        <td><?php echo $state['id'] ?></td>
                                        <td><?php echo $state['countryName'] ?></td>
                                        <td><?php echo $state['name'] ?></td>
                                        <td><?php echo $state['modify_date'] ?></td>
                                        <td><?php echo ($state['status'] == 1) ? 'Published' : 'Draft'; ?></td>
                                        <td>
                                            <?php if(in_array('state-access', $admin_permission)){ ?>
                                            <a href="<?php echo base_url('admin/state/edit/' . $state['id']) ?>" id="edit_state_btn" title="Edit State">
                                                <button type="button" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i></button>
                                            </a>
                                            <?php } ?>
<!--                                            <a href="<?php echo base_url('admin/state/delete/' . $state['id']); ?>" id="delete_btn" title="Delete Page" onclick="if (confirm('Are you sure delete this item?')) {
                                                        return true;
                                                    } else {
                                                        event.stopPropagation();
                                                        event.preventDefault();
                                                    }
                                                    ;">
                                                <button type="button" class="btn btn-primary" style="margin-top: 3px;"><i class="icon-pencil"></i> <i class="fa fa-trash-o"></i></button>
                                            </a>-->
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit State</h4>
            </div>
            <div class="modal-body">
                <?php echo form_open('state/edit', array('name' => 'state_frm_user', 'id' => 'state_frm', 'method' => 'POST')); ?>
                <input type="hidden" class="form-control" name="state_id" id="state_id" value="">
                <div class="row">
                    <div class="form-group col-sm-10">
                        <label for="inputEmail3" name="state" id="state">State Name</label>
                        <input type="text" class="form-control" name="state_name" id="state_name" class="state_name" value="">
                    </div>
                    <div class="col-sm-3   ">
                        <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">send</button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
<script type="text/javascript">
    function getStatename($id)
    {
        var state_id = $id;
        $.ajax({
            url: "<?php echo base_url('state/getCatName'); ?>",
            type: "POST",
            data: {'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>', 'state_id': state_id},
            success: function (data) {
                console.log(data['state_name']);
                $("#state_name").val(data['state_name']);
                $("#state_id").val(state_id);
            }
        });
    }
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#state_frm").validate({
            rules: {
                state_name: {
                    required: true,
                }
            },
            messages:
                    {
                        state_name: {
                            required: "State name is required",
                        }
                    },
        });

    });

</script>
<script language="javascript" type="text/javascript">
    $(document).ready(function () {
        $('.callout-danger').delay(3000).hide('700');
        $('.callout-success').delay(3000).hide('700');
    });
</script>