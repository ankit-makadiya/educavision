<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo $module_name; ?>
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('dashboard'); ?>">
                    <i class="fa fa-dashboard"></i>
                    Home
                </a>
            </li>
            <li class="active"><?php echo $module_name; ?></li>
        </ol>
    </section>
    <section class="content-header">
        <?php if ($this->session->flashdata('success')) { ?>
            <div class="callout callout-success">
                <p><?php echo $this->session->flashdata('success'); ?></p>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>  
            <div class="callout callout-danger" >
                <p><?php echo $this->session->flashdata('error'); ?></p>
            </div>
        <?php } ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo $section_title; ?></h3>
                    </div>
                    <?php
                    $form_attr = array('id' => 'edit_state_frm', 'enctype' => 'multipart/form-data');
                    echo form_open_multipart('admin/localization/stateEdit', $form_attr);
                    ?>
                    <input type="hidden" name="state_id" value="<?php echo $state_detail[0]['id'] ?>">
                    <div class="box-body">
                        <div class="form-group col-sm-10">
                            <label for="lable_country"  class="col-sm-2 control-label">Country</label>
                            <div class="col-sm-10">
                                <select class="form-control country select2" name="country_id" id="country_id" style="width: 100%;" tabindex="-1" required="" >
                                    <?php foreach ($country_list as $country) { ?>
                                        <option value="<?php echo $country['id'] ?>" <?php
                                        if ($country['id'] == $state_detail[0]['country_id']) {
                                            echo 'selected="selected"';
                                        }
                                        ?>><?php echo $country['name'] ?></option>
<?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-sm-10">
                            <label for="inputEmail3" name="name" id="name" class="col-sm-2 control-label">State Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name" id="name" placeholder="State Name" value="<?php echo $state_detail[0]['name'] ?>">
                            </div>
                        </div>
                        <div class="form-group col-sm-10">
                            <label for="page_status" id="page_status" class="col-sm-2 control-label">Status*</label>
                            <div class="col-sm-10">
                                <?php $selected = $state_detail[0]['status']; ?><br>
                                <?php echo form_label('Publish', '1') . ' ' . form_radio(array('name' => 'status', 'value' => '1', 'checked' => ('1' == $selected) ? TRUE : FALSE, 'id' => 'publish')); ?>
<?php echo form_label('Draft', '0') . ' ' . form_radio(array('name' => 'status', 'value' => '0', 'checked' => ('0' == $selected) ? TRUE : FALSE, 'id' => 'draft')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <?php
                        $save_attr = array('id' => 'btn_save', 'name' => 'btn_save', 'value' => 'Save', 'class' => 'btn btn-primary');
                        echo form_submit($save_attr);
                        ?>    
                        <button type="button" onclick="window.history.back();" class="btn btn-default">Back</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    //validation for edit email formate form
    $(document).ready(function () {
        $('.bootstrap3-editor').wysihtml5();

        $(".country").select2({
            placeholder: "Select Country",
            allowClear: true,
            width: '100%'
        });

        $("#edit_state_frm").validate({
            rules: {
                name: {
                    required: true,
                },
                country_id: {
                    required: true,
                }
            },
            messages:
                    {
                        name: {
                            required: "Please enter state name.",
                        },
                        country_id: {
                            required: "Please select country.",
                        },
                    },
        });
    });

</script>