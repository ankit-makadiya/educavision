<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo $module_name; ?>
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('admin/dashboard'); ?>">
                    <i class="fa fa-dashboard"></i>
                    Home
                </a>
            </li>
            <li class="active"><?php echo $module_name; ?></li>
        </ol>
    </section>
    <section class="content">
        <div class="row" >
            <div class="col-xs-12" >
                <?php if ($this->session->flashdata('success')) { ?>
                    <div class="callout callout-success">
                        <p><?php echo $this->session->flashdata('success'); ?></p>
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('error')) { ?>  
                    <div class="callout callout-danger">
                        <p><?php echo $this->session->flashdata('error'); ?></p>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title" style="float: left;">Manage City</h3>
                        <form name="citylist" id="citylist">
                            <div class="col-md-3">
                                <select class="select2 form-control country_id" id="country_id" onchange="changeCountry(this.value)">
                                    <option value="">Select Country</option>
                                    <?php foreach ($country_list as $country) { ?>
                                        <option value="<?php echo $country['id'] ?>" <?php
                                        if ($country['id'] == $country_id) {
                                            echo 'selected="selected"';
                                        }
                                        ?>><?php echo $country['name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select class="select2 form-control state_id" id="state_id">
                                    <option value="">Select State</option>
                                    <?php foreach ($state_list as $state) { ?>
                                        <option value="<?php echo $state['id'] ?>" <?php
                                        if ($state['id'] == $state_id) {
                                            echo 'selected="selected"';
                                        }
                                        ?>><?php echo $state['name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <button type="button" id="listcity" class="btn btn-primary" name="submit">Submit</button>
                        </form>

                        <div class="pull-right">
<!--                            <a href="<?php echo base_url('admin/city/add'); ?>" class="btn btn-primary pull-right">Add City</a>-->
                        </div>
                    </div>
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Country Name</th>
                                    <th>State Name</th>
                                    <th>City Name</th>
                                    <th>Modify Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($city_list as $city) { ?>
                                    <tr>
                                        <td><?php echo $city['id'] ?></td>
                                        <td><?php echo $city['countryName'] ?></td>
                                        <td><?php echo $city['stateName'] ?></td>
                                        <td><?php echo $city['name'] ?></td>
                                        <td><?php echo $city['modify_date'] ?></td>
                                        <td><?php echo ($city['status'] == 1) ? 'Published' : 'Draft'; ?></td>
                                        <td>
                                            <?php if(in_array('city-access', $admin_permission)){ ?>
                                            <a href="<?php echo base_url('admin/city/edit/' . $city['id']) ?>" id="edit_city_btn" title="Edit City">
                                                <button type="button" class="btn btn-primary"> <i class="fa fa-pencil-square-o"></i></button>
                                            </a>
                                            <?php } ?>
    <!--                                            <a href="<?php echo base_url('admin/city/delete/' . $city['id']); ?>" id="delete_btn" title="Delete Page" onclick="if (confirm('Are you sure delete this item?')) {
                                                        return true;
                                                    } else {
                                                        event.stopPropagation();
                                                        event.preventDefault();
                                                    }
                                                    ;">
                                                <button type="button" class="btn btn-primary" style="margin-top: 3px;"><i class="icon-pencil"></i> <i class="fa fa-trash-o"></i></button>
                                            </a>-->
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit City</h4>
            </div>
            <div class="modal-body">
                <?php echo form_open('city/edit', array('name' => 'city_frm_user', 'id' => 'city_frm', 'method' => 'POST')); ?>
                <input type="hidden" class="form-control" name="city_id" id="city_id" value="">
                <div class="row">
                    <div class="form-group col-sm-10">
                        <label for="inputEmail3" name="city" id="city">City Name</label>
                        <input type="text" class="form-control" name="city_name" id="city_name" class="city_name" value="">
                    </div>
                    <div class="col-sm-3   ">
                        <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">send</button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
<script type="text/javascript">
    function getCityname($id)
    {
        var city_id = $id;
        $.ajax({
            url: "<?php echo base_url('city/getCatName'); ?>",
            type: "POST",
            data: {'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>', 'city_id': city_id},
            success: function (data) {
                console.log(data['city_name']);
                $("#city_name").val(data['city_name']);
                $("#city_id").val(city_id);
            }
        });
    }
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#city_frm").validate({
            rules: {
                city_name: {
                    required: true,
                }
            },
            messages:
                    {
                        city_name: {
                            required: "City name is required",
                        }
                    },
        });

    });

</script>
<script language="javascript" type="text/javascript">
    $(document).ready(function () {
        $('.callout-danger').delay(3000).hide('700');
        $('.callout-success').delay(3000).hide('700');
    });
    function changeCountry(value) {
        $.ajax({
            url: '<?php echo base_url() ?>' + 'admin/getStateList',
            type: 'post',
            data: 'country_id=' + value,
            dataType: "html",
            success: function (result) {
                $('#state_id').html(result);
            },
        });
    }

    $('#listcity').on('click', function () {
        var country_id = $('#country_id').val();
        var state_id = $('#state_id').val();

        if (country_id != '' && state_id != '') {
            window.location.href = '<?php echo base_url() ?>' + 'admin/city/' + country_id + '/' + state_id;
        }
    });
</script>