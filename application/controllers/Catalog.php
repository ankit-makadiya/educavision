<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Catalog extends Public_Controller {

    public $data;

    public function __construct() {
        parent::__construct();

        $this->load->model('common');
        $this->load->model('product_filter_model');

        include ("include.php");

        //remove catch so after logout cannot view last visited page if that page is this
        $this->output->set_header('Last-Modified:' . gmdate('D, d M Y H:i:s') . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
        $this->output->set_header('Pragma: no-cache');
    }

    public function index($slug='') {
        $filter_sort = '';
        $filter_pricerange = '';
        $filter_author = '';
        $filter_category = '';
        $filter_page = '';
        if(isset($_GET['sort'])){
            $filter_sort = $_GET['sort'];    
        }
        if(isset($_GET['pricerange'])){
            $filter_pricerange = $_GET['pricerange'];    
        }
        if(isset($_GET['author'])){
            $filter_author = $_GET['author'];    
        }
        if(isset($_GET['category'])){
            $filter_category = $_GET['category'];    
        }
        if(isset($_GET['page'])){
            $filter_page = $_GET['page'];    
        }

        $this->data['pageTitle'] = 'Catalog';
        $this->data['breadcumData'] = array(array('title' => 'Home', 'link' => base_url()),array('title' => 'Catalog', 'link' => base_url().'catalog'));
        $this->data['breadcum'] = $this->load->view('breadcum', $this->data, true);

        $category_data = $this->product_filter_model->fetch_filter_type('category_id');
        
        $this->data['leftCategory'] = $categoryTree = $this->getCategoryTree();
        $this->data['sort'] = $filter_sort;

        $category_ids = array();
        foreach($category_data->result_array() as $row){
            $categoryIdArr = explode(',', $row['category_id']);
            foreach($categoryIdArr as $key => $value){
                if(!empty($value) && $value != ' '){
                    array_push($category_ids, $value);        
                }
            }
        }
        $category_ids = array_unique($category_ids);
        $category_ids = implode(',', $category_ids);
        
        $condition_array = array('status' => 1, 'is_deleted' => 0);
        $search_condition = 'id IN(' . $category_ids . ')';
        $data = 'id,name,slug';
        $this->data['filter_category'] = $categoryList = $this->common->select_data_by_search('category', $search_condition, $condition_array, $data, $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = '', $custom_order_by = '', $group_by = '');

        if(!empty($slug) && $slug != 'all'){
            $condition_array = array('status' => 1, 'is_deleted' => 0, 'slug' => $slug);
            $data = '*';
            $categoryData = $this->common->select_data_by_condition('category', $condition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
        
            if (!empty($categoryData)) {
                $this->data['categoryData'] = $categoryData = $categoryData[0];

                $this->data['pageTitle'] = $categoryData['name'];
                $this->data['breadcumData'] = array(array('title' => 'Home', 'link' => base_url()),array('title' => 'Catalog', 'link' => base_url().'page/catalog'), array('title' => $categoryData['name'], 'link' => ''));
                $this->data['breadcum'] = $this->load->view('breadcum', $this->data, true);

                //book data
                $condition_array = array('FIND_IN_SET ("' . $categoryData['id'] . '", category_id) !=' => '0');
            }
        }else{
            $this->data['pageTitle'] = 'All Category';
            $this->data['breadcumData'] = array(array('title' => 'Home', 'link' => base_url()),array('title' => 'Catalog', 'link' => base_url().'catalog'), array('title' => 'All Category', 'link' => ''));
            $this->data['breadcum'] = $this->load->view('breadcum', $this->data, true);

            //book data
            $condition_array = array();
        }

        $filterPricerange = explode('|', $filter_pricerange);
        $filterAuthor = explode('|', $filter_author);
        $filterCategory = explode('|', $filter_category);

        $data = 'product.*, pv.price, pv.sku, pv.isbn_no, a.slug as attribute_slug';
        $search_condition = $search_condition1 = 'product.status = 1 AND product.is_deleted = 0';
        $search_condition .= ' AND pv.is_default=1';
        if(!empty($filterPricerange[0]) && !empty($filterPricerange[1])){
            $search_condition .= ' AND (price >= "'.$filterPricerange[0].'" AND price <= "'.$filterPricerange[1].'") ';
        }
        if(!empty($filterAuthor[0])){
            $search_condition .= " AND (";
            $or_condition = '';
            foreach ($filterAuthor as $key => $value) {
                $or_condition .= "author_name ='".base64_decode($value)."' OR ";
            }
            $search_condition .= trim($or_condition, ' OR ');
            $search_condition .= " )";
        }
        if(!empty($filterCategory[0])){
            $search_condition .= " AND (";
            $or_condition = '';
            foreach ($filterCategory as $key => $value) {
                $or_condition .= "FIND_IN_SET ('" . $value . "', category_id) OR ";
            }
            $search_condition .= trim($or_condition, ' OR ');
            $search_condition .= " )";
        }

        $short_by ='product.priority';    
        $order_by = 'asc';
        if(!empty($filter_sort)){
            if($filter_sort == 'PriceLowtoHigh'){
                $short_by ='price';    
                $order_by = 'asc';   
            }elseif($filter_sort == 'PriceHightoLow'){
                $short_by ='price';    
                $order_by = 'desc';   
            }elseif($filter_sort == 'NewesttoOldest'){
                $short_by ='created_date';    
                $order_by = 'desc';   
            }elseif($filter_sort == 'OldesttoNewest'){
                $short_by ='created_date';    
                $order_by = 'asc';   
            }elseif($filter_sort == 'BestSeller'){
                $short_by ='best_seller';    
                $order_by = 'desc';   
            }elseif($filter_sort == 'ItemOfTheMonth'){
                $short_by ='item_of_the_month';    
                $order_by = 'desc';   
            }
        }

        $join_str = array();
        $join_str[0]['table'] = 'product_variation as pv';
        $join_str[0]['join_table_id'] = 'pv.product_id';
        $join_str[0]['from_table_id'] = 'product.id';
        $join_str[0]['join_type'] = 'left';

        $join_str[1]['table'] = 'attribute as a';
        $join_str[1]['join_table_id'] = 'a.id';
        $join_str[1]['from_table_id'] = 'pv.attribute_id';
        $join_str[1]['join_type'] = 'left';

        $this->data['productPriceData'] = $productPriceData = $this->common->select_data_by_search('product', $search_condition1, $condition_array, 'MIN(pv.price) as min_price,MAX(pv.price) as max_price, count(*) as Total', $short_by, $order_by, $limit = '', $offset = '', $join_str, $custom_order_by = '', $group_by = '');

        /* Pagination Data */
        $item_limit = 12;
        $item_offset = 0;

        if (empty($filter_page)) {
            $filter_page = 1;
        } 
        $item_offset = ($filter_page - 1) * $item_limit;

        $productData = $this->common->select_data_by_search('product', $search_condition, $condition_array, $data, $short_by, $order_by, $limit = $item_limit, $offset = $item_offset, $join_str, $custom_order_by = '', $group_by = '');

        $fullProductData = $this->common->select_data_by_search('product', $search_condition, $condition_array, 'count(*) as fullTotal', $short_by, $order_by, $limit = '', $offset = '', $join_str, $custom_order_by = '', $group_by = '');

        $this->data['filter_author'] = $this->common->select_data_by_search('product', 'status = 1 AND is_deleted = 0', $condition_array, 'distinct(author_name)', $short_by='', $order_by='', $limit = '', $offset = '', $join_str = '', $custom_order_by = '', $group_by = '');

        $this->data['productData'] = $productData;

        $paginationData = array();
        $paginationData['pLinks'] = 5;
        $paginationData['pMids'] = 3;
        $paginationData['pTot'] = ceil($fullProductData[0]['fullTotal'] / $item_limit);
        $paginationData['pSel'] = $filter_page;
        $paginationData['url']  = base_url() . 'category/'.$slug;
        $paginationData['total_count'] = $fullProductData[0]['fullTotal'];
        $paginationData['offset'] = $item_offset;
        $paginationData['limit'] = $item_limit;
        
        $this->data['paginationData'] = $paginationData;
            
        /* Pagination Data */

        $this->data['leftCategory'] = $categoryTree = $this->getCategoryTree();
        $this->data['sort'] = $filter_sort;

        if(empty($filter_pricerange)){
            $filter_pricerange = intval($productPriceData[0]['min_price']) .'|'. intval($productPriceData[0]['max_price']);
        }

        $filterData = array();
        $filterData['pricerange'] = explode('|', $filter_pricerange);
        $filterData['author'] = explode('|', $filter_author);
        $filterData['category'] = explode('|', $filter_category);
        $filterData['page'] = $filter_page;

        $this->data['filterData'] = $filterData;
        $this->load->view('front/catalog/index', $this->data);
    }

    function getCategoryTree($level = 0, $prefix = '') {
        $contition_array = array('is_deleted' => 0, 'status' => 1, 'parent_id' =>$level);
        $data = 'id, name, slug, parent_id';
        $returnCategoryHerarchy = array();
        $categoryData = $this->common->select_data_by_condition('category', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

        $category = '';
        if (count($categoryData) > 0) {
            foreach ($categoryData as $row) {
            $category .= '<li><a href="'.base_url().'catalog/'.$row['slug'].'">'. $prefix . $row['name'] . '</li>';
                // Append subcategories
            $category .= $this->getCategoryTree($row['id'], $prefix . '&nbsp;&nbsp;');
            }
        }
        return $category;
    }
    public function search(){
        $keyword = $this->input->get('q');
        $filter_sort = '';
        $filter_pricerange = '';
        $filter_author = '';
        $filter_category = '';
        $filter_page = '';
        if(!empty($this->input->get('sort'))){
            $filter_sort = $this->input->get('sort');    
        }
        if(!empty($this->input->get('pricerange'))){
            $filter_pricerange = $this->input->get('pricerange');    
        }
        if(!empty($this->input->get('author'))){
            $filter_author = $this->input->get('author');    
        }
        if(!empty($this->input->get('category'))){
            $filter_category = $this->input->get('category');    
        }
        if(!empty($this->input->get('page'))){
            $filter_page = $this->input->get('page');    
        }

        if(!empty($keyword)){
            $category_data = $this->product_filter_model->fetch_filter_type('category_id');
            $category_ids = array();
            foreach($category_data->result_array() as $row){
                $categoryIdArr = explode(',', $row['category_id']);
                foreach($categoryIdArr as $key => $value){
                    array_push($category_ids, $value);    
                }
            }
            $category_ids = array_unique($category_ids);
            $category_ids = implode(',', $category_ids);
                
            $condition_array = array('status' => 1, 'is_deleted' => 0);
            $search_condition = 'id IN(' . $category_ids . ')';
            $data = 'id,name,slug';
            $this->data['filter_category'] = $categoryList = $this->common->select_data_by_search('category', $search_condition, $condition_array, $data, $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = '', $custom_order_by = '', $group_by = '');


            $filterPricerange = explode('|', $filter_pricerange);
            $filterAuthor = explode('|', $filter_author);
            $filterCategory = explode('|', $filter_category);

            $data = 'product.*, pv.price, pv.sku, pv.isbn_no,a.slug as attribute_slug';
            $search_condition = $search_condition1 = 'product.status = 1 AND product.is_deleted = 0';

            $condition_array = array('product.status' => 1, 'product.is_deleted' => 0);
            
            $search_condition .= " AND pv.is_default=1";
        
            $search_condition .= " AND (product.name LIKE '%".$keyword."%' OR pv.isbn_no LIKE '%".$keyword."%' OR product.author_name LIKE '%".$keyword."%' OR product.description LIKE '%".$keyword."%' OR product.tags LIKE '%".$keyword."%' OR pv.sku LIKE '%".$keyword."%')";
 
            if(!empty($filterPricerange[0]) && !empty($filterPricerange[1])){
                $search_condition .= ' AND (pv.price >= "'.$filterPricerange[0].'" AND pv.price <= "'.$filterPricerange[1].'") ';
            }
            if(!empty($filterAuthor[0])){
                $search_condition .= " AND (";
                $or_condition = '';
                foreach ($filterAuthor as $key => $value) {
                    $or_condition .= "author_name ='".base64_decode($value)."' OR ";
                }
                $search_condition .= trim($or_condition, ' OR ');
                $search_condition .= " )";
            }
            if(!empty($filterCategory[0])){
                $search_condition .= " AND (";
                $or_condition = '';
                foreach ($filterCategory as $key => $value) {
                    $or_condition .= "FIND_IN_SET ('" . $value . "', category_id) OR ";
                }
                $search_condition .= trim($or_condition, ' OR ');
                $search_condition .= " )";
            }

            $short_by ='product.priority';
            $order_by = 'asc';
            if(!empty($filter_sort)){
                if($filter_sort == 'PriceLowtoHigh'){
                    $short_by ='price';
                    $order_by = 'asc';   
                }elseif($filter_sort == 'PriceHightoLow'){
                    $short_by ='price';    
                    $order_by = 'desc';   
                }elseif($filter_sort == 'NewesttoOldest'){
                    $short_by ='created_date';    
                    $order_by = 'desc';   
                }elseif($filter_sort == 'OldesttoNewest'){
                    $short_by ='created_date';    
                    $order_by = 'asc';   
                }elseif($filter_sort == 'BestSeller'){
                    $short_by ='best_seller';    
                    $order_by = 'desc';   
                }elseif($filter_sort == 'ItemOfTheMonth'){
                    $short_by ='item_of_the_month';    
                    $order_by = 'desc';   
                }
            }

            $join_str = array();
            $join_str[0]['table'] = 'product_variation as pv';
            $join_str[0]['join_table_id'] = 'pv.product_id';
            $join_str[0]['from_table_id'] = 'product.id';
            $join_str[0]['join_type'] = 'left';
            
            $join_str[1]['table'] = 'attribute as a';
            $join_str[1]['join_table_id'] = 'a.id';
            $join_str[1]['from_table_id'] = 'pv.attribute_id';
            $join_str[1]['join_type'] = 'left';
            
            $this->data['productPriceData'] = $productPriceData = $this->common->select_data_by_search('product', $search_condition1, $condition_array, 'MIN(pv.price) as min_price,MAX(pv.price) as max_price, count(*) as Total', $short_by, $order_by, $limit = '', $offset = '', $join_str, $custom_order_by = '', $group_by = '');

            /* Pagination Data */
            $item_limit = 12;
            $item_offset = 0;

            if (empty($filter_page)) {
                $filter_page = 1;
            } 
            $item_offset = ($filter_page - 1) * $item_limit;
            
            $productData = $this->common->select_data_by_search('product', $search_condition, $condition_array, $data, $short_by, $order_by, $limit = $item_limit, $offset = $item_offset, $join_str, $custom_order_by = '', $group_by = '');

            $fullProductData = $this->common->select_data_by_search('product', $search_condition, $condition_array, 'count(*) as fullTotal', $short_by, $order_by, $limit = '', $offset = '', $join_str, $custom_order_by = '', $group_by = '');


            $this->data['filter_author'] = $this->common->select_data_by_search('product', 'product.status = 1 AND product.is_deleted = 0', $condition_array, 'distinct(author_name)', $short_by = '', $order_by = '', $limit = '', $offset = '', $join_str = '', $custom_order_by = '', $group_by = '');

            $this->data['productData'] = $productData;
            $paginationData = array();
            $paginationData['pLinks'] = 5;
            $paginationData['pMids'] = 3;
            $paginationData['pTot'] = ceil($fullProductData[0]['fullTotal'] / $item_limit);
            $paginationData['pSel'] = $filter_page;
            $paginationData['url']  = base_url() . 'search?q='.$keyword;
            $paginationData['total_count'] = $fullProductData[0]['fullTotal'];
            $paginationData['offset'] = $item_offset;
            $paginationData['limit'] = $item_limit;
            
            $this->data['paginationData'] = $paginationData;
            
            $this->data['pageTitle'] = 'Search';
            $this->data['breadcumData'] = array(array('title' => 'Home', 'link' => base_url()), array('title' => 'Search', 'link' => ''));
            $this->data['breadcum'] = $this->load->view('breadcum', $this->data, true);
            
            $this->data['leftCategory'] = $categoryTree = $this->getCategoryTree();
            $this->data['keyword'] = $keyword;

            if(empty($filter_pricerange)){
                $filter_pricerange = intval($productPriceData[0]['min_price']) .'|'. intval($productPriceData[0]['max_price']);
            }

            $filterData = array();
            $filterData['pricerange'] = explode('|', $filter_pricerange);
            $filterData['author'] = explode('|', $filter_author);
            $filterData['category'] = explode('|', $filter_category);
            $filterData['page'] = $filter_page;

            $this->data['filterData'] = $filterData;
            $this->load->view('front/catalog/search', $this->data);
        } else {
            redirect('404');
        }
    }
    function getPaginationHtml($paginationData = array()){
        return $pageHtml = $this->load->view('front/catalog/search', $paginationData);
        
    }

}

?>
