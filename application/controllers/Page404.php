<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Page404 extends Public_Controller {

    public $data;

    public function __construct() {
        parent::__construct();

        $this->load->model('common');

        include ("include.php");

        //remove catch so after logout cannot view last visited page if that page is this
        $this->output->set_header('Last-Modified:' . gmdate('D, d M Y H:i:s') . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
        $this->output->set_header('Pragma: no-cache');
    }

    public function index() {
        $this->output->set_status_header('404');

        $this->data['pageTitle'] = '404';
        $this->data['breadcumData'] = array(array('title' => 'Home', 'link' => base_url()), array('title' => 404, 'link' => ''));
        $this->data['breadcum'] = $this->load->view('breadcum', $this->data, true);

        // Make sure you actually have some view file named 404.php
        $this->load->view('front/page/404', $this->data);
    }

}

?>