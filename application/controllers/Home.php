<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends Public_Controller {

    public $data;

    public function __construct() {
        parent::__construct();

        $this->load->model('common');

        include ("include.php");

        //remove catch so after logout cannot view last visited page if that page is this
        $this->output->set_header('Last-Modified:' . gmdate('D, d M Y H:i:s') . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
        $this->output->set_header('Pragma: no-cache');
    }

    public function index() {
        $homePage = $this->data['settingData']['site_home_page'];

        $contition_array = array('status' => 1, 'is_deleted' => 0, 'id' => $homePage);
        $data = '*';
        $pageData = $this->common->select_data_by_condition('pages', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

        if (!empty($pageData)) {
            $this->data['pageData'] = $pageData[0];

            //slider data
            $this->data['sliderData'] = '';
            if($pageData[0]['slider_id'] > 0){
                $this->data['sliderData'] = $this->getSliderData($pageData[0]['slider_id']);    
            }
            $this->load->view('front/home/index', $this->data);
        }else{
            redirect('front/page404');
        }
    }

    function getSliderData($sliderId=''){
        $contition_array = array('is_deleted' => 0, 'slider_id' => $sliderId);
        $data = '*';
        $this->data['sliderImageData'] =  $sliderImageData = $this->common->select_data_by_condition('slider_image', $contition_array, $data, $short_by = 'sort_order', $order_by = 'ASC', $limit = '', $offset = '');

        return $this->load->view('front/page/slider', $this->data, true);
    }

    //logout user
    public function logout() {
        if ($this->session->userdata('educavision_front')) {
            $this->session->unset_userdata('educavision_front');
            redirect('home', 'refresh');
        } else {
            redirect('home', 'refresh');
        }
    }

}

?>