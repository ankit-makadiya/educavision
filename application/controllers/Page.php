<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Page extends Public_Controller {

    public $data;

    public function __construct() {
        parent::__construct();

        $this->load->model('common');
        include ("include.php");

        //remove catch so after logout cannot view last visited page if that page is this
        $this->output->set_header('Last-Modified:' . gmdate('D, d M Y H:i:s') . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
        $this->output->set_header('Pragma: no-cache');
    }

    public function index($slug = '') {
        $contition_array = array('is_deleted' => 0, 'status' => 1, 'slug' => $slug);
        $data = '*';
        $page_data = $this->common->select_data_by_condition('pages', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
        if (!empty($page_data)) {
            $this->data['pageData'] = $page_data;
            $this->data['pageTitle'] = $page_data[0]['name'];
            $this->data['breadcumData'] = array(array('title' => 'Home', 'link' => base_url()), array('title' => $page_data[0]['name'], 'link' => ''));
            $this->data['breadcum'] = $this->load->view('breadcum', $this->data, true);

            $news_data = array();
            $pageTemplate = $page_data[0]['template'];
            if ($pageTemplate == 'news') {
                $contition_array = array('is_deleted' => 0, 'status' => 1);
                $data = '*';
                $news_data = $this->common->select_data_by_condition('news', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
                foreach ($news_data as $key => $value) {
                    $contition_array = array('status' => 1);
                    $data = 'count(*) as total';
                    $news_comment_data = $this->common->select_data_by_condition('news_comment', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
                    $news_data[$key]['comment_count'] = $news_comment_data[0]['total'];
                }
            }
            $this->data['news_data'] = $news_data;
            $this->load->view('front/page/index', $this->data);
        }
    }

    public function page_form_save() {
        unset($_REQUEST['/page/form-save']);
        unset($_REQUEST['q']);
        $returnData = array();
        $form_name = $_REQUEST['form_name'];
        unset($_REQUEST['form_name']);
        $form_data = json_encode($_REQUEST);
        $insert_data = array();
        $insert_data['form_name'] = $form_name;
        $insert_data['form_data'] = $form_data;
        $insert_data['created_date'] = date('Y-m-d H:i:s');
        $insert_data['status'] = 1;

        $formInsertId = $this->common->insert_data_get_id($insert_data, 'form_post');
        if ($formInsertId) {
            $this->load->library('email');
            // $getAppSetting = $this->getAppSetting();
            //SMTP & mail configuration
//            $emailconfig = array(
//                'smtp_host' => $getAppSetting['smtp_host_name'],
//                'smtp_port' => $getAppSetting['smtp_out_going_port'],
//                'smtp_user' => $getAppSetting['smtp_user_name'],
//                'smtp_pass' => $getAppSetting['smtp_password'],
//            );
//            $this->email->initialize($emailconfig);

            // if ($getAppSetting['app_email'] != '') {
            //     $this->email->set_header('Bcc', $getAppSetting['app_email']);
            // }
            
            $mail_body = '';
            $mail_body .= '<h4><b>New Inquiry Received From '.$form_name .' Page</b></h4><br>';
            $mail_body .= '<b>Please Find Below Data</b><br>';
            $form_data = json_decode($form_data, TRUE);
            foreach ($form_data as $key => $value) {
                $newKey = str_replace('_', ' ', $key);
                $fieldName = ucwords($newKey);
                if(!empty($value)){
                    $mail_body .=  '<b>'.$fieldName. '</b> : ' . $value .'<br>';    
                }
            }
            
            // $this->email->to($getAppSetting['app_receiver_email']);
            // $this->email->from($getAppSetting['app_sender_email']);
            // $this->email->subject('New Inquiry from '. $form_name);
            // $this->email->message($mail_body);
            // $this->email->send();

            $this->sendEmail($subject = 'New Inquiry Received From '.$form_name .' Page', $template = $mail_body, $to_email = '');
            
            $returnData['success'] = '1';
            $returnData['message'] = 'Your data has been successfully submitted';
            $returnData['data'] = array();

        } else {
            $returnData['success'] = '0';
            $returnData['message'] = 'Something went wrong';
            $returnData['data'] = array();
        }
        echo json_encode($returnData);
        exit;
    }
    
    public function getAppSetting() {
        $contition_array = array('status' => '1');
        $appSettingData = $this->common->select_data_by_condition('settings', $contition_array, '*', $sortby = 'setting_id', $orderby = 'ASC', $limit = '', $offset = '', $join_str = array(), $group_by = '');

        $appSetting = array();
        $appSetting['app_name'] = $appSettingData[0]['setting_val'];
        $appSetting['app_owner_name'] = $appSettingData[1]['setting_val'];
        $appSetting['app_email'] = $appSettingData[2]['setting_val'];
        $appSetting['app_sender_email'] = $appSettingData[3]['setting_val'];
        $appSetting['app_receiver_email'] = $appSettingData[4]['setting_val'];
        $appSetting['smtp_host_name'] = $appSettingData[6]['setting_val'];
        $appSetting['smtp_out_going_port'] = $appSettingData[7]['setting_val'];
        $appSetting['smtp_user_name'] = $appSettingData[8]['setting_val'];
        $appSetting['smtp_password'] = $appSettingData[9]['setting_val'];
        $appSetting['app_phone'] = $appSettingData[5]['setting_val'];


        return $appSetting;
    }

}

?>