<?php
error_reporting(0);
$CommonAvailableData = array();
$contition_array = array('is_deleted' => 0, 'status' => 1);
$data = 'id,name,slug';
$pageData = $this->common->select_data_by_condition('pages', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

$pageId = array();
$pageSlug = array();
foreach ($pageData as $page) {
    $pageId[] = $page['id'];
    $pageSlug[$page['id']] = $page['slug'];
}
$CommonAvailableData['PageData'] = array('pageId' => $pageId, 'pageSlug' => $pageSlug);

$contition_array = array('is_deleted' => 0, 'status' => 1);
$data = 'id,name,slug';
$categoryData = $this->common->select_data_by_condition('category', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

$categoryId = array();
$categorySlug = array();
foreach ($categoryData as $category) {
    $categoryId[] = $category['id'];
    $categorySlug[$category['id']] = $category['slug'];
}
$CommonAvailableData['CategoryData'] = array('categoryId' => $categoryId, 'categorySlug' => $categorySlug);

$this->data['CommonAvailableData'] = $CommonAvailableData;

$contition_array = array('is_deleted' => 0, 'status' => 1, 'id' => 1);
$data = '*';
$menu_data = $this->common->select_data_by_condition('menu', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
if (!empty($menu_data)) {
    $menuItem = $menu_data[0]['menu_item'];
    $menuItem = json_decode($menuItem, TRUE);
    $this->data['menuItem'] = $menuItem;
}

$contition_array = array('status' => 1);
$data = '*';
$settingData = $this->common->select_data_by_condition('settings', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
$SettingData = array();
foreach ($settingData as $setting) {
    $SettingData[$setting['setting_key']] = $setting['setting_val'];
}
$this->data['settingData'] = $SettingData;

$contition_array = array('status' => 1, 'is_deleted' => 0, 'parent_id' => 0);
$data = 'id, slug, name';
$parentCategoryData = $this->common->select_data_by_condition('category', $contition_array, $data, $short_by = '', $order_by = '', $limit = '10', $offset = '');

$this->data['parentCategoryData'] = $parentCategoryData;

$contition_array = array('status' => 1, 'is_deleted' => 0);
$data = 'id, slug, name, author_name, image';
$recentBook = $this->common->select_data_by_condition('product', $contition_array, $data, $short_by = 'id', $order_by = 'DESC', $limit = '2', $offset = '');

$this->data['recentBook'] = $recentBook;

$this->data['header'] = $this->load->view('header', $this->data, true);
$this->data['footer'] = $this->load->view('footer', $this->data, true);


/* CART DATA UPDATE */
$this->data['cart_contents'] = $this->getCartData();
/* CART DATA UPDATE */
?>