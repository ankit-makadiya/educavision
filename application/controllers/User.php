<?php

error_reporting(0);
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class User extends Public_Controller
{

	public $data;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('common');
		include("include.php");
		//remove catch so after logout cannot view last visited page if that page is this
		$this->output->set_header('Last-Modified:' . gmdate('D, d M Y H:i:s') . 'GMT');
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
		$this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
		$this->output->set_header('Pragma: no-cache');
	}

	public function index()
	{
		$this->load->view('front/user/registration', $this->data);
	}

	public function registration()
	{
		$this->data['pageTitle'] = 'Registration';
		$this->data['breadcumData'] = array(array('title' => 'Home', 'link' => base_url()), array('title' => 'Registration', 'link' => ''));
		$this->data['breadcum'] = $this->load->view('breadcum', $this->data, true);

		$contition_array = array('is_deleted' => '0', 'status' => '1');
		$data = '*';
		$this->data['country_list'] = $country_list = $this->common->select_data_by_condition('country', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

		$this->load->view('front/user/registration', $this->data);
	}

	public function save_registration()
	{
		$insert_data = array();
		$insert_data['firstname'] = $this->input->post('first_name');
		$insert_data['lastname'] = $this->input->post('last_name');
		$insert_data['email'] = $this->input->post('email');
		$insert_data['phone'] = $this->input->post('phone');
		$insert_data['password'] = base64_encode($this->input->post('password'));
		$insert_data['created_date'] = date('Y-m-d H:i:s');
		$insert_data['modify_date'] = date('Y-m-d H:i:s');
		$insert_data['status'] = '1';
		$insert_data['is_deleted'] = '0';

		$insertData = $this->common->insert_data_get_id($insert_data, 'users');

		$insert_address_data['user_id'] = $insertData;
		$insert_address_data['country'] = $this->input->post('country');
		$insert_address_data['state'] = $this->input->post('state');
		$insert_address_data['city'] = $this->input->post('city');
		$insert_address_data['zipcode'] = $this->input->post('zipcode');
		$insert_address_data['company_name'] = $this->input->post('company_name');
		$insert_address_data['address1'] = $this->input->post('address1');
		$insert_address_data['address2'] = $this->input->post('address2');

		$insertAddressData = $this->common->insert_data_get_id($insert_address_data, 'user_address');

		$returnData = array();
		if ($insertData && $insertAddressData) {
			$data = array();
			$data = array_merge($insert_data, $insert_address_data);

			$condition_array = array('uniquename' => 'user_registration_admin', 'status' => 1);
			$e_template = $this->common->select_data_by_condition('email_template', $condition_array, 'subject, template', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $group_by = '');

			$template = $e_template[0]['template'];
			$subject = $e_template[0]['subject'];
			$template = html_entity_decode(
				str_replace("[FIRST_NAME]", $data['firstname'],
					str_replace("[LAST_NAME]", $data['lastname'],
						str_replace("[EMAIL]", $data['email'],
							str_replace("[PHONE_NUMBER]", $data['phone'],
								str_replace("[COUNTRY]", $this->getCountryNameById($data['country']),
									str_replace("[STATE]", $this->getStateNameById($data['state']),
										str_replace("[CITY]", $this->getCityNameById($data['city']),
											str_replace("[ZIPCODE]", $data['zipcode'],
												str_replace("[COMPANY_NAME]", $data['company_name'],
													str_replace("[ADDRESS_LINE1]", $data['address1'],
														str_replace("[ADDRESS_LINE2]", $data['address2'],
															stripslashes($template)
														)
													)
												)
											)
										)
									)
								)
							)
						)
					)
				)
			);
			$this->sendEmail($subject, $template, $to_email = '');
			$returnData['success'] = '1';
			$returnData['message'] = 'Your are successfully registered with us';
			$returnData['data'] = array();
		} else {
			$returnData['success'] = '0';
			$returnData['message'] = 'Something went wrong';
			$returnData['data'] = array();
		}
		echo json_encode($returnData);
		exit;
	}

	public function login()
	{
		if ($this->session->has_userdata('educavision_front')) {
			$this->session->unset_userdata('educavision_front');
			redirect('', 'refresh');
		} else {
			$this->data['pageTitle'] = 'Login';
			$this->data['breadcumData'] = array(array('title' => 'Home', 'link' => base_url()), array('title' => 'Login', 'link' => ''));
			$this->data['breadcum'] = $this->load->view('breadcum', $this->data, true);

			$this->load->view('front/user/login', $this->data);
		}
	}

	public function logout()
	{
		if ($this->session->userdata('educavision_front')) {
			$this->session->unset_userdata('educavision_front');
			redirect('', 'refresh');
		} else {
			redirect('', 'refresh');
		}
	}

	public function getStateList()
	{
		$country_id = $_POST['country_id'];
		$state_id = $_POST['state_id'];

		$contition_array = array('is_deleted' => '0', 'status' => '1', 'country_id' => $country_id);
		$data = '*';
		$state_list = $this->common->select_data_by_condition('state', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

		$return_html = '<option value="">Select State</option>';
		foreach ($state_list as $key => $value) {
			$selected = '';
			if ($state_id == $value['id']) {
				$selected = 'selected="selected"';
			}
			$return_html .= '<option value="' . $value['id'] . '" ' . $selected . '>' . $value['name'] . '</option>';
		}
		echo $return_html;
		exit;
	}

	public function getCityList()
	{
		$state_id = $_POST['state_id'];
		$city_id = $_POST['city_id'];

		$contition_array = array('is_deleted' => '0', 'status' => '1', 'state_id' => $state_id);
		$data = '*';
		$city_list = $this->common->select_data_by_condition('city', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

		$return_html = '<option value="">Select City</option>';
		foreach ($city_list as $key => $value) {
			$selected = '';
			if ($city_id == $value['id']) {
				$selected = 'selected="selected"';
			}
			$return_html .= '<option value="' . $value['id'] . '" ' . $selected . '>' . $value['name'] . '</option>';
		}
		echo $return_html;
		exit;
	}

	public function checkuniqueuseremail()
	{
		$valid = 'true';
		$id = $this->input->post('id');
		$email = $this->input->post('email');
		$contition_array['is_deleted'] = 0;
		$contition_array['email'] = $email;
		if (!empty($id)) {
			$contition_array['id !='] = $id;
		}
		$userData = $this->common->select_data_by_condition('users', $contition_array, '*', $short_by = '', $order_by = '', $limit = '', $offset = '');
		if (!empty($userData)) {
			$valid = 'false';
		}
		echo $valid;
		exit;
	}

	public function login_authentication()
	{
		$returnData = array();
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		if (!empty($email) && !empty($password)) {
			$contition_array['is_deleted'] = 0;
			$contition_array['status'] = 1;
			$contition_array['email'] = $email;
			$contition_array['password'] = base64_encode($password);
			$userData = $this->common->select_data_by_condition('users', $contition_array, '*', $short_by = '', $order_by = '', $limit = '', $offset = '');
			if (!empty($userData)) {
				$this->session->set_userdata('educavision_front', $userData[0]);

				$contition_array = array('user_id' => $userData[0]['id']);
				$data = '*';
				$userAddressData = $this->common->select_data_by_condition('user_address', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
				if (!empty($userAddressData)) :
					unset($userAddressData[0]['id']);
					unset($userAddressData[0]['user_id']);

					$this->session->set_userdata('billing_details', $userAddressData[0]);
					$this->session->set_userdata('shipping_details', $userAddressData[0]);
				endif;
				$returnData['success'] = '1';
				$returnData['message'] = '';
				$returnData['data'] = array();
				$this->updatesessioncart($userData[0]['id']);
				$this->savecart();
			} else {
				$returnData['success'] = '0';
				$returnData['message'] = 'Please enter valid credentials';
				$returnData['data'] = array();
			}
		} else {
			$returnData['success'] = '0';
			$returnData['message'] = 'Something went wrong';
			$returnData['data'] = array();
		}

		echo json_encode($returnData);
		exit;
	}

	public function dashboard()
	{
		$this->data['pageTitle'] = 'Dashboard';
		$this->data['breadcumData'] = array(array('title' => 'Home', 'link' => base_url()), array('title' => 'Dashboard', 'link' => ''));
		$this->data['breadcum'] = $this->load->view('breadcum', $this->data, true);

		$this->load->view('front/user/dashboard', $this->data);
	}

	public function changePassword()
	{

		if ($this->input->post('old_password')) {

			$userData = $this->session->userdata('educavision_front');
			$user_id = $userData['id'];
			$old_password = $this->input->post('old_password');
			$new_password = $this->input->post('new_password');

			$admin_old_password = $this->common->select_data_by_id('admin', 'id', $user_id, 'password');
			$admin_password = $admin_old_password[0]['password'];

			if ($admin_password == md5($old_password)) {
				$update_array = array('password' => md5($new_password));
				$update_result = $this->common->update_data($update_array, 'admin', 'id', $user_id);
				if ($update_result) {
					$this->session->set_flashdata('success', 'Your password is successfully changed.');
					redirect('user/changepassword');
				} else {
					$this->session->set_flashdata('error', 'Something went wrong! Please try Again.');
					redirect('user/changepassword');
				}
			} else {
				$this->session->set_flashdata('error', 'Old password does not match');
				redirect('user/changepassword');
			}
		}
		$this->data['pageTitle'] = 'Change Password';
		$this->data['breadcumData'] = array(array('title' => 'Home', 'link' => base_url()), array('title' => 'Change Password', 'link' => ''));
		$this->data['breadcum'] = $this->load->view('breadcum', $this->data, true);

		$this->load->view('front/user/change_password', $this->data);
	}

	public function changePasswordSave()
	{
		$userData = $this->session->userdata('educavision_front');
		$user_id = $userData['id'];
		$returnData = array();
		$new_password = $this->input->post('new_password');
		if (!empty($user_id) && !empty($new_password)) {
			$update_array['password'] = base64_encode($new_password);
			$update_result = $this->common->update_data($update_array, 'users', 'id', $user_id);

			$returnData['success'] = '1';
			$returnData['message'] = 'Your password has been successfully changed';
			$returnData['data'] = array();
		} else {
			$returnData['success'] = '0';
			$returnData['message'] = 'Something went wrong!';
			$returnData['data'] = array();
		}
		echo json_encode($returnData);
		exit;
	}

	public function editProfile()
	{
		$this->data['pageTitle'] = 'Edit Profile';
		$this->data['breadcumData'] = array(array('title' => 'Home', 'link' => base_url()), array('title' => 'Edit Profile', 'link' => ''));
		$this->data['breadcum'] = $this->load->view('breadcum', $this->data, true);

		$userData = $this->session->userdata('educavision_front');
		$this->data['userId'] = $userData['id'];

		$user_detail = $this->common->select_data_by_id('users', 'id', $this->data['userId'], '*');
		if (!empty($user_detail)) {
			$this->data['module_name'] = 'Profile';
			$this->data['section_title'] = 'Edit Profile';

			$contition_array = array('is_deleted' => '0');
			$data = 'id,name';
			$country_list = $this->common->select_data_by_condition('country', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
			$this->data['country_list'] = $country_list;

			$contition_array = array('user_id' => $user_detail[0]['id']);
			$data = '*';
			$user_address = $this->common->select_data_by_condition('user_address', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

			unset($user_address[0]['id']);
			unset($user_address[0]['user_id']);

			$userDetails = array_merge($user_detail[0], $user_address[0]);

			$this->data['userDetails'] = $userDetails;

			$this->load->view('front/user/edit_profile', $this->data);
		} else {
			$this->session->set_flashdata('error', 'Errorout Occurred. Try Again.');
			redirect('user/editProfile', 'refresh');
		}
	}

	public function edit_profile_save()
	{
		$returnData = array();
		if (!empty($this->input->post('id'))) {
			$id = $this->input->post('id');
			$firstname = $this->input->post('firstname');
			$lastname = $this->input->post('lastname');
			$company_name = $this->input->post('company_name');
			$address1 = $this->input->post('address1');
			$address2 = $this->input->post('address2');
			$country = $this->input->post('country');
			$state = $this->input->post('state');
			$city = $this->input->post('city');
			$zipcode = $this->input->post('zipcode');
			$phone = $this->input->post('phone');

			$updateData = array();
			$updateData['firstname'] = $firstname;
			$updateData['lastname'] = $lastname;
			$updateData['phone'] = $phone;
			$updateData['modify_date'] = date('Y-m-d H:i:s');

			$update_result = $this->common->update_data($updateData, 'users', 'id', $id);

			$updateData = array();
			$updateData['company_name'] = $company_name;
			$updateData['address1'] = $address1;
			$updateData['address2'] = $address2;
			$updateData['country'] = $country;
			$updateData['state'] = $state;
			$updateData['city'] = $city;
			$updateData['zipcode'] = $zipcode;

			$update_result = $this->common->update_data($updateData, 'user_address', 'user_id', $id);

			$returnData['success'] = '1';
			$returnData['message'] = 'Your profile has been successfully updated';
			$returnData['data'] = array();
		} else {
			$returnData['success'] = '0';
			$returnData['message'] = 'Something went wrong';
			$returnData['data'] = array();
		}
		echo json_encode($returnData);
		exit;
	}

	public function orderHistory()
	{
		$this->data['pageTitle'] = 'Order History';
		$this->data['breadcumData'] = array(array('title' => 'Home', 'link' => base_url()), array('title' => 'Order History', 'link' => ''));
		$this->data['breadcum'] = $this->load->view('breadcum', $this->data, true);
		//LOAD order DATA
		$userData = $this->session->userdata('educavision_front');
		$user_id = $userData['id'];
		$contition_array = array('is_deleted' => '0', 'customer_id' => $user_id);
		$data = 'id,order_number,total_qty,total_amount';
		$order_list = $this->common->select_data_by_condition('orders', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

		$this->data['order_list'] = $order_list;
		$this->load->view('front/user/order_history', $this->data);
	}

	public function orderDetails($orderID = 0)
	{
		$this->data['pageTitle'] = 'Order History';
		$this->data['breadcumData'] = array(array('title' => 'Home', 'link' => base_url()), array('title' => 'Order History', 'link' => ''));
		$this->data['breadcum'] = $this->load->view('breadcum', $this->data, true);
		//LOAD order DATA
		$userData = $this->session->userdata('educavision_front');
		$user_id = $userData['id'];
		$contition_array = array('id' => $orderID);
		$data = '*';
		$order_data = $this->common->select_data_by_condition('orders', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
		$this->data['order_data'] = $order_data;

		if ($order_data[0]['customer_id'] != $user_id) {
			redirect('404');
		}

		$contition_array = array('order_id' => $orderID);
		$data = 'order_details.*, a.slug as attribute_slug, pv.download_book';

		$join_str = array();
        $join_str[0]['table'] = 'product_variation as pv';
        $join_str[0]['join_table_id'] = 'pv.sku';
        $join_str[0]['from_table_id'] = 'order_details.product_sku';
        $join_str[0]['join_type'] = 'left';

        $join_str[1]['table'] = 'attribute as a';
        $join_str[1]['join_table_id'] = 'a.id';
        $join_str[1]['from_table_id'] = 'pv.attribute_id';
        $join_str[1]['join_type'] = 'left';
        
		$order_details = $this->common->select_data_by_condition('order_details', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '', $join_str, $custom_order_by = '', $group_by = '');
		$this->data['order_details'] = $order_details;

		$contition_array = array('order_id' => $orderID);
		$data = '*';
		$order_payment = $this->common->select_data_by_condition('order_payment', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

		$this->data['order_payment'] = $order_payment[0];

		$this->load->view('front/user/order_details', $this->data);
	}

	//check old password
	public function check_old_pass()
	{
		if ($this->input->is_ajax_request() && $this->input->post('old_password')) {
			$userData = $this->session->userdata('educavision_front');
			$user_id = $userData['id'];

			$old_pass = $this->input->post('old_password');
			$check_result = $this->common->select_data_by_id('users', 'id', $user_id, 'password');
			if ($check_result[0]['password'] === base64_encode($old_pass)) {
				echo 'true';
				die();
			} else {
				echo 'false';
				die();
			}
		}
	}

	function forgotPassword()
	{
		$email = $this->input->post('email');

		$condition_array = array('email' => $email, 'status' => 1, 'is_deleted' => 0);
		$data = '*';
		$user_data = $this->common->select_data_by_condition('users', $condition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
		if (!empty($user_data)) {

			$condition_array = array('uniquename' => 'user_forgot_password', 'status' => 1);
			$e_template = $this->common->select_data_by_condition('email_template', $condition_array, 'subject, template', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $group_by = '');

			$template = $e_template[0]['template'];
			$subject = $e_template[0]['subject'];
			$template = html_entity_decode(
				str_replace("[FULL_NAME]", $user_data[0]['firstname']. ' ' .$user_data[0]['lastname'],
					str_replace("[PASSWORD]", base64_decode($user_data[0]['password']),
						stripslashes($template)
					)
				)
			);
			$this->sendEmail($subject, $template, $to_email = $user_data[0]['email']);

			$returnData['success'] = '1';
			$returnData['message'] = 'Your credential has been send on your registered email.';
			$returnData['data'] = array();
		} else {
			$returnData['success'] = '0';
			$returnData['message'] = 'Sorry, You are not valid user';
			$returnData['data'] = array();
		}
		echo json_encode($returnData);
	}

	function getEmailTemplate()
	{
		$this->sendEmail($subject = 'test message', $template = 'template', $to_email = '');
	}
	function updatesessioncart($session_user_id){
		$condition_array = array('user_id' => $session_user_id);
		$getCartData = $this->common->select_data_by_condition('savecart', $condition_array, '*', $sortby = '', $orderby = 'ASC', $limit = '', $offset = '', $join_str = array(), $group_by = '');
		if(!empty($getCartData)){
			foreach($getCartData as $cart){
				$product_id = $cart['product_id'];
				$quantity = $cart['qty'];
				$product_sku = $cart['product_sku'];

		        // $contition_array = array('status' => 1, 'is_deleted' => 0, 'id' => $product_id);
		        // $data = '*';
		        // $productData = $this->common->select_data_by_condition('product', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

		        $contition_array = array('product.is_deleted' => '0', 'product.status' => 1, 'pv.sku' => $product_sku);
				$data = 'product.*, pv.price, product.slug, pv.sku, pv.weight, pv.isbn_no, a.slug as book_type';

				$join_str = array();
		        $join_str[0]['table'] = 'product_variation as pv';
		        $join_str[0]['join_table_id'] = 'pv.product_id';
		        $join_str[0]['from_table_id'] = 'product.id';
		        $join_str[0]['join_type'] = 'left';

		        $join_str[1]['table'] = 'attribute as a';
		        $join_str[1]['join_table_id'] = 'a.id';
		        $join_str[1]['from_table_id'] = 'pv.attribute_id';
		        $join_str[1]['join_type'] = 'left';

				$productData = $this->common->select_data_by_condition('product', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '', $join_str, $custom_order_by = '', $group_by = '');


		        if (isset($productData[0]['id']) && !empty($productData[0]['id'])) {

		            $product_category = $productData[0]['category_id'];
		            $product_category = explode(',', $product_category);

		            $shoppingProductData = array();
		            $shoppingProductData['id'] = $productData[0]['sku'];
		            $shoppingProductData['product_id'] = $productData[0]['id'];
		            $shoppingProductData['qty'] = $quantity;
		            $shoppingProductData['name'] = $productData[0]['name'];
		            $shoppingProductData['price'] = $productData[0]['price'];
		            $shoppingProductData['slug'] = $productData[0]['slug'];
		            $shoppingProductData['sku'] = $productData[0]['sku'];
		            $shoppingProductData['author_name'] = $productData[0]['author_name'];
		            $shoppingProductData['image'] = $productData[0]['image'];
		            $shoppingProductData['weight'] = $productData[0]['weight'];
		            $shoppingProductData['isbn_no'] = $productData[0]['isbn_no'];
		            $shoppingProductData['collection'] = $productData[0]['collection'];
		            $shoppingProductData['book_type'] = $productData[0]['book_type'];

		            if ($this->cart->insert($shoppingProductData)) {
		                $cartData = $this->session->userdata('cart_contents');
		                $cartData['cart_tax_data'] = array();
		                $cartData['cart_shipping'] = 0;

		                $this->session->set_userdata('cart_contents', $cartData);
		            } 
		        }
			}
		}
	}
}

?>
