<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Book extends Public_Controller {

    public $data;

    public function __construct() {
        parent::__construct();

        $this->load->model('common');

        include ("include.php");

        //remove catch so after logout cannot view last visited page if that page is this
        $this->output->set_header('Last-Modified:' . gmdate('D, d M Y H:i:s') . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
        $this->output->set_header('Pragma: no-cache');
    }

    public function index($slug = '') {
        $contition_array = array('status' => 1, 'is_deleted' => 0, 'slug' => $slug, 'pv.is_default'=> 1);
        $data = 'product.*, pv.sku, pv.isbn_no, pv.price';

        $join_str = array();
        $join_str[0]['table'] = 'product_variation as pv';
        $join_str[0]['join_table_id'] = 'pv.product_id';
        $join_str[0]['from_table_id'] = 'product.id';
        $join_str[0]['join_type'] = 'left';

        $productData = $this->common->select_data_by_condition('product', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '', $join_str, $custom_order_by = '', $group_by = '');

        if (!empty($productData)) {
            $this->data['productData'] = $productData = $productData[0];

            //$this->data['pageTitle'] = $productData['name'];
            $this->data['pageTitle'] = '';
            $this->data['breadcumData'] = array(array('title' => 'Home', 'link' => base_url()), array('title' => $productData['name'], 'link' => ''));
            $this->data['breadcum'] = $this->load->view('breadcum', $this->data, true);
            $this->data['productData'] = $productData;
            $this->data['suggestedProductData'] = array();
            if(!empty($productData['suggest_book'])){
                $condition_array = array('product.status' => 1, 'product.is_deleted' => 0);
                $productData['suggest_book'] = str_replace(",", "','", $productData['suggest_book']);
                $search_condition = "product_variation.sku IN('" . $productData['suggest_book'] . "')";
                $data = 'product.*, sku, isbn_no, price,a.slug as attribute_slug';

                $join_str = array();
                $join_str[0]['table'] = 'product';
                $join_str[0]['join_table_id'] = 'product.id';
                $join_str[0]['from_table_id'] = 'product_variation.product_id';
                $join_str[0]['join_type'] = 'left';

                $join_str[1]['table'] = 'attribute as a';
                $join_str[1]['join_table_id'] = 'a.id';
                $join_str[1]['from_table_id'] = 'product_variation.attribute_id';
                $join_str[1]['join_type'] = 'left';
            
                $this->data['suggestedProductData'] = $suggestedProductData = $this->common->select_data_by_search('product_variation', $search_condition, $condition_array, $data, $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str, $custom_order_by = '', $group_by = '');
            }

            $contition_array = array('attribute.status' => 1, 'pv.product_id' =>$productData['id']);
            $data = 'attribute.id, attribute.name,attribute.slug,pv.is_default,pv.isbn_no, pv.sku, pv.price';

            $join_str = array();
            $join_str[0]['table'] = 'product_variation as pv';
            $join_str[0]['join_table_id'] = 'pv.attribute_id';
            $join_str[0]['from_table_id'] = 'attribute.id';
            $join_str[0]['join_type'] = 'left';

            $this->data['attributeData'] = $attributeData = $this->common->select_data_by_condition('attribute', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '', $join_str, $custom_order_by = '', $group_by = '');

            $this->data['leftCategory'] = $categoryTree = $this->getCategoryTree();
            $this->load->view('front/book/index', $this->data);
        } else {
            redirect('404');
        }
    }

    function getCategoryTree($level = 0, $prefix = '') {
        $contition_array = array('is_deleted' => 0, 'status' => 1, 'parent_id' => $level);
        $data = 'id, name, slug, parent_id';
        $returnCategoryHerarchy = array();
        $categoryData = $this->common->select_data_by_condition('category', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

        $category = '';
        if (count($categoryData) > 0) {
            foreach ($categoryData as $row) {
                $category_name = $row['name'];
                if(empty($prefix)){ $category_name = '<b>'. strtoupper($row['name']).'</b>'; }
                $category .= '<li><a href="' . base_url() . 'catalog/' . $row['slug'] . '">' . $prefix . $category_name . '</li>';
                // Append subcategories
                $category .= $this->getCategoryTree($row['id'], $prefix . '&nbsp;&nbsp;');
            }
        }
        return $category;
    }
}

?>
