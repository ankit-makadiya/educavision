<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class News extends Public_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        
        $this->load->model('common');
        include ("include.php");
        
        //remove catch so after logout cannot view last visited page if that page is this
        $this->output->set_header('Last-Modified:' . gmdate('D, d M Y H:i:s') . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
        $this->output->set_header('Pragma: no-cache');
    }

    public function index($slug = '') {
        $contition_array = array('is_deleted' => 0, 'status' => 1, 'slug' => $slug);
        $data = '*';
        $news_data = $this->common->select_data_by_condition('news', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
        if (!empty($news_data)) {
            $this->data['newsData'] = $news_data[0];
            $this->data['pageTitle'] = 'News';
            $this->data['breadcumData'] = array(array('title' => 'Home', 'link' => base_url()), array('title' => 'News', 'link' => base_url().'page/news'), array('title' => $news_data[0]['name'], 'link' => ''));
            $this->data['breadcum'] = $this->load->view('breadcum', $this->data, true);

            $contition_array = array('status' => 1, 'news_id' => $news_data[0]['id']);
            $data = '*';
            $this->data['newsCommentData'] = $this->common->select_data_by_condition('news_comment', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

            $this->data['leftCategory'] = $categoryTree = $this->getCategoryTree();
            
            $this->load->view('front/news/index', $this->data);
        }
    }

    function getCategoryTree($level = 0, $prefix = '') {
        $contition_array = array('is_deleted' => 0, 'status' => 1, 'parent_id' =>$level);
        $data = 'id, name, slug, parent_id';
        $returnCategoryHerarchy = array();
        $categoryData = $this->common->select_data_by_condition('category', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

        $category = '';
        if (count($categoryData) > 0) {
            foreach ($categoryData as $row) {
            $category .= '<li><a href="'.base_url().'catalog/'.$row['slug'].'">'. $prefix . $row['name'] . '</li>';
                // Append subcategories
            $category .= $this->getCategoryTree($row['id'], $prefix . '&nbsp;&nbsp;');
            }
        }
        return $category;
    }

    public function add_comments(){
        $insert_data = array();
        $insert_data['news_id'] = $this->input->post('news_id');
        $insert_data['name'] = $this->input->post('name');
        $insert_data['email'] = $this->input->post('email');
        $insert_data['comment'] = $this->input->post('comment');
        $insert_data['created_date'] = date('Y-m-d H:i:s');
        $insert_data['status'] = '2';
        
        $insertData = $this->common->insert_data_get_id($insert_data, 'news_comment');
        $returnData = array();
        if ($insertData) {
            $returnData['success'] = '1';
            $returnData['message'] = 'Your comment has been successfully submitted';
            $returnData['data'] = array();
        }else{
            $returnData['success'] = '0';
            $returnData['message'] = 'Something went wrong';
            $returnData['data'] = array();
        }
        echo json_encode($returnData);
        exit;
    }
}

?>