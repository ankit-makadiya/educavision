<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Shoppingcart extends Public_Controller {

    public $data;

    public function __construct() {
        parent::__construct();

        $this->load->model('common');

        include ("include.php");

        //remove catch so after logout cannot view last visited page if that page is this
        $this->output->set_header('Last-Modified:' . gmdate('D, d M Y H:i:s') . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
        $this->output->set_header('Pragma: no-cache');

        header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
    }

    public function index() {
        $this->data['pageData'] = array();
        $this->data['pageTitle'] = 'Shopping Cart';
        $this->data['breadcumData'] = array(array('title' => 'Home', 'link' => base_url()), array('title' => 'Shopping Cart', 'link' => ''));
        $this->data['breadcum'] = $this->load->view('breadcum', $this->data, true);

        $contition_array = array('is_deleted' => '0');
        $data = '*';
        $country_list = $this->common->select_data_by_condition('country', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
        $this->data['country_list'] = $country_list;

        $this->load->view('front/sales/shoppingcart', $this->data);
    }

    public function checkout() {
        $this->data['pageData'] = array();
        $this->data['pageTitle'] = 'Checkout';
        $this->data['breadcumData'] = array(array('title' => 'Home', 'link' => base_url()), array('title' => 'Checkout', 'link' => ''));
        $this->data['breadcum'] = $this->load->view('breadcum', $this->data, true);

        $contition_array = array('is_deleted' => '0');
        $data = '*';
        $country_list = $this->common->select_data_by_condition('country', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
        $this->data['country_list'] = $country_list;

        $user_data = $this->session->userdata('educavision_front');
        $shipping_details = $this->session->userdata('shipping_details');
        $billing_details = $this->session->userdata('billing_details');

        $this->data['user_data'] = $user_data;
        $this->data['shipping_details'] = $shipping_details;
        $this->data['billing_details'] = $billing_details;
        $this->load->view('front/sales/checkout', $this->data);
    }

    public function thankyou($order_id = '') {
        if (!empty($order_id)) {
            $order_id = base64_decode($order_id);
            $this->data['pageData'] = array();
            $this->data['pageTitle'] = 'Thank you';
            $this->data['breadcumData'] = array(array('title' => 'Home', 'link' => base_url()), array('title' => 'Thank you', 'link' => ''));
            $this->data['breadcum'] = $this->load->view('breadcum', $this->data, true);
            $this->data['order_data'] = array();
            
            $contition_array = array('id' => $order_id);
            $data = '*';
            $order_data = $this->common->select_data_by_condition('orders', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '', $join_str, $custom_order_by = '', $group_by = '');
            if(!empty($order_data)){
                $this->data['order_data'] = $order_data[0];
            }
            
            $contition_array = array('order_id' => $order_id);
            $data = 'order_details.*, a.slug as attribute_slug, pv.download_book';
            
            $join_str = array();
            $join_str[0]['table'] = 'product_variation as pv';
            $join_str[0]['join_table_id'] = 'pv.sku';
            $join_str[0]['from_table_id'] = 'order_details.product_sku';
            $join_str[0]['join_type'] = 'left';

            $join_str[1]['table'] = 'attribute as a';
            $join_str[1]['join_table_id'] = 'a.id';
            $join_str[1]['from_table_id'] = 'pv.attribute_id';
            $join_str[1]['join_type'] = 'left';

            $order_details = $this->common->select_data_by_condition('order_details', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '', $join_str, $custom_order_by = '', $group_by = '');

            if(!empty($order_details)){
                $this->data['order_details'] = $order_details;
            }
            
            $contition_array = array('order_id' => $order_id);
            $data = '*';
            $order_payment = $this->common->select_data_by_condition('order_payment', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
            if(!empty($order_payment)){
                $this->data['order_payment'] = $order_payment[0];
            }
            
            $this->load->view('front/sales/thankyou', $this->data);
        } else {
            redirect('404');
        }
    }

    public function addtocart() {
        $returnData = array();
        $product_id = $this->input->post('id');
		$quantity = $this->input->post('quantity');
        $attribute = $this->input->post('attribute');

        $contition_array = array('product.status' => 1, 'product.is_deleted' => 0, 'product.id' => $product_id, 'a.slug' => $attribute);

        $data = 'product.*, pv.sku, pv.weight, pv.isbn_no, pv.price, a.slug as book_type';

        $join_str = array();
        $join_str[0]['table'] = 'product_variation as pv';
        $join_str[0]['join_table_id'] = 'pv.product_id';
        $join_str[0]['from_table_id'] = 'product.id';
        $join_str[0]['join_type'] = 'left';

        $join_str[1]['table'] = 'attribute as a';
        $join_str[1]['join_table_id'] = 'a.id';
        $join_str[1]['from_table_id'] = 'pv.attribute_id';
        $join_str[1]['join_type'] = 'left';

        $productData = $this->common->select_data_by_condition('product', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '', $join_str, $custom_order_by = '', $group_by = '');

        if (isset($productData[0]['id']) && !empty($productData[0]['id'])) {

            $product_category = $productData[0]['category_id'];
            $product_category = explode(',', $product_category);

            $shoppingProductData = array();
            $shoppingProductData['id'] = $productData[0]['sku'];
            $shoppingProductData['product_id'] = $productData[0]['id'];
            $shoppingProductData['qty'] = $quantity;
            $shoppingProductData['name'] = $productData[0]['name'];
            $shoppingProductData['price'] = $productData[0]['price'];
            $shoppingProductData['slug'] = $productData[0]['slug'];
            //$shoppingProductData['sku'] = $productData[0]['sku'];
            $shoppingProductData['author_name'] = $productData[0]['author_name'];
            $shoppingProductData['image'] = $productData[0]['image'];
            if(!empty($productData[0]['weight'])){
                $shoppingProductData['weight'] = $productData[0]['weight'];
            }
            $shoppingProductData['isbn_no'] = $productData[0]['isbn_no'];
            $shoppingProductData['collection'] = $productData[0]['collection'];
            $shoppingProductData['book_type'] = $productData[0]['book_type'];
            
            if ($this->cart->insert($shoppingProductData)) {
                $cartData = $this->session->userdata('cart_contents');
                $cartData['cart_tax_data'] = array();
                $cartData['cart_shipping'] = 0;

                $this->session->set_userdata('cart_contents', $cartData);
                $returnData['success'] = '1';
                $returnData['message'] = 'Product added to Shopping Cart successfully';
                $returnData['data'] = array();
                $this->savecart();
            } else {
                $returnData['success'] = '0';
                $returnData['message'] = 'Something went wrong';
                $returnData['data'] = array();
            }
        } else {
            $returnData['success'] = '0';
            $returnData['message'] = 'Sorry! now, this product is not available';
            $returnData['data'] = array();
        }
        echo json_encode($returnData);
        exit;
    }

    public function removecart() {
        $returnData = array();
        $rawid = $this->input->post('rawid');
        $itemData = $this->cart->get_item($rawid);
        if ($this->cart->remove($rawid)) {
            if(!empty($itemData)){
                $this->removesavecart($itemData['id']);    
            }
            $returnData['data'] = array();
            $returnData['success'] = '1';
            $returnData['message'] = 'Product successfully remove from Shopping Cart';
        } else {
            $returnData['data'] = array();
            $returnData['success'] = '0';
            $returnData['message'] = 'Something went wrong';
        }
        echo json_encode($returnData);
        exit;
    }

    public function updatecart() {
        $returnData = array();
        $updateData = array();
        $updateData['rowid'] = $this->input->post('rowid');
        $updateData['qty'] = $this->input->post('qty');
        if ($this->cart->update($updateData)) {
            $this->savecart();
            $returnData['success'] = '1';
            $returnData['message'] = 'Product successfully update in Shopping Cart';
            $returnData['data'] = array();
        } else {
            $returnData['success'] = '0';
            $returnData['message'] = 'Something went wrong';
            $returnData['data'] = array();
        }
        echo json_encode($returnData);
        exit;
    }

    public function calculate_shipping_tax() {
        $shipping_country = $this->input->post('shipping_country');

        if (!empty($shipping_country)) {
            $cartData = $this->data['cart_contents'];
            $cartData['cart_shipping_country'] = $shipping_country;

            $this->session->set_userdata('cart_contents', $cartData);
        }

        $returnData['success'] = '1';
        $returnData['message'] = 'Cart successfully updated';
        $returnData['data'] = array();
        echo json_encode($returnData);
        exit;
    }

    public function updateShippingTax() {
        $country_id = $this->input->post('country_id');

        if (!empty($country_id)) {
            $cartData = $this->data['cart_contents'];
            $cartData['cart_shipping_country'] = $country_id;
            $this->session->set_userdata('cart_contents', $cartData);
        }
        $cart_contents = $this->getCartData();
        $cart_contents['cart_shipping'] = $this->cart->format_number($cart_contents['cart_shipping']);
        $cart_contents['cart_tax'] = $this->cart->format_number($cart_contents['cart_tax']);
        $cart_contents['cart_discount'] = $this->cart->format_number($cart_contents['cart_discount']);
        $cart_contents['cart_total'] = $this->cart->format_number($cart_contents['cart_total']);
        $cart_contents['cart_grand_total'] = $this->cart->format_number($cart_contents['cart_grand_total']);
        $returnData['success'] = '1';
        $returnData['message'] = 'Cart successfully updated';
        $returnData['data'] = $cart_contents;
        echo json_encode($returnData);
        exit;
    }

    public function apply_coupon(){
        $coupon_code = $this->input->post('coupon_code');
        $user_data = $this->session->userdata('educavision_front');
        $cartData = $this->session->userdata('cart_contents');
        if(!empty($cartData['cart_coupon_data'])){
            $returnData = array();
            $returnData['success'] = '0';
            $returnData['message'] = 'Sorry! You have already one coupon applied.';
            $returnData['data'] = array();
            echo json_encode($returnData);
            exit;
        }else{
            $condition_array = array('coupon_code' => $coupon_code, 'status' => 1, 'is_deleted' => 0);
            $data = '*';
            $coupon_data = $this->common->select_data_by_condition('coupon', $condition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
            if(!empty($coupon_data)){
                $currentDate = strtotime(date("Y-m-d H:i:s"));
                $couponStartDate = strtotime($coupon_data[0]['start_date']);
                $couponEndDate = strtotime($coupon_data[0]['end_date']);

                if($currentDate > $couponStartDate && $currentDate < $couponEndDate) {
                    $restriction_setting = $coupon_data[0]['restriction_setting'];
                    $restriction_setting = json_decode($restriction_setting , TRUE);

                    $allow_per_email = $restriction_setting['allow_per_email'];
                    $no_of_usage = $restriction_setting['no_of_usage'];
                    if($allow_per_email != 1){
                        $condition_array = array('coupon_id' => $coupon_data[0]['id']);
                        $data = '*';
                        $coupon_usage_data = $this->common->select_data_by_condition('coupon_usage', $condition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
                        if(!empty($coupon_usage_data)){
                            $returnData = array();
                            $returnData['success'] = '0';
                            $returnData['message'] = 'A coupon has already been applied once.';
                            $returnData['data'] = array();
                        }else{
                            $this->check_no_of_usage($coupon_data, $no_of_usage);
                            $returnData = array();
                            $returnData['success'] = '1';
                            $returnData['message'] = 'Coupon has been successfully applied.';
                            $returnData['data'] = array();
                        }
                    }else{
                        $this->check_no_of_usage($coupon_data, $no_of_usage);
                        $returnData = array();
                        $returnData['success'] = '1';
                        $returnData['message'] = 'Coupon has been successfully applied.';
                        $returnData['data'] = array();
                    }
                } else {
                    $returnData = array();
                    $returnData['success'] = '0';
                    $returnData['message'] = 'Sorry! This coupon code is invalid or has expired.';
                    $returnData['data'] = array();
                }
            }else{
                $returnData = array();
                $returnData['success'] = '0';
                $returnData['message'] = 'This coupon code is invalid.';
                $returnData['data'] = array();
            }
        }
        echo json_encode($returnData);
        exit;
    }
	public function apply_coupon_bkp(){
    	$coupon_code = $this->input->post('coupon_code');
		$user_data = $this->session->userdata('educavision_front');
		$cartData = $this->session->userdata('cart_contents');
		if(!empty($cartData['cart_coupon_data'])){
			$returnData = array();
			$returnData['success'] = '0';
			$returnData['message'] = 'Sorry! You have already one coupon applied.';
			$returnData['data'] = array();
			echo json_encode($returnData);
			exit;
		}
		if(!empty($user_data)){
			$condition_array = array('coupon_code' => $coupon_code, 'status' => 1, 'is_deleted' => 0);
			$data = '*';
			$coupon_data = $this->common->select_data_by_condition('coupon', $condition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
			if(!empty($coupon_data)){
				$currentDate = strtotime(date("Y-m-d H:i:s"));
				$couponStartDate = strtotime($coupon_data[0]['start_date']);
				$couponEndDate = strtotime($coupon_data[0]['end_date']);

				if($currentDate > $couponStartDate && $currentDate < $couponEndDate) {
					$restriction_setting = $coupon_data[0]['restriction_setting'];
					$restriction_setting = json_decode($restriction_setting , TRUE);

					$allow_per_email = $restriction_setting['allow_per_email'];
					$no_of_usage = $restriction_setting['no_of_usage'];
					if($allow_per_email != 1){
						$condition_array = array('coupon_id' => $coupon_data[0]['id'], 'customer_id' => $user_data['id']);
						$data = '*';
						$coupon_usage_data = $this->common->select_data_by_condition('coupon_usage', $condition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
						if(!empty($coupon_usage_data)){
							$returnData = array();
							$returnData['success'] = '0';
							$returnData['message'] = 'A coupon has already been applied once.';
							$returnData['data'] = array();
						}else{
							$this->check_no_of_usage($coupon_data, $no_of_usage);
							$returnData = array();
							$returnData['success'] = '1';
							$returnData['message'] = 'Coupon has been successfully applied.';
							$returnData['data'] = array();
						}
					}else{
						$this->check_no_of_usage($coupon_data, $no_of_usage);
						$returnData = array();
						$returnData['success'] = '1';
						$returnData['message'] = 'Coupon has been successfully applied.';
						$returnData['data'] = array();
					}
				} else {
					$returnData = array();
					$returnData['success'] = '0';
					$returnData['message'] = 'Sorry! This coupon code is invalid or has expired.';
					$returnData['data'] = array();
				}
			}else{
				$returnData = array();
				$returnData['success'] = '0';
				$returnData['message'] = 'This coupon code is invalid.';
				$returnData['data'] = array();
			}
		}else{
			$returnData = array();
			$returnData['success'] = '0';
			$returnData['message'] = 'You are not valid user please login with our site.';
			$returnData['data'] = array();
		}

		echo json_encode($returnData);
		exit;
	}

	function check_no_of_usage($coupon_data = array(), $no_of_usage = '0'){
		$coupon_id = $coupon_data[0]['id'];
		if(!empty($no_of_usage)){
			$condition_array = array('coupon_id' => $coupon_id);
			$data = 'count(*) as total';
			$coupon_use_count = $this->common->select_data_by_condition('coupon_usage', $condition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

			$coupon_used_counter = $coupon_use_count[0]['total'];
			if($coupon_used_counter >= $no_of_usage){
				$returnData = array();
				$returnData['success'] = '0';
				$returnData['message'] = 'Coupon usage limit has been reached';
				$returnData['data'] = array();
				echo json_encode($returnData);
				exit;
			}else{
				$this->coupon_apply_cart($coupon_data);
			}
		}else{
			$this->coupon_apply_cart($coupon_data);
		}
	}
	function coupon_apply_cart($coupon_data = array()){
		$coupon_id = $coupon_data[0]['id'];
		$condition_array = array('coupon_id' => $coupon_id);
		$data = '*';
		$coupon_pricing_data = $this->common->select_data_by_condition('coupon_pricing', $condition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
		$coupon_data[0]['pricing_data'] = $coupon_pricing_data;
		$cartData = $this->data['cart_contents'];
		$cartData['cart_coupon_data'] = $coupon_data[0];
		$this->session->set_userdata('cart_contents', $cartData);
		return true;
	}

	function removeCoupon(){
    	$cartData = array();
		$cartData = $this->data['cart_contents'];
		$cartData['cart_coupon_data'] = array();
		$this->session->set_userdata('cart_contents', $cartData);

		$returnData = array();
		$returnData['success'] = '1';
		$returnData['message'] = 'Coupon successfully removed';
		$returnData['data'] = array();
		echo json_encode($returnData);
		exit;
	}
}

?>
