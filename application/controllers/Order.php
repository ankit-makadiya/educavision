<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends Public_Controller {

    public $data;
    public $api_error;

    public function __construct() {
        parent::__construct();

        $this->load->model('common');

        include ("include.php");

        //remove catch so after logout cannot view last visited page if that page is this
        $this->output->set_header('Last-Modified:' . gmdate('D, d M Y H:i:s') . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
        $this->output->set_header('Pragma: no-cache');

        header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
    }

    public function index() {
        require_once('application/libraries/stripe-php/init.php');
        // Set API key 
        \Stripe\Stripe::setApiKey($this->config->item('stripe_secret'));
        if ($this->input->post('stripeToken')) {
            $postData = $this->input->post();

            // Make payment 
            $orderID = $this->payment($postData);
            // If payment successful 
            if ($orderID) {
                //$this->session->unset_userdata('cart_contents');
                $this->cart->destroy();
                $this->removesavecart();
                redirect('thankyou/' . base64_encode($orderID));
            } else {
                $apiError = !empty($this->api_error) ? ' (' . $this->api_error . ')' : '';
                echo $data['error_msg'] = 'Transaction has been failed!' . $apiError;
                exit;
            }
        }
    }

    function payment($postData) {
        require_once('application/libraries/stripe-php/init.php');
        // Set API key 

        \Stripe\Stripe::setApiKey($this->config->item('stripe_secret'));
        // If post data is not empty 
        if (!empty($postData['shipping_country'])) {
            $cartData = $this->data['cart_contents'];
            $cartData['cart_shipping_country'] = $postData['shipping_country'];

            $this->session->set_userdata('cart_contents', $cartData);
        }
        //$cartData = $this->session->userdata('cart_contents');
        $cartData = $this->getCartData();
        if (!isset($cartData['cart_shipping_country'])) {
            $cartData['cart_shipping_country'] = $postData['shipping_country'];
        }

        if (!empty($postData)) {
            $loginUserData = $this->session->userdata('educavision_front');

            $last_order_id = $this->getLastOrderId();
            $last_order_id++;
            $orderNumber = $this->generateNumber(6, $last_order_id);
            $postData['order_number'] = $orderNumber;

            $insert_order_id = $this->orderDataSave($postData);
            $insert_order_details_id = $this->orderDetailsDataSave($insert_order_id, $postData);
            $insert_order_payment_id = $this->orderPaymentDataSave($insert_order_id, $postData);

            $token = $postData['stripeToken'];
            $email = $postData['shipping_email'];

            $is_offline = 0;
            if ($cartData['cart_grand_total'] > 0) {
                // ADD CUSTOMER
                try {
                    // Add customer to stripe 
                    $customer = \Stripe\Customer::create(array(
                                'email' => $email,
                                'source' => $token
                    ));
                } catch (Exception $e) {
                    $update_array['transaction_id'] = '';
                    $update_array['payment_status'] = '2';
                    $update_array['payment_response'] = $e->getMessage();

                    $update_result = $this->common->update_data($update_array, 'order_payment', 'id', $insert_order_payment_id);
                    $this->api_error = $e->getMessage();
                    //return false;
                    return $insert_order_id;
                }
                // ADD CUSTOMER
            } else {
                $is_offline = 1;
            }

            if ($customer || $is_offline) {
                if ($customer) {
                    // ADD PAYMENTS
                    $customerId = $customer->id;
                    $itemPrice = round($cartData['cart_grand_total'], 2);
                    // Convert price to cents 
                    $itemPriceCents = ($itemPrice * 100);
                    $currency = $this->config->item('stripe_currency');
                    $chargeJson = array();
                    try {
                        // Charge a credit or a debit card 
                        $charge = \Stripe\Charge::create(array(
                                    'customer' => $customerId,
                                    'amount' => $itemPriceCents,
                                    'currency' => $currency,
                                    //'source' => $token,
                                    'metadata' => array(
                                        'order_id' => $insert_order_id
                                    )
                        ));
                        // Retrieve charge details 
                        $chargeJson = $charge->jsonSerialize();
                    } catch (Exception $e) {
                        $update_array = array();
                        $update_array['transaction_id'] = '';
                        $update_array['payment_status'] = '2';
                        $update_array['payment_response'] = $e->getMessage();

                        $update_result = $this->common->update_data($update_array, 'order_payment', 'id', $insert_order_payment_id);
                        $this->api_error = $e->getMessage();
                        return $insert_order_id;
                    }
                    // ADD PAYMENTS
                }
                if($is_offline){
                    $itemPrice = round($cartData['cart_grand_total'], 2);
                    $itemPriceCents = ($itemPrice * 100);
                    $currency = $this->config->item('stripe_currency');
                    
                    $chargeJson = array();
                    $chargeJson['balance_transaction'] = time();
                    $chargeJson['amount'] = $itemPriceCents;
                    $chargeJson['currency'] = $currency;
                    $chargeJson['status'] = 'succeeded';
                }
                if ($chargeJson) {
                    // Check whether the charge is successful 
                    if (($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1) || $is_offline) {
                        // Transaction details  
                        $transactionID = $chargeJson['balance_transaction'];
                        $paidAmount = $chargeJson['amount'];
                        $paidAmount = ($paidAmount / 100);
                        $paidCurrency = $chargeJson['currency'];
                        $payment_status = $chargeJson['status'];

                        // If the order is successful 
                        if ($payment_status == 'succeeded') {
                            $update_array = array();
                            $update_array['order_status_id'] = 1;
                            $update_array['payment_status'] = 1;
                            $update_result = $this->common->update_data($update_array, 'orders', 'id', $insert_order_id);

                            $update_array = array();
                            $update_array['transaction_id'] = $transactionID;
                            $update_array['pay_amount'] = $paidAmount;
                            $update_array['payment_status'] = '1';
                            $update_array['payment_response'] = json_encode($chargeJson);
                            if($is_offline){
                                $update_array['payment_method'] = 'offline';
                            }
                            $update_result = $this->common->update_data($update_array, 'order_payment', 'id', $insert_order_payment_id);

                            // coupon used data stored
                            if (!empty($cartData['cart_coupon_data'])) {
                                $coupon_used_data = array();
                                $coupon_used_data['coupon_id'] = $cartData['cart_coupon_data']['id'];
                                $coupon_used_data['customer_id'] = isset($loginUserData['id']) ? $loginUserData['id'] : '';
                                $coupon_used_data['customer_email'] = $postData['shipping_email'];
                                $coupon_used_data['order_id'] = $insert_order_id;
                                $coupon_used_data['coupon_details'] = json_encode($cartData['cart_coupon_data']);
                                $coupon_used_data['coupon_code'] = $cartData['cart_coupon_data']['coupon_code'];
                                $coupon_used_data['used_date'] = date('Y-m-d');
                                $coupon_used_data['created_date'] = date('Y-m-d H:i:s');
                                $coupon_used_data['updated_date'] = date('Y-m-d H:i:s');

                                $insertCouponUsedData = $this->common->insert_data_get_id($coupon_used_data, 'coupon_usage');
                            }
                            /* Send Order Mail */

                            $condition_array = array('uniquename' => 'admin_order_mail', 'status' => 1);
                            $e_template = $this->common->select_data_by_condition('email_template', $condition_array, 'subject, template', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $group_by = '');

                            $template = $e_template[0]['template'];
                            $subject = $e_template[0]['subject'];

                            $this->sendOrderMail($insert_order_id, $subject, $template, $to_email = '');

                            $condition_array = array('uniquename' => 'user_order_mail', 'status' => 1);
                            $e_template = $this->common->select_data_by_condition('email_template', $condition_array, 'subject, template', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $group_by = '');

                            $template = $e_template[0]['template'];
                            $subject = $e_template[0]['subject'];

                            $this->sendOrderMail($insert_order_id, $subject, $template, $to_email = $postData['shipping_email']);

                            /* Send Order Mail */
                            return $insert_order_id;
                        } else {
                            $update_array = array();
                            $update_array['transaction_id'] = $transactionID;
                            $update_array['pay_amount'] = $paidAmount;
                            $update_array['payment_status'] = '2';
                            $update_array['payment_response'] = json_encode($chargeJson);
                            $update_result = $this->common->update_data($update_array, 'order_payment', 'id', $insert_order_payment_id);
                            return $insert_order_id;
                        }
                    } else {
                        $update_array = array();
                        $update_array['transaction_id'] = $transactionID;
                        $update_array['pay_amount'] = $paidAmount;
                        $update_array['payment_status'] = '2';
                        $update_array['payment_response'] = json_encode($chargeJson);
                        $update_result = $this->common->update_data($update_array, 'order_payment', 'id', $insert_order_payment_id);
                        return $insert_order_id;
                    }
                } else {
                    return $insert_order_id;
                }
            } else {
                return $insert_order_id;
            }
        }
        return false;
    }

    function orderDataSave($postData) {
        $loginUserData = $this->session->userdata('educavision_front');

        $billing_details = array();
        if (isset($postData['is_billing_checkbox']) && $postData['is_billing_checkbox'] == 'on') {
            $billing_details['first_name'] = $postData['billing_first_name'];
            $billing_details['last_name'] = $postData['billing_last_name'];
            $billing_details['company_name'] = $postData['billing_company_name'];
            $billing_details['country'] = $postData['billing_country'];
            $billing_details['state'] = $postData['billing_state'];
            $billing_details['city'] = $postData['billing_city'];
            $billing_details['zipcode'] = $postData['billing_zip'];
            $billing_details['address1'] = $postData['billing_address'];
            $billing_details['address2'] = $postData['billing_address1'];
        } else {
            $billing_details['first_name'] = $postData['shipping_first_name'];
            $billing_details['last_name'] = $postData['shipping_last_name'];
            $billing_details['company_name'] = $postData['shipping_company_name'];
            $billing_details['country'] = $postData['shipping_country'];
            $billing_details['state'] = $postData['shipping_state'];
            $billing_details['city'] = $postData['shipping_city'];
            $billing_details['zipcode'] = $postData['shipping_zip'];
            $billing_details['address1'] = $postData['shipping_address'];
            $billing_details['address2'] = $postData['shipping_address1'];
        }

        $shipping_details = array();
        $shipping_details['first_name'] = $postData['shipping_first_name'];
        $shipping_details['last_name'] = $postData['shipping_last_name'];
        $shipping_details['company_name'] = $postData['shipping_company_name'];
        $shipping_details['country'] = $postData['shipping_country'];
        $shipping_details['state'] = $postData['shipping_state'];
        $shipping_details['city'] = $postData['shipping_city'];
        $shipping_details['zipcode'] = $postData['shipping_zip'];
        $shipping_details['address1'] = $postData['shipping_address'];
        $shipping_details['address2'] = $postData['shipping_address1'];

        $this->session->set_userdata('shipping_details', $shipping_details);
        $this->session->set_userdata('billing_details', $billing_details);

        // Retrieve stripe token, card and user info from the submitted form data 
        $token = $postData['stripeToken'];
        $name = $postData['shipping_first_name'] . ' ' . $postData['shipping_last_name'];
        $email = $postData['shipping_email'];
        $card_number = $postData['card_number'];
        $card_number = preg_replace('/\s+/', '', $card_number);
        $card_exp_month = $postData['card_exp_month'];
        $card_exp_year = $postData['card_exp_year'];
        $card_cvc = $postData['card_cvc'];

        //$cartData = $this->session->userdata('cart_contents');
        $cartData = $this->getCartData();
        $orderData = array();
        $orderData['order_number'] = $postData['order_number'];
        $orderData['customer_id'] = isset($loginUserData['id']) ? $loginUserData['id'] : '';
        $orderData['shipping_first_name'] = $postData['shipping_first_name'];
        $orderData['shipping_last_name'] = $postData['shipping_last_name'];
        $orderData['shipping_company_name'] = $postData['shipping_company_name'];
        $orderData['shipping_email'] = $postData['shipping_email'];
        $orderData['shipping_address'] = $postData['shipping_address'];
        $orderData['shipping_address_1'] = $postData['shipping_address1'];
        $orderData['shipping_city'] = $postData['shipping_city'];
        $orderData['shipping_state'] = $postData['shipping_state'];
        $orderData['shipping_country'] = $postData['shipping_country'];
        $orderData['shipping_zipcode'] = $postData['shipping_zip'];
        $orderData['shipping_phone'] = $postData['shipping_phone'];
        //$orderData['shipping_id'] = $postData['shipping_first_name'];
        //$orderData['shipping_type'] = $postData['shipping_first_name'];
        if (isset($postData['is_billing_checkbox']) && $postData['is_billing_checkbox'] == 'on') {
            $orderData['billing_first_name'] = $postData['billing_first_name'];
            $orderData['billing_last_name'] = $postData['billing_last_name'];
            $orderData['billing_company_name'] = $postData['billing_company_name'];
            $orderData['billing_email'] = $postData['billing_email'];
            $orderData['billing_address'] = $postData['billing_address'];
            $orderData['billing_address_1'] = $postData['billing_address1'];
            $orderData['billing_city'] = $postData['billing_city'];
            $orderData['billing_state'] = $postData['billing_state'];
            $orderData['billing_country'] = $postData['billing_country'];
            $orderData['billing_zipcode'] = $postData['billing_zip'];
            $orderData['billing_phone'] = $postData['billing_phone'];
        } else {
            $orderData['billing_first_name'] = $postData['shipping_first_name'];
            $orderData['billing_last_name'] = $postData['shipping_last_name'];
            $orderData['billing_company_name'] = $postData['shipping_company_name'];
            $orderData['billing_email'] = $postData['shipping_email'];
            $orderData['billing_address'] = $postData['shipping_address'];
            $orderData['billing_address_1'] = $postData['shipping_address1'];
            $orderData['billing_city'] = $postData['shipping_city'];
            $orderData['billing_state'] = $postData['shipping_state'];
            $orderData['billing_country'] = $postData['shipping_country'];
            $orderData['billing_zipcode'] = $postData['shipping_zip'];
            $orderData['billing_phone'] = $postData['shipping_phone'];
        }

        $orderData['total_qty'] = $cartData['total_items'];
        $orderData['sub_total'] = $cartData['cart_total'];
        $orderData['total_tax'] = $cartData['cart_tax'];
        $orderData['tax_details'] = json_encode($cartData['cart_tax_data']);
        $orderData['shipping_charge'] = $cartData['cart_shipping'];
        $orderData['total_discount'] = isset($cartData['cart_discount']) ? $cartData['cart_discount'] : '';
        $orderData['discount_details'] = isset($cartData['cart_coupon_data']) ? json_encode($cartData['cart_coupon_data']) : '';
        $orderData['total_amount'] = $cartData['cart_grand_total'];
        $orderData['order_status_id'] = 2;
        $orderData['payment_status'] = 0;

        $orderData['ip_address'] = $this->input->ip_address();
        $orderData['customer_user_agent'] = $this->input->user_agent();
        $orderData['currency_symbol'] = '$';
        $orderData['order_notes'] = $postData['order_notes'];
        $orderData['addition_details'] = '';
        $orderData['status'] = 1;
        $orderData['is_deleted'] = 0;
        $orderData['created_date'] = date('Y-m-d H:i:s');
        $orderData['updated_date'] = date('Y-m-d H:i:s');

        $insertOrderData = $this->common->insert_data_get_id($orderData, 'orders');
        return $insertOrderData;
    }

    function orderDetailsDataSave($insertOrderId) {
        foreach ($this->cart->contents() as $key => $items):
            $orderDetailsData = array();
            $orderDetailsData['order_id'] = $insertOrderId;
            //$orderDetailsData['product_id'] = $items['id'];
            $orderDetailsData['product_id'] = $items['product_id'];
            $orderDetailsData['product_name'] = $items['name'];
            $orderDetailsData['product_sku'] = $items['id'];
            $orderDetailsData['product_qty'] = $items['qty'];
            $orderDetailsData['product_price'] = $items['price'];
            $orderDetailsData['tax_details'] = '';

            $insertOrderData = $this->common->insert_data_get_id($orderDetailsData, 'order_details');
        endforeach;
        return true;
    }

    function orderPaymentDataSave($insertOrderId, $postData) {
        $cartData = $this->session->userdata('cart_contents');
        $loginUserData = $this->session->userdata('educavision_front');

        $last_order_id = $this->getLastOrderId();
        $invoice_number = 'E-' . sprintf('%07d', $last_order_id);
        $orderPaymentData = array();
        $orderPaymentData['order_id'] = $insertOrderId;
        $orderPaymentData['order_number'] = $postData['order_number'];
        $orderPaymentData['invoice_number'] = $invoice_number;
        $orderPaymentData['customer_id'] = ($loginUserData['id']) ? $loginUserData['id'] : '';
        $orderPaymentData['transaction_id'] = '';
        $orderPaymentData['pay_amount'] = $cartData['cart_grand_total'];
        $orderPaymentData['payment_method'] = 'stripe';

        $cardData = array();
        $cardData['card_name'] = $postData['card_name'];
        $cardData['card_number'] = $postData['card_number'];
        $cardData['card_cvc'] = $postData['card_cvc'];
        $cardData['card_exp_month'] = $postData['card_exp_month'];
        $cardData['card_exp_year'] = $postData['card_exp_year'];

        $orderPaymentData['card_details'] = json_encode($cardData);
        $orderPaymentData['payment_status'] = 0;
        $orderPaymentData['payment_response'] = '';
        $orderPaymentData['ip_address'] = $this->input->ip_address();
        $orderPaymentData['created_date'] = date('Y-m-d H:i:s');
        $orderPaymentData['updated_date'] = date('Y-m-d H:i:s');

        $insertOrderPaymentData = $this->common->insert_data_get_id($orderPaymentData, 'order_payment');
        return $insertOrderPaymentData;
    }

    function generateNumber($size = 6, $append = '') {
        $number = substr(number_format(time() * rand(), 0, '', ''), 0, $size);
        if (!empty($append)) {
            $number = $number . $append;
        }
        return $number;
    }

    function getLastOrderId() {
        $contition_array = array();
        $data = 'id';
        $order_data = $this->common->select_data_by_condition('orders', $contition_array, $data, $short_by = 'id', $order_by = 'DESC', $limit = '1', $offset = '');

        return $last = $order_data[0]['id']; // This is fetched from database
    }

    function payment_status($id) {
        $data = array();

        // Get order data from the database 
        $order = $this->product->getOrder($id);

        // Pass order data to the view 
        $data['order'] = $order;
        $this->load->view('products/payment-status', $data);
    }

    function sendOrderMail($order_id = '70', $subject = '', $template = '', $to_email = '') {
        $condition_array = array('id' => $order_id);
        $data = '*';
        $order_data = $this->common->select_data_by_condition('orders', $condition_array, $data, $short_by = '', $order_by = '', $limit = '1', $offset = '');

        $condition_array = array('order_id' => $order_id);
        $data = 'order_details.*, a.slug as attribute_slug, pv.download_book';

        $join_str = array();
        $join_str[0]['table'] = 'product_variation as pv';
        $join_str[0]['join_table_id'] = 'pv.sku';
        $join_str[0]['from_table_id'] = 'order_details.product_sku';
        $join_str[0]['join_type'] = 'left';

        $join_str[1]['table'] = 'attribute as a';
        $join_str[1]['join_table_id'] = 'a.id';
        $join_str[1]['from_table_id'] = 'pv.attribute_id';
        $join_str[1]['join_type'] = 'left';

        $order_details_data = $this->common->select_data_by_condition('order_details', $condition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '', $join_str, $custom_order_by = '', $group_by = '');

        $condition_array = array('order_id' => $order_id);
        $data = '*';
        $order_payment_data = $this->common->select_data_by_condition('order_payment', $condition_array, $data, $short_by = '', $order_by = '', $limit = '1', $offset = '');

        $order_shipping_address = '';
        $order_shipping_address .= '<div style="width:45%; float:left; padding:10px;">';
        $order_shipping_address .= '<p><b><h3>Shipping Details</h3></b></p><hr>';
        $order_shipping_address .= '<p><b>' . $order_data[0]['shipping_company_name'] . '</b></p>';
        $order_shipping_address .= '<p>' . $order_data[0]['shipping_first_name'] . ' ' . $order_data[0]['shipping_last_name'] . '</p>';
        $order_shipping_address .= '<p>' . $order_data[0]['shipping_address'] . ', ' . $order_data[0]['shipping_address_1'] . '</p>';
        $order_shipping_address .= '<p>' . $this->getCityNameById($order_data[0]['shipping_city']) . ', ' . $this->getStateNameById($order_data[0]['shipping_state']) . '</p>';
        $order_shipping_address .= '<p>' . $this->getCountryNameById($order_data[0]['shipping_country']) . '-' . $order_data[0]['shipping_zipcode'] . '</p>';
        $order_shipping_address .= '<p><b>Email</b> : ' . $order_data[0]['shipping_email'] . '</p>';
        $order_shipping_address .= '<p><b>Phone</b> : ' . $order_data[0]['shipping_phone'] . '</p>';
        $order_shipping_address .= '</div>';

        $order_billing_address = '';
        $order_billing_address .= '<div style="width:45%; float:left; padding:10px;">';
        $order_billing_address .= '<p><b><h3>Billing Details</h3></b></p><hr>';
        $order_billing_address .= '<p><b>' . $order_data[0]['billing_company_name'] . '</b></p>';
        $order_billing_address .= '<p>' . $order_data[0]['billing_first_name'] . ' ' . $order_data[0]['billing_last_name'] . '</p>';
        $order_billing_address .= '<p>' . $order_data[0]['billing_address'] . ', ' . $order_data[0]['billing_address'] . '</p>';
        $order_billing_address .= '<p>' . $this->getCityNameById($order_data[0]['billing_city']) . ', ' . $this->getStateNameById($order_data[0]['billing_state']) . '</p>';
        $order_billing_address .= '<p>' . $this->getCountryNameById($order_data[0]['billing_country']) . '-' . $order_data[0]['billing_zipcode'] . '</p>';
        $order_billing_address .= '<p><b>Email</b> : ' . $order_data[0]['billing_email'] . '</p>';
        $order_billing_address .= '<p><b>Phone</b> : ' . $order_data[0]['billing_phone'] . '</p>';
        $order_billing_address .= '</div>';

        $order_details = '';
        $order_details .= '<p><b><h3>Order Details</h3></b></p><hr>';
        $order_details .= '<table class="table_border" style="width:100%;">
            <tbody>
            <tr style="border:1px solid #ccc; height:50px; background:#ccc;">
                <td style="padding:10px;"><b>Id</b></td>
                <td style="padding:10px;"><b>Product Name</b></td>
                <td style="padding:10px;"><b>Product Sku</b></td>
                <td style="padding:10px;"><b>Product Qty</b></td>
                <td style="padding:10px;"><b>Product Price</b></td>
                <td style="padding:10px;"><b>Total Amount</b></td>
            </tr>';
        $i = 1;
        foreach ($order_details_data as $od) {
            $order_details .= '<tr style="height:30px;">';
            $order_details .= '<td style="padding:10px; border-bottom:1px solid #ccc;">' . $i . '</td>
                    <td style="padding:10px; border-bottom:1px solid #ccc;">';
            $order_details .= '<p>' . $od['product_name'] . '</p>';
            if ($od['attribute_slug'] == 'e-book') {
                //$order_details .= '<p><a href="' . base_url() . $od['download_book'] . '" target="_blank">Click Here For Download Your Book</a></p>';
            }
            $order_details .= '</td>
                    <td style="padding:10px; border-bottom:1px solid #ccc;">' . $od['product_sku'] . '</td>
                    <td style="padding:10px; border-bottom:1px solid #ccc;">' . $od['product_qty'] . '</td>
                    <td style="padding:10px; border-bottom:1px solid #ccc;">$' . $od['product_price'] . '</td>
                    <td style="padding:10px; border-bottom:1px solid #ccc;">$' . ($od['product_qty'] * $od['product_price']) . '</td>
                </tr>';
            $i++;
        }

        $order_details .= '<tr style="height:30px;">
                <td></td><td></td><td></td><td></td>
                <td style="padding:10px; border-bottom:1px solid #ccc;"><b>Sub Total</b></td>
                <td style="padding:10px; border-bottom:1px solid #ccc;">$' . $order_data[0]['sub_total'] . '</td>
            </tr>
            <tr style="height:30px;">
                <td></td><td></td><td></td><td></td>
                <td style="padding:10px; border-bottom:1px solid #ccc;"><b>Total Tax</b></td>
                <td style="padding:10px; border-bottom:1px solid #ccc;">$' . $order_data[0]['total_tax'] . '</td>
            </tr>
            <tr style="height:30px;">
                <td></td><td></td><td></td><td></td>
                <td style="padding:10px; border-bottom:1px solid #ccc;"><b>Shipping Charge</b></td>
                <td style="padding:10px; border-bottom:1px solid #ccc;">$' . $order_data[0]['shipping_charge'] . '</td>
            </tr>
            <tr style="height:30px;">
                <td></td><td></td><td></td><td></td>
                <td style="padding:10px; border-bottom:1px solid #ccc;"><b>Discount</b></td>
                <td style="padding:10px; border-bottom:1px solid #ccc;">$' . $order_data[0]['total_discount'] . '</td>
            </tr>
            <tr style="height:30px;">
                <td></td><td></td><td></td><td></td>
                <td style="padding:10px; border-bottom:1px solid #ccc;"><b>Total Amount</b></td>
                <td style="padding:10px; border-bottom:1px solid #ccc;">$' . $order_data[0]['total_amount'] . '</td>
            </tr>
            </tbody>
        </table>';

        $order_payment_details = '';
        $order_payment_details .= '<p><b><h3>Payment Details</h3></b></p><hr>';
        $order_payment_details .= '<table class="table_border" style="width:100%;">';
        $order_payment_details .= '<tbody>';
        $order_payment_details .= '<tr><td style="padding:10px; border-bottom:1px solid #ccc;"><b>Invoice Number</b></td><td style="padding:10px; border-bottom:1px solid #ccc;">' . $order_payment_data[0]['invoice_number'] . '</td><td style="padding:10px; border-bottom:1px solid #ccc;"><b>Transaction Id</b></td><td style="padding:10px; border-bottom:1px solid #ccc;">' . $order_payment_data[0]['transaction_id'] . '</td></tr>';
        // $order_payment_details .= '<tr><td style="padding:10px; border-bottom:1px solid #ccc;"><b>Pay Amount</b></td><td style="padding:10px; border-bottom:1px solid #ccc;">$'.$order_payment_data[0]['pay_amount'].'</td></tr>';
        $order_payment_status = ($order_payment_data[0]['payment_status'] == '1') ? 'Success' : (($order_payment_data[0]['payment_status'] == '2') ? 'Fail' : 'Pending');
        $order_payment_details .= '<tr><td style="padding:10px; border-bottom:1px solid #ccc;"><b>Payment Status</b></td><td style="padding:10px; border-bottom:1px solid #ccc;">' . $order_payment_status . '</td><td style="padding:10px; border-bottom:1px solid #ccc;"><b>Order Date</b></td><td style="padding:10px; border-bottom:1px solid #ccc;">' . date('Y-m-d', strtotime($order_payment_data[0]['created_date'])) . '</td></tr>';
        $order_payment_details .= '</tbody></table>';

        $template1 = html_entity_decode(
                str_replace("[FULL_NAME]", $order_data[0]['billing_first_name'] . ' ' . $order_data[0]['billing_last_name'],
                        str_replace("[INVOICE_NUMBER]", $order_payment_data[0]['invoice_number'],
                                str_replace("[ORDER_NUMBER]", $order_data[0]['order_number'],
                                        str_replace("[ORDER_DETAILS]", $order_details,
                                                str_replace("[ORDER_SHIPPING_ADDRESS]", $order_shipping_address,
                                                        str_replace("[ORDER_BILLING_ADDRESS]", $order_billing_address,
                                                                str_replace("[ORDER_PAYMENT_DETAILS]", $order_payment_details,
                                                                        stripslashes($template)
                                                                )
                                                        )
                                                )
                                        )
                                )
                        )
                )
        );
        return $this->sendEmail($subject, $template1, $to_email);
    }

    function testOrderMail() {
        $condition_array = array('uniquename' => 'admin_order_mail', 'status' => 1);
        $e_template = $this->common->select_data_by_condition('email_template', $condition_array, 'subject, template', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $group_by = '');
        $template = $e_template[0]['template'];
        $subject = $e_template[0]['subject'];

        $this->sendOrderMail(8, $subject, $template, $to_email = '');

        $condition_array = array('uniquename' => 'user_order_mail', 'status' => 1);
        $e_template = $this->common->select_data_by_condition('email_template', $condition_array, 'subject, template', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $group_by = '');

        $template = $e_template[0]['template'];
        $subject = $e_template[0]['subject'];

        $this->sendOrderMail(8, $subject, $template, $to_email = $postData['shipping_email']);
    }

}
