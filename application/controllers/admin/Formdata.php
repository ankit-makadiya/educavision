<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Formdata extends Admin_Controller {

    public $data;

    public function __construct() {

        parent::__construct();

        if (!$this->session->userdata('sl_admin')) {
            redirect('admin/login');
        }

        //site setting details
        $this->load->model('admin/common_model');

        $site_name_values = $this->common_model->select_data_by_id('settings', 'setting_id', '1', '*');
        $this->data['site_name'] = $site_name = $site_name_values[0]['setting_val'];
        $this->data['title'] = 'Formdata | ' . $site_name;

        //remove catch so after logout cannot view last visited page if that page is this
        $this->output->set_header('Last-Modified:' . gmdate('D, d M Y H:i:s') . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
        $this->output->set_header('Pragma: no-cache');
    }

    //display category list
    public function index() {
        $this->data['module_name'] = 'Form Data Management';
        $this->data['section_title'] = 'Form Data Management';
        $contition_array = array('status' => '1');
        $data = '*';
        $form_list = $this->common_model->select_data_by_condition('form_post', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
        $this->data['form_list'] = $form_list;
        //load form data
        $this->template->admin_render('admin/formdata/index', $this->data);
    }

    // view formdata detail
    public function view($id = '') {
        $condition_array = array('form_post.id' => $id);
        $forms_detail = $this->common_model->select_data_by_condition('form_post', $condition_array, 'form_post.*', $short_by = '', $order_by = '', $limit = '', $offset = '', $join_str = array(), $group_by = '');
        if (!empty($forms_detail)) {
            $this->data['module_name'] = 'Manage Form Data';
            $this->data['section_title'] = 'View Form Data';
            $this->data['form_detail'] = $forms_detail;
            $this->data['formDatadetails'] = (array) json_decode($forms_detail[0]['form_data']);
            /* Load Template */
            $this->template->admin_render('admin/formdata/view', $this->data);
        } else {
            $this->session->set_flashdata('error', 'Something went wrong! Try Again.');
            redirect('admin/formdata');
        }
    }

}

?>