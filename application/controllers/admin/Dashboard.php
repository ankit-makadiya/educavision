<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends Admin_Controller {

    public $data;

    public function __construct() {

        parent::__construct();

        if (!$this->session->userdata('sl_admin')) {
            redirect('admin/login');
        }
        //site setting details
        $this->load->model('admin/common_model');
        $site_name_values = $this->common_model->select_data_by_id('settings', 'setting_id', '1', '*');

        $this->data['site_name'] = $site_name = $site_name_values[0]['setting_val'];
        //set header, footer and leftmenu
        $this->data['title'] = 'Dashboard | ' . $site_name;

        //remove catch so after logout cannot view last visited page if that page is this
        $this->output->set_header('Last-Modified:' . gmdate('D, d M Y H:i:s') . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
        $this->output->set_header('Pragma: no-cache');
    }

    //display dashboard
    public function index() {
        $this->data['section_title'] = 'Dashboard';

        // Get users count
        $this->data['order_count'] = count($this->common_model->select_data_by_condition('orders', array("is_deleted" => 0), 'id'));

        $this->data['pages_count'] = count($this->common_model->select_data_by_condition('pages', array("is_deleted" => 0), 'id'));

        $this->data['category_count'] = count($this->common_model->select_data_by_condition('category', array("is_deleted" => 0), 'id'));

        $this->data['product_count'] = count($this->common_model->select_data_by_condition('product', array("is_deleted" => 0), 'id'));

        $this->data['news_count'] = count($this->common_model->select_data_by_condition('news', array("is_deleted" => 0), 'id'));

        $this->data['users_count'] = count($this->common_model->select_data_by_condition('users', array("is_deleted" => 0), 'id'));
        
        $this->data['slider_count'] = count($this->common_model->select_data_by_condition('slider', array("is_deleted" => 0), 'id'));

        /* Load Template */
        $this->template->admin_render('admin/dashboard/index', $this->data);
    }

    //logout user
    public function logout() {
        if ($this->session->userdata('sl_admin')) {
            $this->session->unset_userdata('sl_admin');
            redirect('admin');
        } else {
            redirect('admin');
        }
    }

    public function edit_profile() {

        if ($this->data['loged_in_user'][0]['level'] != '1') {
            $this->session->set_flashdata('error', 'You are not authorized.');
            redirect('dashboard');
        }

        if ($this->input->post('email')) {
            $email = $this->input->post('email');
            $user_name = $this->input->post('user_name');
            $name = $this->input->post('name');
            $user_id = $this->session->userdata('sl_admin');

            $update_result = $this->common_model->update_data($this->input->post(), 'users', 'id', $user_id);
            if ($update_result) {
                $this->session->set_flashdata('success', 'Profile detail successfully updated.');
                redirect('dashboard');
            } else {
                $this->session->set_flashdata('error', 'Something went wrong! Please try Again.');
                redirect('dashboard');
            }
        }

        $this->data['module_name'] = 'Dashboard';
        $this->data['section_title'] = 'Edit Profile';
        $this->template->admin_render('admin/dashboard/edit_profile', $this->data);
    }

    public function change_password() {

        if ($this->input->post('old_password')) {

            $user_id = ($this->session->userdata('sl_admin'));
            $old_password = $this->input->post('old_password');
            $new_password = $this->input->post('new_password');

            $admin_old_password = $this->common_model->select_data_by_id('admin', 'id', 1, 'password');
            $admin_password = $admin_old_password[0]['password'];

            if ($admin_password == md5($old_password)) {
                $update_array = array('password' => md5($new_password));
                $update_result = $this->common_model->update_data($update_array, 'admin', 'id', 1);
                if ($update_result) {
                    $this->session->set_flashdata('success', 'Your password is successfully changed.');
                    redirect('admin/dashboard/change_password');
                } else {
                    $this->session->set_flashdata('error', 'Something went wrong! Please try Again.');
                    redirect('admin/dashboard/change_password');
                }
            } else {
                $this->session->set_flashdata('error', 'Old password does not match');
                redirect('admin/dashboard/change_password');
            }
        }

        $this->data['module_name'] = 'Dashboard';
        $this->data['section_title'] = 'Change Password';
        $this->template->admin_render('admin/dashboard/change_password', $this->data);
    }

    //check old password
    public function check_old_pass() {
        if ($this->input->is_ajax_request() && $this->input->post('old_pass')) {
            $user_id = ($this->session->userdata('sl_admin'));

            $old_pass = $this->input->post('old_pass');
            $check_result = $this->common_model->select_data_by_id('users', 'id', $user_id, 'password');
            if ($check_result[0]['password'] === md5($old_pass)) {
                echo 'true';
                die();
            } else {
                echo 'false';
                die();
            }
        }
    }

    public function check_email() {
        if ($this->input->is_ajax_request() && $this->input->post('email')) {
            $user_id = ($this->session->userdata('sl_admin'));

            $email = $this->input->post('email');
            $check_result = $this->common_model->check_unique_avalibility('users', 'email', $email, 'id', $user_id);
            if ($check_result) {
                echo 'true';
                die();
            } else {
                echo 'false';
                die();
            }
        }
    }

    public function check_username() {
        if ($this->input->is_ajax_request() && $this->input->post('user_name')) {
            $user_id = ($this->session->userdata('sl_admin'));

            $user_name = $this->input->post('user_name');
            $check_result = $this->common_model->check_unique_avalibility('users', 'user_name', $user_name, 'id', $user_id);
            if ($check_result) {
                echo 'true';
                die();
            } else {
                echo 'false';
                die();
            }
        }
    }

}
