<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Order extends Admin_Controller {

    public $data;

    public function __construct() {

        parent::__construct();

        if (!$this->session->userdata('sl_admin')) {
            redirect('admin/login');
        }
        $this->load->model('admin/common_model');

        $site_name_values = $this->common_model->select_data_by_id('settings', 'setting_id', '1', '*');
        $this->data['site_name'] = $site_name = $site_name_values[0]['setting_val'];
        $this->data['title'] = 'Order | ' . $site_name;

        //remove catch so after logout cannot view last visited page if that page is this
        $this->output->set_header('Last-Modified:' . gmdate('D, d M Y H:i:s') . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
        $this->output->set_header('Pragma: no-cache');
    }

    //display tax list
    public function index() {
        $this->data['order_status'] = $status = $this->input->get('status');
        
        $this->data['module_name'] = 'Order Management';
        $this->data['section_title'] = 'Order';

        //LOAD order DATA
        $contition_array['is_deleted'] = 0;
        if($status == 'pending'){
            $contition_array['order_status_id'] = 1;
        }elseif($status == 'confirm'){
            $contition_array['order_status_id'] = 3;
        }
        $data = 'orders.id,orders.order_number,orders.shipping_first_name,orders.shipping_last_name,orders.shipping_email,orders.total_qty,orders.total_amount,orders.created_date,orders.status,op.payment_status';

        $join_str = array();
        $join_str[0]['table'] = 'order_payment as op';
        $join_str[0]['join_table_id'] = 'op.order_id';
        $join_str[0]['from_table_id'] = 'orders.id';
        $join_str[0]['join_type'] = 'left';

        $order_list = $this->common_model->select_data_by_condition('orders', $contition_array, $data, $short_by = 'id', $order_by = 'desc', $limit = '', $offset = '', $join_str, $custom_order_by = '', $group_by = '');
        $this->data['order_list'] = $order_list;
        //load tax data
        $this->template->admin_render('admin/order/index', $this->data);
    }

//    public function orderDetails($orderID = 0) {
//        echo 333;
//        die();
//        $this->data['pageTitle'] = 'Order History';
//        $this->data['breadcumData'] = array(array('title' => 'Home', 'link' => base_url()), array('title' => 'Order History', 'link' => ''));
//        $this->data['breadcum'] = $this->load->view('breadcum', $this->data, true);
//        //LOAD order DATA
//        $userData = $this->session->userdata('educavision_front');
//        $user_id = $userData['id'];
//        $contition_array = array('id' => $orderID);
//        $data = '*';
//        $order_data = $this->common_model->select_data_by_condition('orders', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
//        $this->data['order_data'] = $order_data;
//
//        if ($order_data[0]['customer_id'] != $user_id) {
//            redirect('404');
//        }
//
//        $contition_array = array('order_id' => $orderID);
//        $data = '*';
//        $order_details = $this->common_model->select_data_by_condition('order_details', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
//        $this->data['order_details'] = $order_details;
//
//        $contition_array = array('order_id' => $orderID);
//        $data = '*';
//        $order_payment = $this->common_model->select_data_by_condition('order_payment', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
//
//        $this->data['order_payment'] = $order_payment[0];
//
//        $this->load->view('admin/order/order_details', $this->data);
//    }
    // view users detail
    public function orderDetails($orderID = '') {
        $contition_array = array('id' => $orderID);
        $data = '*';
        $order_data = $this->common_model->select_data_by_condition('orders', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
        $this->data['order_data'] = $order_data;

//        if ($order_data[0]['customer_id'] != $user_id) {
//            redirect('404');
//        }
        // $condition_array = array('status' => '1', 'is_deleted' => '0', 'users.id' => $id);
        $contition_array = array('order_id' => $orderID);
        $data = 'order_details.*, a.slug as attribute_slug, pv.download_book';

        $join_str = array();
        $join_str[0]['table'] = 'product_variation as pv';
        $join_str[0]['join_table_id'] = 'pv.sku';
        $join_str[0]['from_table_id'] = 'order_details.product_sku';
        $join_str[0]['join_type'] = 'left';

        $join_str[1]['table'] = 'attribute as a';
        $join_str[1]['join_table_id'] = 'a.id';
        $join_str[1]['from_table_id'] = 'pv.attribute_id';
        $join_str[1]['join_type'] = 'left';

        $order_details = $this->common_model->select_data_by_condition('order_details', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '', $join_str, $custom_order_by = '', $group_by = '');
        $this->data['order_details'] = $order_details;
//echo '<pre>'; print_r($this->data['order_data']); die();
        $contition_array = array('order_id' => $orderID);
        $data = '*';
        $order_payment = $this->common_model->select_data_by_condition('order_payment', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

        $this->data['order_payment'] = $order_payment[0];

        if (!empty($order_details)) {
            $this->data['module_name'] = 'Manage Orders';
            $this->data['section_title'] = 'View Order';
            $this->data['order_detail'] = $order_details;

            /* Load Template */
            $this->template->admin_render('admin/order/view', $this->data);
        } else {
            $this->session->set_flashdata('error', 'Something went wrong! Try Again.');
            redirect('admin/order');
        }
    }
    
    public function changeorderstatus($order_id=''){
        $update_array = array();
        $update_array['order_status_id'] = '3';
        $update_result = $this->common_model->update_data($update_array, 'orders', 'id', $order_id);
        
        if($update_result){
            $this->session->set_flashdata('success', 'Order successfully changed.');
            redirect('admin/order?status=pending', 'refresh');
        }
    }
    
    public function updatetracking(){
        $order_id = $this->input->post('order_id');
        $tracking = $this->input->post('tracking');
        
        $update_array = array();
        $update_array['tracking_detail'] = $tracking;
        $update_result = $this->common_model->update_data($update_array, 'orders', 'id', $order_id);
        
        if($update_result){
            
            $condition_array = array('id' => $order_id);
            $order_data = $this->common_model->select_data_by_condition('orders', $condition_array, 'shipping_first_name,shipping_last_name,shipping_email,order_number,tracking_detail', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $group_by = '');

            
            $condition_array = array('uniquename' => 'user_order_tracking_mail', 'status' => 1);
            $e_template = $this->common_model->select_data_by_condition('email_template', $condition_array, 'subject, template', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $group_by = '');

            $template = $e_template[0]['template'];
            $subject = $e_template[0]['subject'];
            $template = html_entity_decode(
                str_replace("[FULL_NAME]", $order_data[0]['shipping_first_name']. ' ' .$order_data[0]['shipping_last_name'],
                    str_replace("[ORDER_NUMBER]", $order_data[0]['order_number'],
                        str_replace("[TRACKING_LINK]", $order_data[0]['tracking_detail'],
                            stripslashes($template)
                        )
                    )
                )
            );
            $this->sendEmail($subject, $template, $to_email = $order_data[0]['shipping_email']);

            $this->session->set_flashdata('success', 'Tracking detail successfully updated.');
            redirect('admin/order?status=confirm', 'refresh');
        }
    }

}

?>
