<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Category extends Admin_Controller {

    public $data;

    public function __construct() {

        parent::__construct();

        if (!$this->session->userdata('sl_admin')) {
            redirect('admin/login');
        }

        //site setting details
        $this->load->model('admin/common_model');

        $site_name_values = $this->common_model->select_data_by_id('settings', 'setting_id', '1', '*');
        $this->data['site_name'] = $site_name = $site_name_values[0]['setting_val'];
        $this->data['title'] = 'Category | ' . $site_name;

        //remove catch so after logout cannot view last visited page if that page is this
        $this->output->set_header('Last-Modified:' . gmdate('D, d M Y H:i:s') . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
        $this->output->set_header('Pragma: no-cache');
    }

    //display category list
    public function index() {

        $this->data['module_name'] = 'Category Management';
        $this->data['section_title'] = 'Category Management';

        $contition_array = array('is_deleted' => '0');
        $data = '*';
        $category_list = $this->common_model->select_data_by_condition('category', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

        foreach ($category_list as $key => $category) {
            if ($category['parent_id'] == 0) {
                $category_list[$key]['parentCategory'] = '-';
            } else {
                $contition_array = array('id' => $category['parent_id']);
                $data = 'name';
                $parentCategoryData = $this->common_model->select_data_by_condition('category', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

                $category_list[$key]['parentCategory'] = $parentCategoryData[0]['name'];
            }
        }

        $this->data['category_list'] = $category_list;
        //load category data
        $this->template->admin_render('admin/category/index', $this->data);
    }

    //add the category detail
    public function add() {
        if ($this->input->post()) {
            if ($this->input->post('name') == '') {
                $this->session->set_flashdata('error', 'Category name is required');
                redirect('admin/category', 'refresh');
            }
            $insert_data = array();
            $insert_data['parent_id'] = $this->input->post('parent_id');
            $insert_data['name'] = $this->input->post('name');
            $insert_data['slug'] = $this->input->post('slug');
            $insert_data['description'] = $this->input->post('description');
            $insert_data['created_date'] = date('Y-m-d H:i:s');
            $insert_data['modify_date'] = date('Y-m-d H:i:s');
            $insert_data['status'] = $this->input->post('status');
            $insert_data['is_deleted'] = 0;

            $insertData = $this->common_model->insert_data_get_id($insert_data, 'category');
            if ($insertData) {
                $this->session->set_flashdata('success', 'Category has been sucessfully inserted.');
                redirect('admin/category', 'refresh');
                die();
            } else {
                $this->session->set_flashdata('error', 'oops! something is wrong.');
                redirect('admin/category/add', 'refresh');
                die();
            }
        }
        $this->data['module_name'] = 'Category';
        $this->data['section_title'] = 'Add Category';

        $contition_array = array('is_deleted' => '0');
        $data = '*';
        $this->data['category_list'] = $this->common_model->select_data_by_condition('category', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
        $this->template->admin_render('admin/category/add', $this->data);
    }

    //update the category detail
    public function edit($id = '') {

        if ($this->input->post('category_id')) {

            $category_id = $this->input->post('category_id');
            if ($this->input->post('name') == '') {
                $this->session->set_flashdata('error', 'Category name is required');
                redirect('admin/category', 'refresh');
            } else {
                $update_array = array(
                    'parent_id' => trim($this->input->post('parent_id')),
                    'name' => trim($this->input->post('name')),
                    'slug' => trim($this->input->post('slug')),
                    'description' => trim($this->input->post('description')),
                    'modify_date' => date('Y-m-d H:i:s'),
                    'status' => $this->input->post('status')
                );
                $update_result = $this->common_model->update_data($update_array, 'category', 'id', $this->input->post('category_id'));

                if ($update_result) {
                    $this->session->set_flashdata('success', 'Category successfully updated.');
                    redirect('admin/category', 'refresh');
                } else {
                    $this->session->set_flashdata('error', 'Error Occurred. Try Again!');
                    redirect('admin/category', 'refresh');
                }
            }
        }
        $category_detail = $this->common_model->select_data_by_id('category', 'id', $id, '*');

        if (!empty($category_detail)) {
            $this->data['module_name'] = 'Category';
            $this->data['section_title'] = 'Edit Category';
            $this->data['category_detail'] = $category_detail;

            $contition_array = array('is_deleted' => '0');
            $data = '*';
            $this->data['category_list'] = $this->common_model->select_data_by_condition('category', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');


            $this->template->admin_render('admin/category/edit', $this->data);
        } else {
            $this->session->set_flashdata('error', 'Errorout Occurred. Try Again.');
            redirect('admin/category', 'refresh');
        }
    }

    // get name by id

    function getCatName() {
        if ($this->input->post('category_id')) {
            $category_id = $this->input->post('category_id');
            $contition_array = array('id' => $category_id);
            $data = 'name';
            $category_name = $this->common_model->select_data_by_condition('category', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

            $return_data = array();
            $return_data['category_name'] = $category_name[0]['name'];
            header('Content-Type: application/json');
            echo json_encode($return_data);
            exit;
        }
    }

    public function do_upload_multiple_files($fieldName, $options) {

        $response = array();
        $files = $_FILES;
        $cpt = count($_FILES[$fieldName]['name']);
        for ($i = 0; $i < $cpt; $i++) {
            $_FILES[$fieldName]['name'] = $files[$fieldName]['name'][$i];
            $_FILES[$fieldName]['type'] = $files[$fieldName]['type'][$i];
            $_FILES[$fieldName]['tmp_name'] = $files[$fieldName]['tmp_name'][$i];
            $_FILES[$fieldName]['error'] = $files[$fieldName]['error'][$i];
            $_FILES[$fieldName]['size'] = $files[$fieldName]['size'][$i];

            $this->load->library('upload');
            $this->upload->initialize($options);

            //upload the image
            if (!$this->upload->do_upload($fieldName)) {
                $response['error'][] = $this->upload->display_errors();
            } else {
                $response['result'][] = $this->upload->data();
                $category_thumb[$i]['image_library'] = 'gd2';
                $category_thumb[$i]['source_image'] = $this->config->item('category_main_upload_path') . $response['result'][$i]['file_name'];
                $category_thumb[$i]['new_image'] = $this->config->item('category_thumb_upload_path') . $response['result'][$i]['file_name'];
                $category_thumb[$i]['create_thumb'] = TRUE;
                $category_thumb[$i]['maintain_ratio'] = TRUE;
                $category_thumb[$i]['thumb_marker'] = '';
                $category_thumb[$i]['width'] = $this->config->item('category_thumb_width');
                //$category_thumb[$i]['height'] = $this->config->item('category_thumb_height');
                $category_thumb[$i]['height'] = 2;
                $category_thumb[$i]['master_dim'] = 'width';
                $category_thumb[$i]['quality'] = "100%";
                $category_thumb[$i]['x_axis'] = '0';
                $category_thumb[$i]['y_axis'] = '0';
                $instanse = "image_$i";

                //Loading Image Library
                $this->load->library('image_lib', $category_thumb[$i], $instanse);
                $dataimage = $response['result'][$i]['file_name'];

                //Creating Thumbnail
                $this->$instanse->resize();
                $response['error'][] = $thumberror = $this->$instanse->display_errors();
            }
        }
//        echo "<pre>";
//        print_r($response);
//        die();
        return $response;
    }

    public function delete_image($id = '', $categoryimage_id = '') {
        if ($categoryimage_id != '' && $id != '') {

            $get_image = $this->common_model->select_data_by_id('category_banner', 'id', $categoryimage_id, $data = 'image', $join_str = array());
            $image_name = $get_image[0]['image'];

            $main_image = $this->config->item('category_main_upload_path') . $image_name;
            $thumb_image = $this->config->item('category_thumb_upload_path') . $image_name;

            if (file_exists($main_image)) {
                unlink($main_image);
            }
            if (file_exists($thumb_image)) {
                unlink($thumb_image);
            }
            $this->common_model->delete_data('category_banner', 'id', $categoryimage_id);
            redirect('category/edit/' . $id, 'refresh');
        }
    }
    
    // delete page
    public function delete($id = '') {

        $update_data = array('is_deleted' => 1);
        $update_result = $this->common_model->update_data($update_data, 'category', 'id', $id);

        if ($update_result) {
            $this->session->set_flashdata('success', 'Category deleted successfully.');
            redirect('admin/category');
        } else {
            $this->session->set_flashdata('error', 'Something went wrong! Try Again.');
            redirect('admin/category');
        }
    }
    public function checkunique() {
        $valid = 'true';
        $id = $this->input->post('id');
        $slug = $this->input->post('slug');
        $contition_array['is_deleted'] = 0;
        $contition_array['slug'] = $slug;
        if (!empty($id)) {
            $contition_array['id !='] = $id;
        }
        $categoryData = $this->common_model->select_data_by_condition('category', $contition_array, '*', $short_by = '', $order_by = '', $limit = '', $offset = '');
        if (!empty($categoryData)) {
            $valid = 'false';
        }
        echo  $valid;
        exit;
    }
}

?>