<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends Admin_Controller {

    public $data;

    public function __construct() {
        parent::__construct();

        if ($this->
                session->userdata('sl_admin')) {
            redirect('admin/dashboard');
        }

        $this->data['title'] = "Login | Educavision";
        $this->load->model('common');
        // Load Login Model
        $this->load->model('admin/auth_Model');

        //remove catch so after logout cannot view last visited page if that page is this
        $this->output->set_header('Last-Modified:' . gmdate('D, d M Y H:i:s') . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
        $this->output->set_header('Pragma: no-cache');
    }

    public function index() {
        $this->load->view('admin/login/index', $this->data);
    }

    public function authenticate() {
        $admin_user = $this->input->post('admin_user');
        $admin_password = $this->input->post('admin_password');

        if ($admin_user == '') {
            $this->session->set_flashdata('error', '<div class="callout callout-danger">  <p>Please Enter Email Id</p></div>
');
            redirect('admin');
        }
        if ($admin_password == '') {
            $this->session->set_flashdata('error', '<div class="callout callout-danger">  <p>Please Enter Password</p></div>');
            redirect('admin');
        }

        if ($admin_user != '' && $admin_password != '') {
            $admin_check = $this->auth_Model->check_authentication($admin_user, $admin_password);

            if (count($admin_check) > 0 && $admin_check != 0) {
                $this->session->set_userdata('sl_admin', $admin_check[0]['id']);
                

                $contition_array = array('id' => $admin_check[0]['role_id']);
                $data = 'permissions';
                $role_data = $this->common->select_data_by_condition('role', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
                if(!empty($role_data)){
                    $admin_check[0]['permissions'] = $role_data[0]['permissions'];
                }
                $this->session->set_userdata('sl_admin', $admin_check[0]);
                redirect('admin/dashboard');
            } else {
                $this->session->set_flashdata('error', '<div class="callout callout-danger">  <p>Please enter valid credentials.</p></div>');
                redirect('admin');
            }
        } else {
            $this->session->set_flashdata('error', '<div class="callout callout-danger">  <p>Please enter valid login details.</p></div>');
            redirect('admin');
        }
    }

    public function forgot_password() {
        $forgot_email = $this->input->post('forgot_email');

        if ($forgot_email != '') {
            $forgot_email_check = $this->common->select_data_by_id('admin', 'email', $forgot_email, '*', '');
            if (count($forgot_email_check) > 0) {
                $password = $this->common->randomPassword();
                $data['password'] = md5($password);
                $this->common->update_data($data, 'admin', 'email', $forgot_email);

                $contition_array = array('id' => '1');
                $e_template = $this->common->select_data_by_condition('email_template', $contition_array, '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $group_by = '');
                $mail_body = $e_template[0]['template'];
                $mail_body = html_entity_decode(str_replace("[USER_NAME]", ucfirst($forgot_email_check[0]['username']), str_replace("[USER_PASSWORD]", $password, stripslashes($mail_body))));

                $this->load->library('email');
                $getAppSetting = $this->getAppSetting();

                //SMTP & mail configuration
                $emailconfig = array(
                    'smtp_host' => $getAppSetting['smtp_host_name'],
                    'smtp_port' => $getAppSetting['smtp_out_going_port'],
                    'smtp_user' => $getAppSetting['smtp_user_name'],
                    'smtp_pass' => $getAppSetting['smtp_password'],
                );
                $this->email->initialize($emailconfig);

                if ($getAppSetting['app_email'] != '') {
                    $this->email->set_header('Bcc', $getAppSetting['app_email']);
                }
                $this->email->to($forgot_email_check[0]['email']);
                $this->email->from($getAppSetting['app_sender_email'], $getAppSetting['app_name']);
                $this->email->subject($e_template[0]['subject']);
                $this->email->message($mail_body);
                
                if ($this->email->send()) {
                    $this->session->set_flashdata('success', '<div class="alert alert-success">Password successfully send in your email id.</div>');
                    redirect('admin/login');
                } else {
                    $this->session->set_flashdata('error', '<div class="alert alert-danger">' . $this->email->print_debugger() . '</div>');
                    redirect('admin/login');
                }
//                $this->session->set_flashdata('success', '<div class="alert alert-success">Password successfully send in your email id.</div>');
//                redirect('admin/login');
            } else {

                $this->session->set_flashdata('error', '<div class="alert alert-danger">Please enter register email id.</div>');
                redirect('admin/login');
            }
        } else {
            $this->session->set_flashdata('error', '<div class="alert alert-danger">Please enter email id.</div>');
            redirect('admin/login');
        }
    }

    public function sendEmail($app_name = '', $app_email = '', $to_email = '', $subject = '', $mail_body = '') {


        //Loading E-mail Class
        $this->load->library('email');

//        $emailsetting = $this->common->select_data_by_condition('email_settings', array(), '*');

        $mail_html = '
<table width="100%" cellspacing="10" cellpadding="10" style="background:#f1f1f1;" style="border:2px solid #ccc;" >
<tr>
  <td valign="center"><img src="' . base_url('assets/img/logo.png') . '" alt="' . $this->data['main_site_name'] . '" style="margin:0px auto;display:block;width:150px;"/></td>
</tr>
<tr>
  <td><table border="0" cellpadding="0" cellspacing="0" width="100%">
      <p> "' . $mail_body . '" </p>
    </table></td>
</tr>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr>
    <td style="font-family:Ubuntu, sans-serif;font-size:11px; padding-bottom:15px; padding-top:15px; border-top:1px solid #ccc;text-align:center;background:#eee;">&copy; ' . date("Y") . ' <a href="' . $this->data['main_site_url'] . '" style="color:#268bb9;text-decoration:none;"> ' . $this->data['main_site_name'] . '</a></td>
  </tr>
</table>
</table>
';

        //Loading E-mail Class
        //        $config['protocol'] = "smtp";
//        $config['smtp_host'] = $emailsetting[0]['host_name'];
//        $config['smtp_port'] = $emailsetting[0]['out_going_port'];
//        $config['smtp_user'] = $emailsetting[0]['user_name'];
//        $config['smtp_pass'] = $emailsetting[0]['password'];
//        $config['charset'] = "utf-8";
//        $config['mailtype'] = "html";
//        $config['newline'] = "\r\n";
//        $this->email->initialize($config);

        $this->email->from(APP_FROM_EMAIL, APP_NAME);
        $this->email->to($to_email);
        //    $this->email->cc($cc);
        $this->email->subject($subject);
        $this->email->message(html_entity_decode($mail_body));


        //    $this->email->cc($cc);

        $this->email->subject($subject);
        $this->email->message(html_entity_decode($mail_body));

        if ($this->email->send()) {
            return true;
        } else {
            return FALSE;
        }
    }

    public function getAppSetting() {
        $contition_array = array('status' => '1');
        $appSettingData = $this->common->select_data_by_condition('settings', $contition_array, '*', $sortby = 'setting_id', $orderby = 'ASC', $limit = '', $offset = '', $join_str = array(), $group_by = '');

        $appSetting = array();
        $appSetting['app_name'] = $appSettingData[0]['setting_val'];
        $appSetting['app_owner_name'] = $appSettingData[1]['setting_val'];
        $appSetting['app_email'] = $appSettingData[2]['setting_val'];
        $appSetting['app_sender_email'] = $appSettingData[3]['setting_val'];
        $appSetting['app_receiver_email'] = $appSettingData[4]['setting_val'];
        $appSetting['smtp_host_name'] = $appSettingData[6]['setting_val'];
        $appSetting['smtp_out_going_port'] = $appSettingData[7]['setting_val'];
        $appSetting['smtp_user_name'] = $appSettingData[8]['setting_val'];
        $appSetting['smtp_password'] = $appSettingData[9]['setting_val'];
        $appSetting['app_phone'] = $appSettingData[5]['setting_val'];


        return $appSetting;
    }

    public function product_variation(){
        $contition_array = array('status' => '1');
        $productData = $this->common->select_data_by_condition('product_bkp', $contition_array, 'id, sku, isbn_no, price, download_book,book_type,weight', $sortby = '', $orderby = 'ASC', $limit = '', $offset = '', $join_str = array(), $group_by = '');

        foreach($productData as $product){
            $insert_data = array();
            $insert_data['product_id'] = $product['id'];
            $book_type = '';
            if($product['book_type'] == 'paperback'){$book_type = 1;}
            elseif($product['book_type'] == 'hardcover'){$book_type = 2;}
            else{$book_type = 3;}
            $insert_data['attribute_id'] = $book_type;
            $insert_data['sku'] = $product['sku'];
            $insert_data['isbn_no'] = $product['isbn_no'];
            $insert_data['price'] = $product['price'];
            $insert_data['weight'] = $product['weight'];
            $insert_data['download_book'] = $product['download_book'];
            $insert_data['is_default'] = 1;

            $insertData = $this->common->insert_data_get_id($insert_data, 'product_variation');
        }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */