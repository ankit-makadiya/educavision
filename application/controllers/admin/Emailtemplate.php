<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Emailtemplate extends Admin_Controller {

    public $data;

    public function __construct() {

        parent::__construct();

        if (!$this->session->userdata('sl_admin')) {
            redirect('admin/login');
        }

        //$GLOBALS['record_per_page']=10;
        //site setting details
        $this->load->model('admin/common_model');

        $site_name_values = $this->common_model->select_data_by_id('settings', 'setting_id', '1', '*');

        $this->data['site_name'] = $site_name = $site_name_values[0]['setting_val'];
        //set header, footer and leftmenu
        $this->data['title'] = 'Email Template | ' . $site_name;

        //remove catch so after logout cannot view last visited page if that page is this
        $this->output->set_header('Last-Modified:' . gmdate('D, d M Y H:i:s') . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
        $this->output->set_header('Pragma: no-cache');
    }

    //display user list
    public function index() {

        $this->data['module_name'] = 'EmailTemplate';
        $this->data['section_title'] = 'Manage EmailTemplate';

        $contition_array = array('status' => 1);
        $this->data['emailtemplate_list'] = $this->common_model->select_data_by_condition('email_template', $contition_array, '*', $short_by = '', $order_by = 'ASC');

        /* Load Template */
        $this->template->admin_render('admin/emailtemplate/index', $this->data);
    }

    //search the user
    //add new user
    //update the user detail
    // update the location detail
    public function edit($id = '') {

        // check post and save data
        if ($this->input->post('btn_save')) {
            // form validation
            $id = $this->input->post('id');
            $title = $this->input->post('title');
            $subject = $this->input->post('subject');
            $template = $this->input->post('template');
            //$variables = $this->input->post('variables');
            $template_detail = $this->common_model->select_data_by_id('email_template', 'id', $id, 'title');

            $this->form_validation->set_rules('title', 'Template Title', 'trim|required|max_length[255]' . $is_unique . '');

            if ($this->form_validation->run() === FALSE) {
                $this->session->set_flashdata('error', validation_errors());
                redirect('admin/emailtemplate');
            } else {
                $update_array = array(
                    'title' => $title,
                    'subject' => $subject,
                    'template' => $template,
                //    'variables' => $variables,
                    'datetime' => date('Y-m-d H:i:s'),
                );

                $update_result = $this->common_model->update_data($update_array, 'email_template', 'id', $id);

                if ($update_result) {
                    $this->session->set_flashdata('success', 'Email Template updated successfully.');
                    redirect('admin/emailtemplate');
                } else {
                    $this->session->set_flashdata('error', 'Something went wrong! Try Again.');
                    redirect('admin/emailtemplate');
                }
            }
        }

        $email_template = $this->common_model->select_data_by_id('email_template', 'id', $id, '*');

        if (!empty($email_template)) {
            $this->data['module_name'] = 'Manage Email Template';
            $this->data['section_title'] = 'Edit Email Template';
            $this->data['email_template'] = $email_template;

            /* Load Template */
            $this->template->admin_render('admin/emailtemplate/edit', $this->data);
        } else {
            $this->session->set_flashdata('error', 'Something went wrong! Try Again.');
            redirect('admin/location');
        }
    }

}

?>