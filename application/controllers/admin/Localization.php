<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Localization extends Admin_Controller {

    public $data;

    public function __construct() {

        parent::__construct();

        if (!$this->session->userdata('sl_admin')) {
            redirect('admin/login');
        }

        //site setting details
        $this->load->model('admin/common_model');

        $site_name_values = $this->common_model->select_data_by_id('settings', 'setting_id', '1', '*');
        $this->data['site_name'] = $site_name = $site_name_values[0]['setting_val'];
        $this->data['title'] = 'Localization | ' . $site_name;

        //remove catch so after logout cannot view last visited page if that page is this
        $this->output->set_header('Last-Modified:' . gmdate('D, d M Y H:i:s') . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
        $this->output->set_header('Pragma: no-cache');
    }

    //display country list
    public function index() {

        $this->data['module_name'] = 'Country Management';
        $this->data['section_title'] = 'Country Management';

        $contition_array = array('is_deleted' => '0');
        $data = '*';
        $country_list = $this->common_model->select_data_by_condition('country', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

        $this->data['country_list'] = $country_list;
        //load country data
        $this->template->admin_render('admin/localization/country', $this->data);
    }

    //update the country detail
    public function countryEdit($id = '') {

        if ($this->input->post('country_id')) {

            $country_id = $this->input->post('country_id');
            if ($this->input->post('name') == '') {
                $this->session->set_flashdata('error', 'Country name is required');
                redirect('admin/country', 'refresh');
            } else {
                $update_array = array(
                    'name' => trim($this->input->post('name')),
                    'modify_date' => date('Y-m-d H:i:s'),
                    'status' => $this->input->post('status')
                );
                $update_result = $this->common_model->update_data($update_array, 'country', 'id', $this->input->post('country_id'));

                if ($update_result) {
                    $this->session->set_flashdata('success', 'Country successfully updated.');
                    redirect('admin/country', 'refresh');
                } else {
                    $this->session->set_flashdata('error', 'Error Occurred. Try Again!');
                    redirect('admin/country', 'refresh');
                }
            }
        }
        $country_detail = $this->common_model->select_data_by_id('country', 'id', $id, '*');

        if (!empty($country_detail)) {
            $this->data['module_name'] = 'Country';
            $this->data['section_title'] = 'Edit Country';
            $this->data['country_detail'] = $country_detail;
            $this->template->admin_render('admin/localization/country-edit', $this->data);
        } else {
            $this->session->set_flashdata('error', 'Errorout Occurred. Try Again.');
            redirect('admin/country', 'refresh');
        }
    }

    //display state list
    public function state() {

        $this->data['module_name'] = 'State Management';
        $this->data['section_title'] = 'State Management';

        $contition_array = array('is_deleted' => '0');
        $data = '*';
        $state_list = $this->common_model->select_data_by_condition('state', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

        foreach ($state_list as $key => $state) {
            $state_list[$key]['countryName'] = '';
            $country_name = $this->common_model->select_data_by_id('country', 'id', $state['country_id'], 'name');
            if (!empty($country_name)) {
                $state_list[$key]['countryName'] = $country_name[0]['name'];
            }
        }

        $this->data['state_list'] = $state_list;

        //load state data
        $this->template->admin_render('admin/localization/state', $this->data);
    }

    //update the state detail
    public function stateEdit($id = '') {

        if ($this->input->post('state_id')) {

            $state_id = $this->input->post('state_id');
            if ($this->input->post('name') == '') {
                $this->session->set_flashdata('error', 'State name is required');
                redirect('admin/state', 'refresh');
            } else {
                $update_array = array(
                    'name' => trim($this->input->post('name')),
                    'country_id' => $this->input->post('country_id'),
                    'modify_date' => date('Y-m-d H:i:s'),
                    'status' => $this->input->post('status')
                );
                $update_result = $this->common_model->update_data($update_array, 'state', 'id', $this->input->post('state_id'));

                if ($update_result) {
                    $this->session->set_flashdata('success', 'State successfully updated.');
                    redirect('admin/state', 'refresh');
                } else {
                    $this->session->set_flashdata('error', 'Error Occurred. Try Again!');
                    redirect('admin/state', 'refresh');
                }
            }
        }
        $state_detail = $this->common_model->select_data_by_id('state', 'id', $id, '*');

        if (!empty($state_detail)) {
            $this->data['module_name'] = 'State';
            $this->data['section_title'] = 'Edit State';
            $this->data['state_detail'] = $state_detail;

            $contition_array = array('is_deleted' => '0', 'status' => 1);
            $data = 'id, name';
            $country_list = $this->common_model->select_data_by_condition('country', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
            $this->data['country_list'] = $country_list;

            $this->template->admin_render('admin/localization/state-edit', $this->data);
        } else {
            $this->session->set_flashdata('error', 'Errorout Occurred. Try Again.');
            redirect('admin/state', 'refresh');
        }
    }

    //display city list
    public function city($country_id = '', $state_id = '') {
        $this->data['module_name'] = 'City Management';
        $this->data['section_title'] = 'City Management';

        $this->data['city_list'] = array();
        if ($country_id != '' && $state_id != '') {
            $contition_array = array('is_deleted' => '0', 'country_id' => $country_id, 'state_id' => $state_id);
            $data = '*';
            $city_list = $this->common_model->select_data_by_condition('city', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

            foreach ($city_list as $key => $city) {
                $city_list[$key]['countryName'] = '';
                $city_list[$key]['stateName'] = '';
                $country_name = $this->common_model->select_data_by_id('country', 'id', $city['country_id'], 'name');
                $state_name = $this->common_model->select_data_by_id('state', 'id', $city['state_id'], 'name');
                if (!empty($country_name)) {
                    $city_list[$key]['countryName'] = $country_name[0]['name'];
                    $city_list[$key]['stateName'] = $state_name[0]['name'];
                }
            }

            $this->data['city_list'] = $city_list;
        }
        //load city data

        $contition_array = array('is_deleted' => '0', 'status' => '1');
        $data = '*';
        $country_list = $this->common_model->select_data_by_condition('country', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

        $state_list = array();
        if($country_id != '') {
            $contition_array = array('is_deleted' => '0', 'status' => '1', 'country_id' => $country_id);
            $data = '*';
            $state_list = $this->common_model->select_data_by_condition('state', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
        }
        $this->data['country_list'] = $country_list;
        $this->data['state_list'] = $state_list;
        $this->data['country_id'] = $country_id;
        $this->data['state_id'] = $state_id;
        $this->template->admin_render('admin/localization/city', $this->data);
    }

    //update the city detail
    public function cityEdit($id = '') {

        if ($this->input->post('city_id')) {

            $city_id = $this->input->post('city_id');
            $state_id = $this->input->post('state_id');
            $country_id = $this->input->post('country_id');
            if ($this->input->post('name') == '') {
                $this->session->set_flashdata('error', 'State name is required');
                redirect('admin/city', 'refresh');
            } else {
                $update_array = array(
                    'country_id' => trim($this->input->post('country_id')),
                    'state_id' => trim($this->input->post('state_id')),
                    'name' => trim($this->input->post('name')),
                    'modify_date' => date('Y-m-d H:i:s'),
                    'status' => $this->input->post('status')
                );
                $update_result = $this->common_model->update_data($update_array, 'city', 'id', $this->input->post('city_id'));

                if ($update_result) {
                    $this->session->set_flashdata('success', 'City successfully updated.');
                    redirect('admin/city', 'refresh');
                } else {
                    $this->session->set_flashdata('error', 'Error Occurred. Try Again!');
                    redirect('admin/city', 'refresh');
                }
            }
        }
        $city_detail = $this->common_model->select_data_by_id('city', 'id', $id, '*');

        if (!empty($city_detail)) {
            $this->data['module_name'] = 'City';
            $this->data['section_title'] = 'Edit City';
            $this->data['city_detail'] = $city_detail;

            $contition_array = array('is_deleted' => '0', 'status' => 1);
            $data = 'id, name';
            $country_list = $this->common_model->select_data_by_condition('country', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
            $this->data['country_list'] = $country_list;


            $this->template->admin_render('admin/localization/city-edit', $this->data);
        } else {
            $this->session->set_flashdata('error', 'Errorout Occurred. Try Again.');
            redirect('admin/city', 'refresh');
        }
    }

    public function getStateList() {

        if ($this->input->post('country_id')) {
            $country_id = $this->input->post('country_id');
            $state_id = $this->input->post('state_id');

            $contition_array = array('is_deleted' => '0', 'status' => 1, 'country_id' => $country_id);
            $data = 'id, name';
            $state_list = $this->common_model->select_data_by_condition('state', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
            $return_html = '';
            if (!empty($state_list)) {
                foreach ($state_list as $state) {
                    if (!empty($state_id)) {
                        $selected = '';
                        if ($state['id'] == $state_id) {
                            $selected .= 'selected="selected"';
                        }
                    }
                    $return_html .= '<option value="' . $state['id'] . '" ' . $selected . '>' . $state['name'] . '</option>';
                }
            }
            echo $return_html;
            exit;
        }
    }

}

?>