<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Couponcode extends Admin_Controller {

    public $data;

    public function __construct() {

        parent::__construct();

        if (!$this->session->userdata('sl_admin')) {
            redirect('admin/login');
        }
        $this->load->model('admin/common_model');

        $site_name_values = $this->common_model->select_data_by_id('settings', 'setting_id', '1', '*');
        $this->data['site_name'] = $site_name = $site_name_values[0]['setting_val'];
        $this->data['title'] = 'Coupon Code | ' . $site_name;

        //remove catch so after logout cannot view last visited page if that page is this
        $this->output->set_header('Last-Modified:' . gmdate('D, d M Y H:i:s') . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
        $this->output->set_header('Pragma: no-cache');
    }

    //display shipping list
    public function index() {

        $this->data['module_name'] = 'Module Management';
        $this->data['section_title'] = 'Coupon Code';

        $contition_array = array('coupon.is_deleted' => 0);
        $data = 'coupon.*,coupon.name as cname';
        $join_str[0]['table'] = 'coupon_pricing';
        $join_str[0]['join_table_id'] = 'coupon_pricing.coupon_id';
        $join_str[0]['from_table_id'] = 'coupon.id';
        $join_str[0]['join_type'] = 'left';

        $shipping_list = $this->common_model->select_data_by_condition('coupon', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '', $join_str = array(), $group_by = '');

        $this->data['coupon_list'] = $shipping_list;
        //load shipping data
        $this->template->admin_render('admin/couponcode/index', $this->data);
    }

    //add the shipping detail
    public function add() {
        if ($this->input->post()) {
			if ($this->input->post('coupon_name') == '') {
                $this->session->set_flashdata('error', 'Coupon name is required');
                redirect('admin/couponcode', 'refresh');
            }
            if ($this->input->post('start_date') == '') {
                $this->session->set_flashdata('error', 'Start date is required');
                redirect('admin/couponcode', 'refresh');
            }
            if ($this->input->post('end_date') == '') {
                $this->session->set_flashdata('error', 'End date is required');
                redirect('admin/couponcode', 'refresh');
            }

            $restriction_setting = array();
			//$restriction_setting['allow_per_email'] = $this->input->post('allow_per_email');
			//$restriction_setting['no_of_usage'] = $this->input->post('no_of_usage');

            $restriction_setting['allow_per_email'] = '';
            $restriction_setting['no_of_usage'] = 1;

            $insert_data = array();
            $insert_data['name'] = $this->input->post('coupon_name');
            $insert_data['coupon_code'] = $this->input->post('coupon_code');
            $insert_data['start_date'] = date('Y-m-d H:i:s', strtotime($this->input->post('start_date')));
            $insert_data['end_date'] = date('Y-m-d H:i:s', strtotime($this->input->post('end_date')));
			$insert_data['restriction_setting'] = json_encode($restriction_setting);
            $insert_data['created_date'] = date('Y-m-d H:i:s');
            $insert_data['updated_date'] = date('Y-m-d H:i:s');
            $insert_data['status'] = 1;

            $insertData = $this->common_model->insert_data_get_id($insert_data, 'coupon');

			$insert_data = array();
			$insert_data['coupon_id'] = $insertData;
			$insert_data['item_type'] = 'Discount';
			$insert_data['offer_type'] = ($this->input->post('discount_offer_type') == 1) ? '% Off' : '$ Off';
			$insert_data['offer_vale'] = $this->input->post('discount_offer_value');
			$insert_data['is_free_shipping'] = 0;

			$insertDiscountPriceData = $this->common_model->insert_data_get_id($insert_data, 'coupon_pricing');

			$insert_data = array();
			$insert_data['coupon_id'] = $insertData;
			$insert_data['item_type'] = 'Shipping';
			$insert_data['offer_type'] = ($this->input->post('shipping_offer_type') == 1) ? '% Off' : '$ Off';
			$insert_data['offer_vale'] = $this->input->post('shipping_offer_value');
			$insert_data['is_free_shipping'] = ($this->input->post('free_shipping') == 1) ? '1' : '0';

			if($this->input->post('free_shipping') == 1){
				$insert_data['offer_type'] = '% Off';
				$insert_data['offer_vale'] = '100';
			}

			$insertShippingPriceData = $this->common_model->insert_data_get_id($insert_data, 'coupon_pricing');


			$insert_data = array();
			$insert_data['coupon_id'] = $insertData;
			$insert_data['item_type'] = 'Tax';
			$insert_data['offer_type'] = ($this->input->post('tax_offer_type') == 1) ? '% Off' : '$ Off';
			$insert_data['offer_vale'] = $this->input->post('tax_offer_value');
			$insert_data['is_free_shipping'] = 0;

			$insertTaxPriceData = $this->common_model->insert_data_get_id($insert_data, 'coupon_pricing');


			if ($insertData) {
                $this->session->set_flashdata('success', 'Coupon Code has been sucessfully inserted.');
                redirect('admin/couponcode', 'refresh');
                die();
            } else {
                $this->session->set_flashdata('error', 'oops! something is wrong.');
                redirect('admin/couponcode/add', 'refresh');
                die();
            }
        }
        $this->data['module_name'] = 'Coupon Code';
        $this->data['section_title'] = 'Add Coupon Code';

        $contition_array = array('is_deleted' => '0');
        $data = 'id,name';
        $country_list = $this->common_model->select_data_by_condition('country', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
        $this->data['country_list'] = $country_list;
        //load country data
        $this->template->admin_render('admin/couponcode/add', $this->data);
    }

    //update the shipping detail
    public function edit($id = '') {
        if ($this->input->post('shipping_id')) {
            $shipping_id = $this->input->post('shipping_id');
            if ($this->input->post('country_id') == '') {
                $this->session->set_flashdata('error', 'Country id is required');
                redirect('admin/couponcode', 'refresh');
            } elseif ($this->input->post('price_from') == '') {
                $this->session->set_flashdata('error', 'Price from is required');
                redirect('admin/couponcode', 'refresh');
            } elseif ($this->input->post('price_to') == '') {
                $this->session->set_flashdata('error', 'Price to is required');
                redirect('admin/couponcode', 'refresh');
            } elseif ($this->input->post('amount') == '') {
                $this->session->set_flashdata('error', 'Coupon Code Amount is required');
                redirect('admin/couponcode', 'refresh');
            } else {
                $update_array = array();
                $update_array['country_id'] = $this->input->post('country_id');
                $update_array['price_from'] = $this->input->post('price_from');
                $update_array['price_to'] = $this->input->post('price_to');
                $update_array['amount'] = $this->input->post('amount');
                $update_array['modify_date'] = date('Y-m-d H:i:s');
                $update_array['status'] = $this->input->post('status');

                $update_result = $this->common_model->update_data($update_array, 'shipping', 'id', $this->input->post('shipping_id'));

                if ($update_result) {
                    $this->session->set_flashdata('success', 'Coupon Code successfully updated.');
                    redirect('admin/couponcode', 'refresh');
                } else {
                    $this->session->set_flashdata('error', 'Error Occurred. Try Again!');
                    redirect('admin/couponcode/edit/' . $this->input->post('shipping_id'), 'refresh');
                }
            }
        }
        $shipping_detail = $this->common_model->select_data_by_id('shipping', 'id', $id, '*');
        if (!empty($shipping_detail)) {
            $this->data['module_name'] = 'Coupon Code';
            $this->data['section_title'] = 'Edit Coupon Code';
            $this->data['shipping_detail'] = $shipping_detail;
            //load shipping details
            $contition_array = array('is_deleted' => '0');
            $data = 'id,name';
            $country_list = $this->common_model->select_data_by_condition('country', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
            $this->data['country_list'] = $country_list;
            //load country data
            $this->template->admin_render('admin/Coupon Code/edit', $this->data);
        } else {
            $this->session->set_flashdata('error', 'Errorout Occurred. Try Again.');
            redirect('admin/couponcode', 'refresh');
        }
    }

    public function view($id = '') {
        $this->data['module_name'] = 'Coupon Code';
        $this->data['section_title'] = 'View Coupon Code';

        $contition_array = array('is_deleted' => '0','id' => $id);
        $data = '*';
        $this->data['coupon_data'] = $coupon_data = $this->common_model->select_data_by_condition('coupon', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

        $this->data['coupon_pricing_data'] = $coupon_pricing_data = array();
        if(!empty($coupon_data)){
            $contition_array = array('coupon_id' => $id);
            $data = '*';
            $this->data['coupon_pricing_data'] = $coupon_pricing_data = $this->common_model->select_data_by_condition('coupon_pricing', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
        }

        //load country data
        $this->template->admin_render('admin/couponcode/view', $this->data);
    }

    // delete page
    public function delete($id = '') {
        $update_data = array('is_deleted' => 1);
        $update_result = $this->common_model->update_data($update_data, 'coupon', 'id', $id);

        if ($update_result) {
            $this->session->set_flashdata('success', 'Coupon Code deleted successfully.');
            redirect('admin/couponcode');
        } else {
            $this->session->set_flashdata('error', 'Something went wrong! Try Again.');
            redirect('admin/couponcode');
        }
    }

    public function checkuniqueshipping($countryId = "", $pricefrom = "", $priceto = "") {
        $shippingId = 0;
        $contition_array = array('is_deleted' => '0', 'country_id' => $countryId);
        $data = 'id';
//        $this->db->group_start();
//        $this->db->where('(price_from >= ' . $pricefrom . ' AND price_from <=' . $priceto . ') OR (price_to >= ' . $pricefrom . ' AND price_to <=' . $priceto . ')');
//        $this->db->group_end();

        $this->db->where('price_from >= ', $pricefrom);
        $this->db->where('price_to <= ', $priceto);
        $check_shipping = $this->common_model->select_data_by_condition('shipping', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

        if (!empty($check_shipping) && count($check_shipping) > 0) {
            $shippingId = $check_shipping[0]['id'];
        }
        return $shippingId;
    }

}

?>
