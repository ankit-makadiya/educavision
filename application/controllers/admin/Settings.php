<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Settings extends Admin_Controller {

    public $data;

    public function __construct() {

        parent::__construct();

        if (!$this->session->userdata('sl_admin')) {
            redirect('admin/login');
        }

        //$GLOBALS['record_per_page']=10;
        //site setting details
        $this->load->model('admin/common_model');
        $site_name_values = $this->common_model->select_data_by_id('settings', 'setting_id', '1', '*');

        $this->data['site_name'] = $site_name = $site_name_values[0]['setting_val'];
        //set header, footer and leftmenu
        $this->data['title'] = 'Settings | ' . $site_name;

        //remove catch so after logout cannot view last visited page if that page is this
        $this->output->set_header('Last-Modified:' . gmdate('D, d M Y H:i:s') . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
        $this->output->set_header('Pragma: no-cache');
    }

    //display users list
    public function index() {

        $this->data['module_name'] = 'Settings';
        $this->data['section_title'] = 'Settings';

        $this->data['setting_list'] = $this->common_model->select_data_by_condition('settings', $contition_array = array('status' => 1), $data = '*', $short_by = 'setting_id', $order_by = 'ASC', $limit = '', $offset = '', $join_str = array());

        $this->data['pageData'] = $this->common_model->select_data_by_condition('pages', $contition_array = array('status' => 1, 'is_deleted' => 0), $data = 'id,slug,name', $short_by = '', $order_by = '', $limit = '', $offset = '', $join_str = array());

        /* Load Template */
        $this->template->admin_render('admin/settings/index', $this->data);
    }

    //update the user detail
    public function edit() {
        $i = 1;
        foreach ($_POST as $key => $value) {
            $settings_array = array(
                'setting_val' => $value,
            );
            $update_settings = $this->common_model->update_data($settings_array, 'settings', 'setting_key', $key);
            $i++;
        }
        if ($update_settings) {
            $this->session->set_flashdata('success', 'Settings successfully updated.');
            redirect('admin/settings');
        } else {
            $this->session->set_flashdata('error', 'Something went wrong! Please try Again.');
            redirect('admin/settings');
        }
    }

}

?>
