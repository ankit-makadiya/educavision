<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Menu extends Admin_Controller {

    public $data;

    public function __construct() {

        parent::__construct();

        if (!$this->session->userdata('sl_admin')) {
            redirect('admin/login');
        }

        //site setting details
        $this->load->model('admin/common_model');

        $site_name_values = $this->common_model->select_data_by_id('settings', 'setting_id', '1', '*');
        $this->data['site_name'] = $site_name = $site_name_values[0]['setting_val'];
        $this->data['title'] = 'Menu | ' . $site_name;

        //remove catch so after logout cannot view last visited page if that page is this
        $this->output->set_header('Last-Modified:' . gmdate('D, d M Y H:i:s') . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
        $this->output->set_header('Pragma: no-cache');
    }

    public function index() {

        if ($this->input->post('menu_id')) {

            $menu_id = $this->input->post('menu_id');
            if ($this->input->post('name') == '') {
                $this->session->set_flashdata('error', 'Menu name is required');
                redirect('admin/menu', 'refresh');
            } else {
                $update_array = array(
                    'menu_item' => trim($this->input->post('menuitem')),
                    'modify_date' => date('Y-m-d H:i:s'),
                    'status' => 1
                );
                $update_result = $this->common_model->update_data($update_array, 'menu', 'id', $this->input->post('menu_id'));

                if ($update_result) {
                    $this->session->set_flashdata('success', 'Menu successfully updated.');
                    redirect('admin/menu', 'refresh');
                } else {
                    $this->session->set_flashdata('error', 'Error Occurred. Try Again!');
                    redirect('admin/menu', 'refresh');
                }
            }
        }
        $menu_detail = $this->common_model->select_data_by_id('menu', 'id', 1, '*');

        if (!empty($menu_detail)) {
            $this->data['module_name'] = 'Menu';
            $this->data['section_title'] = 'Menu Management';
            $this->data['menu_detail'] = $menu_detail;

            $contition_array = array('is_deleted' => '0');
            $data = '*';
            $this->data['menu_list'] = $this->common_model->select_data_by_condition('menu', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

            $contition_array = array('is_deleted' => '0', 'status' => '1');
            $data = 'id,name';
            $this->data['page_data'] = $this->common_model->select_data_by_condition('pages', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

            $contition_array = array('is_deleted' => '0', 'status' => '1');
            $data = 'id,icon';
            $this->data['fa_data'] = $this->common_model->select_data_by_condition('fontawesome_icons', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

            $contition_array = array('status' => 1, 'is_deleted' => 0, 'parent_id' => 0);
            $data = 'name,id,parent_id';
            $category_data = $this->common_model->select_data_by_condition('category', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

            $category_data_html = '';
            if (!empty($category_data)) {
                foreach ($category_data as $category) {
                    $category_data_html .= '<div class="checker"><label><span><input type="checkbox"  name="category" value="category*' . $category['id'] . '*' . $category['name'] . '" class="parent_category_' . $category['id'] . '" id="parent_category_' . $category['id'] . '" onclick="getChildCategory(' . $category['id'] . ')"></span> ' . $category['name'] . ' </label></div>';
                    $category_data_html .= $this->getRecursiveCategoryMenuItemData($category['id']);
                }
            }
            $this->data['category_html'] = $category_data_html;

            $this->template->admin_render('admin/menu/index', $this->data);
        } else {
            $this->session->set_flashdata('error', 'Errorout Occurred. Try Again.');
            redirect('admin/menu', 'refresh');
        }
    }

    // get name by id

    function getCatName() {
        if ($this->input->post('menu_id')) {
            $menu_id = $this->input->post('menu_id');
            $contition_array = array('id' => $menu_id);
            $data = 'name';
            $menu_name = $this->common_model->select_data_by_condition('menu', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

            $return_data = array();
            $return_data['menu_name'] = $menu_name[0]['name'];
            header('Content-Type: application/json');
            echo json_encode($return_data);
            exit;
        }
    }

    public function getMenuItem() {
        $menu_id = $this->input->post('menu_id');
        $category_data = array();
        $page_data = array();
        $return_html = '';
        if (!empty($menu_id)) {
            $contition_array = array('id' => $menu_id);
            $data = '*';
            $menu_data = $this->common_model->select_data_by_condition('menu', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

            if (!empty($menu_data)) {
                $menu_data = $menu_data[0];
                $menuitem = $menu_data['menu_item'];
                $menuitem = json_decode($menuitem, true);

                foreach ($menuitem as $menu) {
                    if (!empty($menu['menu_description'])) {
                        $menu['menu_description'] = htmlentities($menu['menu_description']);
                    } else {
                        $menu['menu_description'] = '';
                    }
                    if (!empty($menu['special_description'])) {
                        $menu['special_description'] = htmlentities($menu['special_description']);
                    } else {
                        $menu['special_description'] = '';
                    }
                    if (!empty($menu['menu_image'])) {
                        $menu['menu_image'] = $menu['menu_image'];
                    } else {
                        $menu['menu_image'] = '';
                    }
                    if (!empty($menu['menu_type'])) {
                        $menu['menu_type'] = $menu['menu_type'];
                    } else {
                        $menu['menu_type'] = '';
                    }
                    if (!empty($menu['menu_sub_type'])) {
                        $menu['menu_sub_type'] = $menu['menu_sub_type'];
                    } else {
                        $menu['menu_sub_type'] = '';
                    }
                    if (!empty($menu['special_main_title'])) {
                        $menu['special_main_title'] = $menu['special_main_title'];
                    } else {
                        $menu['special_main_title'] = '';
                    }
                    if (!empty($menu['special_sub_title'])) {
                        $menu['special_sub_title'] = $menu['special_sub_title'];
                    } else {
                        $menu['special_sub_title'] = '';
                    }
                    if (!empty($menu['special_description'])) {
                        $menu['special_description'] = $menu['special_description'];
                    } else {
                        $menu['special_description'] = '';
                    }
                    if (!empty($menu['special_image'])) {
                        $menu['special_image'] = $menu['special_image'];
                    } else {
                        $menu['special_image'] = '';
                    }
                    if (!empty($menu['menu_icon'])) {
                        $menu['menu_icon'] = $menu['menu_icon'];
                    } else {
                        $menu['menu_icon'] = '';
                    }
                    if (!empty($menu['menu_icon_position'])) {
                        $menu['menu_icon_position'] = $menu['menu_icon_position'];
                    } else {
                        $menu['menu_icon_position'] = 'before';
                    }
                    if (!empty($menu['special_position'])) {
                        $menu['special_position'] = $menu['special_position'];
                    } else {
                        $menu['special_position'] = '';
                    }
                    if (!empty($menu['menu_target'])) {
                        $menu['menu_target'] = $menu['menu_target'];
                    } else {
                        $menu['menu_target'] = '';
                    }
                    $lisetd_children = array();
                    if (!empty($menu['children'])) {
                        $lisetd_children = $menu['children'];
                    }
                    if ($menu['table'] == 'cms_page') {
                        $contition_array = array('status' => 1, 'is_deleted' => 0, 'id' => $menu['table_id']);
                        $data = 'name';
                        $page = $this->common_model->select_data_by_condition('pages', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

                        if (!empty($page)) {
                            $return_html .= '<li class="dd-item" data-id="' . $menu['id'] . '" data-table="' . $menu['table'] . '" data-table_id="' . $menu['table_id'] . '" data-original_name="' . $page[0]['name'] . '" data-navigation_label="' . $menu['navigation_label'] . '" data-menu_image="' . $menu['menu_image'] . '" data-menu_icon="' . $menu['menu_icon'] . '" data-menu_icon_position="' . $menu['menu_icon_position'] . '"  data-menu_type="' . $menu['menu_type'] . '" data-menu_sub_type="' . $menu['menu_sub_type'] . '" data-menu_description="' . $menu['menu_description'] . '" data-special_main_title="' . $menu['special_main_title'] . '" data-special_sub_title="' . $menu['special_sub_title'] . '" data-special_description="' . $menu['special_description'] . '"  data-special_image="' . $menu['special_image'] . '"
                            data-special_position="' . $menu['special_position'] . '" >';
                            if (count($lisetd_children) > 0) {
                                $return_html .= '<button data-action="collapse" type="button">Collapse</button><button data-action="expand" type="button" style="display: none;">Expand</button>';
                            }
                            $return_html .= '<div class="dd-handle"><span class="menu_name"><b>' . $menu['navigation_label'] . '</b></span><span class="pull-right">Page<span class="li_edit"> | <a href="javascript:void(0);" onclick="editMenu(' . $menu['id'] . ')"><i class="fa fa-pencil"></i> </a><a href="javascript:void(0);" onclick="itemDelete(' . $menu['id'] . ');"><i class="fa fa-trash"></i></a></span></span></div>';
                        }
                    } elseif ($menu['table'] == 'category') {
                        $contition_array = array('status' => 1, 'is_deleted' => 0, 'id' => $menu['table_id']);
                        $data = 'name';
                        $category = $this->common_model->select_data_by_condition('category', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

                        if (!empty($category)) {
                            $return_html .= '<li class="dd-item" data-id="' . $menu['id'] . '" data-table="' . $menu['table'] . '" data-table_id="' . $menu['table_id'] . '" data-original_name="' . $category[0]['name'] . '" data-navigation_label="' . $menu['navigation_label'] . '" data-menu_image="' . $menu['menu_image'] . '" data-menu_icon="' . $menu['menu_icon'] . '" data-menu_icon_position="' . $menu['menu_icon_position'] . '"  data-menu_type="' . $menu['menu_type'] . '" data-menu_sub_type="' . $menu['menu_sub_type'] . '" data-menu_description="' . $menu['menu_description'] . '" data-special_main_title="' . $menu['special_main_title'] . '" data-special_sub_title="' . $menu['special_sub_title'] . '" data-special_description="' . $menu['special_description'] . '"  data-special_image="' . $menu['special_image'] . '"
                            data-special_position="' . $menu['special_position'] . '">';
                            if (count($lisetd_children) > 0) {
                                $return_html .= '<button data-action="collapse" type="button">Collapse</button><button data-action="expand" type="button" style="display: none;">Expand</button>';
                            }
                            $return_html .= '<div class="dd-handle"><span class="menu_name"><b>' . $menu['navigation_label'] . '</b></span><span class="pull-right">Category<span class="li_edit"> | <a href="javascript:void(0);" onclick="editMenu(' . $menu['id'] . ')"><i class="fa fa-pencil"></i> </a><a href="javascript:void(0);" onclick="itemDelete(' . $menu['id'] . ');"><i class="fa fa-trash"></i></a></span></span></div>';
                        }
                    } elseif ($menu['table'] == 'custom_link') {
                        $return_html .= '<li class="dd-item" data-id="' . $menu['id'] . '" data-table="custom_link" data-menu_name="' . $menu['menu_name'] . '" data-menu_url="' . $menu['menu_url'] . '" data-menu_image="' . $menu['menu_image'] . '" data-menu_icon="' . $menu['menu_icon'] . '" data-menu_icon_position="' . $menu['menu_icon_position'] . '"  data-menu_type="' . $menu['menu_type'] . '" data-menu_sub_type="' . $menu['menu_sub_type'] . '" data-menu_description="' . $menu['menu_description'] . '" data-special_main_title="' . $menu['special_main_title'] . '" data-special_sub_title="' . $menu['special_sub_title'] . '" data-special_description="' . $menu['special_description'] . '"  data-special_image="' . $menu['special_image'] . '"  data-special_position="' . $menu['special_position'] . '"  data-menu_target="' . $menu['menu_target'] . '">';
                        if (count($lisetd_children) > 0) {
                            $return_html .= '<button data-action="collapse" type="button">Collapse</button><button data-action="expand" type="button" style="display: none;">Expand</button>';
                        }
                        $return_html .= '<div class="dd-handle"><span class="menu_name"><b>' . $menu['menu_name'] . '</b></span><span class="pull-right">Custom Link<span class="li_edit"> | <a href="javascript:void(0);"  target="_self" onclick="editCustomMenu(' . $menu['id'] . ')"><i class="fa fa-pencil"></i> </a><a href="javascript:void(0);" target="_self" onclick="itemDelete(' . $menu['id'] . ');"><i class="fa fa-trash"></i></a></span></span></div>';
                    }
                    if (count($lisetd_children) > 0) {
                        $return_html .= $this->getRecursiveMenuItem($lisetd_children);
                    }
                    $return_html .= '</li>';
                }
            }
        }
        echo $return_html;
        exit;
    }

    public function getRecursiveMenuItem($menuitem = array(), $return_html = '') {
        $return_html .= '<ol class="dd-list">';
        foreach ($menuitem as $menu) {

            if (!empty($menu['menu_description'])) {
                $menu['menu_description'] = htmlentities($menu['menu_description']);
            } else {
                $menu['menu_description'] = '';
            }
            if (!empty($menu['special_description'])) {
                $menu['special_description'] = htmlentities($menu['special_description']);
            } else {
                $menu['special_description'] = '';
            }
            if (!empty($menu['menu_image'])) {
                $menu['menu_image'] = $menu['menu_image'];
            } else {
                $menu['menu_image'] = '';
            }
            if (!empty($menu['menu_type'])) {
                $menu['menu_type'] = $menu['menu_type'];
            } else {
                $menu['menu_type'] = '';
            }
            if (!empty($menu['menu_sub_type'])) {
                $menu['menu_sub_type'] = $menu['menu_sub_type'];
            } else {
                $menu['menu_sub_type'] = '';
            }
            if (!empty($menu['special_main_title'])) {
                $menu['special_main_title'] = $menu['special_main_title'];
            } else {
                $menu['special_main_title'] = '';
            }
            if (!empty($menu['special_sub_title'])) {
                $menu['special_sub_title'] = $menu['special_sub_title'];
            } else {
                $menu['special_sub_title'] = '';
            }
            if (!empty($menu['special_description'])) {
                $menu['special_description'] = $menu['special_description'];
            } else {
                $menu['special_description'] = '';
            }
            if (!empty($menu['special_image'])) {
                $menu['special_image'] = $menu['special_image'];
            } else {
                $menu['special_image'] = '';
            }
            if (!empty($menu['menu_icon'])) {
                $menu['menu_icon'] = $menu['menu_icon'];
            } else {
                $menu['menu_icon'] = '';
            }
            if (!empty($menu['menu_icon_position'])) {
                $menu['menu_icon_position'] = $menu['menu_icon_position'];
            } else {
                $menu['menu_icon_position'] = 'before';
            }
            if (!empty($menu['special_position'])) {
                $menu['special_position'] = $menu['special_position'];
            } else {
                $menu['special_position'] = '';
            }
            if (!empty($menu['menu_target'])) {
                $menu['menu_target'] = $menu['menu_target'];
            } else {
                $menu['menu_target'] = '';
            }
            $lisetd_children = array();
            if (!empty($menu['children'])) {
                $lisetd_children = $menu['children'];
            }
            if ($menu['table'] == 'cms_page') {
                $contition_array = array('status' => 1, 'is_deleted' => 0, 'id' => $menu['table_id']);
                $data = 'name';
                $page = $this->common_model->select_data_by_condition('pages', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

                if (!empty($page)) {
                    $return_html .= '<li class="dd-item" data-id="' . $menu['id'] . '" data-table="' . $menu['table'] . '" data-table_id="' . $menu['table_id'] . '" data-original_name="' . $page[0]['name'] . '" data-navigation_label="' . $menu['navigation_label'] . '" data-menu_image="' . $menu['menu_image'] . '" data-menu_icon="' . $menu['menu_icon'] . '" data-menu_icon_position="' . $menu['menu_icon_position'] . '" data-menu_type="' . $menu['menu_type'] . '" data-menu_sub_type="' . $menu['menu_sub_type'] . '" data-menu_description="' . $menu['menu_description'] . '" data-special_main_title="' . $menu['special_main_title'] . '" data-special_sub_title="' . $menu['special_sub_title'] . '" data-special_description="' . $menu['special_description'] . '"  data-special_image="' . $menu['special_image'] . '" data-special_position="' . $menu['special_position'] . '" >';
                    if (count($lisetd_children) > 0) {
                        $return_html .= '<button data-action="collapse" type="button">Collapse</button><button data-action="expand" type="button" style="display: none;">Expand</button>';
                    }
                    $return_html .= '<div class="dd-handle"><span class="menu_name"><b>' . $menu['navigation_label'] . '</b></span><span class="pull-right">Page<span class="li_edit"> | <a href="javascript:void(0);" onclick="editMenu(' . $menu['id'] . ')"><i class="fa fa-pencil"></i> </a><a href="javascript:void(0);" onclick="itemDelete(' . $menu['id'] . ');"><i class="fa fa-trash"></i></a></span></span></div>';
                }
            } elseif ($menu['table'] == 'category') {
                $contition_array = array('status' => 1, 'is_deleted' => 0, 'id' => $menu['table_id']);
                $data = 'name,id,parent_id';
                $category = $this->common_model->select_data_by_condition('category', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
                if (!empty($category)) {
                    if ($category[0]['parent_id'] != 0) {
                        $contition_array = array('status' => 1, 'is_deleted' => 0, 'id' => $category[0]['parent_id']);
                        $data = 'id';
                        $parent_category = $this->common_model->select_data_by_condition('category', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
                    }
                    if ($category[0]['parent_id'] == 0 && !empty($category)) {
                        $return_html .= '<li class="dd-item" data-id="' . $menu['id'] . '" data-table="' . $menu['table'] . '" data-table_id="' . $menu['table_id'] . '" data-original_name="' . $category[0]['name'] . '" data-navigation_label="' . $menu['navigation_label'] . '" data-menu_image="' . $menu['menu_image'] . '"  data-menu_icon="' . $menu['menu_icon'] . '" data-menu_icon_position="' . $menu['menu_icon_position'] . '" data-menu_type="' . $menu['menu_type'] . '" data-menu_sub_type="' . $menu['menu_sub_type'] . '" data-menu_description="' . $menu['menu_description'] . '" data-special_main_title="' . $menu['special_main_title'] . '" data-special_sub_title="' . $menu['special_sub_title'] . '" data-special_description="' . $menu['special_description'] . '"  data-special_image="' . $menu['special_image'] . '"  data-special_position="' . $menu['special_position'] . '" >';
                        if (count($lisetd_children) > 0) {
                            $return_html .= '<button data-action="collapse" type="button">Collapse</button><button data-action="expand" type="button" style="display: none;">Expand</button>';
                        }
                        $return_html .= '<div class="dd-handle"><span class="menu_name"><b>' . $menu['navigation_label'] . '</b></span><span class="pull-right">Category<span class="li_edit"> | <a href="javascript:void(0);" onclick="editMenu(' . $menu['id'] . ')"><i class="fa fa-pencil"></i> </a><a href="javascript:void(0);" onclick="itemDelete(' . $menu['id'] . ');"><i class="fa fa-trash"></i></a></span></span></div>';
                    }
                    if ($category[0]['parent_id'] != 0 && !empty($category) && !empty($parent_category)) {
                        $return_html .= '<li class="dd-item" data-id="' . $menu['id'] . '" data-table="' . $menu['table'] . '" data-table_id="' . $menu['table_id'] . '" data-original_name="' . $category[0]['name'] . '" data-navigation_label="' . $menu['navigation_label'] . '" data-menu_image="' . $menu['menu_image'] . '"  data-menu_icon="' . $menu['menu_icon'] . '" data-menu_icon_position="' . $menu['menu_icon_position'] . '" data-menu_type="' . $menu['menu_type'] . '" data-menu_sub_type="' . $menu['menu_sub_type'] . '" data-menu_description="' . $menu['menu_description'] . '" data-special_main_title="' . $menu['special_main_title'] . '" data-special_sub_title="' . $menu['special_sub_title'] . '" data-special_description="' . $menu['special_description'] . '"  data-special_image="' . $menu['special_image'] . '"  data-special_position="' . $menu['special_position'] . '">';
                        if (count($lisetd_children) > 0) {
                            $return_html .= '<button data-action="collapse" type="button">Collapse</button><button data-action="expand" type="button" style="display: none;">Expand</button>';
                        }
                        $return_html .= '<div class="dd-handle"><span class="menu_name"><b>' . $menu['navigation_label'] . '</b></span><span class="pull-right">Category<span class="li_edit"> | <a href="javascript:void(0);" onclick="editMenu(' . $menu['id'] . ')"><i class="fa fa-pencil"></i> </a><a href="javascript:void(0);" onclick="itemDelete(' . $menu['id'] . ');"><i class="fa fa-trash"></i></a></span></span></div>';
                    }
                }
            } elseif ($menu['table'] == 'custom_link') {
                $return_html .= '<li class="dd-item" data-id="' . $menu['id'] . '" data-table="custom_link" data-menu_name="' . $menu['menu_name'] . '" data-menu_url="' . $menu['menu_url'] . '" data-menu_image="' . $menu['menu_image'] . '"  data-menu_icon="' . $menu['menu_icon'] . '" data-menu_icon_position="' . $menu['menu_icon_position'] . '"  data-menu_type="' . $menu['menu_type'] . '" data-menu_sub_type="' . $menu['menu_sub_type'] . '" data-menu_description="' . $menu['menu_description'] . '" data-special_main_title="' . $menu['special_main_title'] . '" data-special_sub_title="' . $menu['special_sub_title'] . '" data-special_description="' . $menu['special_description'] . '"  data-special_image="' . $menu['special_image'] . '" data-special_position="' . $menu['special_position'] . '"  data-menu_target="' . $menu['menu_target'] . '">';
                if (count($lisetd_children) > 0) {
                    $return_html .= '<button data-action="collapse" type="button">Collapse</button><button data-action="expand" type="button" style="display: none;">Expand</button>';
                }
                $return_html .= '<div class="dd-handle"><span class="menu_name"><b>' . $menu['menu_name'] . '</b></span><span class="pull-right">Custom Link<span class="li_edit"> | <a href="javascript:void(0);" target="_self" onclick="editCustomMenu(' . $menu['id'] . ')"><i class="fa fa-pencil"></i> </a><a href="javascript:void(0);" target="' . $menu['menu_target'] . '" onclick="itemDelete(' . $menu['id'] . ');"><i class="fa fa-trash"></i></a></span></span></div>';
            }
            if (count($lisetd_children) > 0) {
                $return_html .= $this->getRecursiveMenuItem($lisetd_children);
            }
            $return_html .= '</li>';
            //$i++;
        }
        $return_html .= '</ol>';
        return $return_html;
    }

    public function getCategoryMenuItemData() {
        $id = $this->input->post('id');

        $contition_array = array('status' => 1, 'is_deleted' => 0, 'parent_id' => 0);
        $data = 'name,id,parent_id';
        $category_data = $this->common_model->select_data_by_condition('category', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

        $page_data_html = '';
        if (!empty($category_data)) {
            foreach ($category_data as $category) {
                $page_data_html .= '<div class="checker"><label><span><input type="checkbox"  name="category" value="category*' . $category['id'] . '*' . $category['name'] . '" class="parent_category_' . $category['id'] . '" id="parent_category_' . $category['id'] . '" onclick="getChildCategory(' . $category['id'] . ')"></span> ' . $category['name'] . ' </label></div>';
                $page_data_html .= $this->getRecursiveCategoryMenuItemData($category['id']);
            }
        }
        echo $page_data_html;
        exit;
    }

    public function getRecursiveCategoryMenuItemData($parent_id, $left = '0', $page_data_html = '') {
        $left = $left + 15;

        $contition_array = array('status' => 1, 'is_deleted' => 0, 'parent_id' => $parent_id);
        $data = 'name,id,parent_id';
        $sub_category_data = $this->common_model->select_data_by_condition('category', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

        if (!empty($sub_category_data)) {
            foreach ($sub_category_data as $sub_category) {
                $page_data_html .= '<div class="checker" style="padding-left:' . $left . 'px;"><label><span><input type="checkbox" class="child_of_' . $parent_id . '" id="child_category_' . $sub_category['id'] . '" name="category" value="category*' . $sub_category['id'] . '*' . $sub_category['name'] . '" onclick="getChildCategory(' . $sub_category['id'] . ')"></span> ' . $sub_category['name'] . ' </label></div>';
                $page_data_html .= $this->getRecursiveCategoryMenuItemData($sub_category['id'], $left);
            }
            return $page_data_html;
        }
    }

    public function getCategoryMenu() {
        $category_ids = $this->input->post('category_ids');
        $max_data_id = $this->input->post('max_data_id');

        $category_ids = explode(',', $category_ids);

        $parent_ids = '';
        $child_ids = '';
        foreach ($category_ids as $cat) {
            $exp_category = explode('_', $cat);
            $id = array_reverse($exp_category)[0];
            $is_parent = $exp_category[0];
            if ($is_parent == 'parent') {
                $parent_ids .= $id . ',';
            }

            $is_parent = $exp_category[0];
            if ($is_parent == 'child') {
                $child_ids .= $id . ',';
            }
        }

        $parent_ids = trim($parent_ids, ',');
        
        $parent_ids_list = str_replace(",", "','", $parent_ids);
        $parent_ids_array = explode(',', $parent_ids);

        $child_ids = trim($child_ids, ',');
        $child_ids_list = str_replace(",", "','", $child_ids);
        $child_ids_array = explode(',', $child_ids);

        $return_html = '';
        $using_sub_cat = '';
        if ($parent_ids != '') {
            $condition_array = array('status' => 1, 'is_deleted' => 0, 'parent_id' => 0);
            $search_condition = "id IN('" . $parent_ids_list . "')";
            $data = 'id,name';
            $parent_category = $this->common_model->select_data_by_search('category', $search_condition, $condition_array, $data, $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = '', $custom_order_by = '', $group_by = '');
            if (!empty($parent_category)) {
                foreach ($parent_category as $category) {
                    $max_data_id = $max_data_id + 1;
                    $return_html .= '<li class="dd-item" data-id="' . $max_data_id . '" data-table="category" data-table_id="' . $category['id'] . '" data-original_name="' . $category['name'] . '" data-navigation_label="' . $category['name'] . '" data-menu_image=""  data-menu_icon=""  data-menu_icon_position=""  data-menu_type="" data-menu_sub_type="" data-menu_description=""  data-special_main_title="" data-special_sub_title="" data-special_description=""  data-special_image="" data-special_position="">';

                    $contition_array = array('status' => 1, 'is_deleted' => 0, 'parent_id' => $category['id']);
                    $data = 'name,id';
                    $sub_category = $this->common_model->select_data_by_condition('category', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
                    foreach ($sub_category as $sub_cat) {
                        if (in_array($sub_cat['id'], $child_ids_array)) {
                            $return_html .= '<button data-action="collapse" type="button">Collapse</button><button data-action="expand" type="button" style="display: none;">Expand</button>';
                            break;
                        }
                    }
                    $return_html .= '<div class="dd-handle"><span class="menu_name"><b>' . $category['name'] . '</b></span><span class="pull-right">Category<span class="li_edit"> | <a href="javascript:void(0);" onclick="editMenu(' . $max_data_id . ')"><i class="fa fa-pencil"></i> </a><a href="javascript:void(0);" onclick="itemDelete(' . $max_data_id . ');"><i class="fa fa-trash"></i></a></span></span></div>';
                    $return_menu = $this->getRecursiveCategoryMenu($sub_category, $max_data_id, $child_ids_array);
                    $return_html .= $return_menu[0];
                    $using_sub_cat .= $this->getRecursiveUsingSubCat($sub_category, $max_data_id, $child_ids_array);
                    $max_data_id = $return_menu[1] + 1;
                    $return_html .= '</li>';
                }
            }
        }
        if ($parent_ids == '' && $child_ids != '') {
            $condition_array = array('status' => 1, 'is_deleted' => 0);
            $search_condition = 'id IN(' . $child_ids_list . ')';
            $data = 'id,name';
            $child_category = $this->common_model->select_data_by_search('category', $search_condition, $condition_array, $data, $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = '', $custom_order_by = '', $group_by = '');

            if (!empty($child_category)) {
                foreach ($child_category as $category) {
                    $max_data_id = $max_data_id + 1;
                    $contition_array = array('status' => 1, 'is_deleted' => 0, 'parent_id' => $category['id']);
                    $data = 'name,id';
                    $sub_category = $this->common_model->select_data_by_condition('category', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

                    if (!empty($sub_category)) {
                        $using_sub_cat1 .= $this->getRecursiveUsingChildSubCat($sub_category, $max_data_id, $child_ids_array);
                        $using_sub_cat2 = trim($using_sub_cat1, ',');
                        $using_sub_cat3 = explode(',', $using_sub_cat2);
                        if (in_array($category['id'], $using_sub_cat3)) {
                            break;
                        }
                    }
                    $return_html .= '<li class="dd-item" data-id="' . $max_data_id . '" data-table="category" data-table_id="' . $category['id'] . '" data-original_name="' . $category['name'] . '" data-navigation_label="' . $category['name'] . '" data-menu_image=""  data-menu_icon="" data-menu_icon_position="" data-menu_type="" data-menu_sub_type="" data-menu_description=""  data-special_main_title="" data-special_sub_title="" data-special_description=""  data-special_image="" data-special_position="">';
                    if (!empty($sub_category)) {
                        foreach ($sub_category as $sub_cat) {
                            if (in_array($sub_cat['id'], $child_ids_array)) {
                                $return_html .= '<button data-action="collapse" type="button">Collapse</button><button data-action="expand" type="button" style="display: none;">Expand</button>';
                                break;
                            }
                        }
                    }
                    $return_html .= '<div class="dd-handle"><span class="menu_name"><b>' . $category['name'] . '</b></span><span class="pull-right">Category<span class="li_edit"> | <a href="javascript:void(0);" onclick="editMenu(' . $max_data_id . ')"><i class="fa fa-pencil"></i> </a><a href="javascript:void(0);" onclick="itemDelete(' . $max_data_id . ');"><i class="fa fa-trash"></i></a></span></span></div>';
                    if (!empty($sub_category)) {
                        //$return_html .= $this->getRecursiveChildCategoryMenu($sub_category, $max_data_id, $child_ids_array);
                        $return_menu = $this->getRecursiveChildCategoryMenu($sub_category, $max_data_id, $child_ids_array);
                        $return_html .= $return_menu[0];
                        $max_data_id = $return_menu[1] + 1;
                    }
                    $return_html .= '</li>';
                }
            }
        }
        if ($parent_ids != '' && $child_ids != '') {

            $using_sub_cat = trim($using_sub_cat, ',');
            $using_sub_cat = explode(',', $using_sub_cat);

            $different_array = array_diff($child_ids_array, $using_sub_cat);

            if ($different_array) {
                $different_ids = implode(',', $different_array);
                $different_ids_list = str_replace(",", "','", $different_ids);

                $condition_array = array('status' => 1, 'is_deleted' => 0);
                $search_condition = 'id IN(' . $different_ids_list . ')';
                $data = 'id, name';
                $child_category = $this->common_model->select_data_by_search('category', $search_condition, $condition_array, $data, $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = '', $custom_order_by = '', $group_by = '');
                if (!empty($child_category)) {
                    foreach ($child_category as $category) {
                        $max_data_id = $max_data_id + 1;
                        $return_html .= '<li class="dd-item" data-id="' . $max_data_id . '" data-table="category" data-table_id="' . $category['id'] . '" data-original_name="' . $category['name'] . '" data-navigation_label="' . $category['name'] . '" data-menu_image=""  data-menu_icon=""  data-menu_icon_position="" data-menu_type="" data-menu_sub_type="" data-menu_description=""  data-special_main_title="" data-special_sub_title="" data-special_description=""  data-special_image="" data-special_position="">';
                        $return_html .= '<div class="dd-handle"><span class="menu_name"><b>' . $category['name'] . '</b></span><span class="pull-right">Category<span class="li_edit"> | <a href="javascript:void(0);" onclick="editMenu(' . $max_data_id . ')"><i class="fa fa-pencil"></i> </a><a href="javascript:void(0);" onclick="itemDelete(' . $max_data_id . ');"><i class="fa fa-trash"></i></a></span></span></div>';
                        $return_html .= '</li>';
                    }
                }
            }
        }
        echo $return_html;
        exit;
    }

    public function getRecursiveCategoryMenu($sub_category = array(), $max_data_id = '0', $child_ids_array = array(), $return_html = '', $using_sub_cat = '') {
        $return_menu = array();
        $return_html .= '<ol class="dd-list">';
        foreach ($sub_category as $sub_cat) {
            $using_sub_cat .= $sub_cat['id'] . ',';
            $max_data_id = $max_data_id + 1;
            if (in_array($sub_cat['id'], $child_ids_array)) {
                $return_html .= '<li class="dd-item" data-id="' . $max_data_id . '" data-table="category" data-table_id="' . $sub_cat['id'] . '" data-original_name="' . $sub_cat['name'] . '" data-navigation_label="' . $sub_cat['name'] . '" data-menu_image=""  data-menu_icon="" data-menu_icon_position="" data-menu_type="" data-menu_sub_type="" data-menu_description=""  data-special_main_title="" data-special_sub_title="" data-special_description=""  data-special_image="" data-special_position="">';

                $contition_array = array('status' => 1, 'is_deleted' => 0, 'parent_id' => $sub_cat['id']);
                $data = 'name,id';
                $sub_category1 = $this->common_model->select_data_by_condition('category', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

                if (!empty($sub_category1)) {
                    foreach ($sub_category1 as $sub_cat1) {
                        if (in_array($sub_cat1['id'], $child_ids_array)) {
                            $return_html .= '<button data-action="collapse" type="button">Collapse</button><button data-action="expand" type="button" style="display: none;">Expand</button>';
                            break;
                        }
                    }
                }
                $return_html .= '<div class="dd-handle"><span class="menu_name"><b>' . $sub_cat['name'] . '</b></span><span class="pull-right">Category<span class="li_edit"> | <a href="javascript:void(0);" onclick="editMenu(' . $max_data_id . ')"><i class="fa fa-pencil"></i> </a><a href="javascript:void(0);" onclick="itemDelete(' . $max_data_id . ');"><i class="fa fa-trash"></i></a></span></span></div>';
                if (!empty($sub_category1)) {
                    $recCategoryData = $this->getRecursiveCategoryMenu($sub_category1, $max_data_id, $child_ids_array);
                    if (!empty($recCategoryData[0])) {
                        $return_html .= $recCategoryData[0];
                    }
                }
                $return_html .= '</li>';
            }
        }
        $return_html .= '</ol>';
        //return $return_html;
        $return_menu = array($return_html, $max_data_id);
        return $return_menu;
    }

    public function getRecursiveUsingSubCat($sub_category = array(), $max_data_id = '0', $child_ids_array = array(), $using_sub_cat = '') {
        foreach ($sub_category as $sub_cat) {
            $using_sub_cat .= $sub_cat['id'] . ',';
            $max_data_id = $max_data_id + 1;
            if (in_array($sub_cat['id'], $child_ids_array)) {

                $contition_array = array('status' => 1, 'is_deleted' => 0, 'parent_id' => $sub_cat['id']);
                $data = 'name,id';
                $sub_category1 = $this->common_model->select_data_by_condition('category', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

                if (!empty($sub_category1)) {
                    $using_sub_cat .= $this->getRecursiveUsingSubCat($sub_category1, $max_data_id, $child_ids_array);
                }
            }
        }
        return $using_sub_cat;
    }

    public function getRecursiveChildCategoryMenu($sub_category = array(), $max_data_id = '0', $child_ids_array = array()) {
        $return_menu = array();
        $return_html .= '<ol class="dd-list">';
        foreach ($sub_category as $sub_cat) {
            $using_sub_cat .= $sub_cat['id'] . ',';
            $max_data_id = $max_data_id + 1;
            if (in_array($sub_cat['id'], $child_ids_array)) {
                $return_html .= '<li class="dd-item" data-id="' . $max_data_id . '" data-table="category" data-table_id="' . $sub_cat['id'] . '" data-original_name="' . $sub_cat['name'] . '" data-navigation_label="' . $sub_cat['name'] . '" data-menu_image=""  data-menu_icon="" data-menu_icon_position="" data-menu_type="" data-menu_sub_type="" data-menu_description=""  data-special_main_title="" data-special_sub_title="" data-special_description=""  data-special_image="" data-special_position="">';

                $contition_array = array('status' => 1, 'is_deleted' => 0, 'parent_id' => $sub_cat['id']);
                $data = 'name,id';
                $sub_category1 = $this->common_model->select_data_by_condition('category', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

                if (!empty($sub_category1)) {
                    foreach ($sub_category1 as $sub_cat1) {
                        if (in_array($sub_cat1['id'], $child_ids_array)) {
                            $return_html .= '<button data-action="collapse" type="button">Collapse</button><button data-action="expand" type="button" style="display: none;">Expand</button>';
                            break;
                        }
                    }
                }
                $return_html .= '<div class="dd-handle"><span class="menu_name"><b>' . $sub_cat['name'] . '</b></span><span class="pull-right">Category<span class="li_edit"> | <a href="javascript:void(0);" onclick="editMenu(' . $max_data_id . ')"><i class="fa fa-pencil"></i> </a><a href="javascript:void(0);" onclick="itemDelete(' . $max_data_id . ');"><i class="fa fa-trash"></i></a></span></span></div>';
                if (!empty($sub_category1)) {
                    $return_html .= $this->getRecursiveChildCategoryMenu($sub_category1, $max_data_id, $child_ids_array);
                }
                $return_html .= '</li>';
            }
        }
        $return_html .= '</ol>';
        //return $return_html;
        $return_menu = array($return_html, $max_data_id);
        return $return_menu;
    }

    public function getRecursiveUsingChildSubCat($sub_category = array(), $max_data_id = '0', $child_ids_array = array()) {
        foreach ($sub_category as $sub_cat) {
            $em = $this->getDoctrine()->getManager();
            $using_sub_cat1 .= $sub_cat['id'] . ',';
            $max_data_id = $max_data_id + 1;
            if (in_array($sub_cat['id'], $child_ids_array)) {
                $contition_array = array('status' => 1, 'is_deleted' => 0, 'parent_id' => $sub_cat['id']);
                $data = 'name,id';
                $sub_category1 = $this->common_model->select_data_by_condition('category', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

                if (!empty($sub_category1)) {
                    $using_sub_cat1 .= $this->getRecursiveUsingChildSubCat($sub_category1, $max_data_id, $child_ids_array);
                }
            }
        }
        return $using_sub_cat1;
    }

}

?>