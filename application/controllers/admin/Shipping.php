<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Shipping extends Admin_Controller {

    public $data;

    public function __construct() {

        parent::__construct();

        if (!$this->session->userdata('sl_admin')) {
            redirect('admin/login');
        }
        $this->load->model('admin/common_model');

        $site_name_values = $this->common_model->select_data_by_id('settings', 'setting_id', '1', '*');
        $this->data['site_name'] = $site_name = $site_name_values[0]['setting_val'];
        $this->data['title'] = 'Shipping | ' . $site_name;

        //remove catch so after logout cannot view last visited page if that page is this
        $this->output->set_header('Last-Modified:' . gmdate('D, d M Y H:i:s') . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
        $this->output->set_header('Pragma: no-cache');
    }

    //display shipping list
    public function index() {

        $this->data['module_name'] = 'Module Management';
        $this->data['section_title'] = 'Shipping';

        $contition_array = array('shipping.is_deleted' => 0);
        $data = 'shipping.*,country.name as cname';
        $join_str[0]['table'] = 'country';
        $join_str[0]['join_table_id'] = 'country.id';
        $join_str[0]['from_table_id'] = 'shipping.country_id';
        $join_str[0]['join_type'] = '';

        $shipping_list = $this->common_model->select_data_by_condition('shipping', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '', $join_str, $group_by = '');

        $this->data['shipping_list'] = $shipping_list;
        //load shipping data
        $this->template->admin_render('admin/shipping/index', $this->data);
    }

    //add the shipping detail
    public function add() {
        if ($this->input->post()) {
            if ($this->input->post('country_id') == '') {
                $this->session->set_flashdata('error', 'Country id is required');
                redirect('admin/shipping', 'refresh');
            }

            if ($this->input->post('price_from') == '') {
                $this->session->set_flashdata('error', 'Price from is required');
                redirect('admin/shipping', 'refresh');
            }

            if ($this->input->post('price_to') == '') {
                $this->session->set_flashdata('error', 'Price to is required');
                redirect('admin/shipping', 'refresh');
            }
            if ($this->input->post('amount') == '') {
                $this->session->set_flashdata('error', 'Shipping amount is required');
                redirect('admin/shipping', 'refresh');
            }

            $countryId = $this->input->post('country_id');
            $priceFrom = $this->input->post('price_from');
            $priceTo = $this->input->post('price_to');
            $chekshipping = $this->checkuniqueshipping($countryId, $priceFrom, $priceTo);
            if ($chekshipping != 0) {
                $this->session->set_flashdata('error', 'This shipping charge has been selected for this country');
                redirect('admin/shipping', 'refresh');
            }
            $insert_data = array();
            $insert_data['country_id'] = $countryId;
            $insert_data['price_from'] = $priceFrom;
            $insert_data['price_to'] = $priceTo;
            $insert_data['amount'] = $this->input->post('amount');
            $insert_data['created_date'] = date('Y-m-d H:i:s');
            $insert_data['modify_date'] = date('Y-m-d H:i:s');
            $insert_data['status'] = $this->input->post('status');
            $insert_data['is_deleted'] = 0;

            $insertData = $this->common_model->insert_data_get_id($insert_data, 'shipping');
            if ($insertData) {
                $this->session->set_flashdata('success', 'Shipping has been sucessfully inserted.');
                redirect('admin/shipping', 'refresh');
                die();
            } else {
                $this->session->set_flashdata('error', 'oops! something is wrong.');
                redirect('admin/shipping/add', 'refresh');
                die();
            }
        }
        $this->data['module_name'] = 'Shipping';
        $this->data['section_title'] = 'Add Shipping';

        $contition_array = array('is_deleted' => '0');
        $data = 'id,name';
        $country_list = $this->common_model->select_data_by_condition('country', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
        $this->data['country_list'] = $country_list;
        //load country data
        $this->template->admin_render('admin/shipping/add', $this->data);
    }

    //update the shipping detail
    public function edit($id = '') {
        if ($this->input->post('shipping_id')) {
            $shipping_id = $this->input->post('shipping_id');
            if ($this->input->post('country_id') == '') {
                $this->session->set_flashdata('error', 'Country id is required');
                redirect('admin/shipping', 'refresh');
            } elseif ($this->input->post('price_from') == '') {
                $this->session->set_flashdata('error', 'Price from is required');
                redirect('admin/shipping', 'refresh');
            } elseif ($this->input->post('price_to') == '') {
                $this->session->set_flashdata('error', 'Price to is required');
                redirect('admin/shipping', 'refresh');
            } elseif ($this->input->post('amount') == '') {
                $this->session->set_flashdata('error', 'Shipping Amount is required');
                redirect('admin/shipping', 'refresh');
            } else {
                $update_array = array();
                $update_array['country_id'] = $this->input->post('country_id');
                $update_array['price_from'] = $this->input->post('price_from');
                $update_array['price_to'] = $this->input->post('price_to');
                $update_array['amount'] = $this->input->post('amount');
                $update_array['modify_date'] = date('Y-m-d H:i:s');
                $update_array['status'] = $this->input->post('status');

                $update_result = $this->common_model->update_data($update_array, 'shipping', 'id', $this->input->post('shipping_id'));

                if ($update_result) {
                    $this->session->set_flashdata('success', 'Shipping successfully updated.');
                    redirect('admin/shipping', 'refresh');
                } else {
                    $this->session->set_flashdata('error', 'Error Occurred. Try Again!');
                    redirect('admin/shipping/edit/' . $this->input->post('shipping_id'), 'refresh');
                }
            }
        }
        $shipping_detail = $this->common_model->select_data_by_id('shipping', 'id', $id, '*');
        if (!empty($shipping_detail)) {
            $this->data['module_name'] = 'Shipping';
            $this->data['section_title'] = 'Edit Shipping';
            $this->data['shipping_detail'] = $shipping_detail;
            //load shipping details
            $contition_array = array('is_deleted' => '0');
            $data = 'id,name';
            $country_list = $this->common_model->select_data_by_condition('country', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
            $this->data['country_list'] = $country_list;
            //load country data
            $this->template->admin_render('admin/shipping/edit', $this->data);
        } else {
            $this->session->set_flashdata('error', 'Errorout Occurred. Try Again.');
            redirect('admin/shipping', 'refresh');
        }
    }

    // delete page
    public function delete($id = '') {
        $update_data = array('is_deleted' => 1);
        $update_result = $this->common_model->update_data($update_data, 'shipping', 'id', $id);

        if ($update_result) {
            $this->session->set_flashdata('success', 'Shipping deleted successfully.');
            redirect('admin/shipping');
        } else {
            $this->session->set_flashdata('error', 'Something went wrong! Try Again.');
            redirect('admin/shipping');
        }
    }

    public function checkuniqueshipping($countryId = "", $pricefrom = "", $priceto = "") {
        $shippingId = 0;
        $contition_array = array('is_deleted' => '0', 'country_id' => $countryId);
        $data = 'id';
//        $this->db->group_start();
//        $this->db->where('(price_from >= ' . $pricefrom . ' AND price_from <=' . $priceto . ') OR (price_to >= ' . $pricefrom . ' AND price_to <=' . $priceto . ')');
//        $this->db->group_end();
		$this->db->group_start();
        $this->db->group_start();
        $this->db->where('price_from >= ', $pricefrom);
        $this->db->where('price_to <= ', $priceto);
        $this->db->group_end();
        $this->db->or_group_start();
        $this->db->or_where('price_to >= ', $pricefrom);
        $this->db->where('price_from <= ', $priceto);
        $this->db->group_end();
		$this->db->group_end();
        $check_shipping = $this->common_model->select_data_by_condition('shipping', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
		if (!empty($check_shipping) && count($check_shipping) > 0) {
            $shippingId = $check_shipping[0]['id'];
        }
        return $shippingId;
    }

}

?>
