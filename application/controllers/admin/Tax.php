<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tax extends Admin_Controller {

    public $data;

    public function __construct() {

        parent::__construct();

        if (!$this->session->userdata('sl_admin')) {
            redirect('admin/login');
        }
        $this->load->model('admin/common_model');

        $site_name_values = $this->common_model->select_data_by_id('settings', 'setting_id', '1', '*');
        $this->data['site_name'] = $site_name = $site_name_values[0]['setting_val'];
        $this->data['title'] = 'Tax | ' . $site_name;

        //remove catch so after logout cannot view last visited page if that page is this
        $this->output->set_header('Last-Modified:' . gmdate('D, d M Y H:i:s') . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
        $this->output->set_header('Pragma: no-cache');
    }

    //display tax list
    public function index() {

        $this->data['module_name'] = 'Module Management';
        $this->data['section_title'] = 'Tax';

        $contition_array = array('tax.is_deleted' => 0);
        $data = 'tax.*,country.name as cname';
        $join_str[0]['table'] = 'country';
        $join_str[0]['join_table_id'] = 'country.id';
        $join_str[0]['from_table_id'] = 'tax.country_id';
        $join_str[0]['join_type'] = '';

        $tax_list = $this->common_model->select_data_by_condition('tax', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '', $join_str, $group_by = '');

        $this->data['tax_list'] = $tax_list;
        //load tax data
        $this->template->admin_render('admin/tax/index', $this->data);
    }

    //add the tax detail
    public function add() {
        if ($this->input->post()) {
            if ($this->input->post('country_id') == '') {
                $this->session->set_flashdata('error', 'Country id is required');
                redirect('admin/tax', 'refresh');
            }

            if ($this->input->post('tax_label') == '') {
                $this->session->set_flashdata('error', 'Tax label is required');
                redirect('admin/tax', 'refresh');
            }

            if ($this->input->post('tax_rate') == '') {
                $this->session->set_flashdata('error', 'Tax rate is required');
                redirect('admin/tax', 'refresh');
            }

            $countryId = $this->input->post('country_id');
            $taxLabel = $this->input->post('tax_label');
            $taxRate = $this->input->post('tax_rate');
            $chektax = $this->checkuniquetax($countryId);
            if ($chektax != 0) {
                $this->session->set_flashdata('error', 'This Tax rate has been selected for this country');
                redirect('admin/tax', 'refresh');
            }
            $insert_data = array();
            $insert_data['country_id'] = $countryId;
            $insert_data['tax_rate'] = $taxRate;
            $insert_data['tax_label'] = $taxLabel;
            $insert_data['created_date'] = date('Y-m-d H:i:s');
            $insert_data['modify_date'] = date('Y-m-d H:i:s');
            $insert_data['is_deleted'] = 0;
			$insert_data['status'] = 1;

            $insertData = $this->common_model->insert_data_get_id($insert_data, 'tax');
            if ($insertData) {
                $this->session->set_flashdata('success', 'Tax has been sucessfully inserted.');
                redirect('admin/tax', 'refresh');
                die();
            } else {
                $this->session->set_flashdata('error', 'oops! something is wrong.');
                redirect('admin/tax/add', 'refresh');
                die();
            }
        }
        $this->data['module_name'] = 'Tax';
        $this->data['section_title'] = 'Add Tax';

        $contition_array = array('is_deleted' => '0');
        $data = 'id,name';
        $country_list = $this->common_model->select_data_by_condition('country', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
        $this->data['country_list'] = $country_list;
        //load country data
        $this->template->admin_render('admin/tax/add', $this->data);
    }

    //update the tax detail
    public function edit($id = '') {
        if ($this->input->post('tax_id')) {
            $tax_id = $this->input->post('tax_id');

            if ($this->input->post('country_id') == '') {
                $this->session->set_flashdata('error', 'Country id is required');
                redirect('admin/tax', 'refresh');
            }

            if ($this->input->post('tax_label') == '') {
                $this->session->set_flashdata('error', 'Tax Lebel is required');
                redirect('admin/tax', 'refresh');
            }

            if ($this->input->post('tax_rate') == '') {
                $this->session->set_flashdata('error', 'Tax Rate is required');
                redirect('admin/tax', 'refresh');
            }

            $countryId = $this->input->post('country_id');
            $chektax = $this->checkuniquetax($countryId, $tax_id);
            if ($chektax != 0) {
                $this->session->set_flashdata('error', 'This Tax rate has been selected for this country');
                redirect('admin/tax', 'refresh');
            }


            $update_array = array();
            $update_array['country_id'] = $this->input->post('country_id');
            $update_array['tax_label'] = $this->input->post('tax_label');
            $update_array['tax_rate'] = $this->input->post('tax_rate');
            $update_array['modify_date'] = date('Y-m-d H:i:s');

            $update_result = $this->common_model->update_data($update_array, 'tax', 'id', $this->input->post('tax_id'));

            if ($update_result) {
                $this->session->set_flashdata('success', 'Tax successfully updated.');
                redirect('admin/tax', 'refresh');
            } else {
                $this->session->set_flashdata('error', 'Error Occurred. Try Again!');
                redirect('admin/tax/edit/' . $this->input->post('tax_id'), 'refresh');
            }
        }
        $tax_detail = $this->common_model->select_data_by_id('tax', 'id', $id, '*');
        if (!empty($tax_detail)) {
            $this->data['module_name'] = 'Tax';
            $this->data['section_title'] = 'Edit Tax';
            $this->data['tax_detail'] = $tax_detail;
            //load tax details
            $contition_array = array('is_deleted' => '0');
            $data = 'id,name';
            $country_list = $this->common_model->select_data_by_condition('country', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
            $this->data['country_list'] = $country_list;
            //load country data
            $this->template->admin_render('admin/tax/edit', $this->data);
        } else {
            $this->session->set_flashdata('error', 'Errorout Occurred. Try Again.');
            redirect('admin/tax', 'refresh');
        }
    }

    // delete page
    public function delete($id = '') {
        $update_data = array('is_deleted' => 1);
        $update_result = $this->common_model->update_data($update_data, 'tax', 'id', $id);

        if ($update_result) {
            $this->session->set_flashdata('success', 'Tax deleted successfully.');
            redirect('admin/tax');
        } else {
            $this->session->set_flashdata('error', 'Something went wrong! Try Again.');
            redirect('admin/tax');
        }
    }

    public function checkuniquetax($countryId = "", $id = "") { 
        $taxId = 0;
        $contition_array = array('is_deleted' => '0', 'country_id' => $countryId);
        $data = 'id';
        if (!empty($id))
            $this->db->where('id !=', $id);
        $check_tax = $this->common_model->select_data_by_condition('tax', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

        if (!empty($check_tax) && count($check_tax) > 0) {
            $taxId = $check_tax[0]['id'];
        }
        return $taxId;
    }

}

?>