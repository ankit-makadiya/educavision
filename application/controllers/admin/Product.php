<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Product extends Admin_Controller {

    public $data;

    public function __construct() {

        parent::__construct();

        if (!$this->session->userdata('sl_admin')) {
            redirect('admin/login');
        }

        //site setting details
        $this->load->model('admin/common_model');
        //Loadin Pagination Custome Config File
        $this->config->load('paging', TRUE);
        $this->paging = $this->config->item('paging');

        $site_name_values = $this->common_model->select_data_by_id('settings', 'setting_id', '1', '*');
        $this->data['site_name'] = $site_name = $site_name_values[0]['setting_val'];
        $this->data['title'] = 'Product | ' . $site_name;

        //remove catch so after logout cannot view last visited page if that page is this
        $this->output->set_header('Last-Modified:' . gmdate('D, d M Y H:i:s') . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
        $this->output->set_header('Pragma: no-cache');
    }

    public function index() {
        $this->data['module_name'] = 'Product Management';
        $this->data['section_title'] = 'Product Management';
        $this->session->unset_userdata('user_search_keyword');

        $this->data['limit'] = $limit = $this->paging['per_page'];

        if ($this->uri->segment(3) != '' && $this->uri->segment(4) != '') {
            $offset = ($this->uri->segment(5) != '') ? $this->uri->segment(5) - 1 : 0;
            $short_by = $this->uri->segment(3);
            $order_by = $this->uri->segment(4);
        } else {
            $offset = ($this->uri->segment(3) != '') ? $this->uri->segment(3) - 1 : 0;
            $short_by = 'id';
            $order_by = 'asc';
        }
        //echo $offset.'_'.$short_by.'_'.$order_by; exit;
        $offset = $offset * $limit;
        $this->data['offset'] = $offset;

        $contition_array = array('product.is_deleted' => '0', 'pv.is_default' => 1);
        $data = 'product.*,pv.sku,pv.price,pv.isbn_no';

        $join_str = array();
        $join_str[0]['table'] = 'product_variation as pv';
        $join_str[0]['join_table_id'] = 'pv.product_id';
        $join_str[0]['from_table_id'] = 'product.id';
        $join_str[0]['join_type'] = 'left';

        $product_list = $this->common_model->select_data_by_condition('product', $contition_array, $data, $short_by, $order_by, $limit, $offset, $join_str, $custom_order_by = '', $group_by = '');

        if ($this->uri->segment(3) != '' && $this->uri->segment(4) != '') {
            $this->paging['base_url'] = site_url("admin/product/" . $short_by . "/" . $order_by);
        } else {
            $this->paging['base_url'] = site_url("admin/product/");
        }
        if ($this->uri->segment(3) != '' && $this->uri->segment(4) != '') {
            $this->paging['uri_segment'] = 5;
        } else {
            $this->paging['uri_segment'] = 3;
        }
        foreach ($product_list as $key => $product) {
            $condition_array = array('is_deleted' => '0');
            $search_condition = "id IN ('" . trim($product['category_id'], ',') . "')";
            $data = 'GROUP_CONCAT(name) as category_name';
            $category_data = $this->common_model->select_data_by_search('category', $search_condition, $condition_array, $data, $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = '', $custom_order_by = '', $group_by = '');

            $product_list[$key]['categoryName'] = $category_data[0]['category_name'];
        }

        $this->data['product_list'] = $product_list;
        $contition_array1 = array('product.is_deleted' => '0', 'pv.is_default' => 1);
        $data = 'product.id';

        $join_str = array();
        $join_str[0]['table'] = 'product_variation as pv';
        $join_str[0]['join_table_id'] = 'pv.product_id';
        $join_str[0]['from_table_id'] = 'product.id';
        $join_str[0]['join_type'] = 'left';

        $this->paging['total_rows'] = count($this->common_model->select_data_by_condition('product', $contition_array1, $data, $short_by, $order_by, $limit = '', $offset = '', $join_str, $custom_order_by = '', $group_by = ''));

        $this->data['total_rows'] = $this->paging['total_rows'];


        $this->pagination->initialize($this->paging);
        $this->data['search_keyword'] = '';
        $this->template->admin_render('admin/product/index', $this->data);
    }

    //search the blog
    public function search() {
        $this->data['module_name'] = 'Product Management';
        $this->data['section_title'] = 'Product Management';

        if ($this->input->post('search_keyword')) {
            $this->data['search_keyword'] = $search_keyword = $this->input->post('search_keyword');
            $this->session->set_userdata('user_search_keyword', $search_keyword);

            $this->data['limit'] = $limit = $this->paging['per_page'];

            if ($this->uri->segment(4) != '' && $this->uri->segment(5) != '') {
                $offset = ($this->uri->segment(6) != '') ? $this->uri->segment(6) - 1 : 0;
                $short_by = $this->uri->segment(4);
                $order_by = $this->uri->segment(3);
            } else {
                $offset = ($this->uri->segment(4) != '') ? $this->uri->segment(4) - 1 : 0;
                $short_by = 'product.id';
                $order_by = 'asc';
            }
            $offset = $offset * $limit;
            $this->data['offset'] = $offset;

            //prepare search condition
            $search_condition = "(product.name LIKE '%$search_keyword%' OR product.slug LIKE '%$search_keyword%' OR product.collection LIKE '%$search_keyword%' OR product.author_name LIKE '%$search_keyword%' OR product.description LIKE '%$search_keyword%' OR product.tags LIKE '%$search_keyword%')";

            $contition_array = array('product.is_deleted' => '0', 'pv.is_default' => 1);
            $data = 'product.*,pv.sku,pv.price,pv.isbn_no';

            $join_str = array();
            $join_str[0]['table'] = 'product_variation as pv';
            $join_str[0]['join_table_id'] = 'pv.product_id';
            $join_str[0]['from_table_id'] = 'product.id';
            $join_str[0]['join_type'] = 'left';


            $product_list = $this->common_model->select_data_by_search('product', $search_condition, $contition_array, $data, $short_by, $order_by, $limit, $offset, $join_str, $custom_order_by = '', $group_by = '');

            if ($this->uri->segment(4) != '' && $this->uri->segment(3) != '') {
                $this->paging['base_url'] = site_url("admin/product/search/" . $short_by . "/" . $order_by);
            } else {
                $this->paging['base_url'] = site_url("admin/product/search/");
            }
            if ($this->uri->segment(4) != '' && $this->uri->segment(5) != '') {
                $this->paging['uri_segment'] = 6;
            } else {
                $this->paging['uri_segment'] = 4;
            }

            foreach ($product_list as $key => $product) {
                $condition_array = array('is_deleted' => '0');
                $search_condition1 = "id IN ('" . trim($product['category_id'], ',') . "')";
                $data = 'GROUP_CONCAT(name) as category_name';
                $category_data = $this->common_model->select_data_by_search('category', $search_condition1, $condition_array, $data, $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $custom_order_by = '', $group_by = '');

                $product_list[$key]['categoryName'] = $category_data[0]['category_name'];
            }

            $this->data['product_list'] = $product_list;

            $contition_array1 = array();
            $data = 'count(*) as total';

            $join_str = array();
            $join_str[0]['table'] = 'product_variation as pv';
            $join_str[0]['join_table_id'] = 'pv.product_id';
            $join_str[0]['from_table_id'] = 'product.id';
            $join_str[0]['join_type'] = 'left';

            $total_rows = $this->common_model->select_data_by_search('product', $search_condition, $contition_array, $data, $short_by, $order_by, $limit = '', $offset = '', $join_str, $custom_order_by = '', $group_by = '');

            $this->paging['total_rows'] = $total_rows[0]['total'];
            $this->data['total_rows'] = $this->paging['total_rows'];

            $this->pagination->initialize($this->paging);
            $this->data['search_keyword'] = $this->input->post('search_keyword');
        } else if ($this->session->userdata('user_search_keyword')) {
            $this->data['search_keyword'] = $search_keyword = $this->session->userdata('user_search_keyword');

            $this->data['limit'] = $limit = $this->paging['per_page'];
            if ($this->uri->segment(4) != '' && $this->uri->segment(5) != '') {
                $offset = ($this->uri->segment(6) != '') ? $this->uri->segment(6) - 1 : 0;
                $short_by = $this->uri->segment(4);
                $order_by = $this->uri->segment(5);
            } else {
                $offset = ($this->uri->segment(4) != '') ? $this->uri->segment(4) - 1 : 0;
                $short_by = 'product.id';
                $order_by = 'asc';
            }
            $offset = $offset * $limit;
            $this->data['offset'] = $offset;

            $search_condition = "(product.name LIKE '%$search_keyword%' OR product.slug LIKE '%$search_keyword%' OR product.collection LIKE '%$search_keyword%' OR product.author_name LIKE '%$search_keyword%' OR product.description LIKE '%$search_keyword%' OR product.tags LIKE '%$search_keyword%')";

            $contition_array = array('product.is_deleted' => '0', 'pv.is_default' => 1);
            $data = 'product.*,pv.sku,pv.price,pv.isbn_no';

            $join_str = array();
            $join_str[0]['table'] = 'product_variation as pv';
            $join_str[0]['join_table_id'] = 'pv.product_id';
            $join_str[0]['from_table_id'] = 'product.id';
            $join_str[0]['join_type'] = 'left';

            $product_list = $this->common_model->select_data_by_search('product', $search_condition, $contition_array, $data, $short_by, $order_by, $limit, $offset, $join_str, $custom_order_by = '', $group_by = '');

            foreach ($product_list as $key => $product) {
                $condition_array = array('is_deleted' => '0');
                $search_condition1 = "id IN ('" . trim($product['category_id'], ',') . "')";
                $data = 'GROUP_CONCAT(name) as category_name';
                $category_data = $this->common_model->select_data_by_search('category', $search_condition1, $condition_array, $data, $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $custom_order_by = '', $group_by = '');

                $product_list[$key]['categoryName'] = $category_data[0]['category_name'];
            }

            $this->data['product_list'] = $product_list;



            if ($this->uri->segment(4) != '' && $this->uri->segment(5) != '') {
                $this->paging['base_url'] = site_url("admin/product/search/" . $short_by . "/" . $order_by);
            } else {
                $this->paging['base_url'] = site_url("admin/product/search/");
            }
            if ($this->uri->segment(4) != '' && $this->uri->segment(5) != '') {
                $this->paging['uri_segment'] = 6;
            } else {
                $this->paging['uri_segment'] = 4;
            }

            $contition_array1 = array();
            $data = 'count(*) as total';
            $join_str = array();
            $join_str[0]['table'] = 'product_variation as pv';
            $join_str[0]['join_table_id'] = 'pv.product_id';
            $join_str[0]['from_table_id'] = 'product.id';
            $join_str[0]['join_type'] = 'left';


            $total_rows = $this->common_model->select_data_by_search('product', $search_condition, $contition_array, $data, $short_by, $order_by, $limit = '', $offset = '', $join_str, $custom_order_by = '', $group_by = '');

            $this->paging['total_rows'] = $total_rows[0]['total'];
            $this->data['total_rows'] = $this->paging['total_rows'];

            $this->pagination->initialize($this->paging);
            $this->data['search_keyword'] = $this->input->post('search_keyword');
        }
        $this->template->admin_render('admin/product/index', $this->data);
    }

    //display category list
    public function indexBkp() {

        $this->data['module_name'] = 'Product Management';
        $this->data['section_title'] = 'Product Management';

        $contition_array = array('product.is_deleted' => '0', 'pv.is_default' => 1);
        $data = 'product.*,pv.sku,pv.price,pv.isbn_no';

        $join_str = array();
        $join_str[0]['table'] = 'product_variation as pv';
        $join_str[0]['join_table_id'] = 'pv.product_id';
        $join_str[0]['from_table_id'] = 'product.id';
        $join_str[0]['join_type'] = 'left';

        $product_list = $this->common_model->select_data_by_condition('product', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '', $join_str, $custom_order_by = '', $group_by = '');

        foreach ($product_list as $key => $product) {
            $condition_array = array('is_deleted' => '0');
            $search_condition = "id IN ('" . trim($product['category_id'], ',') . "')";
            $data = 'GROUP_CONCAT(name) as category_name';
            $category_data = $this->common_model->select_data_by_search('category', $search_condition, $condition_array, $data, $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = '', $custom_order_by = '', $group_by = '');

            $product_list[$key]['categoryName'] = $category_data[0]['category_name'];
        }

        $this->data['product_list'] = $product_list;
        //load product data
        $this->template->admin_render('admin/product/index', $this->data);
    }

    //add the product detail
    public function add() {
        if ($this->input->post()) {
            if ($this->input->post('name') == '') {
                $this->session->set_flashdata('error', 'Product name is required');
                redirect('admin/product', 'refresh');
            }
            if ($this->input->post('slug') == '') {
                $this->session->set_flashdata('error', 'Product slug is required');
                redirect('admin/product', 'refresh');
            }
            // if ($this->input->post('isbn_no') == '0') {
            // 	$this->session->set_flashdata('error', 'Product ISBN No. is required');
            // 	redirect('admin/product', 'refresh');
            // }
            if (count($this->input->post('category_id')) == '0') {
                $this->session->set_flashdata('error', 'Product category is required');
                redirect('admin/product', 'refresh');
            }
            // if ($this->input->post('price') == '') {
            // 	$this->session->set_flashdata('error', 'Product price is required');
            // 	redirect('admin/product', 'refresh');
            // }
            if ($this->input->post('author_name') == '') {
                $this->session->set_flashdata('error', 'Author name is required');
                redirect('admin/product', 'refresh');
            }
            if ($this->input->post('description') == '') {
                $this->session->set_flashdata('error', 'Product description is required');
                redirect('admin/product', 'refresh');
            }
            if ($this->input->post('product_image') == '') {
                $this->session->set_flashdata('error', 'Product image is required');
                redirect('admin/product', 'refresh');
            }

            if (count($this->input->post('attribute')) == '0') {
                $this->session->set_flashdata('error', 'Product variation is required');
                redirect('admin/product', 'refresh');
            }

            $insert_data = array();
            $insert_data['language'] = $this->input->post('language');
            $insert_data['name'] = $this->input->post('name');
            $insert_data['slug'] = $this->input->post('slug');
            //$insert_data['sku'] = $this->input->post('sku');
            //$insert_data['isbn_no'] = $this->input->post('isbn_no');
            $insert_data['collection'] = $this->input->post('collection');
            $insert_data['category_id'] = implode(',', $this->input->post('category_id'));
            //$insert_data['price'] = $this->input->post('price');
            $insert_data['author_name'] = $this->input->post('author_name');
            $insert_data['image'] = $this->input->post('product_image');
            //$insert_data['download_book'] = $this->input->post('download_book');
            $insert_data['tags'] = $this->input->post('tags');
            //$insert_data['weight'] = $this->input->post('weight');
            //$insert_data['book_type'] = $this->input->post('book_type');
            $insert_data['description'] = $this->input->post('description');
            $insert_data['best_seller'] = ($this->input->post('best_seller')) ? $this->input->post('best_seller') : '';
            $insert_data['item_of_the_month'] = ($this->input->post('item_of_the_month')) ? $this->input->post('item_of_the_month') : '';
            $insert_data['suggest_book'] = (!empty($this->input->post('suggest_book'))) ? implode(',', $this->input->post('suggest_book')) : '';
            $insert_data['priority'] = $this->input->post('priority');
            $insert_data['created_date'] = date('Y-m-d H:i:s');
            $insert_data['modify_date'] = date('Y-m-d H:i:s');
            $insert_data['status'] = $this->input->post('status');
            $insert_data['is_deleted'] = 0;
            $insertData = $this->common_model->insert_data_get_id($insert_data, 'product');
            if ($insertData) {

                if (!empty($this->input->post('variation'))) {
                    $variation = $this->input->post('variation');

                    if (isset($variation['paperback']) && !empty($variation['paperback'])) {
                        $insert_data = array();
                        $insert_data['product_id'] = $insertData;
                        $insert_data['attribute_id'] = 1;
                        $insert_data['sku'] = $variation['paperback']['sku'];
                        $insert_data['isbn_no'] = $variation['paperback']['isbn_no'];
                        $insert_data['price'] = $variation['paperback']['price'];
                        $insert_data['weight'] = $variation['paperback']['weight'];
                        $insert_data['download_book'] = '';
                        $insert_data['is_default'] = (!empty($variation['paperback']['is_default'])) ? $variation['paperback']['is_default'] : 0;
                        $insertVariationData = $this->common_model->insert_data_get_id($insert_data, 'product_variation');
                    }

                    if (isset($variation['hardcover']) && !empty($variation['hardcover'])) {
                        $insert_data = array();
                        $insert_data['product_id'] = $insertData;
                        $insert_data['attribute_id'] = 2;
                        $insert_data['sku'] = $variation['hardcover']['sku'];
                        $insert_data['isbn_no'] = $variation['hardcover']['isbn_no'];
                        $insert_data['price'] = $variation['hardcover']['price'];
                        $insert_data['weight'] = $variation['hardcover']['weight'];
                        $insert_data['download_book'] = '';
                        $insert_data['is_default'] = (!empty($variation['hardcover']['is_default'])) ? $variation['hardcover']['is_default'] : 0;
                        $insertVariationData = $this->common_model->insert_data_get_id($insert_data, 'product_variation');
                    }

                    if (isset($variation['e-book']) && !empty($variation['e-book'])) {
                        $insert_data = array();
                        $insert_data['product_id'] = $insertData;
                        $insert_data['attribute_id'] = 3;
                        $insert_data['sku'] = $variation['e-book']['sku'];
                        $insert_data['isbn_no'] = $variation['e-book']['isbn_no'];
                        $insert_data['price'] = $variation['e-book']['price'];
                        $insert_data['weight'] = $variation['e-book']['weight'];
                        $insert_data['download_book'] = $variation['e-book']['download_book'];
                        $insert_data['is_default'] = (!empty($variation['e-book']['is_default'])) ? $variation['e-book']['is_default'] : 0;
                        $insertVariationData = $this->common_model->insert_data_get_id($insert_data, 'product_variation');
                    }
                }

                $this->session->set_flashdata('success', 'Product has been sucessfully inserted.');
                redirect('admin/product', 'refresh');
                die();
            } else {
                $this->session->set_flashdata('error', 'oops! something is wrong.');
                redirect('admin/product', 'refresh');
                die();
            }
        }

        $this->data['module_name'] = 'Product';
        $this->data['section_title'] = 'Add Product';

        $contition_array = array('status' => 1, 'is_deleted' => 0);
        $data = 'id,name';
        $this->data['category_list'] = $this->common_model->select_data_by_condition('category', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');


        $contition_array = array('product.status' => 1, 'product.is_deleted' => 0);
        $data = 'product.name,product_variation.sku,attribute.name as attribute_name';

        $join_str[0]['table'] = 'product';
        $join_str[0]['join_table_id'] = 'product.id';
        $join_str[0]['from_table_id'] = 'product_variation.product_id';
        $join_str[0]['join_type'] = 'left';

        $join_str[1]['table'] = 'attribute';
        $join_str[1]['join_table_id'] = 'attribute.id';
        $join_str[1]['from_table_id'] = 'product_variation.attribute_id';
        $join_str[1]['join_type'] = 'left';

        $this->data['suggested_product_list'] = $suggested_product_list = $this->common_model->select_data_by_condition('product_variation', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '', $join_str, $group_by = '');

        $contition_array = array('status' => 1);
        $data = '*';

        $this->data['attribute_list'] = $this->common_model->select_data_by_condition('attribute', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '', $join_str = array(), $group_by = '');


        $this->template->admin_render('admin/product/add', $this->data);
    }

    //update the category detail
    public function edit($id = '') {
        if ($this->input->post('product_id')) {
            $product_id = $this->input->post('product_id');
            if ($this->input->post('name') == '') {
                $this->session->set_flashdata('error', 'Product name is required');
                redirect('admin/product', 'refresh');
            } elseif ($this->input->post('slug') == '') {
                $this->session->set_flashdata('error', 'Product slug is required');
                redirect('admin/product', 'refresh');
            }
            // elseif ($this->input->post('isbn_no') == '') {
            // 	$this->session->set_flashdata('error', 'Product ISBN No. is required');
            // 	redirect('admin/product', 'refresh');
            // } 
            elseif (count($this->input->post('category_id')) == '0') {
                $this->session->set_flashdata('error', 'Product category is required');
                redirect('admin/product', 'refresh');
            }
            // elseif ($this->input->post('price') == '') {
            // 	$this->session->set_flashdata('error', 'Product price is required');
            // 	redirect('admin/product', 'refresh');
            // } 
            elseif ($this->input->post('author_name') == '') {
                $this->session->set_flashdata('error', 'Author name is required');
                redirect('admin/product', 'refresh');
            } elseif ($this->input->post('description') == '') {
                $this->session->set_flashdata('error', 'Product description is required');
                redirect('admin/product', 'refresh');
            } elseif ($this->input->post('product_image') == '') {
                $this->session->set_flashdata('error', 'Product image is required');
                redirect('admin/product', 'refresh');
            } else {
                $update_array = array();
                $update_array['language'] = $this->input->post('language');
                $update_array['name'] = $this->input->post('name');
                $update_array['slug'] = $this->input->post('slug');
                //$update_array['isbn_no'] = $this->input->post('isbn_no');
                //$update_array['sku'] = $this->input->post('sku');
                $update_array['collection'] = $this->input->post('collection');
                $update_array['category_id'] = implode(',', $this->input->post('category_id'));
                //$update_array['price'] = $this->input->post('price');
                $update_array['author_name'] = $this->input->post('author_name');
                $update_array['image'] = $this->input->post('product_image');
                //$update_array['download_book'] = $this->input->post('download_book');
                $update_array['tags'] = $this->input->post('tags');
                //
                //$update_array['weight'] = $this->input->post('weight');
                //$update_array['book_type'] = $this->input->post('book_type');
                $update_array['description'] = $this->input->post('description');
                $update_array['best_seller'] = $this->input->post('best_seller');
                $update_array['item_of_the_month'] = $this->input->post('item_of_the_month');
                $update_array['suggest_book'] = (!empty($this->input->post('suggest_book'))) ? implode(',', $this->input->post('suggest_book')) : '';
                $update_array['priority'] = $this->input->post('priority');
                $update_array['modify_date'] = date('Y-m-d H:i:s');
                $update_array['status'] = $this->input->post('status');


                $update_result = $this->common_model->update_data($update_array, 'product', 'id', $this->input->post('product_id'));

                if ($update_result) {
                    $variation = $this->input->post('variation');

                    $condition_array = array('product_id' => $this->input->post('product_id'));
                    $data = 'GROUP_CONCAT(attribute_id) as attribute_id';
                    $available_variation = $this->common_model->select_data_by_condition('product_variation', $condition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

                    $available_attribute = explode(',', $available_variation[0]['attribute_id']);

                    if (isset($variation['paperback']) && !empty($variation['paperback'])) {

                        if (in_array(1, $available_attribute)) {
                            $update_data = array();
                            $update_data['sku'] = $variation['paperback']['sku'];
                            $update_data['isbn_no'] = $variation['paperback']['isbn_no'];
                            $update_data['price'] = $variation['paperback']['price'];
                            $update_data['weight'] = $variation['paperback']['weight'];
                            $update_data['download_book'] = '';
                            $paperback_default = 0;
                            if (!empty($variation['paperback']['is_default'])) {
                                $paperback_default = $variation['paperback']['is_default'];
                            }
                            $update_data['is_default'] = $paperback_default;

                            $where = array('product_id' => $this->input->post('product_id'), 'attribute_id ' => 1);
                            $this->db->where($where);
                            $this->db->update('product_variation ', $update_data);
                        } else {
                            $insert_data = array();
                            $insert_data['product_id'] = $this->input->post('product_id');
                            $insert_data['attribute_id'] = 1;
                            $insert_data['sku'] = $variation['paperback']['sku'];
                            $insert_data['isbn_no'] = $variation['paperback']['isbn_no'];
                            $insert_data['price'] = $variation['paperback']['price'];
                            $insert_data['weight'] = $variation['paperback']['weight'];
                            $insert_data['download_book'] = '';
                            $paperback_default = 0;
                            if (!empty($variation['paperback']['is_default'])) {
                                $paperback_default = $variation['paperback']['is_default'];
                            }
                            $insert_data['is_default'] = $paperback_default;
                            $insertVariationData = $this->common_model->insert_data_get_id($insert_data, 'product_variation');
                        }
                    }

                    if (isset($variation['hardcover']) && !empty($variation['hardcover'])) {
                        if (in_array(2, $available_attribute)) {
                            $update_data = array();
                            $update_data['sku'] = $variation['hardcover']['sku'];
                            $update_data['isbn_no'] = $variation['hardcover']['isbn_no'];
                            $update_data['price'] = $variation['hardcover']['price'];
                            $update_data['weight'] = $variation['hardcover']['weight'];
                            $update_data['download_book'] = '';
                            $hardcover_default = 0;
                            if (!empty($variation['hardcover']['is_default'])) {
                                $hardcover_default = $variation['hardcover']['is_default'];
                            }
                            $update_data['is_default'] = $hardcover_default;

                            $where = array('product_id' => $this->input->post('product_id'), 'attribute_id ' => 2);
                            $this->db->where($where);
                            $this->db->update('product_variation ', $update_data);
                        } else {
                            $insert_data = array();
                            $insert_data['product_id'] = $this->input->post('product_id');
                            $insert_data['attribute_id'] = 2;
                            $insert_data['sku'] = $variation['hardcover']['sku'];
                            $insert_data['isbn_no'] = $variation['hardcover']['isbn_no'];
                            $insert_data['price'] = $variation['hardcover']['price'];
                            $insert_data['weight'] = $variation['hardcover']['weight'];
                            $insert_data['download_book'] = '';
                            $hardcover_default = 0;
                            if (!empty($variation['hardcover']['is_default'])) {
                                $hardcover_default = $variation['hardcover']['is_default'];
                            }
                            $insert_data['is_default'] = $hardcover_default;
                            $insertVariationData = $this->common_model->insert_data_get_id($insert_data, 'product_variation');
                        }
                    }

                    if (isset($variation['e-book']) && !empty($variation['e-book'])) {
                        if (in_array(3, $available_attribute)) {
                            $update_data = array();
                            $update_data['sku'] = $variation['e-book']['sku'];
                            $update_data['isbn_no'] = $variation['e-book']['isbn_no'];
                            $update_data['price'] = $variation['e-book']['price'];
                            $update_data['weight'] = $variation['e-book']['weight'];
                            $update_data['download_book'] = '';
                            $e_book_default = 0;
                            if (!empty($variation['e-book']['is_default'])) {
                                $e_book_default = $variation['e-book']['is_default'];
                            }
                            $update_data['is_default'] = $e_book_default;
                            $where = array('product_id' => $this->input->post('product_id'), 'attribute_id ' => 3);
                            $this->db->where($where);
                            $this->db->update('product_variation ', $update_data);
                        } else {
                            $insert_data = array();
                            $insert_data['product_id'] = $this->input->post('product_id');
                            $insert_data['attribute_id'] = 3;
                            $insert_data['sku'] = $variation['e-book']['sku'];
                            $insert_data['isbn_no'] = $variation['e-book']['isbn_no'];
                            $insert_data['price'] = $variation['e-book']['price'];
                            $insert_data['weight'] = $variation['e-book']['weight'];
                            $insert_data['download_book'] = $variation['e-book']['download_book'];
                            $e_book_default = 0;
                            if (!empty($variation['e-book']['is_default'])) {
                                $e_book_default = $variation['e-book']['is_default'];
                            }
                            $insert_data['is_default'] = $e_book_default;
                            $insertVariationData = $this->common_model->insert_data_get_id($insert_data, 'product_variation');
                        }
                    }

                    $this->db->where_not_in('attribute_id', $this->input->post('attribute'));
                    $this->db->delete('product_variation');

                    $this->session->set_flashdata('success', 'Product successfully updated.');
                    redirect('admin/product', 'refresh');
                } else {
                    $this->session->set_flashdata('error', 'Error Occurred. Try Again!');
                    redirect('admin/product', 'refresh');
                }
            }
        }
        $product_detail = $this->common_model->select_data_by_id('product', 'id', $id, '*');
        if (!empty($product_detail)) {
            $this->data['module_name'] = 'Product';
            $this->data['section_title'] = 'Edit Product';
            $this->data['product_detail'] = $product_detail;

            $contition_array = array('status' => 1, 'is_deleted' => 0);
            $data = 'id,name';
            $this->data['category_list'] = $this->common_model->select_data_by_condition('category', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');


            $contition_array = array('product.status' => 1, 'product.is_deleted' => 0);
            $data = 'product.name,product_variation.sku,attribute.name as attribute_name';

            $join_str[0]['table'] = 'product';
            $join_str[0]['join_table_id'] = 'product.id';
            $join_str[0]['from_table_id'] = 'product_variation.product_id';
            $join_str[0]['join_type'] = 'left';

            $join_str[1]['table'] = 'attribute';
            $join_str[1]['join_table_id'] = 'attribute.id';
            $join_str[1]['from_table_id'] = 'product_variation.attribute_id';
            $join_str[1]['join_type'] = 'left';

            $this->data['suggested_product_list'] = $suggested_product_list = $this->common_model->select_data_by_condition('product_variation', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '', $join_str, $group_by = '');

            $contition_array = array('status' => 1);
            $data = '*';

            $attribute_list = $this->common_model->select_data_by_condition('attribute', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '', $join_str = array(), $group_by = '');

            foreach ($attribute_list as $key => $value) {
                $condition_array = array('attribute_id' => $value['id'], 'product_id' => $product_detail[0]['id']);
                $data = 'count(id) as total';
                $variation_count = $this->common_model->select_data_by_condition('product_variation', $condition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '', $join_str = array(), $group_by = '');
                $attribute_list[$key]['is_checked'] = 0;
                if ($variation_count[0]['total'] > 0) {
                    $attribute_list[$key]['is_checked'] = 1;
                }
            }

            $this->data['attribute_list'] = $attribute_list;

            $this->template->admin_render('admin/product/edit', $this->data);
        } else {
            $this->session->set_flashdata('error', 'Errorout Occurred. Try Again.');
            redirect('admin/product', 'refresh');
        }
    }

    // get name by id

    function getCatName() {
        if ($this->input->post('category_id')) {
            $category_id = $this->input->post('category_id');
            $contition_array = array('id' => $category_id);
            $data = 'name';
            $category_name = $this->common_model->select_data_by_condition('category', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

            $return_data = array();
            $return_data['category_name'] = $category_name[0]['name'];
            header('Content-Type: application/json');
            echo json_encode($return_data);
            exit;
        }
    }

    public function do_upload_multiple_files($fieldName, $options) {

        $response = array();
        $files = $_FILES;
        $cpt = count($_FILES[$fieldName]['name']);
        for ($i = 0; $i < $cpt; $i++) {
            $_FILES[$fieldName]['name'] = $files[$fieldName]['name'][$i];
            $_FILES[$fieldName]['type'] = $files[$fieldName]['type'][$i];
            $_FILES[$fieldName]['tmp_name'] = $files[$fieldName]['tmp_name'][$i];
            $_FILES[$fieldName]['error'] = $files[$fieldName]['error'][$i];
            $_FILES[$fieldName]['size'] = $files[$fieldName]['size'][$i];

            $this->load->library('upload');
            $this->upload->initialize($options);

            //upload the image
            if (!$this->upload->do_upload($fieldName)) {
                $response['error'][] = $this->upload->display_errors();
            } else {
                $response['result'][] = $this->upload->data();
                $category_thumb[$i]['image_library'] = 'gd2';
                $category_thumb[$i]['source_image'] = $this->config->item('category_main_upload_path') . $response['result'][$i]['file_name'];
                $category_thumb[$i]['new_image'] = $this->config->item('category_thumb_upload_path') . $response['result'][$i]['file_name'];
                $category_thumb[$i]['create_thumb'] = TRUE;
                $category_thumb[$i]['maintain_ratio'] = TRUE;
                $category_thumb[$i]['thumb_marker'] = '';
                $category_thumb[$i]['width'] = $this->config->item('category_thumb_width');
                //$category_thumb[$i]['height'] = $this->config->item('category_thumb_height');
                $category_thumb[$i]['height'] = 2;
                $category_thumb[$i]['master_dim'] = 'width';
                $category_thumb[$i]['quality'] = "100%";
                $category_thumb[$i]['x_axis'] = '0';
                $category_thumb[$i]['y_axis'] = '0';
                $instanse = "image_$i";

                //Loading Image Library
                $this->load->library('image_lib', $category_thumb[$i], $instanse);
                $dataimage = $response['result'][$i]['file_name'];

                //Creating Thumbnail
                $this->$instanse->resize();
                $response['error'][] = $thumberror = $this->$instanse->display_errors();
            }
        }
//        echo "<pre>";
//        print_r($response);
//        die();
        return $response;
    }

    public function delete_image($id = '', $categoryimage_id = '') {
        if ($categoryimage_id != '' && $id != '') {

            $get_image = $this->common_model->select_data_by_id('category_banner', 'id', $categoryimage_id, $data = 'image', $join_str = array());
            $image_name = $get_image[0]['image'];

            $main_image = $this->config->item('category_main_upload_path') . $image_name;
            $thumb_image = $this->config->item('category_thumb_upload_path') . $image_name;

            if (file_exists($main_image)) {
                unlink($main_image);
            }
            if (file_exists($thumb_image)) {
                unlink($thumb_image);
            }
            $this->common_model->delete_data('category_banner', 'id', $categoryimage_id);
            redirect('category/edit/' . $id, 'refresh');
        }
    }

    // delete page
    public function delete($id = '') {

        $update_data = array('is_deleted' => 1);
        $update_result = $this->common_model->update_data($update_data, 'product', 'id', $id);

        if ($update_result) {
            $this->session->set_flashdata('success', 'Product deleted successfully.');
            redirect('admin/product');
        } else {
            $this->session->set_flashdata('error', 'Something went wrong! Try Again.');
            redirect('admin/product');
        }
    }

    public function checkunique() {
        $valid = 'true';
        $id = $this->input->post('id');
        $slug = $this->input->post('slug');
        $contition_array['is_deleted'] = 0;
        $contition_array['slug'] = $slug;
        if (!empty($id)) {
            $contition_array['id !='] = $id;
        }
        $productData = $this->common_model->select_data_by_condition('product', $contition_array, '*', $short_by = '', $order_by = '', $limit = '', $offset = '');
        if (!empty($productData)) {
            $valid = 'false';
        }
        echo $valid;
        exit;
    }

    public function checkuniqueisbn() {
        $valid = 'true';
        $id = $this->input->post('id');
        $isbn_no = $this->input->post('isbn_no');
        $contition_array['is_deleted'] = 0;
        $contition_array['isbn_no'] = $isbn_no;
        if (!empty($id)) {
            $contition_array['id !='] = $id;
        }
        $productData = $this->common_model->select_data_by_condition('product_variation', $contition_array, '*', $short_by = '', $order_by = '', $limit = '', $offset = '');
        if (!empty($productData)) {
            $valid = 'false';
        }
        echo $valid;
        exit;
    }

    public function checkuniquesku() {
        $valid = 'true';
        $id = $this->input->post('id');
        $sku = $this->input->post('sku');
        $contition_array['is_deleted'] = 0;
        $contition_array['sku'] = $sku;
        if (!empty($id)) {
            $contition_array['id !='] = $id;
        }
        $productData = $this->common_model->select_data_by_condition('product_variation', $contition_array, '*', $short_by = '', $order_by = '', $limit = '', $offset = '');
        if (!empty($productData)) {
            $valid = 'false';
        }
        echo $valid;
        exit;
    }

    // view form post detail
    public function view($id = '') {

        // $condition_array = array('status' => '1', 'is_deleted' => '0', 'users.id' => $id);
        $condition_array = array('form_posts.id' => $id);

        $forms_detail = $this->common_model->select_data_by_condition('form_post', $condition_array, 'form_post.*', $short_by = '', $order_by = '', $limit = '', $offset = '', $join_str = array(), $group_by = '');


        if (!empty($users_detail)) {
            $this->data['module_name'] = 'Manage Form Data';
            $this->data['section_title'] = 'View Form Data';
            $this->data['form_detail'] = $forms_detail;

            /* Load Template */
            $this->template->admin_render('admin/formdata/view', $this->data);
        } else {
            $this->session->set_flashdata('error', 'Something went wrong! Try Again.');
            redirect('admin/formdata');
        }
    }

    public function exportExcel($cid = 0, $did = 0) {
        $contition_array = array('is_deleted' => '0');
        $data = '*';
        $product_list = $this->common_model->select_data_by_condition('product', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
        include FCPATH . 'PHPExcel/IOFactory.php';
        include FCPATH . 'PHPExcel/PHPExcel.php';
        $fileName = 'Product-' . time() . '.xlsx';
        $alphas = range('A', 'Z');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        // FIRST RAW START

        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'id');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'name');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', "slug");
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', "category_id");
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', "collection");
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', "author_name");
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', "description");
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', "image");
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', "tags");
        $objPHPExcel->getActiveSheet()->SetCellValue('J1', "best_seller");
        $objPHPExcel->getActiveSheet()->SetCellValue('K1', "item_of_the_month");
        $objPHPExcel->getActiveSheet()->SetCellValue('L1', "language");
        $objPHPExcel->getActiveSheet()->SetCellValue('M1', "suggested_book");
        $objPHPExcel->getActiveSheet()->SetCellValue('N1', "paperback_sku");
        $objPHPExcel->getActiveSheet()->SetCellValue('O1', "paperback_isbn_no");
        $objPHPExcel->getActiveSheet()->SetCellValue('P1', "paperback_price");
        $objPHPExcel->getActiveSheet()->SetCellValue('Q1', "paperback_weight");
        $objPHPExcel->getActiveSheet()->SetCellValue('R1', "paperback_is_default");
        $objPHPExcel->getActiveSheet()->SetCellValue('S1', "hardcover_sku");
        $objPHPExcel->getActiveSheet()->SetCellValue('T1', "hardcover_isbn_no");
        $objPHPExcel->getActiveSheet()->SetCellValue('U1', "hardcover_price");
        $objPHPExcel->getActiveSheet()->SetCellValue('V1', "hardcover_weight");
        $objPHPExcel->getActiveSheet()->SetCellValue('W1', "hardcover_is_default");
        $objPHPExcel->getActiveSheet()->SetCellValue('X1', "E-book_sku");
        $objPHPExcel->getActiveSheet()->SetCellValue('Y1', "E-book_isbn_no");
        $objPHPExcel->getActiveSheet()->SetCellValue('Z1', "E-book_price");
        $objPHPExcel->getActiveSheet()->SetCellValue('AA1', "E-book_weight");
        $objPHPExcel->getActiveSheet()->SetCellValue('AB1', "E-book_is_default");
        $objPHPExcel->getActiveSheet()->SetCellValue('AC1', "E-book_download_book");
        $objPHPExcel->getActiveSheet()->SetCellValue('AD1', "status");
        $objPHPExcel->getActiveSheet()->SetCellValue('AE1', "priority");
        // SECOND RAW START
        if (count($product_list) > 0) {
            foreach ($product_list as $pkey => $pval) {
                $pkey = $pkey + 2;
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $pkey, $pval['id']);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $pkey, $pval['name']);
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $pkey, $pval['slug']);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $pkey, $pval['category_id']);
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . $pkey, $pval['collection']);
                $objPHPExcel->getActiveSheet()->SetCellValue('F' . $pkey, $pval['author_name']);
                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $pkey, $pval['description']);
                $objPHPExcel->getActiveSheet()->SetCellValue('H' . $pkey, $pval['image']);
                $objPHPExcel->getActiveSheet()->SetCellValue('I' . $pkey, $pval['tags']);
                $objPHPExcel->getActiveSheet()->SetCellValue('J' . $pkey, $pval['best_seller']);
                $objPHPExcel->getActiveSheet()->SetCellValue('K' . $pkey, $pval['item_of_the_month']);
                $objPHPExcel->getActiveSheet()->SetCellValue('L' . $pkey, $pval['language']);
                $objPHPExcel->getActiveSheet()->SetCellValue('M' . $pkey, $pval['suggest_book']);

                $contition_array = array('product_id' => $pval['id']);
                $data = '*';
                $product_variation = $this->common_model->select_data_by_condition('product_variation', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

                $paperbackData_sku = '';
                $paperbackData_isbn_no = '';
                $paperbackData_price = '';
                $paperbackData_weight = '';
                $paperbackData_is_default = '';
                $paperbackData_download_book = '';

                $hardcoverData_sku = '';
                $hardcoverData_isbn_no = '';
                $hardcoverData_price = '';
                $hardcoverData_weight = '';
                $hardcoverData_is_default = '';
                $hardcoverData_download_book = '';

                $eBookData_sku = '';
                $eBookData_isbn_no = '';
                $eBookData_price = '';
                $eBookData_weight = '';
                $eBookData_is_default = '';
                $eBookData_download_book = '';

                foreach ($product_variation as $variation) {
                    if ($variation['attribute_id'] == 1) {
                        $paperbackData_sku = $variation['sku'];
                        $paperbackData_isbn_no = $variation['isbn_no'];
                        $paperbackData_price = $variation['price'];
                        $paperbackData_weight = $variation['weight'];
                        $paperbackData_is_default = $variation['is_default'];
                        $paperbackData_download_book = $variation['download_book'];
                    }

                    if ($variation['attribute_id'] == 2) {
                        $hardcoverData_sku = $variation['sku'];
                        $hardcoverData_isbn_no = $variation['isbn_no'];
                        $hardcoverData_price = $variation['price'];
                        $hardcoverData_weight = $variation['weight'];
                        $hardcoverData_is_default = $variation['is_default'];
                        $hardcoverData_download_book = $variation['download_book'];
                    }

                    if ($variation['attribute_id'] == 3) {
                        $eBookData_sku = $variation['sku'];
                        $eBookData_isbn_no = $variation['isbn_no'];
                        $eBookData_price = $variation['price'];
                        $eBookData_weight = $variation['weight'];
                        $eBookData_is_default = $variation['is_default'];
                        $eBookData_download_book = $variation['download_book'];
                    }
                }

                $objPHPExcel->getActiveSheet()->SetCellValue('N' . $pkey, $paperbackData_sku);
                $objPHPExcel->getActiveSheet()->SetCellValue('O' . $pkey, $paperbackData_isbn_no);
                $objPHPExcel->getActiveSheet()->SetCellValue('P' . $pkey, $paperbackData_price);
                $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $pkey, $paperbackData_weight);
                $objPHPExcel->getActiveSheet()->SetCellValue('R' . $pkey, $paperbackData_is_default);
                $objPHPExcel->getActiveSheet()->SetCellValue('S' . $pkey, $hardcoverData_sku);
                $objPHPExcel->getActiveSheet()->SetCellValue('T' . $pkey, $hardcoverData_isbn_no);
                $objPHPExcel->getActiveSheet()->SetCellValue('U' . $pkey, $hardcoverData_price);
                $objPHPExcel->getActiveSheet()->SetCellValue('V' . $pkey, $hardcoverData_weight);
                $objPHPExcel->getActiveSheet()->SetCellValue('W' . $pkey, $hardcoverData_is_default);
                $objPHPExcel->getActiveSheet()->SetCellValue('X' . $pkey, $eBookData_sku);
                $objPHPExcel->getActiveSheet()->SetCellValue('Y' . $pkey, $eBookData_isbn_no);
                $objPHPExcel->getActiveSheet()->SetCellValue('Z' . $pkey, $eBookData_price);
                $objPHPExcel->getActiveSheet()->SetCellValue('AA' . $pkey, $eBookData_weight);
                $objPHPExcel->getActiveSheet()->SetCellValue('AB' . $pkey, $eBookData_is_default);
                $objPHPExcel->getActiveSheet()->SetCellValue('AC' . $pkey, $eBookData_download_book);
                $objPHPExcel->getActiveSheet()->SetCellValue('AD' . $pkey, $pval['status']);
                $objPHPExcel->getActiveSheet()->SetCellValue('AE' . $pkey, $pval['priority']);
            }
        }

        $object_excel_writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); // Explain format of Excel data
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=Products-' . time() . '.xls');
        $object_excel_writer->save('php://output');

        //$objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
        /*
          try {
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          $objWriter->save(FCPATH .'uploads/export_excel/'. $fileName);
          chmod(FCPATH .'uploads/export_excel/'. $fileName, 0644);
          } catch (Exception $e) {
          die('Error loading file "' . pathinfo($fileName, PATHINFO_BASENAME)
          . '": ' . $e->getMessage());
          }
         */
        // download file
        //header("Content-Type: application/vnd.ms-excel");
        //redirect(base_url() .'uploads/export_excel/'. $fileName);
    }

    public function import_product($id = null) {
        if ($this->input->post('import_product')) {
            if ($_FILES["sheet"]['name'] == '' || $_FILES["sheet"]['error'] > 0) {
                $this->session->set_flashdata('error', 'Please select file');
                redirect('admin/product', 'refresh');
            }
            $new_name = date('Y-m-d-H-i-s-') . $_FILES["sheet"]['name'];

            $uploadConfig['file_name'] = $new_name;
            $uploadConfig['upload_path'] = $this->config->item('import_main_upload_path');
            $uploadConfig['allowed_types'] = $this->config->item('import_allowed_types');
            $uploadConfig['remove_spaces'] = TRUE;

            //$this->upload->initialize($uploadConfig);
            $this->load->library('upload', $uploadConfig);
            if (!$this->upload->do_upload('sheet')) {
                $this->session->set_flashdata('error', $this->upload->display_errors());
                redirect('admin/product', 'refresh');
            } else {
                $data = array('upload_data' => $this->upload->data());
            }

            if (!empty($data['upload_data']['file_name'])) {
                $import_xls_file = $data['upload_data']['file_name'];
            } else {
                $import_xls_file = 0;
            }
            $inputFileName = $this->config->item('import_main_upload_path') . $import_xls_file;

            include FCPATH . 'PHPExcel/IOFactory.php';
            include FCPATH . 'PHPExcel/PHPExcel.php';

            try {
                $inputFileType = \PHPExcel_IOFactory::identify($inputFileName);
                $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch (Exception $e) {
                die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                        . '": ' . $e->getMessage());
            }
            //$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

            $sheet = $objPHPExcel->getSheet(0);

            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
            $maxCell = $sheet->getHighestRowAndColumn();
            $rowData = $sheet->rangeToArray('A2:' . $maxCell['column'] . $maxCell['row']);

            $inserted_data_count = 0;
            $updated_data_count = 0;
            foreach ($rowData as $row) {
                if (is_numeric($row[0])) {
                    $condition_array = array('id' => $row[0]);
                    $data = '*';
                    $product_data = $this->common_model->select_data_by_condition('product', $condition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
                    if (!empty($product_data)) {
                        $update_array = array();
                        $update_array['name'] = strip_tags($row[1]);
                        $update_array['slug'] = $this->seo_friendly_slug($row[2]);
                        $update_array['category_id'] = trim($row[3], ',');
                        $update_array['collection'] = $row[4];
                        $update_array['author_name'] = $row[5];
                        $update_array['description'] = $row[6];
                        $update_array['image'] = '/assets/uploads/images/' . $row[7];
                        $update_array['tags'] = $row[8];
                        $update_array['best_seller'] = (is_numeric($row[9]) ? $row[9] : 0);
                        $update_array['item_of_the_month'] = (is_numeric($row[10]) ? $row[10] : 0);
                        $update_array['language'] = $row[11];
                        $update_array['suggest_book'] = $row[12];
                        $update_array['modify_date'] = date('Y-m-d H:i:s');
                        $update_array['status'] = (is_numeric($row[29]) ? $row[29] : 0);
                        $update_array['priority'] = (is_numeric($row[30]) ? $row[30] : 3);

                        $update_result = $this->common_model->update_data($update_array, 'product', 'id', $row[0]);
                        $updated_data_count++;
                        $aa = 12;
                    }
                    $product_id = $row[0];
                } else {
                    $insert_data = array();
                    $insert_data['name'] = strip_tags($row[0]);
                    $insert_data['slug'] = $this->seo_friendly_slug($row[1]);
                    $insert_data['category_id'] = trim($row[2], ',');
                    $insert_data['collection'] = $row[3];
                    $insert_data['author_name'] = $row[4];
                    $insert_data['description'] = $row[5];
                    $insert_data['image'] = '/assets/uploads/images/' . $row[6];
                    $insert_data['tags'] = $row[7];
                    $insert_data['best_seller'] = (is_numeric($row[8]) ? $row[8] : 0);
                    $insert_data['item_of_the_month'] = (is_numeric($row[9]) ? $row[9] : 0);
                    $insert_data['language'] = $row[10];
                    $insert_data['suggest_book'] = $row[11];
                    $insert_data['created_date'] = date('Y-m-d H:i:s');
                    $insert_data['modify_date'] = date('Y-m-d H:i:s');
                    $insert_data['status'] = (is_numeric($row[28]) ? $row[28] : 0);
                    $insert_data['priority'] = (is_numeric($row[29]) ? $row[29] : 3);
                    $insert_data['is_deleted'] = 0;
                    $inserted_data_count++;
                    $product_id = $insertData = $this->common_model->insert_data_get_id($insert_data, 'product');

                    $aa = 11;
                }

                if (isset($aa)) {
                    if (!empty($row[$aa + 1]) && !empty($row[$aa + 3])) {
                        $paperbackData = array();
                        $paperbackData['sku'] = $row[$aa + 1];
                        $paperbackData['isbn_no'] = $row[$aa + 2];
                        $paperbackData['price'] = $row[$aa + 3];
                        $paperbackData['weight'] = $row[$aa + 4];
                        $paperbackData['is_default'] = (is_numeric($row[$aa + 5]) ? $row[$aa + 5] : 0);
                        $paperbackData['download_book'] = '';

                        $this->importAttributeData($product_id, 1, $paperbackData);
                    }
                    if (!empty($row[$aa + 6]) && !empty($row[$aa + 8])) {
                        $hardcoverData = array();
                        $hardcoverData['sku'] = $row[$aa + 6];
                        $hardcoverData['isbn_no'] = $row[$aa + 7];
                        $hardcoverData['price'] = $row[$aa + 8];
                        $hardcoverData['weight'] = $row[$aa + 9];
                        $hardcoverData['is_default'] = (is_numeric($row[$aa + 10]) ? $row[$aa + 10] : 0);
                        $hardcoverData['download_book'] = '';

                        $this->importAttributeData($product_id, 2, $hardcoverData);
                    }
                    if (!empty($row[$aa + 11]) && !empty($row[$aa + 13])) {
                        $eBookData = array();
                        $eBookData['sku'] = $row[$aa + 11];
                        $eBookData['isbn_no'] = $row[$aa + 12];
                        $eBookData['price'] = $row[$aa + 13];
                        $eBookData['weight'] = $row[$aa + 14];
                        $eBookData['is_default'] = (is_numeric($row[$aa + 15]) ? $row[$aa + 15] : 0);
                        $eBookData['download_book'] = ($row[$aa + 16]) ? '/assets/uploads/files/' . $row[$aa + 16] : '';
                        $this->importAttributeData($product_id, 3, $eBookData);
                    }
                }
            }
            $this->session->set_flashdata('success', 'Product successfully imported. ' . $inserted_data_count . ' product inserted. ' . $updated_data_count . ' product updated.');
            redirect('admin/product', 'refresh');
        }
    }

    public function importAttributeData($productId, $attributeId, $variationData) {
        $condition_array = array('product_id' => $productId, 'attribute_id' => $attributeId);
        $data = 'id';
        $variation_data = $this->common_model->select_data_by_condition('product_variation', $condition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

        if (!empty($variation_data) && $variation_data[0]['id'] > 0) {
            $update_array = array();
            $update_array['sku'] = $variationData['sku'];
            $update_array['isbn_no'] = $variationData['isbn_no'];
            $update_array['weight'] = $variationData['weight'];
            $update_array['price'] = $variationData['price'];
            $update_array['download_book'] = $variationData['download_book'];
            $update_array['is_default'] = $variationData['is_default'];

            $update_variation = $this->common_model->update_data($update_array, 'product_variation', 'id', $variation_data[0]['id']);
        } else {
            $insert_data = array();
            $insert_data['product_id'] = $productId;
            $insert_data['attribute_id'] = $attributeId;
            $insert_data['sku'] = $variationData['sku'];
            $insert_data['isbn_no'] = $variationData['isbn_no'];
            $insert_data['weight'] = $variationData['weight'];
            $insert_data['price'] = $variationData['price'];
            $insert_data['download_book'] = $variationData['download_book'];
            $insert_data['is_default'] = $variationData['is_default'];

            $insert_variation = $this->common_model->insert_data_get_id($insert_data, 'product_variation');
        }
    }

    public function getAttributeHtml() {
        $attribute = $this->input->post('attribute');
        $product_id = $this->input->post('product_id');

        if (!empty($product_id)) {
            $condition_array = array('attribute.status' => 1);
            $search_condition = 'attribute.id IN(' . $attribute . ')';
            $data = 'attribute.*';

            $attributeList = $this->common_model->select_data_by_search('attribute', $search_condition, $condition_array, $data, $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $custom_order_by = '', $group_by = '');

            $attributeListData = array();
            foreach ($attributeList as $attributes) {
                $return = array();
                $return = $attributes;

                $condition_array = array('product_id' => $product_id, 'attribute_id' => $attributes['id']);
                $data = '*';
                $variation_data = $this->common_model->select_data_by_condition('product_variation', $condition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
                if (!empty($variation_data)) {
                    $return['sku'] = $variation_data[0]['sku'];
                    $return['isbn_no'] = $variation_data[0]['isbn_no'];
                    $return['price'] = $variation_data[0]['price'];
                    $return['weight'] = $variation_data[0]['weight'];
                    $return['download_book'] = $variation_data[0]['download_book'];
                    $return['is_default'] = $variation_data[0]['is_default'];
                } else {
                    $return['sku'] = '';
                    $return['isbn_no'] = '';
                    $return['price'] = '';
                    $return['weight'] = '';
                    $return['download_book'] = '';
                    $return['is_default'] = '';
                }
                array_push($attributeListData, $return);
            }
            //pv.sku,pv.isbn_no,pv.price,pv.download_book, pv.is_default
            $this->data['attribute_list'] = $attributeListData;
        } else {
            $condition_array = array('attribute.status' => 1);
            $search_condition = 'attribute.id IN(' . $attribute . ')';
            $data = '*';
            $this->data['attribute_list'] = $attributeList = $this->common_model->select_data_by_search('attribute', $search_condition, $condition_array, $data, $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = '', $custom_order_by = '', $group_by = '');
        }


        return $this->load->view('admin/product/_attribute_fields', $this->data);
    }

    public function seo_friendly_slug($string) {
        $string = str_replace(array('[\', \']'), '', $string);
        $string = preg_replace('/\[.*\]/U', '', $string);
        $string = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', '-', $string);
        $string = htmlentities($string, ENT_COMPAT, 'utf-8');
        $string = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string);
        $string = preg_replace(array('/[^a-z0-9]/i', '/[-]+/'), '-', $string);
        return strtolower(trim($string, '-'));
    }

}

?>
