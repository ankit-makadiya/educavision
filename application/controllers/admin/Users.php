<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users extends Admin_Controller {

    public $data;

    public function __construct() {

        parent::__construct();

        if (!$this->session->userdata('sl_admin')) {
            redirect('admin/login');
        }

        //site setting details
        $this->load->model('admin/common_model');

        $site_name_values = $this->common_model->select_data_by_id('settings', 'setting_id', '1', '*');
        $this->data['site_name'] = $site_name = $site_name_values[0]['setting_val'];
        $this->data['title'] = 'Users | ' . $site_name;

        //remove catch so after logout cannot view last visited page if that page is this
        $this->output->set_header('Last-Modified:' . gmdate('D, d M Y H:i:s') . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
        $this->output->set_header('Pragma: no-cache');
    }

    // display userss list
    public function index() {

        $this->data['module_name'] = 'User Management';
        $this->data['section_title'] = 'Users';

        $contition_array = array('is_deleted' => 0);
        $this->data['users_list'] = $this->common_model->select_data_by_condition('admin', $contition_array, '*', $short_by = 'id', $order_by = 'DESC', $limit = '', $offset = '', $join_str = array(), $group_by = '');

        /* Load Template */
        $this->template->admin_render('admin/users/index', $this->data);
    }

    // users status change
    public function change_status($id = '', $status = '') {
        if ($id == '' || $status == '') {
            $this->session->set_flashdata('error', 'Something went wrong! Try Again.');
            redirect('admin/users');
        }
        if ($status == 1) {
            $status = 0;
        } else {
            $status = 1;
        }
        $update_data = array('status' => $status);

        $update_result = $this->common_model->update_data($update_data, 'admin', 'id', $id);

        if ($update_result) {
            $this->session->set_flashdata('success', 'User\'s status successfully updated.');
            redirect('admin/users');
        } else {
            $this->session->set_flashdata('error', 'Something went wrong! Try Again.');
            redirect('admin/users');
        }
    }

    // add new users
    public function add() {
        // check post and save data
        if ($this->input->post('btn_save')) {

            // form validation
            $this->form_validation->set_rules('username', 'User Name', 'trim|required');
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'trim|required');
            
            if ($this->form_validation->run() === FALSE) {
                $this->session->set_flashdata('error', validation_errors());
                redirect('admin/users');
            } else {
                // email validation
                $condition_array = array('is_deleted' => 0);
                $is_available = $this->common_model->check_unique_avalibility('admin', 'email', $this->input->post('email'), '', '', $condition_array);

                if (!$is_available) {
                    $this->session->set_flashdata('error', 'An account for the specified email address already exists. Try another email address.');
                    redirect('admin/users');
                }

                // generate random password
                $password = $this->common_model->randomPassword();

                $userData = array(
                    'username' => strip_tags($this->input->post('username')),
                    'name' => strip_tags($this->input->post('name')),
                    'email' => strip_tags($this->input->post('email')),
                    'role_id' => strip_tags($this->input->post('role')),
                    'password' => md5(strip_tags($this->input->post('password'))),
                    'created_on' => date('Y-m-d H:i:s'),
                    'active' => '1',
                    'is_deleted' => '0',
                );

                $insert_result = $this->common_model->insert_data_get_id($userData, 'admin');

                if ($insert_result) {
                    $this->session->set_flashdata('success', 'User\'s added successfully.');
                    redirect('admin/users');
                } else {
                    $this->session->set_flashdata('error', 'Something went wrong! Try Again.');
                    redirect('admin/users');
                }
            }
        }
        $condition_array = array('status' => 1, 'is_deleted'=>0);
        
        $role_data = $this->common_model->select_data_by_condition('role', $condition_array, '*', $short_by = '', $order_by = '', $limit = '', $offset = '', $join_str = array(), $group_by = '');

        $this->data['role_data'] = $role_data;

        $this->data['module_name'] = 'Manage User Management';
        $this->data['section_title'] = 'Add User';

        /* Load Template */
        $this->template->admin_render('admin/users/add', $this->data);
    }

    // update the users detail
    public function edit($id = '') {

        if ($this->input->post('id')) {
            $id = $this->input->post('id');

            // Form Validation
            $users_detail = $this->common_model->select_data_by_id('admin', 'id', $id, 'email');

            if ($this->input->post('email') != $users_detail[0]['email']) {
                $is_unique = '|is_unique[users.email]';
            } else {
                $is_unique = '';
            }

            $this->form_validation->set_rules('username', 'User Name', 'trim|required');
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email' . $is_unique);

            if ($this->form_validation->run() === FALSE) {
                $this->session->set_flashdata('error', validation_errors());
                redirect('admin/users');
            }

            $update_array['username'] = strip_tags($this->input->post('username'));
            $update_array['name'] = strip_tags($this->input->post('name'));
            $update_array['email'] = strip_tags($this->input->post('email'));
            $update_array['role_id'] = strip_tags($this->input->post('role'));
            
            // Change Password
            if (!empty($this->input->post('reset_password'))) {
                $new_password = $this->input->post('reset_password');
                $update_array['password'] = md5($new_password);
            }

            $update_result = $this->common_model->update_data($update_array, 'admin', 'id', $id);

            if ($update_result) {
                $this->session->set_flashdata('success', 'User info updated successfully.');
                redirect('admin/users');
            } else {
                $this->session->set_flashdata('error', 'Something went wrong! Try Again.');
                redirect('admin/users');
            }
        }

        $condition_array = array('admin.id' => $id);
        
        $users_detail = $this->common_model->select_data_by_condition('admin', $condition_array, 'admin.*', $short_by = '', $order_by = '', $limit = '', $offset = '', $join_str = array(), $group_by = '');

        
        if (!empty($users_detail)) {

            $condition_array = array('status' => 1, 'is_deleted'=>0);
        
            $role_data = $this->common_model->select_data_by_condition('role', $condition_array, '*', $short_by = '', $order_by = '', $limit = '', $offset = '', $join_str = array(), $group_by = '');


            $this->data['module_name'] = 'Manage User Management';
            $this->data['section_title'] = 'Edit User';
            $this->data['user_detail'] = $users_detail;
            $this->data['role_data'] = $role_data;

            /* Load Template */
            $this->template->admin_render('admin/users/edit', $this->data);
        } else {
            $this->session->set_flashdata('error', 'Something went wrong! Try Again.');
            redirect('admin/users');
        }
    }

    // view users detail
    public function view($id = '') {

        // $condition_array = array('status' => '1', 'is_deleted' => '0', 'users.id' => $id);
        $condition_array = array('admin.id' => $id);
        $users_detail = $this->common_model->select_data_by_condition('admin', $condition_array, 'admin.*', $short_by = '', $order_by = '', $limit = '', $offset = '', $join_str = array(), $group_by = '');

        if (!empty($users_detail)) {
            $this->data['module_name'] = 'Manage Users';
            $this->data['section_title'] = 'View User';
            $this->data['user_detail'] = $users_detail;

            /* Load Template */
            $this->template->admin_render('admin/users/view', $this->data);
        } else {
            $this->session->set_flashdata('error', 'Something went wrong! Try Again.');
            redirect('admin/users');
        }
    }

    // users delete
    public function delete($id = '') {

        $update_data = array('is_deleted' => 1);
        $update_result = $this->common_model->update_data($update_data, 'admin', 'id', $id);

        if ($update_result) {
            $this->session->set_flashdata('success', 'User is_deleted successfully.');
            redirect('admin/users');
        } else {
            $this->session->set_flashdata('error', 'Something went wrong! Try Again.');
            redirect('admin/users');
        }
    }

    // remove photo
    public function remove_photo($id = '') {
        if ($id != '') {

            $get_image = $this->common_model->select_data_by_id('admin', 'id', $id, $data = 'photo', $join_str = array());
            $image_name = $get_image[0]['photo'];

            $main_image = $this->config->item('user_main_upload_path') . $image_name;
            $thumb_image = $this->config->item('user_thumb_upload_path') . $image_name;

            if (file_exists($main_image)) {
                unlink($main_image);
            }

            if (file_exists($thumb_image)) {
                unlink($thumb_image);
            }

            $update_array['photo'] = '';
            $update_result = $this->common_model->update_data($update_array, 'admin', 'id', $id);

            if ($update_result) {
                $this->session->set_flashdata('success', 'Photo successfully removed.');
                redirect('admin/users/edit/' . $id);
            } else {
                $this->session->set_flashdata('error', 'Something went wrong! Try Again.');
                redirect('admin/users');
            }
        }
    }

	// display users role list
	public function role() {
    	$this->data['module_name'] = 'Users Management';
		$this->data['section_title'] = 'Users Role';

		$condition_array = array('is_deleted' => 0);
		$this->data['role_list'] = $this->common_model->select_data_by_condition('role', $condition_array, '*', $short_by = 'id', $order_by = 'DESC', $limit = '', $offset = '', $join_str = array(), $group_by = '');

		/* Load Template */
		$this->template->admin_render('admin/role/index', $this->data);
	}

	// add new users role add
	public function roleAdd() {
		// check post and save data
		if ($this->input->post('btn_save')) {
			// form validation
			$this->form_validation->set_rules('name', 'Role Title', 'trim|required');

			if ($this->form_validation->run() === FALSE) {
				$this->session->set_flashdata('error', validation_errors());
				redirect('admin/role');
			} else {
				$roleData = array(
					'name' => strip_tags($this->input->post('name')),
					'permissions' => implode(',',$this->input->post('permissions')),
					'created_date' => date('Y-m-d H:i:s'),
					'modify_date' => date('Y-m-d H:i:s'),
					'status' => '1',
					'is_deleted' => '0',
				);
				$insert_result = $this->common_model->insert_data_get_id($roleData, 'role');
				if ($insert_result) {
					$this->session->set_flashdata('success', 'User\'s role added successfully.');
					redirect('admin/role');
				} else {
					$this->session->set_flashdata('error', 'Something went wrong! Try Again.');
					redirect('admin/role');
				}
			}
		}
		$this->data['module_name'] = 'Manage Users Manager';
		$this->data['section_title'] = 'Add User Role';

		$condition_array = array('is_deleted' => '0');
		$permission = $this->common_model->select_data_by_condition('permissions', $condition_array, 'permissions.*', $short_by = '', $order_by = '', $limit = '', $offset = '', $join_str = array(), $group_by = '');

		$this->data['permissions_data'] = $permission;
		/* Load Template */
		$this->template->admin_render('admin/role/add', $this->data);
	}

	// update the users detail
	public function roleEdit($id = '') {
		if ($this->input->post('id')) {
			$id = $this->input->post('id');

			$this->form_validation->set_rules('name', 'Role Title', 'trim|required');

			if ($this->form_validation->run() === FALSE) {
				$this->session->set_flashdata('error', validation_errors());
				redirect('admin/role');
			}
			$update_array['name'] = strip_tags($this->input->post('name'));
			$update_array['permissions'] = implode(',',$this->input->post('permissions'));
			$update_array['modify_date'] = date('Y-m-d H:i:s');
			$update_result = $this->common_model->update_data($update_array, 'role', 'id', $id);

			if ($update_result) {
				$this->session->set_flashdata('success', 'Role info updated successfully.');
				redirect('admin/role');
			} else {
				$this->session->set_flashdata('error', 'Something went wrong! Try Again.');
				redirect('admin/role');
			}
		}
		$condition_array = array('status' => '1', 'is_deleted' => '0', 'role.id' => $id);
		$role_detail = $this->common_model->select_data_by_condition('role', $condition_array, 'role.*', $short_by = '', $order_by = '', $limit = '', $offset = '', $join_str = array(), $group_by = '');
		if (!empty($role_detail)) {
			$condition_array = array('is_deleted' => '0');
			$permission = $this->common_model->select_data_by_condition('permissions', $condition_array, 'permissions.*', $short_by = '', $order_by = '', $limit = '', $offset = '', $join_str = array(), $group_by = '');

			$this->data['module_name'] = 'Manage Users Role';
			$this->data['section_title'] = 'Edit Users Role';
			$this->data['role_detail'] = $role_detail;
			$this->data['permissions_data'] = $permission;

			/* Load Template */
			$this->template->admin_render('admin/role/edit', $this->data);
		} else {
			$this->session->set_flashdata('error', 'Something went wrong! Try Again.');
			redirect('admin/role');
		}
	}

    // display Customers list
    public function customers() {

        $this->data['module_name'] = 'User Management';
        $this->data['section_title'] = 'Customers';

        $contition_array = array('is_deleted' => 0);
        $this->data['users_list'] = $this->common_model->select_data_by_condition('users', $contition_array, '*', $short_by = 'id', $order_by = 'DESC', $limit = '', $offset = '', $join_str = array(), $group_by = '');

        /* Load Template */
        $this->template->admin_render('admin/users/customers', $this->data);
    }
}
