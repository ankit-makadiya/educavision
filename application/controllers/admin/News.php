<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class News extends Admin_Controller {

    public $data;

    public function __construct() {

        parent::__construct();

        if (!$this->session->userdata('sl_admin')) {
            redirect('admin/login');
        }
        $this->load->model('admin/common_model');

        $site_name_values = $this->common_model->select_data_by_id('settings', 'setting_id', '1', '*');
        $this->data['site_name'] = $site_name = $site_name_values[0]['setting_val'];
        $this->data['title'] = 'News | ' . $site_name;

        //remove catch so after logout cannot view last visited page if that page is this
        $this->output->set_header('Last-Modified:' . gmdate('D, d M Y H:i:s') . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
        $this->output->set_header('Pragma: no-cache');
    }

    //display news list
    public function index() {

        $this->data['module_name'] = 'News Management';
        $this->data['section_title'] = 'News Management';

        $contition_array = array('is_deleted' => 0);
        $data = '*';
        $news_list = $this->common_model->select_data_by_condition('news', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

        foreach($news_list as $key => $value){
            $contition_array = array();
            $data = 'count(*) as total';
            $news_comment_data = $this->common_model->select_data_by_condition('news_comment', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

            $news_list[$key]['comment_count'] = $news_comment_data[0]['total'];
        }

        $this->data['news_list'] = $news_list;
        //load news data
        $this->template->admin_render('admin/news/index', $this->data);
    }

    //add the news detail
    public function add() {

        if ($this->input->post()) {
            if ($this->input->post('name') == '') {
                $this->session->set_flashdata('error', 'News name is required');
                redirect('admin/news', 'refresh');
            }
            
            if ($this->input->post('slug') == '') {
                $this->session->set_flashdata('error', 'News slug is required');
                redirect('admin/news', 'refresh');
            }

            if ($this->input->post('description') == '') {
                $this->session->set_flashdata('error', 'News description is required');
                redirect('admin/news', 'refresh');
            }
            if ($this->input->post('news_image') == '') {
                $this->session->set_flashdata('error', 'News image is required');
                redirect('admin/news', 'refresh');
            }
            $insert_data = array();
            $insert_data['name'] = $this->input->post('name');
            $insert_data['slug'] = $this->input->post('slug');
            $insert_data['image'] = $this->input->post('news_image');
            $insert_data['posted_by'] = $this->input->post('posted_by');
            $insert_data['description'] = $this->input->post('description');
            $insert_data['created_date'] = date('Y-m-d H:i:s');
            $insert_data['modify_date'] = date('Y-m-d H:i:s');
            $insert_data['status'] = $this->input->post('status');
            $insert_data['is_deleted'] = 0;

            $insertData = $this->common_model->insert_data_get_id($insert_data, 'news');
            if ($insertData) {
                $this->session->set_flashdata('success', 'News has been sucessfully inserted.');
                redirect('admin/news', 'refresh');
                die();
            } else {
                $this->session->set_flashdata('error', 'oops! something is wrong.');
                redirect('admin/news/add', 'refresh');
                die();
            }
        }
        $this->data['module_name'] = 'News';
        $this->data['section_title'] = 'Add News';

        $this->template->admin_render('admin/news/add', $this->data);
    }

    //update the news detail
    public function edit($id = '') {
        if ($this->input->post('news_id')) {
            $news_id = $this->input->post('news_id');
            if ($this->input->post('name') == '') {
                $this->session->set_flashdata('error', 'News name is required');
                redirect('admin/news', 'refresh');
            }elseif ($this->input->post('slug') == '') {
                $this->session->set_flashdata('error', 'News slug is required');
                redirect('admin/news', 'refresh');
            } elseif ($this->input->post('description') == '') {
                $this->session->set_flashdata('error', 'News description is required');
                redirect('admin/news', 'refresh');
            } elseif ($this->input->post('news_image') == '') {
                $this->session->set_flashdata('error', 'News image is required');
                redirect('admin/news', 'refresh');
            } else {
                $update_array = array();
                $update_array['name'] = $this->input->post('name');
                $update_array['slug'] = $this->input->post('slug');
                $update_array['posted_by'] = $this->input->post('posted_by');
                $update_array['image'] = $this->input->post('news_image');
                $update_array['description'] = $this->input->post('description');
                $update_array['modify_date'] = date('Y-m-d H:i:s');
                $update_array['status'] = $this->input->post('status');

                $update_result = $this->common_model->update_data($update_array, 'news', 'id', $this->input->post('news_id'));

                if ($update_result) {
                    $this->session->set_flashdata('success', 'News successfully updated.');
                    redirect('admin/news', 'refresh');
                } else {
                    $this->session->set_flashdata('error', 'Error Occurred. Try Again!');
                    redirect('admin/news/edit/'.$this->input->post('news_id'), 'refresh');
                }
            }
        }
        $news_detail = $this->common_model->select_data_by_id('news', 'id', $id, '*');

        if (!empty($news_detail)) {
            $this->data['module_name'] = 'News';
            $this->data['section_title'] = 'Edit News';
            $this->data['news_detail'] = $news_detail;
            $this->template->admin_render('admin/news/edit', $this->data);
        } else {
            $this->session->set_flashdata('error', 'Errorout Occurred. Try Again.');
            redirect('admin/news', 'refresh');
        }
    }

    public function news_comments($id){
        $this->data['module_name'] = 'News Management';
        $this->data['section_title'] = 'News Management';

        $contition_array = array('news_id'=>$id);
        $data = '*';
        $news_comment = $this->common_model->select_data_by_condition('news_comment', $contition_array, $data, $short_by = 'id', $order_by = 'DESC', $limit = '', $offset = '');

        $this->data['news_comment'] = $news_comment;
        //load news data
        $this->template->admin_render('admin/news/comments', $this->data);
    }

    public function comments_status_change($id='', $news_id='', $status=''){
        $this->data['module_name'] = 'News Management';
        $this->data['section_title'] = 'News Management';

        $update_array = array();
        $update_array['status'] = ($status) == 1 ? 2 : 1;

        $update_result = $this->common_model->update_data($update_array, 'news_comment', 'id', $id);

        redirect('/admin/news/comments/'.$news_id, 'refresh');
    }
    
    // delete page
    public function delete($id = '') {
        $update_data = array('is_deleted' => 1);
        $update_result = $this->common_model->update_data($update_data, 'news', 'id', $id);

        if ($update_result) {
            $this->session->set_flashdata('success', 'News deleted successfully.');
            redirect('admin/news');
        } else {
            $this->session->set_flashdata('error', 'Something went wrong! Try Again.');
            redirect('admin/news');
        }
    }
    
    public function checkunique() {
        $valid = 'true';
        $id = $this->input->post('id');
        $slug = $this->input->post('slug');
        $contition_array['is_deleted'] = 0;
        $contition_array['slug'] = $slug;
        if (!empty($id)) {
            $contition_array['id !='] = $id;
        }
        $newsData = $this->common_model->select_data_by_condition('news', $contition_array, '*', $short_by = '', $order_by = '', $limit = '', $offset = '');
        if (!empty($newsData)) {
            $valid = 'false';
        }
        echo $valid;
        exit;
    }
}

?>