<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Slider extends Admin_Controller {

    public $data;

    public function __construct() {

        parent::__construct();

        if (!$this->session->userdata('sl_admin')) {
            redirect('admin/login');
        }
        $this->load->model('admin/common_model');

        $site_name_values = $this->common_model->select_data_by_id('settings', 'setting_id', '1', '*');
        $this->data['site_name'] = $site_name = $site_name_values[0]['setting_val'];
        $this->data['title'] = 'Slider | ' . $site_name;

        //remove catch so after logout cannot view last visited page if that page is this
        $this->output->set_header('Last-Modified:' . gmdate('D, d M Y H:i:s') . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
        $this->output->set_header('Pragma: no-cache');
    }

    //display slider list
    public function index() {

        $this->data['module_name'] = 'Slider Management';
        $this->data['section_title'] = 'Slider Management';

        $contition_array = array('is_deleted' => 0);
        $data = '*';
        $slider_list = $this->common_model->select_data_by_condition('slider', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

        $this->data['slider_list'] = $slider_list;
        //load slider data
        $this->template->admin_render('admin/slider/index', $this->data);
    }

    //add the slider detail
    public function add() {

        if ($this->input->post()) {
            if ($this->input->post('name') == '') {
                $this->session->set_flashdata('error', 'Slider name is required');
                redirect('admin/slider', 'refresh');
            }

            $insert_data = array();
            $insert_data['name'] = $this->input->post('name');
            $insert_data['created_date'] = date('Y-m-d H:i:s');
            $insert_data['modify_date'] = date('Y-m-d H:i:s');
            $insert_data['status'] = $this->input->post('status');
            $insert_data['is_deleted'] = 0;

            $insertData = $this->common_model->insert_data_get_id($insert_data, 'slider');

            $commasapareted_images_name = $this->input->post('commasapareted_images_name');
            $slider_title = $this->input->post('slider_title');
            $slider_sort = $this->input->post('slider_sort');
            $slider_title_link = $this->input->post('slider_title_link');
            $slider_target = $this->input->post('slider_target');

            $arr_c_i_n = explode('|', $commasapareted_images_name);

            foreach ($arr_c_i_n as $key => $arr) {
                $insert_slider = array();
                $insert_slider['img_name'] = $arr;
                $insert_slider['slider_id'] = $insertData;
                $insert_slider['sort_order'] = $slider_sort[$key];
                $insert_slider['title'] = $slider_title[$key];
                $insert_slider['link'] = $slider_title_link[$key];
                $insert_slider['link_target'] = $slider_target[$key];
                $insert_slider['is_deleted'] = 0;
                $insertSliderData = $this->common_model->insert_data_get_id($insert_slider, 'slider_image');
            }
            if ($insertData) {
                $this->session->set_flashdata('success', 'Slider has been sucessfully inserted.');
                redirect('admin/slider', 'refresh');
                die();
            } else {
                $this->session->set_flashdata('error', 'oops! something is wrong.');
                redirect('admin/slider/add', 'refresh');
                die();
            }
        }
        $this->data['module_name'] = 'Slider';
        $this->data['section_title'] = 'Add Slider';

        $this->template->admin_render('admin/slider/add', $this->data);
    }

    //update the slider detail
    public function edit($id = '') {
        if ($this->input->post('slider_id')) {
            $slider_id = $this->input->post('slider_id');
            if ($this->input->post('name') == '') {
                $this->session->set_flashdata('error', 'Slider name is required');
                redirect('admin/slider', 'refresh');
            } else {
                $commasapareted_images_name = $this->input->post('commasapareted_images_name');
                $row_count = $this->input->post('row_count');

                $arr_commasapareted_images_name = array();
                if (!empty($commasapareted_images_name)) {
                    $arr_commasapareted_images_name = explode("|", $commasapareted_images_name);
                }
                $update_array = array();
                $update_array['name'] = $this->input->post('name');
                $update_array['modify_date'] = date('Y-m-d H:i:s');
                $update_array['status'] = $this->input->post('status');

                $update_result = $this->common_model->update_data($update_array, 'slider', 'id', $this->input->post('slider_id'));

                if (!empty($arr_commasapareted_images_name)) {
                    $slider_image_id = $this->input->post('slider_image_id');
                    $slider_title = $this->input->post('slider_title');
                    $slider_sort = $this->input->post('slider_sort');
                    $slider_title_link = $this->input->post('slider_title_link');
                    $slider_target = $this->input->post('slider_target');
                    
                    if(!empty($slider_image_id)){
                        $condition_array = array('slider_id' => $slider_id);
                        $search_condition = "id NOT IN(" . implode(',', $slider_image_id) . ")";
                        $data = 'id';
                        $dlt_slider_data = $this->common_model->select_data_by_search('slider_image', $search_condition, $condition_array, $data, $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = '', $custom_order_by = '', $group_by = '');
                    }else{
                        $condition_array = array('slider_id' => $slider_id);
                        $data = 'id';
                        $dlt_slider_data = $this->common_model->select_data_by_condition('slider_image', $condition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');
                    }
                    
                    if (!empty($dlt_slider_data)) {
                        foreach ($dlt_slider_data as $key => $dlt_val) {
                            $update_array = array();
                            $update_array['is_deleted'] = 1;
                            $update_result1 = $this->common_model->update_data($update_array, 'slider_image', 'id', $dlt_val['id']);
                        }
                    }

                    for ($key = 0; $key < count($arr_commasapareted_images_name); $key++) {
                        if (isset($slider_image_id[$key])) {
                            $update_array = array();
                            $update_array['img_name'] = $arr_commasapareted_images_name[$key];
                            $update_array['sort_order'] = $slider_sort[$key];
                            $update_array['title'] = $slider_title[$key];
                            $update_array['link'] = $slider_title_link[$key];
                            $update_array['link_target'] = $slider_target[$key];

                            $update_result1 = $this->common_model->update_data($update_array, 'slider_image', 'id', $slider_image_id[$key]);
                        } else {
                            $insert_slider = array();
                            $insert_slider['img_name'] = $arr_commasapareted_images_name[$key];
                            $insert_slider['slider_id'] = $this->input->post('slider_id');
                            $insert_slider['sort_order'] = $slider_sort[$key];
                            $insert_slider['title'] = $slider_title[$key];
                            $insert_slider['link'] = $slider_title_link[$key];
                            $insert_slider['link_target'] = $slider_target[$key];
                            $insert_slider['is_deleted'] = 0;
                            $insertSliderData = $this->common_model->insert_data_get_id($insert_slider, 'slider_image');
                        }
                    }

                }

                if ($update_result) {
                    $this->session->set_flashdata('success', 'Slider successfully updated.');
                    redirect('admin/slider', 'refresh');
                } else {
                    $this->session->set_flashdata('error', 'Error Occurred. Try Again!');
                    redirect('admin/slider/edit/' . $this->input->post('slider_id'), 'refresh');
                }
            }
        }
        $slider_detail = $this->common_model->select_data_by_id('slider', 'id', $id, '*');

        if (!empty($slider_detail)) {
            $this->data['module_name'] = 'Slider Management';
            $this->data['section_title'] = 'Edit Slider';
            $this->data['slider_detail'] = $slider_detail;

            $contition_array = array('is_deleted' => 0, 'slider_id' => $id);
            $data = '*';
            $slider_img_data = $this->common_model->select_data_by_condition('slider_image', $contition_array, $data, $short_by = '', $order_by = '', $limit = '', $offset = '');

            $slider_img_data_html = '';
            $baseUrl = base_url();
            $commasapareted_images_name = '';
            if (!empty($slider_img_data)) {
                $counter = 0;
                foreach ($slider_img_data as $key => $value) {
                    $slider_title = $value['title'];
                    $img_name = $value['img_name'];
                    $commasapareted_images_name .= $img_name . '|';
                    $img_name = !empty($img_name) ? $img_name : 'noimage.jpg';
                    $path = $baseUrl . $img_name;
                    $slider_imgId = $value['id'];
                    $sort_box = $value['sort_order'];
                    $sort_box = (!empty($sort_box)) ? $sort_box : '';
                    $title = $value['title'];
                    $link = $value['link'];
                    $title = (!empty($title)) ? $title : '';
                    $link = (!empty($link)) ? $link : '';
                    $target_value = $value['link_target'];
                    $target_options = '';
                    $target_select_box = '<select class="form-control "  name="slider_target[]" style="width:130px;" >';
                    foreach ($this->targets as $key => $value) {
                        $selected_target = '';
                        if ($target_value == $value) {
                            $selected_target = 'selected="selected" ';
                        }
                        $target_select_box .= '<option value="' . $value . '" ' . $selected_target . ' >' . $key . '</option>';
                    }
                    $target_select_box .= '</select>';
                    $slider_img_data_html .= '<tr><td class="col-md-1"><input type="hidden" name="slider_image_id[]" value="' . $slider_imgId . '"><img src="' . $path . '" width="66" height="66" /></td><td  align="center" class="col-md-1"><label class="control-label">Title</label><input class="form-control " type="text" name="slider_title[]" value="' . $title . '" style="width:400px;" ></td><td  align="center" class="col-md-1"><label class="control-label">Sort</label><input class="form-control " type="text" name="slider_sort[]" style="width:50px;" value="' . $sort_box . '" ></td><td  align="center" class="col-md-1"><label class="control-label">Hyperlink</label><input class="form-control " type="text" name="slider_title_link[]" value="' . $link . '" style="width:200px;" ><small>e.g:http://www.xyz.com</small></td><td  align="center" class="col-md-1"><label class="control-label">Target</label>' . $target_select_box . '</td><td  class="col-md-1" align="center"><i class="fa fa-trash-o fafont btn_remove " title="remove" style="cursor: pointer;color:#337ab7;line-height:70px !important;" data-img-name="' . $img_name . '" ></td></td></tr>';
                    $counter++;
                }
                $commasapareted_images_name = trim($commasapareted_images_name, '|');
            }

            $hdn_row_count = 0;
            if (!empty($commasapareted_images_name)) {
                $hdn_row_count = count(explode("|", $commasapareted_images_name));
            }

            $this->data['slider_img_data'] = $slider_img_data;
            $this->data['slider_img_data_html'] = $slider_img_data_html;
            $this->data['commasapareted_images_name'] = $commasapareted_images_name;
            $this->data['hdn_row_count'] = $hdn_row_count;

            $this->template->admin_render('admin/slider/edit', $this->data);
        } else {
            $this->session->set_flashdata('error', 'Errorout Occurred. Try Again.');
            redirect('admin/slider', 'refresh');
        }
    }

    public $targets = array(
        'Opens the linked document in a new tab' => '_blank',
        'Open the link in the current frame or window' => '_self',
    );

    // delete page
    public function delete($id = '') {
        $update_data = array('is_deleted' => 1);
        $update_result = $this->common_model->update_data($update_data, 'slider', 'id', $id);

        if ($update_result) {
            $this->session->set_flashdata('success', 'Slider deleted successfully.');
            redirect('admin/slider');
        } else {
            $this->session->set_flashdata('error', 'Something went wrong! Try Again.');
            redirect('admin/slider');
        }
    }

    public function checkunique() {
        $valid = 'true';
        $id = $this->input->post('id');
        $slug = $this->input->post('slug');
        $contition_array['is_deleted'] = 0;
        $contition_array['slug'] = $slug;
        if (!empty($id)) {
            $contition_array['id !='] = $id;
        }
        $sliderData = $this->common_model->select_data_by_condition('slider', $contition_array, '*', $short_by = '', $order_by = '', $limit = '', $offset = '');
        if (!empty($sliderData)) {
            $valid = 'false';
        }
        echo $valid;
        exit;
    }

}

?>