<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Page extends Admin_Controller {

    public $data;

    public function __construct() {

        parent::__construct();

        if (!$this->session->userdata('sl_admin')) {
            redirect('admin/login');
        }

        //site setting details
        $this->load->model('admin/common_model');

        $site_name_values = $this->common_model->select_data_by_id('settings', 'setting_id', '1', '*');
        $this->data['site_name'] = $site_name = $site_name_values[0]['setting_val'];
        $this->data['title'] = 'Pages | ' . $site_name;

        //remove catch so after logout cannot view last visited page if that page is this
        $this->output->set_header('Last-Modified:' . gmdate('D, d M Y H:i:s') . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
        $this->output->set_header('Pragma: no-cache');
    }

    // display clients list
    public function index() {

        $this->data['module_name'] = 'Pages';
        $this->data['section_title'] = 'Manage Pages';

        $contition_array = array('is_deleted' => 0);
        $this->data['pages'] = $this->common_model->select_data_by_condition('pages', $contition_array, '*', $short_by = '', $order_by = '', $limit = '', $offset = '');

        /* Load Template */
        $this->template->admin_render('admin/page/index', $this->data);
    }

    // page status change
    public function change_status($users_id = '', $status = '') {
        if ($users_id == '' || $status == '') {
            $this->session->set_flashdata('error', 'Something went wrong! Try Again.');
            redirect('admin/page');
        }
        if ($status == 1) {
            $status = 0;
        } else {
            $status = 1;
        }
        $update_data = array('status' => $status);

        $update_result = $this->common_model->update_data($update_data, 'pages', 'page_id', $users_id);

        if ($update_result) {
            $this->session->set_flashdata('success', 'Page status successfully updated.');
            redirect('admin/page');
        } else {
            $this->session->set_flashdata('error', 'Something went wrong! Try Again.');
            redirect('admin/page');
        }
    }

    // update the page
    public function add() {
        if ($this->input->post()) {
            // form validation      
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            if (empty($this->input->post('description')) && empty($this->input->post('advance_description'))) {
                $this->form_validation->set_rules('description', 'Description', 'trim|required');
            }
            if ($this->form_validation->run() === FALSE) {
                $this->session->set_flashdata('error', validation_errors());
                redirect('admin/page/add');
            }
            $is_advance = 0;
            $insert_description = '';
            if ($this->input->post('layout_editor_type') == 'advance_editor') {
                $is_advance = 1;
                $insert_advance_description = $this->input->post('advance_description');
                $insert_description = '';
            } else {
                $insert_description = $this->input->post('description');
                $insert_advance_description = '';
            }
            $insert_data = array(
                'name' => $this->input->post('name'),
                'slug' => $this->input->post('slug'),
                'description' => $insert_description,
                'advance_description' => $insert_advance_description,
                'is_advance' => $is_advance,
                'slider_id' => $this->input->post('slider_id'),
                'template' => $this->input->post('template'),
                'created_date' => date('Y-m-d H:i:s'),
                'modify_date' => date('Y-m-d H:i:s'),
                'status' => $this->input->post('status')
            );

            $insertData = $this->common_model->insert_data_get_id($insert_data, 'pages');

            if ($insertData) {
                $this->session->set_flashdata('success', 'Page successfully updated.');
                redirect('admin/page');
            } else {
                $this->session->set_flashdata('error', 'Something went wrong! Please try Again.');
                redirect('admin/page/add');
            }
        }
        $this->data['module_name'] = 'Pages';
        $this->data['section_title'] = 'Add Page';

        $contition_array = array('is_deleted' => 0, 'status' => 1);
        $this->data['sliderData'] = $this->common_model->select_data_by_condition('slider', $contition_array, 'id,name', $short_by = '', $order_by = '', $limit = '', $offset = '');

        /* Load Template */
        $this->template->admin_render('admin/page/add', $this->data);
    }

    // update the page
    public function edit($id = '') {
        if ($this->input->post('id')) {
            // form validation		
            $this->form_validation->set_rules('name', 'Name', 'trim|required');

            if ($this->form_validation->run() === FALSE) {
                $this->session->set_flashdata('error', validation_errors());
                redirect('admin/page');
            }

            $is_advance = 0;
            $update_description = '';
            if ($this->input->post('layout_editor_type') == 'advance_editor') {
                $is_advance = 1;
                $update_advance_description = $this->input->post('advance_description');
                $update_description = '';
            } else {
                $update_description = $this->input->post('description');
                $update_advance_description = '';
            }
            $update_array = array(
                'name' => $this->input->post('name'),
                'slug' => $this->input->post('slug'),
                'description' => $update_description,
                'advance_description' => $update_advance_description,
                'is_advance' => $is_advance,
                'slider_id' => $this->input->post('slider_id'),
                'template' => $this->input->post('template'),
                'modify_date' => date('Y-m-d H:i:s'),
                'status' => $this->input->post('status')
            );

            $update_settings = $this->common_model->update_data($update_array, 'pages', 'id', $this->input->post('id'));

            if ($update_settings) {
                $this->session->set_flashdata('success', 'Page successfully updated.');
                redirect('admin/page');
            } else {
                $this->session->set_flashdata('error', 'Something went wrong! Please try Again.');
                redirect('admin/page/edit/' . $this->input->post('id'));
            }
        }

        $page_info = $this->common_model->select_data_by_id('pages', 'id', $id, '*');

        if (!empty($page_info)) {
            $this->data['module_name'] = 'Pages';
            $this->data['section_title'] = 'Edit Page';
            $this->data['page_info'] = $page_info;

            $contition_array = array('is_deleted' => 0, 'status' => 1);
            $this->data['sliderData'] = $this->common_model->select_data_by_condition('slider', $contition_array, 'id,name', $short_by = '', $order_by = '', $limit = '', $offset = '');

            /* Load Template */
            $this->template->admin_render('admin/page/edit', $this->data);
        } else {
            $this->session->set_flashdata('error', 'Something went wrong! Please try Again.');
            redirect('admin/page');
        }
    }

    // delete page
    public function delete($id = '') {

        $update_data = array('is_deleted' => 1);
        $update_result = $this->common_model->update_data($update_data, 'pages', 'id', $id);

        if ($update_result) {
            $this->session->set_flashdata('success', 'Page deleted successfully.');
            redirect('admin/page');
        } else {
            $this->session->set_flashdata('error', 'Something went wrong! Try Again.');
            redirect('admin/page');
        }
    }

    public function checkunique() {
        $valid = 'true';
        $id = $this->input->post('id');
        $slug = $this->input->post('slug');
        $contition_array['is_deleted'] = 0;
        $contition_array['slug'] = $slug;
        if (!empty($id)) {
            $contition_array['id !='] = $id;
        }
        $pageData = $this->common_model->select_data_by_condition('pages', $contition_array, '*', $short_by = '', $order_by = '', $limit = '', $offset = '');

        if (!empty($pageData)) {
            $valid = 'false';
        }
        echo $valid;
        exit;
    }

}
