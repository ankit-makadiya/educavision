<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}


    // Mail send @Ankit
    function index1($name = 'SomeLounge', $to_email = APP_TEST_TO_EMAIL , $subject = 'Testing HTML Email', $mail_body = '', $cc = '', $from = APP_FROM_EMAIL) {

        //$emailsetting = $this->common->select_data_by_condition('emailsetting', array(), '*');
        //Loading E-mail Class
        $this->load->library('email');

        $this->email->from($from, 'Bhavesh Test');
        $this->email->to($to_email);
       // $this->email->cc($cc);
        $this->email->subject($subject);
        $this->email->set_mailtype("html");
        $this->email->message('<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>404 Page Not Found</title>
<style type="text/css">

::selection { background-color: #E13300; color: white; }
::-moz-selection { background-color: #E13300; color: white; }

body {
	background-color: #fff;
	margin: 40px;
	font: 13px/20px normal Helvetica, Arial, sans-serif;
	color: #4F5155;
}

a {
	color: #003399;
	background-color: transparent;
	font-weight: normal;
}

h1 {
	color: #444;
	background-color: transparent;
	border-bottom: 1px solid #D0D0D0;
	font-size: 19px;
	font-weight: normal;
	margin: 0 0 14px 0;
	padding: 14px 15px 10px 15px;
}

code {
	font-family: Consolas, Monaco, Courier New, Courier, monospace;
	font-size: 12px;
	background-color: #f9f9f9;
	border: 1px solid #D0D0D0;
	color: #002166;
	display: block;
	margin: 14px 0 14px 0;
	padding: 12px 10px 12px 10px;
}

#container {
	margin: 10px;
	border: 1px solid #D0D0D0;
	box-shadow: 0 0 8px #D0D0D0;
}

p {
	margin: 12px 15px 12px 15px;
}
</style>
</head>
<body>
	<div id="container">
		<h1>H1 Tag Sample Text</h1>
		This is test content message
	</div>
</body>
</html>');

        if ($this->email->send()) {
        	echo 'sent';
        	echo $this->email->print_debugger();
            //return true;
        } else {
echo 'not sent';
        	echo $this->email->print_debugger();
            //return FALSE;
        }
    }
}
