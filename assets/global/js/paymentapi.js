function eazzyCheckoutFunction(res) {
    if (res != '') {
        var result = JSON.parse(res);
        EazzyCheckout.configure({token: result.token, amount: result.amount, currency: result.currency, orderRef: result.orderRef, merchantCode: result.merchantCode, outletCode: result.outletCode, payButtonSelector: "#eazzy_pay_action", description: result.description, custName: result.custName, custId: result.custId, ez1_callbackurl: result.ez1_callbackurl, ez2_callbackurl: result.ez2_callbackurl, popupTitle: "Test Title", popupLogo: "https://3.imimg.com/data3/BW/AE/MY-6552227/logo-design-services-250x250.png", popupBtns: ["Confirm Payment", "OK"], popupTheme: JSON.stringify({"logo": "#f6971a", "buttons": "#F49920", "tabs": "#F6981B"}), popupWebsite: "http://abc.com", expiry: "2025-02-17T19:00:00"});
        return true;
    } else {
        return false;
    }
}
$(document).ready(function () {
    if ($('#frminstallmentpay').length > 0) {
        var net_payable_amount = $('#install_net_payable_amount').val();
        $('#amount').val(net_payable_amount);
        $('#frminstallmentpay').bootstrapValidator({feedbackIcons: {valid: 'fa', invalid: 'err', validating: 'fa'}, fields: {'authcard_type': {validators: {notEmpty: {message: 'Please select Card Type'}, }}, 'authcard_number': {validators: {notEmpty: {message: 'Please enter Card Number'}, creditCard: {message: 'Please enter valid Card Number'}, }}, 'authcvv_code': {validators: {notEmpty: {message: 'Please enter CVV Code'}, cvv: {creditCardField: 'authcard_number', message: 'Please enter valid CVV Code'}}}, 'authexp_month': {validators: {notEmpty: {message: 'Please select Month'}, }}, 'authexp_year': {validators: {notEmpty: {message: 'Please select Year'}, }}, 'cyber_type': {validators: {notEmpty: {message: 'Please select Card Type'}, }}, 'cyber_number': {validators: {notEmpty: {message: 'Please enter Card Number'}, creditCard: {message: 'Please enter valid Card Number'}, }}, 'cybercvv_code': {validators: {notEmpty: {message: 'Please enter CVV Code'}, cvv: {creditCardField: 'authcard_number', message: 'Please enter valid CVV Code'}}}, 'cyberexp_month': {validators: {notEmpty: {message: 'Please select Month'}, }}, 'cyberexp_year': {validators: {notEmpty: {message: 'Please select Year'}, }}, 'stripecard_type': {validators: {notEmpty: {message: 'Please select Card Type'}, }}, 'stripecard_number': {validators: {notEmpty: {message: 'Please enter Card Number'}, creditCard: {message: 'Please enter valid Card Number'}, }}, 'stripecard_cvv_code': {validators: {notEmpty: {message: 'Please enter CVV Code'}, cvv: {creditCardField: 'stripecard_number', message: 'Please enter valid CVV Code'}}}, 'stripecardexp_month': {validators: {notEmpty: {message: 'Please select Month'}, }}, 'stripecardexp_year': {validators: {notEmpty: {message: 'Please select Year'}, }}, 'form[is_shipping_calculated]': {excluded: false, validators: {notEmpty: {message: 'Please select shipping option'}, }}, 'panama_card_holder_name': {validators: {notEmpty: {message: 'Please enter Card Holder Name'}, }}, 'panama_card_number': {validators: {notEmpty: {message: 'Please enter Card Number'}, creditCard: {message: 'Please enter valid Card Number'}, }}, 'panama_cvv': {validators: {notEmpty: {message: 'Please enter CVV Code'}, cvv: {creditCardField: 'panama_card_number', message: 'Please enter valid CVV Code'}}}, 'panama_expiry_month': {validators: {notEmpty: {message: 'Please Enter Month'}, }}, 'panama_expiry_year': {validators: {notEmpty: {message: 'Please Enter Year'}, }}, }, submitHandler: function (validator, form, submitButton) {}});
    }
});
$(".paypalstnd").click(function (event) {
    if ($('#frminstallmentpay').length > 0) {
        event.preventDefault();
        event.stopImmediatePropagation();
        var payurl = "";
        var returnurl = "";
        var baseUrl = getBaseUrl();
        $("#item_name").val($("#install_item_name").val());
        $('[name=currency_code]').val($("#install_currency_code").val());
        $("#first_name").val($("#install_first_name").val());
        $("#last_name").val($("#install_last_name").val());
        $("#address1").val($("#install_address1").val());
        $("#address2").val($("#install_address2").val());
        $("#city").val($("#install_cityName").val());
        $("#state").val($("#install_stateName").val());
        $("#zip").val($("#install_zip").val());
        $("#country").val($("#install_countryName").val());
        $("#select_no_installment").val($("#install_select_no_installment").val());
        $("#emailid").val($("#install_email").val());
        $('[name=email]').val($("#install_email").val());
        if ($("#paypal_standard_url").val() != "") {
            payurl = $("#paypal_standard_url").val();
        }
        var mid = $(this).data("id");
        $('#install_btnordpayment').val(mid);
        $('#tax').val($("#install_total_tax").val());
        $.ajax({
            url: '/pos/sales/newinstallmentorder',
            type: 'post',
            data: {'install_enc_order_id': $('#install_enc_order_id').val(), 'install_select_no_installment': $('#install_select_no_installment').val(), 'install_net_payable_amount': $('#install_net_payable_amount').val(), 'install_btnordpayment': $('#install_btnordpayment').val(), },
            beforeSend: function () {},
            success: function (result) {
                if (result != "") {
                    var rs = result.split("||");
                    $("#order_id").val(rs[0]);
                    $("#invoice_id").val(rs[1]);
                    returnurl = baseUrl + '/installmentorderdetails/'+rs[0];
                    $("[name=return]").val(returnurl);
                    document.frminstallmentpay.action = payurl;
                    document.frminstallmentpay.submit();
                }
                return false;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                return false;
            }
        });
    } else {
        if ($('#frmcheckout').bootstrapValidator('validate').has('.has-error').length === 0) {
            event.preventDefault();
            event.stopImmediatePropagation();
            var mid = $(this).data("id");
            var formData = $("#frmcheckout").serializeArray();
            formData.push({name: 'Abandon', value: 'Yes'});
            formData.push({name: 'btnordpayment', value: mid});
            var baseUrl = getBaseUrl();
            var first_name = "";
            var last_name = "";
            var address1 = "";
            var address2 = "";
            var city = "";
            var state = "";
            var zip = "";
            var country = "";
            var emailid = "";
            var payurl = "";
            var returnurl = "";
            if ($('#shipping_countryId').length > 0 &&  $('#shipping_countryId').is(":visible")) {
                if ($("#shipping_firstName").val() != "") {
                    first_name = $("#shipping_firstName").val();
                    $("#first_name").val(first_name);
                }
                if ($("#shipping_lastName").val() != "") {
                    last_name = $("#shipping_lastName").val();
                    $("#last_name").val(last_name);
                }
                if ($("#form_shipping_address1").val() != "") {
                    address1 = $("#form_shipping_address1").val();
                    $("#address1").val(address1);
                }
                if ($("#form_shipping_address2").val() != "") {
                    address2 = $("#form_shipping_address2").val();
                    $("#address2").val(address2);
                }
                if ($("#form_shipping_city").val() != "") {
                    city = $("#form_shipping_city option:selected").text();
                    $("#city").val(city);
                }
                if ($("#form_shipping_stateId").val() != "") {
                    state = $("#form_shipping_stateId option:selected").text();
                    $("#state").val(state);
                }
                if ($("#form_shipping_zipcode").val() != "") {
                    zip = $("#form_shipping_zipcode").val();
                    $("#zip").val(zip);
                }
                if ($("#shipping_countryId").val() != "") {
                    country = $("#shipping_countryId option:selected").text();
                    $("#country").val(country);
                }
                if ($("#shipping_emailid").val() != "") {
                    emailid = $("#shipping_emailid").val();
                    $("#emailid").val(emailid);
                }
            }else{
                if ($("#billing_firstName").val() != "") {
                    first_name = $("#billing_firstName").val();
                    $("#first_name").val(first_name);
                }
                if ($("#billing_lastName").val() != "") {
                    last_name = $("#billing_lastName").val();
                    $("#last_name").val(last_name);
                }
                if ($("#form_billing_address1").val() != "") {
                    address1 = $("#form_billing_address1").val();
                    $("#address1").val(address1);
                }
                if ($("#form_billing_address2").val() != "") {
                    address2 = $("#form_billing_address2").val();
                    $("#address2").val(address2);
                }
                if ($("#form_billing_city").val() != "") {
                    city = $("#form_billing_city option:selected").text();
                    $("#city").val(city);
                }
                if ($("#form_billing_stateId").val() != "") {
                    state = $("#form_billing_stateId option:selected").text();
                    $("#state").val(state);
                }
                if ($("#form_billing_zipcode").val() != "") {
                    zip = $("#form_billing_zipcode").val();
                    $("#zip").val(zip);
                }
                if ($("#billing_countryId").val() != "") {
                    country = $("#billing_countryId option:selected").text();
                    $("#country").val(country);
                }
                if ($("#billing_emailid").val() != "") {
                    emailid = $("#billing_emailid").val();
                    $("#emailid").val(emailid);
                }
            }
            if ($("#paypal_standard_url").val() != "") {
                payurl = $("#paypal_standard_url").val();
            }
            if ($("[name=return]").val() != "") {
                returnurl = $("[name=return]").val();
            }
            tax_amount = $(".c-taxtotal").attr("data-totaltax");
            $("#tax").val(tax_amount);
            $.ajax({
                url: baseUrl + '/pos/sales/neworder',
                type: 'post',
                data: formData,
                beforeSend: function () {},
                success: function (result) {
                    if (result != "") {
                        var rs = result.split("||");
                        $("#order_id").val(rs[0]);
                        $("#invoice_id").val(rs[1]);
                        returnurl = returnurl + '/' + rs[0];
                        $("[name=return]").val(returnurl);
                        document.frmcheckout.action = payurl;
                        document.frmcheckout.submit();
                    }
                    return false;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    return false;
                }
            })
        }    
    }
});
$(".paypalflow").click(function (event) {
    if ($('#frminstallmentpay').length > 0) {
        event.preventDefault();
        event.stopImmediatePropagation();
        var payurl = "";
        var baseUrl = getBaseUrl();
        $("#item_name").val($("#install_item_name").val());
        $('[name=currency_code]').val($("#install_currency_code").val());
        $("#first_name").val($("#install_first_name").val());
        $("#last_name").val($("#install_last_name").val());
        $("#address1").val($("#install_address1").val());
        $("#address2").val($("#install_address2").val());
        $("#city").val($("#install_city").val());
        $("#state").val($("#install_state").val());
        $("#zip").val($("#install_zip").val());
        $("#country").val($("#install_country").val());
        $("#select_no_installment").val($("#install_select_no_installment").val());
        $('[name=email]').val($("#install_email").val());
        if ($("#paypal_standard_url").val() != "") {
            payurl = $("#paypal_standard_url").val();
        }
        var mid = $(this).data("id");
        $('#install_btnordpayment').val(mid);
        $.ajax({
            url: '/pos/sales/newinstallmentorder',
            type: 'post',
            data: {'install_enc_order_id': $('#install_enc_order_id').val(), 'install_select_no_installment': $('#install_select_no_installment').val(), 'install_net_payable_amount': $('#install_net_payable_amount').val(), 'install_btnordpayment': $('#install_btnordpayment').val(), },
            beforeSend: function () {},
            success: function (result) {
                if (result != "") {
                    var rs = result.split("||");
                    $("#order_id").val(rs[0]);
                    $("#invoice_id").val(rs[1]);
                    $.ajax({
                        url: baseUrl + '/product/payflowintegration',
                        type: 'post',
                        data: {'install_enc_order_id': $('#install_enc_order_id').val(), 'install_select_no_installment': $('#install_select_no_installment').val(), 'install_net_payable_amount': $('#install_net_payable_amount').val(), 'install_btnordpayment': $('#install_btnordpayment').val(), 'install_payment_from': 'installment', 'install_invoice': rs[1], },
                        beforeSend: function () {
                            $(".loader").removeClass("hide");
                            $(".loader").addClass('show');
                        },
                        success: function (result) {
                            $(".loader").removeClass("show");
                            $(".loader").addClass('hide');
                            $("#myModal").modal('show');
                            $('#payflowbody').html(result);
                            return false;
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            return false;
                        }
                    });
                } else {
                    $(".loader").removeClass("show");
                    $(".loader").addClass('hide');
                    swal("Error!", "Error", "error");
                    window.location.reload(true);
                }
                return false;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                return false;
            }
        });
    } else {
        if ($('#frmcheckout').bootstrapValidator('validate').has('.has-error').length === 0) {
            event.preventDefault();
            event.stopImmediatePropagation();
            var mid = $(this).data("id");
            var formData = $("#frmcheckout").serializeArray();
            formData.push({name: 'Abandon', value: 'Yes'});
            formData.push({name: 'btnordpayment', value: mid});
            var baseUrl = getBaseUrl();
            var first_name = "";
            var last_name = "";
            var address1 = "";
            var address2 = "";
            var city = "";
            var state = "";
            var zip = "";
            var country = "";
            var emailid = "";
            var payurl = "";

            if ($('#shipping_countryId').length > 0 &&  $('#shipping_countryId').is(":visible")) {
                if ($("#shipping_firstName").val() != "") {
                    first_name = $("#shipping_firstName").val();
                    $("#first_name").val(first_name);
                }
                if ($("#shipping_lastName").val() != "") {
                    last_name = $("#shipping_lastName").val();
                    $("#last_name").val(last_name);
                }
                if ($("#form_shipping_address1").val() != "") {
                    address1 = $("#form_shipping_address1").val();
                    $("#address1").val(address1);
                }
                if ($("#form_shipping_address2").val() != "") {
                    address2 = $("#form_shipping_address2").val();
                    $("#address2").val(address2);
                }
                if ($("#form_shipping_city").val() != "") {
                    city = $("#form_shipping_city option:selected").text();
                    $("#city").val(city);
                }
                if ($("#form_shipping_stateId").val() != "") {
                    state = $("#form_shipping_stateId option:selected").text();
                    $("#state").val(state);
                }
                if ($("#form_shipping_zipcode").val() != "") {
                    zip = $("#form_shipping_zipcode").val();
                    $("#zip").val(zip);
                }
                if ($("#shipping_countryId").val() != "") {
                    country = $("#shipping_countryId option:selected").text();
                    $("#country").val(country);
                }
                if ($("#shipping_emailid").val() != "") {
                    emailid = $("#shipping_emailid").val();
                    $("#emailid").val(emailid);
                }
            }else{
                if ($("#billing_firstName").val() != "") {
                    first_name = $("#billing_firstName").val();
                    $("#first_name").val(first_name);
                }
                if ($("#billing_lastName").val() != "") {
                    last_name = $("#billing_lastName").val();
                    $("#last_name").val(last_name);
                }
                if ($("#form_billing_address1").val() != "") {
                    address1 = $("#form_billing_address1").val();
                    $("#address1").val(address1);
                }
                if ($("#form_billing_address2").val() != "") {
                    address2 = $("#form_billing_address2").val();
                    $("#address2").val(address2);
                }
                if ($("#form_billing_city").val() != "") {
                    city = $("#form_billing_city option:selected").text();
                    $("#city").val(city);
                }
                if ($("#form_billing_stateId").val() != "") {
                    state = $("#form_billing_stateId option:selected").text();
                    $("#state").val(state);
                }
                if ($("#form_billing_zipcode").val() != "") {
                    zip = $("#form_billing_zipcode").val();
                    $("#zip").val(zip);
                }
                if ($("#billing_countryId").val() != "") {
                    country = $("#billing_countryId option:selected").text();
                    $("#country").val(country);
                }
                if ($("#billing_emailid").val() != "") {
                    emailid = $("#billing_emailid").val();
                    $("#emailid").val(emailid);
                }
            }
            if ($("#paypal_standard_url").val() != "") {
                payurl = $("#paypal_standard_url").val();
            }
            tax_amount = $(".c-taxtotal").attr("data-totaltax");
            $("#tax").val(tax_amount);
            $.ajax({
                url: baseUrl + '/pos/sales/neworder',
                type: 'post',
                data: formData,
                beforeSend: function () {
                    $(".loader").removeClass("hide");
                    $(".loader").addClass('show');
                },
                success: function (result) {
                    if (result != "") {
                        var rs = result.split("||");
                        formData.push({name: 'orderId', value: rs[0]});
                        formData.push({name: 'invoice', value: rs[1]});
                        $.ajax({
                            url: baseUrl + '/product/payflowintegration',
                            type: 'post',
                            data: formData,
                            beforeSend: function () {
                                $(".loader").removeClass("hide");
                                $(".loader").addClass('show');
                            },
                            success: function (result) {
                                $(".loader").removeClass("show");
                                $(".loader").addClass('hide');
                                $("#myModal").modal('show');
                                $('#payflowbody').html(result);
                                return false;
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                return false;
                            }
                        })
                    } else {
                        $(".loader").removeClass("show");
                        $(".loader").addClass('hide');
                        swal("Error!", "Error", "error");
                        window.location.reload(true);
                    }
                    return false;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    return false;
                }
            })
        }
    }
});
$(".paypalplus").click(function (event) {
    if ($('#frminstallmentpay').length > 0) {
        event.preventDefault();
        event.stopImmediatePropagation();
        var payurl = "";
        var baseUrl = getBaseUrl();
        $("#item_name").val($("#install_item_name").val());
        $('[name=currency_code]').val($("#install_currency_code").val());
        $("#first_name").val($("#install_first_name").val());
        $("#last_name").val($("#install_last_name").val());
        $("#address1").val($("#install_address1").val());
        $("#address2").val($("#install_address2").val());
        $("#city").val($("#install_city").val());
        $("#state").val($("#install_state").val());
        $("#zip").val($("#install_zip").val());
        $("#country").val($("#install_country").val());
        $("#select_no_installment").val($("#install_select_no_installment").val());
        $('[name=email]').val($("#install_email").val());
        if ($("#paypal_standard_url").val() != "") {
            payurl = $("#paypal_standard_url").val();
        }
        var mid = $(this).data("id");
        $('#install_btnordpayment').val(mid);
        $.ajax({
            url: '/pos/sales/newinstallmentorder',
            type: 'post',
            data: {'install_enc_order_id': $('#install_enc_order_id').val(), 'install_select_no_installment': $('#install_select_no_installment').val(), 'install_net_payable_amount': $('#install_net_payable_amount').val(), 'install_btnordpayment': $('#install_btnordpayment').val(), },
            beforeSend: function () {},
            success: function (result) {
                if (result != "") {
                    var rs = result.split("||");
                    $("#order_id").val(rs[0]);
                    $("#invoice_id").val(rs[1]);
                    $.ajax({
                        url: baseUrl + '/product/payplusintegration',
                        type: 'post',
                        dataType: "json",
                        data: {'install_enc_order_id': $('#install_enc_order_id').val(), 'install_select_no_installment': $('#install_select_no_installment').val(), 'install_net_payable_amount': $('#install_net_payable_amount').val(), 'install_btnordpayment': $('#install_btnordpayment').val(), 'install_payment_from': 'installment', 'install_invoice': rs[1], },
                        beforeSend: function () {
                            $(".loader").removeClass("hide");
                            $(".loader").addClass('show');
                        },
                        success: function (result) {
                            var ppp = "";
                            var urldata = "";
                            var url = "";
                            $(".loader").removeClass("show");
                            $(".loader").addClass('hide');
                            $("#myModalplus").modal('show');
                            ppp = PAYPAL.apps.PPP({
                                "approvalUrl": result.approval_url,
                                "placeholder": "ppplus",
                                "language": "en_US",
                                "mode": "sandbox",
                                onLoad: function () {
                                    $("#ppplus iframe").css("width", "100%");
                                    $("#ppplus iframe").attr("id", "paypalplus_iframe");
                                }
                            });
                            return false;
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            return false;
                        }
                    });
                } else {
                    $(".loader").removeClass("show");
                    $(".loader").addClass('hide');
                    swal("Error!", "Error", "error");
                    window.location.reload(true);
                }
                return false;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                return false;
            }
        });
    } else {
        if ($('#frmcheckout').bootstrapValidator('validate').has('.has-error').length === 0) {
            event.preventDefault();
            event.stopImmediatePropagation();
            var mid = $(this).data("id");
            var formData = $("#frmcheckout").serializeArray();
            formData.push({name: 'Abandon', value: 'Yes'});
            formData.push({name: 'btnordpayment', value: mid});
            var baseUrl = getBaseUrl();
            var first_name = "";
            var last_name = "";
            var address1 = "";
            var address2 = "";
            var city = "";
            var state = "";
            var zip = "";
            var country = "";
            var emailid = "";
            var payurl = "";
            if ($('#shipping_countryId').length > 0 &&  $('#shipping_countryId').is(":visible")) {
                if ($("#shipping_firstName").val() != "") {
                    first_name = $("#shipping_firstName").val();
                    $("#first_name").val(first_name);
                }
                if ($("#shipping_lastName").val() != "") {
                    last_name = $("#shipping_lastName").val();
                    $("#last_name").val(last_name);
                }
                if ($("#form_shipping_address1").val() != "") {
                    address1 = $("#form_shipping_address1").val();
                    $("#address1").val(address1);
                }
                if ($("#form_shipping_address2").val() != "") {
                    address2 = $("#form_shipping_address2").val();
                    $("#address2").val(address2);
                }
                if ($("#form_shipping_city").val() != "") {
                    city = $("#form_shipping_city option:selected").text();
                    $("#city").val(city);
                }
                if ($("#form_shipping_stateId").val() != "") {
                    state = $("#form_shipping_stateId option:selected").text();
                    $("#state").val(state);
                }
                if ($("#form_shipping_zipcode").val() != "") {
                    zip = $("#form_shipping_zipcode").val();
                    $("#zip").val(zip);
                }
                if ($("#shipping_countryId").val() != "") {
                    country = $("#shipping_countryId option:selected").text();
                    $("#country").val(country);
                }
                if ($("#shipping_emailid").val() != "") {
                    emailid = $("#shipping_emailid").val();
                    $("#emailid").val(emailid);
                }
            }else{
                if ($("#billing_firstName").val() != "") {
                    first_name = $("#billing_firstName").val();
                    $("#first_name").val(first_name);
                }
                if ($("#billing_lastName").val() != "") {
                    last_name = $("#billing_lastName").val();
                    $("#last_name").val(last_name);
                }
                if ($("#form_billing_address1").val() != "") {
                    address1 = $("#form_billing_address1").val();
                    $("#address1").val(address1);
                }
                if ($("#form_billing_address2").val() != "") {
                    address2 = $("#form_billing_address2").val();
                    $("#address2").val(address2);
                }
                if ($("#form_billing_city").val() != "") {
                    city = $("#form_billing_city option:selected").text();
                    $("#city").val(city);
                }
                if ($("#form_billing_stateId").val() != "") {
                    state = $("#form_billing_stateId option:selected").text();
                    $("#state").val(state);
                }
                if ($("#form_billing_zipcode").val() != "") {
                    zip = $("#form_billing_zipcode").val();
                    $("#zip").val(zip);
                }
                if ($("#billing_countryId").val() != "") {
                    country = $("#billing_countryId option:selected").text();
                    $("#country").val(country);
                }
                if ($("#billing_emailid").val() != "") {
                    emailid = $("#billing_emailid").val();
                    $("#emailid").val(emailid);
                }
            }
            if ($("#paypal_standard_url").val() != "") {
                payurl = $("#paypal_standard_url").val();
            }
            tax_amount = $(".c-taxtotal").attr("data-totaltax");
            $("#tax").val(tax_amount);
            $.ajax({
                url: baseUrl + '/pos/sales/neworder',
                type: 'post',
                data: formData,
                beforeSend: function () {},
                success: function (result) {
                    if (result != "") {
                        var rs = result.split("||");
                        formData.push({name: 'orderId', value: rs[0]});
                        formData.push({name: 'invoice', value: rs[1]});
                        console.log(formData);
                        $.ajax({
                            url: baseUrl + '/product/payplusintegration',
                            type: 'post',
                            dataType: "json",
                            data: formData,
                            beforeSend: function () {
                                $(".loader").removeClass("hide");
                                $(".loader").addClass('show');
                            },
                            success: function (result) {
                                var ppp = "";
                                var urldata = "";
                                var url = "";
                                $(".loader").removeClass("show");
                                $(".loader").addClass('hide');
                                $("#myModalplus").modal('show');
                                ppp = PAYPAL.apps.PPP({
                                    "approvalUrl": result.approval_url,
                                    "placeholder": "ppplus",
                                    "language": "en_US",
                                    "mode": "sandbox",
                                    onLoad: function () {
                                        $("#ppplus iframe").css("width", "100%");
                                        $("#ppplus iframe").attr("id", "paypalplus_iframe");
                                    }
                                });
                                return false;
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                return false;
                            }
                        })
                    } else {
                        $(".loader").removeClass("show");
                        $(".loader").addClass('hide');
                        swal("Error!", "Error", "error");
                        window.location.reload(true);
                    }
                    return false;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    return false;
                }
            })
        }
    }
});
if ($('.touchspin').length > 0) {
    var first_installment_price_hdn_val = $('#first_installment_price_hdn_val').val();
    if (first_installment_price_hdn_val) {
        $('#first-payment-amount-val').text(first_installment_price_hdn_val);
    }
    $(".touchspin").TouchSpin({buttondown_class: 'btn red', buttonup_class: 'btn blue', min: 2, max: InstallmentData.max_no_of_installment, }).
            on('touchspin.on.startupspin', function () {
                var total_no_of_payments = parseInt($('#form_total_no_of_payments').val());
                console.log('total_no_of_payments : ' + total_no_of_payments);
                console.log('installment_total_amount : ' + InstallmentData.installment_total_amount);
                var new_set_amount = parseFloat(InstallmentData.installment_total_amount) / total_no_of_payments;
                console.log('new_set_amount : ' + new_set_amount);
                var pre_first_installment = parseFloat(InstallmentData.first_installment_price) - parseFloat(InstallmentData.installment_price);
                var new_first_payment = pre_first_installment + new_set_amount;
                console.log('pre_first_installment : ' + pre_first_installment);
                new_first_payment = symbol + new_first_payment.toFixed(2);
                $('#first-payment-amount-val').text(new_first_payment);
                $('#first_installment_price_hdn_val').val(new_first_payment);
                new_set_amount = symbol + new_set_amount.toFixed(2);
                $('#monthly_payment_amount').val(new_set_amount);
                $('#first-payment-amount-val').text(new_first_payment);
                $('#first_installment_price_hdn_val').val(new_first_payment);
                $('#monthly_payment_amount').val(new_set_amount);
            }).on('touchspin.on.startdownspin', function () {
        var total_no_of_payments = parseInt($('#form_total_no_of_payments').val());
        console.log('total_no_of_payments : ' + total_no_of_payments);
        console.log('installment_total_amount : ' + InstallmentData.installment_total_amount);
        var new_set_amount = parseFloat(InstallmentData.installment_total_amount) / total_no_of_payments;
        console.log('new_set_amount : ' + new_set_amount);
        var pre_first_installment = parseFloat(InstallmentData.first_installment_price) - parseFloat(InstallmentData.installment_price);
        console.log('pre_first_installment : ' + pre_first_installment);
        var new_first_payment = pre_first_installment + new_set_amount;
        console.log('new_first_payment : ' + new_first_payment);
        new_first_payment = symbol + new_first_payment.toFixed(2);
        $('#first-payment-amount-val').text(new_first_payment);
        $('#first_installment_price_hdn_val').val(new_first_payment);
        new_set_amount = symbol + new_set_amount.toFixed(2);
        $('#monthly_payment_amount').val(new_set_amount);
    });
}

$("body").delegate("#proceed-installment", "click", function (e) {
    e.preventDefault();
    var total_no_of_payments = $('#form_total_no_of_payments').val();
    var baseUrl = getBaseUrl();
    $.ajax({
        type: "POST",
        url: baseUrl + '/shoppingcart/proceedInstallment',
        cache: false,
        data: {'totalNoOfPayments': total_no_of_payments},
        success: function (response) {
            if (response == 1) {
                window.location = 'login';
            } else if (response == 'true') {
                window.location = 'checkout';
            } else {
                if (response == 'false') {
                    swal({title: 'Something went to wrong !!!', type: 'error', showCancelButton: false, confirmButtonText: 'Ok, Remove', cancelButtonText: 'No, Cancel'});
                } else {
                    var product_ids = response;
                    swal({title: 'Thank you for showing interest. Unfortunately, some products cannot be purchase using Installment. Please click "Ok" in order to remove those product from the cart, and "Cancel" to stay on the page.', type: 'warning', showCancelButton: true, confirmButtonText: 'Ok, Remove', cancelButtonText: 'No, Cancel'}, function () {
                        $.ajax({
                            type: "POST",
                            url: baseUrl + '/shoppingcart/removeWithoutInstallmentItem',
                            cache: false,
                            data: 'product_ids=' + product_ids + '&totalNoOfPayments=' + total_no_of_payments,
                            success: function (response) {
                                if (response == 'true') {
                                    window.location = 'checkout';
                                }
                            }
                        });
                        return false;
                    });
                }
            }
        }
    });
});

$('#stripebtnpayment').click(function (e) {
    if ($('#frmcheckout').bootstrapValidator('validate').has('.has-error').length === 0) {
        var stripeId = $(this).attr('data-id');
        if (stripeId != '') {
            $('#stripebtnvalue').val(stripeId);
        }
        getStripCheckout();

        function getStripCheckout() {
            var handler = StripeCheckout.configure({
                key: StripeAPIKey,
                image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
                locale: 'auto',
                token: function (token) {
                    var token_id = token.id;
                    $("#stripe_tocken").val(token.id);
                    if ($('#frminstallmentpay').length > 0) {
                        document.frminstallmentpay.submit();
                    } else {
                        document.frmcheckout.submit();
                    }
                }
            });
            handler.open({name: 'Stripe.com', zipCode: false, amount: $('#product_amount').value});
            e.preventDefault();
            window.addEventListener('popstate', function () {
                handler.close();
            });
        }
    }
});
$("#affrimCheckoutButton").click(function (e) {
    if ($('#frmcheckout').length > 0) {
        if ($('#frmcheckout').bootstrapValidator('validate').has('.has-error').length === 0) {
            var public_api_key = $("#affirm_public_api_key").val();
            var affirm_script = $("#affirm_script").val();
            var _affirm_config = {public_api_key: public_api_key, script: affirm_script};
            (function (l, g, m, e, a, f, b) {
                var d, c = l[m] || {},
                        h = document.createElement(f),
                        n = document.getElementsByTagName(f)[0],
                        k = function (a, b, c) {
                            return function () {
                                a[b]._.push([c, arguments])
                            }
                        };
                c[e] = k(c, e, "set");
                d = c[e];
                c[a] = {};
                c[a]._ = [];
                d._ = [];
                c[a][b] = k(c, a, b);
                a = 0;
                for (b = "set add save post open empty reset on off trigger ready setProduct".split(" "); a < b.length; a++)
                    d[b[a]] = k(c, e, b[a]);
                a = 0;
                for (b = ["get", "token", "url", "items"]; a < b.length; a++)
                    d[b[a]] = function () {};
                h.async = !0;
                h.src = g[f];
                n.parentNode.insertBefore(h, n);
                delete g[f];
                d(g);
                l[m] = c
            })(window, _affirm_config, "affirm", "checkout", "ui", "script", "ready");
            var baseUrl = getBaseUrl();
            var formData = $("#frmcheckout").serialize();
            $.ajax({
                url: baseUrl + '/affirmobj',
                type: 'post',
                data: formData,
                beforeSend: function () {},
                success: function (affirmObj) {
                    if (affirmObj != '') {
                        affirm.checkout($.parseJSON(affirmObj));
                        affirm.checkout.post();
                    }
                },
            });
        }
    } else {
        var public_api_key = $("#affirm_public_api_key").val();
        var affirm_script = $("#affirm_script").val();
        var _affirm_config = {public_api_key: public_api_key, script: affirm_script};
        (function (l, g, m, e, a, f, b) {
            var d, c = l[m] || {},
                    h = document.createElement(f),
                    n = document.getElementsByTagName(f)[0],
                    k = function (a, b, c) {
                        return function () {
                            a[b]._.push([c, arguments])
                        }
                    };
            c[e] = k(c, e, "set");
            d = c[e];
            c[a] = {};
            c[a]._ = [];
            d._ = [];
            c[a][b] = k(c, a, b);
            a = 0;
            for (b = "set add save post open empty reset on off trigger ready setProduct".split(" "); a < b.length; a++)
                d[b[a]] = k(c, e, b[a]);
            a = 0;
            for (b = ["get", "token", "url", "items"]; a < b.length; a++)
                d[b[a]] = function () {};
            h.async = !0;
            h.src = g[f];
            n.parentNode.insertBefore(h, n);
            delete g[f];
            d(g);
            l[m] = c
        })(window, _affirm_config, "affirm", "checkout", "ui", "script", "ready");
        var baseUrl = getBaseUrl();
        var formData = $("#frminstallmentpay").serialize();
        console.log(formData);
        $.ajax({
            url: baseUrl + '/affirminstallmentobj',
            type: 'post',
            data: formData,
            beforeSend: function () {},
            success: function (affirminstallmentObj) {
                if (affirminstallmentObj != '') {
                    affirm.checkout($.parseJSON(affirminstallmentObj));
                    affirm.checkout.post();
                }
            },
        });
    }
});
if ($("#affirm_public_api_key").length > 0 && $("#hdn_sale_price").length > 0) {
    var hdn_sale_price = $("#hdn_sale_price").val();
    hdn_sale_price = hdn_sale_price.replace(".", "");
    var public_api_key = $("#affirm_public_api_key").val();
    var affirm_script = $("#affirm_script").val();
    var _affirm_config = {public_api_key: public_api_key, script: affirm_script};
    (function (l, g, m, e, a, f, b) {
        var d, c = l[m] || {},
                h = document.createElement(f),
                n = document.getElementsByTagName(f)[0],
                k = function (a, b, c) {
                    return function () {
                        a[b]._.push([c, arguments])
                    }
                };
        c[e] = k(c, e, "set");
        d = c[e];
        c[a] = {};
        c[a]._ = [];
        d._ = [];
        c[a][b] = k(c, a, b);
        a = 0;
        for (b = "set add save post open empty reset on off trigger ready setProduct".split(" "); a < b.length; a++)
            d[b[a]] = k(c, e, b[a]);
        a = 0;
        for (b = ["get", "token", "url", "items"]; a < b.length; a++)
            d[b[a]] = function () {};
        h.async = !0;
        h.src = g[f];
        n.parentNode.insertBefore(h, n);
        delete g[f];
        d(g);
        l[m] = c
    })(window, _affirm_config, "affirm", "checkout", "ui", "script", "ready");
    affirm.ui.ready(function () {
        GetAffirmAsLowAs(hdn_sale_price)
    });

    function GetAffirmAsLowAs(amount) {
        if ((amount == null)) {
            return;
        }
        var options = {apr: "0.10", months: 12, amount: amount};
        try {
            typeof affirm.ui.payments.get_estimate;
        } catch (e) {
            return;
        }

        function handleEstimateResponse(payment_estimate) {
            var hdn_currency = $("#hdn_currency").val();
            var dollars = ((payment_estimate.payment + 99) / 100) | 0;
            var a = document.getElementById("afirm-learn-more");
            var iText = ("innerText" in a) ? "innerText" : "textContent";
            a[iText] = "Learn More";
            var amountInfo = document.getElementById("amounut-info");
            amountInfo.innerHTML = "<div class=amount>" + hdn_currency + dollars + " / month with</div> ";
            amountInfo.style.visibility = "visible";
            var checkouttext = document.getElementById("checkout-text");
            checkouttext.innerHTML = "at checkout";
            checkouttext.style.visibility = "visible";
            a.onclick = payment_estimate.open_modal;
            a.style.visibility = "visible";
        }
        ;
        affirm.ui.payments.get_estimate(options, handleEstimateResponse);
    }
}
if ($("#zibby_pubilc_token").length > 0) {
    var zibby_pubilc_token = $("#zibby_pubilc_token").val();
    var zibby_mode_url = $("#zibby_mode_url").val();
    var _zibby_config = {api_key: zibby_pubilc_token, environment: zibby_mode_url};
    !function (e, t) {
        e.zibby = e.zibby || {};
        var n, i, r;
        i = !1, n = document.createElement("script"), n.type = "text/javascript", n.async = 0, n.src = t.environment + "/plugin/js/zibby.js", n.onload = n.onreadystatechange = function () {
            i || this.readyState && "complete" != this.readyState || (i = 0, e.zibby.setConfig(t.api_key))
        }, r = document.getElementsByTagName("script")[0], r.parentNode.insertBefore(n, r);
        var s = document.createElement("link");
        s.setAttribute("rel", "stylesheet"), s.setAttribute("type", "text/css"), s.setAttribute("href", t.environment + "/plugin/css/zibby.css");
        var a = document.querySelector("head");
        a.insertBefore(s, a.firstChild)
    }(window, _zibby_config);
}
$("body").delegate("#zibby_payment", "click", function (e) {
    if ($("#zibby_pubilc_token").length > 0) {
        if ($('#frmcheckout').length > 0) {
            var baseUrl = getBaseUrl();
            var formData = $("#frmcheckout").serialize();
            $.ajax({
                url: baseUrl + '/zibbyobj',
                type: 'post',
                data: formData,
                beforeSend: function () {},
                success: function (zibbyObj) {
                    zibby.checkout.set($.parseJSON(zibbyObj));
                    zibby.checkout.load();
                },
            });
        } else {
            var baseUrl = getBaseUrl();
            var formData = $("#frminstallmentpay").serialize();
            $.ajax({
                url: baseUrl + '/zibbyinstallmentobj',
                type: 'post',
                data: formData,
                beforeSend: function () {},
                success: function (zibbyObj) {
                    zibby.checkout.set($.parseJSON(zibbyObj));
                    zibby.checkout.load();
                },
            });
        }
    }
});
$("body").delegate("#zibby-preapprove", "click", function (e) {
    var zibby_pubilc_token = $("#zibby_pubilc_token").val();
    var zibby_mode_url = $("#zibby_mode_url").val();
    var _zibby_config = {api_key: zibby_pubilc_token, environment: zibby_mode_url};
    !function (e, t) {
        e.zibby = e.zibby || {};
        var n, i, r;
        i = !1, n = document.createElement("script"), n.type = "text/javascript", n.async = 0, n.src = t.environment + "/plugin/js/zibby.js", n.onload = n.onreadystatechange = function () {
            i || this.readyState && "complete" != this.readyState || (i = 0, e.zibby.setConfig(t.api_key))
        }, r = document.getElementsByTagName("script")[0], r.parentNode.insertBefore(n, r);
        var s = document.createElement("link");
        s.setAttribute("rel", "stylesheet"), s.setAttribute("type", "text/css"), s.setAttribute("href", t.environment + "/plugin/css/zibby.css");
        var a = document.querySelector("head");
        a.insertBefore(s, a.firstChild)
    }(window, _zibby_config);
    zibby.preapprove();
});
$(".citibanamexbutton").click(function (e) {
    if ($('#frmcheckout').length > 0) {
        if ($('#frmcheckout').bootstrapValidator('validate').has('.has-error').length === 0) {
            var baseUrl = getBaseUrl();
            var formData = $("#frmcheckout").serialize();
            $.ajax({
                url: baseUrl + '/banamexdata',
                type: 'post',
                data: formData,
                beforeSend: function () {},
                success: function (result) {
                    if (result != '') {
                        Checkout.configure($.parseJSON(result));
                        Checkout.showLightbox();
                    }
                },
            });
        }
    } else {
        var baseUrl = getBaseUrl();
        var formData = $("#frminstallmentpay").serialize();
        $.ajax({
            url: baseUrl + '/banamexinstallmentdata',
            type: 'post',
            data: formData,
            beforeSend: function () {},
            success: function (result) {
                if (result != '') {
                    Checkout.configure($.parseJSON(result));
                    Checkout.showLightbox();
                }
            },
        });
    }
});
$(".razorpaybutton").click(function (e) {
    if ($('#frmcheckout').length > 0) {
        if ($('#frmcheckout').bootstrapValidator('validate').has('.has-error').length === 0) {
            var baseUrl = getBaseUrl();
            var formData = $("#frmcheckout").serialize();
            $.ajax({
                url: baseUrl + '/razorpaydata',
                type: 'post',
                data: formData,
                beforeSend: function () {},
                success: function (result) {
                    if (result != '') {
                        var options = JSON.parse(result);
                        options.handler = function (response) {
                            $('#razorpay_payment_id').val(response.razorpay_payment_id);
                            $('#razorpay_signature').val(response.razorpay_signature);
                            window.location = options.redirectUrl + response.razorpay_payment_id + '/' + response.razorpay_signature;
                        };
                        options.modal = {
                            ondismiss: function () {
                                console.log("This code runs when the popup is closed");
                            },
                            escape: true,
                            backdropclose: false
                        };
                        var rzp = new Razorpay(options);
                        rzp.open();
                        e.preventDefault();
                    }
                },
            });
        }
    } else {
        var baseUrl = getBaseUrl();
        var formData = $("#frminstallmentpay").serialize();
        $.ajax({
            url: baseUrl + '/razorpayinstallmentdata',
            type: 'post',
            data: formData,
            beforeSend: function () {},
            success: function (result) {
                if (result != '') {
                    var options = JSON.parse(result);
                    options.handler = function (response) {
                        $('#razorpay_payment_id').val(response.razorpay_payment_id);
                        $('#razorpay_signature').val(response.razorpay_signature);
                        window.location = options.redirectUrl + response.razorpay_payment_id + '/' + response.razorpay_signature;
                    };
                    options.modal = {
                        ondismiss: function () {
                            console.log("This code runs when the popup is closed");
                        },
                        escape: true,
                        backdropclose: false
                    };
                    var rzp = new Razorpay(options);
                    rzp.open();
                    e.preventDefault();
                }
            },
        });
    }
});
if ($('#choose_number_of_installment').length > 0) {
    $('#choose_number_of_installment').on('change', function () {
        var choose_number_of_installment = $('#choose_number_of_installment').val();
        var monthly_payment = $('#monthly_payment').data('value');
        var net_payable_amount = choose_number_of_installment * monthly_payment;
        $('#net-payable-amount').data("value", net_payable_amount.toFixed(2));
        $('#net-payable-amount').html(symbol + net_payable_amount.toFixed(2));
    });
}
$(".payfortbtn").click(function (event) {
    if ($('#frminstallmentpay').length > 0) {
        event.preventDefault();
        event.stopImmediatePropagation();
        var baseUrl = getBaseUrl();
        $("#item_name").val($("#install_item_name").val());
        $('[name=currency_code]').val($("#install_currency_code").val());
        $("#first_name").val($("#install_first_name").val());
        $("#last_name").val($("#install_last_name").val());
        $("#address1").val($("#install_address1").val());
        $("#address2").val($("#install_address2").val());
        $("#city").val($("#install_city").val());
        $("#state").val($("#install_state").val());
        $("#zip").val($("#install_zip").val());
        $("#country").val($("#install_country").val());
        $("#select_no_installment").val($("#install_select_no_installment").val());
        $('[name=email]').val($("#install_email").val());
        var mid = $(this).attr("id");
        $('#install_btnordpayment').val(mid);
        var formData = $("#frminstallmentpay").serializeArray();
        formData.push({name: 'Abandon', value: 'Yes'});
        formData.push({name: 'btnordpayment', value: mid});
        $.ajax({
            url: baseUrl + '/pos/sales/newinstallmentorder',
            type: 'post',
            data: formData,
            beforeSend: function () {},
            success: function (result) {
                if (result != "") {
                    var amount = $('#install_net_payable_amount').val();
                    $(".loader").removeClass("show");
                    $(".loader").addClass('hide');
                    $("#myModalfort").modal('show');
                    var rs = result.split("||");
                    $('#client_merchant_oid').val(rs[0]);
                    $('#no_of_installment').val($("#install_select_no_installment").val());
                    $('#product_name').val("p_" + rs[1]);
                    $('#client_merchant_amount').val(amount);
                    $("#payfort_fort_pay_action").attr('disabled', false);
                    return false;
                } else {
                    $(".loader").removeClass("show");
                    $(".loader").addClass('hide');
                    swal("Error!", "Error", "error");
                    window.location.reload(true);
                }
                return false;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                return false;
            }
        });
    } else {
        if ($('#frmcheckout').bootstrapValidator('validate').has('.has-error').length === 0) {
            event.preventDefault();
            event.stopImmediatePropagation();
            var mid = $(this).attr("id");
            var formData = $("#frmcheckout").serializeArray();
            formData.push({name: 'Abandon', value: 'Yes'});
            formData.push({name: 'btnordpayment', value: mid});
            var baseUrl = getBaseUrl();
            var first_name = "";
            var last_name = "";
            var address1 = "";
            var address2 = "";
            var city = "";
            var state = "";
            var zip = "";
            var country = "";
            var emailid = "";
            var payurl = "";
           /* if ($("#firstName").val() != "") {
                first_name = $("#firstName").val();
                $("#first_name").val(first_name);
            }
            if ($("#lastName").val() != "") {
                last_name = $("#lastName").val();
                $("#last_name").val(last_name);
            }
            if ($("#form_address1").val() != "") {
                address1 = $("#form_address1").val();
                $("#address1").val(address1);
            }
            if ($("#form_address2").val() != "") {
                address2 = $("#form_address2").val();
                $("#address2").val(address2);
            }
            if ($("#form_city").val() != "") {
                city = $("#form_city").val();
                $("#city").val(city);
            }
            if ($("#form_stateId").val() != "") {
                state = $("#form_stateId").val();
                $("#state").val(state);
            }
            if ($("#form_zipcode").val() != "") {
                zip = $("#form_zipcode").val();
                $("#zip").val(zip);
            }
            if ($("#countryId").val() != "") {
                country = $("#countryId option:selected").text();
                $("#country").val(country);
            }
            if ($("#emailid").val() != "") {
                emailid = $("#emailid").val();
                $("#emailid").val(emailid);
            }*/
            if ($('#shipping_countryId').length > 0 &&  $('#shipping_countryId').is(":visible")) {
                if ($("#shipping_firstName").val() != "") {
                    first_name = $("#shipping_firstName").val();
                    $("#first_name").val(first_name);
                }
                if ($("#shipping_lastName").val() != "") {
                    last_name = $("#shipping_lastName").val();
                    $("#last_name").val(last_name);
                }
                if ($("#form_shipping_address1").val() != "") {
                    address1 = $("#form_shipping_address1").val();
                    $("#address1").val(address1);
                }
                if ($("#form_shipping_address2").val() != "") {
                    address2 = $("#form_shipping_address2").val();
                    $("#address2").val(address2);
                }
                if ($("#form_shipping_city").val() != "") {
                    city = $("#form_shipping_city option:selected").text();
                    $("#city").val(city);
                }
                if ($("#form_shipping_stateId").val() != "") {
                    state = $("#form_shipping_stateId option:selected").text();
                    $("#state").val(state);
                }
                if ($("#form_shipping_zipcode").val() != "") {
                    zip = $("#form_shipping_zipcode").val();
                    $("#zip").val(zip);
                }
                if ($("#shipping_countryId").val() != "") {
                    country = $("#shipping_countryId option:selected").text();
                    $("#country").val(country);
                }
                if ($("#shipping_emailid").val() != "") {
                    emailid = $("#shipping_emailid").val();
                    $("#emailid").val(emailid);
                }
            }else{
                if ($("#billing_firstName").val() != "") {
                    first_name = $("#billing_firstName").val();
                    $("#first_name").val(first_name);
                }
                if ($("#billing_lastName").val() != "") {
                    last_name = $("#billing_lastName").val();
                    $("#last_name").val(last_name);
                }
                if ($("#form_billing_address1").val() != "") {
                    address1 = $("#form_billing_address1").val();
                    $("#address1").val(address1);
                }
                if ($("#form_billing_address2").val() != "") {
                    address2 = $("#form_billing_address2").val();
                    $("#address2").val(address2);
                }
                if ($("#form_billing_city").val() != "") {
                    city = $("#form_billing_city option:selected").text();
                    $("#city").val(city);
                }
                if ($("#form_billing_stateId").val() != "") {
                    state = $("#form_billing_stateId option:selected").text();
                    $("#state").val(state);
                }
                if ($("#form_billing_zipcode").val() != "") {
                    zip = $("#form_billing_zipcode").val();
                    $("#zip").val(zip);
                }
                if ($("#billing_countryId").val() != "") {
                    country = $("#billing_countryId option:selected").text();
                    $("#country").val(country);
                }
                if ($("#billing_emailid").val() != "") {
                    emailid = $("#billing_emailid").val();
                    $("#emailid").val(emailid);
                }
            }
            if ($("#paypal_standard_url").val() != "") {
                payurl = $("#paypal_standard_url").val();
            }
            tax_amount = $(".c-taxtotal").attr("data-totaltax");
            $("#tax").val(tax_amount);
            $.ajax({
                url: baseUrl + '/pos/sales/neworder',
                type: 'post',
                data: formData,
                beforeSend: function () {},
                success: function (result) { console.log(result); 
                    if (result != "") {
                        var amount = $('#amount').val();
                        $(".loader").removeClass("show");
                        $(".loader").addClass('hide');
                        $("#myModalfort").modal('show');
                        var rs = result.split("||");
                        //$('#client_merchant_oid').val(rs[0]);
                        //$('#product_name').val("p_" + rs[1]);
                        $('#client_merchant_amount').val(amount);
                        return false;
                        $("#payfort_fort_pay_action").attr('disabled', false);
                        //console.log(rs);
                        return false;
                    } else {
                        $(".loader").removeClass("show");
                        $(".loader").addClass('hide');
                        swal("Error!", "Error", "error");
                        window.location.reload(true);
                    }
                    return false;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    return false;
                }
            })
        }
    }
});
$(".paynamics").click(function (event) {
    if ($('#frminstallmentpay').length > 0) {
        event.preventDefault();
        event.stopImmediatePropagation();
        var baseUrl = getBaseUrl();
        var mid = $(this).attr("id");
        var formData = $("#frminstallmentpay").serializeArray();
        formData.push({name: 'Abandon', value: 'Yes'});
        formData.push({name: 'submit', value: mid});
        var baseUrl = getBaseUrl();
        $.ajax({
            url: baseUrl + '/pos/sales/newinstallmentorder',
            type: 'post',
            data: formData,
            beforeSend: function () {},
            success: function (result) {
                if (result != "") {
                    var rs = result.split("||");
                    formData.push({name: 'orderId', value: rs[0]});
                    //formData.push({name: 'invoice', value: rs[1]});
                    $.ajax({
                        url: baseUrl + '/product/paynamicsinstallmentintegration',
                        type: 'post',
                        data: formData,
                        dataType: "json",
                        beforeSend: function () {},
                        success: function (result) {
                            if (result.code == 1) {
                               // $("#paynamicsbody").html(result.form);
                               $("#paynamicsbody").append(result.form);
                                if ($('#paynamicsgatewaymyform').length > 0) {
                                    document.paynamicsgatewaymyform.submit();
                                }
                                console.log(result);
                            } 

                            else {
                            }
                            return false;
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            return false;
                        }
                    })
                } else {
                    $(".loader").removeClass("show");
                    $(".loader").addClass('hide');
                    swal("Error!", "Error", "error");
                    window.location.reload(true);
                }
                return false;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                return false;
            }
        });
    }else{
        if ($('#frmcheckout').bootstrapValidator('validate').has('.has-error').length === 0) {
            event.preventDefault();
            event.stopImmediatePropagation();
            var mid = $(this).attr("id");
            var formData = $("#frmcheckout").serializeArray();
            formData.push({name: 'Abandon', value: 'Yes'});
            formData.push({name: 'submit', value: mid});
            var baseUrl = getBaseUrl();
            $.ajax({
                url: baseUrl + '/pos/sales/neworder',
                type: 'post',
                data: formData,
                beforeSend: function () {},
                success: function (result) {
                    if (result != "") {
                        var rs = result.split("||");
                        formData.push({name: 'orderId', value: rs[0]});
                        formData.push({name: 'invoice', value: rs[1]});
                        $.ajax({
                            url: baseUrl + '/product/paynamicsintegration',
                            type: 'post',
                            data: formData,
                            dataType: "json",
                            beforeSend: function () {},
                            success: function (result) {
                                //$("#paynamModal").modal('show');
                                if (result.code == 1) {
                                   $("#paynamicsbody").append(result.form);
                                    if ($('#paynamicsgatewaymyform').length > 0) {
                                        document.paynamicsgatewaymyform.submit();
                                    }
                                } else {
                                }
                                return false;
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                return false;
                            }
                        })
                    } else {
                        $(".loader").removeClass("show");
                        $(".loader").addClass('hide');
                        swal("Error!", "Error", "error");
                        window.location.reload(true);
                    }
                    return false;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    return false;
                }
            });
        } 
    }
});
if ($(".btn_webpay").length > 0) {
    $(".btn_webpay").click(function (event) {
        var mid = $(this).data("id");
        if ($('#frmcheckout').length > 0) {
            var formData = $("#frmcheckout").serializeArray();
            formData.push({name: 'Abandon', value: 'Yes'});
            formData.push({name: 'btnordpayment', value: mid});
            if ($('#frmcheckout').bootstrapValidator('validate').has('.has-error').length === 0) {
                var baseUrl = getBaseUrl();
                $.ajax({
                    url: baseUrl + '/pos/sales/neworder',
                    type: 'post',
                    data: formData,
                    beforeSend: function () {
                        $(".loader").removeClass("hide");
                        $(".loader").addClass('show');
                    },
                    success: function (result) {
                        if (result != "") {
                            var rs = result.split("||");
                            formData.push({name: 'orderId', value: rs[0]});
                            formData.push({name: 'invoice', value: rs[1]});
                            $.ajax({
                                url: baseUrl + '/webpaytoken',
                                type: 'post',
                                data: formData,
                                beforeSend: function () {
                                    $(".loader").removeClass("hide");
                                    $(".loader").addClass('show');
                                },
                                success: function (data) {
                                    $("#webpay_form").html($.parseJSON(data.form));
                                    $(".loader").removeClass("show");
                                    $(".loader").addClass('hide');
                                    $("#myModalwebpay").removeClass('hide');
                                    $("#myModalwebpay").modal('show');
                                },
                            });
                        } else {
                            $(".loader").removeClass("show");
                            $(".loader").addClass('hide');
                            swal("Error!", "Error", "error");
                            window.location.reload(true);
                        }
                        return false;
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        return false;
                    }
                })
            }
        } else {
            var formData = $("#frminstallmentpay").serializeArray();
            formData.push({name: 'Abandon', value: 'Yes'});
            formData.push({name: 'btnordpayment', value: mid});
            if ($('#frmcheckout').bootstrapValidator('validate').has('.has-error').length === 0) {
                var baseUrl = getBaseUrl();
                $.ajax({
                    url: baseUrl + '/pos/sales/newinstallmentorder',
                    type: 'post',
                    data: formData,
                    beforeSend: function () {
                        $(".loader").removeClass("hide");
                        $(".loader").addClass('show');
                    },
                    success: function (result) {
                        if (result != "") {
                            var rs = result.split("||");
                            formData.push({name: 'orderId', value: rs[0]});
                            formData.push({name: 'invoice', value: rs[1]});
                            $.ajax({
                                url: baseUrl + '/webpayinstallmenttoken',
                                type: 'post',
                                data: formData,
                                beforeSend: function () {
                                    $(".loader").removeClass("hide");
                                    $(".loader").addClass('show');
                                },
                                success: function (data) {
                                    $("#webpay_form").html($.parseJSON(data.form));
                                    $(".loader").removeClass("show");
                                    $(".loader").addClass('hide');
                                    $("#myModalwebpay").removeClass('hide');
                                    $("#myModalwebpay").modal('show');
                                },
                            });
                        } else {
                            $(".loader").removeClass("show");
                            $(".loader").addClass('hide');
                            swal("Error!", "Error", "error");
                            window.location.reload(true);
                        }
                        return false;
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        return false;
                    }
                })
            }
        }
    });
}
$("body").delegate("#webpaysumit", "click", function (e) {
    e.preventDefault();
    $("#webpaymyform").submit();
});
if ($(".emsbutton").length > 0) {
    $(".emsbutton").click(function (e) {
        if ($('#frmcheckout').length > 0) {
            if ($('#frmcheckout').bootstrapValidator('validate').has('.has-error').length === 0) {
                var baseUrl = getBaseUrl();
                var formData = $("#frmcheckout").serialize();
                $.ajax({
                    url: baseUrl + '/emspayment',
                    type: 'post',
                    data: formData,
                    success: function (result) {
                        if (result != '') {
                            var formData = $.parseJSON(result)
                            $('.emsbutton').attr('disabled', true);
                            $("#emspaymentformdiv").append(formData.form);
                            if ($('#emsgatewaymyform').length > 0) {
                                document.emsgatewaymyform.submit();
                            }
                        } else {
                            $('.emsbutton').attr('disabled', false);
                        }
                    },
                });
            }
        } else {
            var baseUrl = getBaseUrl();
            var formData = $("#frminstallmentpay").serialize();
            $.ajax({
                url: baseUrl + '/emsinstallmentpayment',
                type: 'post',
                data: formData,
                success: function (result) {
                    if (result != '') {
                        var formData = $.parseJSON(result)
                        $('.emsbutton').attr('disabled', true);
                        $("#emspaymentformdiv").append(formData.form);
                        if ($('#emsgatewaymyform').length > 0) {
                            document.emsgatewaymyform.submit();
                        }
                    } else {
                        $('.emsbutton').attr('disabled', false);
                    }
                },
            });
        }
    });
}
$("body").delegate("#emsFormSubmit", "click", function (e) {
    e.preventDefault();
    if ($('#emsgatewaymyform').length > 0) {
        document.emsgatewaymyform.submit();
    }
});
$('#eazzy_pay_action').on("click", function (e) {
    if ($('#frmcheckout').length > 0) {
        if ($('#frmcheckout').bootstrapValidator('validate').has('.has-error').length === 0) {
            var formData = $("#frmcheckout").serialize();
            var baseUrl = getBaseUrl();
            var eazzyConfig = $('#eazzyConfig').val();
            if (eazzyConfig != '') {
                eazzyCheckoutFunction(eazzyConfig)
            } else {
                $.ajax({
                    url: baseUrl + '/eazzypaydata',
                    type: 'post',
                    data: formData,
                    success: function (res) {
                        if (res != '') {
                            $('#eazzyConfig').val(res);
                            $('#eazzy_pay_action').click()
                        }
                    },
                })
            }
        }
    } else {
        e.preventDefault();
        var formData = $("#frminstallmentpay").serialize();
        var baseUrl = getBaseUrl();
        var eazzyConfig = $('#eazzyConfig').val();
        if (eazzyConfig != '') {
            eazzyCheckoutFunction(eazzyConfig)
        } else {
            $.ajax({
                url: baseUrl + '/eazzypayinstalldata',
                type: 'post',
                data: formData,
                success: function (res) {
                    if (res != '') {
                        $('#eazzyConfig').val(res);
                        $('#eazzy_pay_action').click()
                    }
                },
            })
        }
    }
});
if ($(".saudiarabiamigsbtn").length > 0) {
    $(".saudiarabiamigsbtn").click(function (e) {
        if ($('#frmcheckout').length > 0) {
            if ($('#frmcheckout').bootstrapValidator('validate').has('.has-error').length === 0) {
                var baseUrl = getBaseUrl();
                var formData = $("#frmcheckout").serialize();
                $.ajax({
                    url: baseUrl + '/saudimigspayment',
                    type: 'post',
                    data: formData,
                    success: function (result) {
                        if (result != '') {
                            window.location = result;
                        }
                    },
                });
            }
        } else {
            var baseUrl = getBaseUrl();
            var formData = $("#frminstallmentpay").serialize();
            $.ajax({
                url: baseUrl + '/saudimigsinstallmentpayment',
                type: 'post',
                data: formData,
                success: function (result) {
                    if (result != '') {
                        window.location = result;
                    }
                },
            });
        }
    });
}
if ($(".baiduribtn").length > 0) {
    $(".baiduribtn").click(function (e) {
        if ($('#frmcheckout').length > 0) {
            if ($('#frmcheckout').bootstrapValidator('validate').has('.has-error').length === 0) {
                var baseUrl = getBaseUrl();
                var formData = $("#frmcheckout").serialize();
                $.ajax({
                    url: baseUrl + '/baiduripayment',
                    type: 'post',
                    data: formData,
                    success: function (result) {
                        if (result != '') {
                            window.location = result;
                        }
                    },
                });
            }
        } else {
            var baseUrl = getBaseUrl();
            var formData = $("#frminstallmentpay").serialize();
            $.ajax({
                url: baseUrl + '/baiduriinstallmentpayment',
                type: 'post',
                data: formData,
                success: function (result) {
                    if (result != '') {
                        window.location = result;
                    }
                },
            });
        }
    });
}
$("#sberbankbtn").click(function (e) {
    if ($('#frmcheckout').length > 0) {
        if ($('#frmcheckout').bootstrapValidator('validate').has('.has-error').length === 0) {
            var baseUrl = getBaseUrl();
            var formData = $("#frmcheckout").serialize();
            $.ajax({
                url: baseUrl + '/sberbankpayment',
                type: 'post',
                data: formData,
                dataType: "json",
                success: function (result) {
                    if (result != '') {
                        window.location = result.formUrl;
                    }
                },
            });
        }
    } else {
        var baseUrl = getBaseUrl();
        var formData = $("#frminstallmentpay").serialize();
        $.ajax({
            url: baseUrl + '/sberbankinstallmentpayment',
            type: 'post',
            data: formData,
            dataType: "json",
            success: function (result) {
                if (result != '') {
                    window.location = result.formUrl;
                }
            },
        });
    }
});
$(".southafricamanzisync").click(function (event) {
    if ($('#frmcheckout').length > 0) {
        if ($('#frmcheckout').bootstrapValidator('validate').has('.has-error').length === 0) {
            event.preventDefault();
            event.stopImmediatePropagation();
            var mid = $(this).attr("id");
            var formData = $("#frmcheckout").serializeArray();
            formData.push({name: 'Abandon', value: 'Yes'});
            formData.push({name: 'btnordpayment', value: mid});
            var baseUrl = getBaseUrl();
            $.ajax({
                url: baseUrl + '/pos/sales/neworder',
                type: 'post',
                data: formData,
                beforeSend: function () {
                    $(".loader").removeClass("hide");
                    $(".loader").addClass('show');
                },
                success: function (result) {
                    if (result != "") {
                        $('.payGateFormDiv').html(result);
                        if ($('#payGateForm').length > 0) {
                            document.payGateForm.submit();
                        }
                    } else {
                        $(".loader").removeClass("show");
                        $(".loader").addClass('hide');
                        swal("Error!", "Error", "error");
                        window.location.reload(true);
                    }
                    return false;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    return false;
                }
            })
        }
    } else {
        event.preventDefault();
        event.stopImmediatePropagation();
        var mid = $(this).attr("id");
        var formData = $("#frminstallmentpay").serializeArray();
        formData.push({name: 'Abandon', value: 'Yes'});
        formData.push({name: 'btnordpayment', value: mid});
        var baseUrl = getBaseUrl();
        $.ajax({
            url: baseUrl + '/pos/sales/newinstallmentorder',
            type: 'post',
            data: formData,
            beforeSend: function () {
                $(".loader").removeClass("hide");
                $(".loader").addClass('show');
            },
            success: function (result) {
                if (result != "") {
                    $('.payGateFormDiv').html(result);
                    if ($('#payGateForm').length > 0) {
                        document.payGateForm.submit();
                    }
                } else {
                    $(".loader").removeClass("show");
                    $(".loader").addClass('hide');
                    swal("Error!", "Error", "error");
                    window.location.reload(true);
                }
                return false;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                return false;
            }
        })
    }
});