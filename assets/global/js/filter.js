var categoryId = $('#pageCatId').val();
var PriceSymbol = $('#PriceSymbol-' + categoryId).val();
var categorySesion = $('#CategorySession' + categoryId).val();
var minimumProductPrice = $('#Minprice' + categoryId).val();
var maximumProductPrice = $('#Maxprice' + categoryId).val();
localStorage.removeItem("ispriceslider" + categoryId);
var searchKeyword = $('#searchKeyword').val();
var promotionId = $('#promotionId').val();


window.onpopstate = function (event) {
    if (event && event.state) {
        location.reload();
    }
}
$('body').on('click', '#clearall-' + categoryId, function (e) {
    $('.category' + categoryId).attr('checked', false);
    removeQString("page");
    removeQString("pricerange");
    removeQString("brand");
    removeQString("category");
    removeQString("attribute");
    removeQString("sort");
    removeQString("rating");
    $('#session' + categoryId).html(" ");
    $(".price_range" + categoryId).val("");
    localStorage.removeItem("ispriceslider" + categoryId);
    var FIlterSelectBox = $("[urlvalue=default]").attr("id");
    if (FIlterSelectBox != null && FIlterSelectBox != "undefined") {
        document.getElementById(FIlterSelectBox).selected = "true";
    }
    var catMax = localStorage.getItem('catMaxVal' + categoryId);
    var catMin = localStorage.getItem('catMinVal' + categoryId);
    $(".pricerange-" + categoryId).slider('setValue', [catMin, catMax]);

    var filter_class;
    filter_class = ".filter-main ul.filter-bar";
    var thisdata = this;
    getCheckboxValue(e, thisdata, filter_class);
    location.reload();
});

$(document).ready(function (e) {
    var filter_class;
//    if(deviceType == 'phone'){
//        filter_class = ".filter-main ul.c-sidebar-menu";
//    }else{
//        filter_class = ".filter-main ul.filter-bar";
//    }
    filter_class = ".filter-main ul.filter-bar";
    var thisdata = this;
    
    if(searchKeyword != '' && searchKeyword != undefined){
        changeUrlWithoutLoad('q', encodeURIComponent(searchKeyword));
    }
    
    // selected box set in session
    var tempData = [];
    var price = getParameterByName('pricerange');
    if (checkboxValues == undefined) {
        $checkboxes = $(filter_class + " :checkbox");
        var checkboxValues = {};
        $checkboxes.each(function () {
            if (this.checked) {
                checkboxValues[this.id] = this.checked;
            }
        });
    }
    var minimumProductPrice = $('#Minprice' + categoryId).val();
    var maximumProductPrice = $('#Maxprice' + categoryId).val();

    if (minimumProductPrice != maximumProductPrice) {
        $(".pricerange-" + categoryId).slider('setValue', [minimumProductPrice, maximumProductPrice]);
    }
    if (price != '') {
        var prices = price.split('|');
        if (prices[0] != '' && prices[1] != '') {
            var priceData = '<span class="filters_cube" id="pricesession-' + categoryId + '">' + PriceSymbol + prices[0] + ' TO ' + PriceSymbol + prices[1] + '<i class="fa fa-times" aria-hidden="true"></i></span>';
        } else {
            var priceData = '';
        }
        var clearData = '<span class="filters_cube_clear  clearall" data-id="clearall-' + categoryId + '" id="clearall-' + categoryId + '">clear all</span>';
        $('#session' + categoryId).html(priceData + clearData);
    }
    $.each(checkboxValues, function (key, value) {
        $(filter_class + " ." + key).prop('checked', value);
        var chekCategory = key.split('-').slice(1, 2);
        if (chekCategory == categoryId) {
            if ($(filter_class + " #" + key).prop('checked') == true) {
                var element_data_name = $(filter_class + " #" + key).attr('data-showname');
                tempData.push('<span class="filters_cube" data-id="' + key + '">' + element_data_name + ' <i class="fa fa-times" aria-hidden="true"></i></span>');
            } else {
                tempData.push('');
            }

            //var myStr =  localStorage.getItem('mystring');
            var myStr = tempData.join(" ");
            if (price != null) {
                var prices = price.split('|');
                if (prices[0] != '' && prices[1] != '') {
                    var priceData = '<span class="filters_cube " id="pricesession-' + categoryId + '">' + PriceSymbol + prices[0] + ' TO ' + PriceSymbol + prices[1] + '<i class="fa fa-times" aria-hidden="true"></i></span>';
                } else {
                    var priceData = '';
                }
            }
            var clearData = '<span class="filters_cube_clear  clearall" data-id="clearall-' + categoryId + '"  id="clearall-' + categoryId + '">clear all</span>';
            $('#session' + categoryId).html(myStr + priceData + clearData);
            if (!$('.category' + categoryId).is(":checked")) {
                $('#session' + categoryId).html(" ");
            }
        }
    });
    getCheckboxValue(e, thisdata, filter_class);
});
$('body').delegate('.pagination_box', 'click', function (e) {
    changeUrl('page', $(this).attr("data-id"));
    var filter_class;
    filter_class = ".filter-main ul.filter-bar";
    var thisdata = this;
    getCheckboxValue(e, thisdata, filter_class);
});
$('body').delegate('.show-item-per-page', 'change', function (e) {
    $('.show-item-per-page').val(this.value);
    removeQString('page');
    changeUrl('limit', this.value);

    var filter_class;
    filter_class = ".filter-main ul.filter-bar";
    var thisdata = this;
    getCheckboxValue(e, thisdata, filter_class);
});
$('body').delegate('#shorting', 'change', function (e) {
    removeQString('page');
    if (this.value == '') {
        removeQString('sort');
        location.reload();
    } else {
        changeUrl('sort', this.value);
    }
    var filter_class;
    filter_class = ".filter-main ul.filter-bar";
    var thisdata = this;
    getCheckboxValue(e, thisdata, filter_class);
});
//$('body').on('slideStop click', '.sort_rang', function (e) {
//    getFilterData(e);
//});
$('body').on('slideStop click', '.pagesort', function (e) {
    var filter_class;
    filter_class = ".filter-main ul.filter-bar";
    var thisdata = this;
    removeQString('page');
    getCheckboxValue(e, thisdata, filter_class);
});

$('body').on('click', '.pagesortmob', function (e) {
    var filter_class;
    filter_class = ".filter-main ul.c-sidebar-menu";
    var thisdata = this;
    removeQString('page');
    getCheckboxValue(e, thisdata, filter_class);
});

$('body').on('click', '#pricesession-' + categoryId, function (e) {
    $('#pricesession-' + categoryId).remove();
    removeQString("pricerange");
    localStorage.removeItem("ispriceslider" + categoryId);
    if (!$('.category' + categoryId).is(":checked")) {
        $('#session' + categoryId).html(" ");
    }
    location.reload();
});

var isProcessing = false;
function mobile_product_list(e, checkboxValues, filter_class, pagenum, isScroll = false) {
    /* PRICE */
    var minimumProductPrice = $('#Minprice' + categoryId).val();
    var maximumProductPrice = $('#Maxprice' + categoryId).val();

    var priceRangeValue = minimumProductPrice + ',' + maximumProductPrice;
    var priceValue = priceRangeValue.split(',');
    if (e.type == "slideStop") {
        $(".price_range" + categoryId).val(e.value);
        var list, index;
        list = document.getElementsByClassName("pricerange-" + categoryId);
        for (index = 0; index < list.length; ++index) {
            list[index].setAttribute("data-slider-value", "[" + e.value + "]");
        }
        var priceValue = e.value;
        var priceRangeValue = priceValue;
        changeUrl("pricerange", priceRangeValue[0] + '|' + priceRangeValue[1]);
        localStorage.setItem("ispriceslider" + categoryId, "y");
    }
    var catMax = localStorage.getItem('catMaxVal' + categoryId);
    var catMin = localStorage.getItem('catMinVal' + categoryId);
    var ispriceSet = localStorage.getItem('ispriceslider' + categoryId);
    if (ispriceSet == "y") {
        var priceRangeValue = priceValue;
    } else {
        var priceRangeValue = [catMin, catMax];
    }
    /* PRICE */

    var page = pagenum;
    var limit = getParameterByName('limit');
    var sort = getParameterByName('sort');
    var category = getParameterByName('category');
    var brand = getParameterByName('brand');
    var attribute = getParameterByName('attribute');
    var rating = getParameterByName('rating');
    var price = getParameterByName('pricerange');

    var baseUrl = getBaseUrl();
    var other_data = '';
    var priceRangeValue = '';
    other_data += '&page=' + page;
    other_data += '&limit=' + limit;
    other_data += '&sort=' + sort;
    var categoryCommaValue = $('#categoryCommaValue').val();
    var productCommaValue = $('#productCommaValue').val();
    other_data += '&categoryId=' + categoryId;
    other_data += '&categoryCommaValue=' + categoryCommaValue;
    other_data += '&productCommaValue=' + productCommaValue;
    other_data += '&category=' + category;
    other_data += '&brand=' + brand;
    other_data += '&attribute=' + attribute;
    other_data += '&rating=' + rating;
    other_data += '&price=' + price;
    other_data += '&searchKeyword=' + encodeURIComponent(searchKeyword);
    other_data += '&promotionId=' + promotionId;

    var filterUrl = baseUrl + '/category/filter';
    var formData = other_data;

    if (isProcessing) {
        return;
    }
    isProcessing = true;
    $.ajax({
        url: filterUrl,
        type: 'post',
        dataType: "json",
        data: formData,
        beforeSend: function () {
            $(".loader").show();
        },
        success: function (response) {
            if (response['product_list'] != '') {
                $('#pageNumber').val(page);
                $('#totalCount').val(response['totalProductCount']);
                $('#perPageRecord').val(response['showItemValue']);

                $("#location_div").show();
                $('.product_listing_title').attr('style', 'display: block !important');
                if (isScroll == true) {
                    $("#mobile_category_products_list").append(response['productList']);
                } else {
                    $("#mobile_category_products_list").html(response['productList']);
                }
                // SHOW PER ITEM
                $('.show-items').html('');
                $(".filter-main").html(response['filterTopView']);
                $("#productCommaValue").val(response['productCommaValue']);
                // SORTING  
                $("#sort_by").html(response['sortSelectboxView']);
                var tempData = [];
                if (checkboxValues == undefined) {
                    $checkboxes = $(filter_class + " :checkbox");
                    var checkboxValues = {};
                    $checkboxes.each(function () {
                        if (this.checked) {
                            checkboxValues[this.id] = this.checked;
                        }
                    });
                }
                var minimumProductPrice = $('#Minprice' + categoryId).val();
                var maximumProductPrice = $('#Maxprice' + categoryId).val();

                if (minimumProductPrice != maximumProductPrice) {
                    $(".pricerange-" + categoryId).slider('setValue', [minimumProductPrice, maximumProductPrice]);
                }
                if (price != '') {
                    var prices = price.split('|');
                    if (prices[0] != '' && prices[1] != '') {
                        var priceData = '<span class="filters_cube" id="pricesession-' + categoryId + '">' + PriceSymbol + prices[0] + ' TO ' + PriceSymbol + prices[1] + '<i class="fa fa-times" aria-hidden="true"></i></span>';
                    } else {
                        var priceData = '';
                    }
                    var clearData = '<span class="filters_cube_clear  clearall" data-id="clearall-' + categoryId + '" id="clearall-' + categoryId + '">clear all</span>';
                    $('#session' + categoryId).html(priceData + clearData);
                }
                $.each(checkboxValues, function (key, value) {
                    $(filter_class + " ." + key).prop('checked', value);
                    var chekCategory = key.split('-').slice(1, 2);
                    if (chekCategory == categoryId) {
                        if ($(filter_class + " #" + key).prop('checked') == true) {
                            var element_data_name = $(filter_class + " #" + key).attr('data-showname');
                            tempData.push('<span class="filters_cube" data-id="' + key + '">' + element_data_name + ' <i class="fa fa-times" aria-hidden="true"></i></span>');
                        } else {
                            tempData.push('');
                        }

                        //var myStr =  localStorage.getItem('mystring');
                        var myStr = tempData.join(" ");
                        if (price != null) {
                            var prices = price.split('|');
                            if (prices[0] != '' && prices[1] != '') {
                                var priceData = '<span class="filters_cube " id="pricesession-' + categoryId + '">' + PriceSymbol + prices[0] + ' TO ' + PriceSymbol + prices[1] + '<i class="fa fa-times" aria-hidden="true"></i></span>';
                            } else {
                                var priceData = '';
                            }
                        }
                        var clearData = '<span class="filters_cube_clear  clearall" data-id="clearall-' + categoryId + '"  id="clearall-' + categoryId + '">clear all</span>';
                        $('#session' + categoryId).html(myStr + priceData + clearData);
                        if (!$('.category' + categoryId).is(":checked")) {
                            $('#session' + categoryId).html(" ");
                        }
                    }
                });
                $("img.lazy").lazyload();
            } else {
                $("#category_products_list").html('No product found');
                $(".product_listing_title").hide();
                $(".pagination_div_id").html('');
                $(".filter-main").html(response['filterTopView']);
            }
            $('.loader').css("display", "none");
            isProcessing = false;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            return false;
        }
    });
}

function getCheckboxValue(e, thisdata, filter_class) {
    var curkey = $(thisdata).attr("id");
    if (curkey != null) {
        var chekcurCategory = curkey.split('-').slice(0, 1);
    }
    var attdata = [];
    var branddata = [];
    var categorydata = [];
    var ratingdata = [];
    var attvalue = "";
    $checkboxes = $(filter_class + " :checkbox");
    var checkboxValues = {};
    if (chekcurCategory == "brand") {
        if (($(filter_class + " input[name='brand[]']:checked").length) == 0) {
            removeQString("brand");
        }
    }
    if (chekcurCategory == "category") {
        if (($(filter_class + " input[name='category[]']:checked").length) == 0) {
            removeQString("category");
        }
    }
    if (chekcurCategory == "attribute") {
        if (($(filter_class + " input[name='attribute[]']:checked").length) == 0) {
            removeQString("attribute");
        }
    }
    if (chekcurCategory == "rating") {
        if (($(filter_class + " input[name='rating[]']:checked").length) == 0) {
            removeQString("rating");
        }
    }
    $checkboxes.each(function () {
        if (this.checked) {
            checkboxValues[this.id] = this.checked;
        }
    });
    var acr = 0;
    var bcr = 0;
    var ccr = 0;
    var rcr = 0;
    $.each(checkboxValues, function (key, value) {
        $(filter_class + " ." + key).prop('checked', value);
        var chekCategory = key.split('-').slice(0, 1);
        var divkey = '';
        divkey = $(filter_class + " ." + key);
        if ($(divkey).prop('checked') == true) {
            var element_data_name = $(filter_class + " ." + key).attr('data-name');
            if (chekCategory == "attribute") {
                attdata.push(element_data_name);
                attvalue = attdata.filter(item => item !== element_data_name);
                attvalue = attdata.join("|");
                var akey = "attribute";
                acr++;
            } else if (chekCategory == "brand") {
                branddata.push(element_data_name);
                attvalue = branddata.join("|");
                var bkey = "brand";
                bcr++;
            } else if (chekCategory == "category") {
                categorydata.push(element_data_name);
                attvalue = categorydata.join("|");
                var ckey = "category";
                ccr++;
            } else if (chekCategory == "rating") {
                ratingdata.push(element_data_name);
                attvalue = ratingdata.join("|");
                var rkey = "rating";
                rcr++;
            }
            if (chekcurCategory == "brand") {
                if (bkey != undefined) {
                    if (($(filter_class + " input[name='brand[]']:checked").length) == bcr) {
                        changeUrl(bkey, attvalue);
                    }
                }
            }
            if (chekcurCategory == "category") {
                if (ckey != undefined) {
                    if (($(filter_class + " input[name='category[]']:checked").length) == ccr) {
                        changeUrl(ckey, attvalue);
                    }
                }
            }
            if (chekcurCategory == "attribute") {
                if (akey != undefined) {
                    if (($(filter_class + " input[name='attribute[]']:checked").length) == acr) {
                        changeUrl(akey, attvalue);
                    }
                }
            }
            if (chekcurCategory == "rating") {
                if (rkey != undefined) {
                    if (($(filter_class + " input[name='rating[]']:checked").length) == rcr) {
                        changeUrl(rkey, attvalue);
                    }
                }
            }
        }
    });

    var minimumProductPrice = $('#Minprice' + categoryId).val();
    var maximumProductPrice = $('#Maxprice' + categoryId).val();

    var priceRangeValue = minimumProductPrice + ',' + maximumProductPrice;
    var priceValue = priceRangeValue.split(',');
    if (e.type == "slideStop") {
        $(".price_range" + categoryId).val(e.value);
        var list, index;
        list = document.getElementsByClassName("pricerange-" + categoryId);
        for (index = 0; index < list.length; ++index) {
            list[index].setAttribute("data-slider-value", "[" + e.value + "]");
        }
        var priceValue = e.value;
        var priceRangeValue = priceValue;
        changeUrl("pricerange", priceRangeValue[0] + '|' + priceRangeValue[1]);
        localStorage.setItem("ispriceslider" + categoryId, "y");
    }
    var catMax = localStorage.getItem('catMaxVal' + categoryId);
    var catMin = localStorage.getItem('catMinVal' + categoryId);
    var ispriceSet = localStorage.getItem('ispriceslider' + categoryId);
    if (ispriceSet == "y") {
        var priceRangeValue = priceValue;
    } else {
        var priceRangeValue = [catMin, catMax];
    }

    if ($('#category_products_list').is(":visible")) {
        //getFilterData(e, checkboxValues, filter_class);
    } else {
        mobile_product_list(e, checkboxValues, filter_class, 1);
    }
}

//Define variable
var objQueryString = {};
//Get querystring value
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
//Add or modify querystring
function changeUrl(key, value) {
    //Get query string value
    var searchUrl = location.search;
    if (searchUrl.indexOf("?") == "-1") {
        var urlValue = '?' + key + '=' + value;
        history.pushState({state: 1, rand: Math.random()}, '', urlValue);
    } else {
        //Check for key in query string, if not present
        if (searchUrl.indexOf(key) == "-1") {
            var urlValue = searchUrl + '&' + key + '=' + value;
        } else {  //If key present in query string
            oldValue = getParameterByName(key);
            if (searchUrl.indexOf("?" + key + "=") != "-1") {
                urlValue = searchUrl.replace('?' + key + '=' + oldValue, '?' + key + '=' + value);
            } else {
                urlValue = searchUrl.replace('&' + key + '=' + oldValue, '&' + key + '=' + value);
            }
        }
        //var urlValue = encodeURI(urlValue);
        history.pushState({state: 1, rand: Math.random()}, '', urlValue);
        //history.pushState function is used to add history state.
        //It takes three parameters: a state object, a title (which is currently ignored), and (optionally) a URL.
    }
    objQueryString.key = value;
    location.reload();
}
function changeUrlWithoutLoad(key, value) {
    //Get query string value
    var searchUrl = location.search;
    if (searchUrl.indexOf("?") == "-1") {
        var urlValue = '?' + key + '=' + value;
        history.pushState({state: 1, rand: Math.random()}, '', urlValue);
    } else {
        //Check for key in query string, if not present
        if (searchUrl.indexOf(key) == "-1") {
            var urlValue = searchUrl + '&' + key + '=' + value;
        } else {  //If key present in query string
            oldValue = getParameterByName(key);
            if (searchUrl.indexOf("?" + key + "=") != "-1") {
                urlValue = searchUrl.replace('?' + key + '=' + oldValue, '?' + key + '=' + value);
            } else {
                urlValue = searchUrl.replace('&' + key + '=' + oldValue, '&' + key + '=' + value);
            }
        }
        //var urlValue = encodeURI(urlValue);
        history.pushState({state: 1, rand: Math.random()}, '', urlValue);
        //history.pushState function is used to add history state.
        //It takes three parameters: a state object, a title (which is currently ignored), and (optionally) a URL.
    }
    objQueryString.key = value;
}
//Function used to remove querystring
function removeQString(key) {
    var urlValue = document.location.href;
    //Get query string value
    var searchUrl = location.search;
    if (key != "") {
        oldValue = getParameterByName(key);
        removeVal = key + "=" + oldValue;
        if (searchUrl.indexOf('?' + removeVal + '&') != "-1") {
            urlValue = urlValue.replace('?' + removeVal + '&', '?');
        } else if (searchUrl.indexOf('&' + removeVal + '&') != "-1") {
            urlValue = urlValue.replace('&' + removeVal + '&', '&');
        } else if (searchUrl.indexOf('?' + removeVal) != "-1") {
            urlValue = urlValue.replace('?' + removeVal, '');
        } else if (searchUrl.indexOf('&' + removeVal) != "-1") {
            urlValue = urlValue.replace('&' + removeVal, '');
        }
    } else {
        var searchUrl = location.search;
        urlValue = urlValue.replace(searchUrl, '');
    }
    history.pushState({state: 1, rand: Math.random()}, '', urlValue);
    location.reload();
}

$("body").delegate("span.filters_cube", "click", function (e) {
    e.preventDefault();
    var filterid = 0;
    filterid = $(this).attr('data-id');
    if (filterid != 0 && filterid != undefined) {
        $('#' + filterid).trigger("click");
        location.reload();
    }
});


//FOR MOBILE
if ($('#mobile_category_products_list').length > 0 && $('#mobile_category_products_list').is(":visible")) {
    $(window).scroll(function (e) {
//        console.log('$(window).scrollTop() =' + $(window).scrollTop());
//        console.log('$(document).height() =' + $(document).height());
//        console.log('$(window).height() =' + $(window).height());
        if ($(window).scrollTop() >= ($(document).height() - $(window).height()) * 0.7) {
            var pageNumber = $("#pageNumber").val();
            var total_record = $("#totalCount").val();
            var perpage_record = $("#perPageRecord").val();
            if (parseInt(perpage_record) <= parseInt(total_record)) {
                var available_page = total_record / perpage_record;
                available_page = parseInt(available_page, 10);
                var mod_page = total_record % perpage_record;
                if (mod_page > 0) {
                    available_page = available_page + 1;
                }
                //if ($(".page_number:last").val() <= $(".total_record").val()) {
                if (parseInt(pageNumber) < parseInt(available_page)) {
                    var pagenum = parseInt(pageNumber) + 1;
                    var filter_class;
                    filter_class = ".filter-main ul.filter-bar";
                    var checkboxValues = null;
                    mobile_product_list(e, checkboxValues, filter_class, pagenum, isScroll = true);
                }
            }
        }
    });
}