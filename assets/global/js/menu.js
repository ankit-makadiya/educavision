//SIMPLE MENU
$(".c-menu-type-classic").delegate("a", "click", function (e) {
    var menuClass = $(e.target).attr('class');
    var menuUrl = $(this).attr('href');
    if (menuClass == 'dropdown-toggle') {
        location.href = menuUrl;
    }
});
$(".c-menu-type-mega").delegate("a", "click", function (e) {
    var menuClass = $(e.target).attr('class');
    var menuUrl = $(this).attr('href');
    if (menuClass == 'dropdown-toggle') {
        location.href = menuUrl;
    }
});
$('.dropdown-child a span.glyphicon').on('click', function (e) {
    e.preventDefault();
    if ($(this).closest('li').children('ul').hasClass("open")) {
        $(this).removeClass('glyphicon-minus');
        $(this).addClass('glyphicon-plus');
        $(this).closest('li').children('ul').removeClass("open");
    } else {
        $(this).addClass('glyphicon-minus').css('float', 'right');
        $(this).removeClass('glyphicon-plus');
        $(this).closest('li').children('ul').addClass("open")
    }
});
$(".c-visible-mobile .dropdown-menu li").click(function (e) {
    e.preventDefault();
    if ($(this).closest('li').children('ul').hasClass("open")) {
        $(this).find('span').removeClass('glyphicon-minus');
        $(this).find('span').addClass('glyphicon-plus');
        $(this).closest('li').children('ul').removeClass("open");
    } else {
        $(this).find('span').addClass('glyphicon-minus').css('float', 'right');
        $(this).find('span').removeClass('glyphicon-plus');

        $(".tab-content .dropdown-menu li").removeClass("open");
        $(this).closest('li').children('ul').addClass("open");
    }
});
if ($('.header-search-right-nav').length > 0) {

} else if ($('.header-search-right').length > 0) {

} else if ($('.header-wide-nav').length > 0) {

} else if ($('.header-wide-nav-dark').length > 0) {

} else if ($('.header-wide-nav-with-bg').length > 0) {
    $('.c-menu-type-classic').on('click', '.c-toggler', function (e) {
        if (App.getViewPort().width < App.getBreakpoint('md')) {
            e.preventDefault();
            $(this).closest("a").toggleClass('c-open');
        }
    });
} else if ($('.header-wide-nav-logo-center-bg').length > 0) {

} else if ($('.header-wide-nav-logo-right').length > 0) {

} else if ($('.header-wide-nav-with-bg-white').length > 0) {

} else if ($('.header-search-right-custom').length > 0) {
    $('body').delegate('.c-content-toggler', 'click', function () {
        $(this).parent().toggleClass('c-open');
        $(this).parent().siblings().find('.dropdown-menu').show()
    });
    $(".c-layout-sidebar-menu .c-sidebar-menu-toggler a.c-content-toggler").on('click', function () {
//        $('ul.c-sidebar-menu').removeClass('collapse');
//        $('ul.c-sidebar-menu').addClass('c-open');
    });
} else {
    $('.c-menu-type-classic').on('click', function (event) {
        event.preventDefault();
        event.stopPropagation();
        $(this).parent().parent().toggleClass('c-open');
        return false;
    });
    $('.c-menu-type-mega a').on('click', function (event) {
        $(this).parent().toggleClass('c-open');
        /*var menuClass = $(event.target).attr('class');
         var menuUrl = $(this).attr('href');
         if (menuClass == 'dropdown-toggle') {
         location.href = menuUrl;
         }*/
    });
    $('.c-menu-type-classic a').on('click', function (event) {
        $(this).parent().toggleClass('c-open');
    });
    $('body').delegate('.c-content-toggler', 'click', function () {
        $(this).parent().toggleClass('c-open');
    });
    $(".c-layout-sidebar-menu .c-sidebar-menu-toggler a.c-content-toggler").on('click', function () {
        $('ul.c-sidebar-menu').removeClass('collapse');
        $('ul.c-sidebar-menu').addClass('c-open');
    });
}