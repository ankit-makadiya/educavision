

//Init the geocoder library
var geocoder = new google.maps.Geocoder();
var geoAddress = [];
var directionsDisplay;
	var map;     var bounds = new google.maps.LatLngBounds();     
	var infowindow = new google.maps.InfoWindow();  
	function addMarker(lat, lan, title) {  
    
		map.setCenter(new google.maps.LatLng(lat, lan));         
		var marker = new google.maps.Marker({             
			map: map,             
			position: new google.maps.LatLng(lat, lan),             
			title: title         
		});        
		 bounds.extend(marker.position);         
		  google.maps.event.addListener(marker, "click", function () {             
		  	infowindow.setContent(this.title);             
		  	//infowindow.open(map, this);         
		  });     
		} 

function initialize()
 {     
 	directionsDisplay = new google.maps.DirectionsRenderer();   
 	  var myLatlng = new google.maps.LatLng(48.87146, 2.35500);     
 	   var mapOptions = {     
 	           zoom: 7,             
 	           center: myLatlng,           
 	       };  
 	       map = new google.maps.Map(document.getElementById("map"), mapOptions);

		 /* var beachMarker = new google.maps.Marker({
		    position: myLatlng,
		    map: map,
		  });*/
 	       google.maps.event.addListenerOnce(map, 'panel-direction', function (event) {          
 	            if (this.getZoom()> 15)  {
                this.setZoom(15);
            	}
            });
 	        map.fitBounds(bounds);         
			 directionsDisplay.setMap(map);         
			/* google.maps.event.addListenerOnce(map, 'bounds_changed', function (event) { 
			 	if (this.getZoom()> 15) { this.setZoom(15); } 
			 });*/
 			directionsDisplay.setPanel(document.getElementById("panel-direction"));
 			var count = 0;
 			$('#txtDestinationPoint option').each(function () {
 				if(count != 0){
		 				var value = $(this).val();
		 				var title = $(this).text();
		 				var data = value.split(',');
		 				console.log('(data[0]'+data[0]);
		 				console.log('(data[1]'+data[1]);
		 				console.log('title:'+title);
		            	addMarker(data[0],data[1],title);
		            }
		            count++;
            	});
  }

    google.maps.event.addDomListener(window, 'load', initialize);

//function showroute
ShowRouteMap = {
	initNavigateMap: function (mapID, panelDirectionID, startLatitude, startLongitude, endLatitude, endLongitude) {
		var directionsDisplay = new google.maps.DirectionsRenderer;
		var directionsService = new google.maps.DirectionsService;
		
		//initialize the map
		var map = new google.maps.Map(document.getElementById(mapID), {
		  zoom: 7,
		  center: {lat: startLatitude, lng: startLongitude}
		}); 
		
		//clear the direction panel
		$("#" + panelDirectionID).html("");
		directionsDisplay.setMap(map);
		directionsDisplay.setPanel(document.getElementById(panelDirectionID));

		//prepare the latitude and longitude data
		start = startLatitude + ", " + startLongitude;
		end = endLatitude + ", " + endLongitude;
		ShowRouteMap.calculateAndDisplayRoute(directionsService, directionsDisplay, start, end);
	},

	//function to get the driving route
	calculateAndDisplayRoute: function (directionsService, directionsDisplay, start, end) {
		directionsService.route({
		  origin: start,
		  destination: end,
		  travelMode: google.maps.TravelMode.DRIVING
		}, function(response, status) {
		  if (status === 'OK') {
			directionsDisplay.setDirections(response);
		  } else {
			alert('Directions request failed due to ' + status);
		  }
		});
	},
	//get geolocation based on address
	codeAddress: function (address) {
		return new Promise(function(resolve, reject){
			geocoder.geocode({ 'address': address }, function (results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					resolve(results);
				} else {
					reject(Error("Geocode for address " + address + " was not successful for the following reason: " + status));
				}
			});
		});
	},
	
	//function to get geolocation of both addresses.
	getGeolocationData: function(){
		if($("#txtStartingPoint").val() != "" && $("#txtDestinationPoint").val() != ""){
			geoAddress = [];
			ShowRouteMap.codeAddress($("#txtStartingPoint").val()).then(function(response){
				var geoData = {
					latitude: response[0].geometry.location.lat(),
					longitude: response[0].geometry.location.lng()
				}
				geoAddress.push(geoData);
			}).then(function(){
				return ShowRouteMap.codeAddress($("#txtDestinationPoint").val()).then(function(response){
					var geoData2 = {
						latitude: response[0].geometry.location.lat(),
						longitude: response[0].geometry.location.lng()
					}
					geoAddress.push(geoData2);
				});
				
			}).then(function(){
				ShowRouteMap.initNavigateMap("map_canvas", "panel-direction", geoAddress[0].latitude, geoAddress[0].longitude, geoAddress[1].latitude, geoAddress[1].longitude);
			});
		}else{
			alert("Please enter both addresses");
		}
	},
	
	//clear entries and map display
	clearEntries: function(){
		$("#txtStartingPoint, #txtDestinationPoint").val("");
		$("#map_canvas, #panel-direction").html("");
	}}
	$("document").ready(function () {
                $("body").delegate("#gmapopen", "click", function (event) {
                	if($("#txtStartingPoint").val() != "" || $("#txtDestinationPoint").val() != ""){
                    	window.open("https://maps.google.com?saddr=" + $("#txtStartingPoint").val() + "&daddr=" + $("#txtDestinationPoint").val(), '_blank');
                	}else{
						alert("Please select any address or enter enter address");
					}
                });
                $('.location_address').each(function () {
                    var link = "<a href='http://maps.google.com/maps?q=" + encodeURIComponent($(this).text()) + "' target='_blank' style='color:#111213;'>" + $(this).text() + "</a>";
                    $(this).html(link);
                });
    });