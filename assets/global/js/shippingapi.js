var lastUrl = lastUrlSegment(site_url);
var stateAjax = true;
var cityAjax = true;
function calculateCheckoutTax(ship_country, ship_state, ship_city, ship_zipcode, bill_country, bill_state, bill_city, bill_zipcode) {
    var noOfInstallment = $('#noOfInstallment').val();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: site_url + "/product/getCalculateCheckoutTax",
        cache: false,
        data: "ship_country=" + ship_country + '&ship_state=' + ship_state + '&ship_city=' + ship_city + '&ship_zipcode=' + ship_zipcode + '&bill_country=' + bill_country + '&bill_state=' + bill_state + '&bill_city=' + bill_city + '&bill_zipcode=' + bill_zipcode + '&noOfInstallment=' + noOfInstallment,
        success: function (response) {
            console.log(response);
            console.log(response.total_tax_new);
            if (PaymentType == 'fullpayment') {
                var taxtotalcart_without_currency = 0;
                var subtotalcart = response.totalcart_new;
                var finaltotalcart = response.totalcart_with_tax_new;
                var taxtotalcart = response.total_tax_new;
                var taxtotalcart_without_currency = response.total_tax;
                $('.c-subtotal').html(subtotalcart);
                $('.c-taxtotal').html(taxtotalcart);
                $('.c-shipping-total').html(finaltotalcart);
                $('#amount').val(response.cart_total_with_shipping_charge_tax);
                if ($(".c-taxtotal").attr('data-totaltax').length) {
                    $(".c-taxtotal").attr('data-totaltax', taxtotalcart_without_currency.toFixed(2));
                }
                $('.grand_total_with_tax_scharge').html(response.cart_total_with_shipping_charge_tax_new);
            } else {
                var taxtotalcart_without_currency = 0;
                var subtotalcart = response.installment.installment_sale_price_with_currency;
                var finaltotalcart = response.total_tax_with_installment_markup_new;
                var taxtotalcart = response.installment.installment_tax_with_currency;
                var taxtotalcart_without_currency = response.installment.installment_tax;
                $('.c-subtotal').html(subtotalcart);
                $('.c-taxtotal').html(taxtotalcart);
                $('.c-shipping-total').html(finaltotalcart);
                $('#amount').val(response.installment.first_installment_price);
                if ($(".c-taxtotal").attr('data-totaltax').length) {
                    $(".c-taxtotal").attr('data-totaltax', taxtotalcart_without_currency.toFixed(2));
                }
                $('.c-grandtotal').attr('data-grandtotal', response.installment.installment_total_payable_amount);
                $('.c-grandtotal').html(response.installment.installment_total_payable_amount_with_currency);
                $('.grand_total_with_tax_scharge').html(response.installment.first_installment_price_with_currency);
            }
        }
    });
}

if (lastUrl == "shoppingcart") {
    var session_state = '';
    var session_city = '';
    $('body').delegate('input.show_shipping_charge', 'click', function (e) { //alert("DD");
        //alert("JJJ"+$("input[name='radio_shipping_method_name']:checked").val());
        e.preventDefault();
        e.stopImmediatePropagation();
        $(".show_shipping_charge").removeAttr("checked");
        $(this).attr("checked", "checked");
        $(".show_shipping_charge").next().find('span.check').css("opacity", "0");
        $(".show_shipping_charge").next().find('span.check').css("transform", "none");
        $(this).next().find('span.check').css("opacity", "1");
        $(this).next().find('span.check').css("transform", "none");
        var selected_method_name = $(this).attr('data-radiobtnshippingslug');
        $("#selected_shipping_radio_btn").val(selected_method_name);
        if (selected_method_name == 'ups') {
            $("#shipping_method_ups").css("display", "block");
            return true;
        }
        if (selected_method_name == 'fedex') {
            $("#shipping_method_fedex").css("display", "block");
            return true;
        }
        if (selected_method_name == 'instorepickup') {
            $("#shipping_method_instorepickup").css("display", "block");
            return true;
        }
        if (selected_method_name == 'usps') {
            $("#shipping_method_usps").css("display", "block");
            return true;
        }
        $(".thirdparty_shipping option:selected").prop("selected", false);
        var shipping_method_id = $(this).attr('data-shipping-method-id');
        var shipping_method_name = $(this).attr('data-shipping-method');
        var shipping_method_slug = $(this).attr('data-shipping-slug');
        var shipping_options_id = $(this).val();
        var charge_symbol = $(this).attr('data-charge-symbol');
        var charge = $(this).attr('data-charge');
        var carttotalprice = $(".shpsubtotal").attr('data-subtotal');
        var total_tax = 0;
        $("#shipping_charge").html(charge_symbol);
        var countryId = $("#countryId").val();
        var stateId = $("#stateId").val();
        var cityId = $("#city").val();
        var zipcode = $("#zipcode").val();
        var baseUrl = getBaseUrl();
        var shipping_label = $(this).attr('data-label');
        bindShippingData(shipping_method_id, shipping_method_name, shipping_method_slug, shipping_options_id, charge, countryId, stateId, cityId, zipcode, baseUrl, shipping_label);
    });

    // $('body').delegate('input.show_free_shipping_charge', 'click', function (e) {
    //     e.preventDefault();
    //     e.stopImmediatePropagation();
    //     $(".thirdparty_shipping option:selected").prop("selected", false);
    //     var shipping_method_id = $(this).attr('data-shipping-method-id');
    //     var shipping_method_name = $(this).attr('data-shipping-method');
    //     var shipping_method_slug = $(this).attr('data-shipping-slug');
    //     var shipping_options_id = $(this).val();
    //     var charge_symbol = $(this).attr('data-charge-symbol');
    //     var charge = $(this).attr('data-charge');
    //     var carttotalprice = $(".shpsubtotal").attr('data-subtotal');
    //     var total_tax = 0;
    //     $("#shipping_charge").html(charge_symbol);
    //     var countryId = $("#countryId").val();
    //     var stateId = $("#stateId").val();
    //     var cityId = $("#city").val();
    //     var zipcode = $("#zipcode").val();
    //     var baseUrl = getBaseUrl();
    //     var shipping_label = $(this).attr('data-label');
    //     bindShippingData(shipping_method_id, shipping_method_name, shipping_method_slug, shipping_options_id, charge, countryId, stateId, cityId, zipcode, baseUrl, shipping_label);
    // });

    $('body').delegate('#shipping_method_fedex', 'change', function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        $(".show_shipping_charge").prop('checked', false);
        $('#shipping_method_usps option').attr("selected", false);
        $('#shipping_method_ups option').attr("selected", false);
        $('#shipping_method_instorepickup option').attr("selected", false);
        var shipping_method_id = $('option:selected', this).attr('data-shipping-method-id');
        var shipping_method_name = $('option:selected', this).attr('data-shipping-method');
        var shipping_method_slug = $('option:selected', this).attr('data-shipping-slug');
        var shipping_options_id = $('option:selected', this).attr('data-value');
        var charge_symbol = $('option:selected', this).attr('data-charge-symbol');
        var charge = $('option:selected', this).attr('data-charge');
        var shipping_label = $('option:selected', this).attr('data-label');
        var countryId = $("#countryId").val();
        var stateId = $("#stateId").val();
        var cityId = $("#city").val();
        var zipcode = $("#zipcode").val();
        var baseUrl = getBaseUrl();
        bindShippingData(shipping_method_id, shipping_method_name, shipping_method_slug, shipping_options_id, charge, countryId, stateId, cityId, zipcode, baseUrl, shipping_label);
    });
    $('body').delegate('#shipping_method_usps', 'change', function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        $(".show_shipping_charge").prop('checked', false);
        $('#shipping_method_fedex option').attr("selected", false);
        $('#shipping_method_ups option').attr("selected", false);
        $('#shipping_method_instorepickup option').attr("selected", false);
        var shipping_method_id = $('option:selected', this).attr('data-shipping-method-id');
        var shipping_method_name = $('option:selected', this).attr('data-shipping-method');
        var shipping_method_slug = $('option:selected', this).attr('data-shipping-slug');
        var shipping_options_id = $('option:selected', this).attr('data-value');
        var charge_symbol = $('option:selected', this).attr('data-charge-symbol');
        var charge = $('option:selected', this).attr('data-charge');
        var shipping_label = $('option:selected', this).attr('data-label');
        var carttotalprice = $(".shpsubtotal").attr('data-subtotal');
        var opt_value = $('option:selected', this).attr('value');
        var countryId = $("#countryId").val();
        var stateId = $("#stateId").val();
        var cityId = $("#city").val();
        var zipcode = $("#zipcode").val();
        var baseUrl = getBaseUrl();
        bindShippingData(shipping_method_id, shipping_method_name, shipping_method_slug, shipping_options_id, charge, countryId, stateId, cityId, zipcode, baseUrl, shipping_label);
    });
    $('body').delegate('#shipping_method_ups', 'change', function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        $(".show_shipping_charge").prop('checked', false);
        $('#shipping_method_fedex option').attr("selected", false);
        $('#shipping_method_usps option').attr("selected", false);
        $('#shipping_method_instorepickup option').attr("selected", false);
        var shipping_method_id = $('option:selected', this).attr('data-shipping-method-id');
        var shipping_method_name = $('option:selected', this).attr('data-shipping-method');
        var shipping_method_slug = $('option:selected', this).attr('data-shipping-slug');
        var shipping_options_id = $('option:selected', this).attr('data-value');
        var charge_symbol = $('option:selected', this).attr('data-charge-symbol');
        var charge = $('option:selected', this).attr('data-charge');
        var shipping_label = $('option:selected', this).attr('data-label');
        var carttotalprice = $(".shpsubtotal").attr('data-subtotal');
        var opt_value = $('option:selected', this).attr('value');
        var countryId = $("#countryId").val();
        var stateId = $("#stateId").val();
        var cityId = $("#city").val();
        var zipcode = $("#zipcode").val();
        var baseUrl = getBaseUrl();
        bindShippingData(shipping_method_id, shipping_method_name, shipping_method_slug, shipping_options_id, charge, countryId, stateId, cityId, zipcode, baseUrl, shipping_label);
    });
    $('body').delegate('#shipping_method_instorepickup', 'change', function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        $(".show_shipping_charge").prop('checked', false);
        $('#shipping_method_fedex option').attr("selected", false);
        $('#shipping_method_usps option').attr("selected", false);
        $('#shipping_method_ups option').attr("selected", false);
        var shipping_method_id = $('option:selected', this).attr('data-shipping-method-id');
        var shipping_method_name = $('option:selected', this).attr('data-shipping-method');
        var shipping_method_slug = $('option:selected', this).attr('data-shipping-slug');
        var shipping_options_id = $('option:selected', this).attr('data-value');
        var charge_symbol = $('option:selected', this).attr('data-charge-symbol');
        var charge = $('option:selected', this).attr('data-charge');
        var shipping_label = $('option:selected', this).attr('data-label');
        var carttotalprice = $(".shpsubtotal").attr('data-subtotal');
        var opt_value = $('option:selected', this).attr('value');
        var countryId = $("#countryId").val();
        var stateId = $("#stateId").val();
        var cityId = $("#city").val();
        var zipcode = $("#zipcode").val();
        var baseUrl = getBaseUrl();
        bindShippingData(shipping_method_id, shipping_method_name, shipping_method_slug, shipping_options_id, charge, countryId, stateId, cityId, zipcode, baseUrl, shipping_label);
    });
}

function bindShippingData(shipping_method_id, shipping_method_name, shipping_method_slug, shipping_options_id, charge, countryId, stateId, cityId, zipcode, baseUrl, shipping_label) {
    showLoader();
    $.ajax({
        type: "POST",
        cache: true,
        url: baseUrl + '/updateshippingdata',
        data: 'countryId=' + countryId + '&stateId=' + stateId + '&cityId=' + cityId + '&zipcode=' + zipcode + '&shipping_method_id=' + shipping_method_id + '&shipping_options_id=' + shipping_options_id + '&shipping_charge=' + charge + '&shipping_label=' + shipping_label + '&shipping_method_name=' + shipping_method_name + '&shipping_method_slug=' + shipping_method_slug,
        success: function (result) {
            // location.reload(true);
            if (wbGoogleAnalyticId != '' && wbStandardTracking == 1 && wbEcommerceTracking == 1) {
                gtag('event', 'set_checkout_option', {
                    "event_label": shipping_method_name,
                    "checkout_step": 1,
                    "checkout_option": "shipping method",
                    "value": shipping_method_name
                });
            }
            loadShopppingCart();
            return true;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            return false;
        },
    });
    hideLoader();
}

function bindShippingDataCheckoutPage(shipping_method_id, shipping_method_name, shipping_method_slug, shipping_options_id, charge, shipping_countryId, shipping_stateId, shipping_cityId, shipping_zipcode, billing_countryId, billing_stateId, billing_cityId, billing_zipcode, baseUrl, shipping_label) {
    showLoader();
    $.ajax({
        type: "POST",
        cache: true,
        url: baseUrl + '/checkoutupdateshippingdata',
        data: 'shipping_countryId=' + shipping_countryId + '&shipping_stateId=' + shipping_stateId + '&shipping_cityId=' + shipping_cityId + '&shipping_zipcode=' + shipping_zipcode + '&billing_countryId=' + billing_countryId + '&billing_stateId=' + billing_stateId + '&billing_cityId=' + billing_cityId + '&billing_zipcode=' + billing_zipcode + '&shipping_method_id=' + shipping_method_id + '&shipping_options_id=' + shipping_options_id + '&shipping_charge=' + charge + '&shipping_label=' + shipping_label + '&shipping_method_name=' + shipping_method_name + '&shipping_method_slug=' + shipping_method_slug,
        success: function (result) {
            if (wbGoogleAnalyticId != '' && wbStandardTracking == 1 && wbEcommerceTracking == 1) {
                gtag('event', 'set_checkout_option', {
                    "event_label": shipping_method_name,
                    "checkout_step": 1,
                    "checkout_option": "shipping method",
                    "value": shipping_method_name
                });
            }
            $(".loader").removeClass("hide");
            $(".loader").addClass('show');
            setTimeout(function () {
                location.reload(true);
            }, 1000);
            return true;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            return false;
        }
    });
}

if (lastUrl == "checkout") {
    getshippingoptins();
}
function getshippingoptins() {
    showLoader();
    $("#shipping_html").delegate("input.show_shipping_charge", "click", function (e) {
//  $(".thirdparty_shipping option:selected").prop("selected", false);
        $("#shipping_options_html").attr('checked', false);
        $(this).attr("checked", "checked");
        $(".show_shipping_charge").next().find('span.check').css("opacity", "0");
        $(".show_shipping_charge").next().find('span.check').css("transform", "none");
        $(this).next().find('span.check').css("opacity", "1");
        $(this).next().find('span.check').css("transform", "none");
        var selected_method_name = $(this).attr('data-radiobtnshippingslug');
        $("#selected_shipping_radio_btn").val(selected_method_name);
        if (selected_method_name == 'ups') {
            $("#shipping_method_ups").css("display", "block");
            return false;
        }
        if (selected_method_name == 'fedex') {
            $("#shipping_method_fedex").css("display", "block");
            return false;
        }
        if (selected_method_name == 'instorepickup') {
            $("#shipping_method_instorepickup").css("display", "block");
            return false;
        }
        if (selected_method_name == 'usps') {
            $("#shipping_method_usps").css("display", "block");
            return false;
        }
        var shipping_method_id = $(this).attr('data-shipping-method-id');
        var shipping_method_name = $(this).attr('data-shipping-method');
        var shipping_method_slug = $(this).attr('data-shipping-slug');
        var shipping_options_id = $(this).val();
        //var charge_symbol = $(this).attr('data-charge-symbol');
        var charge = $(this).attr('data-charge');
        var shipping_countryId = $("#shipping_countryId").val();
        var shipping_stateId = $("#form_shipping_stateId").val();
        var shipping_cityId = $("#form_shipping_city").val();
        var shipping_zipcode = $("#form_shipping_zipcode").val();
        var billing_countryId = $("#billing_countryId").val();
        var billing_stateId = $("#form_billing_stateId").val();
        var billing_cityId = $("#form_billing_city").val();
        var billing_zipcode = $("#form_billing_zipcode").val();
        var baseUrl = getBaseUrl();
        var shipping_label = $(this).attr('data-label');

        var selected_shipping_method_slug = $("#selected_shipping_method_slug").val();
        if ((shipping_countryId != '' && shipping_countryId != undefined && shipping_stateId != '' && shipping_stateId != undefined && shipping_cityId != '' && shipping_cityId != undefined && shipping_zipcode != '' && shipping_zipcode != undefined) || selected_shipping_method_slug == 'instorepickup') {
            if (billing_stateId == null) {
                billing_stateId = '';
            }
            if (billing_cityId == null) {
                billing_cityId = '';
            }
            bindShippingDataCheckoutPage(shipping_method_id, shipping_method_name, shipping_method_slug, shipping_options_id, charge, shipping_countryId, shipping_stateId, shipping_cityId, shipping_zipcode, billing_countryId, billing_stateId, billing_cityId, billing_zipcode, baseUrl, shipping_label);
        } else {
            $("#frmcheckout").data('bootstrapValidator').revalidateField('shipping_countryId');
            $("#frmcheckout").data('bootstrapValidator').revalidateField('shippingstateId');
            $("#frmcheckout").data('bootstrapValidator').revalidateField('form[shipping_city]');
            $("#frmcheckout").data('bootstrapValidator').revalidateField('form[shipping_zipcode]');
        }
    });
}
// function updateshippingCharge() {
//     $("input:radio.show_shipping_charge:checked").trigger("click");
// }
// updatewarrantyCharge();
function checkSelectedShippingOption() {
    //if($("#shipping_options_html").length > 0){
    var radio_btn_slug = $("#selected_shipping_radio_btn").val();// $("input[name='radio_shipping_method_name']:checked").attr('data-shipping-slug');
    //alert(radio_btn_slug);
    if (radio_btn_slug == 'instorepickup') {
        if ($("#shipping_method_instorepickup").val() == '') {
            swal("Error!", "Please select option of In-Store Pickup from Store Option", "error");
            return false;
        }
    }
    if (radio_btn_slug == 'usps') {
        if ($("#shipping_method_usps").val() == '') {
            swal("Error!", "Please select option of USPS from USPS Option", "error");
            return false;
        }
    }
    if (radio_btn_slug == 'ups') {
        if ($("#shipping_method_ups").val() == '') {
            swal("Error!", "Please select option of UPS from UPS Option", "error");
            return false;
        }
    }
    if (radio_btn_slug == 'fedex') {
        if ($("#shipping_method_fedex").val() == '') {
            swal("Error!", "Please select option of Fedex from Fedex Option", "error");
            return false;
        }
    }
    //}
    return true;
}
$(document).ready(function () {
    if (strstr(lastUrl, "checkout", false) == 'checkout') {
        $("html, body").animate({scrollTop: 0}, "slow");
    }
    //var session_state='';
    if ($('#shipping_countryId').length > 0 && $('#shipping_countryId').is(":visible")) {
        var countryId = $('#shipping_countryId').val();
        getCheckoutState(countryId, session_state, 'shipping');
        var stateId = $('#form_shipping_stateId').val();
        getCheckoutCity(session_state, session_city, 'shipping');
    }
    if ($('#billing_countryId').length > 0) {
        var countryId = $('#billing_countryId').val();
        getCheckoutState(countryId, session_state, 'billing');
        var stateId = $('#form_billing_stateId').val();
        getCheckoutCity(session_state, session_city, 'billing');
    }
    $("#shipping_html").delegate("#shipping_method_fedex", "change", function (e) {
// $(".show_shipping_charge").prop('checked', false);
// $('#shipping_method_usps option').attr("selected", false);
// $('#shipping_method_ups option').attr("selected", false);
        var shipping_method_id = $('option:selected', this).attr('data-shipping-method-id');
        var shipping_method_name = $('option:selected', this).attr('data-shipping-method');
        var shipping_method_slug = $('option:selected', this).attr('data-shipping-slug');
        var shipping_options_id = $('option:selected', this).attr('data-value');
        //var charge_symbol = $('option:selected', this).attr('data-charge-symbol');
        var charge = $('option:selected', this).attr('data-charge');
        var shipping_label = $('option:selected', this).attr('data-label');
        //var opt_value = $('option:selected', this).attr('value');
        var shipping_countryId = $("#shipping_countryId").val();
        var shipping_stateId = $("#form_shipping_stateId").val();
        var shipping_cityId = $("#form_shipping_city").val();
        var shipping_zipcode = $("#form_shipping_zipcode").val();
        var billing_countryId = $("#billing_countryId").val();
        var billing_stateId = $("#form_billing_stateId").val();
        var billing_cityId = $("#form_billing_city").val();
        var billing_zipcode = $("#form_billing_zipcode").val();
        var baseUrl = getBaseUrl();
        var selected_shipping_method_slug = $("#selected_shipping_method_slug").val();
        if ((shipping_zipcode != '' && shipping_zipcode != undefined) || selected_shipping_method_slug == 'instorepickup') {
            if (billing_stateId == null) {
                billing_stateId = '';
            }
            if (billing_cityId == null) {
                billing_cityId = '';
            }
            bindShippingDataCheckoutPage(shipping_method_id, shipping_method_name, shipping_method_slug, shipping_options_id, charge, shipping_countryId, shipping_stateId, shipping_cityId, shipping_zipcode, billing_countryId, billing_stateId, billing_cityId, billing_zipcode, baseUrl, shipping_label);
        }
    });
    $("#shipping_html").delegate("#shipping_method_usps", "change", function (e) {
// $(".show_shipping_charge").prop('checked', false);
//   $('#shipping_method_fedex option').attr("selected", false);
// $('#shipping_method_ups option').attr("selected", false);
        var shipping_method_id = $('option:selected', this).attr('data-shipping-method-id');
        var shipping_method_name = $('option:selected', this).attr('data-shipping-method');
        var shipping_method_slug = $('option:selected', this).attr('data-shipping-slug');
        var shipping_options_id = $('option:selected', this).attr('data-value');
        //var charge_symbol = $('option:selected', this).attr('data-charge-symbol');
        var charge = $('option:selected', this).attr('data-charge');
        var shipping_label = $('option:selected', this).attr('data-label');
        //var opt_value = $('option:selected', this).attr('value');
        var shipping_countryId = $("#shipping_countryId").val();
        var shipping_stateId = $("#form_shipping_stateId").val();
        var shipping_cityId = $("#form_shipping_city").val();
        var shipping_zipcode = $("#form_shipping_zipcode").val();
        var billing_countryId = $("#billing_countryId").val();
        var billing_stateId = $("#form_billing_stateId").val();
        var billing_cityId = $("#form_billing_city").val();
        var billing_zipcode = $("#form_billing_zipcode").val();
        var baseUrl = getBaseUrl();
        var selected_shipping_method_slug = $("#selected_shipping_method_slug").val();
        if ((shipping_zipcode != '' && shipping_zipcode != undefined) || selected_shipping_method_slug == 'instorepickup') {
            if (billing_stateId == null) {
                billing_stateId = '';
            }
            if (billing_cityId == null) {
                billing_cityId = '';
            }
            bindShippingDataCheckoutPage(shipping_method_id, shipping_method_name, shipping_method_slug, shipping_options_id, charge, shipping_countryId, shipping_stateId, shipping_cityId, shipping_zipcode, billing_countryId, billing_stateId, billing_cityId, billing_zipcode, baseUrl, shipping_label);
        }
    });
    $("#shipping_html").delegate("#shipping_method_ups", "change", function (e) {
// $(".show_shipping_charge").prop('checked', false);
// $('#shipping_method_fedex option').attr("selected", false);
//  $('#shipping_method_usps option').attr("selected", false);
        var shipping_method_id = $('option:selected', this).attr('data-shipping-method-id');
        var shipping_method_name = $('option:selected', this).attr('data-shipping-method');
        var shipping_method_slug = $('option:selected', this).attr('data-shipping-slug');
        var shipping_options_id = $('option:selected', this).attr('data-value');
        // var charge_symbol = $('option:selected', this).attr('data-charge-symbol');
        var charge = $('option:selected', this).attr('data-charge');
        var shipping_label = $('option:selected', this).attr('data-label');
        // var opt_value = $('option:selected', this).attr('value');
        var shipping_countryId = $("#shipping_countryId").val();
        var shipping_stateId = $("#form_shipping_stateId").val();
        var shipping_cityId = $("#form_shipping_city").val();
        var shipping_zipcode = $("#form_shipping_zipcode").val();
        var billing_countryId = $("#billing_countryId").val();
        var billing_stateId = $("#form_billing_stateId").val();
        var billing_cityId = $("#form_billing_city").val();
        var billing_zipcode = $("#form_billing_zipcode").val();
        var baseUrl = getBaseUrl();
        var selected_shipping_method_slug = $("#selected_shipping_method_slug").val();
        if ((shipping_zipcode != '' && shipping_zipcode != undefined) || selected_shipping_method_slug == 'instorepickup') {
            if (billing_stateId == null) {
                billing_stateId = '';
            }
            if (billing_cityId == null) {
                billing_cityId = '';
            }
            bindShippingDataCheckoutPage(shipping_method_id, shipping_method_name, shipping_method_slug, shipping_options_id, charge, shipping_countryId, shipping_stateId, shipping_cityId, shipping_zipcode, billing_countryId, billing_stateId, billing_cityId, billing_zipcode, baseUrl, shipping_label);
        }
    });
    // bindShippingData();
    $("#shipping_html").delegate("#shipping_method_instorepickup", "change", function (e) {
// $(".show_shipping_charge").prop('checked', false);
// $('#shipping_method_usps option').attr("selected", false);
// $('#shipping_method_ups option').attr("selected", false);
        var shipping_method_id = $('option:selected', this).attr('data-shipping-method-id');
        var shipping_method_name = $('option:selected', this).attr('data-shipping-method');
        var shipping_method_slug = $('option:selected', this).attr('data-shipping-slug');
        var shipping_options_id = $('option:selected', this).attr('data-value');
        //var charge_symbol = $('option:selected', this).attr('data-charge-symbol');
        var charge = $('option:selected', this).attr('data-charge');
        var shipping_label = $('option:selected', this).attr('data-label');
        //var opt_value = $('option:selected', this).attr('value');
        var shipping_countryId = $("#shipping_countryId").val();
        var shipping_stateId = $("#form_shipping_stateId").val();
        var shipping_cityId = $("#form_shipping_city").val();
        var shipping_zipcode = $("#form_shipping_zipcode").val();
        var billing_countryId = $("#billing_countryId").val();
        var billing_stateId = $("#form_billing_stateId").val();
        var billing_cityId = $("#form_billing_city").val();
        var billing_zipcode = $("#form_billing_zipcode").val();
        var baseUrl = getBaseUrl();
        var selected_shipping_method_slug = $("#selected_shipping_method_slug").val();
        if ((shipping_zipcode != '' && shipping_zipcode != undefined) || selected_shipping_method_slug == 'instorepickup') {
            if (billing_stateId == null) {
                billing_stateId = '';
            }
            if (billing_cityId == null) {
                billing_cityId = '';
            }
            bindShippingDataCheckoutPage(shipping_method_id, shipping_method_name, shipping_method_slug, shipping_options_id, charge, shipping_countryId, shipping_stateId, shipping_cityId, shipping_zipcode, billing_countryId, billing_stateId, billing_cityId, billing_zipcode, baseUrl, shipping_label);
        }
    });

    $('#countryId').on('select2:unselecting', function (e) {
        $("#stateId").val(null).trigger("change");
        $("#city").val(null).trigger("change");
    });
    $('#stateId').on('select2:unselecting', function (e) {
        $("#city").val(null).trigger("change");
    });
    $('#shippingcalculationfrm').bootstrapValidator({excluded: ':disabled', feedbackIcons: {valid: 'fa', invalid: 'err', validating: 'fa'}, fields: {'countryId': {validators: {notEmpty: {message: 'Please select Country'}, }}, 'stateId': {validators: {notEmpty: {message: 'Please select State / County'}, }}, 'city': {validators: {notEmpty: {message: 'Please select Town / City'}, }}, 'zipcode': {validators: {notEmpty: {message: 'Please enter Postcode / Zip'}, stringLength: {max: 15, message: 'Please enter valid Postcode / Zip'}, }}, }, submitHandler: function (validator, form, submitButton) {}});
    if ($('#shipping_countryId').size() > 0) {
        if (location_flag == "No") {
            $('#shipping_countryId').val(session_country).trigger('change');
        }
        if (session_zipcode != '') {
            $('#form_shipping_zipcode').val(session_zipcode);
        }
    }
    if ($('#billing_countryId').size() > 0) {
        if (location_flag == "No") {
            $('#billing_countryId').val(session_country).trigger('change');
        }
        if (session_zipcode != '') {
            $('#form_billing_zipcode').val(session_zipcode);
        }
    }
    if ($(".select2").length > 0) {
        if ($('.countryId').is(":visible")) {
            $(".countryId").select2({placeholder: "Select Country", allowClear: true, width: '100%', });
        }
        if ($('.stateId').is(":visible")) {
            $(".stateId").select2({placeholder: "Select State / County", allowClear: true, width: '100%', });
        }
        if ($('.city').is(":visible")) {
            $(".city").select2({placeholder: "Select Town / City", allowClear: true, width: '100%', });
        }
        $('#checkbox6-444').change(function () {
            if ($(this).is(':checked')) {
                $("#countryId").select2({placeholder: countryPlaceholder, allowClear: true, width: '100%', });
                $("#stateId").select2({placeholder: statePlaceholder, allowClear: true, width: '100%', });
                $("#city").select2({placeholder: cityPlaceholder, allowClear: true, width: '100%', });
            }
        });
        if ($('.addressId').is(":visible")) {
            $(".addressId").select2({placeholder: "Select Available Address", allowClear: true, width: '100%', });
        }
        if ($('.form_dealers').is(":visible")) {
            $(".form_dealers").select2({placeholder: "Select Dealers", allowClear: true, width: '100%', });
        }
        if ($('#shipping_countryId').is(":visible")) {
            $("#shipping_countryId").select2({placeholder: countryPlaceholder, allowClear: true, width: '100%', });
        }
        if ($('#form_shipping_stateId').is(":visible")) {
            $("#form_shipping_stateId").select2({placeholder: statePlaceholder, allowClear: true, width: '100%', });
        }
        if ($('#form_shipping_city').is(":visible")) {
            $("#form_shipping_city").select2({placeholder: cityPlaceholder, allowClear: true, width: '100%', });
        }
        $('#chkbilladdress').change(function () {
            if ($(this).is(':checked')) {
                $("#billing_countryId").select2({placeholder: countryPlaceholder, allowClear: true, width: '100%', });
                $("#form_billing_stateId").select2({placeholder: statePlaceholder, allowClear: true, width: '100%', });
                $("#form_billing_city").select2({placeholder: cityPlaceholder, allowClear: true, width: '100%', });

            }
        });
    }
    $('#calculatetaxmodal').on('shown.bs.modal', function (e) {
        $('#taxcalculationfrm #locationCountry').focus();
        if ($(".select2").length > 0) {
            $("#locationCountry").select2({placeholder: "Select Country", allowClear: true, width: '100%', dropdownParent: $("#calculatetaxmodal"), });
            $("#locationState").select2({placeholder: "Select State / County", allowClear: true, width: '100%', dropdownParent: $("#calculatetaxmodal"), });
            $("#locationCity").select2({placeholder: "Select Town / City", allowClear: true, width: '100%', dropdownParent: $("#calculatetaxmodal"), });
        }
        $('#taxcalculationfrm').bootstrapValidator({excluded: ':disabled', feedbackIcons: {valid: 'fa', invalid: 'err', validating: 'fa'}, fields: {'locationCountry': {validators: {notEmpty: {message: 'Please select Country'}, }}, 'locationState': {validators: {notEmpty: {message: 'Please select State / County'}, }}, 'locationCity': {validators: {notEmpty: {message: 'Please select Town / City'}, }}, 'locationZipcode': {validators: {notEmpty: {message: 'Please enter Postcode / Zip'}, stringLength: {max: 15, message: 'Please enter valid Postcode / Zip'}, }}, }, submitHandler: function (validator, form, submitButton) {}});
    });
});
$('#form_shipping_zipcode').on('change', function (event) {
    if ($(this).val() != '' && $(this).val() != undefined) {
        event.preventDefault();
        event.stopImmediatePropagation();
        var formData = $("#frmcheckout").serialize();
        var country = $("#shipping_countryId").val();
        var state = $("#form_shipping_stateId").val();
        var city = $("#form_shipping_city").val();
        if (country != '' && state != '' && city != '') {
            if (("#shipping_base_on").length > 0) {
                if ($("#shipping_base_on").val() == 'shipping_address' || $("#hdn_location_flag").val() == 'No')
                    showShippingOptions(formData);
            }
        } else {
            $("#frmcheckout").data('bootstrapValidator').revalidateField('shipping_countryId');
            $("#frmcheckout").data('bootstrapValidator').revalidateField('shippingstateId');
            $("#frmcheckout").data('bootstrapValidator').revalidateField('form[shipping_city]');
            $("#frmcheckout").data('bootstrapValidator').revalidateField('form[shipping_zipcode]');
        }
    }
});

$('#form_billing_zipcode').on('change', function (event) {
    if ($(this).val() != '' && $(this).val() != undefined) {
        event.preventDefault();
        event.stopImmediatePropagation();
        var formData = $("#frmcheckout").serialize();
        var country = $("#billing_countryId").val();
        var state = $("#form_billing_stateId").val();
        var city = $("#form_billing_city").val();
        if (country != '' && state != '' && city != '') {
            if (("#shipping_base_on").length > 0) {
                if ($("#shipping_base_on").val() != 'shipping_address')
                    showShippingOptions(formData);
            }
        } else {
            $("#frmcheckout").data('bootstrapValidator').revalidateField('billing_countryId');
            $("#frmcheckout").data('bootstrapValidator').revalidateField('billingstateId');
            $("#frmcheckout").data('bootstrapValidator').revalidateField('form[billing_city]');
            $("#frmcheckout").data('bootstrapValidator').revalidateField('form[billing_zipcode]');
        }
    }
});
function showShippingOptions(formData) {
    var baseUrl = getBaseUrl();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: baseUrl + "/shippingchargeoptions",
        cache: false,
        data: formData,
        beforeSend: function () {
            $(".loader").removeClass("hide");
            $(".loader").addClass('show');
        },
        success: function (response) {
            if (response['shipping_data'] != '') {
                $("#shipping_html").html(response['shipping_data']);
                if (response['is_error'] != 1) {
                    $("#accordian").hide();
                    $(".cls_shipping_charge").html('');
                    $("#is_shipping_calculated").val('');
                    $("#frmcheckout").data('bootstrapValidator').revalidateField('form[is_shipping_calculated]');
                    $("#delivery_address_div").css("display", "block");
                    if ($("#instorepickup_address_div").length > 0) {
                        $("#instorepickup_address_div").css("display", "none");
                    }
                } else {
                    $("#accordian").show();
                    $("#accordian").removeClass('hide');
                    $("#is_shipping_calculated").val(1);
                    $("#frmcheckout").data('bootstrapValidator').revalidateField('form[is_shipping_calculated]');
                    var grandtotal = $(".c-subtotal").attr('data-subtotal');
                    var selectedradioId = $('input[type=radio][name="radio_shipping_method_name"]:checked').attr('id');
                    var selectedradio_slug = $('input[type=radio][name="radio_shipping_method_name"]:checked').attr('data-shipping-slug');
                    if (selectedradio_slug != 'instorepickup') {
                        $("#delivery_address_div").css("display", "block");
                        if ($("#instorepickup_address_div").length > 0) {
                            $("#instorepickup_address_div").css("display", "none");
                        }
                    }
                    if (selectedradioId != undefined) {
                        var totaldiscount = 0;
                        var charge_symbol = $('input[type=radio][name="radio_shipping_method_name"]:checked').attr('data-charge-symbol');
                        var shipping_charge = $('input[type=radio][name="radio_shipping_method_name"]:checked').attr('data-charge');
                        $(".cls_shipping_charge").attr("data-shippingcharge", shipping_charge);
                        var tax = 0;
                        if ($(".c-taxtotal").length > 0) {
                            tax = $(".c-taxtotal").attr("data-totaltax");
                        }
                        if ($(".c-discounttotal").length > 0) {
                            totaldiscount = $(".c-discounttotal").attr('data-totaldiscount');
                        }
                        grandtotal = parseFloat(grandtotal) + parseFloat(shipping_charge) + parseFloat(tax) - parseFloat(totaldiscount);
                        grandtotal = grandtotal * session_currency_rate;
                        grandtotal = grandtotal.toFixed(2);
                        grandtotal = new Intl.NumberFormat('en-IN').format(grandtotal)

                        $(".cls_shipping_charge").html(charge_symbol);
                        if (PaymentType == 'fullpayment') {
                            $(".grand_total_with_tax_scharge").html(symbol + grandtotal);
                        }
                    }
                }
            } else {
                $("#shipping_html").html('');
            }
            $('#loading').css("display", "none");
        },
        complete: function () {
            $(".loader").removeClass("show");
            $(".loader").addClass('hide');
        },
    });
}
$('#shipping_countryId').on('change', function () {
    var ship_country = $('#shipping_countryId').val();
    getCheckoutState(ship_country, selectedState = '', 'shipping');
    var ship_state = $('#form_shipping_stateId').val();
    var ship_city = $('#form_shipping_city').val();
    var ship_zipcode = $('#form_shipping_zipcode').val();
    var bill_country = $('#billing_countryId').val();
    var bill_state = $('#form_billing_stateId').val();
    var bill_city = $('#form_billing_city').val();
    var bill_zipcode = $('#form_billing_zipcode').val();
    calculateCheckoutTax(ship_country, ship_state, ship_city, ship_zipcode, bill_country, bill_state, bill_city, bill_zipcode);
    $("#form_shipping_zipcode").trigger("change");
});
$('#form_shipping_stateId').on('change', function () {
    var ship_state = $('#form_shipping_stateId').val();
    $('#form_shipping_city').val('');
    getCheckoutCity(ship_state, selectedCity = '', 'shipping');
    var ship_country = $('#shipping_countryId').val();
    var ship_city = $('#form_shipping_city').val();
    var ship_zipcode = $('#form_shipping_zipcode').val();
    var bill_country = $('#billing_countryId').val();
    var bill_state = $('#form_billing_stateId').val();
    var bill_city = $('#form_billing_city').val();
    var bill_zipcode = $('#form_billing_zipcode').val();
    calculateCheckoutTax(ship_country, ship_state, ship_city, ship_zipcode, bill_country, bill_state, bill_city, bill_zipcode);
    $("#form_shipping_zipcode").trigger("change");
});
$('#form_shipping_city').on('change', function () {
    var ship_country = $('#shipping_countryId').val();
    var ship_state = $('#form_shipping_stateId').val();
    var ship_city = $('#form_shipping_city').val();
    var ship_zipcode = $('#form_shipping_zipcode').val();
    var bill_country = $('#billing_countryId').val();
    var bill_state = $('#form_billing_stateId').val();
    var bill_city = $('#form_billing_city').val();
    var bill_zipcode = $('#form_billing_zipcode').val();
    calculateCheckoutTax(ship_country, ship_state, ship_city, ship_zipcode, bill_country, bill_state, bill_city, bill_zipcode);
    $("#form_shipping_zipcode").trigger("change");
});
$('#form_shipping_zipcode').on('blur', function () {
    var ship_country = $('#shipping_countryId').val();
    var ship_state = $('#form_shipping_stateId').val();
    var ship_city = $('#form_shipping_city').val();
    var ship_zipcode = $('#form_shipping_zipcode').val();
    var bill_country = $('#billing_countryId').val();
    var bill_state = $('#form_billing_stateId').val();
    var bill_city = $('#form_billing_city').val();
    var bill_zipcode = $('#form_billing_zipcode').val();
    calculateCheckoutTax(ship_country, ship_state, ship_city, ship_zipcode, bill_country, bill_state, bill_city, bill_zipcode);
    $("#form_shipping_zipcode").trigger("change");
});
$('#billing_countryId').on('change', function () {
    var ship_country = $('#shipping_countryId').val();
    var ship_state = $('#form_shipping_stateId').val();
    var ship_city = $('#form_shipping_city').val();
    var ship_zipcode = $('#form_shipping_zipcode').val();
    var bill_country = $('#billing_countryId').val();
    getCheckoutState(bill_country, selectedState = '', 'billing');
    var bill_state = $('#form_billing_stateId').val();
    var bill_city = $('#form_billing_city').val();
    var bill_zipcode = $('#form_billing_zipcode').val();
    calculateCheckoutTax(ship_country, ship_state, ship_city, ship_zipcode, bill_country, bill_state, bill_city, bill_zipcode);
    $("#form_billing_zipcode").trigger("change");
});
$('#form_billing_stateId').on('change', function () {
    var ship_country = $('#shipping_countryId').val();
    var ship_state = $('#form_shipping_stateId').val();
    var ship_city = $('#form_shipping_city').val();
    var ship_zipcode = $('#form_shipping_zipcode').val();
    var bill_country = $('#billing_countryId').val();
    var bill_state = $('#form_billing_stateId').val();
    getCheckoutCity(bill_state, selectedCity = '', 'billing');
    var bill_city = $('#form_billing_city').val();
    var bill_zipcode = $('#form_billing_zipcode').val();
    calculateCheckoutTax(ship_country, ship_state, ship_city, ship_zipcode, bill_country, bill_state, bill_city, bill_zipcode);
    $("#form_billing_zipcode").trigger("change");
});
$('#form_billing_city').on('change', function () {
    var ship_country = $('#shipping_countryId').val();
    var ship_state = $('#form_shipping_stateId').val();
    var ship_city = $('#form_shipping_city').val();
    var ship_zipcode = $('#form_shipping_zipcode').val();
    var bill_country = $('#billing_countryId').val();
    var bill_state = $('#form_billing_stateId').val();
    var bill_city = $('#form_billing_city').val();
    var bill_zipcode = $('#form_billing_zipcode').val();
    calculateCheckoutTax(ship_country, ship_state, ship_city, ship_zipcode, bill_country, bill_state, bill_city, bill_zipcode);
    $("#form_billing_zipcode").trigger("change");
});
$('#form_billing_zipcode').on('blur', function () {
    var ship_country = $('#shipping_countryId').val();
    var ship_state = $('#form_shipping_stateId').val();
    var ship_city = $('#form_shipping_city').val();
    var ship_zipcode = $('#form_shipping_zipcode').val();
    var bill_country = $('#billing_countryId').val();
    var bill_state = $('#form_billing_stateId').val();
    var bill_city = $('#form_billing_city').val();
    var bill_zipcode = $('#form_billing_zipcode').val();
    calculateCheckoutTax(ship_country, ship_state, ship_city, ship_zipcode, bill_country, bill_state, bill_city, bill_zipcode);
    $("#form_billing_zipcode").trigger("change");
});
if ($('.datepicker').length > 0) {
    $('#rangeFrom').datepicker({format: 'mm/dd/yyyy', startDate: '0d', beforeShowDay: closedDay, firstDay: 1}).on('changeDate', function (e) {
        var minDate = new Date(e.date.valueOf());
        $('#rangeTo').datepicker('setStartDate', minDate);
        $('#frmcheckout').bootstrapValidator('revalidateField', 'rangeFrom');
        $('#frmcheckout').bootstrapValidator('revalidateField', 'rangeTo');
    });
    $('#rangeTo').datepicker({format: 'mm/dd/yyyy', startDate: '0d', beforeShowDay: closedDay, firstDay: 1}).on('changeDate', function (e) {
        var maxDate = new Date(e.date.valueOf());
        $('#rangeFrom').datepicker('setEndDate', maxDate);
        $('#frmcheckout').bootstrapValidator('revalidateField', 'rangeFrom');
        $('#frmcheckout').bootstrapValidator('revalidateField', 'rangeTo');
    });
    function closedDay(date) {
        if (shipping_days != '') {
            var ship_days = shipping_days.split(',');
            var total_days = ['1', '2', '3', '4', '5', '6'];
            var onDays = [];
            var offDays = [];
            for (var d = 0; d < total_days.length; d++) {
                if ($.inArray(total_days[d], ship_days) != -1) {
                    onDays.push(total_days[d]);
                } else {
                    offDays.push(total_days[d]);
                }
            }
            var day = date.getDay();
            if (day == 0) {
                return false;
            } else {
                for (var i = 0; i < offDays.length; i++) {
                    if (day == offDays[i]) {
                        return false;
                    }
                }
            }
            return true;
        } else {
            return true;
        }
    }
    $(".datepicker").on('change', function () {
        $("#frmcheckout").bootstrapValidator('revalidateField', "rangeFrom");
        $("#frmcheckout").bootstrapValidator('revalidateField', "rangeTo");
    });
}
$('body').delegate('#calculate_shipping_charge', 'click', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    $('#shippingcalculationfrm').bootstrapValidator({excluded: ':disabled', feedbackIcons: {valid: 'fa', invalid: 'err', validating: 'fa'}, fields: {'countryId': {validators: {notEmpty: {message: 'Please select Country'}, }}, 'stateId': {validators: {notEmpty: {message: 'Please select State / County'}, }}, 'city': {validators: {notEmpty: {message: 'Please select Town / City'}, }}, 'zipcode': {validators: {notEmpty: {message: 'Please enter Postcode / Zip'}, stringLength: {max: 15, message: 'Please enter valid Postcode / Zip'}, }}, }, submitHandler: function (validator, form, submitButton) {}});
    $('#shippingcalculationfrm').bootstrapValidator('revalidateField', 'countryId');
    $('#shippingcalculationfrm').bootstrapValidator('revalidateField', 'stateId');
    $('#shippingcalculationfrm').bootstrapValidator('revalidateField', 'city');
    $('#shippingcalculationfrm').bootstrapValidator('revalidateField', 'zipcode');
    if ($('#shippingcalculationfrm').bootstrapValidator('validate').has('.has-error').length === 0) {
        var formData = $("#shippingcalculationfrm").serialize();
        getShippingCharge(formData);
    } else {
        return false;
    }
});
// $('body').on('success.form.bv', '#shippingcalculationfrm', function(event) {
//     event.preventDefault();
//     event.stopImmediatePropagation();
//     var formData = $("#shippingcalculationfrm").serialize();
//     getShippingCharge(formData);
// });
$('#chkbilladdress').change(function () {
    if ($(this).is(':checked')) {
        $('#billing_firstName').val($('#shipping_firstName').val());
        $('#billing_middleName').val($('#shipping_middleName').val());
        $('#billing_lastName').val($('#shipping_lastName').val());
        $('#billing_companyName').val($('#shipping_companyName').val());
        $('#billing_emailid').val($('#shipping_emailid').val());
        $('#billing_contact').val($('#shipping_contact').val());
        if (($('#shipping_countryId').val() == $('#billing_countryId').val()) && $('#form_billing_city').val() == null && $('#form_billing_stateId').val() == null) {
            $('#billing_countryId').val($('#shipping_countryId').val());
            $("#form_billing_stateId").html($('#form_shipping_stateId').html());
            $("#form_billing_city").html($('#form_shipping_city').html());
            //getCheckoutState($('#shipping_countryId').val(),$('#form_shipping_stateId').val(),'billing');
            //getCheckoutCity($('#form_shipping_stateId').val(),$('#form_shipping_city').val(),'billing');
            $('#form_billing_zipcode').val($('#form_shipping_zipcode').val());
            $("#form_billing_zipcode").trigger("change");
        }
    }
});
function getShippingCharge(formData) {
    var baseUrl = getBaseUrl();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: baseUrl + "/shippingcharge",
        cache: false,
        data: formData,
        beforeSend: function () {
            showLoader();
        },
        success: function (response) {
            if (response['shipping_data'] != '') {
                $("#shipping_options_html").html(response['shipping_data']);
                var selectedradioId = $('.show_shipping_charge:checked').attr('id');
                $('input[id="' + selectedradioId + '"]:radio').trigger('click');

                // var selectedradioId = $('.show_free_shipping_charge:checked').attr('id');
                // $('input[id="' + selectedradioId + '"]:radio').trigger('click');
                loadShopppingCart();
                //location.reload(true);
            } else {
                $("#shipping_options_html").html('');
            }
            $('#loading').css("display", "none");
        },
        complete: function () {
            hideLoader();
        },
    });
}

function loadShopppingCart() {
    $.ajax({
        type: "GET",
        cache: false,
        url: '/shoppingcartajax',
        dataType: "json",
        beforeSend: function () {
            showLoader();
        },
        success: function (result) {
            $("#myShoppingCartDiv").html(result.html);
            $(".countryId").select2({placeholder: "Select Country", allowClear: true, width: '100%', });
            $(".stateId").select2({placeholder: "Select State", allowClear: true, width: '100%', });
            $(".city").select2({placeholder: "Select City", allowClear: true, width: '100%', });
            var first_installment_price_hdn_val = $('#first_installment_price_hdn_val').val();
            if (first_installment_price_hdn_val) {
                $('#first-payment-amount-val').text(first_installment_price_hdn_val);
            }
            $(".touchspin").TouchSpin({buttondown_class: 'btn red', buttonup_class: 'btn blue', min: 2, max: InstallmentData.max_no_of_installment, }).on('touchspin.on.startupspin', function () {
                var total_no_of_payments = parseInt($('#form_total_no_of_payments').val());
                var extra_charge = InstallmentData.first_installment_price - InstallmentData.installment_price - InstallmentData.down_payment;
                var new_set_amount = (parseFloat(InstallmentData.installment_total_amount) - extra_charge) / total_no_of_payments;
                var pre_first_installment = parseFloat(InstallmentData.first_installment_price) - parseFloat(InstallmentData.installment_price);
                var new_first_payment = pre_first_installment + new_set_amount;
                new_first_payment = new_first_payment * session_currency_rate;
                new_first_payment = symbol + new_first_payment.toFixed(2);
                $('#first-payment-amount-val').text(new_first_payment);
                $('#first_installment_price_hdn_val').val(new_first_payment);
                new_set_amount = new_set_amount * session_currency_rate;
                new_set_amount = symbol + new_set_amount.toFixed(2);
                $('#monthly_payment_amount').val(new_set_amount);
            }).on('touchspin.on.startdownspin', function () {
                console.log(InstallmentData);
                var total_no_of_payments = parseInt($('#form_total_no_of_payments').val());
                console.log("total_no_of_payments=>" + total_no_of_payments);
                console.log("installment_total_amount=>" + InstallmentData.installment_total_amount);
                var extra_charge = InstallmentData.first_installment_price - InstallmentData.installment_price - InstallmentData.down_payment;
                console.log("extra_charge=>" + extra_charge);
                var new_set_amount = (parseFloat(InstallmentData.installment_total_amount) - extra_charge) / total_no_of_payments;
                console.log("new_set_amount=>" + new_set_amount);
                console.log("first_installment_price=>" + InstallmentData.first_installment_price);
                console.log("installment_price=>" + InstallmentData.installment_price);
                var pre_first_installment = parseFloat(InstallmentData.first_installment_price) - parseFloat(InstallmentData.installment_price);
                console.log("pre_first_installment=>" + pre_first_installment);
                var new_first_payment = pre_first_installment + new_set_amount;
                console.log("new_first_payment=>" + new_first_payment);
                new_first_payment = new_first_payment * session_currency_rate;
                new_first_payment = symbol + new_first_payment.toFixed(2);
                $('#first-payment-amount-val').text(new_first_payment);
                $('#first_installment_price_hdn_val').val(new_first_payment);
                new_set_amount = new_set_amount * session_currency_rate;
                new_set_amount = symbol + new_set_amount.toFixed(2);
                $('#monthly_payment_amount').val(new_set_amount);
            });
            if (result.gtaData != '' && wbGoogleAnalyticId != '' && wbStandardTracking == 1 && wbEcommerceTracking == 1) {
                //var gtaData = JSON.parse(result.gtaData);
                gtag('event', 'checkout_progress', {"event_label": 'Checkout Progress', "items": result.gtaData.item, "coupon": result.gtaData.coupon});
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            return false;
        },
        complete: function () {
            hideLoader();
        },
    });
}

function getCheckoutState(countryId, selectedState, onChange) {
    if (stateAjax == true) {
        stateAjax = false;
        $.ajax({
            type: "POST",
            cache: true,
            url: '/getstate',
            data: 'ctrid=' + countryId + '&state_id=' + selectedState,
            beforeSend: function () {
                $(".loader").removeClass("hide");
                $(".loader").addClass('show');
            },
            success: function (result) {
                stateAjax = true;
                if (onChange == 'shipping') {
                    $('#form_shipping_stateId').html(result);
                } else if (onChange == 'billing') {
                    $('#form_billing_stateId').html(result);
                } else if (onChange == 'invoice') {
                    $('#invoice_state_id').html(result);
                } else {
                    $('.product_inquiry_stateId').html(result);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                return false;
            },
            complete: function () {
                $(".loader").removeClass("show");
                $(".loader").addClass('hide');
            },
        });
    }
}

function getCheckoutCity(stateId, selectedCity, onChange) {
    if (cityAjax == true) {
        cityAjax = false;
        $.ajax({
            type: "POST",
            cache: true,
            url: '/getcity',
            data: 'stateid=' + stateId + '&city_id=' + selectedCity,
            beforeSend: function () {
                $(".loader").removeClass("hide");
                $(".loader").addClass('show');
            },
            success: function (result) {
                if (onChange == 'shipping') {
                    $('#form_shipping_city').html(result);
                } else if (onChange == 'billing') {
                    $('#form_billing_city').html(result);
                } else if (onChange == 'invoice') {
                    $('#invoice_city').html(result);
                } else {
                    $('.product_inquiry_cityId').html(result);
                }
                cityAjax = true;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                return false;
            },
            complete: function () {
                $(".loader").removeClass("show");
                $(".loader").addClass('hide');
            }
        });
    }
}

if ($('.form_dealers').length > 0) {
    if ($('#hdnDealerId').val()) {
        //$('.form_dealers').val($('#hdnDealerId').val()).trigger('change');
        getDealerAddress($('#hdnDealerId').val());
    }
}
$('body').delegate('.form_dealers', 'change', function (e) {
    // e.preventDefault();
    // e.stopImmediatePropagation();
    var dealerId = $(this).val();
    getDealerAddress(dealerId);

});
function getDealerAddress(dealerId) {
    var baseUrl = getBaseUrl();
    $.ajax({
        type: 'post',
        cache: false,
        url: baseUrl + '/getdealers',
        data: 'dealerId=' + dealerId,
        dataType: 'json',
        beforeSend: function () {
            $(".loader").removeClass("hide");
            $(".loader").addClass('show');
        },
        success: function (result) {
            $('.addressId').html(result.ShippingAddrHtml);
            $('#shipping_firstName').val(result.customerAddress.firstName);
            $('#shipping_middleName').val(result.customerAddress.middleName);
            $('#shipping_lastName').val(result.customerAddress.lastName);
            $('#shipping_emailid').val(result.customerAddress.emailid);
            $('#shipping_contact').val(result.customerAddress.contact);
            $('#shipping_fax').val(result.customerAddress.fax);

            $("#frmcheckout").data('bootstrapValidator').revalidateField('form[shipping_address1]');
            $("#frmcheckout").data('bootstrapValidator').revalidateField('form[shipping_address2]');
            $("#frmcheckout").data('bootstrapValidator').revalidateField('shipping_countryId');
            $("#frmcheckout").data('bootstrapValidator').revalidateField('shippingstateId');
            $("#frmcheckout").data('bootstrapValidator').revalidateField('form[shipping_city]');
            $("#frmcheckout").data('bootstrapValidator').revalidateField('rangeFrom');
            $("#frmcheckout").data('bootstrapValidator').revalidateField('rangeTo');
            $("#frmcheckout").data('bootstrapValidator').revalidateField('form[shipping_zipcode]');
            $("#frmcheckout").data('bootstrapValidator').revalidateField('form[shipping_firstName]');
            $("#frmcheckout").data('bootstrapValidator').revalidateField('form[shipping_lastName]');
            $("#frmcheckout").data('bootstrapValidator').revalidateField('form[shipping_emailid]');
            $("#frmcheckout").data('bootstrapValidator').revalidateField('form[shipping_contact]');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            return false;
        },
        complete: function () {
            $(".loader").removeClass("show");
            $(".loader").addClass('hide');
        }
    });
}
$(document).ready(function() {
	$('body').delegate('.date-picker', 'change', function (e) {
		if ( $(this).prop('required') )
			$('#storefrmbuilder').bootstrapValidator('updateStatus', $(this), 'NOT_VALIDATED').bootstrapValidator('validateField', $(this));
	});
});