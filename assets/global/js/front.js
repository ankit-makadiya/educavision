var site_url = $(location).attr('href');
var siteUrl = document.location.origin;
var location_flag = "No";
function getBaseUrl() {
    return window.location.origin;
}
var site_url1 = getBaseUrl();
var forUrl = getBaseUrl();
function strstr(haystack, needle, bool) {
    var pos = 0;
    haystack += "";
    pos = haystack.indexOf(needle);
    if (pos == -1) {
        return false;
    } else {
        if (bool) {
            return haystack.substr(0, pos);
        } else {
            return haystack.slice(pos);
        }
    }
}
function lastUrlSegment(url) {
    var lastURLSegment = url.substr(url.lastIndexOf('/') + 1);
    return lastURLSegment;
}
function getkey(e) {
    if (window.event)
        return window.event.keyCode;
    else if (e)
        return e.which;
    else
        return null;
}
function goodchars(e, goods) {
    var key, keychar;
    key = getkey(e);
    if (key == null)
        return true;
    keychar = String.fromCharCode(key);
    keychar = keychar.toLowerCase();
    goods = goods.toLowerCase();
    if (goods.indexOf(keychar) != -1)
        return true;
    if (key == null || key == 0 || key == 8 || key == 9 || key == 13 || key == 27)
        return true;
    return false;
}
function toInteger(a) {
    var b;
    a = a.replace(/[$,]/g, "");
    if (a.indexOf('.') > 0) {
        b = a.replace(/[.]/g, "");
    } else {
        b = a * 100;
    }
    return b
}
$(window).load(resizeWindow());
$(window).resize(function () {
    resizeWindow();
});
function resizeWindow() {
    if ($('.container').length > 0) {
        var scrWidth = $(window).width();
        var conWidth = $('.container').css('width').replace("px", "");
        if (scrWidth < 1920 && scrWidth < conWidth) {
            $('.container').css('width', scrWidth + 'px');
        }
    }
}

/*function getCookie(name){
 var pattern = RegExp(name + "[0-9]")
 matched = document.cookie.match(pattern)
 if(matched){
 var cookie = matched[0].split('_')
 return cookie[1]
 }
 return false
 }
 */
// Cookies
function createCookie(name, value, days) {
    if (days) {//alert(name);
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    } else
        var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0)
            return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}

function showLoader() {
    $(".loader").show();
}
function hideLoader() {
    $(".loader").hide();
}
$(".closeAddDiv").click(function () {
    var tempPopupId1 = (this.id).replace("closeAddDiv_", "");
    $("#" + tempPopupId1).hide();
    $.cookie(tempPopupId1, 1, {expires: 1});
});

$(document).ready(function () {

    //$(document).on("click","a",function(){
    $('[data-toggle="popover"]').popover();
    loadMiniCart();
    window.setTimeout(function () {
        $(".alert").fadeTo(1000, 0).slideUp(1000, function () {
            $(this).remove();
        });
    }, 2500);
    $("[id^='frmPopup_']").each(function (e) {
        var popup_id = (this.id).replace("frmPopup_", "");
        $("#frmPopup_" + popup_id).bootstrapValidator({feedbackIcons: {valid: 'fa', invalid: 'err', validating: 'fa'}, fields: {'email': {validators: {notEmpty: {message: 'Please Enter Your Email'}, }}, 'phone': {validators: {notEmpty: {message: 'Please enter Phone'}, regexp: {regexp: /^(\+\d{1,3}[- ]?)?\d{10}$/, message: 'Phone is not in Proper Format'}}, }, }, submitHandler: function (validator, form, submitButton) {}});
    });
    $("body").delegate(".closePopup", "click", function (e) {
        var tempPopupId = (this.id).replace("btnClosePopup_", "");
        $("#marketing_popup_" + tempPopupId).hide();
        $.cookie(tempPopupId, 1, {expires: 1});
    });
    $("body").delegate(".closeNoThanks", "click", function (e) {
        var tempPopupId = (this.id).replace("btnNoThanks_", "");
        $("#marketing_popup_" + tempPopupId).hide();
        $.cookie(tempPopupId, 1, {expires: 1});
    });
    /*$("#search-suggestions").mouseleave(function () {
     $("#search-suggestions").hide();
     });
     $("#q").keyup(function () {
     $("#search-suggestions").hide();
     var search_allow_characters = 3;
     if ($("#search_charact_number").size() > 0) {
     search_allow_characters = $("#search_charact_number").val();
     }
     if (($("#q").val()).length >= search_allow_characters) {
     $.ajax({
     type: "POST",
     url: "/productSearch",
     data: 'q=' + $(this).val(),
     beforeSend: function () {},
     success: function (data) {
     $("#search-suggestions").show();
     $("#search-suggestions").html(data);
     $("#q").css("background", "#FFF");
     }
     });
     }
     });*/
    $('.captcha-error').hide();
});
$('body').on('success.form.bv', "[id^='frmPopup_']", function (event) {
    event.preventDefault();
    event.stopImmediatePropagation();
    var baseUrl = getBaseUrl();
    popup_id = (this.id).replace("frmPopup_", "");
    $.ajax({
        url: baseUrl + '/submitPopupAndSendCoupon',
        type: 'post',
        data: $("#frmPopup_" + popup_id).serialize(),
        dataType: "json",
        success: function (result) {
            if (result != '') {
                $("h3#popup_" + popup_id).html(result.coupon_page_title);
                $("#divPopupContent_" + popup_id).hide();
                htmlContent = "";
                htmlContent += '<div class="col-xs-12 col-sm-12 col-md-12"><b>' + result.message + '</b></div>';
                htmlContent += "<div class='clearfix'></div>";
                htmlContent += '<div class="col-xs-12 col-sm-12 col-md-12">' + result.coupon_content + '</div>';
                htmlContent += "<div class='clearfix'></div>";
                if (result.coupon_image != "") {
                    htmlContent += '<div class="col-xs-12 col-sm-12 col-md-12"><img src="' + baseUrl + '/assets/uploads/coupon/' + result.coupon_image + '"/></div>';
                    htmlContent += "<div class='clearfix'></div>";
                }
                htmlContent += '<div class="col-xs-12 col-sm-12 col-md-12">' + result.coupon_code + "</div>";
                htmlContent += "<div class='clearfix'></div>";
                htmlContent += '<div class="col-xs-12 col-sm-12 col-md-12">' + result.coupon_discount + "</div>";
                htmlContent += "<div class='clearfix'></div>";
                $("#divCouponSent_" + popup_id).show();
                $("#divCouponSent_" + popup_id).html(htmlContent);
            }
        },
    });
    $("#btnSendcoupon_" + popup_id).attr('disabled', false);
    return false;
});
if ($('#contactUs_form').length > 0) {
    $('#contactUs_form').bootstrapValidator({feedbackIcons: {valid: 'fa', invalid: 'err', validating: 'fa'}, fields: {'form[personName]': {validators: {notEmpty: {message: 'Please enter Name'}, stringLength: {min: 1, max: 100, message: 'Name between 1 to 100 character long'}, }}, 'form[personEmail]': {validators: {notEmpty: {message: 'Please enter Email-Id'}, emailAddress: {message: 'Please enter valid Email-Id'}, }, }, 'form[personPhone]': {validators: {notEmpty: {message: 'Please enter Contact Number'}, regexp: {regexp: /^[^-\s][0-9()+-\s]+(?:,\d+)*$/i, message: 'Contact Number is not in Proper Format'}}, }, 'form[contactComments]': {validators: {notEmpty: {message: 'Please enter Message'}, }}, }, submitHandler: function (validator, form, submitButton) {}});
    $('body').on('success.form.bv', '#contactUs_form', function (event) {
        event.preventDefault();
        event.stopImmediatePropagation();
        var formData = $(this).serialize();
        var baseUrl = getBaseUrl();
        $.ajax({
            url: baseUrl + '/contact/submit',
            type: 'post',
            data: formData,
            beforeSend: function () {
                showLoader();//$('body').Wload({text: 'Sending query...'});
            },
            success: function (result) {
                if (result > 0) {
                    hideLoader();
                    swal("Thanks!", "Thank you for contacting us, we will be in touch with you very soon!", "success");
                    $("#contactUs_form").data('bootstrapValidator').resetForm();
                    $("#contactUs_form")[0].reset();
                    if($('.g-recaptcha').attr('data-sitekey') > 0) {
                        grecaptcha.reset();
                    }
                } else {
                    hideLoader();
                    swal("Error!", "Please Complete the Captcha by clicking the Checkbox", "error");
                    $("#form_submit").attr('disabled', false);
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                return false;
            }
        })
    });
}
$('body').on('success.form.bv', '#frmsubscribe', function (event) {
    event.preventDefault();
    var formData = $(this).serialize();
    $.ajax({
        url: siteUrl + '/home/newsubscribe',
        type: 'post',
        data: formData,
        success: function (result) {
            swal("Subscribe!", "You have Subscribe successfully", "success");
            $("#frmsubscribe").data('bootstrapValidator').resetForm();
            $("#frmsubscribe")[0].reset();
            $.cookie("stickyclose", 1, {expires: 1});
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $("#frmsubscribe").data('bootstrapValidator').resetForm();
            $("#frmsubscribe")[0].reset();
        }
    })
});

if ($('#contactUs_form_mapless').length > 0) {
    $('#contactUs_form_mapless').bootstrapValidator({
        feedbackIcons: {
            valid: 'fa',
            invalid: 'err',
            validating: 'fa'
        },
        fields: {
            'form[source]': {
                validators: {
                    notEmpty: {
                        message: 'Please select the most appropriate topic concerning your questions'
                    },
                }
            },
            'form[personName]': {
                validators: {
                    notEmpty: {
                        message: 'Please enter First Name'
                    },
                    stringLength: {
                        min: 1,
                        max: 100,
                        message: 'First Name between 1 to 100 character long'
                    },
                }
            },
            'form[personLastName]': {
                validators: {
                    notEmpty: {
                        message: 'Please enter Last Name'
                    },
                    stringLength: {
                        min: 1,
                        max: 100,
                        message: 'Last Name between 1 to 100 character long'
                    },
                }
            },
            'form[address]': {
                validators: {
                    notEmpty: {
                        message: 'Please enter Address'
                    },
                },
            },
            'coutact_stateid': {
                validators: {
                    notEmpty: {
                        message: 'Please select State'
                    },
                },
            },
            'contact_city_id': {
                validators: {
                    notEmpty: {
                        message: 'Please select City'
                    },
                },
            },
            'contact_store_id': {
                validators: {
                    notEmpty: {
                        message: 'Please select a Store Location'
                    },
                },
            },
            'form[zipcode]': {
                validators: {
                    notEmpty: {
                        message: 'Please enter Zipcode'
                    },
                    stringLength: {max: 10, message: 'Please enter valid Zipcode'}
                },
            },
            'form[personPhone]': {
                validators: {
                    notEmpty: {
                        message: 'Please enter Phone Number'
                    },
                    stringLength: {min: 10, message: 'Phone Number must be 10 number long'},
                    regexp: {regexp: /^[^-\s][0-9()+-\s]+(?:,\d+)*$/i, message: 'Please enter valid Phone Number'},
                }
            },
            'form[personEmail]': {
                validators: {
                    notEmpty: {
                        message: 'Please enter Email Address'
                    },
                    emailAddress: {
                        message: 'Please enter valid Email Address'
                    },
                },
            },
            'form[contactComments]': {
                validators: {
                    notEmpty: {
                        message: 'Please enter Comments'
                    },
                }
            },
        },
        submitHandler: function (validator, form, submitButton) {}
    });
}
$('body').on('success.form.bv', '#contactUs_form_mapless', function (event) {
    event.preventDefault();
    event.stopImmediatePropagation();
    var formData = $(this).serialize();
    var baseUrl = getBaseUrl();
    $.ajax({
        url: baseUrl + '/contact/maplessubmit',
        type: 'post',
        data: formData,
        dataType: 'json',
        beforeSend: function () {
            showLoader();
        },
        success: function (result) {
            if (result.status == 'success') {
                hideLoader();
                swal("Thanks!", result.message, "success");
                $("#contactUs_form_mapless").data('bootstrapValidator').resetForm();
                $("#contactUs_form_mapless")[0].reset();
                if($('.g-recaptcha').attr('data-sitekey') > 0) {
                    grecaptcha.reset();
                }
            } else if (result.status == 'warning') {
                swal("Warning!", result.message, "warning");
                hideLoader();
                $("#form_submit").attr('disabled', false);
            } else {
                swal("Error!", result.message, "error");
                hideLoader();
                $("#form_submit").attr('disabled', false);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            return false;
        }
    })
});
if ($('#productsafetyform').size() > 0) {
    $('#productsafetyform').bootstrapValidator({
        feedbackIcons: {
            valid: 'fa',
            invalid: 'err',
            validating: 'fa'
        },
        fields: {
            'form[personName]': {
                validators: {
                    notEmpty: {
                        message: 'Please enter Name'
                    },
                    stringLength: {
                        min: 1,
                        max: 150,
                        message: 'Name between 1 to 150 character long'
                    },
                }
            },
            'form[personEmail]': {
                validators: {
                    notEmpty: {
                        message: 'Please enter Email'
                    },
                    emailAddress: {
                        message: 'Please enter valid Email'
                    },
                },
            },
            'form[address1]': {
                validators: {
                    notEmpty: {
                        message: 'Please enter Mailing Address 1'
                    },
                },
            },
            'form[personPhone]': {
                validators: {
                    notEmpty: {
                        message: 'Please enter Phone Number'
                    },
                    stringLength: {min: 10, message: 'Phone Number must be 10 number long'},
                    regexp: {regexp: /^[^-\s][0-9()+-\s]+(?:,\d+)*$/i, message: 'Please enter valid Phone Number'},
                }
            },
            'form[cedarSerialNumber]': {
                validators: {
                    notEmpty: {
                        message: 'Please enter Cedar Chest Serial Number'
                    },
                },
            },
        },
        submitHandler: function (validator, form, submitButton) {}
    });
}
$('body').on('success.form.bv', '#productsafetyform', function (event) {
    event.preventDefault();
    event.stopImmediatePropagation();
    var formData = $(this).serialize();
    var baseUrl = getBaseUrl();
    $.ajax({
        url: baseUrl + '/productsafety/requestsubmit',
        type: 'post',
        data: formData,
        beforeSend: function () {
            showLoader();
        },
        success: function (result) {
            if (result > 0) {
                hideLoader();
                swal("Thanks!", "Thank you for sending request, we will be in touch with you very soon!", "success");
                $("#productsafetyform").data('bootstrapValidator').resetForm();
                $("#productsafetyform")[0].reset();
                if($('.g-recaptcha').attr('data-sitekey') > 0) {
                    grecaptcha.reset();
                }
            } else {
                swal("Error!", "Please Complete the Captcha by clicking the Checkbox", "error");
                hideLoader();
                $("#form_submit").attr('disabled', false);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            return false;
        }
    })
});

$("body").delegate(".product_inquiry_countryId", "change", function (e) {
    var ctryid = "";
    ctryid = $(this).val();
    if (ctryid > 0 && ctryid != "") {
        getCheckoutState(ctryid, '', '');
    }
});
$("body").delegate(".product_inquiry_stateId", "change", function (e) {
    var stateid = "";
    stateid = $(this).val();
    var site_url1 = getBaseUrl();
    if (stateid > 0 && stateid != "") {
        getCheckoutCity(stateid, '', '')
    }
});
if ($('.carousel').length > 0) {
    var slider_id = $('.carousel').attr('id');
    if (slider_id == 'myCarousel') {
        $('#cms_page').find('.ke-advance-padding:first').attr('style', 'padding-top: 0px !important');
    }
}
$(function () {
    if (site_url.search("find-a-retailer") >= 0 && site_url.search("zipcode") >= 0) {
        $("#findaretailer_form #find_retailer").trigger("click");
    }
});
$("body").delegate("#find_retailer", "click", function (event) {
    var default_country = $("#default_country").val();
    var address = $("#retailer_find_zipcode").val();
    if ($("#retailer_find_zipcode").val() != '' && $("#retailer_find_radius").val() != '') {
        address = $("#retailer_find_zipcode").val();
        address = address + ',' + default_country;
    } else if ($("#retailer_find_city").val() != '' && $("#retailer_find_state").val() != '' && $("#retailer_find_radius").val() != '') {
        address = $("#retailer_find_city").val() + ',' + $("#retailer_find_state").val();
    } else {
        alert('Please enter zipcode or city and state along with radius');
        return false;
    }
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({address: address}, function (results, status) {
        //console.log(results[0].geometry.location);
        if (status == google.maps.GeocoderStatus.OK) {
            searchLocationsNear(results[0].geometry.location);
            $("#map").show();
            $("#searched_result_retailers").hide();
        } else {
            alert(address + ' not found');
        }
    });
    /* var formData = $("#findaretailer_form").serialize();
     var state_id = $('#retailer_find_state option:selected').attr('data_stateCode');
     if(state_id != '' && state_id!=undefined)
     formData = formData+'&state_id='+state_id;
     var baseUrl = getBaseUrl();
     $.ajax({url: baseUrl + '/findretailesdata', type: 'post', data:formData,
     success: function (result) {
     $("#map_canvas").show();
     var json_parse_data = $.parseJSON(result);
     $('#searched_result_retailers').html(json_parse_data.html);
     $('#retailer_pagination').html(json_parse_data.pagination_data);
     var data1 = $.parseJSON(json_parse_data.address_data);//$.parseJSON(json_parse_data.address_data);
     $.each(data1, function (data,val) {
     console.log(val);
     addMarkerHtml(val.latitude, val.longitude,val.lat_lng_link);
     });
     },
     error: function (jqXHR, textStatus, errorThrown) {
     return false;
     }
     });*/
});
$("body").delegate(".findaretailer_pagination", "click", function (event) {
    var page_no = $(this).attr('pageid');
    var formData = $("#findaretailer_form").serialize();
    var state_id = $('#retailer_find_state option:selected').attr('data_stateCode');
    if (state_id != '' && state_id != undefined)
        formData = formData + '&state_id=' + state_id;
    if (page_no != '' && page_no != undefined)
        formData = formData + '&pageno=' + page_no;
    var baseUrl = getBaseUrl();
    $.ajax({
        url: baseUrl + '/findretailesdata',
        type: 'post',
        data: formData,
        success: function (result) {
            var json_parse_data = $.parseJSON(result);
            $('#searched_result_retailers').html(json_parse_data.html);
            $('#retailer_pagination').html(json_parse_data.pagination_data);
            var data1 = $.parseJSON(json_parse_data.address_data); //$.parseJSON(json_parse_data.address_data);
            $.each(data1, function (data, val) {
                addMarkerHtml(val.latitude, val.longitude, val.lat_lng_link);
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            return false;
        }
    });
});
/* Google Map */
if ($("#map").length > 0) {
    var map;
    var markers = [];
    var infoWindow;
    var locationSelect;

    function initMap() {
        var sydney = {lat: -33.863276, lng: 151.107977};
        map = new google.maps.Map(document.getElementById('map'), {
            center: sydney,
            zoom: 11,
            mapTypeId: 'roadmap',
            mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU}
        });
        infoWindow = new google.maps.InfoWindow();
    }

    function searchLocations() {
        var address = document.getElementById("addressInput").value;
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({address: address}, function (results, status) {
            console.log(results);
            alert(results[0]);
            if (status == google.maps.GeocoderStatus.OK) {
                searchLocationsNear(results[0].geometry.location);
            } else {
                alert(address + ' not found');
            }
        });
    }

    function clearLocations() {
        infoWindow.close();
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
        markers.length = 0;

        locationSelect.innerHTML = "";
        var option = document.createElement("option");
        option.value = "none";
        option.innerHTML = "See all results:";
        locationSelect.appendChild(option);
    }

    function searchLocationsNear(center) {
        //clearLocations();
        var forUrl = getBaseUrl();
        var radius = $("#retailer_find_radius").val(); // document.getElementById('radiusSelect').value;
        var is_store_location = '';
        if ($("#is_store_location").val() > 0) {
            is_store_location = 1;
        }
        var searchUrl = forUrl + '/findretailermarker?lat=' + center.lat().toFixed(3) + '&lng=' + center.lng().toFixed(3) + '&radius=' + radius + '&is_store_location=' + is_store_location;
        downloadUrl(searchUrl, function (data) {
            var xml = parseXml(data);
            var markerNodes = xml.documentElement.getElementsByTagName("marker");
            var bounds = new google.maps.LatLngBounds();
            var html = '';
            if (markerNodes.length > 0) {
                html = '<div class="pagination">Showing <strong>1-' + markerNodes.length + '</strong> result of <strong>' + markerNodes.length + '</strong></div>';
                for (var i = 0; i < markerNodes.length; i++) {
                    console.log(markerNodes[i]);
                    var id = markerNodes[i].getAttribute("id");
                    var name = markerNodes[i].getAttribute("name");
                    var address = markerNodes[i].getAttribute("address");
                    var city_name = markerNodes[i].getAttribute("city");
                    var state_name = markerNodes[i].getAttribute("state");
                    var latlng = new google.maps.LatLng(parseFloat(markerNodes[i].getAttribute("lat")), parseFloat(markerNodes[i].getAttribute("lng")));
                    var distance = parseFloat(markerNodes[i].getAttribute("distance")).toFixed(2);
                    var latitude = parseFloat(markerNodes[i].getAttribute("latitude"));
                    var longitude = parseFloat(markerNodes[i].getAttribute("longitude"));
                    var store_hours = parseFloat(markerNodes[i].getAttribute("store_hours"));
                    var storeTime = markerNodes[i].getAttribute("storeTime");
                    var contact = markerNodes[i].getAttribute("contact");
                    var website_url = markerNodes[i].getAttribute("website_url");
                    var direction_link = markerNodes[i].getAttribute("direction_link");
                    var lat_lng_link = markerNodes[i].getAttribute("lat_lng_link");
                    var zipcode = markerNodes[i].getAttribute("zipcode");
                    direction_link = '<p><a href="' + direction_link + '" class="btn dir-btn" target="_blank">Get Directions ></a></p>';
                    if (website_url != '') {
                        website_url = '<p><a class="btn view-btn" href="' + website_url + '" target="_blank">View Website ></a></p>';
                    }
                    //var lat_lng_link = '<a rel="nofollow" href="http://maps.google.com/maps?q=loc:'+markerNodes[i].getAttribute("lat")+','+markerNodes[i].getAttribute("lng").'" target="_blank"><strong>'.$name.'</strong> <br/>'.$address.'<br/>'.$city_name.', '.$state_name.' '.$zipcode.'<br/>'.$contact_number.'</a>';
                    html += '<div class="row"><div class="col-xs-12 col-sm-12 col-md-12"><h3>' + name + ' <strong>(' + distance + 'mi)</strong></h3></div>';
                    html += '<div class="col-xs-12 col-sm-4 col-md-5"><p>' + address + '<br /> ' + city_name + ', ' + state_name + ', ' + zipcode + '<br> ' + contact + '</p></div>';
                    html += '<div class="col-xs-12 col-sm-4 col-md-4">' + storeTime + '</div>';
                    html += '<div class="col-xs-12 col-sm-4 col-md-3">' + website_url + direction_link + '</div> <div class="col-xs-12 col-sm-12 col-md-12"><p class="c-line"></p></div></div>';
                    //createOption(name, distance, i);
                    createMarker(latlng, name, lat_lng_link);
                    bounds.extend(latlng);
                }
            } else {
                html = 'No Records Found';
            }
            $("#searched_result_retailers").show();
            $("#searched_result_retailers").html(html);
            map.fitBounds(bounds);
        });
    }

    function createMarker(latlng, name, address) {
        var html = "<b>" + name + "</b> <br/>" + address;
        var marker = new google.maps.Marker({
            map: map,
            position: latlng
        });
        google.maps.event.addListener(marker, 'click', function () {
            infoWindow.setContent(html);
            infoWindow.open(map, marker);
        });
        markers.push(marker);
    }

    function createOption(name, distance, num) {
        var option = document.createElement("option");
        option.value = num;
        option.innerHTML = name;
        locationSelect.appendChild(option);
    }

    function downloadUrl(url, callback) {
        var request = window.ActiveXObject ?
                new ActiveXObject('Microsoft.XMLHTTP') :
                new XMLHttpRequest;

        request.onreadystatechange = function () {
            if (request.readyState == 4) {
                request.onreadystatechange = doNothing;
                callback(request.responseText, request.status);
            }
        };

        request.open('GET', url, true);
        request.send(null);
    }

    function parseXml(str) {
        if (window.ActiveXObject) {
            var doc = new ActiveXObject('Microsoft.XMLDOM');
            doc.loadXML(str);
            return doc;
        } else if (window.DOMParser) {
            return (new DOMParser).parseFromString(str, 'text/xml');
        }
    }

    function doNothing() {}
}
/* Google Map */
$("#filter").delegate(".c-arrow, .c-content-toggler", "click", function (event) {
    $('ul#sidebar-menu-1 li').removeClass('c-open');
});

if ($('#fmdstore').length > 0 && $('.c-layout-page').length > 1) {
    $('#cms_page').parent('.c-layout-page').attr('style', 'margin: 0px !important');
}
$(".login_form").click(function () {
    //$('a.dropdown-toggle').parent("li").removeClass('open');
    $(this).parent("li").toggleClass('open');
});

/* Tax Calucalation PopUp Header: START */
$('#calculatetaxmodal').on('shown.bs.modal', function (e) {
    $('#taxcalculationfrm #locationCountry').focus();
    if ($(".select2").length > 0) {
        $("#locationCountry").select2({placeholder: "Select Country", allowClear: true, width: '100%', dropdownParent: $("#calculatetaxmodal"), });
        $("#locationState").select2({placeholder: "Select State / County", allowClear: true, width: '100%', dropdownParent: $("#calculatetaxmodal"), });
        $("#locationCity").select2({placeholder: "Select Town / City", allowClear: true, width: '100%', dropdownParent: $("#calculatetaxmodal"), });
    }
    $('#taxcalculationfrm').bootstrapValidator({excluded: ':disabled', feedbackIcons: {valid: 'fa', invalid: 'err', validating: 'fa'}, fields: {'locationCountry': {validators: {notEmpty: {message: 'Please select Country'}, }}, 'locationState': {validators: {notEmpty: {message: 'Please select State / County'}, }}, 'locationCity': {validators: {notEmpty: {message: 'Please select Town / City'}, }}, 'locationZipcode': {validators: {notEmpty: {message: 'Please enter Postcode / Zip'}, stringLength: {max: 15, message: 'Please enter valid Postcode / Zip'}, }}, }, submitHandler: function (validator, form, submitButton) {}});
});
$('body').on('success.form.bv', '#taxcalculationfrm', function (event) {
    event.preventDefault();
    event.stopImmediatePropagation();
    var baseUrl = getBaseUrl();
    var formData = $("#taxcalculationfrm").serialize();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: baseUrl + "/location_details",
        cache: false,
        data: formData,
        success: function (response) {
            if (response > 0) {
                $('#calculatetaxmodal').modal('hide');
                location.reload(true);
            }
        }
    });
});
$('#locationCountry').on('select2:unselecting', function (e) {
    $("#locationState").val(null).trigger("change");
    $("#locationCity").val(null).trigger("change");
});
$('#locationState').on('select2:unselecting', function (e) {
    $("#locationCity").val(null).trigger("change");
});
/* Tax Calucalation PopUp Header: END */
function loadMiniCart() {
    $.ajax({
        type: "GET",
        url: forUrl + '/minicartajax',
        cache: false,
        dataType: 'json',
        success: function (result) {
            $(".modshopcartbox").html(result.cartHtml);
            if (result.cartCount > 0) {
                $('.totalcart').removeClass('hide');
                $('.totalcart').html(result.cartCount);
            } else {
                $('.totalcart').html('');
                $('.totalcart').addClass('hide');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            return false;
        },
    });
}

$("body").delegate(".addtowishlist", "click", function (e) {
    if ($("#landingpagepopup").size() > 0) {
        return;
    }
    var custid = $("#hdn_custid").val();
    if (custid == "") {
        $('#login-form').modal('show');
        return false;
    }
    var prdid = "";
    prdid = $(this).attr("data-name");
    var product_original_name = '';
    product_original_name = $(this).attr('data-original-name');
    var commaseparated_attrid = $("#commaseparated_attribute_id").val();
    if (commaseparated_attrid == undefined) {
        commaseparated_attrid = '';
    }
    var site_url1 = getBaseUrl();
    if (prdid != "") {
        $.ajax({
            url: site_url1 + '/product/addtowishlist',
            type: 'post',
            data: 'prdname=' + prdid + '&custid=' + custid + '&commaseparated_attrid=' + commaseparated_attrid,
            success: function (result) {
                if (result > 0) {
                    if (!$(".wishlist_counter").hasClass('c-cart-number')) {
                        $(".wishlist_counter").addClass('c-cart-number');
                    }
                    $(".wishlist_counter").html(result);
                    if (wbGoogleAnalyticId != '' && wbStandardTracking == 1 && wbEcommerceTracking == 1) {
                        gtag('event', 'add_to_wishlist', {"event_label": product_original_name});
                    }
                    swal("Wishlist!", "Product added to Wishlist successfully", "success");
                } else {
                    swal("Wishlist!", "Product is alreday in Wishlist", "info");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                return false;
            }
        });
    }
});

$('.marking_label').on('click', function () {
    if (wbGoogleAnalyticId != '' && wbStandardTracking == 1) {
        var marking_id = $(this).data('id');
        var marking_name = $(this).data('name');
        if (marking_id != '' && marking_name != '') {
            gtag('event', 'select_content', {"event_label": "promotions", "promotions": [{"id": marking_id, "name": marking_name}]});
        }
    }
});

if ($('#container_flipbook').length > 0) {
    var arr = [];
    $.each(array_data, function (key, value) {
        arr.push({"src": value['src'], "thumb": value['thumb'], "title": value['title'], "htmlContent": value['htmlContent']
        });
    });
    var ecircularId = $("#ecircularId").val();
    var ecircularPdf = $("#ecircularPdf").val();
    $("#container_flipbook").flipBook({
        pages: arr,
        btnAutoplay: {
            enabled: menuOptions.btnAutoplay,
            title: "Autoplay",
            icon: "fa-play",
            iconAlt: "fa-pause"
        },
        btnZoomIn: {
            enabled: menuOptions.btnZoomIn,
            title: "Zoom in",
            icon: "fa-plus"
        },
        btnZoomOut: {
            enabled: menuOptions.btnZoomOut,
            title: "Zoom out",
            icon: "fa-minus"
        },
        btnToc: {
            enabled: menuOptions.btnToc,
            title: "Table of content",
            icon: "fa-list-ol"
        },
        btnThumbs: {
            enabled: menuOptions.btnThumbs,
            title: "Pages",
            icon: "fa-th-large"
        },
        btnShare: {
            enabled: menuOptions.btnShare,
            title: "Share",
            icon: "fa-link"
        },
        btnDownloadPages: {
            enabled: menuOptions.btnDownloadPages,
            title: "Download pages",
            icon: "fa-download",
            url: forUrl + "/admin/contentcms/ecircular/downloadpdf/" + ecircularId,
        },
        btnPrint: {
            enabled: menuOptions.btnPrint,
            title: "Print",
            icon: "fa-print"
        },
        btnDownloadPdf: {
            enabled: menuOptions.btnDownloadPdf,
            title: "Download PDF",
            icon: "fa-file",
            url: forUrl + "/assets/uploads/ecircular/pdf/" + ecircularPdf,
        },
        btnSound: {
            enabled: menuOptions.btnSound,
            title: "Sound",
            icon: "fa-volume-up",
            iconAlt: "fa-volume-off",
        },
        btnExpand: {
            enabled: menuOptions.btnExpand,
            title: "Toggle fullscreen",
            icon: "fa-expand",
            iconAlt: "fa-compress",
        },
        facebook: {
            enabled: true,
            url: forUrl + '/ecircular/show/id/' + ecircularId,
        },
        google_plus: {
            enabled: true,
            url: forUrl + '/ecircular/show/id/' + ecircularId,
        },
        twitter: {
            enabled: true,
            url: forUrl + '/ecircular/show/id/' + ecircularId,
        },
        pinterest: {
            enabled: true,
            url: forUrl + '/ecircular/show/id/' + ecircularId,
        },
        email: {
            enabled: false,
        },
    });
    var screen_height = $(window).height();
    $("#container_flipbook").css({"height": screen_height});
    $(".flipbook-main-wrapper").css({"margin-top": "40px"});
}

//Quickview: Start
$("body").delegate("area", "click", function (e) {
    e.preventDefault();
    $(document).off('focusin.modal');
    var href = $(this).attr('href');
    var lastsegment = href.split('/');
    var id = lastsegment[lastsegment.length - 1];
    var ecircularId = $("#ecircularId").val();
    quickViewDisplay(id, ecircularId)
});
$("body").delegate("[id^='quickview_modal_']", "click", function (e) {
    if ($("#landingpagepopup").size() > 0) {
        return;
    }
    var productId = (this.id).replace("quickview_modal_", "");
    var ecircularId = $("#ecircularId").val();
    quickViewDisplay(productId, ecircularId);
});



function quickViewDisplay(productId, ecircularId) {
    var url = forUrl + '/product/quickview/id/' + productId;
    if (ecircularId != '' && ecircularId != undefined) {
        var id = 0;
    } else {
        var id = productId;
    }
    $(".close").on('click', function() {
        $("#quickview-form_" + id).modal('hide');
        $("#mobile_category_products_list #quickview-form_" + id).css("display","none");
    });
    $.ajax({
        url: url,
        data: {product_id: productId},
        beforeSend: function () {
            showLoader();
        },
        success: function (res) {
            $("#quickview-form_" + id).modal('hide');
            $("#quickview-form_" + id).modal('toggle');
            $("#mobile_category_products_list #quickview-form_" + id).css("display","block");
            $(".ps-current").remove();
            $(".quickview_" + id).html(res);
            hideLoader();
            zibbyaffirm();
            $("img.lazy").lazyload();
            $("#quickview_body .ps-current ul li").remove();
            $('.pgwSlideshow').pgwSlideshow({
                autoSlide: false,
                displayList: true,
                displayControls: true,
                touchControls: false
            });

            if ($("[id^='reviewrate']").size() > 0) {
                //ratingbar();
            }

        },
        complete: function () {
            hideLoader();
        },
    });
}
$("body").delegate(".close_quickview", "click", function (e) {
    $("[id^='quickview-form_']").modal('hide');
    $("[id^='quickview_body']").html('');
});
//Quickview: End


// Product Sharing: Start 
var shareCaptcha;
var shareCorrectCaptcha = function (response) {
    $("[id^='mailForm'] [id^='sharecaptcharesponse']").val(response);
    $("[id^='productshareform']").bootstrapValidator('revalidateField', "sharecaptcharesponse");
};
function productShareDisplay(productId, captcha_site_key) {
    $("[id^='mailForm" + productId + "']").find('form').trigger('reset');
    $("[id^='mailForm" + productId + "']").modal('show');
    if ($("[id^='productshareform']").size() > 0) {
        $("[id^='productshareform']").bootstrapValidator({excluded: ':disabled', feedbackIcons: {valid: 'fa', invalid: 'err', validating: 'fa'}, fields: {'yourname': {validators: {notEmpty: {message: 'Please Enter Your Name'}, }}, 'youremail': {validators: {notEmpty: {message: 'Please Enter Your Email'}, emailAddress: {message: 'Please enter valid Email-Id'}, }}, 'friendname': {validators: {notEmpty: {message: 'Please enter Friend\'s name'}, }}, 'friendemail': {validators: {notEmpty: {message: 'Please enter Friend\'s Email'}, emailAddress: {message: 'Please enter valid Email-Id'}, }}, 'subject': {validators: {notEmpty: {message: 'Please enter Subject'}, }}, 'message': {validators: {notEmpty: {message: 'Please enter Message'}, }}, }, submitHandler: function (validator, form, submitButton) {}})
        $("[id^='productshareform']").data('bootstrapValidator').resetForm();
    }
    if(captcha_site_key != ''){
        shareCaptcha = grecaptcha.render('sharecaptcha' + productId, {
            'sitekey': captcha_site_key,
        });
    }
}

$("body").delegate("[id^='share_']", "click", function (e) {
    var productId = (this.closest('form').id).replace("productshareform", "");
    var baseUrl = getBaseUrl();
    $("#productshareform" + productId).bootstrapValidator({excluded: ':disabled', feedbackIcons: {valid: 'fa', invalid: 'err', validating: 'fa'}, fields: {'yourname': {validators: {notEmpty: {message: 'Please Enter Your Name'}, }}, 'youremail': {validators: {notEmpty: {message: 'Please Enter Your Email'}, emailAddress: {message: 'Please enter valid Email-Id'}, }}, 'friendname': {validators: {notEmpty: {message: 'Please enter Friend\'s name'}, }}, 'friendemail': {validators: {notEmpty: {message: 'Please enter Friend\'s Email'}, emailAddress: {message: 'Please enter valid Email-Id'}, }}, 'subject': {validators: {notEmpty: {message: 'Please enter Subject'}, }}, 'message': {validators: {notEmpty: {message: 'Please enter Message'}, }}, }, submitHandler: function (validator, form, submitButton) {}})
});

$('body').on('success.form.bv', "[id^='productshareform']", function (event) {
    event.preventDefault();
    event.stopImmediatePropagation();
    var productId = (this.id).replace("productshareform", "");
    $("#share" + productId).attr('disabled', false);
    var baseUrl = getBaseUrl();
    var formData = $("form#productshareform" + productId).serialize();
    $.ajax({
        type: "POST",
        url: baseUrl + "/product/productshare",
        cache: false,
        data: formData,
        beforeSend: function () {
            $(".loader").removeClass("hide");
            $(".loader").addClass('show');
        },
        success: function (response) {
            if (response > 0) {
                if(shareCaptcha > 0){
                    grecaptcha.reset(shareCaptcha);
                }
                //$('body').Wload('hide', {time: 5});
                $("[id^='mailForm']").modal('hide');
                $("[id^='productshareform']").data('bootstrapValidator').resetForm();
                $("[id^='productshareform']")[0].reset();

                swal("Mail Sent!", "Product is refer to friend successfully", "success");
            } else {
                $("[id^='mailForm']").modal('hide');
                swal({title: 'Error!', text: 'Please Complete the Captcha by clicking the Checkbox', type: 'error'}, function () {
                    $("[id^='mailForm']").modal('show');
                });
                $("[id^='productshareform']").data('bootstrapValidator').resetForm();
            }
        },
        complete: function () {
            $(".loader").removeClass("show");
            $(".loader").addClass('hide');
        },
    });
});
$("body").delegate(".close_mailform", "click", function (e) {
    $(".show_ss_email_modal").modal('hide');
});
//Product Sharing: End

//Product Inquiry: Start
var inquiryCaptcha;
function productInquiryDisplay($productId, $captcha_site_key, type) {
    if ($("#landingpagepopup").size() > 0) {
        return;
    }
    var productId = $productId;
    var captcha_site_key = $captcha_site_key;
    var baseUrl = getBaseUrl();
    $("[id^='inqModal" + productId + "']").html('');
    $("[id^='inqDetailModal" + productId + "']").html('');
    $.ajax({
        type: "POST",
        url: baseUrl + '/../getProductInquiry',
        cache: false,
        data: 'productId=' + productId,
        success: function (response) {
            if (type == 'detail') {
                $("[id^='inqDetailModal" + productId + "']").modal('show');
                $("[id^='inqDetailModal" + productId + "']").html(response);
                $('#offerform' + productId + ' #btnofferform').attr('disabled', false);
            } else {
                $("[id^='inqModal" + productId + "']").modal('show');
                $("[id^='inqModal" + productId + "']").html(response);
                $('#offerform' + productId + ' #btnofferform').attr('disabled', false);
            }
            if(captcha_site_key != '')
            {
                inquiryCaptcha = grecaptcha.render('captcha' + productId, {
                    sitekey: captcha_site_key,
                });
                getCheckoutState($("#countryId" + productId).val(), '', '');
                if (screen.width < 768) {
                    var width = $("[id^='offerform" + productId + "']").width();
                    var scale = 0.80;//width / 302;
                    $('.g-recaptcha').css('transform', 'scale(' + scale + ')');
                    $('.g-recaptcha').css('-webkit-transform', 'scale(' + scale + ')');
                    $('.g-recaptcha').css('transform-origin', '0 0');
                    $('.g-recaptcha').css('-webkit-transform-origin', '0 0');
                }
            }
        }
    });
}
var correctCaptcha = function (response) {
    $("[id^='inqModal'] [id^='captcharesponse']").val(response);
    $("[id^='inqDetailModal'] [id^='captcharesponse']").val(response);
    $("[id^='offerform']").bootstrapValidator('revalidateField', "captcharesponse");
};

$(document.body).on('click', '#btnofferform', function () {
    var productId = $(this).attr('productid');
    $(".manufacturerofferform").bootstrapValidator({feedbackIcons: {valid: 'fa', invalid: 'err', validating: 'fa'}, fields: {'name': {excluded: false, validators: {notEmpty: {message: 'Please Enter Your Name'}, }}, 'phone': {validators: {notEmpty: {message: 'Please enter Phone Number'}, stringLength: {min: 10, message: 'Phone Number must be 10 number long'}, regexp: {regexp: /^([0|\(\+[0-9]{1,5}\))?([0-9][0-9]{9})$/, message: 'Please enter valid Phone Number'}, }}, 'dealer_id': {validators: {notEmpty: {message: 'Please Select Store'}, }}, 'country_id': {validators: {notEmpty: {message: 'Please Select Country'}, }}, 'state': {validators: {notEmpty: {message: 'Please Select State'}, }}, 'city': {validators: {notEmpty: {message: 'Please Select City'}, }}, 'zip': {validators: {notEmpty: {message: 'Please enter Zip'}, }}, 'question': {validators: {notEmpty: {message: 'Please enter Question'}, }}, 'email': {validators: {notEmpty: {message: 'Please enter Email Address'}, regexp: {regexp: /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/, message: 'Please enter valid Email-Id'}, }}, }, submitHandler: function (validator, form, submitButton) {}});
    $('body').on('success.form.bv', ".manufacturerofferform", function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var form = this;
        var formData = $("#offerform" + productId).serializeArray();
        var baseUrl = getBaseUrl();
        // $("[id^='captcharesponse" + productId + "']")
        //var googleResponse = jQuery('#captcharesponse').val();
        var googleResponse = jQuery("#captcharesponse").val();
        if (googleResponse == "") {
            $('.inquiry_modal ').css('z-index', 999);
            swal({title: "Error!", text: "Please Complete the Captcha by clicking the Checkbox", type: "error"}, function () {
                $('.inquiry_modal ').css('z-index', 10060);
                $('#offerform' + productId + ' #btnofferform').attr('disabled', false);
            });
        } else {
            $.ajax({
                url: baseUrl + '/../manufacturersendinquiry',
                type: 'post',
                data: formData,
                beforeSend: function () {
                    $("#inqModal" + productId).modal('hide');
                    $("#inqDetailModal" + productId).modal('hide');
                    $(".loader").removeClass("hide");
                    $(".loader").addClass('show');
                },
                success: function (response) {
                    if (response == 1) {
                        $("#inqModal" + productId).modal('hide');
                        $("#inqDetailModal" + productId).modal('hide');
                        $("#ecircular_quickview-form").modal('hide');
                        $("#inqEircularModal").modal('hide');
                        swal("Success!", "Inquiry Sent successfully", "success");
                        $('#offerform' + productId + ' #btnofferform').attr('disabled', false);
                        return true;
                    }
                },
                complete: function (xhr) {
                    $(".loader").removeClass("show");
                    $(".loader").addClass('hide');
                },
            })
        }
    });
    $("[id^='offerform" + productId + "']").bootstrapValidator({feedbackIcons: {valid: 'fa', invalid: 'err', validating: 'fa'}, fields: {'name': {excluded: false, validators: {notEmpty: {message: 'Please Enter Your Name'}, }}, 'phone': {validators: {notEmpty: {message: 'Please enter Phone Number'}, stringLength: {min: 10, message: 'Phone Number must be 10 number long'}, regexp: {regexp: /^([0|\(\+[0-9]{1,5}\))?([0-9][0-9]{9})$/, message: 'Please enter valid Phone Number'}, }}, 'dealer_id': {validators: {notEmpty: {message: 'Please Select Store'}, }}, 'countryId': {validators: {notEmpty: {message: 'Please Select Country'}, }}, 'state': {validators: {notEmpty: {message: 'Please Select State'}, }}, 'city': {validators: {notEmpty: {message: 'Please Select City'}, }}, 'zip': {validators: {notEmpty: {message: 'Please enter Zip'}, }}, 'question': {validators: {notEmpty: {message: 'Please enter Question'}, }}, 'email': {validators: {notEmpty: {message: 'Please enter Email Address'}, regexp: {regexp: /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/, message: 'Please enter valid Email-Id'}}}, }, submitHandler: function (validator, form, submitButton) {}});
    $('body').on('success.form.bv', "[id^='offerform" + productId + "']", function (e) {

        e.preventDefault();
        e.stopImmediatePropagation();
        var form = this;
        var formData = $("[id^='offerform" + productId + "']").serializeArray();
      // console.log(formData);
        var baseUrl = getBaseUrl();
        /* var googleResponse = jQuery('#g-recaptcha-response').val();
         alert(googleResponse);return false;
         //var googleResponse = jQuery('#recaptcha-token').val();
         if (!googleResponse) {
         swal("Error!", "Please Complete the Captcha by clicking the Checkbox", "error");
         return false;
         }*/

        $.ajax({
            url: baseUrl + '/../sendinquiry',
            type: 'post',
            data: formData,
            beforeSend: function () {
                $(".loader").removeClass("hide");
                $(".loader").addClass('show');
            },
            success: function (response) {
                if (response == 1) {
                    $("[id^='inqModal" + productId + "']").modal('hide');
                    $("[id^='inqDetailModal" + productId + "']").modal('hide');
                    $("#ecircular_quickview-form").modal('hide');
                    $("#inqEircularModal").modal('hide');
                    $('#offerform' + productId + ' #btnofferform').attr('disabled', false);
                    swal("Success!", "Inquiry Sent successfully", "success");
                    return true;
                } else if (response == 0) {
                    $('#offerform' + productId + ' #btnofferform').attr('disabled', false);
                    swal("Error!", "Please Complete the Captcha by clicking the Checkbox", "error");
                    return false;
                }
            },
            complete: function () {
                $(".loader").removeClass("show");
                $(".loader").addClass('hide');
            },
        })
    });
});
$("body").delegate(".close_inquiry", "click", function (e) {
    productId = (this.id).replace("close_", "");
    $("[id^='inqModal" + productId + "']").modal('hide');
    $("[id^='inqDetailModal" + productId + "']").modal('hide');
    $("#inqEircularModal").modal('hide');
});
//Product Inquiry: End

$("body").delegate(".form_builder_cls", "submit", function (e) {
    var baseUrl = getBaseUrl();
    var post_data = $(this).serializeArray();
    $.ajax({
        type: "POST",
        url: baseUrl + '/fbpostdata',
        data: post_data,
        success: function (result) {
            $(e.target).closest('.form-horizontal').prepend("<div class='alert alert-success flash_msg'>Form submitted successfully!</div>");
            setTimeout(function () {
                $('.flash_msg').remove();
                $('form.form_builder_cls')[0].reset();
            }, 2000);
            return true;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            return false;
        }
    });
    return false;
});

function zibbyaffirm() {
    if ($("#affirm_public_api_key").length > 0 && $("#hdn_sale_price").length > 0) {
        var hdn_sale_price = $("#hdn_sale_price").val();
        hdn_sale_price = hdn_sale_price.replace(".", "");
        var public_api_key = $("#affirm_public_api_key").val();
        var affirm_script = $("#affirm_script").val();
        var _affirm_config = {public_api_key: public_api_key, script: affirm_script};
        (function (l, g, m, e, a, f, b) {
            var d, c = l[m] || {},
                    h = document.createElement(f),
                    n = document.getElementsByTagName(f)[0],
                    k = function (a, b, c) {
                        return function () {
                            a[b]._.push([c, arguments])
                        }
                    };
            c[e] = k(c, e, "set");
            d = c[e];
            c[a] = {};
            c[a]._ = [];
            d._ = [];
            c[a][b] = k(c, a, b);
            a = 0;
            for (b = "set add save post open empty reset on off trigger ready setProduct".split(" "); a < b.length; a++)
                d[b[a]] = k(c, e, b[a]);
            a = 0;
            for (b = ["get", "token", "url", "items"]; a < b.length; a++)
                d[b[a]] = function () {};
            h.async = !0;
            h.src = g[f];
            n.parentNode.insertBefore(h, n);
            delete g[f];
            d(g);
            l[m] = c
        })(window, _affirm_config, "affirm", "checkout", "ui", "script", "ready");
        affirm.ui.ready(function () {
            GetAffirmAsLowAs(hdn_sale_price)
        });

        function GetAffirmAsLowAs(amount) {
            if ((amount == null)) {
                return;
            }
            var options = {apr: "0.10", months: 12, amount: amount};
            try {
                typeof affirm.ui.payments.get_estimate;
            } catch (e) {
                return;
            }

            function handleEstimateResponse(payment_estimate) {
                var hdn_currency = $("#hdn_currency").val();
                var dollars = ((payment_estimate.payment + 99) / 100) | 0;
                var a = document.getElementById("afirm-learn-more");
                var iText = ("innerText" in a) ? "innerText" : "textContent";
                a[iText] = "Learn More";
                var amountInfo = document.getElementById("amounut-info");
                amountInfo.innerHTML = "<div class=amount>" + hdn_currency + dollars + " / month with</div> ";
                amountInfo.style.visibility = "visible";
                var checkouttext = document.getElementById("checkout-text");
                checkouttext.innerHTML = "at checkout";
                checkouttext.style.visibility = "visible";
                a.onclick = payment_estimate.open_modal;
                a.style.visibility = "visible";
            }
            ;
            affirm.ui.payments.get_estimate(options, handleEstimateResponse);
        }
    }

    if ($("#zibby_pubilc_token").length > 0) {
        var zibby_pubilc_token = $("#zibby_pubilc_token").val();
        var zibby_mode_url = $("#zibby_mode_url").val();
        var _zibby_config = {api_key: zibby_pubilc_token, environment: zibby_mode_url};
        !function (e, t) {
            e.zibby = e.zibby || {};
            var n, i, r;
            i = !1, n = document.createElement("script"), n.type = "text/javascript", n.async = 0, n.src = t.environment + "/plugin/js/zibby.js", n.onload = n.onreadystatechange = function () {
                i || this.readyState && "complete" != this.readyState || (i = 0, e.zibby.setConfig(t.api_key))
            }, r = document.getElementsByTagName("script")[0], r.parentNode.insertBefore(n, r);
            var s = document.createElement("link");
            s.setAttribute("rel", "stylesheet"), s.setAttribute("type", "text/css"), s.setAttribute("href", t.environment + "/plugin/css/zibby.css");
            var a = document.querySelector("head");
            a.insertBefore(s, a.firstChild)
        }(window, _zibby_config);
    }
}

//HD JS START
if ($("#request_consultation_model").size() > 0) {
    $('#consultation_form').bootstrapValidator({
        feedbackIcons: {
            valid: 'fa',
            invalid: 'err',
            validating: 'fa'
        },

        fields: {
            'consult_firstname': {
                excluded: false,
                validators: {
                    notEmpty: {
                        message: 'Please enter First Name'
                    },
                }
            },
            'consult_lastname': {
                validators: {
                    notEmpty: {
                        message: 'Please enter Last Name'
                    },
                }
            },
            'consult_email': {
                validators: {
                    notEmpty: {
                        message: 'Please enter Email Address'
                    },
                    emailAddress: {
                        message: 'Please enter valid Email Address'
                    },
                }
            },
            'consult_phone': {
                validators: {
                    notEmpty: {
                        message: 'Please enter Phone'
                    },
                }
            },
            'consult_address': {
                validators: {
                    notEmpty: {
                        message: 'Please enter Address'
                    },
                }
            },
            'consult_country': {
                validators: {
                    notEmpty: {
                        message: 'Please enter Country'
                    },
                }
            },
            'consult_state': {
                validators: {
                    notEmpty: {
                        message: 'Please enter State'
                    },
                }
            },
            'consult_city': {
                validators: {
                    notEmpty: {
                        message: 'Please enter City'
                    },
                }
            },
            'consult_zip': {
                validators: {
                    notEmpty: {
                        message: 'Please enter Zipcode'
                    },
                }
            },
            'consult_message': {
                validators: {
                    notEmpty: {
                        message: 'Please enter Message'
                    },
                }
            },
        },
        submitHandler: function (validator, form, submitButton) {}
    });
    $('body').on('success.form.bv', '#consultation_form', function (e) {
        e.preventDefault();

        var form = this;
        var formData = $("#consultation_form").serializeArray();
        var baseUrl = getBaseUrl();
        if($('.g-recaptcha').attr('data-sitekey') != ''){
            var googleResponse = jQuery('#header_rc_captcha .g-recaptcha-response').val();
            if (googleResponse == '' ) {
                $('#consultation_form .captcha-error').show();
                return false;
            }
        }
        $.ajax({
            url: '/sendconsultation',
            type: 'post',
            data: formData,
            dataType: 'json',
            beforeSend: function () {
            },
            success: function (response) {
                if (response.status == 'success') {
                    $("#request_consultation_model").modal('hide');
                    swal("Success!", response.message, "success");
                    window.location.reload(true);
                } else if (response.status == 'warning') {
                    $("#request_consultation_model").modal('show');
                    swal("Warning!", response.message, "warning");
                    //window.location.reload(true);
                } else {
                    swal("Error!", response.message, "error");
                }
            },
        })
    });
}
if ($("#footer-request-a-consultation").size() > 0) {
    $('#footer-request-a-consultation').bootstrapValidator({
        feedbackIcons: {
            valid: 'fa',
            invalid: 'err',
            validating: 'fa'
        },

        fields: {
            'consult_firstname': {
                excluded: false,
                validators: {
                    notEmpty: {
                        message: 'Please enter First Name'
                    },
                }
            },
            'consult_lastname': {
                validators: {
                    notEmpty: {
                        message: 'Please enter Last Name'
                    },
                }
            },
            'consult_email': {
                validators: {
                    notEmpty: {
                        message: 'Please enter Email Address'
                    },
                    emailAddress: {
                        message: 'Please enter valid Email Address'
                    },
                }
            }
        },
        submitHandler: function (validator, form, submitButton) {}
    });
    $('body').on('success.form.bv', '#footer-request-a-consultation', function (e) {
        e.preventDefault();
        var form = this;
        var formData = $("#footer-request-a-consultation").serializeArray();
        var baseUrl = getBaseUrl();

        if($('.g-recaptcha').attr('data-sitekey') != ''){
            var googleResponse = jQuery('#header_fc_captcha .g-recaptcha-response').val();
            if (googleResponse == '') {
                $('#footer-request-a-consultation .captcha-error').show();
                return false;
            }
        }
        $.ajax({
            url: '/sendconsultation',
            type: 'post',
            data: formData,
            dataType: 'json',
            beforeSend: function () {
            },
            success: function (response) {
                if (response.status == 'success') {
                    $("#request_consultation_model").modal('hide');
                    swal("Success!", response.message, "success");
                    window.location.reload(true);
                } else if (response.status == 'warning') {
                    $("#request_consultation_model").modal('hide');
                    swal("Warning!", response.message, "warning");
                  //  window.location.reload(true);
                } else {
                    swal("Error!", response.message, "error");
                }
            },
        })
    });
}
if ($("#footer-request-a-consultation-custom").size() > 0) {
    $('#footer-request-a-consultation-custom').bootstrapValidator({
        feedbackIcons: {
            valid: 'fa',
            invalid: 'err',
            validating: 'fa'
        },
        fields: {
            'consult_fullname': {
                excluded: false,
                validators: {
                    notEmpty: {
                        message: 'Please enter Full Name'
                    },
                }
            },
            'consult_phone': {
                validators: {
                    notEmpty: {
                        message: 'Please enter Phone Number'
                    },
                }
            },
            'consult_email': {
                validators: {
                    notEmpty: {
                        message: 'Please enter Email Address'
                    },
                    emailAddress: {
                        message: 'Please enter valid Email Address'
                    },
                }
            }
        },
        submitHandler: function (validator, form, submitButton) {}
    });
    $('body').on('success.form.bv', '#footer-request-a-consultation-custom', function (e) {
        e.preventDefault();
        var form = this;
        var formData = $("#footer-request-a-consultation-custom").serializeArray();
        var baseUrl = getBaseUrl();

        $.ajax({
            url: '/sendconsultationcustom',
            type: 'post',
            data: formData,
            dataType: 'json',
            beforeSend: function () {
            },
            success: function (response) {
                if (response.status == 'success') {
                    $("#request_consultation_model").modal('hide');
                    swal("Success!", response.message, "success");
                    window.location.reload(true);
                } else if (response.status == 'warning') {
                    $("#request_consultation_model").modal('hide');
                    swal("Warning!", response.message, "warning");
                    //window.location.reload(true);
                } else {
                    swal("Error!", response.message, "error");
                }
            },
        })
    });
}
if ($('#consult_country').length > 0) {
    $('#consult_country').on('change', function () {
        var country = $('#consult_country').val();
        $.ajax({
            type: "POST",
            cache: true,
            url: '/getstate',
            data: 'ctrid=' + country,
            success: function (result) {
                $('#consult_state').html(result);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                return false;
            }
        });
    });
}
if ($('#consult_state').length > 0) {
    $('#consult_state').on('change', function () {
        var state = $('#consult_state').val();
        $.ajax({
            type: "POST",
            cache: true,
            url: '/getcity',
            data: 'stateid=' + state,
            success: function (result) {
                $('#consult_city').html(result);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                return false;
            }
        });
    });
}


if ($(".portfolioFilter").size() > 0) {
    $(window).load(function () {
        var $container = $('.portfolioContainer');
        $container.isotope({
            filter: '*',
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: true
            }
        });
        $('.portfolioFilter li a').click(function () {
            var selector = $(this).attr('data-filter');
            $container.isotope({
                filter: selector,
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: true
                }
            });
            return true;
        });
    });
}
if ($(".fancybox").size() > 0)
{
    $(".fancybox").fancybox({
        openEffect: 'elastic',
        closeEffect: 'elastic',
        helpers: {
            title: {
                type: 'inside'
            }
        }
    });
}
if ($("#gmapbg").length > 0) {
    var PageContact = function () {
        var _init = function () {
            //var store_title = $(".c-body > .c-section:first-child").html();
            //var address_data = $('.c-font').next().html();
            //address_data = store_title + address_data;
            var address_data = $("#hdn_store_address").val();
            var mapbg = new GMaps({
                div: '#gmapbg',
                lat: 0,
                lng: 0,
                zoom: 10,
                scrollwheel: true,
            });
            GMaps.geocode({
                address: address_data,
                callback: function(results, status) {
                    if (status == 'OK') {
                        var latlng = results[0].geometry.location;
                        mapbg.fitBounds(results[0].geometry.viewport);
                        mapbg.addMarker({
                            lat: latlng.lat(),
                            lng: latlng.lng(),
                            //title: 'Your Location',
                            infoWindow: {
                                content: address_data
                            }
                        });
                    }
                }
            });
        }
        return {
            //main function to initiate the module
            init: function () {
                _init();
            }
        };
    }();
    PageContact.init();
    $(window).resize(function () {
        PageContact.init();
    });
}

function recaptcha_callback_consultation_frm() {
    if($('.g-recaptcha').attr('data-sitekey') != ''){
        var googleResponse = jQuery('#header_rc_captcha .g-recaptcha-response').attr('id');
        var googleResponse1 = jQuery('#header_rc_captcha .g-recaptcha-response').attr('id');
    }

    if (googleResponse == 'g-recaptcha-response-1') {
        $('#consultation_form .captcha-error').show();
        return false;
    }
    else if (googleResponse1 == 'g-recaptcha-response-2') {
        $('#consultation_form .captcha-error').show();
        return false;
    }  else {
        $("#btnconsultation_form").attr('disabled', false);
        $('#consultation_form .captcha-error').hide();
        return true;
    }
}
function recaptcha_callback() {
    //return true;
    if($('.g-recaptcha').attr('data-sitekey') != ''){
        var googleResponse = jQuery('#header_fc_captcha .g-recaptcha-response').val();
    }
    if (googleResponse == '') {
        $('#footer-request-a-consultation .captcha-error').show();
        return false;
    } else {
        $("#footer-request-a-consultation button").attr('disabled', false);
        $('#footer-request-a-consultation .captcha-error').hide();
        return true;
    }
}

if ($("#contact_store_id").length > 0) {
    $("#contact_store_id").select2({minimumInputLength: 3, placeholder: "Select a Store Location", allowClear: true, width: '100%', });
}
if ($("#form_source").length > 0) {
    $("#form_source").select2({placeholder: "Please select the most appropriate topic concerning your questions", allowClear: true, width: '100%', });
}

if ($('#searchfrm').length > 0) {
    $("#searchfrm").submit(function (event) {
        if ($('#q1').length > 0 || $('#q').length > 0) {
            var search_val = $('#q1').val();
            var search_val1 = $('#q').val();
            if (search_val == '' && search_val1 == '') {
                return false;
            }
        }
    });
}
if ($('#searchfrmmobile').length > 0) {
    $("#searchfrmmobile").submit(function (event) {
        if ($('#q1').length > 0 || $('#q').length > 0) {
            var search_val = $('#q1').val();
            var search_val1 = $('#q').val();
            if (search_val == '' && search_val1 == '') {
                return false;
            }
        }
    });
}
//$('#storefrmbuilder').bootstrapValidator();
$('#storefrmbuilder').bootstrapValidator().on('success.form.bv', function(e) {
    e.preventDefault();
    // ajax call
    callajax();
});
function callajax()
{
    theurl = $('#storefrmbuilder').attr('action');
    var formfields = $('#storefrmbuilder').serialize();
    $.ajax({
        type: "POST",
        cache: true,
        url: theurl,
        data: formfields,
        success: function (result) {
            $('#captcha-err').hide();
            if ( result == '0' )
            {
                $('#captcha-err').show();
                $('#storefrmbuilder').bootstrapValidator('destroy');
                $('#storefrmbuilder').bootstrapValidator().on('success.form.bv', function(e) {
                    e.preventDefault();
                    // ajax call
                    callajax();
                });
            }
            else
            {
                location.href = result;
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            return false;
        }
    });
}
if ($(".select2").length > 0) {
    $(".select2").select2({allowClear: true, width: '100%', });
}
