if (strstr(site_url, "checkout", false) == 'checkout') {
    $(".c-cart-menu-close").html("");
    location_flag = $("#hdn_location_flag").val();
}
var lastUrl = lastUrlSegment(site_url);
/*$("body").delegate(".addtowishlist", "click", function(e) {
 var custid = $("#hdn_custid").val();
 if (custid == "") {
 $('#login-form').modal('show');
 return false;
 }
 var prdid = "";
 prdid = $(this).attr("data-name");
 var product_original_name = '';
 product_original_name = $(this).attr('data-original-name');
 var commaseparated_attrid = $("#commaseparated_attribute_id").val();
 if (commaseparated_attrid == undefined) {
 commaseparated_attrid = '';
 }
 var site_url1 = getBaseUrl();
 if (prdid != "") {
 $.ajax({
 url: site_url1 + '/product/addtowishlist',
 type: 'post',
 data: 'prdname=' + prdid + '&custid=' + custid + '&commaseparated_attrid=' + commaseparated_attrid,
 success: function(result) {
 if (result > 0) {
 if (!$(".wishlist_counter").hasClass('c-cart-number')) {
 $(".wishlist_counter").addClass('c-cart-number');
 }
 $(".wishlist_counter").html(result);
 if (wbGoogleAnalyticId != '' && wbStandardTracking == 1 && wbEcommerceTracking == 1) {
 gtag('event', 'add_to_wishlist', { "event_label": product_original_name });
 }
 swal("Wishlist!", "Product added to Wishlist successfully", "success");
 } else {
 swal("Wishlist!", "Product is alreday in Wishlist", "info");
 }
 },
 error: function(jqXHR, textStatus, errorThrown) {
 return false;
 }
 });
 }
 });*/

$("body").delegate(".addtocart", "click", function (e) {
    if ($("#landingpagepopup").size() > 0) {
        return;
    }
    e.preventDefault();
    e.stopImmediatePropagation();
    var custid = "";
    custid = $("#hdn_custid").val();
    var prdid = "";
    var prdqty = 1;
    var show_cart_popup = 1;
    var commaseparated_attrid = "";
    prdid = $(this).attr("data-name");
    prdqty = $("#prdqty").val();
    commaseparated_attrid = $("#commaseparated_attribute_id").val();
    if (commaseparated_attrid == undefined) {
        commaseparated_attrid = '';
    }
    var attr = $(this).attr('data-attribute');
    if (typeof attr !== typeof undefined && attr !== false) {
        commaseparated_attrid = attr;
    }
    if (prdqty == undefined) {
        prdqty = 1;
    }
    if ($(this).hasClass('show-minicart')) {
        show_cart_popup = 0;
    }
    var site_url1 = getBaseUrl();
    if (typeof productType != 'undefined' && productType == 2 && commaseparated_attrid == '') {
        swal("Warning!", "Please choose product options", "warning");
        return false;
    }
    if (prdid != "" && prdqty > 0) {
        $.ajax({
            url: site_url1 + '/shoppingcart/addtocart',
            type: 'post',
            data: 'prdname=' + prdid + '&custid=' + custid + '&prdqty=' + prdqty + '&pn=hmc' + '&commaseparated_attrid=' + commaseparated_attrid,
            cache: false,
            dataType: 'json',
            success: function (result) {
                if (result.status == 1) {
                    swal("Shopping Cart!", "Product added to Shopping Cart successfully", "success");
                    if (show_cart_popup == 0) {
                        setTimeout(function () {
                            $('li.CartButton').addClass("open");
                        }, 500);
                    }
                    if (result.abandonModal != 0) {
                        showAbandonedCartModal();
                    }
                    loadMiniCart();
                } else {
                    swal("Shopping Cart!", "There is some issue in Shopping Cart", "info");
                }
                if (result.gtaData != '' && wbGoogleAnalyticId != '' && wbStandardTracking == 1 && wbEcommerceTracking == 1) {
                    var gtaData = JSON.parse(result.gtaData);
                    gtag('event', 'add_to_cart', {"event_label": gtaData.name, "items": result.gtaData});
                }
                /*var res = "";
                 if (result) {
                 res = result.split("||");
                 }
                 if (res[1] != "") {
                 $(".modshopcartbox").html(res[1]);
                 }
                 if (res[0] > 0) {
                 swal("Shopping Cart!", "Product added to Shopping Cart successfully", "success");
                 if (show_cart_popup == 0) {
                 setTimeout(function() {
                 $('body').addClass("c-header-cart-shown");
                 }, 2000);
                 }
                 $(".totalcart").removeClass("hide");
                 $(".totalcart").html(res[0]);
                 } else {
                 swal("Shopping Cart!", "There is some issue in Shopping Cart", "info");
                 }
                 if (res[2] != '' && wbGoogleAnalyticId != '' && wbStandardTracking == 1 && wbEcommerceTracking == 1) {
                 var gtaData = JSON.parse(res[2]);
                 gtag('event', 'add_to_cart', { "event_label": gtaData.name, "items": res[2] });
                 }
                 if (res[3] != '') {
                 var AbandonedCart = JSON.parse(res[3]);
                 if (AbandonedCart.popupshow == 1) {
                 showAbandonedCartModal();
                 }
                 }
                 return false;*/
            },
            error: function (jqXHR, textStatus, errorThrown) {
                return false;
            }
        });
    }
});
$("body").delegate(".delcartitem", "click", function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var prdids = $(this).attr("data-name");
    var prdqty = $(this).attr("data-qty");
    var prdprc = $(this).attr("data-price");
    var PAIds = $(this).attr("data-paids");
    var prodattribute = $(this).attr('data-prodattribute');
    var carttotalprice = $(".carttotalprice").attr("data-totprice");
    var totalitem = $(".totalitem").html();
    var totalcart = $(".totalcart").html();
    if (parseInt(totalitem) > 0) {
        totalitem = parseInt(totalitem) - 1;
        $(".totalitem").html(parseInt(totalitem));
    }
    if (parseInt(totalcart) > 0) {
        totalcart = parseInt(totalcart) - 1;
        $(".totalcart").html(parseInt(totalcart));
        if (parseInt(totalcart) == 0) {
            $(".c-cart-menu").html('<div><ul class="c-cart-menu-items"><li>There is no any item in your shopping cart.</li></ul></div>');
            $(".shopcartshiping").html('<div class="row c-cart-table-row">There is no any item in your shopping cart.</div>');
            $(".btnqtyupdate").hide();
            $(".btnshopcheckout").hide();
            $(".totalcart").addClass("hide");
        }
    }
    var totalprice = 0;
    var total = 0;
    total = prdqty * prdprc;
    totalprice = parseFloat(carttotalprice) - parseFloat(total);
    totalprice = parseFloat(totalprice).toFixed(2);
    $(".carttotalprice").attr("data-totprice", parseFloat(totalprice));
    var site_url1 = getBaseUrl();
    $(".prodrow_" + prodattribute).remove();
    if (prdids != "") {
        $.ajax({
            url: site_url1 + '/shoppingcart/removefromcart',
            type: 'post',
            cache: false,
            data: 'prdname=' + prdids + '&prdprc=' + parseFloat(total) + '&totalprc=' + parseFloat(carttotalprice) + '&PAIds=' + PAIds,
            dataType: 'json',
            success: function (result) {
                if (result.status == 1) {
                    loadMiniCart();
                    $(".carttotalprice").html(result.TotalPrice);
                    prtotal = $(".carttotalprice").html();
                    // $("#calculate_shipping_charge").trigger("click");
                    // updateshippingCharge();
                    var charge_symbol = 0;
                    var shipping_charge = 0;
                    if ($('.show_shipping_charge').length) {
                        shipping_charge = $("input:radio.show_shipping_charge:checked").attr('data-charge');
                        charge_symbol = $("input:radio.show_shipping_charge:checked").attr('data-charge-symbol');
                    }
                    var grandtotal = $(".shpsubtotal").attr('data-subtotal');
                    grandtotal = parseFloat(shipping_charge) + parseFloat(grandtotal);
                    grandtotal = grandtotal.toFixed(2);
                    $(".shpsubtotal").html(prtotal);
                    $("#shipping_charge").html(charge_symbol);
                    $(".shpgrdtotal").html(charge_symbol + grandtotal);
                    //$("#calculate_shipping_charge").trigger("click");
                    //updateshippingCharge();
                    swal("Shopping Cart!", "Product deleted from Shopping Cart successfully", "success");
                    if (lastUrl == 'checkout') {
                        window.location.href = site_url1 + '/' + lastUrl;
                    }
                    loadShopppingCart();
                } else {
                    swal("Shopping Cart!", "There is some issue in Shopping Cart", "info");
                    if (lastUrl == 'checkout') {
                        window.location.href = site_url1 + '/' + lastUrl;
                    }
                    loadShopppingCart();
                }
                if (result.gtaData != '' && wbGoogleAnalyticId != '' && wbStandardTracking == 1 && wbEcommerceTracking == 1) {
                    var gtaData = JSON.parse(result.gtaData);
                    gtag('event', 'remove_from_cart', {"event_label": gtaData.name, "items": result.gtaData});
                }
                /*if (result != "") {
                 var rs = result.split("||");
                 if (rs[2] != "" && wbGoogleAnalyticId != '' && wbStandardTracking == 1 && wbEcommerceTracking == 1) {
                 var gtaData = JSON.parse(rs[2]);
                 gtag('event', 'remove_from_cart', { "event_label": gtaData.name, "items": rs[2] });
                 }
                 $(".carttotalprice").html(rs[1]);
                 prtotal = $(".carttotalprice").html();
                 $("#calculate_shipping_charge").trigger("click");
                 updateshippingCharge();
                 var charge_symbol = 0;
                 var shipping_charge = 0;
                 if ($('.show_shipping_charge').length) {
                 shipping_charge = $("input:radio.show_shipping_charge:checked").attr('data-charge');
                 charge_symbol = $("input:radio.show_shipping_charge:checked").attr('data-charge-symbol');
                 }
                 var grandtotal = $(".shpsubtotal").attr('data-subtotal');
                 grandtotal = parseFloat(shipping_charge) + parseFloat(grandtotal);
                 grandtotal = grandtotal.toFixed(2);
                 $(".shpsubtotal").html(prtotal);
                 $("#shipping_charge").html(charge_symbol);
                 $(".shpgrdtotal").html(symbol + grandtotal);
                 $("#calculate_shipping_charge").trigger("click");
                 updateshippingCharge();
                 swal("Shopping Cart!", "Product deleted from Shopping Cart successfully", "success");
                 //window.location.href = site_url;
                 loadShopppingCart();
                 } else {
                 swal("Shopping Cart!", "There is some issue in Shopping Cart", "info");
                 //window.location.href = site_url;
                 loadShopppingCart();
                 }
                 return false;*/
            },
            error: function (jqXHR, textStatus, errorThrown) {
                return false;
            }
        });
    }
    return false;
})
$("body").delegate(".prdqtyup,.prdqtydown", "click", function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var custid = "";
    var prdid = "";
    var prdqty = 1;
    var prtotal = 0;
    var prdprice = 0;
    var newprice = 0;
    custid = $("#hdn_custid").val();
    index = $(this).attr("data-index");
    prdqty = $(this).parent().prev(".prdqty").val();
    prdid = $(this).attr("data-name");
    prdprice = $(this).attr("data-price");
    var PAIds = $(this).attr("data-paids");
    var prodattribute = $(this).attr('data-prodattribute');
    var commaseparated_attrid = $(this).attr('data-attribute');
    $(".delcartitem[data-prodattribute='" + prodattribute + "']").attr('data-qty', prdqty);
    if (commaseparated_attrid == undefined) {
        commaseparated_attrid = '';
    }
    if (prdqty == undefined) {
        prdqty = 1;
        $(".c-item-" + index).val(prdqty);
        return false;
    }
    var site_url1 = getBaseUrl();
    if (prdid != "" && prdqty > 0) {
        $.ajax({
            url: site_url1 + '/shoppingcart/addtocart',
            type: 'post',
            cache: false,
            data: 'prdname=' + prdid + '&custid=' + custid + '&prdqty=' + prdqty + '&pn=sh' + '&PAIds=' + PAIds + '&commaseparated_attrid=' + commaseparated_attrid,
            dataType: 'json',
            success: function (result) {
                if (result.status == 1) {
                    loadMiniCart();
                    prtotal = $(".carttotalprice").html();
                    // $("#calculate_shipping_charge").trigger("click");
                    // updateshippingCharge();
                    var shipping_charge = 0;
                    var charge_symbol = 0;
                    if ($('.show_shipping_charge').length) {
                        shipping_charge = $("input:radio.show_shipping_charge:checked").attr('data-charge');
                        charge_symbol = $("input:radio.show_shipping_charge:checked").attr('data-charge-symbol');
                    }
                    var grandtotal = $(".shpsubtotal").attr('data-subtotal');
                    grandtotal = parseFloat(shipping_charge) + parseFloat(grandtotal);
                    grandtotal = grandtotal.toFixed(2);
                    $(".shpsubtotal").html(prtotal);
                    $("#shipping_charge").html(charge_symbol);
                    $(".shpgrdtotal").html(symbol + grandtotal);
                    newprice = parseInt(prdqty) * parseFloat(prdprice);
                    $(this).attr("data-price", newprice);
                    $.ajax({
                        url: site_url1 + '/product/reformatCurrency',
                        type: 'post',
                        data: 'prdprc=' + newprice,
                        success: function (result) {
                            if (result) {
                                $(".prod_price" + prodattribute).html(result);
                                $(".prod_price" + prodattribute).attr("data-price", result);
                            } else {
                                $(".prod_price" + prodattribute).html(newprice.toFixed(2));
                                $(".prod_price" + prodattribute).attr("data-price", newprice.toFixed(2));
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            return false;
                        }
                    });
                    //$("#calculate_shipping_charge").trigger("click");
                    //updateshippingCharge();
                    /*if (res[2] != '' && wbGoogleAnalyticId != '' && wbStandardTracking == 1 && wbEcommerceTracking == 1) {
                     var gtaData = JSON.parse(res[2]);
                     gtag('event', 'add_to_cart', { "event_label": gtaData.name, "items": res[2] });
                     }*/
                    swal({title: "Shopping Cart!", text: "Shopping Cart updated successfully", type: "success",
                    }, function () {
                        loadShopppingCart();
                    });
                    //swal("Shopping Cart!", "Shopping Cart updated successfully", "success");
                    //window.location.href = site_url;
                } else {
                    swal({title: "Shopping Cart!", text: "There is some issue in Shopping Cart", type: "info",
                    }, function () {
                        loadShopppingCart();
                    });
                }
                /*var res = "";
                 if (result) {
                 res = result.split("||");
                 }
                 if (res[1] != "") {
                 $(".modshopcartbox").html(res[1]);
                 }*/
                /*if (res[0] > 0) {
                 $(".totalcart").html(res[0]);
                 prtotal = $(".carttotalprice").html();
                 $("#calculate_shipping_charge").trigger("click");
                 updateshippingCharge();
                 var shipping_charge = 0;
                 var charge_symbol = 0;
                 if ($('.show_shipping_charge').length) {
                 shipping_charge = $("input:radio.show_shipping_charge:checked").attr('data-charge');
                 charge_symbol = $("input:radio.show_shipping_charge:checked").attr('data-charge-symbol');
                 }
                 var grandtotal = $(".shpsubtotal").attr('data-subtotal');
                 grandtotal = parseFloat(shipping_charge) + parseFloat(grandtotal);
                 grandtotal = grandtotal.toFixed(2);
                 $(".shpsubtotal").html(prtotal);
                 $("#shipping_charge").html(charge_symbol);
                 $(".shpgrdtotal").html(symbol + grandtotal);
                 newprice = parseInt(prdqty) * parseFloat(prdprice);
                 $(this).attr("data-price", newprice);
                 $.ajax({
                 url: site_url1 + '/product/reformatCurrency',
                 type: 'post',
                 data: 'prdprc=' + newprice,
                 success: function(result) {
                 if (result) {
                 $(".prod_price" + prodattribute).html(result);
                 $(".prod_price" + prodattribute).attr("data-price", result);
                 } else {
                 $(".prod_price" + prodattribute).html(newprice.toFixed(2));
                 $(".prod_price" + prodattribute).attr("data-price", newprice.toFixed(2));
                 }
                 },
                 error: function(jqXHR, textStatus, errorThrown) {
                 return false;
                 }
                 });
                 $("#calculate_shipping_charge").trigger("click");
                 updateshippingCharge();
                 if (res[2] != '' && wbGoogleAnalyticId != '' && wbStandardTracking == 1 && wbEcommerceTracking == 1) {
                 var gtaData = JSON.parse(res[2]);
                 gtag('event', 'add_to_cart', { "event_label": gtaData.name, "items": res[2] });
                 }
                 swal("Shopping Cart!", "Shopping Cart updated successfully", "success");
                 //window.location.href = site_url;
                 loadShopppingCart();
                 } else {
                 swal("Shopping Cart!", "There is some issue in Shopping Cart", "info");
                 //window.location.href = site_url;
                 loadShopppingCart();
                 }*/
            },
            error: function (jqXHR, textStatus, errorThrown) {
                return false;
            }
        });
    }
});
$("body").delegate(".prdqty", "keyup", function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var custid = "";
    var prdid = "";
    var prdqty = 1;
    var prtotal = 0;
    var prdprice = 0;
    var newprice = 0;
    custid = $("#hdn_custid").val();
    index = $(this).attr("data-index");
    prdqty = $(this).val();
    prdid = $(this).attr("data-name");
    prdprice = $(this).attr("data-price");
    var PAIds = $(this).attr("data-paids");
    var commaseparated_attrid = $(this).attr('data-attribute');
    var prodattribute = $(this).attr('data-prodattribute');
    if (commaseparated_attrid == undefined) {
        commaseparated_attrid = '';
    }
    $(".delcartitem[data-prodattribute='" + prodattribute + "']").attr('data-qty', prdqty);
    if (prdqty == undefined || prdqty == 0) {
        prdqty = 1;
        $(".c-item-" + index).val(prdqty);
        return false;
    }
    var site_url1 = getBaseUrl();
    if (prdid != "" && prdqty > 0) {
        $(this).val(prdqty);
        $.ajax({
            url: site_url1 + '/shoppingcart/addtocart',
            type: 'post',
            cache: false,
            data: 'prdname=' + prdid + '&custid=' + custid + '&prdqty=' + prdqty + '&pn=sh' + '&PAIds=' + PAIds + '&commaseparated_attrid=' + commaseparated_attrid,
            dataType: 'json',
            success: function (result) {
                if (result.status == 1) {
                    loadMiniCart();
                    prtotal = $(".carttotalprice").html();
                    var shipping_charge = 0;
                    var charge_symbol = 0;
                    if ($('.show_shipping_charge').length) {
                        shipping_charge = $("input:radio.show_shipping_charge:checked").attr('data-charge');
                        charge_symbol = $("input:radio.show_shipping_charge:checked").attr('data-charge-symbol');
                    }
                    var grandtotal = $(".shpsubtotal").attr('data-subtotal');
                    grandtotal = parseFloat(shipping_charge) + parseFloat(grandtotal);
                    grandtotal = grandtotal.toFixed(2);
                    $(".shpsubtotal").html(prtotal);
                    $("#shipping_charge").html(charge_symbol);
                    $(".shpgrdtotal").html(symbol + grandtotal);
                    newprice = parseInt(prdqty) * parseFloat(prdprice);
                    $(this).attr("data-price", newprice);
                    $.ajax({
                        url: site_url1 + '/product/reformatCurrency',
                        type: 'post',
                        data: 'prdprc=' + newprice,
                        success: function (result) {
                            if (result) {
                                $(".prod_price" + prodattribute).html(result);
                            } else {
                                $(".prod_price" + prodattribute).html(newprice.toFixed(2));
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            return false;
                        }
                    });
                    swal("Shopping Cart!", "Shopping Cart updated successfully", "success");
                    //window.location.href = site_url;
                    loadShopppingCart();
                } else {
                    swal("Shopping Cart!", "There is some issue in Shopping Cart", "info");
                    //window.location.href = site_url;
                    loadShopppingCart();
                }
                /*var res = "";
                 if (result) {
                 res = result.split("||");
                 }
                 if (res[1] != "") {
                 $(".modshopcartbox").html(res[1]);
                 }
                 if (res[0] > 0) {
                 $(".totalcart").html(res[0]);
                 prtotal = $(".carttotalprice").html();
                 var shipping_charge = 0;
                 var charge_symbol = 0;
                 if ($('.show_shipping_charge').length) {
                 shipping_charge = $("input:radio.show_shipping_charge:checked").attr('data-charge');
                 charge_symbol = $("input:radio.show_shipping_charge:checked").attr('data-charge-symbol');
                 }
                 var grandtotal = $(".shpsubtotal").attr('data-subtotal');
                 grandtotal = parseFloat(shipping_charge) + parseFloat(grandtotal);
                 grandtotal = grandtotal.toFixed(2);
                 $(".shpsubtotal").html(prtotal);
                 $("#shipping_charge").html(charge_symbol);
                 $(".shpgrdtotal").html(symbol + grandtotal);
                 newprice = parseInt(prdqty) * parseFloat(prdprice);
                 $(this).attr("data-price", newprice);
                 $.ajax({
                 url: site_url1 + '/product/reformatCurrency',
                 type: 'post',
                 data: 'prdprc=' + newprice,
                 success: function(result) {
                 if (result) {
                 $(".prod_price" + prodattribute).html(result);
                 } else {
                 $(".prod_price" + prodattribute).html(newprice.toFixed(2));
                 }
                 },
                 error: function(jqXHR, textStatus, errorThrown) {
                 return false;
                 }
                 });
                 swal("Shopping Cart!", "Shopping Cart updated successfully", "success");
                 //window.location.href = site_url;
                 loadShopppingCart();
                 } else {
                 swal("Shopping Cart!", "There is some issue in Shopping Cart", "info");
                 //window.location.href = site_url;
                 loadShopppingCart();
                 }*/
            },
            error: function (jqXHR, textStatus, errorThrown) {
                return false;
            }
        });
    }
});
$("body").delegate(".removewishlist", "click", function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var wishlist = $(this).attr("data-name");
    var site_url1 = getBaseUrl();
    if (wishlist != "") {
        swal({title: 'Are you sure you want to remove this product from wishlist?', type: 'warning', showCancelButton: true, confirmButtonText: 'Yes, Remove', cancelButtonText: 'No, Cancel'}, function () {
            $.ajax({
                url: site_url1 + '/product/removefromwishlist',
                type: 'post',
                data: 'wishlist=' + wishlist,
                success: function (result) {
                    if (result >= 0) {
                        window.location.reload(true);
                    } else {
                        swal("Wishlist!", "There is some issue in Wishlist", "info");
                    }
                    return false;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    return false;
                }
            });
        });
    }
    return false;
});
// function updatewarrantyCharge() {
$("body").delegate("input.warranty_price", "click", function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var baseUrl = getBaseUrl();
    var warranty_price = $(this).attr('data-warranty-price');
    if (warranty_price != '') {
        $.ajax({
            type: "POST",
            cache: true,
            url: baseUrl + '/shoppingcart/cartupdatewarrantydata/update',
            success: function (result) {
                loadShopppingCart();
                //location.reload();
                return true;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                return false;
            }
        });
    }
});
$("body").delegate("#delwarranty", "click", function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var baseUrl = getBaseUrl();
    swal({title: 'Are you sure you want to Remove this Warranty?', type: 'warning', showCancelButton: true, confirmButtonText: 'Yes, Delete', cancelButtonText: 'No, Cancel', closeOnConfirm: false, closeOnCancel: true}, function () {
        $.ajax({
            type: "POST",
            cache: false,
            url: baseUrl + '/shoppingcart/cartupdatewarrantydata/remove',
            success: function (response) {
                swal({title: "Success!", text: "Warranty Remove successfully", type: "success"}, function () {
                    //window.location.reload();
                    loadShopppingCart();
                });
            }
        });
        return false;
    });
});
//}
$('body').delegate(".applycoupon", 'click', function () {
    var copcode = $("#coupon_code").val();
    var baseUrl = getBaseUrl();
    if (copcode != "" && copcode != null) {
        $.ajax({
            type: "POST",
            cache: true,
            url: baseUrl + '/applycouponcode',
            data: 'copcode=' + copcode,
            success: function (result) {
                var jsonres = $.parseJSON(result);
                if (jsonres.responce_code == 1) {
                    swal("Success!", "Coupon applied successfully", "success");
                    $("#coupon_code").val("");
                    loadShopppingCart();
                    //window.location.reload(true);
                } else {
                    swal("Error!", jsonres.message, "error");
                    $("#coupon_code").val("");
                }
                return false;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                return false;
            }
        });
    }
});
$("body").delegate(".delcouponitem", "click", function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var couponid = $(this).attr("data-id");
    var baseUrl = getBaseUrl();
    if (couponid != "") {
        swal({title: 'Are you sure you want to delete this Coupon Code?', type: 'warning', showCancelButton: true, confirmButtonText: 'Yes, Delete', cancelButtonText: 'No, Cancel'}, function () {
            var url = "removecouponcode";
            $.ajax({
                type: "POST",
                url: baseUrl + "/" + url,
                cache: false,
                data: 'cpnid=' + couponid,
                success: function (response) {
                    if (response) {
                        swal("Success!", "Coupon Code deleted successfully", "success");
                        //window.location.reload(true);
                        loadShopppingCart();
                    }
                }
            });
            return false;
        });
    }
    return false;
});
if (typeof productType != 'undefined' && productType == 1) {
//$(".product_attribute_group_selectbox").on("change", function() {
    $(document).on('change', '.product_attribute_group_selectbox', function () {
        var sale_price = 0;
        var variasion = 0;
        sale_price = $("#hdn_sale_price").val();
        var commaseparated_attribute_id = '';
//    $.each($(".product_attribute_group_selectbox"), function () {
//        if ($(this).val() != '') {
//            variasion = parseFloat(variasion) + parseFloat($("option:selected", this).attr('attribute-price-variation'));
//            commaseparated_attribute_id = commaseparated_attribute_id + $(this).val() + ',';
//        }
//    });

        $.each($(".product_attribute_group_selectbox"), function () {
            if ($(this).val() != '') {
                var attribute_price_variation = $("option:selected", this).attr('attribute-price-variation');
                if (attribute_price_variation == '') {
                    attribute_price_variation = 0;
                }
                variasion = parseFloat(variasion) + parseFloat(attribute_price_variation);
                commaseparated_attribute_id = commaseparated_attribute_id + $(this).val() + ',';
            }
        });
        $.each($(".product_attribute_group_radio input[type='radio']:checked"), function () {
            var attribute_price_variation1 = $(this).attr('attribute-price-variation');
            if (attribute_price_variation1 == '') {
                attribute_price_variation1 = 0;
            }
            variasion = variasion + parseFloat(attribute_price_variation1);
            commaseparated_attribute_id = commaseparated_attribute_id + $(this).val() + ',';
        });

        var new_sale_price = parseFloat(sale_price) + parseFloat(variasion);
        var site_url = getBaseUrl();
        if (commaseparated_attribute_id != '') {
            commaseparated_attribute_id = commaseparated_attribute_id.slice(0, -1);
        }
        $("#commaseparated_attribute_id").val(commaseparated_attribute_id);
        var productId = $('#prdDataId').val();
        $.ajax({
            type: "POST",
            dataType: "json",
            url: site_url + "/product/getproductprice",
            cache: false,
            data: "sale_price=" + new_sale_price + '&commaseparated_attribute_id=' + commaseparated_attribute_id + '&product_id=' + productId,
            success: function (response) {
                if (response['sale_price'] != '') {
                    $("#sale_price_id").html(response['sale_price']);
                    $("#msrp_price_id").html(response['msrp_price']);
                }
            }
        });
    });
}
if (typeof productType != 'undefined' && productType == 2) {

//    $("body").delegate(".product_attribute_group_selectbox", "change", function (e) {
//        console.log(1);
//        getproductvariationdata();
//    });

//    $("body").delegate(".product_attribute_group_radio", "click", function (e) {
//        console.log(2);
//        getproductvariationdata();
//    });

//    $(".product_attribute_group_selectbox").on("change", function () {
//        getproductvariationdata()
//    });
//
//    $(".product_attribute_group_radio").on("click", function () {
//        getproductvariationdata()
//    });
}
function getproductvariationdata() {
    var gray_box_count = $('#product-variation .gray-box').length;

    var variasion = 0;
    var commaseparated_attribute_id = '';
    var attributeGroupVisible = {};
    var selectedAttributesId = [];
    $.each($(".product_attribute_group_selectbox"), function () {
        if ($(this).val() != '') {
            var attribute_price_variation = $("option:selected", this).attr('attribute-price-variation');
            if (attribute_price_variation == '') {
                attribute_price_variation = 0;
            }
            variasion = parseFloat(variasion) + parseFloat(attribute_price_variation);
            commaseparated_attribute_id = commaseparated_attribute_id + $(this).val() + ',';
            attributeGroupVisible[$("option:selected", this).attr('attribute-group')] = $("option:selected", this).attr('attribute-name');

            var attribute_id = $("option:selected", this).attr('attribute-id');
            selectedAttributesId.push(attribute_id);
        }
    });
    $.each($(".product_attribute_group_radio input[type='radio']:checked"), function () {
        var attribute_price_variation1 = $(this).attr('attribute-price-variation');
        if (attribute_price_variation1 == '') {
            attribute_price_variation1 = 0;
        }
        variasion = variasion + parseFloat(attribute_price_variation1);
        commaseparated_attribute_id = commaseparated_attribute_id + $(this).val() + ',';
        attributeGroupVisible[$(this).attr('attribute-group')] = $(this).attr('attribute-name');

        var attribute_id = $(this).attr('attribute-id');
        selectedAttributesId.push(attribute_id);
    });
    if (commaseparated_attribute_id != '') {
        commaseparated_attribute_id = commaseparated_attribute_id.slice(0, -1);
    }

    var selectedAttributesId = selectedAttributesId.join(',');

    $("#commaseparated_attribute_id").val(commaseparated_attribute_id);
    var productId = $('#prdDataId').val();
    var site_url = getBaseUrl();
    var selected_commaseparated_attribute;
    if (commaseparated_attribute_id != '') {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: site_url + "/product/getvariationattribute",
            cache: false,
            data: "commaseparated_attribute_id=" + commaseparated_attribute_id + '&product_id=' + productId + '&variasion=' + variasion + '&selectedAttributesId=' + selectedAttributesId,
            beforeSend: function () {
                $(".loader").removeClass("hide");
                $(".loader").addClass('show');
            },
            success: function (response) {
                $('#product-variation-attribute').html(response.html);
                $('#attribute-variation').html(response.attributesCombination);
//                $(".product_attribute_group_selectbox").on("change", function () {
//                    getproductvariationdata()
//                });
//
//                $(".product_attribute_group_radio").on("click", function () {
//                    getproductvariationdata()
//                });
                $(".loader").removeClass("show");
                $(".loader").addClass('hide');
            }
        });
    }
    var selected_commaseparated_attribute = commaseparated_attribute_id.split(',').length;
    if (selected_commaseparated_attribute == gray_box_count) {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: site_url + "/product/getproductvariationdata",
            cache: false,
            data: "commaseparated_attribute_id=" + commaseparated_attribute_id + '&product_id=' + productId + '&variasion=' + variasion,
            beforeSend: function () {
                $(".loader").removeClass("hide");
                $(".loader").addClass('show');
            },
            success: function (response) {
                if (response.sku != undefined) {
                    $("#sale_price_id").html(response['sale_price_with_currency']);
                    $("#msrp_price_id").html(response['msrp_price_with_currency']);
                    $('#product_variation_sku').html(response['sku']);
                    $('#product_variation_shortdescription').html('<p>' + response['description'] + '</p>');
                    $('#product_variation_bulletfeatures').html('<p>' + response['bullet_features'] + '</p>');
                    //$('#product_variation_advancefeatures').html(response['sku']);
                    $('#product_variation_advancedimension').html('<p>' + response['advance_dimension'] + '</p>');
                    $('#product_tag_list').show();
                    if (response['tags'] != '') {
                        $('#product_variation_tags').html(response['tags']);
                    }
                    $('#product_variation_dimension').html(response['rawitemDimensionHtml']);
                    $('#product_variation_gallery').html(response['rawitemGalleryHtml']);
                    $('#product_variation_visible_attribute').html(response['visibleAttributeHtml']);
                    if (response['InstallmentSetting'] != null) {
                        $('#product_variation_installment').html(response['InstallmentSetting']['installmentLable']);
                    }
                    $('#commaseparated_attribute_id').val(response['commaseparated_attribute_id']);
                    $("#product-variation .addtocart").attr("disabled", false);
                    $('#no_variation_message').html('');
                    $('.pgwSlideshow').pgwSlideshow({
                        autoSlide: false,
                        displayList: true,
                        displayControls: false,
                        touchControls: false
                    });
                    $('.ps-current ul li').zoom();
                    $.each(attributeGroupVisible, function (index, value) {
                        if ($('#attributelist-' + index).length > 0) {
                            $('#attributelist-' + index).html(value);
                        }
                    });

                } else {
                    $('#no_variation_message').html(response['message']);
                    $("#product-variation .addtocart").attr("disabled", true);
                }
                $(".loader").removeClass("show");
                $(".loader").addClass('hide');
            },
            complete: function (response) {
//                $(".attribute_variation").removeClass('hide');
//                $(".attribute_variation").addClass('show');
//                $('#attribute-variation').html(response.responseJSON.attributesCombination);
            }
        });
    } else {
        $("#product-variation .addtocart").attr("disabled", true);
    }
}

window.onload = function () {
    if ($("#commaseparated_attribute_id").val() != '' && $("#commaseparated_attribute_id").val() != undefined) {
        var sale_price = 0;
        var variasion = 0;
        sale_price = $("#hdn_sale_price").val();
        var commaseparated_attribute_id = '';
        $.each($(".product_attribute_group_selectbox"), function () {
            if ($(this).val() != '') {
                variasion = parseFloat(variasion) + parseFloat($("option:selected", this).attr('attribute-price-variation'));
                commaseparated_attribute_id = commaseparated_attribute_id + $(this).val() + ',';
            }
        });
        var new_sale_price = parseFloat(sale_price) + parseFloat(variasion);
        var site_url = getBaseUrl();
        if (commaseparated_attribute_id != '') {
            commaseparated_attribute_id = commaseparated_attribute_id.slice(0, -1);
        }
        $("#commaseparated_attribute_id").val(commaseparated_attribute_id);
        var productId = $('#prdDataId').val();
        $.ajax({
            type: "POST",
            dataType: "json",
            url: site_url + "/product/getproductprice",
            cache: false,
            data: "sale_price=" + new_sale_price + '&commaseparated_attribute_id=' + commaseparated_attribute_id + '&product_id=' + productId,
            success: function (response) {
                if (response['sale_price'] != '') {
                    $("#sale_price_id").html(response['sale_price']);
                    $("#msrp_price_id").html(response['msrp_price']);
                }
            }
        });
    }
};

// textsurvey form
$("#textSurvey").attr("checked", false);
$("body").delegate("#textsurveycancel", "click", function () {
    $('#textSurveyModal').modal('hide');
    $("#agreetextsurvey").prop("checked", false);
});
$("body").delegate("#textsurveyaccept", "click", function () {
    $('#textSurveyModal').modal('hide');
    $("#textSurvey").prop("checked", true);
});
$("#textSurvey").click(function (e) {
    if ($("#textSurvey").is(':checked')) {
        e.preventDefault();
        $('#textSurveyModal').modal('show');
        $('#textSurveyModal').modal({keyboard: false, show: true, backdrop: 'static'});
        $("#textSurveyModal").on("hide.bs.modal", function () {
        });
    } else {
        $("#textSurvey").prop("checked", false);
    }
});


// addtional invoice details
$("#checkbox_invoice").on('change', function ()
{
    $('#showInvoiceModal').modal('show', {backdrop: 'true', keyboard: false});
});

$("body").delegate("#submit_invoice", "click", function (e) {
// console.log("productId = "+productId);
    var baseUrl = getBaseUrl();
    $('#invoice_same_billing').attr('checked', false);
    $('#invoice_same_shipping').attr('checked', false);
    $("#invoiceform").bootstrapValidator({
        excluded: ':disabled',
        feedbackIcons: {
            valid: 'fa',
            invalid: 'err',
            validating: 'fa'
        },
        fields: {
            'invoice_first_name': {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Frist Name'
                    },
                }
            },
            'invoice_last_name': {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Last Name'
                    },
                }
            },
            'invoice_address1': {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Addres'
                    },
                }
            },
            'tax_id': {
                validators: {
                    notEmpty: {
                        message: 'Please Enter TaxId'
                    },
                }
            },
            'invoice_email': {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Your Email'
                    },
                    emailAddress: {
                        message: 'Please enter valid Email-Id'
                    },
                }
            },
            'invoice_country_id': {
                validators: {
                    notEmpty: {
                        message: 'Please select Country'
                    },
                }
            },
            'invoice_city': {
                validators: {
                    notEmpty: {
                        message: 'Please select Town / City'
                    },

                }
            },
            'invoice_state_id': {
                validators: {
                    notEmpty: {
                        message: 'Please select State'
                    },

                }
            },
            'invoice_zip': {
                validators: {
                    notEmpty: {
                        message: 'Please enter Postcode / Zip'
                    },
                    stringLength: {
                        max: 15,
                        message: 'Please enter valid Postcode / Zip'
                    },
                }
            },
        },
        submitHandler: function (validator, form, submitButton) {}
    })
});
$('body').on('success.form.bv', "#invoiceform", function (event) {
    event.preventDefault();
    event.stopImmediatePropagation();
    $("#submit_invoice").attr('disabled', false);
    var id = $('#invoice_oderid').val();
    alert(id);


    var baseUrl = getBaseUrl();
    var formData = $("#invoiceform").serialize();
    $.ajax({
        type: "POST",
        url: baseUrl + "/order/invoicedata",
        cache: false,
        data: formData,
        beforeSend: function () {
            $('body').Wload({text: 'Sending Mail...'});
        },
        success: function (response) {
            if (response == 'true') {
                $('body').Wload('hide', {time: 5});
                $("#showInvoiceModal").modal('hide');
                $("#invoiceform").data('bootstrapValidator').resetForm();
                $("#showInvoiceModal")[0].reset();
            } else {
                $("#showInvoiceModal").modal('show');
                $("#invoiceform").data('bootstrapValidator').resetForm();
            }
        }
    });
});
$("#invoice_same_shipping").on('change', function () {
    if ($(this).is(':checked')) {
        $('#invoice_same_billing').attr('checked', false);
        $("#invoice_first_name").val($("#shipping_firstName").val());
        $("#invoice_last_name").val($("#shipping_lastName").val());
        $("#invoice_company_name").val($("#shipping_companyName").val());
        $("#invoice_email").val($("#shipping_emailid").val());
        $("#invoice_address1").val($("#form_shipping_address1").val());
        $("#invoice_address2").val($("#form_shipping_address2").val());
        $("#invoice_city").val($("#form_shipping_city").val());
        $("#invoice_country_id").val($("#shipping_countryId").val());
        $("#invoice_zip").val($("#form_shipping_zipcode").val());
        $("#invoice_state_id").val($("#form_shipping_stateId").val());
        $("#invoice_phone").val($("#shipping_contact").val());
        $("#invoiceFrom").data('bootstrapValidator').revalidateField('form[invoice_first_name]');
        $("#invoiceFrom").data('bootstrapValidator').revalidateField('form[invoice_last_name]');
        $("#invoiceFrom").data('bootstrapValidator').revalidateField('form[invoice_email]');
        $("#invoiceFrom").data('bootstrapValidator').revalidateField('form[invoice_address1]');
        $("#invoiceFrom").data('bootstrapValidator').revalidateField('form[invoice_city]');
        $("#invoiceFrom").data('bootstrapValidator').revalidateField('form[invoice_zip]');
        $("#invoiceFrom").data('bootstrapValidator').revalidateField('form[invoice_state_id]');
        $("#invoiceFrom").data('bootstrapValidator').revalidateField('form[invoice_country_id]');
        $("#invoiceFrom").data('bootstrapValidator').revalidateField('form[invoice_phone]');
        $("#invoiceFrom").data('bootstrapValidator').revalidateField('form[invoice_tax_id]');
    } else {
        this.form.reset();
    }
});
$("#invoice_same_billing").on('change', function () {
    if ($(this).is(':checked')) {
        $('#invoice_same_shipping').attr('checked', false);
        $("#invoice_first_name").val($('#billing_firstName').val());
        $("#invoice_last_name").val($('#billing_lastName').val());
        $("#invoice_company_name").val($("#billing_companyName").val());
        $("#invoice_email").val($('#billing_emailid').val());
        $("#invoice_address1").val($("#form_billing_address1").val());
        $("#invoice_address2").val($("#form_billing_address2").val());
        $("#invoice_city").val($("#form_billing_city").val());
        $("#invoice_country_id").val($("#billing_countryId").val());
        $("#invoice_zip").val($("#form_billing_zipcode").val());
        $("#invoice_state_id").val($("#form_billing_stateId").val());
        $("#invoice_phone").val($("#billing_contact").val());
        $("#invoiceFrom").data('bootstrapValidator').revalidateField('form[invoice_first_name]');
        $("#invoiceFrom").data('bootstrapValidator').revalidateField('form[invoice_last_name]');
        $("#invoiceFrom").data('bootstrapValidator').revalidateField('form[invoice_email]');
        $("#invoiceFrom").data('bootstrapValidator').revalidateField('form[invoice_address1]');
        $("#invoiceFrom").data('bootstrapValidator').revalidateField('form[invoice_city]');
        $("#invoiceFrom").data('bootstrapValidator').revalidateField('form[invoice_zip]');
        $("#invoiceFrom").data('bootstrapValidator').revalidateField('form[invoice_state_id]');
        $("#invoiceFrom").data('bootstrapValidator').revalidateField('form[invoice_country_id]');
        $("#invoiceFrom").data('bootstrapValidator').revalidateField('form[invoice_phone]');
        $("#invoiceFrom").data('bootstrapValidator').revalidateField('form[invoice_tax_id]');
    } else {
        this.form.reset();
    }
});
$('body').delegate('#invoice_country_id', 'change', function () {
    var invoice_country = $('#invoice_country_id').val();
    getCheckoutState(invoice_country, selectedState = '', 'invoice');
    var invoice_state = $('#invoice_state_id').val();
});
$('body').delegate('#invoice_state_id', 'change', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var invoice_state = $('#invoice_state_id').val();
    getCheckoutCity(invoice_state, selectedCity = '', 'invoice');
    var invoice_country = $('#invoice_country_id').val();
    var invoice_city = $('#invoice_city').val();
});
$("body").delegate("#submit_invoice", "click", function (e) {
    if ($("#invoiceFrom").size() > 0) {
        $("#invoiceFrom").bootstrapValidator({excluded: ':disabled', feedbackIcons: {valid: 'fa', invalid: 'err', validating: 'fa'}, fields: {'form[invoice_first_name]': {validators: {notEmpty: {message: 'Please Enter Frist Name'}, }}, 'form[invoice_last_name]': {validators: {notEmpty: {message: 'Please Enter Last Name'}, }}, 'form[invoice_address1]': {validators: {notEmpty: {message: 'Please Enter Addres'}, }}, 'form[invoice_tax_id]': {validators: {notEmpty: {message: 'Please Enter Tax'}, integer: {message: 'Please enter valid Tax'}, }}, 'form[invoice_email]': {validators: {notEmpty: {message: 'Please Enter Your Email'}, emailAddress: {message: 'Please enter valid Email-Id'}, }}, 'form[invoice_country_id]': {validators: {notEmpty: {message: 'Please select Country'}, }}, 'form[invoice_city]': {validators: {notEmpty: {message: 'Please select Town / City'}, }}, 'form[invoice_state_id]': {validators: {notEmpty: {message: 'Please select State'}, }}, 'form[invoice_zip]': {validators: {notEmpty: {message: 'Please enter Postcode / Zip'}, stringLength: {max: 15, message: 'Please enter valid Postcode / Zip'}, }}, 'form[invoice_phone]': {validators: {notEmpty: {message: 'Please enter Phone Number'}, stringLength: {min: 10, message: 'Phone Number must be 10 number long'}, regexp: {regexp: /^[^-\s][0-9()+-\s]+(?:,\d+)*$/i, message: 'Please enter valid Phone Number'}, }}}, submitHandler: function (validator) {}});
    }
    if ($('#invoiceFrom').bootstrapValidator('validate').has('.has-error').length === 0) {
        var baseUrl = getBaseUrl();
        var formData = $("#invoiceFrom").serialize();
        $.ajax({
            type: "POST",
            url: baseUrl + "/order/invoicedata",
            cache: false,
            data: formData,
            success: function (response) {
                if (response) {
                    $('#hdn_invoice_form_data').val(response);
                }
                $("#showInvoiceModal").modal('hide');
                $("#invoiceform").data('bootstrapValidator').resetForm();
                $("#showInvoiceModal")[0].reset();
            }
        });
    } else {
        return false;
    }
});

function showAbandonedCartModal() {
    if ($('#abandonedCartDataModal').length > 0) {
        $('#abandonedCartDataModal').modal('show');
        $('#abandoned_cart_data_submit').click(function (event) {
            $("#abandonedcartfrm").data('bootstrapValidator').revalidateField('abandoned_email');
            event.preventDefault();
            event.stopImmediatePropagation();
            var baseUrl = getBaseUrl();
            var formData = $("#abandonedcartfrm").serialize();
            $.ajax({
                type: "POST",
                dataType: "json",
                url: baseUrl + "/abandonedcart/getemailabandonedcart",
                cache: false,
                data: formData,
                success: function (response) {
                    if (response > 0) {
                        $('#abandonedCartDataModal').modal('hide');
                        return true;
                    } else {
                        return false;
                    }
                }
            });
        });
    }
}
$('#abandonedCartDataModal').on('shown.bs.modal', function (e) {
    $('#abandonedcartfrm').bootstrapValidator({feedbackIcons: {valid: 'fa', invalid: 'err', validating: 'fa'}, fields: {'abandoned_email': {validators: {notEmpty: {message: 'Please enter Email Id'}, emailAddress: {message: 'Please enter valid Email Id'}, }}, }, submitHandler: function (validator, form, submitButton) {}});
});
//$('body').on('success.form.bv', '#abandonedcartfrm', function (event) {
//    event.preventDefault();
//    event.stopImmediatePropagation();
//    var baseUrl = getBaseUrl();
//    var formData = $("#abandonedcartfrm").serialize();
//    $.ajax({type: "POST", dataType: "json", url: baseUrl + "/admin/pos/getemailabandonedcart", cache: false, data: formData, success: function (response) {
//            if (response > 0) {
//                $('#abandonedCartDataModel').modal('hide');
//                location.reload(true);
//            }
//        }});
//});
