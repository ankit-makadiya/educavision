var catID = $('#pageCatId').val();
var PriceSymbol = $('#PriceSymbol-' + catID).val();
var categorySesion = $('#CategorySession' + catID).val();
var minimumProductPrice = $('#Minprice' + catID).val();
var maximumProductPrice = $('#Maxprice' + catID).val();
localStorage.removeItem("ispriceslider" + catID);
var showItem =  $('.show-item-per-page').val();
var categoryCommaValue =  $('#categoryCommaValue').val();

window.onpopstate = function (event) {
    if (event && event.state) {
        location.reload();
    }
}
$(document).ready(function () {
    var pageid = $(this).attr('pageid');
    var baseUrl = getBaseUrl();
    var currentUrl = $(location).attr('href');
    var currensstUrl = currentUrl.split("?").slice(1, 2);
    var passurl = currensstUrl[0];
    if (pageid != null) {
        var filterUrl = baseUrl + '/category/filter/' + $(this).attr('pageid');
    } else {
        if (currensstUrl != "") {
            var filterUrl = baseUrl + '/category/filter?';
        } else {
            var filterUrl = baseUrl + '/category/filter';
        }
    }
    var abc = [];
    var selectedBrand = [];
    var abc1 = [];
    var top_filter_attr = '';
    var top_filter_brand = '';
    if ($('#top_filter').serializeArray().length > 0) {

//        console.log('SERIALIZE DATA = ' + $('#top_filter').serializeArray());
//        console.log('SERIALIZE DATA = ' + JSON.stringify($('#top_filter').serializeArray()));

        //var json = JSON.stringify($('#top_filter').serializeArray());
        var json = $('#top_filter').serializeArray();
        Object.keys(json).forEach(function (key) {

            if (json[key].name == 'attribute[]') {
                abc1.push(json[key].value);
                abc.push('&attribute[]=' + json[key].value);
            }
            if (json[key].name == 'brand[]') {
                abc1.push(json[key].value);
                selectedBrand.push('&brand[]=' + json[key].value);
            }
        });

        var top_filter_attr = abc.toString().replace(/\,/g, '');
        var top_filter_brand = selectedBrand.toString().replace(/\,/g, '');

//        var data = $('#top_filter').serializeArray().reduce(function (obj, item) {
//            if (jQuery.inArray(item.value, abc1) == -1) {
//                if (item.name == 'attribute[]') {
//                    abc1.push(item.value);
//                    abc.push('&attribute[]=' + item.value);
//                }
//                if (item.name == 'brand[]') {
//                    abc1.push(item.value);
//                    selectedBrand.push('&brand[]=' + item.value);
//                }
//            }
//        });
//        var top_filter_attr = abc.toString().replace(/\,/g, '');
//        var top_filter_brand = selectedBrand.toString().replace(/\,/g, '');

//        console.log('top_filter_attr = ' + top_filter_attr);
//        console.log('top_filter_brand = ' + top_filter_brand);
    }

    var formData = $("#select_search_form,#search_form").serialize() + top_filter_attr + top_filter_brand + '&showItem=' + showItem + '&categoryCommaValue =' + categoryCommaValue;

    $.ajax({
        url: filterUrl + currensstUrl,
        type: 'post',
        dataType: "json",
        data: formData,
        beforeSend: function () {
            $(".loader").show();
        },
        success: function (response) {
            if (response['productList'] != '') {
                $("#location_div").show();
                $('.product_listing_title').attr('style', 'display: block !important');
                if (categorySesion != '') {
                    $("#category_products_list").html(response['productList']);
                }
                $(".pagination_div_id").html(response['pagination']);
                $(".filter_left_view_id").html(response['filterAttributeView']);
                $(".filter_right_view_id").html(response['filterAttributeRightView']);
                $(".filter_both_view_id").html(response['filterbothleftView']);
                $("#filter").html(response['filtermobileView']);
                $("#mobileproductcount").html(response['product_count']);
                $("#myDIV").html(response['filterAttributetopView']);

                // new changes start
                var minimumProductPrice = $('#Minprice' + catID).val();
                var maximumProductPrice = $('#Maxprice' + catID).val();
                if (currensstUrl == "") {
                    localStorage.setItem("catMaxVal" + catID, maximumProductPrice);
                    localStorage.setItem("catMinVal" + catID, minimumProductPrice);
                }
                if (currensstUrl != '') {
                    var arr1 = currensstUrl[0].split("&");
                    var checkedvalue = {};
                    for (var propertyName in arr1) { // loop through properties
                        var aa = arr1[propertyName].split("=").slice(0, 1);
                        var bb = arr1[propertyName].split("=").slice(1, 2);
                        var forPrice = arr1[propertyName].split("=").slice(1, 2);
                        if (aa == 'price') {
                            localStorage.setItem("ispriceslider" + catID, "y");
                            var priceRangeValue = forPrice[0].split("|");
                        } else if (aa == 'sort') {
                            var FIlterSelectBox = $("[urlvalue=" + bb + "]").attr("id");
                            if (FIlterSelectBox != null && FIlterSelectBox != "undefined") {
                                document.getElementById(FIlterSelectBox).selected = "true";
                            }
                        } else {
                            var adata = aa[0];
                            var bdata = bb[0].split("|");
                            $.each(bdata, function (key, value) {
//                                var keyId = $("[data-name=" + value + "]").attr("id");
//                                var value = true;
//                                $("." + keyId).prop('checked', value);
//                                checkedvalue[keyId] = value;

                                if ($('#filter_from').val() == 'web') {
                                    var keyId = $("#myDIV [data-name=" + value + "]").attr("id");
                                    var value = true;
                                    $("#myDIV ." + keyId).prop('checked', value);
                                    checkedvalue[keyId] = value;
                                } else {
                                    var keyId = $("#filter [data-name=" + value + "]").attr("id");
                                    var value = true;
                                    $("#filter ." + keyId).prop('checked', value);
                                    checkedvalue[keyId] = value;
                                }


                            });
                        }
                    }
                    if (Object.keys(checkedvalue).length == 0) {
                        if (priceRangeValue != null) {
                            var priceData = '<span class="filters_cube sort_rang" id="pricesession-' + catID + '">' + PriceSymbol + priceRangeValue[0] + ' TO ' + PriceSymbol + priceRangeValue[1] + '<i class="fa fa-times" aria-hidden="true"></i></span>';
                        } else {
                            var priceData = '';
                        }
                        var clearData = '<span class="filters_cube sort_rang clearall" data-id="clearall-' + catID + '">clear all</span>';
                        $('#session' + catID).html(priceData + clearData);
                        if (!$('.category' + catID).is(":checked") && priceData == '') {
                            $('#session' + catID).html(" ");
                        }
                    }
                    var minimumProductPrice = $('#Minprice' + catID).val();
                    var maximumProductPrice = $('#Maxprice' + catID).val();
                    if (currensstUrl == "") {
                        localStorage.setItem("catMaxVal" + catID, maximumProductPrice);
                        localStorage.setItem("catMinVal" + catID, minimumProductPrice);
                    }
                    var tempData = [];
                    var priceData = "";
                    // On page load
                    $.each(checkedvalue, function (key, value) {
//                        $("#" + key).prop('checked', value);
                        if ($('#filter_from').val() == 'web') {
                            $("#myDIV #" + key).prop('checked', value);
                        } else {
                            $("#filter #" + key).prop('checked', value);
                        }

                        var chekCategory = key.split('-').slice(1, 2);
                        if (chekCategory == catID) {
//                            if ($("#" + key).prop('checked') == true) {
//                                var element_data_name = $("#" + key).attr('data-showname');
//                                tempData.push('<span class="filters_cube" data-id="' + key + '">' + element_data_name + ' <i class="fa fa-times" aria-hidden="true"></i></span>');
//                            } else {
//                                tempData.push('');
//                            }

                            if ($('#filter_from').val() == 'web') {
                                if ($("#myDIV #" + key).prop('checked') == true) {
                                    var element_data_name = $("#myDIV #" + key).attr('data-showname');
                                    tempData.push('<span class="filters_cube" data-id="' + key + '">' + element_data_name + ' <i class="fa fa-times" aria-hidden="true"></i></span>');
                                } else {
                                    tempData.push('');
                                }
                            } else {
                                if ($("#filter #" + key).prop('checked') == true) {
                                    var element_data_name = $("#filter #" + key).attr('data-showname');
                                    tempData.push('<span class="filters_cube" data-id="' + key + '">' + element_data_name + ' <i class="fa fa-times" aria-hidden="true"></i></span>');
                                } else {
                                    tempData.push('');
                                }
                            }

                            var myStr = tempData.join(" ");

                            if (priceRangeValue != null) {
                                var priceData = '<span class="filters_cube sort_rang" id="pricesession-' + catID + '">' + PriceSymbol + priceRangeValue[0] + ' TO ' + PriceSymbol + priceRangeValue[1] + '<i class="fa fa-times" aria-hidden="true"></i></span>';
                            } else {
                                var priceData = '';
                            }
                            var clearData = '<span class="filters_cube sort_rang clearall" data-id="clearall-' + catID + '">clear all</span>';
                            $('#session' + catID).html(myStr + priceData + clearData);
                            if (!$('.category' + catID).is(":checked") && priceData == '') {
                                $('#session' + catID).html(" ");
                            }
                        }
                    });
                } else {
                    $('.category' + catID).attr('checked', false);
                }
                // new chnages end
                var minimumProductPrice = $('#Minprice' + catID).val();
                var maximumProductPrice = $('#Maxprice' + catID).val();
                if (priceRangeValue != null) {
                    var ispriceSet = localStorage.getItem('ispriceslider' + catID);
                    if (ispriceSet == "y") {
                        $(".pricerange-" + catID).slider('setValue', [priceRangeValue[0], priceRangeValue[1]]);
                    } else {
                        $(".pricerange-" + catID).slider('setValue', [catMin, catMax]);
                    }
                } else {
                    if (maximumProductPrice != minimumProductPrice) {
                        $(".pricerange-" + catID).slider('setValue', [minimumProductPrice, maximumProductPrice]);
                    }
                }
                if (wbStandardTracking == 1 && wbGoogleAnalyticId != '' && wbEcommerceTracking == 1) {
                    gtag('event', 'view_item_list', {
                        "event_label": 'Product Item List',
                        "items": JSON.stringify(response.gtag_items)
                    });
                }
                if ($(".popovers").size() > 0)
                {
                    $(".popovers").popover();
                }
            } else {
                //$("#category_products_list").html('No product found');
                $('.product_listing_title').hide();
                $(".pagination_div_id").html('');
                $(".filter_left_view_id").html(response['filterAttributeView']);
                $(".filter_right_view_id").html(response['filterAttributeRightView']);
                $(".filter_both_view_id").html(response['filterbothleftView']);
                $("#filter").html(response['filtermobileView']);
                $("#mobileproductcount").html(response['product_count']);
                if (response['product_count'] == '')
                {
                    $("#location_div").hide();
                }
            }
            $('.loader').css("display", "none");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            return false;
        }
    })
});

$('body').on('click', '#clearall-' + catID, function (e) {
    $('.category' + catID).attr('checked', false);
    removeQString("price");
    removeQString("brand");
    removeQString("category");
    removeQString("attribute");
    removeQString("sort");
    $('#session' + catID).html(" ");
    // $(".price_range" + catID).val("");
    localStorage.removeItem("ispriceslider" + catID);
    var FIlterSelectBox = $("[urlvalue=default]").attr("id");
    if (FIlterSelectBox != null && FIlterSelectBox != "undefined") {
        document.getElementById(FIlterSelectBox).selected = "true";
    }
    var catMax = localStorage.getItem('catMaxVal' + catID);
    var catMin = localStorage.getItem('catMinVal' + catID);
    $(".pricerange-" + catID).slider('setValue', [catMin, catMax]);
});
$('body').on('click', '#pricesession-' + catID, function (e) {
    $('#pricesession-' + catID).remove();
    removeQString("price");
    localStorage.removeItem("ispriceslider" + catID);
    if (!$('.category' + catID).is(":checked")) {
        $('#session' + catID).html(" ");
    }
});

//   $('.sort_rang').change(function(){
$('body').on('slideStop click', '.sort_rang', function (e) {
    var curkey = $(this).attr("id");
    if (curkey != null) {
        var changeType = curkey.slice(0, 9);
        var mobchangeType = curkey.slice(0, 6);
        var PriceType = curkey.slice(0, 12);
        var chekcurCategory = curkey.split('-').slice(0, 1);
    }
    
    var tempData = [];
    var attdata = [];
    var categorydata = [];
    var branddata = [];
    var attvalue = "";
    var bravalue = "";
    var priceData = "";
    //$checkboxes = $("#sidebar-menu-1 :checkbox");
    if ($('#filter_from').val() == 'web') {
        $checkboxes = $("#myDIV #sidebar-menu-1 :checkbox");
    } else {
        $checkboxes = $("#filter #sidebar-menu-1 :checkbox");
    }
    var checkboxValues = {};
    var uncheckboxValues = {};
    if (chekcurCategory == "brand") {
//        if (($("#sidebar-menu-1 input[name='brand[]']:checked").length) == 0) {
//            removeQString("brand");
//        }
        if ($('#filter_from').val() == 'web') {
            if (($("#myDIV #sidebar-menu-1 input[name='brand[]']:checked").length) == 0) {
                removeQString("brand");
            }
        } else {
            if (($("#filter #sidebar-menu-1 input[name='brand[]']:checked").length) == 0) {
                removeQString("brand");
            }
        }
    }
    if (chekcurCategory == "category") {
//        if (($("#sidebar-menu-1 input[name='category[]']:checked").length) == 0) {
//            removeQString("category");
//        }
        if ($('#filter_from').val() == 'web') {
            if (($("#myDIV #sidebar-menu-1 input[name='category[]']:checked").length) == 0) {
                removeQString("category");
            }
        } else {
            if (($("#filter #sidebar-menu-1 input[name='category[]']:checked").length) == 0) {
                removeQString("category");
            }
        }
    }
    if (chekcurCategory == "attribute") {
//        if (($("#sidebar-menu-1 input[name='attribute[]']:checked").length)/2 == 0) {
//            removeQString("attribute");
//        }
        if ($('#filter_from').val() == 'web') {
            if (($("#myDIV #sidebar-menu-1 input[name='attribute[]']:checked").length) == 0) {
                removeQString("attribute");
            }
        } else {
            if (($("#filter #sidebar-menu-1 input[name='attribute[]']:checked").length) == 0) {
                removeQString("attribute");
            }
        }
    }

    $checkboxes.each(function () {
        if (this.checked) {
            checkboxValues[this.id] = this.checked;
        }
    });

    if (e.type != "slideStop" && changeType != "Selectbox" && PriceType != "pricesession") {
        var ctr = 0;
        var acr = 0;
        var bcr = 0;
        var ccr = 0;

        $.each(checkboxValues, function (key, value) {
            //$("." + key).prop('checked', value);

            if ($('#filter_from').val() == 'web') {
                $("#myDIV ." + key).prop('checked', value);
            } else {
                $("#filter ." + key).prop('checked', value);
            }

            var chekCategory = key.split('-').slice(0, 1);

            var divkey = '';
            if ($('#filter_from').val() == 'web') {
                divkey = $("#myDIV ." + key);
            } else {
                divkey = $("#filter ." + key);
            }
            if ($(divkey).prop('checked') == true) {
                //var element_data_name = $("." + key).attr('data-name');
                if ($('#filter_from').val() == 'web') {
                    var element_data_name = $("#myDIV ." + key).attr('data-name');
                } else {
                    var element_data_name = $("#filter ." + key).attr('data-name');
                }
                if (chekCategory == "attribute") {
                    attdata.push(element_data_name);
                    attvalue = attdata.filter(item => item !== element_data_name);
                    attvalue = attdata.join("|");
                    var akey = "attribute";
                    acr++;
                } else if (chekCategory == "brand") {
                    branddata.push(element_data_name);
                    attvalue = branddata.join("|");
                    var bkey = "brand";
                    bcr++
                } else if (chekCategory == "category") {
                    categorydata.push(element_data_name);
                    attvalue = categorydata.join("|");
                    var ckey = "category";
                    ccr++
                }
                if (chekcurCategory == "brand") {
                    if (bkey != undefined) {
//                        if (($("#sidebar-menu-1 input[name='brand[]']:checked").length) == bcr) {
//                            changeUrl(bkey, attvalue);
//                        }

                        if ($('#filter_from').val() == 'web') {
                            if (($("#myDIV #sidebar-menu-1 input[name='brand[]']:checked").length) == bcr) {
                                changeUrl(bkey, attvalue);
                            }
                        } else {
                            if (($("#filter #sidebar-menu-1 input[name='brand[]']:checked").length) == bcr) {
                                changeUrl(bkey, attvalue);
                            }
                        }
                    }
                }
                if (chekcurCategory == "category") {
                    if (ckey != undefined) {
//                        if (($("#sidebar-menu-1 input[name='category[]']:checked").length) == ccr) {
//                            changeUrl(ckey, attvalue);
//                        }
                        if ($('#filter_from').val() == 'web') {
                            if (($("#myDIV #sidebar-menu-1 input[name='category[]']:checked").length) == ccr) {
                                changeUrl(ckey, attvalue);
                            }
                        } else {
                            if (($("#filter #sidebar-menu-1 input[name='category[]']:checked").length) == ccr) {
                                changeUrl(ckey, attvalue);
                            }
                        }
                    }
                }
                if (chekcurCategory == "attribute") {
                    if (akey != undefined) {
//                        if (($("#sidebar-menu-1 input[name='attribute[]']:checked").length)/2 == acr) {
//                                changeUrl(akey, attvalue);
//                            }

                        if ($('#filter_from').val() == 'web') {
                            if (($("#myDIV #sidebar-menu-1 input[name='attribute[]']:checked").length) == acr) {
                                changeUrl(akey, attvalue);
                            }
                        } else {
                            if (($("#filter #sidebar-menu-1 input[name='attribute[]']:checked").length) == acr) {
                                changeUrl(akey, attvalue);
                            }
                        }
                    }
                }
            }
        });
    }
    var minimumProductPrice = $('#Minprice' + catID).val();
    var maximumProductPrice = $('#Maxprice' + catID).val();
    var SelectboxValue = $('#Selectbox' + catID).val();
    var SelctedId = $('#select_search_form').find('option:selected').attr('id');
    var mobSelctedId = $("#select_search_form input[type='radio']:checked").attr('id');

    if (changeType == "Selectbox") {
        if (SelctedId != "undefined") {
            var selectBox = $("#" + SelctedId).attr('urlvalue');
            if (selectBox == "default") {
                removeQString("sort");
            } else {
                changeUrl("sort", selectBox);
            }
        }
    }

    if (mobchangeType == "select") {
        if (mobSelctedId != "undefined") {
            var selectBox = $("#" + mobSelctedId).attr('urlvalue');
            if (selectBox == "default") {
                removeQString("sort");
            } else {
                changeUrl("sort", selectBox);
            }
        }
    }

    var priceRangeValue = minimumProductPrice + ',' + maximumProductPrice;
    var priceValue = priceRangeValue.split(',');
    if (e.type == "slideStop") {
        $(".price_range" + catID).val(e.value);
        var list, index;
        list = document.getElementsByClassName("pricerange-" + catID);
        for (index = 0; index < list.length; ++index) {
            list[index].setAttribute("data-slider-value", "[" + e.value + "]");
        }
        var priceValue = e.value;
        var priceRangeValue = priceValue;
        changeUrl("price", priceRangeValue[0] + '|' + priceRangeValue[1]);
        localStorage.setItem("ispriceslider" + catID, "y");
    }
    var catMax = localStorage.getItem('catMaxVal' + catID);
    var catMin = localStorage.getItem('catMinVal' + catID);
    var ispriceSet = localStorage.getItem('ispriceslider' + catID);
    if (ispriceSet == "y") {
        var priceRangeValue = priceValue;
    } else {
        var priceRangeValue = [catMin, catMax];
    }
    var pageid = $(this).attr('pageid');
    var baseUrl = getBaseUrl();

    var abc = [];
    var selectedBrand = [];
    var abc1 = [];
    var top_filter_attr = '';
    var top_filter_brand = '';
    if ($('#top_filter').serializeArray().length > 0) {

//        console.log('SERIALIZE DATA = ' + $('#top_filter').serializeArray());
//        console.log('SERIALIZE DATA = ' + JSON.stringify($('#top_filter').serializeArray()));

        //var json = JSON.stringify($('#top_filter').serializeArray());
        var json = $('#top_filter').serializeArray();
        Object.keys(json).forEach(function (key) {

            if (json[key].name == 'attribute[]') {
                abc1.push(json[key].value);
                abc.push('&attribute[]=' + json[key].value);
            }
            if (json[key].name == 'brand[]') {
                abc1.push(json[key].value);
                selectedBrand.push('&brand[]=' + json[key].value);
            }
        });

        var top_filter_attr = abc.toString().replace(/\,/g, '');
        var top_filter_brand = selectedBrand.toString().replace(/\,/g, '');

//        var data = $('#top_filter').serializeArray().reduce(function (obj, item) {
//            if (jQuery.inArray(item.value, abc1) == -1) {
//                if (item.name == 'attribute[]') {
//                    abc1.push(item.value);
//                    abc.push('&attribute[]=' + item.value);
//                }
//                if (item.name == 'brand[]') {
//                    abc1.push(item.value);
//                    selectedBrand.push('&brand[]=' + item.value);
//                }
//            }
//        });
//        var top_filter_attr = abc.toString().replace(/\,/g, '');
//        var top_filter_brand = selectedBrand.toString().replace(/\,/g, '');

//        console.log('top_filter_attr = ' + top_filter_attr);
//        console.log('top_filter_brand = ' + top_filter_brand);
    }
    var showItem =  $('.show-item-per-page').val();
    if (pageid != null) {
        var filterUrl = baseUrl + '/category/filter/' + $(this).attr('pageid');
        if (catMin != "undefined" && catMax != "undefined") {
            var formData = $("#select_search_form,#search_form").serialize() + '&price=' + priceRangeValue + top_filter_attr + top_filter_brand + '&showItem=' + showItem;
        } else {
            var formData = $("#select_search_form,#search_form").serialize() + top_filter_attr + top_filter_brand + '&showItem=' + showItem;
        }
    } else {
        var filterUrl = baseUrl + '/category/filter';
        var formData = $("#select_search_form,#search_form").serialize() + '&price=' + priceRangeValue + top_filter_attr + top_filter_brand + '&showItem=' + showItem;
    }
    $.ajax({
        url: filterUrl,
        type: 'post',
        dataType: "json",
        data: formData,
        beforeSend: function () {
            $(".loader").show();
        },
        success: function (response) {
            if (response['productList'] != '') {
                $("#location_div").show();
                $('.product_listing_title').attr('style', 'display: block !important');
                $("#category_products_list").html(response['productList']);
                $(".pagination_div_id").html(response['pagination']);
                //$checkboxes = $("#sidebar-menu-1 :checkbox");
                if ($('#filter_from').val() == 'web') {
                    $checkboxes = $("#myDIV #sidebar-menu-1 :checkbox");
                } else {
                    $checkboxes = $("#filter #sidebar-menu-1 :checkbox");
                }
                $checkboxes.each(function () {
                    checkboxValues[this.id] = this.checked;
                });
                $(".filter_left_view_id").html(response['filterAttributeView']);
                $(".filter_both_view_id").html(response['filterbothleftView']);
                $(".filter_right_view_id").html(response['filterAttributeRightView']);
                $("#filter").html(response['filtermobileView']);
                $("#mobileproductcount").html(response['product_count']);
                $("#myDIV").html(response['filterAttributetopView']);
                if (priceRangeValue != null) {
                    var ispriceSet = localStorage.getItem('ispriceslider' + catID);
                    if (ispriceSet == "y") {
                        $(".pricerange-" + catID).slider('setValue', [priceRangeValue[0], priceRangeValue[1]]);
                    } else {
                        $(".pricerange-" + catID).slider('setValue', [catMin, catMax]);
                    }
                } else {
                    if (catMax != catMin) {
                        $(".pricerange-" + catID).slider('setValue', [catMin, catMax]);
                    }
                }
                var tempData = [];
                var priceData = "";
                // On page load
                $.each(checkboxValues, function (key, value) {
                    // $('#session' + catID).html(" ");
                    //$("." + key).prop('checked', value);

                    if ($('#filter_from').val() == 'web') {
                        $("#myDIV ." + key).prop('checked', value);
                    } else {
                        $("#filter ." + key).prop('checked', value);
                    }

                    var chekCategory = key.split('-').slice(1, 2);
                    if (chekCategory == catID) {
//                        if ($("#" + key).prop('checked') == true) {
//                            var element_data_name = $("#" + key).attr('data-showname');
//                            tempData.push('<span class="filters_cube" data-id="' + key + '">' + element_data_name + ' <i class="fa fa-times" aria-hidden="true"></i></span>');
//                            // var myStr = tempData.join(" ");
//                        } else {
//                            tempData.push('');
//                        }

                        if ($('#filter_from').val() == 'web') {
                            if ($("#myDIV #" + key).prop('checked') == true) {
                                var element_data_name = $("#myDIV #" + key).attr('data-showname');
                                tempData.push('<span class="filters_cube" data-id="' + key + '">' + element_data_name + ' <i class="fa fa-times" aria-hidden="true"></i></span>');
                            } else {
                                tempData.push('');
                            }
                        } else {
                            if ($("#filter #" + key).prop('checked') == true) {
                                var element_data_name = $("#filter #" + key).attr('data-showname');
                                tempData.push('<span class="filters_cube" data-id="' + key + '">' + element_data_name + ' <i class="fa fa-times" aria-hidden="true"></i></span>');
                            } else {
                                tempData.push('');
                            }
                        }

                        //var myStr =  localStorage.getItem('mystring');
                        var myStr = tempData.join(" ");
                        if (priceRangeValue != null) {
                            var ispriceSet = localStorage.getItem('ispriceslider' + catID);
                            if (ispriceSet == "y") {
                                var priceData = '<span class="filters_cube sort_rang" id="pricesession-' + catID + '">' + PriceSymbol + priceRangeValue[0] + ' TO ' + PriceSymbol + priceRangeValue[1] + '<i class="fa fa-times" aria-hidden="true"></i></span>';
                            } else {
                                var priceData = '';
                            }
                        } else {
                            var priceData = '';
                        }
                        var clearData = '<span class="filters_cube sort_rang clearall" data-id="clearall-' + catID + '">clear all</span>';
                        $('#session' + catID).html(myStr + priceData + clearData);
                        if (!$('.category' + catID).is(":checked") && priceData == '') {
                            $('#session' + catID).html(" ");
                        }
                    }
                });
                // localStorage.removeItem("mystring");
                // localStorage.setItem("checkboxValues", JSON.stringify(checkboxValues));
            } else {
                $("#category_products_list").html('No product found');
                $(".product_listing_title").hide();
                $(".pagination_div_id").html('');
                $(".filter_left_view_id").html(response['filterAttributeView']);
                $(".filter_right_view_id").html(response['filterAttributeRightView']);
                $(".filter_both_view_id").html(response['filterbothleftView']);
                $("#filter").html(response['filtermobileView']);
                $("#mobileproductcount").html(response['product_count']);
                $("#myDIV").html(response['filterAttributetopView']);
                $("#location_div").hide();
            }
            $('.loader').css("display", "none");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            return false;
        }
    })
})
//Define variable
var objQueryString = {};
//Get querystring value
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
//Add or modify querystring
function changeUrl(key, value) {
    //Get query string value
    var searchUrl = location.search;
    if (searchUrl.indexOf("?") == "-1") {
        var urlValue = '?' + key + '=' + value;
        history.pushState({state: 1, rand: Math.random()}, '', urlValue);
    } else {
        //Check for key in query string, if not present
        if (searchUrl.indexOf(key) == "-1") {
            var urlValue = searchUrl + '&' + key + '=' + value;
        } else {  //If key present in query string
            oldValue = getParameterByName(key);
            if (searchUrl.indexOf("?" + key + "=") != "-1") {
                urlValue = searchUrl.replace('?' + key + '=' + oldValue, '?' + key + '=' + value);
            } else {
                urlValue = searchUrl.replace('&' + key + '=' + oldValue, '&' + key + '=' + value);
            }
        }
        //var urlValue = encodeURI(urlValue);
        history.pushState({state: 1, rand: Math.random()}, '', urlValue);
        //history.pushState function is used to add history state.
        //It takes three parameters: a state object, a title (which is currently ignored), and (optionally) a URL.
    }
    objQueryString.key = value;
}
//Function used to remove querystring
function removeQString(key) {
    var urlValue = document.location.href;
    //Get query string value
    var searchUrl = location.search;
    if (key != "") {
        oldValue = getParameterByName(key);
        removeVal = key + "=" + oldValue;
        if (searchUrl.indexOf('?' + removeVal + '&') != "-1") {
            urlValue = urlValue.replace('?' + removeVal + '&', '?');
        } else if (searchUrl.indexOf('&' + removeVal + '&') != "-1") {
            urlValue = urlValue.replace('&' + removeVal + '&', '&');
        } else if (searchUrl.indexOf('?' + removeVal) != "-1") {
            urlValue = urlValue.replace('?' + removeVal, '');
        } else if (searchUrl.indexOf('&' + removeVal) != "-1") {
            urlValue = urlValue.replace('&' + removeVal, '');
        }
    } else {
        var searchUrl = location.search;
        urlValue = urlValue.replace(searchUrl, '');
    }
    history.pushState({state: 1, rand: Math.random()}, '', urlValue);
}

//mobile ajax load data start
if (mobileview == 1) {
    $(window).scroll(function () {
        if ($(window).scrollTop() >= ($(document).height() - $(window).height()) * 0.7) {
            var page = $(".page_number:last").val();
            var total_record = $(".total_record").val();
            var perpage_record = $(".perpage_record").val();
            if (parseInt(perpage_record) <= parseInt(total_record)) {
                var available_page = total_record / perpage_record;
                available_page = parseInt(available_page, 10);
                var mod_page = total_record % perpage_record;
                if (mod_page > 0) {
                    available_page = available_page + 1;
                }
                //if ($(".page_number:last").val() <= $(".total_record").val()) {
                if (parseInt(page) < parseInt(available_page)) {
                    var pagenum = parseInt($(".page_number:last").val()) + 1;
                    mobile_product_list(pagenum);
                }
            }
        }
    });

    var isProcessing = false;
    function mobile_product_list(pagenum) {
        var minimumProductPrice = $('#Minprice' + catID).val();
        var maximumProductPrice = $('#Maxprice' + catID).val();
        var priceRangeValue = minimumProductPrice + ',' + maximumProductPrice;
        var priceValue = priceRangeValue.split(',');

        var catMax = localStorage.getItem('catMaxVal' + catID);
        var catMin = localStorage.getItem('catMinVal' + catID);
        var ispriceSet = localStorage.getItem('ispriceslider' + catID);
        if (ispriceSet == "y") {
            var priceRangeValue = priceValue;
        } else {
            var priceRangeValue = [catMin, catMax];
        }
        var baseUrl = getBaseUrl();

        var abc = [];
        var selectedBrand = [];
        var abc1 = [];
        var top_filter_attr = '';
        var top_filter_brand = '';
        if ($('#top_filter').serializeArray().length > 0) {
            //        console.log('SERIALIZE DATA = ' + $('#top_filter').serializeArray());
//        console.log('SERIALIZE DATA = ' + JSON.stringify($('#top_filter').serializeArray()));

            //var json = JSON.stringify($('#top_filter').serializeArray());
            var json = $('#top_filter').serializeArray();
            Object.keys(json).forEach(function (key) {

                if (json[key].name == 'attribute[]') {
                    abc1.push(json[key].value);
                    abc.push('&attribute[]=' + json[key].value);
                }
                if (json[key].name == 'brand[]') {
                    abc1.push(json[key].value);
                    selectedBrand.push('&brand[]=' + json[key].value);
                }
            });

            var top_filter_attr = abc.toString().replace(/\,/g, '');
            var top_filter_brand = selectedBrand.toString().replace(/\,/g, '');

//        var data = $('#top_filter').serializeArray().reduce(function (obj, item) {
//            if (jQuery.inArray(item.value, abc1) == -1) {
//                if (item.name == 'attribute[]') {
//                    abc1.push(item.value);
//                    abc.push('&attribute[]=' + item.value);
//                }
//                if (item.name == 'brand[]') {
//                    abc1.push(item.value);
//                    selectedBrand.push('&brand[]=' + item.value);
//                }
//            }
//        });
//        var top_filter_attr = abc.toString().replace(/\,/g, '');
//        var top_filter_brand = selectedBrand.toString().replace(/\,/g, '');

//        console.log('top_filter_attr = ' + top_filter_attr);
//        console.log('top_filter_brand = ' + top_filter_brand);
        }

        if (pagenum != null) {
            var filterUrl = baseUrl + '/category/filter/' + pagenum;
            var formData = $("#select_search_form,#search_form").serialize() + '&price=' + priceRangeValue + top_filter_attr + top_filter_brand;
        } else {
            var filterUrl = baseUrl + '/category/filter';
            var formData = $("#select_search_form,#search_form").serialize() + '&price=' + priceRangeValue + top_filter_attr + top_filter_brand;
        }
        if (isProcessing) {
            /*
             *This won't go past this condition while
             *isProcessing is true.
             *You could even display a message.
             **/
            return;
        }
        isProcessing = true;
        $.ajax({
            url: filterUrl,
            type: 'post',
            dataType: "json",
            data: formData,
            beforeSend: function () {
                $(".loader").show();
            },
            success: function (response) {
                if (response['product_list'] != '') {
                    $('.product_listing_title').attr('style', 'display: block !important');
                    $("#category_products_list").append(response['product_list']);
                    $(".pagination_div_id").html(response['pagination']);
                    $("#filter").html(response['filtermobileView']);
                    $("#mobileproductcount").html(response['product_count']);
                    $("#location_div").show();
                    if ($(".popovers").size() > 0) {
                        $(".popovers").popover();
                    }
                    // localStorage.removeItem("mystring");
                    // localStorage.setItem("checkboxValues", JSON.stringify(checkboxValues));
                } else {
                    $("#category_products_list").html('No product found');
                    $(".product_listing_title").hide();
                    $(".pagination_div_id").html('');
                    $(".filter_left_view_id").html(response['filterAttributeView']);
                    $(".filter_right_view_id").html(response['filterAttributeRightView']);
                    $(".filter_both_view_id").html(response['filterbothleftView']);
                    $("#filter").html(response['filtermobileView']);
                    $("#mobileproductcount").html(response['product_count']);
                    $("#location_div").hide();
                }
                $('.loader').css("display", "none");
                isProcessing = false;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                return false;
            }
        })
    }
}
//mobile ajax load data end