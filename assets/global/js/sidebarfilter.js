var categoryId = $('#pageCatId').val();
var PriceSymbol = $('#PriceSymbol-' + categoryId).val();
var categorySesion = $('#CategorySession' + categoryId).val();
var minimumProductPrice = $('#Minprice' + categoryId).val();
var maximumProductPrice = $('#Maxprice' + categoryId).val();
localStorage.removeItem("ispriceslider" + categoryId);
var searchKeyword = $('#searchKeyword').val();
var promotionId = $('#promotionId').val();


window.onpopstate = function (event) {
    if (event && event.state) {
        location.reload();
    }
}
$('body').on('click', '#clearall-' + categoryId, function (e) {
    $('.category' + categoryId).attr('checked', false);
    removeQString1("page");
    removeQString1("pricerange");
    removeQString1("brand");
    removeQString1("category");
    removeQString1("attribute");
    removeQString1("sort");
    removeQString1("rating");
    $('#session_' + categoryId).html(" ");
    $(".price_range" + categoryId).val("");
    localStorage.removeItem("ispriceslider" + categoryId);
    var FIlterSelectBox = $("[urlvalue=default]").attr("id");
    if (FIlterSelectBox != null && FIlterSelectBox != "undefined") {
        document.getElementById(FIlterSelectBox).selected = "true";
    }
    var catMax = localStorage.getItem('catMaxVal' + categoryId);
    var catMin = localStorage.getItem('catMinVal' + categoryId);
    $(".pricerange-" + categoryId).slider('setValue', [catMin, catMax]);

    var filter_class;
    filter_class = "#sidebar-menu-1 ul.sidebar";
    var thisdata = this;
    getCheckboxValue1(e, thisdata, filter_class);
    location.reload();
});

$(document).ready(function (e) {
    var filter_class;
//    if(deviceType == 'phone'){
//        filter_class = ".filter-main ul.c-sidebar-menu";
//    }else{
//        filter_class = ".filter-main ul.filter-bar";
//    }
    filter_class = "#sidebar-menu-1 ul.sidebar";
    var thisdata = this;

    if(searchKeyword != '' && searchKeyword != undefined){
        changeUrlWithoutLoad1('q', encodeURIComponent(searchKeyword));
    }
    
    // selected box set in session
    var tempData = [];
    var price = getParameterByName1('pricerange');
    if (checkboxValues == undefined) {
        $checkboxes = $(filter_class + " :checkbox");
        var checkboxValues = {};
        $checkboxes.each(function () {
            if (this.checked) {
                checkboxValues[this.id] = this.checked;
            }
        });
    }
    var minimumProductPrice = $('#Minprice_' + categoryId).val();
    var maximumProductPrice = $('#Maxprice_' + categoryId).val();

    if (minimumProductPrice != maximumProductPrice) {
        $(".pricerange-" + categoryId).slider('setValue', [minimumProductPrice, maximumProductPrice]);
    }
    if (price != '') {
        var prices = price.split('|');
        if (prices[0] != '' && prices[1] != '') {
            var priceData = '<span class="filters_cube" id="pricesession-' + categoryId + '">' + PriceSymbol + prices[0] + ' TO ' + PriceSymbol + prices[1] + '<i class="fa fa-times" aria-hidden="true"></i></span>';
        } else {
            var priceData = '';
        }
        var clearData = '<span class="filters_cube_clear1  clearall" data-id="clearall-' + categoryId + '" id="clearall-' + categoryId + '">clear all</span>';
      //  $('#session_' + categoryId).html(priceData);

    }
    $.each(checkboxValues, function (key, value) {
        $(filter_class + " ." + key).prop('checked', value);
        var chekCategory = key.split('-').slice(1, 2);
        if (chekCategory == categoryId) {
            if ($(filter_class + " #" + key).prop('checked') == true) {alert(filter_class)
                var element_data_name = $(filter_class + " #" + key).attr('data-showname');
                tempData.push('<span class="filters_cube" data-id="' + key + '">' + element_data_name + ' <i class="fa fa-times" aria-hidden="true"></i></span>');
            } else {
                tempData.push('');
            }

            //var myStr =  localStorage.getItem('mystring');
            var myStr = tempData.join(" ");
            if (price != null) {
                var prices = price.split('|');
                if (prices[0] != '' && prices[1] != '') {
                    var priceData = '<span class="filters_cube " id="pricesession-' + categoryId + '">' + PriceSymbol + prices[0] + ' TO ' + PriceSymbol + prices[1] + '<i class="fa fa-times" aria-hidden="true"></i></span>';
                } else {
                    var priceData = '';
                }
            }
            var clearData = '<span class="filters_cube_clear1  clearall" data-id="clearall-' + categoryId + '"  id="clearall-' + categoryId + '">clear all</span>';
          //  $('#session_' + categoryId).html(myStr + priceData);
            if (!$('.category' + categoryId).is(":checked")) {
                $('#session_' + categoryId).html(" ");
            }
        }
    });
    getCheckboxValue1(e, thisdata, filter_class);
});

$('body').on('click', '.sort_rang', function (e) {
    var filter_class;
    filter_class = "#sidebar-menu-1 ul.sidebar";
    var thisdata = this;
    removeQString1('page');
    getCheckboxValue1(e, thisdata, filter_class);
});

$('body').on('click', '#sidebarright', function (e) {
    var filter_class;
    filter_class = "#sidebar-menu-1 ul.sidebar";
    var thisdata = this;
    removeQString1('page');
    getCheckboxValue1(e, thisdata, filter_class);
});

$('body').on('click', '#pricesession-' + categoryId, function (e) {
    $('#pricesession-' + categoryId).remove();
    removeQString1("pricerange");
    localStorage.removeItem("ispriceslider" + categoryId);
    if (!$('.category' + categoryId).is(":checked")) {
        $('#session_' + categoryId).html(" ");
    }
    location.reload();
});


function getCheckboxValue1(e, thisdata, filter_class) {
    var curkey = $(thisdata).attr("id");
    if (curkey != null) {
        var chekcurCategory = curkey.split('-').slice(0, 1);
    }
    var attdata = [];
    var branddata = [];
    var categorydata = [];
    var ratingdata = [];
    var attvalue = "";
    $checkboxes = $(filter_class + " :checkbox");
    var checkboxValues = {};
    if (chekcurCategory == "brand") {
        if (($(filter_class + " input[name='brand[]']:checked").length) == 0) {
            removeQString1("brand");
        }
    }
    if (chekcurCategory == "category") {
        if (($(filter_class + " input[name='category[]']:checked").length) == 0) {
            removeQString1("category");
        }
    }
    if (chekcurCategory == "attribute") {
        if (($(filter_class + " input[name='attribute[]']:checked").length) == 0) {
            removeQString1("attribute");
        }
    }
    if (chekcurCategory == "rating") {
        if (($(filter_class + " input[name='rating[]']:checked").length) == 0) {
            removeQString1("rating");
        }
    }
    $checkboxes.each(function () {
        if (this.checked) {
            checkboxValues[this.id] = this.checked;
        }
    });
    var acr = 0;
    var bcr = 0;
    var ccr = 0;
    var rcr = 0;
    $.each(checkboxValues, function (key, value) {
        $(filter_class + " ." + key).prop('checked', value);
        var chekCategory = key.split('-').slice(0, 1);
        var divkey = '';
        divkey = $(filter_class + " ." + key);
        if ($(divkey).prop('checked') == true) {
            var element_data_name = $(filter_class + " ." + key).attr('data-name');
            if (chekCategory == "attribute") {
                attdata.push(element_data_name);
                attvalue = attdata.filter(item => item !== element_data_name);
                attvalue = attdata.join("|");
                var akey = "attribute";
                acr++;
            } else if (chekCategory == "brand") {
                branddata.push(element_data_name);
                attvalue = branddata.join("|");
                var bkey = "brand";
                bcr++;
            } else if (chekCategory == "category") {
                categorydata.push(element_data_name);
                attvalue = categorydata.join("|");
                var ckey = "category";
                ccr++;
            } else if (chekCategory == "rating") {
                ratingdata.push(element_data_name);
                attvalue = ratingdata.join("|");
                var rkey = "rating";
                rcr++;
            }
            if (chekcurCategory == "brand") {
                if (bkey != undefined) {
                    if (($(filter_class + " input[name='brand[]']:checked").length) == bcr) {
                        changeUrl1(bkey, attvalue);
                    }
                }
            }
            if (chekcurCategory == "category") {
                if (ckey != undefined) {
                    if (($(filter_class + " input[name='category[]']:checked").length) == ccr) {
                        changeUrl1(ckey, attvalue);
                    }
                }
            }
            if (chekcurCategory == "attribute") {
                if (akey != undefined) {
                    if (($(filter_class + " input[name='attribute[]']:checked").length) == acr) {
                        changeUrl1(akey, attvalue);
                    }
                }
            }
            if (chekcurCategory == "rating") {
                if (rkey != undefined) {
                    if (($(filter_class + " input[name='rating[]']:checked").length) == rcr) {
                        changeUrl1(rkey, attvalue);
                    }
                }
            }
        }
    });

    var minimumProductPrice = $('#Minprice_' + categoryId).val();
    var maximumProductPrice = $('#Maxprice_' + categoryId).val();

    var priceRangeValue = minimumProductPrice + ',' + maximumProductPrice;
    var priceValue = priceRangeValue.split(',');
    if (e.type == "slideStop") {
        $(".price_range" + categoryId).val(e.value);
        var list, index;
        list = document.getElementsByClassName("pricerange-" + categoryId);
        for (index = 0; index < list.length; ++index) {
            list[index].setAttribute("data-slider-value", "[" + e.value + "]");
        }
        var priceValue = e.value;
        var priceRangeValue = priceValue;
        changeUrl1("pricerange", priceRangeValue[0] + '|' + priceRangeValue[1]);
        localStorage.setItem("ispriceslider" + categoryId, "y");
    }
    var catMax = localStorage.getItem('catMaxVal' + categoryId);
    var catMin = localStorage.getItem('catMinVal' + categoryId);
    var ispriceSet = localStorage.getItem('ispriceslider' + categoryId);
    if (ispriceSet == "y") {
        var priceRangeValue = priceValue;
    } else {
        var priceRangeValue = [catMin, catMax];
    }

    if ($('#category_products_list').is(":visible")) {
        //getFilterData(e, checkboxValues, filter_class);
    } else {
        mobile_product_list(e, checkboxValues, filter_class, 1);
    }
}

//Define variable
var objQueryString = {};
//Get querystring value
function getParameterByName1(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
//Add or modify querystring
function changeUrl1(key, value) {
    //Get query string value
    var searchUrl = location.search;
    if (searchUrl.indexOf("?") == "-1") {
        var urlValue = '?' + key + '=' + value;
        history.pushState({state: 1, rand: Math.random()}, '', urlValue);
    } else {
        //Check for key in query string, if not present
        if (searchUrl.indexOf(key) == "-1") {
            var urlValue = searchUrl + '&' + key + '=' + value;
        } else {  //If key present in query string
            oldValue = getParameterByName1(key);
            if (searchUrl.indexOf("?" + key + "=") != "-1") {
                urlValue = searchUrl.replace('?' + key + '=' + oldValue, '?' + key + '=' + value);
            } else {
                urlValue = searchUrl.replace('&' + key + '=' + oldValue, '&' + key + '=' + value);
            }
        }
        //var urlValue = encodeURI(urlValue);
        history.pushState({state: 1, rand: Math.random()}, '', urlValue);
        //history.pushState function is used to add history state.
        //It takes three parameters: a state object, a title (which is currently ignored), and (optionally) a URL.
    }
    objQueryString.key = value;
    location.reload();
}
function changeUrlWithoutLoad1(key, value) {
    //Get query string value
    var searchUrl = location.search;
    if (searchUrl.indexOf("?") == "-1") {
        var urlValue = '?' + key + '=' + value;
        history.pushState({state: 1, rand: Math.random()}, '', urlValue);
    } else {
        //Check for key in query string, if not present
        if (searchUrl.indexOf(key) == "-1") {
            var urlValue = searchUrl + '&' + key + '=' + value;
        } else {  //If key present in query string
            oldValue = getParameterByName1(key);
            if (searchUrl.indexOf("?" + key + "=") != "-1") {
                urlValue = searchUrl.replace('?' + key + '=' + oldValue, '?' + key + '=' + value);
            } else {
                urlValue = searchUrl.replace('&' + key + '=' + oldValue, '&' + key + '=' + value);
            }
        }
        //var urlValue = encodeURI(urlValue);
        history.pushState({state: 1, rand: Math.random()}, '', urlValue);
        //history.pushState function is used to add history state.
        //It takes three parameters: a state object, a title (which is currently ignored), and (optionally) a URL.
    }
    objQueryString.key = value;
}
//Function used to remove querystring
function removeQString1(key) {
    var urlValue = document.location.href;
    //Get query string value
    var searchUrl = location.search;
    if (key != "") {
        oldValue = getParameterByName1(key);
        removeVal = key + "=" + oldValue;
        if (searchUrl.indexOf('?' + removeVal + '&') != "-1") {
            urlValue = urlValue.replace('?' + removeVal + '&', '?');
        } else if (searchUrl.indexOf('&' + removeVal + '&') != "-1") {
            urlValue = urlValue.replace('&' + removeVal + '&', '&');
        } else if (searchUrl.indexOf('?' + removeVal) != "-1") {
            urlValue = urlValue.replace('?' + removeVal, '');
        } else if (searchUrl.indexOf('&' + removeVal) != "-1") {
            urlValue = urlValue.replace('&' + removeVal, '');
        }
    } else {
        var searchUrl = location.search;
        urlValue = urlValue.replace(searchUrl, '');
    }
    history.pushState({state: 1, rand: Math.random()}, '', urlValue);
    location.reload();
}

$("body").delegate("span.filters_cube1", "click", function (e) {
    e.preventDefault();
    var filterid = 0;
    filterid = $(this).attr('data-id');
    if (filterid != 0 && filterid != undefined) {
        $('#' + filterid).trigger("click");
        location.reload();
    }
});

