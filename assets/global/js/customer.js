var siteUrl = document.location.origin;
$('#login-form').on('hidden.bs.modal', function () {
    $(this).find('form')[0].reset();
    $('#frmcustomer_login').bootstrapValidator('resetForm', true);
})
$('#forget-password-form').on('hidden.bs.modal', function () {
    $(this).find('form')[0].reset();
    $('#frmforgotpass').bootstrapValidator('resetForm', true);
})
$('#frmcust_login').find('input:first').focus();
$(".regnow").click(function () {
    if (!$("#frm_customerreg").hasClass("show")) {
        $(".c-form-register").addClass('show');
        $(".c-form-register").removeClass("hide");
        $('#frm_customerreg').find('select:first').focus();
        $("#frm_customerreg")[0].reset();
        $("#frm_customerreg").data('bootstrapValidator').resetForm();
    } else {
        $(".c-form-register").addClass('hide');
        $(".c-form-register").removeClass("show");
        $('#frmcust_login').find('input:first').focus();
    }
});
(function ($) {
    Flash = {}
    Flash.success = function (msg, time) {
        time = time || 3000;
        $('#flash-container')[0].innerHTML = "<div class='alert alert-success message'>" + msg + "</div>";
        $('#flash-container').addClass('show');
        setTimeout(function () {
            $('#flash-container').removeClass('show');
        }, time);
    };
})(jQuery);
$(document).ready(function () {
    if ($("#frmforgotpass").length > 0) {
        var currentPath = $('#currentPath').val();
        if (currentPath == '/saleslogin') {
            site_url = site_url.replace(/saleslogin/g, "..");
        } else if (currentPath == '/retailerlogin') {
            site_url = site_url.replace(/retailerlogin/g, "..");
        } else {
            site_url = site_url.replace(/login/g, "..");
        }
        site_url = site_url.replace(/checkout/g, "..");
        site_url1 = strstr(site_url, '..', true);
        if (site_url1 == false) {
            site_url1 = forUrl + '/';
        }
        $('#frmforgotpass').bootstrapValidator({
            excluded: ':disabled',
            feedbackIcons: {valid: 'fa', invalid: 'err', validating: 'fa'},
            fields: {
                forgetemail: {
                    validators: {
                        notEmpty: {message: 'Please enter Email-Id'},
                        emailAddress: {message: 'Please enter valid Email-Id'},
                        remote: {
                            type: 'POST',
                            url: site_url1 + "home/checkforgotpass",
                            data: function (validator, $field, value) {
                                emailid = validator.getFieldElements('form[emailid]').val();
                                return {emailid: emailid, };
                            },
                            message: 'Your Email-Id is not register with us',
                            delay: 500
                        },
                    }
                },
            },
            submitHandler: function (validator, form, submitButton) {}
        });
    }
    if ($("#frmchangepassword").size() > 0) {
        $('#frmchangepassword').bootstrapValidator({
            excluded: ':disabled',
            feedbackIcons: {valid: 'fa', invalid: 'err', validating: 'fa'},
            fields: {
                'form[old_password]': {
                    validators: {
                        notEmpty: {message: 'Please enter Old Password'},
                        remote: {
                            type: 'POST',
                            url: site_url1 + "/../home/checkoldpassword",
                            data: function (validator, $field, value) {
                                oldpasword = validator.getFieldElements('form[old_password]').val();
                                return {oldpasword: oldpasword, };
                            },
                            message: 'Please enter correct Old Password',
                            delay: 500
                        },
                    }
                },
                'form[new_password]': {validators: {notEmpty: {message: 'Please enter New Password'}, stringLength: {min: 6, max: 16, message: 'Password between 6 to 16 character long'}, }},
                'form[confirm_password]': {validators: {notEmpty: {message: 'Please enter Confirm Password'}, identical: {field: 'form[new_password]', message: 'The password and its new are not the same'}}},
            },
            submitHandler: function (validator, form, submitButton) {}
        });
    }
    $('#frmcust_login').bootstrapValidator({feedbackIcons: {valid: 'fa', invalid: 'err', validating: 'fa'}, fields: {cust_emailid: {validators: {notEmpty: {message: 'Please enter Email-Id'}, emailAddress: {message: 'Please enter valid Email-Id'}, }}, cust_password: {validators: {notEmpty: {message: 'Please enter Password'}, stringLength: {min: 6, max: 16, message: 'Password between 6 to 16 character long'}, }}, }, submitHandler: function (validator, form, submitButton) {}});
    $('#frmcustomer_login').bootstrapValidator({excluded: ':disabled', feedbackIcons: {valid: 'fa', invalid: 'err', validating: 'fa'}, fields: {cust_emailid: {validators: {notEmpty: {message: 'Please enter Email-Id'}, emailAddress: {message: 'Please enter valid Email-Id'}, }}, cust_password: {validators: {notEmpty: {message: 'Please enter Password'}, stringLength: {min: 6, max: 16, message: 'Password between 6 to 16 character long'}, }}, }, submitHandler: function (validator, form, submitButton) {}});
    if ($('#frmsubscribe').length > 0) {
        $('#frmsubscribe').bootstrapValidator({
            feedbackIcons: {valid: 'fa', invalid: 'err', validating: 'fa'},
            fields: {
                emailid: {
                    validators: {
                        notEmpty: {message: 'Please enter Email-Id'},
                        emailAddress: {message: 'Please enter valid Email-Id'},
                        remote: {
                            type: 'POST',
                            url: siteUrl + "/home/checksubscribe",
                            data: function (validator, $field, value) {
                                emailid = validator.getFieldElements('emailid').val();
                                return {emailid: emailid, };
                            },
                            message: 'Your Email-Id is alreday in Subscriber list',
                            delay: 500
                        },
                    }
                },
            },
            submitHandler: function (validator, form, submitButton) {}
        });
    }
    if ($("#frmnewregister").size() > 0) {
        site_url = site_url.replace(/login/g, "..");
        site_url1 = strstr(site_url, '..', true);
        if (site_url1 == false) {
            site_url1 = site_url;
        }
        $('#frmnewregister').bootstrapValidator({
            feedbackIcons: {valid: 'fa', invalid: 'err', validating: 'fa'},
            fields: {
                'form[firstName]': {validators: {notEmpty: {message: 'Please enter Firstname'}, regexp: {regexp: /^[^-\s][a-z0-9\\\/\_\-\s&\']+[^-\s]$/i, message: 'Please enter valid Firstname'}, }},
                'form[lastName]': {validators: {notEmpty: {message: 'Please enter Lastname'}, regexp: {regexp: /^[^-\s][a-z0-9\\\/\_\-\s&\']+[^-\s]$/i, message: 'Please enter valid Lastname'}, }},
                'form[emailid]': {
                    validators: {
                        notEmpty: {message: 'Please enter Email-Id'},
                        emailAddress: {message: 'Please enter valid Email-Id'},
                        remote: {
                            type: 'POST',
                            url: getBaseUrl() + "/home/checkregister",
                            data: function (validator, $field, value) {
                                emailid = validator.getFieldElements('form[emailid]').val();
                                return {emailid: emailid, };
                            },
                            message: 'Your Email-Id is alreday register with us',
                            delay: 500
                        },
                    }
                },
                'form[password]': {validators: {notEmpty: {message: 'Please enter Password'}, stringLength: {min: 6, max: 16, message: 'Password between 6 to 16 character long'}}},
                'form[contact]': {validators: {notEmpty: {message: 'Please enter Contact Number'}, stringLength: {min: 10, message: 'Contact Number must be 10 number long'}, regexp: {regexp: /^[^-\s][0-9()+-\s]+(?:,\d+)*$/i, message: 'Please enter valid Contact Number'}, }},
            },
            submitHandler: function (validator, form, submitButton) {}
        });
    }
    if ($("#frmprofile").size() > 0) {
        $("#countryId").select2({placeholder: countryPlaceholder, allowClear: true, width: '100%', });
        $("#stateId").select2({placeholder: statePlaceholder, allowClear: true, width: '100%', });
        $("#city").select2({placeholder: cityPlaceholder, allowClear: true, width: '100%', });
        $('#frmprofile').bootstrapValidator({feedbackIcons: {valid: 'fa', invalid: 'err', validating: 'fa'}, fields: {'form[firstName]': {validators: {notEmpty: {message: 'Please enter Firstname'}, regexp: {regexp: /^[^-\s][a-z0-9\\\/\_\-\s&\']+[^-\s]$/i, message: 'Please enter valid Firstname'}, }}, 'form[lastName]': {validators: {notEmpty: {message: 'Please enter Lastname'}, regexp: {regexp: /^[^-\s][a-z0-9\\\/\_\-\s&\']+[^-\s]$/i, message: 'Please enter valid Lastname'}, }}, 'form[address1]': {validators: {notEmpty: {message: 'Please enter Street Address'}, stringLength: {max: 50, message: 'Street Address can not be more than 50 characters'}, }}, countryId: {validators: {notEmpty: {message: 'Please select Country'}, }}, stateId: {validators: {notEmpty: {message: 'Please enter State / Country'}, }}, city: {validators: {notEmpty: {message: 'Please enter Town / City'}, }}, 'form[zipcode]': {validators: {notEmpty: {message: 'Please enter Postcode / Zip'}, stringLength: {max: 15, message: 'Postcode / Zip can not be more than 15 characters'}, }}, 'form[fax]': {validators: {stringLength: {max: 15, message: 'Fax can not be more than 15 characters'}, }}, 'form[contact]': {validators: {notEmpty: {message: 'Please enter Phone Number'}, stringLength: {min: 10, message: 'Phone Number must be 10 number long'}, regexp: {regexp: /^[^-\s][0-9()+-\s]+(?:,\d+)*$/i, message: 'Please enter valid Phone Number'}, }}, }, submitHandler: function (validator, form, submitButton) {}});
    }
    if ($("#frm_customerreg").size() > 0) {
        site_url = site_url.replace(/login/g, "..");
        site_url1 = strstr(site_url, '..', true);
        if (site_url1 == false) {
            site_url1 = site_url;
        }
        $('#frm_customerreg').bootstrapValidator({
            feedbackIcons: {valid: 'fa', invalid: 'err', validating: 'fa'},
            fields: {
                countryId: {validators: {notEmpty: {message: 'Please select Country'}, }},
                'form[firstName]': {validators: {notEmpty: {message: 'Please enter Firstname'}, regexp: {regexp: /^[^-\s][a-z0-9\\\/\_\-\s&\']+[^-\s]$/i, message: 'Please enter valid Firstname'}, }},
                'form[lastName]': {validators: {notEmpty: {message: 'Please enter Lastname'}, regexp: {regexp: /^[^-\s][a-z0-9\\\/\_\-\s&\']+[^-\s]$/i, message: 'Please enter valid Lastname'}, }},
                'form[address1]': {validators: {notEmpty: {message: 'Please enter Street Address'}, regexp: {regexp: /^[^-\s][a-z0-9\\\/\_\-\s&\,.']+[^-\s]$/i, message: 'Please enter valid Street Address'}, }},
                'form[address2]': {validators: {regexp: {regexp: /^[^-\s][a-z0-9\\\/\_\-\s&\,.']+[^-\s]$/i, message: 'Please enter valid Apartment, Suite, Unit'}, }},
                city: {validators: {notEmpty: {message: 'Please enter Town / City'}, }},
                stateId: {validators: {notEmpty: {message: 'Please enter State / County'}, }},
                'form[zipcode]': {validators: {notEmpty: {message: 'Please enter Postcode / Zip'}, stringLength: {min: 4, message: 'Postcode / Zip must be 4 number long'}, regexp: {regexp: /^[^-\s][a-z0-9()-\s]+[^-\s]$/i, message: 'Please enter valid Postcode / Zip'}, }},
                'form[emailid]': {
                    validators: {
                        notEmpty: {message: 'Please enter Email-Id'},
                        emailAddress: {message: 'Please enter valid Email-Id'},
                        remote: {
                            type: 'POST',
                            url: site_url1 + "home/checkregister",
                            data: function (validator, $field, value) {
                                emailid = validator.getFieldElements('form[emailid]').val();
                                return {emailid: emailid, };
                            },
                            message: 'Your Email-Id is alreday register with us',
                            delay: 500
                        },
                    }
                },
                'form[contact]': {validators: {notEmpty: {message: 'Please enter Phone Number'}, stringLength: {min: 10, message: 'Phone Number must be 10 number long'}, regexp: {regexp: /^[^-\s][0-9()+-\s]+(?:,\d+)*$/i, message: 'Please enter valid Phone Number'}, }},
                'form[fax]': {validators: {stringLength: {min: 10, message: 'Fax Number must be 10 number long'}, regexp: {regexp: /^[^-\s][0-9()+-\s]+[^-\s]$/i, message: 'Please enter valid Fax Number'}, }},
                'form[password]': {validators: {notEmpty: {message: 'Please enter Password'}, stringLength: {min: 6, max: 16, message: 'Password between 6 to 16 character long'}}},
            },
        });
    }
    if ($("#frmcheckout").size() > 0) {
        $(function () {
            $("select#billingaddressId").change();
        });
        $("#billing_countryId").select2({placeholder: countryPlaceholder, allowClear: true, width: '100%', });
        $("#form_billing_stateId").select2({placeholder: statePlaceholder, allowClear: true, width: '100%', });
        $("#form_billing_city").select2({placeholder: cityPlaceholder, allowClear: true, width: '100%', });
        site_url = site_url.replace(/checkout/g, "..");
        site_url1 = strstr(site_url, '..', true);
        if (site_url1 == false) {
            site_url1 = site_url;
        }
        $('#frmcheckout').bootstrapValidator({
            feedbackIcons: {valid: 'fa', invalid: 'err', validating: 'fa'},
            fields: {
                'form[shipping_firstName]': {validators: {notEmpty: {message: 'Please enter Firstname'}, regexp: {regexp: /^[^-\s][a-z0-9\\\/\_\-\s&\']+[^-\s]$/i, message: 'Please enter valid Firstname'}, }},
                'form[shipping_lastName]': {validators: {notEmpty: {message: 'Please enter Lastname'}, regexp: {regexp: /^[^-\s][a-z0-9\\\/\_\-\s&\']+[^-\s]$/i, message: 'Please enter valid Lastname'}, }},
                'form[shipping_address1]': {validators: {notEmpty: {message: 'Please enter Street Address'}, regexp: {regexp: /^[^-\s][a-z0-9\\\/\_\-\s&\,.']+[^-\s]$/i, message: 'Please enter valid Street Address'}, }},
                'form[shipping_address2]': {validators: {regexp: {regexp: /^[^-\s][a-z0-9\\\/\_\-\s&\,.']+[^-\s]$/i, message: 'Please enter valid Apartment, Suite, Unit'}, }},
                shipping_countryId: {validators: {notEmpty: {message: 'Please select Country'}, }},
                shippingstateId: {validators: {notEmpty: {message: 'Please enter State / County'}, }},
                'form[shipping_city]': {validators: {notEmpty: {message: 'Please enter Town / City'}, regexp: {regexp: /^[^-\s][a-z0-9\\\/\_\-\s&\']+[^-\s]$/i, message: 'Please enter valid Town / City'}, }},
                'form[shipping_zipcode]': {validators: {notEmpty: {message: 'Please enter Postcode / Zip'}, stringLength: {min: 4, message: 'Postcode / Zip must be 4 number long'}, regexp: {regexp: /^[^-\s][a-z0-9()-\s]+[^-\s]$/i, message: 'Please enter valid Postcode / Zip'}, }},
                'form[shipping_emailid]': {
                    validators: {
                        notEmpty: {message: 'Please enter Email-Id'},
                        emailAddress: {message: 'Please enter valid Email-Id'},
                        remote: {
                            type: 'POST',
                            url: site_url1 + "home/checkregister",
                            data: function (validator, $field, value) {
                                custid = $("#hdn_custid").val();
                                if (custid == "") {
                                    emailid = validator.getFieldElements('form[shipping_emailid]').val();
                                    return {emailid: emailid, };
                                } else {
                                    return {valid: true};
                                }
                            },
                            message: 'Your Email-Id is alreday register with us',
                            delay: 500
                        },
                    }
                },
                'form[shipping_contact]': {validators: {notEmpty: {message: 'Please enter Phone Number'}, stringLength: {min: 10, message: 'Phone Number must be 10 number long'}, regexp: {regexp: /^[^-\s][0-9()+-\s]+(?:,\d+)*$/i, message: 'Please enter valid Phone Number'}, }},
                'form[shipping_fax]': {validators: {stringLength: {min: 10, message: 'Fax Number must be 10 number long'}, regexp: {regexp: /^[^-\s][0-9()+-\s]+[^-\s]$/i, message: 'Please enter valid Fax Number'}, }},
                'form[salesPerson]': {validators: {notEmpty: {message: 'Please select SalesPerson'}, }},
                'form[password]': {
                    validators: {
                        callback: {
                            message: 'Please enter Password',
                            callback: function (value, validator, $field) {
                                var newacc = $('#frmcheckout').find('[name="form[shopnewaccount]"]:checked').val();
                                return (newacc !== '1') ? true : (value !== '');
                            },
                        },
                    }
                },
                'form_dealers': {validators: {notEmpty: {message: 'Please Select Dealer'}, }},
                'form[billing_firstName]': {validators: {notEmpty: {message: 'Please enter Firstname'}, regexp: {regexp: /^[^-\s][a-z0-9\\\/\_\-\s&\']+[^-\s]$/i, message: 'Please enter valid Firstname'}, }},
                'form[billing_lastName]': {validators: {notEmpty: {message: 'Please enter Lastname'}, regexp: {regexp: /^[^-\s][a-z0-9\\\/\_\-\s&\']+[^-\s]$/i, message: 'Please enter valid Lastname'}, }},
                'form[billing_address1]': {validators: {notEmpty: {message: 'Please enter Street Address'}, stringLength: {max: 50, message: 'Street Address can not be more than 50 characters'}, }},
                'form[billing_address2]': {validators: {regexp: {regexp: /^[^-\s][a-z0-9\\\/\_\-\s&\,.']+[^-\s]$/i, message: 'Please enter valid Apartment, Suite, Unit'}, }},
                billing_countryId: {validators: {notEmpty: {message: 'Please select Country'}, }},
                billingstateId: {validators: {notEmpty: {message: 'Please enter State / Country'}, }},
                'form[billing_city]': {validators: {notEmpty: {message: 'Please enter Town / City'}, }},
                'form[billing_zipcode]': {validators: {notEmpty: {message: 'Please enter Postcode / Zip'}, stringLength: {max: 15, message: 'Postcode / Zip can not be more than 15 characters'}, }},
                'form[billing_emailid]': {validators: {notEmpty: {message: 'Please enter Email-Id'}, emailAddress: {message: 'Please enter valid Email-Id'}, }},
                'form[billing_contact]': {validators: {notEmpty: {message: 'Please enter Phone Number'}, stringLength: {min: 10, message: 'Phone Number must be 10 number long'}, regexp: {regexp: /^[^-\s][0-9()+-\s]+(?:,\d+)*$/i, message: 'Please enter valid Phone Number'}, }},
                'form[billing_fax]': {validators: {stringLength: {min: 10, message: 'Fax Number must be 10 number long'}, regexp: {regexp: /^[^-\s][0-9()+-\s]+[^-\s]$/i, message: 'Please enter valid Fax Number'}, }},
                'form[shopterms]': {validators: {notEmpty: {message: 'Please read and accept Terms & Conditions'}, }},
                rangeFrom: {validators: {notEmpty: {message: 'Please select From Date'}, }},
                rangeTo: {validators: {notEmpty: {message: 'Please select To Date'}, }},
                'authcard_type': {validators: {notEmpty: {message: 'Please select Card Type'}, }},
                'authcard_number': {validators: {notEmpty: {message: 'Please enter Card Number'}, creditCard: {message: 'Please enter valid Card Number'}, }},
                'authcvv_code': {validators: {notEmpty: {message: 'Please enter CVV Code'}, cvv: {creditCardField: 'authcard_number', message: 'Please enter valid CVV Code'}}},
                'authexp_month': {validators: {notEmpty: {message: 'Please select Month'}, }},
                'authexp_year': {validators: {notEmpty: {message: 'Please select Year'}, }},
                'cyber_type': {validators: {notEmpty: {message: 'Please select Card Type'}, }},
                'cyber_number': {validators: {notEmpty: {message: 'Please enter Card Number'}, creditCard: {message: 'Please enter valid Card Number'}, }},
                'cybercvv_code': {validators: {notEmpty: {message: 'Please enter CVV Code'}, cvv: {creditCardField: 'authcard_number', message: 'Please enter valid CVV Code'}}},
                'cyberexp_month': {validators: {notEmpty: {message: 'Please select Month'}, }},
                'cyberexp_year': {validators: {notEmpty: {message: 'Please select Year'}, }},
                'stripecard_type': {validators: {notEmpty: {message: 'Please select Card Type'}, }},
                'stripecard_number': {validators: {notEmpty: {message: 'Please enter Card Number'}, creditCard: {message: 'Please enter valid Card Number'}, }},
                'stripecard_cvv_code': {validators: {notEmpty: {message: 'Please enter CVV Code'}, cvv: {creditCardField: 'stripecard_number', message: 'Please enter valid CVV Code'}}},
                'stripecardexp_month': {validators: {notEmpty: {message: 'Please select Month'}, }},
                'stripecardexp_year': {validators: {notEmpty: {message: 'Please select Year'}, }},
                'form[is_shipping_calculated]': {excluded: false, validators: {notEmpty: {message: 'Please select shipping option'}, }},
                'panama_card_holder_name': {validators: {notEmpty: {message: 'Please enter Card Holder Name'}, }},
                'panama_card_number': {validators: {notEmpty: {message: 'Please enter Card Number'}, creditCard: {message: 'Please enter valid Card Number'}, }},
                'panama_cvv': {validators: {notEmpty: {message: 'Please enter CVV Code'}, cvv: {creditCardField: 'panama_card_number', message: 'Please enter valid CVV Code'}}},
                'panama_expiry_month': {validators: {notEmpty: {message: 'Please Enter Month'}, }},
                'panama_expiry_year': {validators: {notEmpty: {message: 'Please Enter Year'}, }},
                'cardinalCard_type': {validators: {notEmpty: {message: 'Please select Card Type'}, }},
                'cardinalCard_number': {validators: {notEmpty: {message: 'Please enter Card Number'}, creditCard: {message: 'Please enter valid Card Number'}, }},
                'cardinalCvv_code': {validators: {notEmpty: {message: 'Please enter CVV Code'}, cvv: {creditCardField: 'authcard_number', message: 'Please enter valid CVV Code'}}},
                'cardinalExp_month': {validators: {notEmpty: {message: 'Please select Month'}, }},
                'cardinalExp_year': {validators: {notEmpty: {message: 'Please select Year'}, }},
            },
            submitHandler: function (validator, form, submitButton) {}
        }).on('change', '[name="form[shopnewaccount"]', function (e) {
            $('#frmcheckout').formValidation('revalidateField', 'form[password]');
        }).on('change', '[name="form[chkbillingaddress"]', function (e) {
            $('#frmcheckout').formValidation('revalidateField', 'form[billing_firstName]');
            $('#frmcheckout').formValidation('revalidateField', 'form[billing_lastName]');
            $('#frmcheckout').formValidation('revalidateField', 'form[billing_address1]');
            $('#frmcheckout').formValidation('revalidateField', 'form[billing_address2]');
            $('#frmcheckout').formValidation('revalidateField', 'billing_countryId');
            $('#frmcheckout').formValidation('revalidateField', 'billingstateId');
            $('#frmcheckout').formValidation('revalidateField', 'form[billing_city]');
            $('#frmcheckout').formValidation('revalidateField', 'form[billing_zipcode]');
            $('#frmcheckout').formValidation('revalidateField', 'form[billing_emailid]');
            $('#frmcheckout').formValidation('revalidateField', 'form[billing_contact]');
            $('#frmcheckout').formValidation('revalidateField', 'form[billing_fax]');
            $('#frmcheckout').formValidation('revalidateField', 'form[is_shipping_calculated]');
        }).on('change', '[name="shippingaddressId"]', function (e) {
            var addrid = "";
            var addresstype = "shipping";
            addrid = $(this).val();
            var site_url1 = getBaseUrl();
            if (addrid > 0 && addrid != "") {
                $.ajax({
                    url: site_url1 + '/customer/getaddress',
                    type: 'post',
                    data: 'addrid=' + addrid + '&addresstype=' + addresstype,
                    beforeSend: function () {
                        $(".loader").removeClass("hide");
                        $(".loader").addClass('show');
                    },
                    success: function (result) {
                        $(".loader").removeClass("show");
                        $(".loader").addClass('hide');
                        if (result) {
                            var res = result.split("||");
                            if (res[1] || res[1] == '') {
                                $("#shipping_companyName").val(res[1]);
                            }
                            if (res[2]) {
                                $("#form_shipping_address1").val(res[2]);
                            }
                            if (res[3] || res[3] == '') {
                                $("#form_shipping_address2").val(res[3]);
                            }
                            if (res[5]) {
                                $("#form_shipping_zipcode").val(res[5]).change();
                            }
                            if (res[6]) {
                                stateAjax = false;
                                $('#shipping_countryId').val(res[6]).change();
                                stateAjax = true;
                            }
                            if (res[7]) {
                                console.log(res[6] + " .. " + res[7]);
                                cityAjax = false;
                                getCheckoutState(res[6], res[7], 'shipping');
                                cityAjax = true;
                                $("#frmcheckout").data('bootstrapValidator').revalidateField('shippingstateId');

                                // $.ajax({
                                //     url: site_url1 + '/getstate',
                                //     type: 'post',
                                //     data: 'ctrid=' + res[6] + '&state_id=' + res[7],
                                //     success: function(result) {
                                //         if (result) {
                                //             $("#form_shipping_stateId").html('');
                                //             $(result).appendTo("#form_shipping_stateId");
                                //             $("#frmcheckout").data('bootstrapValidator').revalidateField('shippingstateId');
                                //         }
                                //     },
                                //     error: function(jqXHR, textStatus, errorThrown) {
                                //         return false;
                                //     }
                                // });
                            }
                            if (res[4]) {

                                getCheckoutCity(res[7], res[4], 'shipping');
                                $("#frmcheckout").data('bootstrapValidator').revalidateField('form[shipping_city]');
                                // $.ajax({
                                //     url: site_url1 + '/getcity',
                                //     type: 'post',
                                //     data: 'stateid=' + res[7] + '&city_id=' + res[4],
                                //     success: function(result) {
                                //         if (result) {
                                //             $(".form_shipping_city").html('');
                                //             $(result).appendTo(".form_shipping_city");
                                //             $("#frmcheckout").data('bootstrapValidator').revalidateField('form[shipping_city]');
                                //         }
                                //     },
                                //     error: function(jqXHR, textStatus, errorThrown) {
                                //         return false;
                                //     }
                                // });
                            }
                            calculateCheckoutTax(res[6], res[7], res[4], res[5]);
                            $("#frmcheckout").data('bootstrapValidator').revalidateField('form[shipping_address1]');
                            $("#frmcheckout").data('bootstrapValidator').revalidateField('form[shipping_address2]');
                            $("#frmcheckout").data('bootstrapValidator').revalidateField('shipping_countryId');
                            $("#frmcheckout").data('bootstrapValidator').revalidateField('shippingstateId');
                            $("#frmcheckout").data('bootstrapValidator').revalidateField('form[shipping_city]');
                            $("#frmcheckout").data('bootstrapValidator').revalidateField('rangeFrom');
                            $("#frmcheckout").data('bootstrapValidator').revalidateField('rangeTo');
                            $("#frmcheckout").data('bootstrapValidator').revalidateField('form[shipping_zipcode]');

                        }
                        setTimeout(function () {
                            //location.reload(true);
                            reloadOrSetTimeForShipping('shipping');

                        }, 200);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        return false;
                    }
                });
            }
        }).on('change', '[name="billingaddressId"]', function (e) {
            var addrid = "";
            var addresstype = "billing";
            addrid = $(this).val();
            var site_url1 = getBaseUrl();
            if (addrid > 0 && addrid != "") {
                $.ajax({
                    url: site_url1 + '/customer/getaddress',
                    type: 'post',
                    data: 'addrid=' + addrid,
                    data: 'addrid=' + addrid + '&addresstype=' + addresstype,
                    success: function (result) {
                        if (result) {
                            var res = result.split("||");
                            if (res[1]) {
                                $("#billing_companyName").val(res[1]);
                            }
                            if (res[2]) {
                                $("#form_billing_address1").val(res[2]);
                            }
                            if (res[3]) {
                                $("#form_billing_address2").val(res[3]);
                            }
                            if (res[5]) {
                                $("#form_billing_zipcode").val(res[5]);
                            }
                            if (res[6]) {
                                // stateAjax = false;
                                $('#billing_countryId').val(res[6]).change();
                                stateAjax = true;
                            }
                            if (res[7]) {
                                cityAjax = false;
                                getCheckoutState(res[6], res[7], 'billing');
                                cityAjax = true;
                                $("#frmcheckout").data('bootstrapValidator').revalidateField('billingstateId');
                                // $.ajax({
                                //     url: site_url1 + '/getstatecheckout',
                                //     type: 'post',
                                //     data: 'ctrid=' + res[6] + '&state_id=' + res[7],
                                //     success: function(result) {
                                //         if (result) {
                                //             $("#form_billing_stateId").html('');
                                //             $(result).appendTo("#form_billing_stateId");
                                //             $("#frmcheckout").data('bootstrapValidator').revalidateField('billingstateId');
                                //         }
                                //     },
                                //     error: function(jqXHR, textStatus, errorThrown) {
                                //         return false;
                                //     }
                                // });
                            }
                            if (res[4]) {
                                getCheckoutCity(res[7], res[4], 'billing');
                                $("#frmcheckout").data('bootstrapValidator').revalidateField('form[billing_city]');
                                // $.ajax({
                                //     url: site_url1 + '/getcitycheckout',
                                //     type: 'post',
                                //     data: 'stateid=' + res[7] + '&city_id=' + res[4],
                                //     success: function(result) {
                                //         if (result) {
                                //             $(".form_billing_city").html('');
                                //             $(result).appendTo(".form_billing_city");
                                //             $("#frmcheckout").data('bootstrapValidator').revalidateField('form[billing_city]');
                                //         }
                                //     },
                                //     error: function(jqXHR, textStatus, errorThrown) {
                                //         return false;
                                //     }
                                // });
                            }
                            calculateCheckoutTax('', '', '', '', res[6], res[7], res[4], res[5]);
                            $("#frmcheckout").data('bootstrapValidator').revalidateField('form[billing_address1]');
                            $("#frmcheckout").data('bootstrapValidator').revalidateField('form[billing_address2]');
                            $("#frmcheckout").data('bootstrapValidator').revalidateField('billing_countryId');
                            $("#frmcheckout").data('bootstrapValidator').revalidateField('billingstateId');
                            $("#frmcheckout").data('bootstrapValidator').revalidateField('form[billing_city]');
                            $("#frmcheckout").data('bootstrapValidator').revalidateField('form[billing_zipcode]');
                            $("#frmcheckout").data('bootstrapValidator').revalidateField('rangeFrom');
                            $("#frmcheckout").data('bootstrapValidator').revalidateField('rangeTo');
                        }
                        setTimeout(function () {
                            //location.reload(true);
                            reloadOrSetTimeForShipping('billing');

                        }, 200);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        return false;
                    }
                });
            }
        }).on('change', '[name="shipping_countryId"]', function (e) {
            var ctryid = "";
            ctryid = $(this).val();
            var site_url1 = getBaseUrl();
            if (ctryid > 0 && ctryid != "") {
                getCheckoutState(ctryid, selectedState = '', 'shipping');
                $("#frmcheckout").data('bootstrapValidator').revalidateField('shippingstateId');
                // $.ajax({
                //     url: site_url1 + '/getstate',
                //     type: 'post',
                //     data: 'ctrid=' + ctryid,
                //     beforeSend: function() {
                //         $(".loader").removeClass("hide");
                //         $(".loader").addClass('show');
                //     },
                //     success: function(result) {
                //         $(".loader").removeClass("show");
                //         $(".loader").addClass('hide');
                //         if (result) {
                //             $("#form_shipping_stateId").html('');
                //             $(result).appendTo("#form_shipping_stateId");
                //             $("#frmcheckout").data('bootstrapValidator').revalidateField('shippingstateId');
                //         }
                //     },
                //     error: function(jqXHR, textStatus, errorThrown) {
                //         return false;
                //     }
                // });
            }
        }).on('change', '[name="shippingstateId"]', function (e) {
            var stateid = "";
            stateid = $(this).val();
            var site_url1 = getBaseUrl();
            if (stateid > 0 && stateid != "") {
                getCheckoutCity(stateid, selectedCity = '', 'shipping');
                $("#frmcheckout").data('bootstrapValidator').revalidateField('form[shipping_city]');
                // $.ajax({
                //     url: site_url1 + '/getcitycheckout',
                //     type: 'post',
                //     data: 'stateid=' + stateid,
                //     beforeSend: function() {
                //         $(".loader").removeClass("hide");
                //         $(".loader").addClass('show');
                //     },
                //     success: function(result) {
                //         $(".loader").removeClass("show");
                //         $(".loader").addClass('hide');
                //         if (result) {
                //             $(".form_shipping_city").html('');
                //             $(result).appendTo(".form_shipping_city");
                //             $("#frmcheckout").data('bootstrapValidator').revalidateField('form[shipping_city]');
                //         }
                //     },
                //     error: function(jqXHR, textStatus, errorThrown) {
                //         return false;
                //     }
                // });
            }
        }).on('change', '[name="billing_countryId"]', function (e) {
            var ctryid = "";
            ctryid = $(this).val();
            var site_url1 = getBaseUrl();
            if (ctryid > 0 && ctryid != "") {
                getCheckoutState(ctryid, selectedState = '', 'billing');
                $("#frmcheckout").data('bootstrapValidator').revalidateField('billingstateId');
                // $.ajax({
                //     url: site_url1 + '/getstatecheckout',
                //     type: 'post',
                //     data: 'ctrid=' + ctryid,
                //     beforeSend: function() {
                //         $(".loader").removeClass("hide");
                //         $(".loader").addClass('show');
                //     },
                //     success: function(result) {
                //         $(".loader").removeClass("show");
                //         $(".loader").addClass('hide');
                //         if (result) {
                //             $("#form_billing_stateId").html('');
                //             $(result).appendTo("#form_billing_stateId");
                //             $("#frmcheckout").data('bootstrapValidator').revalidateField('billingstateId');
                //         }
                //     },
                //     error: function(jqXHR, textStatus, errorThrown) {
                //         return false;
                //     }
                // });
            }
        }).on('change', '[name="billingstateId"]', function (e) {
            var stateid = "";
            stateid = $(this).val();
            var site_url1 = getBaseUrl();
            if (stateid > 0 && stateid != "") {
                getCheckoutCity(stateid, selectedCity = '', 'billing');
                $("#frmcheckout").data('bootstrapValidator').revalidateField('form[billing_city]');
                // $.ajax({
                //     url: site_url1 + '/getcitycheckout',
                //     type: 'post',
                //     data: 'stateid=' + stateid,
                //     beforeSend: function() {
                //         $(".loader").removeClass("hide");
                //         $(".loader").addClass('show');
                //     },
                //     success: function(result) {
                //         $(".loader").removeClass("show");
                //         $(".loader").addClass('hide');
                //         if (result) {
                //             $(".form_billing_city").html('');
                //             $(result).appendTo(".form_billing_city");
                //             $("#frmcheckout").data('bootstrapValidator').revalidateField('form[billing_city]');
                //         }
                //     },
                //     error: function(jqXHR, textStatus, errorThrown) {
                //         return false;
                //     }
                // });
            }
        })
    }
});
$('body').on('success.form.bv', '#frm_customerreg', function (event) {
    event.preventDefault();
    event.stopImmediatePropagation();
    var captcha = $("#g-recaptcha-response").val();
    if (captcha == "") {
        swal("Error!", "Please Complete the Captcha by clicking the Checkbox", "error");
        $("#btnRegSubmit").attr('disabled', false);
    } else {
        document.getElementById("frm_customerreg").submit();
    }
});
$("#form_new_password").on('change', function () {
    $("#frmchangepassword").bootstrapValidator('revalidateField', "form[confirm_password]");
});
$('body').on('success.form.bv', '#frmchangepassword', function (e) {
    e.preventDefault();
    var form = this;
    var formData = $("#frmchangepassword").serializeArray();
    var baseUrl = getBaseUrl();
    $.ajax({
        url: baseUrl + '/../home/updatepassword',
        type: 'post',
        data: formData,
        beforeSend: function () {},
        success: function (response) {
            if (response == 1) {
                $('#form_old_password').val('');
                $('#form_new_password').val('');
                $('#form_confirm_password').val('');
                Flash.success("Password Changed Successfully");
            } else {
                $('#form_old_password').val('');
                $('#form_new_password').val('');
                $('#form_confirm_password').val('');
                Flash.success("Please enter correct Old Password");
            }
        },
    })
});
$("body").delegate(".removeaddress", "click", function (e) {
    var addressid = $(this).attr("data-name");
    var site_url1 = getBaseUrl();
    if (addressid != "") {
        swal({title: 'Are you sure you want to remove this address?', type: 'warning', showCancelButton: true, confirmButtonText: 'Yes, Remove', cancelButtonText: 'No, Cancel'}, function () {
            $.ajax({
                url: site_url1 + '/customer/removefromaddress',
                type: 'post',
                data: 'addressid=' + addressid,
                success: function (result) {
                    if (result >= 0) {
                        window.location.reload(true);
                    } else {
                        swal("Wishlist!", "There is some issue in Address", "info");
                    }
                    return false;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    return false;
                }
            });
        });
    }
    return false;
});
$("body").delegate(".countryId", "change", function (e) {
    var ctryid = "";
    ctryid = $(this).val();
    var site_url1 = getBaseUrl();
    if (ctryid > 0 && ctryid != "") {
        $.ajax({
            url: site_url1 + '/getstate',
            type: 'post',
            data: 'ctrid=' + ctryid,
            beforeSend: function () {
                showLoader();
                // $(".loader").removeClass("hide");
                // $(".loader").addClass('show');
            },
            success: function (result) {
                if (result) {
                    $(".stateId").html(result);
                    $(".city").html('');
                }
            },
            complete: function () {
                hideLoader();
                // $(".loader").removeClass("show");
                // $(".loader").addClass('hide');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                return false;
            }
        });
    }
});
$("body").delegate(".stateId", "change", function (e) {
    var stateid = "";
    stateid = $(this).val();
    var site_url1 = getBaseUrl();
    if (stateid > 0 && stateid != "") {
        $.ajax({
            url: site_url1 + '/getcity',
            type: 'post',
            data: 'stateid=' + stateid,
            beforeSend: function () {
                showLoader();
                // $(".loader").removeClass("hide");
                // $(".loader").addClass('show');
            },
            success: function (result) {
                if (result) {
                    $(".city").html(result);
                }
            },
            complete: function () {
                hideLoader();
                // $(".loader").removeClass("show");
                // $(".loader").addClass('hide');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                return false;
            }
        });
    }
});
$("body").delegate("#locationCountry", "change", function (e) {
    var ctryid = "";
    ctryid = $(this).val();
    var site_url1 = getBaseUrl();
    if (ctryid > 0 && ctryid != "") {
        $.ajax({
            url: site_url1 + '/getstate',
            type: 'post',
            data: 'ctrid=' + ctryid,
            beforeSend: function () {
                $(".loader").removeClass("hide");
                $(".loader").addClass('show');
            },
            success: function (result) {
                if (result) {
                    $("#locationState").html(result);
                    $("#locationCity").html('');
                }
            },
            complete: function () {
                $(".loader").removeClass("show");
                $(".loader").addClass('hide');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                return false;
            }
        });
    }
});
$("body").delegate("#locationState", "change", function (e) {
    var stateid = "";
    stateid = $(this).val();
    var site_url1 = getBaseUrl();
    if (stateid > 0 && stateid != "") {
        $.ajax({
            url: site_url1 + '/getcity',
            type: 'post',
            data: 'stateid=' + stateid,
            beforeSend: function () {
                $(".loader").removeClass("hide");
                $(".loader").addClass('show');
            },
            success: function (result) {
                if (result) {
                    $("#locationCity").html(result);
                }
            },
            complete: function () {
                $(".loader").removeClass("show");
                $(".loader").addClass('hide');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                return false;
            }
        });
    }
});
$('#payment-history').on('hidden.bs.modal', function () {
    $(this).find('form')[0].reset();
    $('#frmcustomer_login').bootstrapValidator('resetForm', true);
});
$('.view-payment-history').on('click', function () {
    var order_id = $(this).data('value');
    $.ajax({
        url: '/viewPaymentHistory',
        type: 'post',
        data: {'order_id': order_id},
        dataType: 'html',
        beforeSend: function () {},
        success: function (result) {
            if (result != "") {
                $('#payment-history-data').html(result);
            }
            return false;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            return false;
        }
    });
});
function reloadOrSetTimeForShipping(type) {
    if (cityAjax == false || stateAjax == false) {
        setTimeout(function () {
            reloadOrSetTimeForShipping(type);
        }, 200);
    } else {
        if (type == 'shipping') {
            $("#form_shipping_zipcode").trigger("change");
        } else {
            $("#form_billing_zipcode").trigger("change");
        }
    }
}
