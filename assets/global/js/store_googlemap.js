//var locationList = ["7710000122","9000035149","9000083243","7710000052","7710000138","7710000109","7710000074","7710000482","7710000110","7710000111","7710000019","7710000031","7710000140","7710000076","7710000141","7710000114","7710000033","7710000142","7710000077","7710000034","7710000035","7710000116","9000038390","7710000081","9000107814","7710000448","7710000484","9000036741","7710000036","7710000037","7710000038","7710000039","9000037221","9000107815","7710000022","7710000041","7710000301","9000032689","7710000149","7710000117","7710000488","7710000087","7710000042","7710000088","7710000303","7710000302","7710000304","7710000119","7710000043","7710000044","7710000537","7710000121","7710000089","7710000169","7710000026","7710000045","9000081607","7710000150","7710000046","7710000047","7710000151","7710000123","7710000152","7710000048","9000037125","7710000094","7710000049","7710000050","7710000051","7710000097","9000109379","7710000305","7710000155","7710000054","7710000536","7710000055","7710000451","7710000057","7710000058","7710000059","7710000098","7710000060","7710000061","7710000099","7710000157","7710000028","7710000107","9000069346","7710000102","9000109141","7710000161","7710000062","7710000063","7710000105","7710000064","7710000065"];
$(window).on('load',function(){
	$('a[href*="javascript:void(0);"]').each(function() {
		$(this).attr("href","javascript:void(0);");
	});
	$(document).off('click', '.current-country').on('click', '.current-country',function(e) {
		console.log('Button click');
		$(".country-selector").toggleClass("dac-opened");
    }); 

	
});
$(window).on('load resize', function () {

	if ($(window).width() < 1025) {//not desktop
		$(".shopping-cart-icon").replaceWith("<img class='icon shopping-cart-icon' src='/resources/images/header/header-cart.png'>");
		$(".store-locator-icon").replaceWith("<img class='icon store-locator-icon' src='/resources/images/header/header-marker.png'>");
		$(".menu-icon").replaceWith("<img class='icon menu-icon' src='/resources/images/header/header-menu.png'>");
	}
	var view = "desktop";
	if ($(window).width() < 768) {
		view = "mobile";
	}
	else if ($(window).width() >= 768 && $(window).width() < 992) {
		view = "tablet";
	}

	var virtualTour = $('#virtual-tour');
	var ads = $('#ads');
	
	if (view == "tablet")
	{
		virtualTour.detach();
		ads.detach();
		$('#virtual-tour-tablet').append(virtualTour);
		$('#ads-tablet').append(ads);
	}
	else
	{
		virtualTour.detach();
		ads.detach();
		$('#location-details .store-content').append(virtualTour);
		$('#location-details .store-content').append(ads);
	}
});	
$(window).on('load resize scroll', function () {

	if ($(window).width() >= 1025) {//not desktop
		if($(window).scrollTop() === 0) {//scroll at top of page
			$(".shopping-cart-icon").replaceWith("<img class='icon shopping-cart-icon' src='/resources/images/header/header-cart-white.png'>");
			$(".profile-icon").replaceWith("<img class='icon  profile-icon' src='/resources/images/header/header-profile-white.png'>");
			$(".store-locator-icon").replaceWith("<img class='icon store-locator-icon' src='/resources/images/header/header-marker-white.png'>");

		}else{//scroll not at top of page
			$(".shopping-cart-icon").replaceWith("<img class='icon shopping-cart-icon' src='/resources/images/header/header-cart.png'>");
			$(".profile-icon").replaceWith("<img class='icon  profile-icon' src='/resources/images/header/header-profile.png'>");
			$(".store-locator-icon").replaceWith("<img class='icon store-locator-icon' src='/resources/images/header/header-marker.png'>");
		}
	}

});
	
$.support.cors = true;	
	
$(document).ready(function() {
	/*sticky header*/
	$(window).on("scroll",function(){
		if($(window).scrollTop() > 0){
			$("body").addClass("js-offset");
			$("body").css("padding-top",$(".header-main").height() + "px");
		}else{
			$("body").removeClass("js-offset");
			$("body").css("padding-top","0");
		}
	});
	//Back button fix
	$('#country').val('21');
	$(".address-rbtn").prop("checked", true);
	
	// function init(val) {
	// 	if (val == "" || val == "Address") {
		
	// 		var country = document.getElementById('country');
	// 		var options = [];
				
	// 		if (country != null) {
	// 			options = {
	// 				componentRestrictions: {country: country.value}
	// 			};
	// 		}
				
	// 		var input = document.getElementById('address');
	// 		(function pacSelectFirst(input){
 //    				// store the original event binding function
 //    				var _addEventListener = (input.addEventListener) ? input.addEventListener : input.attachEvent;

 //    				function addEventListenerWrapper(type, listener) {
 //    				// Simulate a 'down arrow' keypress on hitting 'return' when no pac suggestion is selected,
 //    				// and then trigger the original listener.

 //    				if (type == "keydown") {
 //      					var orig_listener = listener;
 //      					listener = function (event) {
 //        				var suggestion_selected = $(".pac-item-selected").length > 0;
 //        				if (event.which == 13 && !suggestion_selected) {
 //          					var simulated_downarrow = $.Event("keydown", {keyCode:40, which:40})
 //          					orig_listener.apply(input, [simulated_downarrow]);
 //        				}

 //        				orig_listener.apply(input, [event]);
 //      				};
 //    			}

 //    			// add the modified listener
 //    			_addEventListener.apply(input, [type, listener]);
 //  				}

 //  				if (input.addEventListener)
 //    				input.addEventListener = addEventListenerWrapper;
 //  				else if (input.attachEvent)
 //    				input.attachEvent = addEventListenerWrapper;

	// 		})(input);
			
	// 		var autocomplete = new google.maps.places.Autocomplete(input, options);
			
	// 		autocomplete.addListener('place_changed', function() {
	// 			var place = autocomplete.getPlace();
		
	// 			if (place.geometry.location != null) {
	// 				$('#lat').val(place.geometry.location.lat());
	// 				$('#lng').val(place.geometry.location.lng());	
	// 			}
	// 		})
					
	// 		if (country != null) {
	// 			google.maps.event.addDomListener(country,'input',function(){
	// 				autocomplete.setComponentRestrictions({country:this.value});
	// 			});
	// 		}
					
	// 		$('#address').keydown(function (e) {
	// 			if (e.which == 13 && $('.pac-container:visible').length) 
	// 			{
	// 				e.preventDefault();
	// 			}
	// 		});
	// 	}
	// 	else if (val == "Zip Code")
	// 	{
	// 		var country = document.getElementById('country');
	// 		var options = [];
				
	// 		options = {
	// 			types: ['(regions)'],
	// 			componentRestrictions: {country: country.value}
	// 		};
				
	// 		var input = document.getElementById('zip-code');
			
	// 		(function pacSelectFirst(input){
 //    				// store the original event binding function
 //    				var _addEventListener = (input.addEventListener) ? input.addEventListener : input.attachEvent;
	
 //    				function addEventListenerWrapper(type, listener) {
 //    				// Simulate a 'down arrow' keypress on hitting 'return' when no pac suggestion is selected,
 //    				// and then trigger the original listener.

 //    				if (type == "keydown") {
 //      					var orig_listener = listener;
 //      					listener = function (event) {
 //        				var suggestion_selected = $(".pac-item-selected").length > 0;
 //        				if (event.which == 13 && !suggestion_selected) {
 //          					var simulated_downarrow = $.Event("keydown", {keyCode:40, which:40})
 //          					orig_listener.apply(input, [simulated_downarrow]);
 //        				}

 //        				orig_listener.apply(input, [event]);
 //      				};
 //    			}

 //    			// add the modified listener
 //    			_addEventListener.apply(input, [type, listener]);
 //  				}

 //  				if (input.addEventListener)
 //    				input.addEventListener = addEventListenerWrapper;
 //  				else if (input.attachEvent)
 //    				input.attachEvent = addEventListenerWrapper;

	// 		})(input);
			
	// 		var autocomplete = new google.maps.places.Autocomplete(input, options);
			
	// 		if (country != null) {
	// 			autocomplete.setComponentRestrictions({'country': country.value});
	// 		}
			
	// 		autocomplete.addListener('place_changed', function() {
	// 			var place = autocomplete.getPlace();
		
	// 			if (place.geometry.location != null) {
	// 				$('#lat').val(place.geometry.location.lat());
	// 				$('#lng').val(place.geometry.location.lng());	
	// 			}
	// 		})
			
	// 		if (country != null) {
	// 			google.maps.event.addDomListener(country,'input',function(){
	// 				autocomplete.setComponentRestrictions({country:this.value});
	// 			});
	// 		}
			
	// 		$('#zip-code').keydown(function (e) {
	// 			if (e.which == 13) 
	// 			{
	// 				e.preventDefault();
	// 			}
	// 		});
	// 	}
	// }
	
	// google.maps.event.addDomListener(window, 'load', init("Address"));
	//js to ensure custom radio button works
	$("input[type='radio'] + label").on("click",function(){
		$(this).prev("input").click();
	});

	//change display elements on radio change
	$('input[type=radio][name=search-by]').change(function() {
		
		var addressGroup = $(".address-group");
		var stateGroup = $(".state-group");
		var zipGroup = $(".zip-group");
		var addressInput = $("#address");
		var stateInput = $("#state");
		var ZipCodeInput = $("#zip-code");
		var latInput = $("#lat");
		var lngInput = $("#lng");
		
        if (this.value == 'Address') {
            addressGroup.show();
			stateGroup.hide();
			zipGroup.hide();
			addressInput.attr("required", "");
			stateInput.removeAttr("required");
			ZipCodeInput.removeAttr("required");
			latInput.val("");
			lngInput.val("");
			addressInput.val("");
			stateInput.val("");
			ZipCodeInput.val("");
			
			init(this.value);
        }
        else if (this.value == 'State') {
            addressGroup.hide();
			stateGroup.show();
			zipGroup.hide();
			addressInput.removeAttr("required");
			stateInput.attr("required", "");
			ZipCodeInput.removeAttr("required");
			latInput.val("");
			lngInput.val("");
			addressInput.val("");
			stateInput.val("");
			ZipCodeInput.val("");
        }
		else if (this.value == 'Zip Code') {
            addressGroup.hide();
			stateGroup.hide();
			zipGroup.show();
			addressInput.removeAttr("required");
			stateInput.removeAttr("required");
			ZipCodeInput.attr("required","");
			latInput.val("");
			lngInput.val("");
			addressInput.val("");
			stateInput.val("");
			ZipCodeInput.val("");
			
			init(this.value);
        }
    });
	
	$('#country').change(function(){
		/*
		if ($(this).val() == "US")
		{
			$('form .address-text').text("Enter your City or Zip Code");
		}
		else if ($(this).val() == "CA")
		{
			$('form .address-text').text("Enter your City, Province or Postal Code");
		}
		else
		{
			$('form .address-text').text("Enter your location");
		}
		*/
		if ($(this).val() == "US" || $(this).val() == "CA")
		{
			$("#state").find("option[value!='']").remove();
			
			$.ajax({
				url:"/getstatecheckout",
				type: "POST",
				data: {"country": $(this).val().toLowerCase()},
				success:function(data){
					var states = JSON.parse(data);
					
					for (var state in states) {
						var option = $('<option />');
						option.attr('value', states[state]["ShortName"]).text(states[state]["FullName"]);

						$('#state').append(option);
					}
					
					$(".usa-canada .second-column").show();
					$(".usa-canada .third-column").show();
					$(".allstore-CTA").show();
				}
			});
			
			if ($(this).val() == "CA") {
				$(".state-text").text("Province");
				$(".zip-text").text("Postal Code");
				$("#state").attr("placeholder", "Select a Province");
				$("#state").find("option[value='']").text("Select a Province");
				$("#zip-code").attr("placeholder", "Enter a Postal code");
			} else {
				$(".state-text").text("State");
				$(".zip-text").text("Zip Code");
				$("#state").attr("placeholder", "Select a State");
				$("#state").find("option[value='']").text("Select a State");
				$("#zip-code").attr("placeholder", "Enter a Zip code");
			}
		}
		else {
			$(".usa-canada .second-column").hide();
			$(".usa-canada .third-column").hide();
			$(".address-rbtn").prop("checked", true);
			$(".address-rbtn").change();
			$(".allstore-CTA").hide();
		}
		
		init($('input[type=radio][name=search-by]:checked').val());
	});
	
	var bodyClass = $("body").attr('class');
	
	if (bodyClass == "location-details")
	{
		var phone = "";
		var custphone = "";
		var country = "";
		
		if ($(".location-info .phone") != null)
		{
			phone = $(".location-info .phone").text().trim();
		}
		if ($(".location-info .cust-phone") != null) 
		{
			custphone = $(".location-info .cust-phone").text().trim();
		}
		if ($("#location-details").attr("data-country-code") != null)
		{
			country = $("#location-details").attr("data-country-code").toLowerCase();
		}
		
		if (phone != "" && country != "")
		{
			phone = formatPhoneNumber(phone, country);
			
			if (phone) {
				$(".location-info .phone").text(phone);
			}
		}
		
		if (custphone != "" && country != "")
		{
			custphone = formatPhoneNumber(custphone, country);
			
			if (custphone) {
				$(".location-info .cust-phone").text(custphone);
			}
		}
		/*
		var url = "https://dataanywherebeta.azure-api.net/storelocator/v1/api/stores/" + ID;
		
		$.ajax({
			url: url,
			beforeSend: function(xhrObj){
				// Request headers
				xhrObj.setRequestHeader("Ocp-Apim-Subscription-Key","e4481a3f5adc433e8185a246ebc8b16e");
			},
			type: "GET",
			// Request body
			data: "{body}"
		})
		.done(function(data) {
			renderPage(data);
		})
		.fail(function(data) {
			//alert("error!");
			console.log("Store Locator API is not responding.");
		});
		*/
		renderPage();
	}
	else if (bodyClass="locations") {
		storeLocations();
	}

	function renderPage() {
		
		var lat = 0;
		var lng = 0;
		var address = "";
		var name = "";
		var cityState = "";
		
		if ($('#location-details').attr("data-lat") != null)
		{
			lat = parseFloat($('#location-details').attr("data-lat"));
		}
		if ($('#location-details').attr("data-lng") != null)
		{
			lng = parseFloat($('#location-details').attr("data-lng"));
		}
		if ($('#location-details .info .address') != null)
		{
			address = $('#location-details .info .address').text();
		}
		if ($('.store-title h1') != null)
		{
			name = $('.store-title h1').text();
		}
		if ($('#location-details .city-postal-code') != null)
		{
			cityState = $('#location-details .city-postal-code').text();
		}
		
		renderMap(lat, lng, name, address, cityState);
		
		var today = new Date();
		var day = today.getDay();
		day = (day == 0) ? 7 : day;
		
		var str = $('#storeHours .day:nth-child(' + day  + ') .hours').text();
		
		var openHoursText = $(".open-hours").attr("data-status");
		openHoursText = (openHoursText.toLowerCase().indexOf('closed') !== -1) ? openHoursText : getOpenHours(str, day);
		
		$('#storeHours .day:nth-child(' + day  + '),#custHours .day:nth-child(' + day  + ')').addClass("current-day");
		$('.open-hours h3').html(openHoursText);
	}
	
	function renderMap(lat, lng, name, address, cityState) {
		var coordinates = {lat: lat, lng: lng};
		map = new google.maps.Map(document.getElementById('map-canvas'), {
			center: coordinates,
			zoom: 17
		});
		//this code allows us to resize the marker pin
		var pinIcon = new google.maps.MarkerImage(
			"/resources/images/ellipse-listing.svg",
			null, /* size is determined at runtime */
			null, /* origin is 0,0 */
			null, /* anchor is bottom center of the scaled image */
			new google.maps.Size(20, 30)
		); 
		var bounds = new google.maps.LatLngBounds();
		
		if (address != "")
		{
			var marker = new MarkerWithLabel ({	
				map: map,
				icon: pinIcon,
				animation: google.maps.Animation.BOUNCE,
				title: name,
				labelContent: "",
				labelAnchor: new google.maps.Point(3, 30),
				position: coordinates,
				markeraddress: address
			});
			
			var iw = new google.maps.InfoWindow({
				content: name + "<br>" + address + ((cityState != "") ? (", " + cityState) : "")
			});
			google.maps.event.addListener(marker, "click", function (e) { 
				iw.open(map, this); 
				//track marker click
				var screenSize=$(window).width();
				var screenType=(screenSize<=623)?"mobile":"desktop";
					dataLayer.push({
						'event':'trackevent',
						'eventcategory':"pt-store-details",
						'eventaction':screenType+" - map-marker click",
						'eventlabel':marker.markeraddress.toLowerCase(),
						'eventvalue':""
					});
				
			});
		}
		bounds.extend(coordinates);
		
		// Don't zoom in too far on only one marker
		if (bounds.getNorthEast().equals(bounds.getSouthWest())) {
			var extendPoint1 = new google.maps.LatLng(bounds.getNorthEast().lat() + 0.001, bounds.getNorthEast().lng() + 0.001);
			var extendPoint2 = new google.maps.LatLng(bounds.getNorthEast().lat() - 0.001, bounds.getNorthEast().lng() - 0.001);
			bounds.extend(extendPoint1);
			bounds.extend(extendPoint2);
		}
			
		map.fitBounds(bounds);
	}

	$('.hide-text').click(function(){
		if ($(this).parent().hasClass("expanded"))
		{
			$(this).siblings("#map").fadeOut("slow");
			$(this).parent().removeClass("expanded");
			$(this).text("Show Map");
		}
		else
		{
			$(this).siblings("#map").fadeIn("slow");
			$(this).parent().addClass("expanded");
			$(this).text("Hide Map");
		}
	});

	function updateMilesNumber() {
		var milesNumber = $('#miles').val() + " miles";
				
		$('.found .miles-number').text(milesNumber);
	}
			
	function updateStoreCount(count) {
		var storeString = (count == 1) ? "<span class='number'>" + count.toString() + "</span> store" : "<span class='number'>" + count.toString() + "</span> stores"
				
		$('.found .store-count').html(storeString);
	}

	$( "#miles" ).change(function() {
		
		storeLocations();
		updateMilesNumber();
	});
						
	function formatPhoneNumber(number, country)
	{
		var countryCodes = {
							"us": "1",
							"ca": "1",
							"ag": "1268",
							"au": "61",
							"bb": "1246",
							"cl": "56",
							"cr": "506",
							"do": "1809",
							"gt": "502",
							"hn": "504",
							"id": "62",
							"jp": "81",
							"kr": "82",
							"kw": "965",
							"ky": "1345",
							"mx": "52",
							"pa": "507",
							"sa": "966",
							"tr": "90",
							"vi": "1340",
							"ph": "63"
						};

		var codeDigits = (countryCodes[country] != null) ? countryCodes[country].length : 0;
		var numberLength = number.substring(codeDigits + 1, number.length).length;
		var regex = "";
		var result = number;
		var countryCode = (countryCodes[country] != null) ? countryCodes[country] : "";
		formattedCountryCode = (codeDigits == 4) ? countryCode.replace(new RegExp('(\\d{1})(\\d{3})', 'g'), '$1 ($2)') : countryCode;
		
		if (country == "us" || country == "ca")
		{
			regex = new RegExp("([+])(\\d{" + codeDigits + "})(\\d{3})(\\d{3})(\\d{4})", "g");
			result = number.replace(regex, '($3) $4-$5');
		}
		else if (numberLength == 12)
		{
			regex = new RegExp("(\\d{" + codeDigits + "})(\\d{3})(\\d{9})", "g");
			result = number.replace(regex, formattedCountryCode + ' $2-$3');
		}
		else if (numberLength == 11)
		{
			regex = new RegExp("(\\d{" + codeDigits + "})(\\d{2})(\\d{9})", "g");
			result = number.replace(regex, formattedCountryCode + ' $2-$3');
		}
		else if (numberLength == 10) {
			regex = new RegExp("(\\d{" + codeDigits + "})(\\d{3})(\\d{3})(\\d{4})", "g");
			result = number.replace(regex, formattedCountryCode + ' $2-$3-$4');
		}
		else if (numberLength == 9) {
			regex = new RegExp("(\\d{" + codeDigits + "})(\\d{1})(\\d{4})(\\d{4})", "g");
			result = number.replace(regex, formattedCountryCode + ' $2-$3-$4');
		}
		else if (numberLength == 8) {
			regex = new RegExp("(\\d{" + codeDigits + "})(\\d{4})(\\d{4})", "g");
			result = number.replace(regex, formattedCountryCode + ' $2-$3');
		}
		else if (numberLength == 7) {
			regex = new RegExp("(\\d{" + codeDigits + "})(\\d{3})(\\d{4})", "g");
			result = number.replace(regex, formattedCountryCode + ' $2-$3');
		}
		return result;
	}

	function storeLocations() {
		var curLocation = getParamValue("current-location");
		var address = getParamValue("address");
		var lat = getParamValue("lat");
		var lng = getParamValue("lng");
		//var coordinates = {lat: 37.426216, lng: -121.919851};
		var coordinates;
		var country = getParamValue("country");
		
		map = new google.maps.Map(document.getElementById('map-canvas'), {
			center: coordinates,
			zoom: 10
		});
		
		if (lat != null && lng != null)
		{
			coordinates = new google.maps.LatLng(lat, lng);
			map.setCenter(coordinates);
		}
		else if (country != null)
		{
			
			$.ajax({
				url:"https://maps.googleapis.com/maps/api/geocode/json?address="+decodeURIComponent(country)+"&sensor=false",
				type: "POST",
				success:function(res){
					lat = res.results[0].geometry.location.lat;
					lng = res.results[0].geometry.location.lng;					
					coordinates = new google.maps.LatLng(lat, lng);
					map.setCenter(coordinates);
				}
			});
		}
		
		if (curLocation != null && curLocation == "true")
		{
			if (navigator.geolocation) {
				var options = {
					enableHighAccuracy: true,
					timeout: 5000,
					maximumAge: 0
				};

				function success(pos) {
					var crd = pos.coords;
					
					map.setCenter(new google.maps.LatLng(crd.latitude, crd.longitude));
					
					getStores(crd.longitude, crd.latitude, "");
				};

				function error(err) {
					//console.log(err.code + ': ' + err.message);
					showNoLocationMessage();
				};

				navigator.geolocation.getCurrentPosition(success, error, options);
			} else {
				// Browser doesn't support Geolocation
				showNoLocationMessage();
			}
		}
		else
		{
			var lat = getParamValue("lat");
			var lng = getParamValue("lng");
			
			if (lat == "" || lng == "")
			{
				var address = decodeURIComponent(getParamValue("address"));
				var state = decodeURIComponent(getParamValue("state"));
				var zipCode = decodeURIComponent(getParamValue("zip-code"));
				
				if (address != "" || zipCode != "")
				{
					var cntr = (country != null) ? (" " + country) : "";
					
					if (address) {
						cntr = address + cntr;
					} else if (zipCode) {
						cntr = zipCode + cntr;
					}
					
					$.ajax({
						url:"https://maps.googleapis.com/maps/api/geocode/json?address=" + cntr + "&sensor=false",
						type: "POST",
						success:function(res){
							if (res.results.length > 0)
							{
								lat = res.results[0].geometry.location.lat;
								lng = res.results[0].geometry.location.lng;
								getStores(lng, lat, "");
							}
							else {
								showNoLocationMessage();
							}
							//map.setCenter(new google.maps.LatLng(res.results[0].geometry.location.lng, res.results[0].geometry.location.lat));
						}
					});
				}
				else if (state != "")
				{
					getStores("", "", state);
				}
			}
			else if (lat != "" && lng != "")
			{
				getStores(lng, lat, "");	
			}
		}

		function showNoLocationMessage()
		{
			var miles = $('#miles').val();
							
			if (miles != null && miles == "100")
			{
				$('.map-container').hide();
				$('#list .no-location-message .text-other').hide();
				$('#list .no-location-message .text-50-mile').show();
				$('#list').addClass("full-size");
				$('.form-container .form-title').hide();
				$('.form-container .form-title-50-miles').show();
			}
			else {
				$('.map-container').show();
				renderMap(0, 0, "", "", "");
				$('#list .no-location-message .text-50-mile').hide();
				$('#list .no-location-message .text-other').show();
				$('#list').removeClass("full-size");
				$('.form-container .form-title-50-miles').hide();
				$('.form-container .form-title').show();
			}
			
			$('.no-location-message').show();
		}
		
		function getParamValue (name) {
			var url = location.href;
			name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
			var regexS = "[\\?&]"+name+"=([^&#]*)";
			var regex = new RegExp( regexS );
			var results = regex.exec( url );
			return results == null ? null : results[1];
		}
		
		function updateLabels(count) {
			var milesNumber = $('#miles').val() + " miles";
			var storeString = (count == 1) ? count.toString() + " store" : count.toString() + " stores"
			
			$('.found .miles-number').text(milesNumber);
			$('.found .store-count').text(storeString);
		}

		function getStores(longitude, latitude, state) {			
			if (state != "")
			{
				/*
				$(".within-text").text(" in ");
				$(".miles-number").text(state);
				*/
				
				var params = {
					"region": state
				};
				
				//var url = "https://dataanywherebeta.azure-api.net/storelocator/v1/api/stores?" + $.param(params);
				//var url = "https://dataanywhereprod.azure-api.net/V1/api/stores?" + $.param(params);
				var url = "/get-locations-by-region?" + $.param(params);

				$.ajax({
					url: url,
					type: "POST"
				})
				.done(function(res) {
					var data = JSON.parse(res);
					var guids = [];
					
					for (var i = 0, place; place = data.StoreLocations[i]; i++) {
						if (place.LocationId != null)
						{
							guids.push(place.LocationId);
						}
					}
					
					$.ajax({
						url:"/address-by-guid",
						type: "POST",
						data: {"guids": JSON.stringify(guids)},
						success:function(res){
							var addresses = JSON.parse(res);
							createMarkers(data, addresses);
						},
						complete: function (){
							$('.found').show();
						}
						
					});
					
					updateStoreCount(data.StoreLocations.length);
					
					$(".no-location-message").hide();
					
					if (data.StoreLocations.length == 0)
					{
						$(".no-location-message").show();
						showNoLocationMessage();
					}
				})
				.fail(function(data) {
					console.log("Store Locator API is not responding.");
				});
			}
			else
			if (latitude != null && longitude != null && state == "")
			{
				/*
				$(".within-text").text(" within ");
				updateMilesNumber();
				*/
				
				var result = [];
				
				var miles = $('#miles').val();
				
				var params = {
					// Request parameters
					//"longitude": "-121.919851",
					//"latitude": "37.426216",
					"longitude": longitude,
					"latitude": latitude,
					"distance": miles,
					"units": "miles",
					"amenities": "",
					"paymentMethods": ""
				};
				
				//var url = "https://dataanywherebeta.azure-api.net/storelocator/v1/api/stores?" + $.param(params);
				//var url = "https://dataanywhereprod.azure-api.net/V1/api/stores?" + $.param(params);
				var url = "/anydata-api?" + $.param(params);

				$.ajax({
					url: url,
					/*headers: {
					},*//*
					beforeSend: function(xhrObj){
						// Request headers
					},*/
					type: "POST"
				})
				.done(function(res) {
					var data = JSON.parse(res);
					var guids = [];
					
					for (var i = 0, place; place = data.StoreLocations[i]; i++) {
						if (place.LocationId != null)
						{
							guids.push(place.LocationId);
						}
					}
					
					$.ajax({
						url:"/address-by-guid",
						type: "POST",
						data: {"guids": JSON.stringify(guids)},
						success:function(res){
							var addresses = JSON.parse(res);
							createMarkers(data, addresses);
						},
						complete: function (){
							$('.found').show()
						}
					});
					
					updateStoreCount(data.StoreLocations.length);
					
					$(".no-location-message").hide();
					
					if (data.StoreLocations.length == 0)
					{
						$(".no-location-message").show();
						showNoLocationMessage();
					}
					$('.found').show()
				})
				.fail(function(data) {
					console.log("Store Locator API is not responding.");
				});
			}
		}
		
		function toUrlFormat(val) {
			var result;
			result = val.replace(/[^a-zA-Z0-9\-\s]+/g,"").replace(/ /g,"-").toLowerCase();
			result = result.replace(/--+/g, '-');
			
			return result;
		}
		
		function createMarkers(places, addresses) {
			var stateParam = decodeURIComponent(getParamValue("state"));
			var bounds = new google.maps.LatLngBounds();
			var el = $('#list');
			/*var guids = [];
			var addresses;*/
			$('.row', el).remove();
			/*	
			for (var i = 0, place; place = places.StoreLocations[i]; i++) {
				if (place.LocationId != null)
				{
					guids.push(place.LocationId);
				}
			}
				
			$.ajax({
					url:"/address-by-guid",
					type: "POST",
					data: {"guids": JSON.stringify(guids)},
					success:function(res){
						addresses = JSON.parse(res);
					}
			});
			*/
			for (var i = 0, place; place = places.StoreLocations[i]; i++) {
				var phoneNumber = "";
				var businessNumber = "";
				var coords = new google.maps.LatLng(place.Location.coordinates[1], place.Location.coordinates[0]);
				var digits = (i+1).toString().length;
				var markerClass = (digits == 1) ? "marker-label-digit" : "marker-label-digits";
				
				//this code allows us to resize the marker pin
				var pinIcon = new google.maps.MarkerImage(
					"/resources/images/ellipse-listing.svg",
					null, /* size is determined at runtime */
					null, /* origin is 0,0 */
					null, /* anchor is bottom center of the scaled image */
					new google.maps.Size(25, 30)
				); 

				//var marker = new google.maps.Marker({
				var marker = new MarkerWithLabel ({	
					map: map,
					icon: pinIcon,
					animation: google.maps.Animation.DROP,
					labelContent: "" + (i+1),
					labelClass: markerClass,
					labelAnchor: new google.maps.Point(0, 32),
					title: place.Name,
					position: coords,
					address: place.Address
				});

				if (place.ExtraData["Phone"] != null) {
					if (place.ExtraData.Address["CountryCode"] != null) {
						phoneNumber = formatPhoneNumber(place.ExtraData["Phone"], place.ExtraData.Address["CountryCode"].toLowerCase());
					}
					else
					{
						phoneNumber = place.ExtraData["Phone"];
					}
				}

				if(place.ExtraData["Customer Service Phone"] != null && place.ExtraData["Customer Service Phone"].length > 0){
					$.each(place.ExtraData["Customer Service Phone"], function(i,cser){
						if(cser["Type"] == 0){

							if (place.ExtraData.Address["CountryCode"] != null) {
								businessNumber = formatPhoneNumber(cser["Number"], place.ExtraData.Address["CountryCode"].toLowerCase());
							}
							else
							{
								businessNumber = cser["Number"];
							}
							
						}
					});

				}
				
				var distance = (Math.round(place.Distance) == 0) ? "1 mile" : Math.round(place.Distance) + " miles";
				var city = (place.ExtraData.Address["Locality"] != null) ? place.ExtraData.Address["Locality"] : "";
				var state = (place.ExtraData.Address["Region"]) ? place.ExtraData.Address["Region"] : "";
				var postalCode = (place.ExtraData.Address["PostalCode"]) ? place.ExtraData.Address["PostalCode"] : "";
				var region = (place.ExtraData.Address["Region"] != null) ? place.ExtraData.Address["Region"] : "";
				var country = (place.ExtraData.Address["CountryCode"] != null) ? place.ExtraData.Address["CountryCode"] : "";
				var locationNumber = (place.LocationNumber != null) ? place.LocationNumber : "";
				var locationId = (place.LocationId != null) ? place.LocationId : "";
				var scheduleAppointment = (place.ExtraData.ScheduleApptURL != null) ? place.ExtraData.ScheduleApptURL : "";
				var storeStatus = (place.ExtraData.StoreStatus != null) ? place.ExtraData.StoreStatus : "";
				region = (city != region) ? region : "";
				
				var city_state_postal_code = city;
				if (city_state_postal_code != "" && (state != "" || postalCode != ""))
				{
					city_state_postal_code += ", "
				}
				if (state != "")
				{
					city_state_postal_code += state + " ";
				}
				city_state_postal_code += postalCode;
				
				var state_country_postal_code = ((state != "") ? (state + ", " + country + " " + postalCode) : (country + " " + postalCode));
				
				var ptAddressURL = (addresses != null && addresses[locationId] != null && addresses[locationId]["addressURL"] != null) ? addresses[locationId]["addressURL"] : "";
				var ptCityURL = (addresses != null && addresses[locationId] != null && addresses[locationId]["cityURL"] != null) ? addresses[locationId]["cityURL"] : "";
				var ptStateURL = (addresses != null && addresses[locationId] != null && addresses[locationId]["stateURL"] != null) ? addresses[locationId]["stateURL"] : "";
				var ptCountryURL = (addresses != null && addresses[locationId] != null && addresses[locationId]["countryURL"] != null) ? addresses[locationId]["countryURL"] : "";

				var url = ptCountryURL + 
						((ptStateURL != "" && ptStateURL != ptCityURL) ? ("/" + ptStateURL) : "") + 
						"/" + ptCityURL +
						"/" + ptAddressURL;

				if(locationId == "70e66a21-1569-4295-b435-357c90c11aee"){
					url = url+"-suite-90";
				}
				if(locationId == "5b7d3d33-8bdf-4636-8234-904b27682aab"){
					url = url+"-suite-110";
				}
				var today = new  Date();
				var day = today.getDay();
				day = (day == 7) ? 0 : day;
				var str = "";
				var hoursArray = [];
				
				if (place.ExtraData["Hours of operations"] != null)
				{
					hoursArray = place.ExtraData["Hours of operations"].split("|");
                }

                if (hoursArray[day] != null && hoursArray.length == 7){
					str = hoursArray[day];
                }
                
                var specialDay = "";
                var today = new  Date();

                var specialHourJson = place.ExtraData["HoursOfOpStruct"];
                if(typeof specialHourJson != 'undefined'){
                    specialHourJson = specialHourJson["SpecialHours"];
                    if(specialHourJson != null){
                        for(var shj in specialHourJson){
                            specialDay = new Date(specialHourJson[shj]["Date"]);
                            if(specialDay.setHours(0,0,0,0) == today.setHours(0,0,0,0)) {
                                if(specialHourJson[shj]["Ranges"] != null){
                                    str = specialHourJson[shj]["Ranges"][0]["StartTime"] + "-" + specialHourJson[shj]["Ranges"][0]["EndTime"];
                                }else{
                                    str = "Closed";
                                }
                            }
                        }
                    }
                }
		
				var openHoursText = (storeStatus.toLowerCase().indexOf('closed') !== -1) ? storeStatus : getOpenHours(str, day);
				
				var directions = ((city != state) ? (encodeURIComponent(place.Address) + " " + encodeURIComponent(city) + " " + encodeURIComponent(state)) : 
													(encodeURIComponent(place.Address) + " " + encodeURIComponent(state) + " " + encodeURIComponent(country)));
				var infoContent = "<div class='info-content'><p class='name'>" + place.Name + "</p>" +
								"<p class='address'>" + place.Address + "</p>" +
								"<p class='city-postal-code'>" + ((city != state) ? city_state_postal_code : state_country_postal_code) + "</p>" +
								(phoneNumber != "" ? "<a href='tel:" + phoneNumber + "'class='phone'><span class='phone-text'>Store Phone: </span><span class='phone-number'>" + phoneNumber + "</span></a>" : "") +
								(businessNumber != "" ? "<a href='tel:" + businessNumber + "'class='phone'><span class='phone-text'>Customer Service: </span><span class='phone-number'>" + businessNumber + "</span></a>" : "") +
								"<a class='details-button btn btn-default' href='/store/" + url + "' target='_self'>Store Details</a></div>";
				var infowindow = new google.maps.InfoWindow();
				
				google.maps.event.addListener(marker,'click', (function(marker,infoContent,infowindow){
					return function() {
						infowindow.setContent(infoContent);
						infowindow.open(map,marker);
						//track marker click results page
						var screenSize=$(window).width();
						var screenType=(screenSize<=623)?"mobile":"desktop";
							dataLayer.push({
								'event':'trackevent',
								'eventcategory':"pt-store-list",
								'eventaction':screenType+" - map-marker click",
								'eventlabel':marker.address.toLowerCase(),
								'eventvalue':""
							});

						
					};
				})(marker,infoContent,infowindow));
				
				
					//placesList.innerHTML += '<div class="row"><h3>' + place.name + '<h3></div>';
				text = "<div class='row'><div class='counter'>" + (i+1) + "</div><div class='text'><a class='details-link' href='/store/" + url + "' target='_self'><h3>" + place.Name + "</h3></a>" + 
				"<span class='distance'>" + ((stateParam == "null" || stateParam == "") ? distance : "") + "</span>" +
				"<span class='storeid'>" + locationId + "</span>" + 
				"<p class='address'>" + place.Address + "</p>" +
				"<p class='city-postal-code'>" + ((city != state) ? city_state_postal_code : state_country_postal_code) +  "</p>" +
				"<p class='open-hours " + ((storeStatus.toLowerCase().indexOf('closed') !== -1) ? "closed" : "") + "'>" + openHoursText +  "</p>" +
				"<a class='directions' href='https://www.google.com/maps/dir//" + directions + "' target='_blank'>" +
				"Get Directions to this Store <i class='fa fa-chevron-right' aria-hidden='true'></i>" +
				"</a>" +
				(scheduleAppointment != "" ? 
				"<div><a id='scheduleAppointment' class='schedule-appointment' data-location='" + locationNumber + "'  href='" + scheduleAppointment + "' target='_blank' onclick='return tt.launchWorkflow(this); ' data-lightbox-width='1000' data-lightbox-height='1000'"+
				   "data-lightbox-loading-message='Loading... '>Schedule an appointment</a></div>" : "") +
				(phoneNumber != "" ? "<a href='tel:" + phoneNumber + "'class='phone' data-tel='" + place.ExtraData["Phone"] + "' data-country='" + country + "'><span class='phone-text'>Store Phone: </span><span class='phone-number'>" + phoneNumber + "</span></a>" : "") +
				(businessNumber != "" ? "</br><a href='tel:" + businessNumber + "'class='phone extra-phone' data-tel='" + businessNumber + "' data-country='" + country + "'><span class='phone-text'>Customer Service: </span><span class='phone-number'>" + businessNumber + "</span></a>" : "") +
				//"<p class='phone'>Store Phone: " + phoneNumber + "</p>" +
				"<a class='details-button btn btn-default' href='/store/" + url + "' target='_self'>Store Details</a>" +
				"</div><hr /></div>";



				$(el).append(text);

				bounds.extend(coords);
			}
			
			
			// Don't zoom in too far on only one marker
			if (bounds.getNorthEast().equals(bounds.getSouthWest())) {
				var extendPoint1 = new google.maps.LatLng(bounds.getNorthEast().lat() + 0.001, bounds.getNorthEast().lng() + 0.001);
				var extendPoint2 = new google.maps.LatLng(bounds.getNorthEast().lat() - 0.001, bounds.getNorthEast().lng() - 0.001);
				bounds.extend(extendPoint1);
				bounds.extend(extendPoint2);
			}
			
			map.fitBounds(bounds);
		}
	}
})
/*on state layer, sort cities */
if($("body").hasClass("store-ca")){
	var sortedCities = [];
	$(".city-details").each(function ( index ){
		sortedCities.push($(this).data("sort"));
	});
	sortedCities.sort();

	for(var sc in sortedCities){
		$(".state-cities").append($('.city-details[data-sort="'+sortedCities[sc]+'"]'));
		
	}
}
/*temporary for Mundy Location*/
var selector = ".address:contains('251 Mundy Street')";
var waitForEl = function(selector, callback) {
	if (jQuery(selector).length) {
	  callback();
	} else {
	  setTimeout(function() {
		waitForEl(selector, callback);
	  }, 100);
	}
  };
  waitForEl(selector, function() {
    $(selector).siblings(".open-hours").remove();
	$(selector).siblings(".phone").remove();
	$(selector).siblings(".directions").remove();
	if($("body").hasClass("locaions")){
		$(selector).siblings(".details-button").remove();
	}else if($("body").hasClass("location-details")){
		$(".open-hours").remove();
		$(".info-space-top").remove();
		$("#storeHours").remove();
		$("#custHours").remove();
  	}
    $(selector).siblings(".city-postal-code").after("<div class='error-warning'>Store is temporarily closed </br>due to storm damage</div><div class='error-warning-2'>Please visit us at our</br><a href='/store/us/pennsylvania/hazleton-township/454-airport-rd'>Hazelton Location</a></div>");
  });


  
  
  
	$(document).on("click",".opendiv",function(){
			
			$(this).toggleClass("closediv");
			$(this).removeClass("opendiv");
			
			$(this).next(".state-cities").css({ "display": "none" });
			$(this).find(".ashley-uparrow").toggleClass("ashley-arrow");
		
			$(this).find(".ashley-arrow").removeClass("ashley-uparrow");
		
			
			
			
	});
	
	$(document).on("click",".closediv",function(){
			$(this).toggleClass("opendiv");
			$(this).removeClass("closediv");
			var number=$(".closediv .number").text();
			
			$(this).next(".state-cities").css({ "display": "table" });
		
			
					
			
			$(this).find(".ashley-arrow").toggleClass("ashley-uparrow");
		
			$(this).find(".ashley-uparrow").removeClass("ashley-arrow");
			/*$(this).siblings(".state-cities").css({ "display": "block!important" });*/
			
			/*$('.closeda').toggleClass("openda");
			$('.openda').removeClass("closeda");
			/*$('.ashley-arrow').toggleClass("ashley-uparrow");
			$('.ashley-uparrow').removeClass("ashley-arrow");
			$('.closediv').toggleClass("opendiv");
			$('.opendiv').removeClass("closediv");*/
	});
	
if ($("body").attr('class') == "store-ca") {
	$('.city-details .phone>a').each(function () {
		var phonenumber = $(this).text().replace("+1", "").trim();

		phonenumber = formatPhone(phonenumber);
		function formatPhone(number) {
			var regex = "";
			var result = number;
			regex = new RegExp("(\\d{3})(\\d{3})(\\d{4})", "g");
			result = number.replace(regex, "$1-$2-$3");
			return result;
		}
		if (phonenumber) {
			$(this).text(phonenumber);
		}
	});
	$('.city-details .cust-phone>a').each(function () {
		var phonenumber = $(this).text().replace("+1", "").trim();

		phonenumber = formatPhone(phonenumber);
		function formatPhone(number) {
			var regex = "";
			var result = number;
			regex = new RegExp("(\\d{3})(\\d{3})(\\d{4})", "g");
			result = number.replace(regex, "$1-$2-$3");
			return result;
		}

		if (phonenumber) {
			$(this).text(phonenumber);
		}
	});
}

function getOpenHours(str, day)
{
    var closedText = "";
    
    if (str != "")
    {
        var closingHour = str.substring(str.toLowerCase().lastIndexOf("-")+1, str.length);
        var openingHour = str.substring(0, str.toLowerCase().indexOf("-"));
        var currentTime = getCurrentTime();
        var closingHour2 = (closingHour.charAt(0) == "0") ? closingHour.substring(1,closingHour.length) : closingHour;
        var openingHour2 = (openingHour.charAt(0) == "0") ? openingHour.substring(1,openingHour.length) : openingHour;
        
        if (currentTime > convertTo24Hour(closingHour.toLowerCase()) || str == "Closed") 
        {
            closedText = "Closed today";
        }
        else if (currentTime < convertTo24Hour(openingHour.toLowerCase())) 
        {
            closedText = "Opens today at " + openingHour2;
        }
        else if (currentTime < convertTo24Hour(closingHour.toLowerCase()) && currentTime >= convertTo24Hour(openingHour.toLowerCase()))
        {
            closedText = "Open today until " + closingHour2;
        }
        else if (str == "24Hrs")
        {
            closedText = "Open today 24 Hrs";
        }
    }
    return closedText;
}
function getCurrentTime()
{
    var dt = new Date();
    return Date.parse('01/01/2011 ' + dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds());
}
function convertTo24Hour(time) {
    var hours = parseInt(time.substr(0, 2));
    if(time.indexOf('am') != -1 && hours == 12) {
        time = time.replace('12', '0');
    }
    if(time.indexOf('pm')  != -1 && hours < 12) {
        time = time.replace(hours, (hours + 12));
    }
    return Date.parse('01/01/2011 ' + time.replace(/(am|pm)/, ''));
}