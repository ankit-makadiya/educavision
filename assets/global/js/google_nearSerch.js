
$("body").delegate(".country", "change", function(e) { 
	var ctryid = "";
    ctryid = $(this).val(); var site_url1 = getBaseUrl(); 
    if (ctryid > 0 && ctryid != "") 
    	{ 
    		$.ajax({ 
    			url: site_url1 + '/getstatecheckout11', 
    			type: 'post', 
    			data: 'ctrid=' + ctryid, 
    			beforeSend: function() { 
    				$(".loader").removeClass("hide");
                    $(".loader").addClass('show'); 
            	}, 
            	success: function(result) { 
            		if (result) { 
            			$(".stateId").html(result);
                    } 
                    }, 
                complete: function() { 
                	$(".loader").removeClass("show");
                    $(".loader").addClass('hide'); 
                }, 
                error: function(jqXHR, textStatus, errorThrown) { 
                	return false; 
                } 
            }); 
    	} 
    });

$( document ).ready(function() {

if (bodyClass="locations") {
		storeLocations();
	}
	$(window).on("scroll",function(){
		if($(window).scrollTop() > 0){
			$("body").addClass("js-offset");
			$("body").css("padding-top",$(".header-main").height() + "px");
		}else{
			$("body").removeClass("js-offset");
			$("body").css("padding-top","0");
		}
	});
	//Back button fix
	 var con = $('#country').val("21");
	$(".address-rbtn").prop("checked", true);
	 $(".state-group").hide();
	function init(val) {
		if (val == "" || val == "Address") {

			var country = document.getElementById('country');
			// var countData = country.getAttribute('data_countryCode');
			// alert(country.getAttribute('data_countryCode'));
			var options = [];
				
			if (country != null) {
				options = {
					componentRestrictions: {country: country.value}
				};
			}
			var input = document.getElementById('address');
			(function pacSelectFirst(input){
    				// store the original event binding function
    				var _addEventListener = (input.addEventListener) ? input.addEventListener : input.attachEvent;
    				function addEventListenerWrapper(type, listener) {
    				// Simulate a 'down arrow' keypress on hitting 'return' when no pac suggestion is selected,
    				// and then trigger the original listener.
    				if (type == "keydown") {
      					var orig_listener = listener;
      					listener = function (event) {
        				var suggestion_selected = $(".pac-item-selected").length > 0;
        				if (event.which == 13 && !suggestion_selected) {
          					var simulated_downarrow = $.Event("keydown", {keyCode:40, which:40})
          					orig_listener.apply(input, [simulated_downarrow]);
        				}
        				orig_listener.apply(input, [event]);
      				};
    			}

    			// add the modified listener
    			_addEventListener.apply(input, [type, listener]);
  				}
  				if (input.addEventListener)
    				input.addEventListener = addEventListenerWrapper;
  				else if (input.attachEvent)
    				input.attachEvent = addEventListenerWrapper;
			})(input);
			var autocomplete = new google.maps.places.Autocomplete(input, options);
			
			autocomplete.addListener('place_changed', function() {
				var place = autocomplete.getPlace();
		
				if (place.geometry.location != null) {
					$('#lat').val(place.geometry.location.lat());
					$('#lng').val(place.geometry.location.lng());	
				}
			})
			if (country != null) {
				google.maps.event.addDomListener(country,'input',function(){
					autocomplete.setComponentRestrictions({country:country.value});
				});
			}	
			$('#address').keydown(function (e) {
				if (e.which == 13 && $('.pac-container:visible').length) 
				{
					e.preventDefault();
				}
			});
		}
	}
	
	google.maps.event.addDomListener(window, 'load', init("Address"));
	//js to ensure custom radio button works
	$("input[type='radio'] + label").on("click",function(){

		$(this).prev("input").click();
	});

	//change display elements on radio change
	$('input[type=radio][name=search-by]').change(function() {
		
		var addressGroup = $(".address-group");
		var stateGroup = $(".state-group");
		var addressInput = $("#address");
		var stateInput = $("#state");
		var latInput = $("#lat");
		var lngInput = $("#lng");
		
        if (this.value == 'Address') {
            addressGroup.show();
			stateGroup.hide();
			addressInput.attr("required", "");
			stateInput.removeAttr("required");
			latInput.val("");
			lngInput.val("");
			addressInput.val("");
			stateInput.val("");
			init(this.value);
        }
        else if (this.value == 'State') {
            addressGroup.hide();
			stateGroup.show();
			addressInput.removeAttr("required");
			stateInput.attr("required", "");
			latInput.val("");
			lngInput.val("");
			addressInput.val("");
			stateInput.val("");
        }
    });
	$('#country').change(function(){
		
		if ($(this).attr() == "US")
		{
			$(".state-text").text("State");
			$("#state").attr("placeholder", "Select a State");
			$("#state").find("option[value='']").text("Select a State");
		}
		else 
		{
			$(".usa-canada .second-column").hide();
			$(".usa-canada .third-column").hide();
			$(".address-rbtn").prop("checked", true);
			$(".address-rbtn").change();
			$(".allstore-CTA").hide();
		}

		init($('input[type=radio][name=search-by]:checked').val());
	});
	
function storeLocations() {
		var curLocation = getParamValue("current-location");
		var address = getParamValue("address");
		var lat = getParamValue("lat");
		var lng = getParamValue("lng");
		//var coordinates = {lat: 37.426216, lng: -121.919851};
		var coordinates;
		var country = getParamValue("country");
		map = new google.maps.Map(document.getElementById('map-canvas'), {
			center: coordinates,
			zoom: 3,
		});
		
		if (lat != null && lng != null)
		{
			coordinates = new google.maps.LatLng(lat, lng);
			map.setCenter(coordinates);
		}
		else if (country != null)
		{
			$.ajax({
				url:"https://maps.googleapis.com/maps/api/geocode/json?address="+decodeURIComponent(country)+"&sensor=false",
				type: "POST",
				success:function(res){
					lat = res.results[0].geometry.location.lat;
					lng = res.results[0].geometry.location.lng;
					
					coordinates = new google.maps.LatLng(lat, lng);
					map.setCenter(coordinates);
				}
			});
		}
		
		if (curLocation != null && curLocation == "true")
		{
			if (navigator.geolocation) {
				var options = {
					enableHighAccuracy: true,
					timeout: 5000,
					maximumAge: 0
				};

				function success(pos) {
					var crd = pos.coords;
					
					map.setCenter(new google.maps.LatLng(crd.latitude, crd.longitude));
					
					getStores(crd.longitude, crd.latitude, "");
				};

				function error(err) {
					//console.log(err.code + ': ' + err.message);
					showNoLocationMessage();
				};

				navigator.geolocation.getCurrentPosition(success, error, options);
			} else {
				// Browser doesn't support Geolocation
				showNoLocationMessage();
			}
		}
		else
		{
			var lat = getParamValue("lat");
			var lng = getParamValue("lng");
			
			if (lat == "" || lng == "")
			{
				var address = decodeURIComponent(getParamValue("address"));
				var state = decodeURIComponent(getParamValue('state'));
				if (address != "")
				{
					var cntr = (country != null) ? (" " + country) : "";
					
					if (address) {
						cntr = address + cntr;
					} 
					$.ajax({
						url:"https://maps.googleapis.com/maps/api/geocode/json?address=" + cntr + "&sensor=false",
						type: "POST",
						success:function(res){
							if (res.results.length > 0)
							{
								lat = res.results[0].geometry.location.lat;
								lng = res.results[0].geometry.location.lng;
								getStores(lng, lat, "");
							}
							else {
								showNoLocationMessage();
							}
							//map.setCenter(new google.maps.LatLng(res.results[0].geometry.location.lng, res.results[0].geometry.location.lat));
						}
					});
				}
				else if (state != "")
				{
					getStores("", "", state);
				}
			}
			else if (lat != "" && lng != "")
			{
				getStores(lng, lat, "");	
			}
		}

		function showNoLocationMessage()
		{
			var miles = $('#miles').val();
							
			if (miles != null && miles == "100")
			{
				$('.map-container').hide();
				$('#list .no-location-message .text-other').hide();
				$('#list .no-location-message .text-50-mile').show();
				$('#list').addClass("full-size");
				$('.form-container .form-title').hide();
				$('.form-container .form-title-50-miles').show();
			}
			else {
				$('.map-container').show();
				renderMap(0, 0, "", "", "");
				$('#list .no-location-message .text-50-mile').hide();
				$('#list .no-location-message .text-other').show();
				$('#list').removeClass("full-size");
				$('.form-container .form-title-50-miles').hide();
				$('.form-container .form-title').show();
			}
			
			$('.no-location-message').show();
		}
		
		function getParamValue (name) {
			var url = location.href;
			name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
			var regexS = "[\\?&]"+name+"=([^&#]*)";
			var regex = new RegExp( regexS );
			var results = regex.exec( url );
			return results == null ? null : results[1];
		}
		
		function updateLabels(count) {
			var milesNumber = $('#miles').val() + " miles";
			var storeString = (count == 1) ? count.toString() + " store" : count.toString() + " stores"
			
			$('.found .miles-number').text(milesNumber);
			$('.found .store-count').text(storeString);
		}

		function getStores(longitude, latitude, state) {			
			if (state != "")
			{ 
				/*
				$(".within-text").text(" in ");
				$(".miles-number").text(state);
				*/
				
				var params = {
					"region": state
				};
				
				//var url = "https://dataanywherebeta.azure-api.net/storelocator/v1/api/stores?" + $.param(params);
				//var url = "https://dataanywhereprod.azure-api.net/V1/api/stores?" + $.param(params);
				var url = "/get-locations-by-region?" + $.param(params);

				$.ajax({
					url: url,
					type: "POST"
				})
				.done(function(res) {
					var data = JSON.parse(res);
					var guids = [];
					
					// for (var i = 0, place; place = data.StoreLocations[i]; i++) {
					// 	if (place.LocationId != null)
					// 	{
					// 		guids.push(place.LocationId);
					// 	}
					// }
					for (var i = 0, place; place = data.results[i]; i++) {
						if (place.place_id != null)
						{
							guids.push(place.place_id);
						}
					}
					$.ajax({
						url:"/address-by-guid",
						type: "POST",
						data: {"guids":guids},
						success:function(res){
							var addresses = JSON.parse(res);
							createMarkers(data, addresses);
						},
						complete: function (){
							$('.found').show();
						}
					});
					updateStoreCount(data.results.length);
					$(".no-location-message").hide();
					
					if (data.results.length == 0)
					{
						$(".no-location-message").show();
						showNoLocationMessage();
					}
				})
				.fail(function(data) {
					console.log("Store Locator API is not responding.");
				});
			}
			else
			if (latitude != null && longitude != null && state == "")
			{
				/*
				$(".within-text").text(" within ");
				updateMilesNumber();
				*/
				var result = [];
				var miles = $('#miles').val();
				var params = {
					// Request parameters
					// "longitude": "-87.491508",
					// "latitude": "37.989632",
					"longitude": longitude,
					"latitude": latitude,
					"distance": miles,
					"units": "miles",
					"amenities": ""
				};
				//var url = "https://dataanywherebeta.azure-api.net/storelocator/v1/api/stores?" + $.param(params);
				//var url = "https://dataanywhereprod.azure-api.net/V1/api/stores?" + $.param(params);
				var url = "/anydata-api?" + $.param(params);
				$.ajax({
					url: url,
					type: "POST"
				})
				.done(function(res) {
					var data = JSON.parse(res);
					var guids = [];
					for (var i = 0, place; place = data.results[i]; i++) {
						if (place.place_id != null)
						{
							guids.push(place.place_id);
						}
					}
					$.ajax({
						url:"/address-by-guid",
						type: "POST",
						data: {"guids": guids},
						success:function(res){
							console.log(res);
							var addresses = JSON.parse(res);
							createMarkers( data,addresses);
						},
						complete: function (){
							$('.found').show()
						}
					});
					updateStoreCount(data.results.length);
					$(".no-location-message").hide();
					
					if (data.results.length == 0)
					{
						$(".no-location-message").show();
						showNoLocationMessage();
					}
					$('.found').show()
				})
				.fail(function(data) {
					console.log("Store Locator API is not responding.");
				});
			}
		}
		
		function toUrlFormat(val) {
			var result;
			result = val.replace(/[^a-zA-Z0-9\-\s]+/g,"").replace(/ /g,"-").toLowerCase();
			result = result.replace(/--+/g, '-');
			
			return result;
		}
		
		function updateMilesNumber() {
		var milesNumber = $('#miles').val() + " miles";
				
		$('.found .miles-number').text(milesNumber);
	}
			
	function updateStoreCount(count) {
		var storeString = (count == 1) ? "<span class='number'>" + count.toString() + "</span> store" : "<span class='number'>" + count.toString() + "</span> stores"
				
		$('.found .store-count').html(storeString);
	}

	$( "#miles" ).change(function() {
		
		storeLocations();
		updateMilesNumber();
	});

		function createMarkers(places,addresses) {
			var stateParam = decodeURIComponent(getParamValue("state"));
			var bounds = new google.maps.LatLngBounds();
			var el = $('#list');
			/*var guids = [];
			var addresses;*/
			$('.row', el).remove();
				
			// for (var i = 0, place; place = places.place_id[i]; i++) {
			// 	var guids = [];
			// 	if (place.place_id != null)
			// 	{
			// 		guids.push(place.place_id);
			// 	}
			// }
				
			// $.ajax({
			// 		url:"/address-by-guid",
			// 		type: "POST",
			// 		data: {"guids": guids},
			// 		success:function(res){
			// 			addresses = JSON.parse(res);
			// 		}
			// });
			
			for (var i = 0, place; place = places.results[i]; i++) {
				var phoneNumber = "";
				var businessNumber = "";
				var coords = new google.maps.LatLng(place['geometry']['location']['lat'], place['geometry']['location']['lng']);
				var digits = (i+1).toString().length;
				var markerClass = (digits == 1) ? "marker-label-digit" : "marker-label-digits";
				
				//this code allows us to resize the marker pin
				var pinIcon = new google.maps.MarkerImage(
					"https://stores.ashleyfurniture.com/resources/images/ellipse-listing.svg",
					null, /* size is determined at runtime */
					null, /* origin is 0,0 */
					null, /* anchor is bottom center of the scaled image */
					new google.maps.Size(25, 30)
				); 

				//var marker = new google.maps.Marker({
				var marker = new google.maps.Marker ({	
					map: map,
					icon: pinIcon,
					animation: google.maps.Animation.DROP,
					labelContent:""+ (i+1),
					labelClass: markerClass,
					labelAnchor: new google.maps.Point(0, 32),
					title: place['name'],
					position: coords,
					address: place['formatted_address']
				});

				
				var distance = (Math.round(place.Distance) == 0) ? "1 mile" : Math.round(place.Distance) + " miles";
				// var city = (place.ExtraData.Address["Locality"] != null) ? place.ExtraData.Address["Locality"] : "";
				// var state = (place.ExtraData.Address["Region"]) ? place.ExtraData.Address["Region"] : "";
				// var postalCode = (place.ExtraData.Address["PostalCode"]) ? place.ExtraData.Address["PostalCode"] : "";
				 //var region = (place.ExtraData.Address["Region"] != null) ? place.ExtraData.Address["Region"] : "";
				// var country = (place.ExtraData.Address["CountryCode"] != null) ? place.ExtraData.Address["CountryCode"] : "";
				// var locationNumber = (place.LocationNumber != null) ? place.LocationNumber : "";
				 var locationId = (place['place_id'] != null) ? place['place_id'] : "";
				 var name = (place['name']!= null)? place['name'] : "";
				// var scheduleAppointment = (place.ExtraData.ScheduleApptURL != null) ? place.ExtraData.ScheduleApptURL : "";
				// var storeStatus = (place.ExtraData.StoreStatus != null) ? place.ExtraData.StoreStatus : "";
				// region = (city != region) ? region : "";
				//console.log(place['place_id']);
				// var city_state_postal_code = city;
				// if (city_state_postal_code != "" && (state != "" || postalCode != ""))
				// {
				// 	city_state_postal_code += ", "
				// }
				// if (state != "")
				// {
				// 	city_state_postal_code += state + " ";
				// }
				// city_state_postal_code += postalCode;
				
				// var state_country_postal_code = ((state != "") ? (state + ", " + country + " " + postalCode) : (country + " " + postalCode));
				
				var ptAddressURL = (addresses != null && place['place_id'] != null && place['formatted_address'] != null) ? place['formatted_address'] : "";
				// var ptCityURL = (addresses != null && addresses[locationId] != null && addresses[locationId]["cityURL"] != null) ? addresses[locationId]["cityURL"] : "";
				// var ptStateURL = (addresses != null && addresses[locationId] != null && addresses[locationId]["stateURL"] != null) ? addresses[locationId]["stateURL"] : "";
				// var ptCountryURL = (addresses != null && addresses[locationId] != null && addresses[locationId]["countryURL"] != null) ? addresses[locationId]["countryURL"] : "";
				//var ptAddressURL = ptAddressURL.replace(" " , "+");
				//var url = "/store"; 
				var url = "https://www.google.com/maps/dir//"+ptAddressURL;

				//console.log(ptAddressURL);
				// if(locationId == "ChIJqxmked4YDTkR2yu_JpLXDzs"){
				// 	url = url+"-suite-90";
				// 	//alert(url);
				// 	console.log(url);
				// }
				// if(locationId == "ChIJoWhx5SHBwjsRXY__V74qH9k"){
				// 	url = url+"-suite-110";
				// 		console.log(url);
				// }
				var today = new  Date();
				var day = today.getDay();
				day = (day == 7) ? 0 : day;
				var str = "";
				var hoursArray = [];
				
				// if (place.ExtraData["Hours of operations"] != null)
				// {
				// 	hoursArray = place.ExtraData["Hours of operations"].split("|");
    //             }

                if (hoursArray[day] != null && hoursArray.length == 7){
					str = hoursArray[day];
                }
                
                var specialDay = "";
                var today = new  Date();

                // var specialHourJson = place.ExtraData["HoursOfOpStruct"];
                // if(typeof specialHourJson != 'undefined'){
                //     specialHourJson = specialHourJson["SpecialHours"];
                //     if(specialHourJson != null){
                //         for(var shj in specialHourJson){
                //             specialDay = new Date(specialHourJson[shj]["Date"]);
                //             if(specialDay.setHours(0,0,0,0) == today.setHours(0,0,0,0)) {
                //                 if(specialHourJson[shj]["Ranges"] != null){
                //                     str = specialHourJson[shj]["Ranges"][0]["StartTime"] + "-" + specialHourJson[shj]["Ranges"][0]["EndTime"];
                //                 }else{
                //                     str = "Closed";
                //                 }
                //             }
                //         }
                //     }
                // }
		
				//var openHoursText = (storeStatus.toLowerCase().indexOf('closed') !== -1) ? storeStatus : getOpenHours(str, day);
				
				// var directions = ((city != state) ? (encodeURIComponent(place.Address) + " " + encodeURIComponent(city) + " " + encodeURIComponent(state)) : 
				// 								(encodeURIComponent(place.Address) + " " + encodeURIComponent(state) + " " + encodeURIComponent(country)));
				var infoContent = "<div class='info-content'><p class='name'>" + name + "</p>" +
								"<p class='address'>" + place['formatted_address'] + "</p>" +
								"<a class='details-button btn btn-default' href='" + url + "' target='_blank'>Store direction</a></div>";
				var infowindow = new google.maps.InfoWindow();
				
				google.maps.event.addListener(marker,'click', (function(marker,infoContent,infowindow){
					return function() {
						infowindow.setContent(infoContent);
						infowindow.open(map,marker);
						//track marker click results page
						var screenSize=$(window).width();
						// var screenType=(screenSize<=623)?"mobile":"desktop";
						// 	dataLayer.push({
						// 		'event':'trackevent',
						// 		'eventcategory':"pt-store-list",
						// 		'eventaction':screenType+" - map-marker click",
						// 		'eventlabel':marker.address.toLowerCase(),
						// 		'eventvalue':""
						// 	});

						
					};
				})(marker,infoContent,infowindow));
				
				
				//placesList.innerHTML += '<div class="row"><h3>' + place['name'] + '<h3></div>';
				text = "<div class='row'><div class='counter'>" + (i+1) + "</div><div class='text'><a class='details-link' href='" + url + "' target='_self'><h3>" +  name + "</h3></a>" + 
				
				"<span class='address'>" + place['formatted_address'] + "</span><br>" + 
				
				"</a>" +
				//"<p class='phone'>Store Phone: " + phoneNumber + "</p>" +
				"<a class='details-button btn btn-default' href='" + url + "' target='_blank'>Store direction</a>" +
				"</div><hr /></div>";
				$(el).append(text);

				bounds.extend(coords);
			}
			
			
			// Don't zoom in too far on only one marker
			if (bounds.getNorthEast().equals(bounds.getSouthWest())) {
				var extendPoint1 = new google.maps.LatLng(bounds.getNorthEast().lat() + 0.001, bounds.getNorthEast().lng() + 0.001);
				var extendPoint2 = new google.maps.LatLng(bounds.getNorthEast().lat() - 0.001, bounds.getNorthEast().lng() - 0.001);
				bounds.extend(extendPoint1);
				bounds.extend(extendPoint2);
			}
			
			map.fitBounds(bounds);
		}
	}
})
	
	function renderMap(lat, lng, name, address, cityState) {
		var coordinates = {lat: lat, lng: lng};
		map = new google.maps.Map(document.getElementById('map-canvas'), {
			center: coordinates,
			zoom: 17
		});
		//this code allows us to resize the marker pin
		var pinIcon = new google.maps.MarkerImage(
			"/resources/images/ellipse-listing.svg",
			null, /* size is determined at runtime */
			null, /* origin is 0,0 */
			null, /* anchor is bottom center of the scaled image */
			new google.maps.Size(20, 30)
		); 
		var bounds = new google.maps.LatLngBounds();
		
		if (address != "")
		{
			var marker = new MarkerWithLabel ({	
				map: map,
				icon: pinIcon,
				animation: google.maps.Animation.BOUNCE,
				title: name,
				labelContent: "",
				labelAnchor: new google.maps.Point(3, 30),
				position: coordinates,
				markeraddress: address
			});
			
			var iw = new google.maps.InfoWindow({
				content: name + "<br>" + address + ((cityState != "") ? (", " + cityState) : "")
			});
			google.maps.event.addListener(marker, "click", function (e) { 
				iw.open(map, this); 
				//track marker click
				var screenSize=$(window).width();
				var screenType=(screenSize<=623)?"mobile":"desktop";
					dataLayer.push({
						'event':'trackevent',
						'eventcategory':"pt-store-details",
						'eventaction':screenType+" - map-marker click",
						'eventlabel':marker.markeraddress.toLowerCase(),
						'eventvalue':""
					});
				
			});
		}
		bounds.extend(coordinates);
		
		// Don't zoom in too far on only one marker
		if (bounds.getNorthEast().equals(bounds.getSouthWest())) {
			var extendPoint1 = new google.maps.LatLng(bounds.getNorthEast().lat() + 0.001, bounds.getNorthEast().lng() + 0.001);
			var extendPoint2 = new google.maps.LatLng(bounds.getNorthEast().lat() - 0.001, bounds.getNorthEast().lng() - 0.001);
			bounds.extend(extendPoint1);
			bounds.extend(extendPoint2);
		}
			
		map.fitBounds(bounds);
	}