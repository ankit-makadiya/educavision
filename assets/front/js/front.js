$(document).ready(function () {
    if ($('.slider-area').length > 0) {
        $(".slider-area").css({"margin-top": $(".navgation-bar").height() + "px"});
    }
    if ($('.c-layout-breadcrumbs-1').length > 0) {
        $(".c-layout-breadcrumbs-1").css({"margin-top": $(".navgation-bar").height() + "px"});
    }
});

/* COMMON SCRIPTS */
$('a.hit, a.dropdown-toggle').click(function (e) {
    var menuClass = $(e.target).attr('class');
    var menuUrl = $(this).attr('href');
    if (menuClass != 'zmdi zmdi-chevron-down') {
        location.href = menuUrl;
    }
});
(function () {
    $("#cart").on("click", function () {
        var element = document.getElementsByTagName("body")[0];
        element.classList.toggle("c-header-cart-shown");

        var element1 = document.getElementsByClassName("c-cart-menu")[0];
        element1.classList.toggle("c-layout-cart-menu-shown");

    });
})();

function changeCountry(val, sel_val = '') {
    $.ajax({
        url: base_url + 'user/getStateList',
        type: 'post',
        data: 'country_id=' + val + '&state_id=' + sel_val,
        dataType: "html",
        success: function (result) {
            $('.state').html(result);
        },
    });

    $.ajax({
        url: base_url + 'update-shipping-tax',
        type: 'post',
        data: 'country_id=' + val,
        dataType: "json",
        success: function (result) {
            //console.log(result.data);
            $('.c-subtotal').html(result.data.cart_total);
            $('.c-tax').html(result.data.cart_tax);
            $('.c-shipping').html(result.data.cart_shipping);
            $('.c-discount').html(result.data.cart_discount);
            $('.c-grand-total').html(result.data.cart_grand_total);
            $('#cart_grand_total').val(result.data.cart_grand_total);
            if(result.data.cart_grand_total > 0){
                $('.stripe_payment').removeClass('hide');
                $('.stripe_payment').addClass('show');
            }else{
                $('.stripe_payment').removeClass('show');
                $('.stripe_payment').addClass('hide');
            }
        },
    });
}
function changeState(val, sel_val = '') {
    $.ajax({
        url: base_url + 'user/getCityList',
        type: 'post',
        data: 'state_id=' + val + '&city_id=' + sel_val,
        dataType: "html",
        success: function (result) {
            $('.city').html(result);
        },
    });
}

/* COMMON SCRIPTS */

/* CATALOG SCRIPTS */

$('body').delegate('.pagination_box', 'click', function (e) {
    changeUrl('page', $(this).attr("data-id"));
});

function sortBy(value) {
    if (value != '') {
        changeUrl("sort", value);
    } else {
        removeQString("sort");
    }
    window.location.href = '';
}
var objQueryString = {};
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
function changeUrl(key, value) {
    //Get query string value
    var searchUrl = location.search;
    if (searchUrl.indexOf("?") == "-1") {
        var urlValue = '?' + key + '=' + value;
        //var urlValue = encodeURI(urlValue);
        history.pushState({state: 1, rand: Math.random()}, '', urlValue);
    } else {
        //Check for key in query string, if not present
        if (searchUrl.indexOf(key) == "-1") {
            var urlValue = searchUrl + '&' + key + '=' + value;
        } else {  //If key present in query string
            oldValue = getParameterByName(key);
            if (searchUrl.indexOf("?" + key + "=") != "-1") {
                urlValue = searchUrl.replace('?' + key + '=' + oldValue, '?' + key + '=' + value);
            } else {
                urlValue = searchUrl.replace('&' + key + '=' + oldValue, '&' + key + '=' + value);
            }
        }
        //var urlValue = encodeURI(urlValue);
        history.pushState({state: 1, rand: Math.random()}, '', urlValue);
        //history.pushState function is used to add history state.
        //It takes three parameters: a state object, a title (which is currently ignored), and (optionally) a URL.
    }
    objQueryString.key = value;
    location.reload();
}
//Function used to remove querystring
function removeQString(key) {
    var urlValue = document.location.href;
    //Get query string value
    var searchUrl = location.search;
    if (key != "") {
        oldValue = getParameterByName(key);
        removeVal = key + "=" + oldValue;
        if (searchUrl.indexOf('?' + removeVal + '&') != "-1") {
            urlValue = urlValue.replace('?' + removeVal + '&', '?');
        } else if (searchUrl.indexOf('&' + removeVal + '&') != "-1") {
            urlValue = urlValue.replace('&' + removeVal + '&', '&');
        } else if (searchUrl.indexOf('?' + removeVal) != "-1") {
            urlValue = urlValue.replace('?' + removeVal, '');
        } else if (searchUrl.indexOf('&' + removeVal) != "-1") {
            urlValue = urlValue.replace('&' + removeVal, '');
        }
    } else {
        var searchUrl = location.search;
        urlValue = urlValue.replace(searchUrl, '');
    }
    history.pushState({state: 1, rand: Math.random()}, '', urlValue);
}

function filter_data() {
    $('.filter_data').html('<div id="loading" style=""></div>');
    var minimum_price = $('#hidden_minimum_price').val();
    var maximum_price = $('#hidden_maximum_price').val();
    var author = get_filter('author');
    var category = get_filter('category');
    author = author.join("|");
    category = category.join("|");
    pricerange = minimum_price + '|' + maximum_price;
    removeQString('page');
    if (pricerange != '') {
        changeUrl('pricerange', pricerange);
    }
    if (author != '') {
        changeUrl('author', author);
    }
    if (category != '') {
        changeUrl('category', category);
    }
}

function get_filter(class_name) {
    var filter = [];
    $('.' + class_name + ':checked').each(function () {
        filter.push($(this).val());
    });
    if ($('.' + class_name + ':checked').length == 0) {
        removeQString(class_name);
    }
    return filter;
}

$('.common_selector').click(function () {
    filter_data();
});
if ($('#price_range').length > 0) {
    $('#price_range').slider({
        range: true,
        min: 0,
        max: max_price,
        values: [filter_min_price, filter_max_price],
        step: 1,
        stop: function (event, ui) {
            $('#price_show').html('$ ' + ui.values[0] + ' - ' + '$ ' + ui.values[1]);
            $('#hidden_minimum_price').val(ui.values[0]);
            $('#hidden_maximum_price').val(ui.values[1]);
            filter_data();
        }
    });
}

/* CATALOG SCRIPTS */

/* NEWS SCRIPTS */

if ($('#news_comment_form').length > 0) {
    $("#news_comment_form").validate({
        rules: {
            name: {
                required: true,
            },
            comment: {
                required: true,
            },
        },
        messages: {
            name: {
                required: "Please enter name",
            },
            comment: {
                required: "Please enter comment",
            },
        },
        submitHandler: submitNewsCommentForm
    });
}
function submitNewsCommentForm()
{
    var news_id = $("#news_id").val();
    var name = $("#name").val();
    var email = $("#email").val();
    var comment = $("#comment").val();

    var post_data = {
        'news_id': news_id,
        'name': name,
        'email': email,
        'comment': comment,
    }
    $.ajax({
        type: 'POST',
        url: base_url + 'news-add-comments',
        data: post_data,
        dataType: "json",
        beforeSend: function ()
        {
            $("#error").fadeOut();
            $("#btn-login").html('Submit ...');
        },
        success: function (response)
        {
            console.log(response)
            if (response.success == '1') {
                swal({
                    title: "Success",
                    text: response.message,
                    type: "success",
                    showCancelButton: false,
                    confirmButtonClass: "btn-success",
                    confirmButtonText: "Ok",
                    closeOnConfirm: false
                },
                        function () {
                            window.location.href = "";
                        });
            } else {
                swal({
                    title: "Error",
                    text: response.message,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-error",
                    confirmButtonText: "Ok",
                    closeOnConfirm: false
                },
                        function () {
                            window.location.href = "";
                        });
            }
        }
    });
    return false;
}

/* NEWS SCRIPTS */

/* REGISTRATION SCRIPTS */

if ($('#registration_form').length > 0) {
    $('#country').select2({
        placeholder: 'Select Country'
    });
    $('#state').select2({
        placeholder: 'Select State'
    });
    $('#city').select2({
        placeholder: 'Select City'
    });

    $("#registration_form").validate({
        rules: {
            first_name: {
                required: true,
            },
            last_name: {
                required: true,
            },
            phone: {
                required: true,
            },
            email: {
                required: true,
                remote: {
                    url: base_url + 'user/checkuniqueuseremail',
                    type: "post",
                    data: {
                        email: function () {
                            return $("#email").val();
                        }
                    }
                }
            },
            password: {
                required: true,
            },
            confirm_password: {
                required: true,
                equalTo: "#password"
            },
            country: {
                required: true,
            },
            state: {
                required: true,
            },
            city: {
                required: true,
            },
            zipcode: {
                required: true,
            },
            address1: {
                required: true,
            },
        },
        messages: {
            first_name: {
                required: "Please enter first name",
            },
            last_name: {
                required: "Please enter last name",
            },
            phone: {
                required: "Please enter phone number",
            },
            email: {
                required: "Please enter email id",
                remote: "This email id is already in used",
            },
            password: {
                required: "Please enter password",
            },
            confirm_password: {
                required: "Please enter confirm password",
                equalTo: "Please enter confirm password same as password"
            },
            country: {
                required: "Please select country",
            },
            state: {
                required: "Please select state",
            },
            city: {
                required: "Please select city",
            },
            zipcode: {
                required: "Please enter zipcode",
            },
            address1: {
                required: "Please enter address",
            },
        },
        highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            if (elem.hasClass("select2-hidden-accessible")) {
                $("#select2-" + elem.attr("id") + "-container").parent().addClass(errorClass);
            } else {
                elem.addClass(errorClass);
            }
        },
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            if (elem.hasClass("select2-hidden-accessible")) {
                $("#select2-" + elem.attr("id") + "-container").parent().removeClass(errorClass);
            } else {
                elem.removeClass(errorClass);
            }
        },
        errorPlacement: function (error, element) {
            var elem = $(element);
            if (elem.hasClass("select2-hidden-accessible")) {
                element = $("#select2-" + elem.attr("id") + "-container").parent();
                error.insertAfter(element);
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: submitRegistrationForm
    });
    function submitRegistrationForm()
    {
        var post_data = {
            'first_name': $("#first_name").val(),
            'last_name': $("#last_name").val(),
            'email': $("#email").val(),
            'phone': $("#phone").val(),
            'password': $("#password").val(),
            'country': $("#country").val(),
            'state': $("#state").val(),
            'city': $("#city").val(),
            'zipcode': $("#zipcode").val(),
            'company_name': $("#company_name").val(),
            'address1': $("#address1").val(),
            'address2': $("#address2").val(),
        }
        $.ajax({
            type: 'POST',
            url: base_url + 'user/save-registration',
            data: post_data,
            dataType: "json",
            beforeSend: function ()
            {
                $("#error").fadeOut();
                $("#btn-login").html('Submit ...');
            },
            success: function (response)
            {
                console.log(response)
                if (response.success == '1') {
                    swal({
                        title: "Success",
                        text: response.message,
                        type: "success",
                        showCancelButton: false,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Ok",
                        closeOnConfirm: false
                    },
                            function () {
                                window.location.href = "";
                            });
                } else {
                    swal({
                        title: "Error",
                        text: response.message,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-error",
                        confirmButtonText: "Ok",
                        closeOnConfirm: false
                    },
                            function () {
                                window.location.href = "";
                            });
                }
            }
        });
        return false;
    }
}

/* REGISTRATION SCRIPTS */

/* LOGIN SCRIPTS */

if ($('#login_form').length > 0) {
    $("#login_form").validate({
        rules: {
            login_email: {
                required: true,
            },
            login_password: {
                required: true,
            },
        },
        messages: {
            login_email: {
                required: "Please enter email id",
            },
            login_password: {
                required: "Please enter password",
            },
        },
        submitHandler: submitLoginForm
    });
    function submitLoginForm()
    {
        var post_data = {
            'email': $("#login_email").val(),
            'password': $("#login_password").val(),
        }
        $.ajax({
            type: 'POST',
            url: base_url + 'user/login-authentication',
            data: post_data,
            dataType: "json",
            beforeSend: function ()
            {
                $("#error").fadeOut();
                $("#btn-login").html('Submit ...');
            },
            success: function (response)
            {
                if (response.success == '1') {
                    window.location.href = base_url;
                } else {
                    swal({
                        title: "Error",
                        text: response.message,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-error",
                        confirmButtonText: "Ok",
                        closeOnConfirm: false
                    },
                            function () {
                                window.location.href = "";
                            });
                }
            }
        });
        return false;
    }
}

/* LOGIN SCRIPTS */

/* FORGOT PASSWORD */


if ($('#forgot_form').length > 0) {
    $("#forgot_form").validate({
        rules: {
            email: {
                required: true,
            },
        },
        messages: {
            email: {
                required: "Please enter email id",
            },
        },
        submitHandler: submitForgotForm
    });
    function submitForgotForm()
    {
        var post_data = {
            'email': $("#forgot_email").val(),
        }
        $.ajax({
            type: 'POST',
            url: base_url + 'user/forgot-password',
            data: post_data,
            dataType: "json",
            beforeSend: function ()
            {
                $("#error").fadeOut();
                $("#btn-login").html('Submit ...');
            },
            success: function (response)
            {
                $('#forgotPassword').modal('hide');
                if (response.success == '1') {
                    swal({
                        title: "Success",
                        text: response.message,
                        type: "success",
                        showCancelButton: false,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Ok",
                        closeOnConfirm: false
                    },
                            function () {
                                window.location.href = "";
                            });
                } else {
                    swal({
                        title: "Error",
                        text: response.message,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-error",
                        confirmButtonText: "Ok",
                        closeOnConfirm: false
                    },
                            function () {
                                window.location.href = "";
                            });
                }
            }
        });
        return false;
    }
}


/* FORGOT PASSWORD */

function addtocart(id, attribute = '') {
    var quantity = 1;
    if ($('#quantity').length > 0) {
        var quantity = $('#quantity').val();
    }
    if (attribute == '') {
        var attribute = $('#attribute').val();
    }
    var post_data = {
        'id': id,
        'quantity': quantity,
        'attribute': attribute,
    }
    if (attribute != '') {
        $.ajax({
            type: 'POST',
            url: base_url + 'shoppingcart/addtocart',
            data: post_data,
            dataType: "json",
            beforeSend: function ()
            {
                $("#error").fadeOut();
                $("#btn-login").html('Submit ...');
            },
            success: function (response)
            {
                console.log(response)
                if (response.success == '1') {
                    swal({
                        title: "Shopping Cart!",
                        text: response.message,
                        type: "success",
                        showCancelButton: false,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Ok",
                        closeOnConfirm: false
                    },
                            function () {
                                window.location.href = "";
                            });
                } else {
                    swal({
                        title: "Shopping Cart!",
                        text: response.message,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-error",
                        confirmButtonText: "Ok",
                        closeOnConfirm: false
                    },
                            function () {
                                window.location.href = "";
                            });
                }
            }
        });
}
}

function removeCart(rawid = '') {
    var post_data = {
        'rawid': rawid,
    }
    $.ajax({
        type: 'POST',
        url: base_url + 'shoppingcart/removecart',
        data: post_data,
        dataType: "json",
        beforeSend: function ()
        {
        },
        success: function (response)
        {
            if (response.success == '1') {
                swal({
                    title: "Shopping Cart!",
                    text: response.message,
                    type: "success",
                    showCancelButton: false,
                    confirmButtonClass: "btn-success",
                    confirmButtonText: "Ok",
                    closeOnConfirm: false
                },
                        function () {
                            window.location.href = "";
                        });
            } else {
                swal({
                    title: "Shopping Cart!",
                    text: response.message,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-error",
                    confirmButtonText: "Ok",
                    closeOnConfirm: false
                },
                        function () {
                            window.location.href = "";
                        });
            }
        }
    });
}
if ($('.touchspin').length > 0) {
    $(".touchspin").TouchSpin(
            {
                buttondown_class: 'btn c-btn-red',
                buttonup_class: 'btn blue',
                min: 1,
                max: 1000
            })
            .on('touchspin.on.startupspin', function () {
                var rowid = $(this).data('rowid');
                var qty = $(this).val();
                updateQty(rowid, qty);
            })
            .on('touchspin.on.startdownspin', function () {
                var rowid = $(this).data('rowid');
                var qty = $(this).val();
                updateQty(rowid, qty);
            });

    function updateQty(rowid, qty) {
        var post_data = {
            'rowid': rowid,
            'qty': qty,
        }

        $.ajax({
            type: 'POST',
            url: base_url + 'shoppingcart/updatecart',
            data: post_data,
            dataType: "json",
            beforeSend: function ()
            {
            },
            success: function (response)
            {
                if (response.success == '1') {
                    swal({
                        title: "Shopping Cart!",
                        text: response.message,
                        type: "success",
                        showCancelButton: false,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Ok",
                        closeOnConfirm: false
                    },
                            function () {
                                //window.location.href = "";
                                window.location.reload(true)
                            });
                } else {
                    swal({
                        title: "Shopping Cart!",
                        text: response.message,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-error",
                        confirmButtonText: "Ok",
                        closeOnConfirm: false
                    },
                            function () {
                                //window.location.href = "";
                                window.location.reload(true)
                            });
                }
            }
        });
    }
}
/* checkout */
$(document).ready(function () {
    $('#is_billing_checkbox').click(function () {
        if ($(this).prop("checked") == true) {
            $('.c-billing-address').show();
        } else if ($(this).prop("checked") == false) {
            $('.c-billing-address').hide();
        }
    });
});

/* checkout */

if ($('#shippingcalculationform').length > 0) {
    $("#shippingcalculationform").validate({
        rules: {
            shipping_country: {
                required: true,
            },
        },
        messages: {
            shipping_country: {
                required: "Please enter email id",
            },
        },
        submitHandler: submitCalculateShippingForm
    });
    function submitCalculateShippingForm()
    {
        var post_data = {
            'shipping_country': $("#shipping_country").val(),
        }
        $.ajax({
            type: 'POST',
            url: base_url + 'calculate-shipping-tax',
            data: post_data,
            dataType: "json",
            beforeSend: function ()
            {
                $("#error").fadeOut();
                $("#btn-login").html('Submit ...');
            },
            success: function (response)
            {
                console.log(response)
                if (response.success == '1') {
                    swal({
                        title: "Success",
                        text: response.message,
                        type: "success",
                        showCancelButton: false,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Ok",
                        closeOnConfirm: false
                    },
                            function () {
                                window.location.href = "";
                            });
                } else {
                    swal({
                        title: "Error",
                        text: response.message,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-error",
                        confirmButtonText: "Ok",
                        closeOnConfirm: false
                    },
                            function () {
                                window.location.href = "";
                            });
                }
            }
        });
        return false;
    }
}

if ($('#change_password').length > 0) {
    $("#change_password").validate({
        rules: {
            old_password: {
                required: true,
            },
            new_password: {
                required: true,
            },
            confirm_password: {
                required: true,
                equalTo: "#new_password"
            },
        },
        messages: {
            old_password: {
                required: "Please enter old password",
            },
            new_password: {
                required: "Please enter new password",
            },
            confirm_password: {
                required: "Please enter Confirm password",
                equalTo: "Your password is not match to new password"
            },
        },
        submitHandler: submitForgotForm
    });
    function submitForgotForm()
    {
        var post_data = {
            'email': $("#email").val(),
        }
        $.ajax({
            type: 'POST',
            url: base_url + 'user/forgot-password',
            data: post_data,
            dataType: "json",
            beforeSend: function ()
            {
                $("#error").fadeOut();
                $("#btn-login").html('Submit ...');
            },
            success: function (response)
            {
                console.log(response)
                if (response.success == '1') {
                    swal({
                        title: "Success",
                        text: response.message,
                        type: "success",
                        showCancelButton: false,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Ok",
                        closeOnConfirm: false
                    },
                            function () {
                                window.location.href = "";
                            });
                } else {
                    swal({
                        title: "Error",
                        text: response.message,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-error",
                        confirmButtonText: "Ok",
                        closeOnConfirm: false
                    },
                            function () {
                                window.location.href = "";
                            });
                }
            }
        });
        return false;
    }
}

if ($('#apply_coupon_form').length > 0) {
    $("#apply_coupon_form").validate({
        rules: {
            coupon_code: {
                required: true,
            }
        },
        messages: {
            coupon_code: {
                required: "Please enter coupon code.",
            }
        },
        submitHandler: submitCouponForm
    });
}
if ($('.applycoupon').length > 0) {
    $('.applycoupon').on('click', function () {
        var coupon_code = $("#coupon_code").val();
        if (coupon_code != '') {
            submitCouponForm();
        }
    });
}
function submitCouponForm() {
    var post_data = {
        'coupon_code': $("#coupon_code").val(),
    }
    $.ajax({
        type: 'POST',
        url: base_url + 'shoppingcart/apply_coupon',
        data: post_data,
        dataType: "json",
        beforeSend: function ()
        {
            $("#error").fadeOut();
            $("#btn-login").html('Submit ...');
        },
        success: function (response)
        {
            console.log(response)
            if (response.success == '1') {
                swal({
                    title: "Success",
                    text: response.message,
                    type: "success",
                    showCancelButton: false,
                    confirmButtonClass: "btn-success",
                    confirmButtonText: "Ok",
                    closeOnConfirm: false
                },
                        function () {
                            window.location.href = "";
                        });
            } else {
                swal({
                    title: "Error",
                    text: response.message,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-error",
                    confirmButtonText: "Ok",
                    closeOnConfirm: false
                },
                        function () {
                            window.location.href = "";
                        });
            }
        }
    });
    return false;
}


function searchAuthor() {
    // Declare variables
    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById('myInput');
    filter = input.value.toUpperCase();
    ul = document.getElementById("myUL");
    li = ul.getElementsByClassName('list-group-item');

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByClassName("author")[0];
        //txtValue = a.textContent || a.innerText;
        txtValue = a.getAttribute("data-name");

        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}
$('.pagination_box').on("click", function () {
    $('html, body').animate({scrollTop: 0}, 'fast');
    return true;
});

$('body').on('click', '#clearall', function (e) {
    $('.category' + categoryId).attr('checked', false);
    removeQString("page");
    removeQString("pricerange");
    removeQString("category");
    removeQString("sort");
    removeQString("rating");
    removeQString("author");
    location.reload();
});

function changeAttribute(a) {
    console.log(a.data.sku);
}
$('#attribute').on('change', function () {
    var sku = $(this).find(':selected').data('sku');
    var isbn = $(this).find(':selected').data('isbn');
    var price = $(this).find(':selected').data('price');

    $('#product-price').html(price);
    $('#product-isbn').html(isbn);
    $('#product-sku').html(sku);
});