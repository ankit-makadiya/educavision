<?php
$page_id = $_GET['id'];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <title>Content Builder</title>
        <link rel="stylesheet" type="text/css" href="plugins/bootstrap-3.3.6/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="plugins/font-awesome-4.5.0/css/font-awesome.min.css" />
        <link rel="stylesheet" type="text/css" href="../dist/css/keditor-1.1.5.css" />
        <link rel="stylesheet" type="text/css" href="../dist/css/keditor-components-1.1.5.css" />
        <link rel="stylesheet" type="text/css" href="css/examples.css" />

        <script type="text/javascript" src="plugins/jquery-1.11.3/jquery-1.11.3.min.js"></script>
        <script type="text/javascript" src="plugins/bootstrap-3.3.6/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="plugins/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
        <script type="text/javascript" src="plugins/jquery-ui.touch-punch-0.2.3/jquery.ui.touch-punch.js"></script>
        <script type="text/javascript" src="plugins/jquery.nicescroll-3.6.6/jquery.nicescroll.js"></script>
        <script type="text/javascript" src="plugins/ckeditor-4.5.6/ckeditor.js"></script>
        <script type="text/javascript" src="plugins/ckeditor-4.5.6/adapters/jquery.js"></script>
        <script type="text/javascript" src="../dist/js/keditor-1.1.5.js"></script>
        <script type="text/javascript" src="../dist/js/keditor-components-1.1.5.js"></script>
    </head>

    <body>
        <div id="content-area"></div>
        <input type="hidden" name="content" id="content" value="" size="100">
        <script type="text/javascript">
            $(function () {
                $('#content-area').keditor({
                    tabTooltipEnabled: true,
                    tabContainersText: 'Containers',
                    tabContainersTitle: 'Containers',
                    tabComponentsText: 'Components',
                    tabComponentsTitle: 'Components',
//                    extraTabs: {
//                        tabName: {
//                            text: 'Custom Plugins',
//                            title: 'Custom Plugins',
//                            content: 'Here is content of My Extra Tab #1'
//                        },
//                    },
                    extraTabs: null,
                    snippetsTooltipEnabled: false,
                    debug: false,
                    containerSettingEnabled: true,
                    containerSettingInitFunction: function (form, keditor) {
                        // Add control for settings form
                        form.append(
                                '<div class="form-horizontal">' +
                                '   <div class="form-group">' +
                                '       <div class="col-sm-12">' +
                                '           <label>Background color</label>' +
                                '           <input type="text" class="form-control txt-bg-color" />' +
                                '       </div>' +
                                '   </div>' +
                                '   <div class="form-group">' +
                                '       <div class="col-sm-12">' +
                                '           <label>Padding</label>' +
                                '           <input type="text" class="form-control txt-padding" placeholder="i.e 10px 10px 10px 10px" />' +
                                '       </div>' +
                                '   </div>' +
                                '   <div class="form-group">' +
                                '       <div class="col-sm-12">' +
                                '           <label>Margin</label>' +
                                '           <input type="text" class="form-control txt-margin" placeholder="i.e 10px 10px 10px 10px" />' +
                                '       </div>' +
                                '   </div>' +
                                '</div>'
                                );

                        // Add event handle for background color textbox
                        form.find('.txt-bg-color').on('change', function () {
                            // Get current setting container
                            var container = keditor.getSettingContainer();
                            // Find '.row' for setting background color
                            // Note: Make sure you have a div for setting background color
                            var row = container.find('.row');
                            // Set background color with value of textbox
                            row.css('background-color', this.value);

                            var fileContent = $('#content-area').keditor('getContent');
                            $('#content').val(fileContent);
                        });
                        form.find('.txt-padding').on('change', function () {
                            // Get current setting container
                            var container = keditor.getSettingContainer();
                            // Find '.row' for setting background color
                            // Note: Make sure you have a div for setting background color
                            var row = container.find('.row');
                            // Set background color with value of textbox
                            row.css('padding', this.value);

                            var fileContent = $('#content-area').keditor('getContent');
                            $('#content').val(fileContent);
                        });
                        form.find('.txt-margin').on('change', function () {
                            // Get current setting container
                            var container = keditor.getSettingContainer();
                            // Find '.row' for setting background color
                            // Note: Make sure you have a div for setting background color
                            var row = container.find('.row');
                            row.css('margin', this.value);

                            var fileContent = $('#content-area').keditor('getContent');
                            $('#content').val(fileContent);
                        });
                    },
                    containerSettingShowFunction: function (form, container, keditor) {
                        // Find '.row' div
                        // Note: Make sure you have a div for setting background color
                        var row = container.find('.row');
                        // User "prop()" method to get properties of HTML element
                        var backgroundColor = row.prop('style').backgroundColor || '';
                        var margin = row.prop('style').margin || '';
                        var padding = row.prop('style').padding || '';

                        // User 'backgroundColor' for value of background color textbox
                        form.find('.txt-bg-color').val(backgroundColor);
                        form.find('.txt-margin').val(margin);
                        form.find('.txt-padding').val(padding);
                    },
                    containerSettingHideFunction: function (form, keditor) {
                        // Clean value of background color textbox when hiding settings form
                        form.find('.txt-bg-color').val('');
                        form.find('.txt-margin').val('');
                        form.find('.txt-padding').val('');
                    },
                    onReady: function () {
                        $('.keditor-content-area').css('min-height', $(window).height());
                    },
                    onInitFrame: function (frame, frameHead, frameBody) {
                        //var fileContent = contentArea.keditor('getContent');
                        var fileContent = $('#content-area').keditor('getContent');
                        $('#content').val(fileContent);
                    },
                    onSidebarToggled: function (isOpened) {
                        var fileContent = $('#content-area').keditor('getContent');
                        $('#content').val(fileContent);
                    },
                    onInitContentArea: function (contentArea) {
                        var fileContent = $('#content-area').keditor('getContent');
                        $('#content').val(fileContent);
                    },
                    onContentChanged: function (event, contentArea) {
                        var fileContent = $('#content-area').keditor('getContent');
                        $('#content').val(fileContent);
                    },
                    onInitContainer: function (container, contentArea) {
                        var fileContent = $('#content-area').keditor('getContent');
                        $('#content').val(fileContent);
                    },
                    onBeforeContainerDeleted: function (event, selectedContainer, contentArea) {
                        var fileContent = $('#content-area').keditor('getContent');
                        $('#content').val(fileContent);
                    },
                    onContainerDeleted: function (event, selectedContainer, contentArea) {
                        var fileContent = $('#content-area').keditor('getContent');
                        $('#content').val(fileContent);
                    },
                    onContainerChanged: function (event, changedContainer, contentArea) {
                        var fileContent = $('#content-area').keditor('getContent');
                        $('#content').val(fileContent);
                    },
                    onContainerDuplicated: function (event, originalContainer, newContainer, contentArea) {
                        var fileContent = $('#content-area').keditor('getContent');
                        $('#content').val(fileContent);
                    },
                    onContainerSelected: function (event, selectedContainer, contentArea) {
                        var fileContent = $('#content-area').keditor('getContent');
                        $('#content').val(fileContent);
                    },
                    onContainerSnippetDropped: function (event, newContainer, droppedSnippet, contentArea) {
                        var fileContent = $('#content-area').keditor('getContent');
                        $('#content').val(fileContent);
                    },
                    onComponentReady: function (component) {
                        var fileContent = $('#content-area').keditor('getContent');
                        $('#content').val(fileContent);
                    },
                    onInitComponent: function (component, contentArea) {
                        var fileContent = $('#content-area').keditor('getContent');
                        $('#content').val(fileContent);
                    },
                    onBeforeComponentDeleted: function (event, selectedComponent, contentArea) {
                        var fileContent = $('#content-area').keditor('getContent');
                        $('#content').val(fileContent);
                    },
                    onComponentDeleted: function (event, selectedComponent, contentArea) {
                        var fileContent = $('#content-area').keditor('getContent');
                        $('#content').val(fileContent);
                    },
                    onComponentChanged: function (event, changedComponent, contentArea) {
                        var fileContent = $('#content-area').keditor('getContent');
                        $('#content').val(fileContent);
                    },
                    onComponentDuplicated: function (event, originalComponent, newComponent, contentArea) {
                        var fileContent = $('#content-area').keditor('getContent');
                        $('#content').val(fileContent);
                    },
                    onComponentSelected: function (event, selectedComponent, contentArea) {
                        var fileContent = $('#content-area').keditor('getContent');
                        $('#content').val(fileContent);
                    },
                    onComponentSnippetDropped: function (event, newComponent, droppedSnippet, contentArea) {
                        var fileContent = $('#content-area').keditor('getContent');
                        $('#content').val(fileContent);
                    },
                    onBeforeDynamicContentLoad: function (dynamicElement, component, contentArea) {
                        var fileContent = $('#content-area').keditor('getContent');
                        $('#content').val(fileContent);
                    },
                    onDynamicContentLoaded: function (dynamicElement, response, status, xhr, contentArea) {
                        var fileContent = $('#content-area').keditor('getContent');
                        $('#content').val(fileContent);
                    },
                    onDynamicContentError: function (dynamicElement, response, status, xhr, contentArea) {
                        var fileContent = $('#content-area').keditor('getContent');
                        $('#content').val(fileContent);
                    }
                });
            });
            $("#content-area").focusout(function () {
                var fileContent = $('#content-area').keditor('getContent');
                $('#content').val(fileContent);
            });
            $("#content-area")
                    .mouseover(function () {
                        var fileContent = $('#content-area').keditor('getContent');
                        $('#content').val(fileContent);
                    })
                    .mouseout(function () {
                        var fileContent = $('#content-area').keditor('getContent');
                        $('#content').val(fileContent);
                    })
                    .mouseenter(function () {
                        var fileContent = $('#content-area').keditor('getContent');
                        $('#content').val(fileContent);
                    })
                    .mouseleave(function () {
                        var fileContent = $('#content-area').keditor('getContent');
                        $('#content').val(fileContent);
                    });
//            $(document).ready(function () {
//                setInterval(function () {
//                    var fileContent = $('#content-area').keditor('getContent');
//                    $('#content').val(fileContent);
//                }, 2000);
//            });

            $(document).ready(function () {
                var form = parent.document.getElementById('advance_description');
                var pageDesc = form.value;
                $('#content-area').keditor('setContent', pageDesc);
            });

//            $.ajax({
//                url: '/admin/contentcms/cmspages/getpagecontent',
//                type: 'POST',
//                data: {
//                    id: '<?php echo $page_id ?>',
//                },
//                dataType: 'html',
//                success: function (data) {
//                    $('#content-area').keditor('setContent', data);
//                },
//                error: function (e) {
//
//                }
//            });
        </script>
    </body>
</html>
