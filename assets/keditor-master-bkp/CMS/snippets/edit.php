<?php

namespace ContentcmsBundle\Controller;

use AppBundle\Service\GeneralService;
use AppBundle\Controller\CommonController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Controller\GeneralFunctionController;
use ContentcmsBundle\Entity\BlogCategory;
use LocalizationBundle\Entity\Language;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceList;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType ;
use Symfony\Component\Form\Extension\Core\Type\TextType ;
use Symfony\Component\Form\Extension\Core\Type\TextareaType ;
use Symfony\Component\Form\Extension\Core\Type\SubmitType ;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType ;

class BlogCategoryController extends CommonController
{
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * @Route("admin/contentcms/blogcategory", name="admin_contentcms_blogcategory")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        //Check Here Admin User is logged in or not
        $GeneralService = $this->get(GeneralService::class);
        $AdminUserId = $GeneralService->getAdminUserSession();

        $Permission = "";
        $Permission = $GeneralService->UserAccessDenied();
        if(empty($Permission)){
            header('HTTP/1.0 403 Forbidden');
            $TempPage = 'ContentcmsBundle:Admin/CMSPage:errordeniedpageadmin.html.twig';
        }else{
            $TempPage = 'ContentcmsBundle:Admin/BlogCategory:index.html.twig';
        }

        //Get User Admin Session Details
        $AdminDetails = $GeneralService->getAdminSessionDetails();

        //Get Store Main Configuration
        $SiteSetting = $GeneralService->getStoreMainConfig();
        $StoreImgSetting = json_decode($SiteSetting['storeLogoSetting']);

        //Get Admin User Sidebar and access module
        $AdminSideBar = $GeneralService->getAdminUserSidebar($AdminDetails);

        $lang_data = $em->getRepository(Language::class)->findBy(array('status'=>1,'isDeleted'=>0));

        return $this->render($TempPage,array('SiteSetting'=>$SiteSetting,'AdminDetails'=>$AdminDetails,'StoreImgSetting'=>$StoreImgSetting,'AdminSideBar'=>$AdminSideBar, 'lang_data'=>$lang_data));
    }

    /**
     * Get BlogCategoryNamebyId
     *
     * @return \BlogCategory Name
     */
    public function getBlogCategoryNamebyId($id="")
    {
        $catename = "";
        $catename = $this->getDoctrine()->getRepository('ContentcmsBundle:BlogCategory')->findOneById($id)->getName();
        return $catename;
    }

    /**
     * @Route("admin/contentcms/blogcategory/ajaxblogcategorydata")
     */
    public function getAjaxBlogCategoryData()
    {
        $request = Request::createFromGlobals();
        $langs = $this->getCommaseparatedEnabledLanguages();
        $filter_data = "c.isDeleted='0'";
        if(!empty($langs)){
            $filter_data .=" AND IDENTITY(c.lang) IN ($langs)";
        }
        if($request->request->get('name')){
            $filter_data .= " AND c.name LIKE '%".$request->request->get('name')."%'";
        }
        if($request->request->get('slug')){
            $filter_data .= " AND c.categorySlug LIKE '%".$request->request->get('slug')."%'";
        }
        if($request->request->get('lang_id')){
            $filter_data .= " AND IDENTITY(c.lang) = ".$request->request->get('lang_id');
        }
        if($request->request->get('status')){
            if($request->request->get('status') == 'enabled'){
                $post_status = 1;
            }
            else{
                $post_status = 0;
            }
            $filter_data .= " AND c.status = ".$post_status;
        }
        $em = $this->getDoctrine()->getManager();
        $BlogCategory = $em->createQuery(
            "SELECT c.id, c.name, c.categorySlug, IDENTITY(c.lang) as lang_id, c.status
            FROM ContentcmsBundle:BlogCategory c
            WHERE ".$filter_data."
            ORDER BY c.id DESC")->getResult();
        $i=0;
        foreach($BlogCategory as &$bcat)
        {
            $BlogCategory[$i]['langname'] = $this->getLanguageNamebyId($bcat['lang_id']);
            $i++;
        }
        //echo "<pre>";print_r($Category);exit;
        //Get Store Main Configuration
        $GeneralFunction = new GeneralFunctionController;
        $GeneralService = $this->get(GeneralService::class);
        $SiteSetting = $GeneralService->getStoreMainConfig();
        $iTotalRecords = count($BlogCategory);
        //$iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $SiteSetting['pageSetting'];
        if($request->request->get('length')){
            $iDisplayLength = $request->request->get('length');
        }
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $check = "'fa fa-check'";
        $check1 = "'fa fa-times'";
        $checked = "";
        for($i = $iDisplayStart; $i < $end; $i++) {
            $CatId   = $BlogCategory[$i]['id'];
            $Name    = $BlogCategory[$i]['name'];
            $Slug    = $BlogCategory[$i]['categorySlug'];
            $Langnm  = $BlogCategory[$i]['langname'];
            if($BlogCategory[$i]['status'] == 1)
            {
                $checked = 'checked="checked"';
            }
            else
            {
                $checked = "";
            }
            $ActLink = '<a href="blogcategory/editblogcategory/'.$GeneralFunction->encryptString($CatId).'" title="Edit Blog Category"><i class="fa fa-edit fafont mar-right-10"></i></a><a href="javascript:void(0);" title="Delete Blog Category" data-id="'.$GeneralFunction->encryptString($CatId).'" class="delonerows"><i class="fa fa-trash-o fafont mar-right-10"></i></a><a href="blogcategory/details/'.$GeneralFunction->encryptString($CatId).'" title="View Blog Category Details"><i class="fa fa-eye fafont"></i></a>';
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="cat_id[]" type="checkbox" class="rowcheckbox checkboxes" value="'.$GeneralFunction->encryptString($CatId).'"/><span></span></label>',
                $Name,
                $Slug,
                $Langnm,
                '<input type="checkbox" class="make-switch status" name="status" '.$checked.' data-id="'.$GeneralFunction->encryptString($CatId).'" data-on-text="<i class='.$check.'></i>" data-off-text="<i class='.$check1.'></i>" />',
                $ActLink,
            );
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);exit;
    }
    /**
     * @Route("admin/contentcms/blogcategory/deleterows")
     */
    public function deleterows(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        //Check Here Admin User is logged in or not
        $GeneralService = $this->get(GeneralService::class);
        parent::commonGlobalData();
        list($SiteSetting,$AdminDetails,$StoreImgSetting,$AdminSideBar) = $this->commonData;
        $lang_data = $em->getRepository(Language::class)->findBy(array('status'=>1,'isDeleted'=>0));
        $Permission = "";
        $Permission = $GeneralService->UserAccessDenied();
        if(empty($Permission)){
            echo 403;exit;
        }else{
            $GeneralFunction = new GeneralFunctionController;
            $DelId = $request->get('delid');
            $DelId1 = explode(',',$DelId);
            $cnt = 0;
            $datetime = new \DateTime('now');
            for($i=0;$i<count($DelId1);$i++)
            {
                $FildId      = $GeneralFunction->decryptString($DelId1[$i]);
                $fildname   = $em->getRepository('ContentcmsBundle:BlogCategory')->findOneById($FildId);
                if(!empty($fildname))
                {
                    $fildname->setIsDeleted(1);
                    $fildname->setUpdatedDate($datetime);
                    $em->flush();
                    $cnt++;
                }
            }
            if($cnt > 0)
            {
                echo 1;exit;
            }
            else
            {
                echo 0;exit;
            }
        }
    }
    /**
     * @Route("admin/contentcms/blogcategory/deleteonerows")
     */
    public function deleteonerows(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $GeneralService = $this->get(GeneralService::class);
        parent::commonGlobalData();
        list($SiteSetting,$AdminDetails,$StoreImgSetting,$AdminSideBar) = $this->commonData;
        $lang_data = $em->getRepository(Language::class)->findBy(array('status'=>1,'isDeleted'=>0));
        $Permission = "";
        $Permission = $GeneralService->UserAccessDenied();
        if(empty($Permission)){
            echo 403;exit;
        }else{
            $GeneralFunction = new GeneralFunctionController;
            $DelId = $request->get('delid');
            $DelId1 = $GeneralFunction->decryptString($DelId);
            $fildname = $em->getRepository('ContentcmsBundle:BlogCategory')->findOneById($DelId1);
            $datetime = new \DateTime('now');
            if(!empty($fildname))
            {
                $fildname->setIsDeleted(1);
                $fildname->setUpdatedDate($datetime);
                $em->flush();
                echo 1;exit;
            }
            else
            {
                echo 0;exit;
            }
        }
    }
    /**
     * @Route("admin/contentcms/blogcategory/statusonerows")
     */
    public function statusonerows(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $GeneralService = $this->get(GeneralService::class);
        parent::commonGlobalData();
        list($SiteSetting,$AdminDetails,$StoreImgSetting,$AdminSideBar) = $this->commonData;
        $lang_data = $em->getRepository(Language::class)->findBy(array('status'=>1,'isDeleted'=>0));
        $Permission = "";
        $Permission = $GeneralService->UserAccessDenied();
        if(empty($Permission)){
            echo 403;exit;
        }else{
            $GeneralFunction = new GeneralFunctionController;
            $UpdId = $request->get('updid');
            $UpSts = $request->get('upsts');
            $UpdId1 = $GeneralFunction->decryptString($UpdId);
            $fildname = $em->getRepository('ContentcmsBundle:BlogCategory')->findOneById($UpdId1);
            $datetime = new \DateTime('now');
            if(!empty($fildname))
            {
                $fildname->setStatus($UpSts);
                $fildname->setUpdatedDate($datetime);
                $em->flush();
                echo 1;exit;
            }
            else
            {
                echo 0;exit;
            }
        }
    }
    /**
     * @Route("admin/contentcms/blogcategory/addblogcategory", name="admin_blogcategory_new")
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $GeneralFunction = new GeneralFunctionController();
        //Check Here Admin User is logged in or not
        $GeneralService = $this->get(GeneralService::class);
        $AdminUserId = $GeneralService->getAdminUserSession();

        $Permission = "";
        $Permission = $GeneralService->UserAccessDenied();
        if(empty($Permission)){
            header('HTTP/1.0 403 Forbidden');
            $TempPage = 'ContentcmsBundle:Admin/CMSPage:errordeniedpageadmin.html.twig';
        }else{
            $TempPage = 'ContentcmsBundle:Admin/BlogCategory:new.html.twig';
        }

        //Get User Admin Session Details
        $AdminDetails = $GeneralService->getAdminSessionDetails();

        //Get Store Main Configuration
        $SiteSetting = $GeneralService->getStoreMainConfig();
        $StoreImgSetting = json_decode($SiteSetting['storeLogoSetting']);

        //Get Admin User Sidebar and access module
        $AdminSideBar = $GeneralService->getAdminUserSidebar($AdminDetails);

        $datetime = new \DateTime('now'); // For CMSPage Table
        $lang_data = $em->getRepository('LocalizationBundle:Language')->findBy(array('status'=>1,'isDeleted'=>0));
        $blogcategory = new BlogCategory();
        $form = $this->createFormBuilder($blogcategory)
            ->add('name', TextType::class, array('label' => 'Name','data_class' => null,'required'=>true))
            ->add('categorySlug', TextType::class, array('label' => 'Slug','data_class' => null,'required'=>true))
            ->add('description',TextareaType::class, array('label' => 'Description','data_class' => null,'required'=>true))
            ->add('status',CheckboxType::class,array('label'=> 'Status','required' => false))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $form_data  = $request->request->get('form');
            $language   = $request->request->get('language');
            $slug       = $form_data['categorySlug'];
            if(!empty($language))
            {
                $blogcategory->setIsDeleted(0);
                $blogcategory->setCreatedDate($datetime);
                $blogcategory->setUpdatedDate($datetime);
                $count = 1;
                foreach($language as $lang_val){
                    $lang_obj = $em->getRepository(Language::class)->find($lang_val);
                    if($count == 1){
                        $blogcategory->setLang($lang_obj);
                        $em->persist($blogcategory);
                    }else{
                        $lang_code      = $lang_obj->getCode();
                        $new_blogcategory_data = $em->getRepository(BlogCategory::class)->find($blogcategory->getId());
                        $new_blogcategory_obj = clone $new_blogcategory_data;
                        $new_blogcategory_obj->setLang($lang_obj);
                        $em->persist($new_blogcategory_obj);
                    }
                    $em->flush();
                    $count++;
                }
            }
            $this->addFlash('success','Blog Category added successfully');
            return $this->redirectToRoute('admin_contentcms_blogcategory');
        }

        return $this->render($TempPage,array(
            'SiteSetting'=>$SiteSetting,
            'AdminDetails'=>$AdminDetails,
            'StoreImgSetting'=>$StoreImgSetting,
            'AdminSideBar'=>$AdminSideBar,
            'form' => $form->createView(),
            'lang_data' =>$lang_data,
        ));
    }
    /**
     * @Route("admin/contentcms/blogcategory/editblogcategory/{id}", name="admin_blogcategory_edit")
     */
    public function editAction(Request $request)
    {
        $encId = "";
        $encId = $request->attributes->get('id');
        $em = $this->getDoctrine()->getManager();
        $GeneralFunction = new GeneralFunctionController();
        if(!empty($encId))
        {
            $CatId = $GeneralFunction->decryptString($encId);
            $blogcategory = $em->getRepository('ContentcmsBundle:BlogCategory')->find($CatId);
            if(empty($blogcategory)){
                return $this->redirectToRoute('admin_contentcms_blogcategory');
            }
        }
        else
        {
            return $this->redirectToRoute('admin_contentcms_blogcategory');
        }
        //Check Here Admin User is logged in or not
        $GeneralService = $this->get(GeneralService::class);
        $AdminUserId = $GeneralService->getAdminUserSession();

        $Permission = "";
        $Permission = $GeneralService->UserAccessDenied();
        if(empty($Permission)){
            header('HTTP/1.0 403 Forbidden');
            $TempPage = 'ContentcmsBundle:Admin/CMSPage:errordeniedpageadmin.html.twig';
        }else{
            $TempPage = 'ContentcmsBundle:Admin/BlogCategory:edit.html.twig';
        }

        //Get User Admin Session Details
        $AdminDetails = $GeneralService->getAdminSessionDetails();

        //Get Store Main Configuration
        $SiteSetting = $GeneralService->getStoreMainConfig();
        $StoreImgSetting = json_decode($SiteSetting['storeLogoSetting']);

        //Get Admin User Sidebar and access module
        $AdminSideBar = $GeneralService->getAdminUserSidebar($AdminDetails);

        $datetime = new \DateTime('now'); // For Category Table
        $lang_data = $em->getRepository('LocalizationBundle:Language')->findBy(array('status'=>1,'isDeleted'=>0));
        $form = $this->createFormBuilder($blogcategory)
            ->add('name', TextType::class, array('label' => 'Name','data_class' => null,'required'=>true))
            ->add('categorySlug', TextType::class, array('label' => 'Slug','data_class' => null,'required'=>true))
            ->add('description',TextareaType::class, array('label' => 'Description','data_class' => null,'required'=>true))
            ->add('status',CheckboxType::class,array('label'=> 'Status','required' => false))
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $form_data  = $request->request->get('form');
            $language   = $request->request->get('language');
            $slug       = $form_data['slug'];
            if(!empty($language))
            {
                $lang_obj = $em->getRepository(Language::class)->find($language);
                $blogcategory->setLang($lang_obj);
                $blogcategory->setUpdatedDate($datetime);
                $em->persist($blogcategory);
                $em->flush();
            }
            $this->addFlash('success','Blog Category updated successfully');
            return $this->redirectToRoute('admin_contentcms_blogcategory');
        }

        return $this->render($TempPage,array(
            'SiteSetting'=>$SiteSetting,
            'AdminDetails'=>$AdminDetails,
            'StoreImgSetting'=>$StoreImgSetting,
            'AdminSideBar'=>$AdminSideBar,
            'form' => $form->createView(),
            'lang_data' =>$lang_data,
            'blogcategory' => $blogcategory,
        ));
    }
    /**
     * @Route("admin/contentcms/blogcategory/details/{id}", name="admin_blogcategory_details")
     */
    public function detailsAction(Request $request)
    {
        $encId = "";
        $encId = $request->attributes->get('id');
        if(!empty($encId))
        {
            $GeneralFunction = new GeneralFunctionController();
            $em = $this->getDoctrine()->getManager();
            $CatId = $GeneralFunction->decryptString($encId);
            $blogcategory = $em->getRepository('ContentcmsBundle:BlogCategory')->find($CatId);
            if(empty($blogcategory)){
                return $this->redirectToRoute('admin_contentcms_blogcategory');
            }
            $lang_name = $this->getLanguageNamebyId($blogcategory->getLang());
        }
        else
        {
            return $this->redirectToRoute('admin_contentcms_blogcategory');
        }
        $em = $this->getDoctrine()->getManager();
        //Check Here Admin User is logged in or not
        $GeneralService = $this->get(GeneralService::class);
        $AdminUserId = $GeneralService->getAdminUserSession();

        $Permission = "";
        $Permission = $GeneralService->UserAccessDenied();
        if(empty($Permission)){
            header('HTTP/1.0 403 Forbidden');
            $TempPage = 'ContentcmsBundle:Admin/CMSPage:errordeniedpageadmin.html.twig';
        }else{
            $TempPage = 'ContentcmsBundle:Admin/BlogCategory:details.html.twig';
        }

        //Get User Admin Session Details
        $AdminDetails = $GeneralService->getAdminSessionDetails();

        //Get Store Main Configuration
        $SiteSetting = $GeneralService->getStoreMainConfig();
        $StoreImgSetting = json_decode($SiteSetting['storeLogoSetting']);

        //Get Admin User Sidebar and access module
        $AdminSideBar = $GeneralService->getAdminUserSidebar($AdminDetails);

        return $this->render($TempPage,array(
            'SiteSetting'=>$SiteSetting,
            'AdminDetails'=>$AdminDetails,
            'StoreImgSetting'=>$StoreImgSetting,
            'AdminSideBar'=>$AdminSideBar,
            'blogcategory'=>$blogcategory,
            'lang_name'=>$lang_name,
        ));
    }
}

