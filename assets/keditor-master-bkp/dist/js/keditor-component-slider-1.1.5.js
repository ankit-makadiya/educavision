/**!
 * KEditor - Kademi content editor
 * @copyright: Kademi (http://kademi.co)
 * @author: Kademi (http://kademi.co)
 * @version: 1.1.5
 * @dependencies: $, $.fn.draggable, $.fn.droppable, $.fn.sortable, Bootstrap (optional), FontAwesome (optional)
 */
(function ($) {
    var KEditor = $.keditor;
    var flog = KEditor.log;

    KEditor.components['slider'] = {
        init: function (contentArea, container, component, keditor) {
            flog('init "slider" component', component);

            var componentContent = component.children('.keditor-component-content');
            var img = componentContent.find('img');

            img.css('display', 'inline-block');
        },
        settingEnabled: true,

        settingTitle: 'Slider Settings',

        initSettingForm: function (form, keditor) {
            flog('initSettingForm "slider" component');

            form.append(
                    '<form class="form-horizontal">' +
                    '   <div class="form-group">' +
                    '       <label class="col-sm-12">Select Slider</label>' +
                    '       <div class="col-sm-12">' +
                    '           <select name="slider" id="dynamic-slider"><option value="">Select Slider</option></select>' +
                    '       </div>' +
                    '   </div>' +
                    '</form>'
                    );
        },
        showSettingForm: function (form, component, keditor, event) {
            flog('showSettingForm "slider" component', component);
            var options = keditor.options;
            var panel = component.find('.slider-panel');
            var slider = panel.find('img');
            event.preventDefault();

            $.ajax({
                url: '/admin/contentcms/slider/getsliderlist',
                type: 'POST',
                data: {},
                dataType: 'html',
                success: function (data) {
                    $('#dynamic-slider').html(data);
                },
                error: function (e) {

                }
            });

            var s = $('#dynamic-slider');
            s.on('change', function (event) {
                var s_id = s.val();
                event.preventDefault();
                $.ajax({
                    url: '/admin/contentcms/slider/getsliderimage',
                    type: 'POST',
                    data: {
                        id: s_id,
                    },
                    dataType: 'html',
                    success: function (response) {
                        panel.html(response);
                        keditor.showSettingPanel(component, options);
                    },
                    error: function (e) {

                    }
                });
            });
        }
    };

})(jQuery);
