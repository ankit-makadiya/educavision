/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here. For example:
    // config.language = 'fr';
    // config.uiColor = '#AADC6E';
    //config.extraPlugins = 'sharedspace';
    config.extraPlugins = 'sourcedialog';
    config.filebrowserBrowseUrl = '/assets/global/plugins/kcfinder/browse.php?opener=ckeditor&type=images';
    config.filebrowserImageBrowseUrl = '/assets/global/plugins/kcfinder/browse.php?opener=ckeditor&type=images';
    config.filebrowserFlashBrowseUrl = '/assets/global/plugins/kcfinder/browse.php?opener=ckeditor&type=flash';
    config.filebrowserUploadUrl = '/assets/global/plugins/kcfinder/browse.php?opener=ckeditor&type=images';
    config.filebrowserImageUploadUrl = '/assets/global/plugins/kcfinder/upload.php?opener=ckeditor&type=images';
    config.filebrowserFlashUploadUrl = '/assets/global/plugins/kcfinder/upload.php?opener=ckeditor&type=flash';
    //config.allowedContent = 'div{*}';
    config.allowedContent = true;
    config.protectedSource.push(/<i[^>]*><\/i>/g);
    CKEDITOR.dtd.$removeEmpty['i'] = false;
    config.contentsCss = ['/assets/keditor-master/CMS/plugins/ckeditor-4.5.6/contents.css', '/assets/global/plugins/front/simple-line-icons/simple-line-icons.min.css'];

    config.toolbar =
            [
                {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', '-', 'RemoveFormat']},
                {name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote',
                        '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
                {name: 'styles', items: ['Styles', 'Format', 'FontSize']},
                {name: 'colors', items: ['TextColor', 'BGColor']},
                '/',
                {name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                {name: 'editing', items: ['SpellChecker', 'Scayt']},
                {name: 'links', items: ['Link', 'Unlink', 'Anchor']},
                {name: 'insert', items: ['Image', 'Table', 'HorizontalRule', 'SpecialChar']},
                {name: 'tools', items: ['Maximize']},
                {name: 'document', items: ['Source', '-']}
            ];

//    config.toolbar_Full =
//            [
//                {name: 'document', items: ['Source', '-', 'Save', 'NewPage', 'DocProps', 'Preview', 'Print', '-', 'Templates']},
//                {name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
//                {name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll', '-', 'SpellChecker', 'Scayt']},
//                {name: 'forms', items: ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton',
//                        'HiddenField']},
//                '/',
//                {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
//                {name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv',
//                        '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']},
//                {name: 'links', items: ['Link', 'Unlink', 'Anchor']},
//                {name: 'insert', items: ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe']},
//                '/',
//                {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
//                {name: 'colors', items: ['TextColor', 'BGColor']},
//                {name: 'tools', items: ['Maximize', 'ShowBlocks', '-', 'About']}
//            ];
//
//    config.toolbar_Basic =
//            [
//                ['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink', '-', 'About']
//            ];
};
